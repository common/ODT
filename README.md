# Documentation
The ODT code has doxygen documentation that can be generated from the source.  
Alternatively, a version is updated nightly and available online [here](https://software.crsim.utah.edu/jenkins/job/ODT/doxygen)

In the [documentation](https://jenkins.multiscale.utah.edu/job/ODT/doxygen) you will find information about:
  - [Building the ODT code](https://jenkins.multiscale.utah.edu/job/ODT/doxygen/#building)
  - [Input files](https://jenkins.multiscale.utah.edu/job/ODT/doxygen/#parser)
  - [Source code documentation](https://jenkins.multiscale.utah.edu/job/ODT/doxygen/annotated.html)
A description of the theory behind ODT itself can be found [here](https://gitlab.multiscale.utah.edu/common/ODT/blob/master/doc/ICSE_100101.pdf).

A few papers where this code has been used:
  - Punati, N., Wang, H., Hawkes, E. R., & Sutherland, J. C. (2016). One-Dimensional Modeling of Turbulent Premixed Jet Flames - Comparison to DNS. Flow, Turbulence and Combustion, accepted. doi:10.1007/s10494-016-9721-x
  - Punati, N., Sutherland, J. C., Kerstein, A. R., Hawkes, E. R., & Chen, J. H. (2011). An Evaluation of the One-Dimensional Turbulence Model: Comparison with Direct Numerical Simulations of CO/H2 Jets with Extinction and Reignition. Proc. Combust. Inst., 33(1), 1515–1522. doi:10.1016/j.proci.2010.06.127
  - Biglari, A., & Sutherland, J. C. (2012). A filter-independent model identification technique for turbulent combustion modeling. Combustion and Flame, 159, 1960–1970. doi:10.1016/j.combustflame.2011.12.024
  - Goshayeshi, B., & Sutherland, J. C. (2015). Prediction of oxy-coal flame stand-off using high-fidelity thermochemical models and the one-dimensional turbulence model. Proceedings of the Combustion Institute, 35(3), 2829–2837. doi:10.1016/j.proci.2014.07.003
  - Goshayeshi, B., & Sutherland, J. C. (2014). A comparison of various models in predicting ignition delay in single-particle coal combustion. Combustion and Flame, 161, 1900–1910. doi:10.1016/j.combustflame.2014.01.010
  - Goshayeshi, B., & Sutherland, J. C. (2015). A comparative study of thermochemistry models for oxy-coal combustion simulation. Combustion and Flame, 162(10), 4016–4024. doi:10.1016/j.combustflame.2015.07.041

# Questions?
For questions, please contact [Professor Sutherland](https://sutherland.che.utah.edu)
