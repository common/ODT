import flamelet.CoalFlamelet as cf
import numpy as np
from spitfire2.flamelet.mechanism import ChemicalMechanismSpec
import cantera as ct
import pickle
import time
import os

chemicalMechFuel       = 'soot'
chemicalMechIdentifier = 'polimi'

mech = ChemicalMechanismSpec(chemicalMechFuel, chemicalMechIdentifier)

adiabaticGenerated = False
outputDir = '/Users/joshmcconnell/Documents/'

oxidizerStream = ct.Solution(mech.mech_xml_path)
fuelStream1    = ct.Solution(mech.mech_xml_path)

fuelStream1.TPY = 1000, ct.one_atm, {'C10H8': 1}
fuelStream2     = fuelStream1
oxidizerStream.TPX = 1200, ct.one_atm, {'O2':0.21, 'N2':0.79}

# tableVars = None
alphaVals  = np.linspace(0.0,0.0,1)
chiMaxVals = np.hstack([0.01, 0.05, np.linspace(1e-1, 0.5, 5), np.logspace(0, np.log10(2000), 20)])

# alphaVals  = np.array([0.14,0.3])
# chiMaxVals = np.array([0.001])

params = cf.coalFlameletParams(chemicalMechFuel       =chemicalMechFuel,
                               chemicalMechIdentifier = chemicalMechIdentifier,
                               chiMax                 = chiMaxVals,
                               alpha                  = alphaVals,
                               gammaSlices= 75,
                               refTemperature         = 298,
                               volatileStream         = fuelStream1,
                               charStream             = fuelStream2,
                               oxidizerStream         = oxidizerStream,
                               convectiveDeltaT       = -2000,
                               gammaUpperBound =  1.4,
                               gammaLowerBound = -0.3)

tableGen = cf.coalFlameletTableGenerator(params         = params,
                                         gridPoints     = 128,
                                         equilibrateBCs = False)

tableGen.savePath = os.path.join(outputDir, 'table.p')
tableGen.solverSpecs.tolerance = 2e-4
tableGen.solverSpecs.ds_max = 1e5
tableGen.solverSpecs.ds_init = 1e-2
tableGen.solverSpecs.residual_tol = np.inf
tableGen.solverSpecs.norm_order = np.inf
tableGen.solverSpecs.logInterval = 1
tableGen.plot = True
    # tableGen.solverSpecs = cf.SolverSpec(ds_init = 1e-3)
if not adiabaticGenerated:
    t = time.time()
    tableGen.generate_adiabatic_table()
    print('solve over alpha and chiMax took ', time.time() - t, 'seconds')
    tableGen.save_table()
    adiabaticGenerated = True
else:
    # if tableVars is None:
    #     tableVars = gtv.generate_table_vars(coalType = coalType,dbDir = dbDir, canteraInput = mech.mech_path)
    table = pickle.load(open(tableGen.savePath, 'rb'))
    tableGen.setup()
    tableGen.table = table.copy()
    tableGen.extrude_table_for_gamma()
    tableGen.savePath = os.path.join(outputDir, 'table-4D.p')
    tableGen.save_table()
    tableGen.gammaLowerBound = -0.2
    tableGen.gammaUpperBound = 0.8
    #
    tableGen.htcMinMultiplier          = 1.05
    tableGen.htcMaxMultiplier          = 1.3
    tableGen.maxSolveRetries           = 10
    tableGen.findConvCoeffbounds       = 1
    tableGen.gammaRefinementFactor     = None
    tableGen.doFlameletSolveAferInterp = False
    tableGen.generate_noadiabatic_table()