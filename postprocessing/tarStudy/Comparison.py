# %load_ext autoreload
# %autoreload 2

import tarStudy.MixtureFractionParameterization as mfp
import numpy as np
import cantera as ct
import util.coal as coal
import util.stream_spec as stream_spec
import matplotlib as mpl
import matplotlib.pyplot as plt
from mixtureFraction import equivalence_ratio
# from glue import qglue

plt.rc('axes', axisbelow=True)

import os

import config

font = {'family' : 'sans-serif',
       'weight' : 'normal',
       'size'   : 14}

mpl.rc('font', **font)

mechName     = "methane-lu30"
canteraInput = os.path.join(config.chemicalMechDir,mechName,mechName + ".xml")

tableDir_tar_c10h8 = '/Users/joshmcconnell/runs/OFC/tarStudy/' \
                     'tarIsC2H2=false-speciation=false-diffDiff=false/table-4D.p'
tableDir_tar_c2h2  = '/Users/joshmcconnell/runs/OFC/tarStudy/new/sootIsTar/' \
                     'tarIsC2H2=true-empiricalTar=true-speciation=false-diffDiff=false/0//table-4D.p'
tableDir_c2h2      = '/Users/joshmcconnell/runs/OFC/tarStudy/' \
                     'tarIsC2H2=true-speciation=false-diffDiff=false/table-4D.p'
P      = ct.one_atm
T_ox   = 1200   # oxidizer temperature
T_ref  = 298    # reference temperature for calculating enthalpy deficit

airMoleFracs = {'O2' : 0.21,'N2' : 0.79}
oxidizerStream = coal.oxidizer_stream(T_ox, P, airMoleFracs, canteraInput, 'mole')

# ----------------------
#  - soot is assumed to be carbon : tar --> soot + H2
#  - empirical formula for tar is C10H8 (naphthalene)
#  - differential diffusion and volatiles speciation ENABLED
dbDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/",
                      "new/sootIsCarbon/tarIsC2H2=false-empiricalTar=true-speciation=true-diffDiff=true/",
                      "0","0.db")
sootIsC_c10h8_ds = \
    mfp.MixtureFractionParameterization(dbDir=dbDir,
                                        canteraInput=canteraInput,
                                        tarFormula=coal.empiricalFuel(C=10,H=8),
                                        sootFormula=coal.empiricalFuel(C=1,H=0),
                                        oxidizerStream=oxidizerStream)
sootIsC_c10h8_ds.tableDir = tableDir_tar_c10h8

# ----------------------
#  - soot is assumed to be carbon : tar --> soot + H2
#  - empirical formula for tar is C10H8 (naphthalene)
#  - differential diffusion and volatiles speciation DISABLED
dbDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/",
                      "new/sootIsCarbon/tarIsC2H2=false-empiricalTar=true-speciation=false-diffDiff=false/",
                      "0","0.db")
sootIsC_c10h8 = \
    mfp.MixtureFractionParameterization(dbDir=dbDir,
                                        canteraInput=canteraInput,
                                        tarFormula=coal.empiricalFuel(C=10,H=8),
                                        sootFormula=coal.empiricalFuel(C=1,H=0),
                                        oxidizerStream=oxidizerStream)
sootIsC_c10h8.tableDir = tableDir_tar_c10h8

# ----------------------
#  - soot is assumed to have the same composition as tar
#  - empirical formula for tar is C10H8 (naphthalene)
#  - differential diffusion and volatiles speciation DISABLED
dbDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/",
                      "tarIsC2H2=false-speciation=false-diffDiff=false",
                      "0","0.db")
sootIsTar_c10h8 = \
    mfp.MixtureFractionParameterization(dbDir=dbDir,
                                        canteraInput=canteraInput,
                                        tarFormula=coal.empiricalFuel(C=10,H=8),
                                        sootFormula=coal.empiricalFuel(C=10,H=8),
                                        oxidizerStream=oxidizerStream)
sootIsTar_c10h8.tableDir = tableDir_tar_c10h8

# ----------------------
#  - soot is assumed to have the same composition as tar
#  - empirical formula for tar is C2H2 (acetylene)
#  - differential diffusion and volatiles speciation DISABLED
dbDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/",
                      "new/sootIsTar/tarIsC2H2=true-empiricalTar=true-speciation=false-diffDiff=false/",
                      "0","0.db")
sootIsTar_c2h2 =\
mfp.MixtureFractionParameterization(dbDir=dbDir,
                                    canteraInput=canteraInput,
                                    tarFormula=coal.empiricalFuel(C=2,H=2),
                                    sootFormula=coal.empiricalFuel(C=2,H=2),
                                    oxidizerStream=oxidizerStream)
sootIsTar_c2h2.tableDir = tableDir_tar_c2h2

#  - no tar/soot model -- tar surrogate is C2H2 and participates in chemical mechanism
#  - differential diffusion and volatiles speciation DISABLED
dbDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/",
                      "tarIsC2H2=true-speciation=false-diffDiff=false",
                      "0","0.db")
c2h2 = \
    mfp.MixtureFractionParameterization(dbDir=dbDir,
                                        canteraInput=canteraInput,
                                        tarFormula=None,
                                        sootFormula=None,
                                        oxidizerStream=oxidizerStream)
c2h2.tableDir = tableDir_c2h2
c2h2.alpha = np.zeros(c2h2.alpha.shape)
# ----------------------
cases = {
     "soot=C-tar=C10H8-ds"  : sootIsC_c10h8_ds,
     "soot=C-tar=C10H8"     : sootIsC_c10h8,
     "soot=tar=C10H8"       : sootIsTar_c10h8,
     "soot=tar=C2H2"        : sootIsTar_c2h2,
     "regular-species-C2H2" : c2h2
}



# ----------------------
def get_lims(fieldList, pad=0.):
    fMin = np.inf
    fMax = -np.inf
    for f in fieldList:
        fMin = min(f.min(), fMin)
        fMax = max(f.max(), fMax)
    fRange = fMax - fMin
    return ( fMin - pad*fRange, fMax + pad*fRange )

# ----------------------
def db_field(field):
    if field == 'T':
        return field
    else:
        return 'Y_'+field

# ----------------------
def scatter_2d(dDict, fieldName, color, saveDir=None):
    assert type(dDict) is dict
    # for val in dDict.values():
    #     assert isinstance(val, mfp.MixtureFractionParameterization)

    dbField = db_field(fieldName)
    ub = oxidizerStream.mass_fraction_dict()['O2']*0.99

    y = dict()
    x = dict()
    c = dict()
    for key, val in dDict.items():
        cond = val.db.get_field('Y_O2')<ub
        y[key] = val.db.get_field(dbField)[cond]
        x[key] = (equivalence_ratio(val))[cond]
        c[key] = getattr(val,color)[cond]


    # xlim = (0, max(get_lims(x.values(), pad=0.02)))
    xlim = (0,4)
    ylim = get_lims(y.values(), pad = 0.02)
    clim = get_lims(c.values())

    a = 0.1
    s = 2
    map = plt.cm.jet

    for key, val in dDict.items():

            plt.figure(key)
            plt.scatter(x[key],
                        y[key],
                        s=s,
                        alpha=a,
                        c=c[key],
                        cmap=map,
                        vmin=min(clim),
                        vmax=max(clim))
            cb = plt.colorbar(aspect=20, extend='both', )
            cb.set_alpha(1)
            cb.draw_all()
            plt.grid()
            plt.xlim(xlim)
            plt.ylim(ylim)
            plt.tight_layout()

            if saveDir is not None:
                svd = os.path.join(saveDir, 'color='+color)
                if not os.path.exists(svd):
                    os.makedirs(svd)
                plt.savefig(os.path.join(svd,fieldName+'-'+key+'.png'))
                plt.close()

# ----------------------
def parity_plot(dDict, fieldName, saveDir=None):
    assert type(dDict) is dict
    # for val in dDict.values():
    #     assert isinstance(val, mfp.MixtureFractionParameterization)

    dbFieldName = db_field(fieldName)
    ub = oxidizerStream.mass_fraction_dict()['O2']*0.98

    y = dict()
    x = dict()
    c = dict()
    for key, val in dDict.items():
        yO2 = val.db.get_field('Y_O2')
        cond = (yO2 < ub) \
               & (val.chiMax < 2000) \
               & (val.chiMax > 0) \
               & (val.gamma < 0.6)

        x[key] = val.db.get_field(dbFieldName)[cond]
        y[key] = val.slfm[fieldName][cond]
        # c[key] = getattr(val,color)[cond]
        c[key] = val.equivalenceRatio[cond]

    xlim = get_lims(list(x.values())+ list(y.values()),0.02)
    ylim = xlim
    clim = (0,3)

    a = 0.1
    s = 2
    map = plt.cm.jet

    for key, val in dDict.items():

            plt.figure(key)
            plt.scatter(x[key],
                        y[key],
                        s=s,
                        alpha=a,
                        c=c[key],
                        cmap=map,
                        vmin=min(clim),
                        vmax=max(clim))
            cb = plt.colorbar(aspect=20, extend='both', )
            cb.set_alpha(1)
            cb.draw_all()
            plt.grid()
            plt.xlim(xlim)
            plt.ylim(ylim)
            plt.tight_layout()

            plt.plot(xlim, xlim, 'k--', linewidth=1.2)

            if saveDir is not None:
                svd = os.path.join(saveDir,fieldName)
                if not os.path.exists(svd):
                    os.makedirs(svd)
                plt.savefig(os.path.join(svd,key+'.png'))
                plt.close()

