# %load_ext autoreload
# %autoreload 2

import ExprDatabase as exdb
import numpy as np
import cantera as ct
import util.coal as coal
import mixtureFraction as mf
import matplotlib as mpl
import matplotlib.pyplot as plt
plt.rc('axes', axisbelow=True)
import matplotlib.animation as anm
from mpl_toolkits.mplot3d import Axes3D
from collections import OrderedDict
import time
import os
import pickle

import config

import mixtureFraction.models.BurkeSchumannModel as BS
import mixtureFraction.models.EquilibriumModel as EQ
import mixtureFraction.models.SteadyFlameletModel as SF

import util.TransportEquationRHSTerms as rhsTerms

mechName     = "methane-lu30"
canteraInput = os.path.join(config.chemicalMechDir,mechName,mechName + ".xml")

font = {'family' : 'serif',
       'weight' : 'normal',
       'size'   : 12}

mpl.rc('font', **font)

wkDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/tarIsC2H2=false-speciation=false-diffDiff=false" )

dbDir = os.path.join(wkDir,"0","0.db")

tableDir = os.path.join( os.path.expanduser('~'),
                      "runs/OFC/tarStudy/tarIsC2H2=false-speciation=false-diffDiff=false",
                        "table-4D.p")

minTime = 0.0
maxTime = 0.25
interval = 1

db = exdb.ExprDatabase(dbDir, interval, minTime, maxTime)
db.set_cantera_input_file(canteraInput)
db.set_state_variables()

# remove volatile species we aren't considering
coal.devSpec.remove('NH3')
coal.devSpec.remove('HCN')

# set tar empirical formula
coal.tar = coal.empiricalFuel(C=10, H=10)

coal.load_particle_variables(db)
P      = ct.one_atm
T_ox   = 1200   # oxidizer temperature
T_ref  = 298    # reference temperature for calculating enthalpy deficit

airMoleFracs = {'O2' : 0.21,'N2' : 0.79}
oxidizerStream = coal.oxidizer_stream(T_ox, P, airMoleFracs, canteraInput, 'mole')

volatileStream = coal.volatiles_stream(db, P, canteraInput)
tarStream = coal.tar_stream(db,P,canteraInput)

streams = OrderedDict()
streams['volatiles'] = volatileStream
streams['tar'      ] = tarStream
streams['oxidizer' ] = oxidizerStream

zv = db.get_field('volatilesMixtureFraction')
zt = db.get_field('tarMixtureFraction'      )

zTot = zv + zt
alpha = zv/zTot; alpha[np.isnan(alpha)] = 0

mixFracs = {'volatiles' : zv, 'tar' : zt}

streamMix = mf.StreamMixture(canteraInput)
streamMix.streams = streams
streamMix.mixtureFractions = mixFracs
# streamMix.set_mixture_fractions_from_composition(db.mix(), elements)

# stoichiometric mixture fraction
ofrVol = mf.stoichiometric_oxidizer_to_fuel_mass_ratio(volatileStream, oxidizerStream)
ofrTar = mf.stoichiometric_oxidizer_to_fuel_mass_ratio(tarStream     , oxidizerStream)
zStoich = (1 + alpha*ofrVol + (1-alpha)*ofrTar)**-1

chi = mf.calculate_scalar_dissipation_rate(zTot, db.mix, db)
chiMax = mf.chi_to_chiMax(chi,zTot)
chiMax[np.isnan(chiMax)] = 0
chiMax = chiMax.clip(None, 1e5)

# calculate gamma
gamma = mf.calculate_gamma(db.mix(), streamMix.mix(), T_ref=T_ref)

z = np.linspace(0,1,201)
# ------------
yO2 = db.get_field('Y_O2')
percent = np.random.random(yO2.shape) < 1
cond = (yO2 < yO2[0,1]*0.98) & (chiMax <2000) & (chiMax > 0) & (gamma < 0.7)
# ------------
fields = volatileStream.species_names; del fields[-1]
fields += ['T']

# -- burke-schumann model
bs = BS.BurkeShumannModel(canteraInput, streams, mixFracs, T_ref)
bsDict = bs.values(alpha, gamma, fields)

# -- equilibrium model
eq = EQ.EquilibriumModel(canteraInput, streams, mixFracs, T_ref)
eqDict = eq.values(alpha, gamma, fields)

# -- steady flamelet model
table = pickle.load(open(tableDir, 'rb'))
sf = SF.SteadyFlameletModel(canteraInput, streams, mixFracs, table)
sfDict = sf.values(chiMax, alpha, gamma, fields + ['gamma'])

def partiy_plot(field, model, colorBy, condition=None):
    assert type(model) is dict
    if condition is None:
        condition = np.ones(db.get_field('T').shape, dtype=bool)

    plt.figure(field, figsize=(4.5,3))
    if field == 'T':
        dbField = field
    else:
        dbField = 'Y_'+field

    obs = db.get_field(dbField)[condition]
    pre = model[field][condition]

    plt.scatter(obs,
                pre,
                s = 2,
                alpha = 0.1,
                c=colorBy['color'][condition],
                cmap=plt.cm.jet)

    cb = plt.colorbar(aspect=20)
    cb.set_alpha(1)
    cb.draw_all()
    cb.set_label(colorBy['label'])
    plt.tight_layout()
    plt.grid()

    maxVal = max(np.max(obs), np.max(pre))
    minVal = min(np.min(obs), np.min(pre))

    r = maxVal - minVal

    line = (minVal - r*0.05, maxVal + r*0.05)
    plt.plot(line, line, 'k--', linewidth=1.2)
    plt.xlim(line)
    plt.ylim(line)

def generate_flamelet(fieldsArg, zArg, chiMaxArg, alphaArg, gammaArg):
    gammaProxy = sf.interpolator.gamma_to_gamma_proxy(zArg, chiMaxArg, alphaArg, gammaArg)
    slfmFields = dict()
    for field in fieldsArg:
        slfmFields[field] =sf.interpolator.interpolate( zArg,
                                                        chiMaxArg,
                                                        alphaArg,
                                                        gammaProxy,
                                                        field )
    return slfmFields

# this sets an upper bound for which

# figMaker = fm.figureMaker(db, streamMix, zTot, chiMax, gamma, T_ref, table)
