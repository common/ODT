import ExprDatabase as exdb
import numpy as np
import cantera as ct
import util.coal as coal
import mixtureFraction as mf
import matplotlib as mpl
import matplotlib.pyplot as plt
plt.rc('axes', axisbelow=True)
import matplotlib.animation as anm
from collections import OrderedDict
import time
import os
import pickle

import mixtureFraction.models.BurkeSchumannModel as BS
import mixtureFraction.models.EquilibriumModel as EQ
import mixtureFraction.models.SteadyFlameletModel as SF

# remove volatile species we aren't considering
coal.devSpec.remove('NH3')
coal.devSpec.remove('HCN')


class MixtureFractionParameterization:
    def __init__(self,
                 dbDir,
                 canteraInput,
                 tarFormula,
                 sootFormula,
                 oxidizerStream,
                 tableDir = None,
                 flameletTable = None,
                 minTime = 0,
                 maxTime = np.inf,
                 timeInterval=1,
                 setMixFracsFromComposition=False):
        self.dbDir        = dbDir
        self.canteraInput = canteraInput
        self.tarFormula   = tarFormula
        self.sootFormula  = sootFormula
        self.T_ref = 298
        self.db  = exdb.ExprDatabase(dbDir, timeInterval=timeInterval, minTime=minTime, maxTime=maxTime)
        self.db.set_cantera_input_file(canteraInput)
        self.db.set_state_variables()
        coal.tar  = tarFormula
        coal.soot = sootFormula


        self.z_vol = self.db.get_field('volatilesMixtureFraction')
        self.z_tar = self.db.get_field('tarMixtureFraction')

        coal.load_particle_variables(self.db)
        P = ct.one_atm

        self.streams = OrderedDict()
        self.oxidizerStream = oxidizerStream
        self.volatileStream = coal.volatiles_stream(self.db, P, canteraInput)
        noTar = False
        if self.z_tar.max() == 0.:
            noTar = True
            self.tarStream = self.volatileStream
        else:
            self.tarStream = coal.tar_stream(self.db, P, canteraInput)

        self.streams['tar']       = self.tarStream
        self.streams['volatiles'] = self.volatileStream
        self.streams['oxidizer' ] = self.oxidizerStream

        self.mixtureFractions = {'volatiles' : self.z_vol, 'tar' : self.z_tar}

        self.z_tot = self.z_vol + self.z_tar

        if noTar:
            print('removing tar stream from consideration')
            del self.mixtureFractions['tar']
            del self.streams         ['tar']

        streamMix = mf.StreamMixture(canteraInput)
        streamMix.streams = self.streams

        if setMixFracsFromComposition:
            print('setting mixture fractions from composition...')
            elements = ['C', 'H', 'O']
            streamMix.set_mixture_fractions_from_composition(self.db.mix, elements)

            self.z_vol = np.clip(streamMix.mixtureFractions['volatiles'], 0, 1)
            if not noTar:
                self.z_tar = np.clip(streamMix.mixtureFractions['tar'], 0, 1)
        else:
            streamMix.mixtureFractions = self.mixtureFractions

        self.alpha = self.z_vol / self.z_tot
        # take care of non-realizable alpha values
        self.alpha[np.isnan(self.alpha)] = 0
        self.alpha = np.clip(self.alpha, 0, 1)

        # oxidizer to fuel mass ratio of volatiles
        ofrStoichVol = mf.stoichiometric_oxidizer_to_fuel_mass_ratio(self.volatileStream,
                                                                     self.oxidizerStream)

        # oxidizer to fuel mass ratio of tar partially-reacted products (CO, H2O, N2)
        ofrStoichTar = mf.stoichiometric_oxidizer_to_fuel_mass_ratio(self.tarStream,
                                                                     self.oxidizerStream)

        # oxidizer to fuel mass ratio of tar partially-reacted products and volatiles mixture
        ofrStoich = self.alpha * ofrStoichVol + (1 - self.alpha) * ofrStoichTar

        self.z_stoich = (1 + ofrStoich) ** -1

        # equivalence ratio
        # = (fuel-to-oxidizer ratio) / (stoich. fuel-to-oxidizer ratio)
        # = (fuel-to-oxidizer ratio) * (stoich. oxidizer-to-fuel ratio)
        #
        # = zTot/(1-zTot) * (stoich. oxidizer-to-fuel ratio)
        self.equivalenceRatio = self.z_tot / (1 - self.z_tot) * ofrStoich

        chi = mf.calculate_scalar_dissipation_rate(self.z_tot, self.db.mix, self.db)
        self.chiMax = mf.chi_to_chiMax(chi, self.z_tot)
        self.chiMax[np.isnan(self.chiMax)] = 0
        self.chiMax = self.chiMax.clip(None, 1e5)

        self.gamma = mf.calculate_gamma(self.db.mix(), streamMix.mix(), T_ref=self.T_ref)

        self.fields = self.volatileStream.species_names
        del self.fields[-1]
        self.fields += ['T']

        self.tableDir          = tableDir
        self.flameletTable     = flameletTable
        self.flamelet          = None
        self.flameletVals      = None
        self.burkeSchumann     = None
        self.burkeSchumannVals = None
        self.equilibrium       = None
        self.equilibriumVals   = None

    # ---------------------------------------------------
    def initialize_flamelet_model(self):
        if self.tableDir is None:
            if self.flameletTable is None:
                raise RuntimeError('Member "tableDir" or "flameletTable"  needs to be set')
            else:
                print('Initializing flamelet model from table at member "flameletTable"')
        else:
            if self.flameletTable is not None:
                raise RuntimeError('Only one of member "tableDir" or "flameletTable" is allowed to be set. '
                                   'set either "tableDir" or "flameletTable"  to None and try again.')
            else:
                self.flameletTable = pickle.load(open(self.tableDir, 'rb'))

        assert (type(self.flameletTable) is dict)

        if self.flameletTable['params']['alpha'].size == 1:
            ss = self.flameletTable['T']
            for key, val in self.flameletTable.items():
                if np.shape(val) == np.shape(ss):
                    print(key)
                    self.flameletTable[key] = np.concatenate([val, val], axis=2)
                    self.flameletTable['params']['alpha'] = np.array([-0.1, 1.1])

        self.flamelet = SF.SteadyFlameletModel(self.canteraInput, self.streams, self.mixtureFractions, self.flameletTable)

    # ---------------------------------------------------
    def project_onto_flamelet_solution(self):
        if self.flamelet is None:
            self.initialize_flamelet_model()

        self.flameletVals = self.flamelet.values(self.chiMax, self.alpha, self.gamma, self.fields + ['gamma'])

    # ---------------------------------------------------
    def initialize_burke_schumann_model(self):
        self.burkeSchumann = BS.BurkeShumannModel(self.canteraInput, self.streams, self.mixtureFractions, self.T_ref)

    # ---------------------------------------------------
    def project_onto_burke_schumann_solution(self):
        if self.burkeSchumann is None:
            self.initialize_burke_schumann_model()

        self.burkeSchumannVals = self.burkeSchumann.values(self.alpha, self.gamma, self.fields)

    # ---------------------------------------------------
    def initialize_equilibrium_model(self):
        self.equilibrium = EQ.EquilibriumModel(self.canteraInput, self.streams, self.mixtureFractions, self.T_ref)

    # ---------------------------------------------------
    def project_onto_equilibrium_solution(self):
        if self.equilibrium is None:
            self.initialize_equilibrium_model()

        self.burkeSchumannVals = self.equilibrium.values(self.alpha, self.gamma, self.fields)

    # ---------------------------------------------------
    def tar_plus_soot_mass(self):
        return np.clip(self.db.get_field('rhoTar' ), 0, 1) + \
               np.clip(self.db.get_field('rhoSoot'), 0, 1)

    # ---------------------------------------------------
    def total_density(self):
        '''
        coputes density + density-weighted tar and soot mass fractions.
        '''

        # get sum of density-weighted mass fractions of tar and soot.
        return self.db.get_field('density') + self.tar_plus_soot_mass()

    # ---------------------------------------------------
    def tar_and_soot_unreacted_mixture_fraction(self):
        '''
        computes Z_ub =  (mass originating tar + soot)/(total mass) from a mixture fraction,
        Z_tar = (mass of partially oxidized tar/soot products)/(mass regular species), where "regular species"
        are species that directly couple to thermodynamic properties such as density and molecular weight (i.e. neither
        tar nor soot) and (total mass) = (mass of reqular species) + (mass tar) + (mass soot).

        tar is assumed to be composed completely of carbon and hydrogen. Soot is either pure carbon, or has the same
        empirical formula as tar.

        soot is assumed to be formed from tar though the following reaction:
        tar -> soot + H2.

        if tar and soot are assumed to have the same empirical formula, no H2 is evolved
        '''

        # get mass of carbon and hydrogen in fuel stream
        yHC = self.tarStream.elemental_mass_fraction('C') + \
              self.tarStream.elemental_mass_fraction('H')

        # get density-weighted tar/soot mixture fraction
        rhoZ_tar = np.clip(self.db.get_field('rhotarMixtureFraction'), 0, 1)

        # get density. this does not include direct contribution from tar nor soot
        rho = self.db.get_field('density')

        # compute mass originating from tar.
        massOriginatingFromTar = ( rhoZ_tar * yHC + self.tar_plus_soot_mass() )

        # compute and return the tar/soot mixture fraction
        return massOriginatingFromTar/self.total_density()

    # ---------------------------------------------------
    def tar_stoichiometric_oxidizer_to_fuel_mass_ratio(self):
        # mass of carbon and hydrogen in tar stream --> mass originating from CxHy in tar stream
        yHC = self.tarStream.elemental_mass_fraction('C') + \
              self.tarStream.elemental_mass_fraction('H')

        xC = self.tarStream.elemental_mass_fraction('C') / yHC
        xH = self.tarStream.elemental_mass_fraction('H') / yHC

        iO2 = self.oxidizerStream.species_index('O2')

        # availableO2 is the mass fraction of O2 in the oxidizer stream
        availableO2 = self.oxidizerStream.Y[..., iO2]

        # requiredO2 is the mass of O2 per mass of fuel required for stoichiometric conditions
        requiredO2 = (
                                xC / self.tarStream.atomic_weight('C')
                       + 0.25 * xH / self.tarStream.atomic_weight('H')
                     ) * self.oxidizerStream.molecular_weights[iO2]

        assert (requiredO2 > 0)

        return requiredO2 / availableO2

    # ---------------------------------------------------
    def unburned_equivalence_ratio(self):

        rho = self.db.get_field('density')

        # volatiles mixture fraction accounting for tar and soot mass
        Z_vol_unburned = self.z_vol * rho / self.total_density()
        z_tar_unburned = self.tar_and_soot_unreacted_mixture_fraction()
        z_tot_unburned = Z_vol_unburned + z_tar_unburned

        alpha_unburned = Z_vol_unburned / z_tar_unburned

        # take care of non-realizable alpha values
        alpha_unburned[np.isnan(alpha_unburned)] = 0
        alpha_unburned = np.clip(alpha_unburned, 0, 1)

        # oxidizer to fuel mass ratio of volatiles
        ofrStoichVol = mf.stoichiometric_oxidizer_to_fuel_mass_ratio(self.volatileStream,
                                                                     self.oxidizerStream)

        # oxidizer to fuel mass ratio of unreacted tar
        ofrStoichTar = self.tar_stoichiometric_oxidizer_to_fuel_mass_ratio()

        # oxidizer to fuel mass ratio of tar partially-reacted products and volatiles mixture
        ofrStoich = alpha_unburned * ofrStoichVol + (1 - alpha_unburned) * ofrStoichTar

        # z_stoich_unburned = (1 + ofrStoich) ** -1

        return z_tot_unburned / (1 - z_tot_unburned) * ofrStoich