# =====================================================================================================================
import LoadDatabase
import numpy
import cantera
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import psutil
import gc

class ContourOptions:
    def __init__(self):
        self.width = 14
        self.height = 5.4
        self.colorMap = plt.cm.jet
        self.colorbarAspect = 15
        self.norm = None


class ExprDatabase:

    def __init__(self, databaseDir, timeInterval, minTime = 0., maxTime = float('inf'), forceLoadFromODTDB = False):
        assert isinstance(databaseDir, str)
        assert isinstance(timeInterval,int)

        self.__npyMetaDataName = "metadata.p"

        self.__directory = os.path.expanduser(databaseDir)
        self.__min_time = minTime
        self.__max_time = maxTime

        self.__fieldNames = None

        self.__firstFieldLoaded = False
        self.__rhoYiLoaded      = False
        self.__mixInitialized   = False
        self.__stateVarsSet     = False
        self.__specPrefix     = 'rhoY_'
        self.__densityName    = 'density'
        self.__nthSpecies     = ''
        self.__speciesNames   = []
        self.__mix            = []
        self.__transportModel = 'Mix'
        self.__canteraInput = ''
        self.__fieldDictionary = dict()
        self.__aliasDictionary  = dict() # keys are aliases, values are field names
        self.__timeInterval = timeInterval
        self.__db = LoadDatabase.FieldDatabase(databaseDir)

        self.contourOptions = ContourOptions()
        self.__xMGrid = None
        self.__tMGrid = None

        self.__subdirs = self.__db._FieldDatabase__subdirs
        self.__subdirs = self.__subdirs[::timeInterval]
        self.__simTime = []

        indeciesToDelete = []
        for i in range( len(self.__subdirs) ):
            t = float(self.__subdirs[i])
            if self.__min_time <= t <= self.__max_time:
                self.__simTime.append(t)
            else:
                indeciesToDelete.append(i)

        for i in sorted(indeciesToDelete, reverse=True):
            del self.__subdirs[i]

        self.__simTime = np.array(self.__simTime)

        self.__isNumpyDB = False

        self.__setup(forceLoadFromODTDB)

        # nth species

# ......................................................................................................................
    def __setup(self, forceLoadFromODTDB):
        mdPath = os.path.join(self.__directory, self.__npyMetaDataName)

        if os.path.exists(mdPath) and not forceLoadFromODTDB:
            self.__isNumpyDB = True
            print("Fields will be loaded from a numpy binary database")

            # load metadata
            md = pickle.load(open(mdPath, 'rb'))

            self.__fieldNames = md['fields']

            timePath = os.path.join(self.__directory, "time.npy")
            self.__simTime = np.load(timePath)
        else:
            print("Fields will be loaded from an ExprLib database")
            try:
                d = self.__subdirs[0]
                self.__fieldNames = self.__db.get_database_entry(d).get_field_names()
            except:
                raise RuntimeError("An error occured while trying to obtain database field names. "
                                   "This is likely due to 'databaseDir' not pointing to an ExprLib database ")

# ......................................................................................................................
    def delete_FieldDatabase(self):
        process = psutil.Process(os.getpid())

        mem = process.memory_info().rss
        self.__db.delete_entries()
        gc.collect()
        mem = (mem - process.memory_info().rss)/1e6

        print(str(mem), " MB freed")

# ......................................................................................................................
    def load_field(self, fieldNames, fieldAliases=None, forceLoad = False):
        if fieldAliases == None:
            fieldAliases_ = fieldNames
        else:
            fieldAliases_ = fieldAliases

        # routine for loading a single field with name 'fieldNames'
        if type(fieldNames) == str:
            assert(type(fieldAliases_) == str )
            print('loading field "'+fieldNames+'" with alias "'+fieldAliases_+'"')
            # check if database has been converted to .npy files. Loading this is much faster than loading text.
            filePath = os.path.join(self.__directory, fieldNames + ".npy")

            if self.__isNumpyDB:
                print('loading from .npy')
                load_method = self.load_field_from_npy_into_db
            else:
                print('loading from ExprLib database')
                load_method = self.load_field_from_expr_db

            # check if a field has previously been loaded
            if fieldNames not in self.get_loaded_field_names() or forceLoad:
                load_method(fieldName = fieldNames, fieldAlias = fieldAliases_)
            else:
                print('field "' + fieldNames + '" has already been loaded')

        elif type(fieldNames) == list:
            n = len(fieldNames)
            assert(type(fieldAliases_) == list)
            assert(len(fieldAliases_)  == n   )

            for i in range(n):
                name  = fieldNames[i]
                alias = fieldAliases_[i]
                self.load_field(name, alias)

        else:
            raise ValueError('arguments "fieldNames" and "fieldAliases must be a "str" or a list where all elements are of type "str"')


# ......................................................................................................................
    def load_field_from_expr_db(self, fieldName, fieldAlias = None):
        f = []
        for t in self.__subdirs:
            if f == []:
                f = self.__db.get_field(t, fieldName).data()
            else:
                # this assumes data is 1-D for each output time. This will need to change if data is 2-D or 3-D
                f = numpy.concatenate([f, self.__db.get_field(t, fieldName).data()], axis=1)

        f = numpy.squeeze(f)
        self.__fieldDictionary.update({fieldAlias: f})
        self.__aliasDictionary.update({fieldAlias: fieldName})

# ......................................................................................................................
    def load_field_from_npy_into_db(self, fieldName, fieldAlias = None):

        if fieldAlias == None:
            fieldAlias_ = fieldName
        else:
            fieldAlias_ = fieldAlias

        filePath = os.path.join(self.__directory, fieldName + ".npy")

        f = np.load(filePath)

        self.__fieldDictionary.update({fieldAlias: f})
        self.__aliasDictionary.update({fieldAlias: fieldName})

# ......................................................................................................................
    def save_all_vars_to_npy(self, saveDir=None, deleteFields=False):
        if saveDir == None:
            saveDir_ = self.__directory
        else:
            saveDir_ = os.path.expanduser(saveDir)

        if not os.path.isdir(saveDir_):
            os.makedirs(saveDir_)

        metaData = dict()
        timeName = "time"

        metaData['time'  ] = timeName
        metaData['fields'] = []

        # save time values
        time = self.get_time()
        filePath = os.path.join(saveDir_, timeName)
        np.save(filePath, time)

        fieldNames = self.get_field_names()

        for fieldName in fieldNames:
            field = self.get_field(fieldName)
            filePath = os.path.join(saveDir_, fieldName)
            print("saving field ", fieldName)
            np.save(filePath, field)
            metaData['fields'].append(fieldName)
            if deleteFields:
                del self.__fieldDictionary[fieldName]

        self.__db.delete_entries()

        filePath = os.path.join(saveDir_, self.__npyMetaDataName)
        pickle.dump(metaData, open(filePath, 'wb'))

        print("done!")

# ......................................................................................................................
    def get_time(self):
        return self.__simTime

# ......................................................................................................................
    def get_loaded_field_names(self):
        return list(self.__fieldDictionary.keys())

# ......................................................................................................................
    def get_field(self, name):
        if name not in self.get_loaded_field_names():
            try:
                self.load_field(name)
            except:
                print('error occurred while trying to load field "' + name + '"')
                raise

        return self.__fieldDictionary.get(name).copy()

# ......................................................................................................................
    def get_field_names(self):
        return self.__fieldNames

# ......................................................................................................................
    @property
    def directory(self):
        return self.__directory

# ......................................................................................................................
    @property
    def species_names(self):
        return self.__speciesNames

# ......................................................................................................................
    @property
    def nth_species(self):
        return self.__nthSpecies

# ......................................................................................................................
    @property
    def cantera_input_file(self):
        return self.__canteraInput

# ......................................................................................................................
    @property
    def mix(self):
        '''returns a cantera.SolutionArray with temperature (T), pressure (P) and composition (Y_O2, Y_CO2, etc) in
        the database'''
        if not self.__mixInitialized:
            self.__initialize_cantera_objects()
            print('initializing cantera solution array')
            self.__mixInitialized = True

        return self.__mix

# ......................................................................................................................
    def mix_copy(self):
        g = cantera.Solution(self.__canteraInput)

        # calling get_mix() ensures that the mix is initialized
        mix = self.mix
        s = numpy.shape( mix.P )
        g = cantera.SolutionArray(g, s)

        g.TPY = mix.T, mix.P, mix.Y
        return g

# ......................................................................................................................

    def load_density_weighted_species_mass_fractions(self):
        names = self.get_field_names()
        n = len(self.__specPrefix)
        # find field names corresponding to density-weighted species mass fractions
        for name in names:
            if name[0:n] == self.__specPrefix:
                self.load_field(name)
                specName = name[n:]
                if specName not in self.__speciesNames: self.__speciesNames.append(specName)

        self.__rhoYiLoaded = True

# ......................................................................................................................
    def load_density_weighed_state_variables(self):
        if not self.__rhoYiLoaded:
            self.load_density_weighted_species_mass_fractions()

        self.load_field('rhoE0')

# ......................................................................................................................
    def set_nth_species(self, name):
            self.__nthSpecies = name

# ......................................................................................................................
    def set_species_mass_fractions(self):
        if not self.__rhoYiLoaded:
            self.load_density_weighted_species_mass_fractions()

        prefix = 'Y_'
        densName = self.__densityName
        self.load_field(densName)
        density = self.get_field(densName)

        for name in self.__speciesNames:
            massFracName = prefix + name
            rhoMassFracName = self.__specPrefix + name

            if massFracName not in self.get_loaded_field_names():
                print('adding ' + massFracName + ' to database')
                Yi = (self.get_field(rhoMassFracName)/density).clip(0, 1)
                self.__fieldDictionary.update({massFracName:Yi})
            else:
                print(massFracName + ' is already in database')

# ......................................................................................................................
    def set_state_variables(self):
        temperatureNameOptions = ['T','temperature']
        pressureNameOptions    = ['P','pressure']
        temperatureAlias = 'T'
        pressureAlias    = 'P'
        self.set_species_mass_fractions()

        densName = self.__densityName
        name    = 'E0'
        rhoName = 'rho' + name
        self.load_field(rhoName)

        if name not in self.get_loaded_field_names():
            print('adding ' + name + ' to database')
            E0 = self.get_field(rhoName)/self.get_field(densName)
            self.__fieldDictionary.update({name:E0})

        # load temperature and pressure
        self.__load_multiOptionField(temperatureNameOptions, temperatureAlias)
        self.__load_multiOptionField(pressureNameOptions   , pressureAlias   )

        self.__stateVarsSet = True

# ......................................................................................................................
    def set_cantera_input_file(self, canteraInputFile):
        self.__canteraInput = canteraInputFile

# ......................................................................................................................
    def __load_multiOptionField(self,fieldNameOptions, fieldAlias):
        fieldIsSet = False
        fieldName = '(name not reset)'
        for name in fieldNameOptions:
            if name in self.get_field_names():
                if fieldIsSet:
                    msg = 'field with name "'+name+'" and alias "'+fieldAlias+'has already been loaded as "'\
                          +fieldName+'"'
                else:
                    fieldName = name
                    self.load_field(fieldName, fieldAlias)
                    fieldIsSet = True

# ......................................................................................................................
    def __initialize_cantera_objects(self):
        if self.__canteraInput =='':
            raise ValueError('Cantera input file must be set by calling the "set_cantera_input_file" before '
                             'cantera objects are used/initialized')
        else:

            if not self.__stateVarsSet:
                self.set_state_variables()

            T = self.get_field('T')
            P = self.get_field('P')

            s                    = numpy.shape(T)
            soln                 = cantera.Solution(self.__canteraInput)
            soln.transport_model = self.__transportModel
            self.__mix           = cantera.SolutionArray(soln, s)

            nthSpec = self.nth_species
            if nthSpec == '':
                nthSpec = self.__mix.species_names[-1]
                self.set_nth_species(nthSpec)
                print('nth species set to "'+nthSpec+'" from cantera input file')
            else:
                print('nth species set to "' + nthSpec + '" by user input')

            n = self.__mix.n_species
            sY = s + (n,)
            Y = numpy.zeros(sY)
            Y_n = 1
            # 1st n-1 species
            for name in self.__speciesNames:

                i    = self.__mix.species_index(name)
                Y_i  = self.get_field('Y_'+name)
                Y_n -= Y_i
                Y[...,i] = Y_i

            # nth species
            i = self.__mix.species_index(self.nth_species)
            Y[..., i] = Y_n

            self.__mix.TPY = T, P, Y

# ......................................................................................................................
    def contourf(self, field, shades = 120, vmin = -np.inf, vmax = np.inf):

        if self.__xMGrid is None or self.__tMGrid is None:
            print('initializing contourf mesh')
            x = self.get_field('xcoord')
            L = x.max()
            npts = x.shape[0]
            self.__tMGrid, self.__xMGrid = np.meshgrid(self.get_time(),
                                                       np.linspace(-L / 2,L / 2, npts),
                                                       indexing='xy')

        if type(field) is str:
            field = self.get_field(field)

        figSize = (self.contourOptions.width, self.contourOptions.height)
        f = plt.figure(figsize=figSize)

        if not vmin == vmax ==None:
            field.clip(vmin, vmax)

        ax = plt.contourf(self.__tMGrid,
                          self.__xMGrid,
                          np.clip(field, vmin, vmax),
                          shades,
                          cmap=self.contourOptions.colorMap,
                          norm=self.contourOptions.norm)
        # plt.colorbar(ax, cax=cax)
        cb = plt.colorbar(ax, aspect=15, extend='both')
        plt.xlabel('time (s)')
        plt.ylabel('position (m)')
        plt.tight_layout()