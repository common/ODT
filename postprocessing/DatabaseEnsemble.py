import LoadDatabase as loaddb
import numpy as np
import os
import psutil
from collections import OrderedDict
import pickle
import gc

def load_from_saved(saveDir):
    fieldMean     = pickle.load(open( os.path.join(saveDir, 'fieldMean.p'    ), 'rb'))
    fieldVariance = pickle.load(open( os.path.join(saveDir, 'fieldVariance.p'), 'rb'))
    info          = pickle.load(open( os.path.join(saveDir, 'info.p'         ), 'rb'))

    return fieldMean, fieldVariance, info

class DatabaseManager:

    def __search_dir_error(self):
        return TypeError('argument "searchDirs" must be either a string or list of strings')
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    def __init__(self, searchDirs):
        self.databaseDict = OrderedDict()
        self.threshold = 6
        self.__speciesNames = set()

        if type(searchDirs) == str:
            self.__searchDirs = {searchDirs}
        elif type(searchDirs) == list:
            self.__searchDirs = set(searchDirs)
        else:
            raise TypeError('argument "searchDirs" must be either a string or list of strings')
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def add_search_dirs(self, searchDirs):
        if type(searchDirs) == str:
            if searchDirs in self.__searchDirs:
                print('"' + searchDirs + '" is already in the set of search directories.' )
            else:
                self.__searchDirs.add(searchDirs)

        elif type(searchDirs) == list:
            for sd in searchDirs:
                self.add_search_dirs(sd)
        else:
            raise self.__search_dir_error()
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def find_databases(self):
        dirs = []
        for sd in self.__searchDirs:
            dirs += [os.path.join(sd, d) for d in os.listdir(sd)]

            dirs = list(filter(lambda x: '.db' in x[-3:], dirs))
            dirs = [dir for dir in dirs if dir not in self.databaseDict.keys()]

        for dir in dirs:
            print('adding database found in "' + dir + '" to collection of databases')
            self.databaseDict[dir] = loaddb.FieldDatabase(dir)
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def database_directories(self):
        return list(self.databaseDict.keys())
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def output_time_to_entry_dict(self, dbDir):
        '''Returns a dictionary where the keys are output times and the values are the entry names for the database
        corresponding to "databaseDirectory"'''
        entries = self.databaseDict[dbDir].get_entry_names()
        outputTimeToEntry = OrderedDict()
        for ent in entries[:]:
            outputTime = round(float(ent), self.threshold)
            if outputTime not in outputTimeToEntry.keys():
                outputTimeToEntry[outputTime] = ent

        return outputTimeToEntry
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def find_common_output_times(self):

        dbs  = list(self.databaseDict.values())
        db = dbs[0]
        commonOutputTimes = set( self.output_time_to_entry_dict(db).keys() )

        for db in dbs[1:]:
            otherEntries = set( self.output_time_to_entry_dict(db).keys() )
            commonOutputTimes.intersection_update(otherEntries)

        return commonOutputTimes
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def databases_with_max_sim_time(self, maxSimTime):
        chosenDBs = []
        for dbDir in self.databaseDict.keys():
            entries = self.output_time_to_entry_dict(dbDir)
            maxVal = max([float(ent) for ent in entries])
            print('Maximum residence time of database located at ' + dbDir + ': ' + repr(maxVal))
            if maxVal >= maxSimTime:
                chosenDBs.append(dbDir)

        return chosenDBs


########################################################################################################################
########################################################################################################################

class DatabaseEnsemble:

    def __init__(self, searchDirs, maxOutputTime = '', pathToSaved = ''):
        self.__specPrefix       = 'rhoY_'
        self.__densityName      = 'density'
        self.__temperatureName  = 'T'
        self.__exclude          = ['log_frequency_distribution', 'speciesSourceTerm', 'p_core_density',
                                   'xface', 'cpd_delta', 'moisture_mass',
                                   'ash_porosity', 'ptempconv', 'cpd_l']
        self.__fieldMean        = dict()
        self.__fieldVariance    = dict()
        self.__YiNames          = set()
        self.__rhoYiNames       = set()
        self.__aliasDictionary  = dict() # keys are aliases, values are field names
        self.__dbNames          = []
        self.__timeInterval     = 1
        self.__fieldNames       = []
        self.__fieldNamesFromDB = []
        self.__pathToSaved      = pathToSaved
        self.__setupCalled      = False
        self.__saveDir          = ''

        self.manager = DatabaseManager(searchDirs)
        self.manager.find_databases()

        if maxOutputTime == '':
            self.maxOutputTime = max(self.manager.find_common_output_times())
        else:
            self.maxOutputTime = maxOutputTime

        self.__databaseDirs  = self.manager.databases_with_max_sim_time(self.maxOutputTime)
        dbDir = self.__databaseDirs[0]
        outTimes = list(self.manager.output_time_to_entry_dict(dbDir).keys())
        self.__outputTimes  = [s for s in outTimes if s <= self.maxOutputTime]

        for dbDir in self.__databaseDirs:
            _, dbName = os.path.split(dbDir)
            self.__dbNames.append(dbName)

        self.__numDatabases          = len(self.__dbNames)
        self.__numDatabasesFromSaved = 0

    # ..................................................................................................................

    def __find_common_field_names(self):
        dbs  = [self.manager.databaseDict[dbDir] for dbDir in self.__databaseDirs]
        db = dbs[0]
        firstEntry = db._FieldDatabase__subdirs[0]
        commonFieldNames = set(db.get_database_entry(firstEntry).get_field_names())
        for db in dbs[1:]:
            firstEntry = db._FieldDatabase__subdirs[0]
            otherFieldNames  = set(db.get_database_entry(firstEntry).get_field_names())
            commonFieldNames.intersection_update(otherFieldNames)

        commonFieldNames = list(commonFieldNames)
        # remove fields we want to exlude
        for name in commonFieldNames[:]:
            for exclude in self.__exclude:
                if exclude in name:
                    print('removing: ' + name )
                    commonFieldNames.remove(name)

        self.__fieldNamesFromDB = commonFieldNames
        self.__fieldNames       = self.__fieldNamesFromDB[:]
    # ..................................................................................................................

    def __find_species_names(self):
        # get the first database in the dict of (directory, database) pairs
        names = self.__fieldNamesFromDB[:]
        n = len(self.__specPrefix)
        # find field names corresponding to density-weighted species mass fractions
        for name in names:
            if name[:n] == self.__specPrefix:
                specName = 'Y_' + name[n:]
                print('name: '  + name)
                print('specName: ' + specName)
                self.__rhoYiNames.add(name)
                self.__YiNames.add(specName)

        self.__fieldNames += self.__YiNames

    # ..................................................................................................................

    def __get_field_from_db(self, db, dbDir, fieldName):
        assert type(fieldName) == str

        # routine for loading a single field with name 'fieldNames'
        if type(fieldName) == str:
            print('loading field "' + fieldName)
            f = []
            outputToEntry = self.manager.output_time_to_entry_dict(dbDir)
            for t in self.__outputTimes:
                entry = outputToEntry[t]
                if f == []:
                    f = db.get_field(entry, fieldName).data()
                else:
                    # this assumes data is 1-D for each output time. This will need to change if data is 2-D or 3-D
                    f = np.concatenate([f, db.get_field(entry, fieldName).data()], axis=1)

            f = np.squeeze(f)
            return f.copy()

    # ..................................................................................................................
    def setup(self):

        if not self.__pathToSaved == '':
            print('loading field mean and variance data from ' + self.__pathToSaved)
            self.__fieldMean, self.__fieldVariance, info = load_from_saved(self.__pathToSaved)
            self.__numDatabasesFromSaved = info['numDatabases']
            dbNamesFromSaved = info['databaseNames']

            # remove databases with names in the saved output
            for dbDir in self.__databaseDirs:
                __, dbName = os.path.split(dbDir)
                if dbName in dbNamesFromSaved:
                    print('removing database in the following directory from database list: ' + dbDir)
                    del self.__databaseDirs[dbDir]

        self.__find_common_field_names()
        self.__find_species_names()

        if not self.__pathToSaved == '':
            set1 = set(self.__fieldNames)
            set2 = set(self.__fieldMean.keys())
            set3 = set(self.__fieldVariance.keys())

            cond1 = set1 == set2
            cond2 = set2 == set3

            if not cond1:
                raise ValueError('field names from "fieldMean.p" do not match fields in list of databases')
            if not cond2:
                raise ValueError('field names from "fieldMean.p" do not match fields in "fieldVariance.p"')

        else:
            for name in self.__fieldNames:
                self.__fieldMean[name]     = 0
                self.__fieldVariance[name] = 0

        self.__setupCalled = True

    # ..................................................................................................................
    def set_save_dir(self, saveDir):
        self.__saveDir = saveDir

    # ..................................................................................................................
    def calculate_ensemble_statistics(self):
        '''
        calculates mean and variance of an ensemble of databases
        '''
        if not self.__setupCalled:
            self.setup()

        dbs = list(self.__databaseDirs)
        n = len(dbs)
        m = len(self.__specPrefix)

        i = 1
        for dbDir in self.__databaseDirs:
            db = loaddb.FieldDatabase(dbDir)
            print('----------------------------------------------------------------')
            print('loading database: [' + str(i) + '/' + str(n) + ']')
            print('----------------------------------------------------------------')

            density = self.__get_field_from_db(db, dbDir, self.__densityName)

            for name in self.__fieldNamesFromDB:
                newVal = self.__get_field_from_db(db, dbDir, name)

                if name in self.__rhoYiNames:
                    newVal /= density
                    valName = 'Y_' + name[m:]
                else:
                    valName = name

                if i == 1:
                    self.__fieldMean    [valName] = newVal
                    self.__fieldVariance[valName] = 0
                else:
                    mean = self.__fieldMean    [valName]
                    var  = self.__fieldVariance[valName]

                    delta_im1 = newVal - mean
                    mean      = mean - delta_im1/i
                    delta_i   = newVal - mean
                    var       = ( (i-1)*var + delta_i*delta_im1 )/i

            process = psutil.Process(os.getpid())
            print('memory usage before deleting (MB)')
            print(process.memory_info().rss/1e6)
            db = None
            gc.collect()
            print('memory usage after deleting (MB)')
            print(process.memory_info().rss / 1e6)

            i += 1

        if not self.__saveDir == '':
            print('saving to ' + self.__saveDir)
            self.save(self.__saveDir)
    # ..................................................................................................................
    def save(self, saveDir):
        info = dict()
        info['databaseNames'] = self.__dbNames
        info['numDataBases']  = self.__numDatabases
        pickle.dump(self.__fieldMean    , open(os.path.join(saveDir, 'fieldMean.p'    ), 'wb'), -1)
        pickle.dump(self.__fieldVariance, open(os.path.join(saveDir, 'fieldVariance.p'), 'wb'), -1)
        pickle.dump(info                , open(os.path.join(saveDir, 'info.p'         ), 'wb'), -1)


    # ..................................................................................................................
    @property
    def get_field_names(self):
        return self.__fieldNames
    # ..................................................................................................................

    def get_database_list(self):
        return [db for db in self.__databaseDirs.values()]

    # ..................................................................................................................
    @property
    def field_mean(self, name):
        return self.__fieldMean[name]

    # ..................................................................................................................
    @property
    def feild_variance(self, name):
        return self.__fieldVariance[name]