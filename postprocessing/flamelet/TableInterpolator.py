
import numpy as np
import cantera as ct
import matplotlib.pyplot as plt
from scipy.interpolate import RegularGridInterpolator
from scipy.interpolate import LinearNDInterpolator

class FlameletTableInterpolator:
    def __init__(self, table, mechPath):
        '''
        Provides an interface for obtaining an interpolated temperature and species mass fraction from arrays for
        mixture fraction, max dissipation rate, alpha, an enthalpy deficit.
        '''
        self.table = table.copy()
        self.mesh = np.meshgrid()

        # Presently, adiabatic flamelets are obtained by adding a convective source term to the energy conservation
        # with fixed values for the boundary temperatures. As a result, gradients in temperature (and heat loss) can
        # have incredibly large gradients near the mixture fraction boundaries when the convective heat transfer term is
        # large compared to the reactive temperature source term. As a result, interpolation can lead to eratic behavior
        # at the boundaries. For now, I (Josh McConnell) only care about fixing the behavior at small values of mixture
        # fraction. In the future, It may be best to vary heat loss by changing temperature at the boundaries.
        # linearMixFracUpperbound is the upper bound of mixture fraction for which interpolated values will be assumed
        # to be linear
        self.linearMixFracUpperbound = 0.01
        # ---------------------------------------------------------------------------------------
        self.gas = ct.Solution(mechPath)
        fields = ['T']
        for name in self.gas.species_names:
            if name in self.table.keys():
                fields.append(name)

        self.fields = fields
        self.interpolants = dict()

        params = self.table['params']
        gTable = self.table['gamma']

        self.mixFrac    = params['mixFrac'   ]
        self.chiMax     = params['chiMax'    ]
        self.alpha      = params['alpha'     ]
        self.gammaProxy = params['gammaProxy']

        self.gamma = self.table['gamma']

        gMin_table = np.min(gTable, axis=3)
        gMax_table = np.max(gTable, axis=3)

        self.grid = (self.mixFrac, self.chiMax, self.alpha, self.gammaProxy)
        self.gamma_interp = RegularGridInterpolator(self.grid, gTable)

        self.gammaMin_interp = RegularGridInterpolator(self.grid[:-1], gMin_table)
        self.gammaMax_interp = RegularGridInterpolator(self.grid[:-1], gMax_table)


    def make_gamma_monotonic(self):
        params = self.table['params']
        mixFrac = params['mixFrac'   ]
        chiMax  = params['chiMax'    ]
        alpha   = params['alpha'     ]
        gp      = params['gammaProxy']

        n = len(gp)
        m = (len(mixFrac)-2)*len(chiMax)*len(alpha)
        progress = 0
        p = max(round(m/100),1)

        for i_mixFrac in range(1,len(mixFrac)-1):
            for i_chiMax in range(len(chiMax)):
                for i_alpha in range(len(alpha)):

                    if progress % 1000 == 0:
                        print(progress/m, '%')

                    gamma = self.table['gamma'][i_mixFrac, i_chiMax, i_alpha, :]
                    T     = self.table['T'    ][i_mixFrac, i_chiMax, i_alpha, :]
                    j = np.diff(gamma) > 0

                    if np.min(j) == True:
                        continue

                    if not j[0]:
                        k = np.argmax(gamma[0] < gamma)
                        gamma[0:k+1] = np.linspace(gamma[0], gamma[k], k+1)
                        for field in self.fields:
                            f = self.table[field][i_mixFrac, i_chiMax, i_alpha, :]
                            self.table[field][i_mixFrac, i_chiMax, i_alpha, 0:k+1] = np.linspace(f[0], f[k], k+1)

                    if gamma[-1] < np.max(gamma):
                        k1 = np.where(gamma      > gamma[-1])[0][-1]
                        k2 = np.where(gamma[:k1] < gamma[k1])[0]
                        if len(k2)==0:
                            plt.plot(gamma)
                            print(i_mixFrac, i_chiMax, i_alpha)

                        k2 = k2[0]
                        gamma[k2::] = np.linspace(gamma[k2], gamma[k1], n-k2 )
                        for field in self.fields:
                            f = self.table[field][i_mixFrac, i_chiMax, i_alpha, :]
                            self.table[field][i_mixFrac, i_chiMax, i_alpha, k2::] = np.linspace(f[k2], f[k1], n-k2 )

                    if gamma[0] > gamma[-1]:
                        plt.plot(gamma)
                        raise ValueError('unable to make gamma monatonic increasing')

                    j = np.diff(gamma) > 0
                    if np.min(j) == True:
                        continue

                    monotonic = False
                    count     = 0
                    while not monotonic:
                        if count > n:
                            plt.plot(gamma)
                            print(i1,i2)
                            print(i_mixFrac, i_chiMax, i_alpha)
                            raise ValueError('to many iterations trying to make gamma monatonic')
                        i1 = j.argmin()
                        i2 = np.where((gamma[1::] > gamma[i1]) & (j))[0]
                        if len(i2) == 0:
                            print(i_mixFrac,i_chiMax,i_alpha)
                            plt.plot(gamma)
                        i2  = i2[0] + 1

                        gamma[i1:i2 + 1] = np.linspace(gamma[i1], gamma[i2], i2 - i1 + 1)
                        for field in self.fields:
                            f = self.table[field][i_mixFrac, i_chiMax, i_alpha, :]
                            self.table[field][i_mixFrac, i_chiMax, i_alpha, i1:i2 + 1] \
                                = np.linspace(f[i1], f[i2], i2 - i1 + 1)
                        count += 1
                        j = np.diff(gamma) > 0
                        monotonic = np.min(j)

                    self.table['gamma'][i_mixFrac, i_chiMax, i_alpha, :] = gamma

                    progress +=1


    def gamma_to_gamma_proxy(self, mixFrac, chiMax, alpha, gamma, tol=1e-3):
        zeroDTypes = (float, int)

        if type(mixFrac) in zeroDTypes:
            z = np.array(mixFrac)
            shape = ()
        else:
            z = mixFrac.flatten()
            shape = mixFrac.shape

        ones = np.ones(z.shape)

        if type(chiMax) in zeroDTypes:
            x = chiMax * ones
        else:
            x = chiMax.flatten()

        if type(alpha) in zeroDTypes:
            a = alpha * ones
        else:
            a = alpha.flatten()

        if type(gamma) in zeroDTypes:
            g = gamma * ones
        else:
            g = gamma.flatten()

        print('clipping chiMax to ', self.chiMax.min(), self.chiMax.max())
        x = x.clip(self.chiMax.min(), self.chiMax.max())

        gp_lb = np.ones(g.shape) * self.gammaProxy[0 ]
        gp_ub = np.ones(g.shape) * self.gammaProxy[-1]
        gp_m  = 0.5*(gp_lb + gp_ub)

        p_lb = (z, x, a, gp_lb)
        p_ub = (z, x, a, gp_ub)
        p_m  = (z, x, a, gp_m )

        gMin = self.gammaMin_interp(p_m[:-1])
        gMax = self.gammaMax_interp(p_m[:-1])

        i = g < gMin
        g[i] = gMin[i]

        i = g > gMax
        g[i] = gMax[i]

        convereged = False
        count = 0
        rMax = tol + 1
        r_m = rMax
        while not convereged:
            print('iteration: ', count)
            if count > 20:
                break
            count += 1
            r_lb = self.gamma_interp(p_lb) - g
            r_ub = self.gamma_interp(p_ub) - g
            r_m  = self.gamma_interp(p_m ) - g

            rMax = np.max(np.abs(r_m))
            convereged = rMax <= tol

            s_lb = np.sign(r_lb)
            s_ub = np.sign(r_ub)
            s_m  = np.sign(r_m )

            j = np.abs(r_lb) <= tol; gp_ub[j] = gp_m[j]  = gp_lb[j]
            j = np.abs(r_ub) <= tol; gp_lb[j] = gp_m[j]  = gp_ub[j]
            j = np.abs(r_m ) <= tol; gp_ub[j] = gp_lb[j] = gp_m[j]

            j = s_lb*s_m == -1
            gp_ub[j] = gp_m[j]
            gp_m[j] = 0.5*(gp_m[j] + gp_lb[j])

            j = s_ub * s_m == -1
            gp_lb[j] = gp_m[j]
            gp_m[j] = 0.5*(gp_m[j] + gp_ub[j])

            p_lb = (z, x, a, gp_lb)
            p_ub = (z, x, a, gp_ub)
            p_m  = (z, x, a, gp_m )

        print('max error:', rMax)
        print('mean error:', np.mean(np.abs(r_m)))
        return np.reshape(gp_m, shape)



    def gamma_to_gamma_proxy_exhaustive(self, mixFrac, chiMax, alpha, gamma, intervals = 101, tol = 1e-2):

        shape = mixFrac.shape
        z = mixFrac.flatten()
        x = chiMax.flatten()
        a = alpha.flatten()
        g = gamma.flatten()

        print('clipping chiMax to ', self.chiMax.min(), self.chiMax.max())
        x = x.clip(self.chiMax.min(), self.chiMax.max())

        gp = np.ones(g.shape) * self.gammaProxy[0]

        p = (z, x, a, gp)

        gMin = self.gammaMin_interp(p[:-1])
        gMax = self.gammaMax_interp(p[:-1])

        s = 0
        i = g < gMin
        g[i] = gMin[i]
        s += np.count_nonzero(i)

        i = g > gMax
        g[i] = gMax[i]
        s += np.count_nonzero(i)

        n = np.size(g)
        print(s,'out of', n, 'gamma clipped')

        # plt.figure('clipped')
        # plt.scatter(gamma,g, s=0.5, alpha=0.5)
        # plt.figure('residual')

        found = np.zeros(g.shape, dtype=bool)
        rOld  = np.ones(g.shape) * np.inf

        count = 0
        m = np.linspace(0,n-1, n)
        gpRefined = np.linspace(self.gammaProxy.min(), self.gammaProxy.max(), intervals)
        plt.ion()
        for gp_i in gpRefined:
            gpGuess = np.ones(g.shape) * gp_i
            p = (z, x, a, gpGuess)
            count += 1
            print('gp_i:',gp_i, 'progress: ', count, '/', intervals )
            rNew = np.abs( self.gamma_interp(p) - g )

            # plt.cla()
            # plt.scatter(m,rNew,s=0.5,alpha=0.5)
            # plt.pause(1e-3)

            found = (rNew <= tol) | found
            # only change values of gp if the residual has decreased and is greater than the tolerance
            i = (rNew < rOld)
            print(np.count_nonzero(i), 'gammaProxy values changed')
            gp[i] = gp_i
            s = np.count_nonzero(found)
            print('found: ',round(100*s/n,2), '%')
            rOld = rNew.copy()

        plt.ioff()
        return np.reshape(gp, shape)


    def interpolate(self, mixFrac, chiMax, alpha, gammaProxy, fieldName):
        assert fieldName in self.table.keys()
        assert  self.mixFrac[0] == 0

        zeroDTypes = (float, int)

        if type(mixFrac) in zeroDTypes:
            z = np.array(mixFrac)
            shape = ()
        else:
            z = mixFrac.flatten()
            shape = mixFrac.shape

        ones = np.ones(z.shape)

        if type(chiMax) in zeroDTypes:
            x = chiMax * ones
        else:
            x = chiMax.flatten()

        if type(alpha) in zeroDTypes:
            a = alpha * ones
        else:
            a = alpha.flatten()

        if type(gammaProxy) in zeroDTypes:
            gp = gammaProxy * ones
        else:
            gp = gammaProxy.flatten()

        # gTable = self.gamma

        zTable  = self.mixFrac
        xTable  = self.chiMax
        aTable  = self.alpha
        gpTable = self.gammaProxy

        print('clipping chiMax to ', xTable.min(), xTable.max())
        x = x.clip(xTable.min(), xTable.max())

        grid = (zTable, xTable, aTable, gpTable)
        f = self.table[fieldName]
        largeMixFracInterp = RegularGridInterpolator(grid, f)

        # get index corresponding to linear mixture fraction upper bound
        indecies = np.where(zTable >= self.linearMixFracUpperbound)[0]
        if len(indecies) is 0:
            raise ValueError('no tabulated values for mixture fraction >=', repr(self.linearMixFracUpperbound))
        iZ = indecies[0]
        smallMixFracIndecies = [0, iZ]
        # indecies such that zTable[linearIndecies] = [0, self.linearMixFracUpperbound]

        smallMixFracGrid = (zTable[smallMixFracIndecies],
                            xTable,
                            aTable,
                            gpTable)
        smallMixFracInterp = RegularGridInterpolator(smallMixFracGrid, f[smallMixFracIndecies])

        smallMixFracUpperBound = zTable[iZ]
        cond  = z > smallMixFracUpperBound

        fInterp = np.ones(z.shape) * np.nan
        fInterp[ cond] = largeMixFracInterp( (z[ cond], x[ cond], a[ cond], gp[ cond]) )
        fInterp[~cond] = smallMixFracInterp( (z[~cond], x[~cond], a[~cond], gp[~cond]) )


        return np.reshape( fInterp, shape )




# one option (slow):
# from scipy.interpolate import griddata
# params = table['params']
# x,y,z, _ = np.meshgrid(params['mixFrac'],params['chiMax'], params['alpha'], params['gammaProxy'], indexing='ij')
# points = (x.flatten(), y.flatten(), z.flatten(), table['gamma'].flatten())
# T_interp = griddata(points,table['T'].flatten(),(0.5,0.5,0.5,0.5))

# another option
# from scipy.interpolate import interpn
# grid = (params['mixFrac'],params['chiMax'],params['alpha'],params['gammaProxy'])
# T_interp=RegularGridInterpolator(grid, table['T'])
