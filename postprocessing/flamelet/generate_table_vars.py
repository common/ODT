import ExprDatabase as exdb
import mixtureFraction as mf
from mixtureFraction import scalar_dissipation_rate as sdr


def generate_table_vars(coalType, dbDir, canteraInput):
    '''
    :param coalType: coal type
    :param dbDir: directory in which the database (something.db) exists
    :param canteraInput: path to cantera input file
    :return:
    '''

    minTime = 0.
    maxTime = 0.2
    interval = 1

    oxidizerT = 350
    volatileT = 600
    charT     = 600
    # volatileT = 1246
    # charT     = 1128 # char oxidation-weighted mean particle temperature
    # charT = 1529  # char consumption-weighted (oxidation + gasification) mean particle temperature

    db = exdb.ExprDatabase(dbDir,interval,minTime,maxTime)
    db.set_cantera_input_file(canteraInput)
    db.set_state_variables()
    streams = mf.get_streams(canteraInput, coalType, volatileT, charT, oxidizerT)
    elements = ['C','H','O','N']
    normBehav = 'NORMALIZE_MIN_ZERO'

    streamMix = mf.StreamMixture(canteraInput)
    streamMix.set_mixture_fraction_normalization(normBehav)
    streamMix.add_streams(streams)
    streamMix.set_mixture_fractions_from_composition(db.mix(), elements)

    gamma = mf.calculate_gamma(db.mix(), streamMix.mix())

    dx = 0.4/2000
    mixFracVol  = streamMix.get_mixture_fraction('volatiles')
    mixFracChar = streamMix.get_mixture_fraction('char')
    mixFracTot = mixFracVol + mixFracChar
    alpha = mixFracVol/mixFracTot
    chi = sdr.calculate_scalar_dissipation_rate(mixFracTot, dx, streamMix.mix())
    chiMax = sdr.chi_to_chiMax(chi, mixFracTot)

    tableVars = dict()
    tableVars['alpha' ] = alpha
    tableVars['chi'   ] = chi
    tableVars['chiMax'] = chiMax
    tableVars['gamma' ] = gamma

    return tableVars