import cantera as ct
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from spitfire2.flamelet.kinetics import chemical_equilibrium
from spitfire2.flamelet.utilities import linear_profiles, uniform_grid
from spitfire2.flamelet.mechanism import ChemicalMechanismSpec
from spitfire2.flamelet.steady_solve import SteadyFlamelet, SolverSpec, FlameletParameters
import mixtureFraction as mf
import pickle
import time


def heat_transfer_coeff(chiMax, alpha, h):
    a = 1.
    b = 1e-6
    return b + h * (chiMax ** a / 1e5) * (1 - 1 / 0.73 * (1 - 220 / 450) * alpha)

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

class coalFlameletParams:
    def __init__(self,
                 chemicalMechFuel,
                 chemicalMechIdentifier,
                 chiMax,
                 alpha,
                 gammaSlices,
                 gammaUpperBound,
                 gammaLowerBound,
                 refTemperature,
                 volatileStream,
                 charStream,
                 oxidizerStream,
                 convectiveDeltaT,
                 pressure = ct.one_atm):
        assert(-1 < gammaLowerBound <= 0)
        assert(gammaUpperBound >= 0)
        self.fuel             = chemicalMechFuel
        self.identifier       = chemicalMechIdentifier
        self.alpha            = alpha
        self.chiMax           = chiMax
        self.gammaSlices      = gammaSlices
        self.gammaUpperBound  = gammaUpperBound
        self.gammaLowerBound  = gammaLowerBound
        self.refTemperature   = refTemperature
        self.volatileStream   = volatileStream
        self.charStream       = charStream
        self.oxidizerStream   = oxidizerStream
        self.convectiveDeltaT = convectiveDeltaT
        self.pressure         = pressure

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

class coalFlameletTableGenerator:
    def __init__(self, params, gridPoints, saveDir = '', equilibrateBCs = False):
        assert isinstance(params, coalFlameletParams)
        self.params           = params
        self.savePath          = saveDir
        self.__equilibrateBCs = equilibrateBCs

        self.__mechanism = ChemicalMechanismSpec(params.fuel, params.identifier)

        self.__interiorGridPoints = gridPoints
        self.__Z, self.__dZ = uniform_grid(gridPoints + 2)

        self.solverSpecs = SolverSpec(ds_init=1.e0)

        self.logInterval      = 20
        self.plot             = False
        self.__setupCalled    = False
        self.solver           = None
        self.solverIputParams = None
        self.table            = None
        self.tableExtruded    = False

        # htcMultiplier is used for generating a series of nonadiabatic flamelets
        # htc_new = htc_old * htcMultiplier
        self.htcMinMultiplier = 1.1
        self.htcMaxMultiplier = 2

        # enthalpy is a linear function of gamma/(1 + gamma), so the number of table slices in the positive and
        # negative gamma directions are set based on this relationship
        n  = self.params.gammaSlices - 1
        lb = params.gammaLowerBound / (1 + params.gammaLowerBound)
        ub = params.gammaUpperBound / (1 + params.gammaUpperBound)
        negGammaSlices = round( n * abs(lb / (ub - lb)) )
        negGammaSlices = max(negGammaSlices, 1)

        self.maxSolveRetries = 5

        self.__gammaPointsPos = n - negGammaSlices
        self.__gammaPointsNeg = negGammaSlices

        self.findConvCoeffbounds = 5
        self.gammaRefinementFactor = None # only used if solveAferInterp = True
        self.doFlameletSolveAferInterp = False   # note: setting this to True greatly increases solve time

        # coordinate for the 4th dimension of the table to be generated at adiabatic conditions
        self.__adiabaticTableDim = 0
        # heat transfer coeffs with abs value less than htcTol will be considered to be zero
        self.htcTol = 1e-11
    # ..................................................................................................................

    def set_fuel_BC(self, alpha):
        gas = self.__mechanism.gas
        P = self.params.pressure

        volStream  = self.params.volatileStream
        charStream = self.params.charStream

        # enthalpy contribution from volatiles portion of stream
        enthalpy_BC = alpha    * volStream .enthalpy_mass + \
                     (1-alpha) * charStream.enthalpy_mass

        Y_BC = alpha       * volStream .Y + \
               (1 - alpha) * charStream.Y

        gas.HPY = enthalpy_BC, P, Y_BC
        if self.__equilibrateBCs:
            print('equilibrating fuel BC')
            gas.equilibrate('HP')

        fuelBC = dict()
        fuelBC['T'] = gas.T
        fuelBC['Y'] = gas.Y[:-1]
        return fuelBC
    # ..................................................................................................................

    def __initialize_table(self):
        print('initializing table')
        chiMax = self.params.chiMax
        alpha  = self.params.alpha
        Z = self.__Z

        table = dict()
        table['params'] = dict()
        table['params']['mixFrac'   ] = Z
        table['params']['chiMax'    ] = chiMax
        table['params']['alpha'     ] = alpha
        table['params']['gammaProxy'] = 0 #corresponds to a gamma value of 0 (adiabatic)
        table['axes'] = dict()
        table['axes']['mixFrac'   ] = 0
        table['axes']['chiMax'    ] = 1
        table['axes']['alpha'     ] = 2
        table['axes']['gammaProxy'] = 3

        tblDims = (len(Z), len(chiMax), len(alpha), 1,)

        table['T'    ] = np.zeros(tblDims)
        table['gamma'] = np.zeros(tblDims)

        specNames = self.params.volatileStream.species_names
        for spec in specNames[:-1]:
            table[spec] = np.zeros(tblDims)

        self.table = table
    # ..................................................................................................................

    def setup(self):
        oxidizerBC = dict()
        oxidizerBC['T'] = self.params.oxidizerStream.T
        oxidizerBC['Y'] = self.params.oxidizerStream.Y[:-1]

        self.solver = SteadyFlamelet(self.__Z,
                                     self.__dZ,
                                     self.params.pressure,
                                     self.__mechanism.gas,
                                     self.__mechanism.pyjac_module)

        self.solverIputParams = FlameletParameters(chimax               = 0.,
                                                   convection_coeff     = 0.,
                                                   emissivity           = 0.,
                                                   particle_temperature = 0,
                                                   fuel                 = None,
                                                   oxy                  = oxidizerBC)
        self.__initialize_table()
        self.__setupCalled = True
    # ..................................................................................................................
    def indecies_from_list(self, indexList):
        l = [None] + indexList
        return [slice(ll) if ll is None else ll for ll in l]
    # ..................................................................................................................

    def update_table(self, state, chiMax, alpha, gammaProxy = 0., convectiveCoeff = None, gamma = None):
        iChiMax     = np.where(self.table['params']['chiMax']     == chiMax)[0][0]
        iAlpha      = np.where(self.table['params']['alpha' ]     == alpha )[0][0]
        iGammaProxy = np.where(self.table['params']['gammaProxy'] == gammaProxy)[0][0]
        ind = self.indecies_from_list([iChiMax, iAlpha, iGammaProxy])

        fuelBC = self.solverIputParams.fuel
        oxidBC = self.solverIputParams.oxy

        gas = self.__mechanism.gas
        nSpec = gas.n_species  # n-1 species, and temperature

        self.table['T'][ind] = np.hstack((oxidBC['T'], state[0::nSpec], fuelBC['T']))

        for j in range(nSpec - 1):
            spec = gas.species_name(j)
            k = j + 1
            self.table[spec][ind] = np.hstack((oxidBC['Y'][j], state[k::nSpec], fuelBC['Y'][j]))

        if gamma is not None:
            self.table['gamma'][ind] = gamma
            self.table['convective_coeff'][iChiMax, iAlpha, iGammaProxy] = convectiveCoeff
    # ..................................................................................................................


    def save_table(self, savePath = ''):
        print('saving table...')
        if savePath == '':
            savePath = self.savePath

        try:
            pickle.dump(self.table, open(savePath, 'wb'), -1)
        except:
            print('an error occured while trying to save the table to the following directory ', self.savePath)
            print('ensure that the "saveDir" method is set')

        print('done')

    # ..................................................................................................................
    def load_table(self,loadPath):
        try:
            self.table = pickle.load(open(loadPath, 'rb'))
        except:
            print('an error occured while trying to load the table')

    # ..................................................................................................................

    def state_from_table(self, chiMax, alpha, gammaProxy  = 0.):
        gas = self.__mechanism.gas
        nSpec = gas.n_species
        m = len(self.table['T']) - 2
        iChiMax     = np.where(self.table['params']['chiMax']     == chiMax)[0][0]
        iAlpha      = np.where(self.table['params']['alpha' ]     == alpha )[0][0]
        iGammaProxy = np.where(self.table['params']['gammaProxy'] == gammaProxy)[0][0]

        ind = self.indecies_from_list([iChiMax, iAlpha, iGammaProxy])
        state = np.zeros(m * nSpec)

        if np.sum(self.table['T'][ind]) == 0:
            print('not setting state from unpopulated portionm of table')
            return state
        else:
            state[0::nSpec] = self.table['T'][ind][1:-1].copy()

            for j in range(nSpec - 1):
                spec = gas.species_name(j)
                k = j + 1
                state[k::nSpec] = self.table[spec][ind][1:-1].copy()

            return state
    # ..................................................................................................................

    def state_to_solution_array(self, state, fuel, oxy):
        gas = self.__mechanism.gas
        nSpec = gas.n_species
        nZ = int(len(state) / nSpec + 2)
        gasArray = ct.SolutionArray(gas, nZ)

        T = np.hstack((oxy['T'], state[0::nSpec], fuel['T']))
        Y = np.zeros(np.shape(gasArray.Y))
        Yn = 1

        for j in range(nSpec - 1):
            k = j + 1
            Yi = np.hstack((oxy['Y'][j], state[k::nSpec], fuel['Y'][j]))
            Y[..., j] = Yi
            Yn -= Yi

        Y[..., -1] = Yn

        gasArray.TPY = T, gasArray.P, Y

        return gasArray

    # ..................................................................................................................

    def calculate_gamma(self, state, state_ad, fuel, oxy):
        g    = self.state_to_solution_array(state   , fuel, oxy)
        g_ad = self.state_to_solution_array(state_ad, fuel, oxy)

        gamma = mf.calculate_gamma(g, g_ad, T_ref = self.params.refTemperature)
        return gamma
    # ..................................................................................................................

    def extrude_table_for_gamma(self):
        '''
        extrudes the object's table for generation over a changing heat transfer coefficient, h. All tabulated values at
        val[...,0] are adiabatic values (h = 0).
        '''
        tblShape = list(np.shape(self.table['T']))
        assert len(tblShape) == 4
        assert tblShape[-1] == 1

        n    = self.params.gammaSlices
        nNeg = self.__gammaPointsNeg
        nPos = self.__gammaPointsPos
        i_ad = self.__adiabaticTableDim = nNeg

        gammaPoints = self.params.gammaSlices
        tblShape[-1] = gammaPoints
        self.table['params']['gammaProxy'] = np.linspace(-nNeg, nPos, n)
        # initialize an array for the convective heat transfer coefficient, h.
        # h is the same over mixture fraction for a given chiMax, alpha, and gamma.
        self.table['convective_coeff'] = np.empty(tblShape[1:]) * np.nan
        self.table['convective_coeff'][...,i_ad] = 0

        self.table['gamma'] = np.empty(tblShape) * np.nan
        self.table['gamma'][..., i_ad] = 0
        self.table['adiabaticDim'] = i_ad

        for name in self.table.keys():
            if name in ['axes', 'params', 'gamma', 'convective_coeff', 'adiabaticDim']:
                continue
            tmp = np.zeros(tblShape)
            tmp[..., i_ad] = self.table[name][...,0]
            self.table[name] = tmp
    # ..................................................................................................................

    def flamelet_solve(self, state, propertyName = None, newVal = None, oldVal = None, allowLinearGuess = False):
        # plt.ion()
        plt.figure('T')
        if propertyName is None:
            if newVal is not None or oldVal is not None:
                raise ValueError('cannot specify "newVal" and "oldVal: if "propertyName is not specified')

            return self.solver.pseudotransient_solve(state,
                                                     self.solverIputParams,
                                                     solver_spec=self.solverSpecs,
                                                     plot=self.plot,
                                                     log_rate=self.logInterval)

        elif oldVal is None:
            if newVal is None:
                raise ValueError('"newVal" must be specified if "propertyName is specified')

            setattr(self.solverIputParams, propertyName, newVal)
            return self.flamelet_solve(state)

        else:
            oldState = state.copy()
            converged = False
            tries = 0

            intermediateVal  = newVal
            intermediateVals = [newVal]

            while not converged:
                tries += 1
                if tries > self.maxSolveRetries:
                    if allowLinearGuess:
                        state, _, _, _, _, _ = linear_profiles(self.__Z,
                                                               self.solverIputParams.fuel,
                                                               self.solverIputParams.oxy)
                        self.flamelet_solve(state, propertyName, newVal, oldVal, False)

                    else:
                        msg = 'flamelet solve max retry count exceeded:' + repr(self.maxSolveRetries) + '\n' + \
                               'property: ' + propertyName + '\n' + \
                               'newVal: ' + repr(newVal) + '\n' + \
                               'oldVal: ' + repr(oldVal)
                        raise ValueError(msg)

                setattr(self.solverIputParams, propertyName, intermediateVal)
                try:
                    state = self.flamelet_solve(state)
                    converged = True
                except ValueError:
                    state = oldState.copy()
                    intermediateVal = 0.5 * (intermediateVal + oldVal)
                    intermediateVals.append(intermediateVal)
                    print('retrying flamelet solve at', propertyName, ' = ', intermediateVal)

            # if len(intermediateVals) > 1, then the flamelet solution at chiMax still needs to be obtained

            if len(intermediateVals) > 1:
                print('attempting to obtain flamelet solution at ',propertyName ,' = ', newVal)
                nSolves = len(intermediateVals) - 1
                j = 0

                # increase the residual tolerance here.
                oldMaxRes = self.solverSpecs.max_residual
                newMaxRes = oldMaxRes * 1e3
                print('setting solver max residual to:', newMaxRes)
                self.solverSpecs.max_residual = newMaxRes
                for intermediateVal in intermediateVals[-2::-1]:
                    j += 1
                    print(j, '/', nSolves, ':', intermediateVal)
                    setattr(self.solverIputParams, propertyName, intermediateVal)
                    state = self.flamelet_solve(state)
                print('changing solver  max residual back to:', oldMaxRes)
                self.solverSpecs.max_residual = oldMaxRes

            return state

    # ..................................................................................................................

    def do_solve_over_chiMax(self, alpha, guessFromTable = False):
        state = -1
        self.solverIputParams.fuel = self.set_fuel_BC(alpha)

        if guessFromTable is False:
            state, _, _, nQ, nZi, nDOF = linear_profiles(self.__Z,
                                                         self.solverIputParams.fuel,
                                                         self.solverIputParams.oxy)
            state = chemical_equilibrium(self.__mechanism.gas, self.params.pressure, state)

        if 0 not in self.params.chiMax and not guessFromTable:
            self.solverIputParams.chimax = 0
            state = self.flamelet_solve(state)

        # chiMaxDown = 1 # value of chiMax where we switch the continuation direction
        # if alpha < 0.01:
        #     chiMaxDown = 0.5
        #
        oldChiMax = 0.
        chiMaxVals = self.params.chiMax[:]
        # j = np.where(chiMaxVals >= chiMaxDown)[0]
        # if len(j) == 0:
        #     chiMaxVals = np.hstack([chiMaxVals,chiMaxDown,chiMaxVals[::-1]])
        #     chiMaxUp = np.inf
        # else:
        #     chiMaxVals = np.hstack([chiMaxVals[0:j[0]+1], chiMaxVals[j[0]-1::-1], chiMaxVals[j[0]+1::]])
        #     chiMaxUp = chiMaxVals[chiMaxVals >= chiMaxDown][1]

        for chiMax in chiMaxVals:
            # if guessFromTable:
            #     state = self.state_from_table(chiMax, alpha)
            # elif chiMax == chiMaxUp:
            #     chiMaxOld = chiMaxVals[chiMaxVals >= chiMaxDown][0]
            #     state = self.state_from_table(chiMaxOld, alpha)

            print('alpha:', alpha, 'max dissipation rate (Hz):', chiMax)

            state = self.flamelet_solve(state,
                                        propertyName = 'chimax',
                                        oldVal = oldChiMax,
                                        newVal = chiMax)

            if chiMax in self.params.chiMax:
                self.update_table(state=state, chiMax=chiMax, alpha=alpha)
            oldChiMax = chiMax

    # ..................................................................................................................

    def generate_adiabatic_table(self, guessFromTable = False):

        if not self.__setupCalled:
            self.setup()

        # set convection coefficient to zero which provides a set of adiabatic solutions
        self.solverIputParams.convection_coeff = 0.

        for alpha in self.params.alpha[:]:
            print('.............................................................................')
            self.do_solve_over_chiMax(alpha, guessFromTable)
            plt.cla()
    # ..................................................................................................................

    def find_convective_coeff_bound(self, state_ad, htc):
        gammaUpperBound = self.params.gammaUpperBound
        gammaLowerBound = self.params.gammaLowerBound
        maxTLimit = 4000 #K

        sgn  = np.sign(htc)
        m    = self.__interiorGridPoints
        nQ   = self.__mechanism.gas.n_species
        zero = np.zeros(m)
        boundingHtc = sgn * np.inf

        state  = state_ad.copy()
        T_ad = state[::nQ]
        T_new = T_ad.copy()

        self.solverIputParams.particle_temperature = T_ad + np.sign(htc)*self.params.convectiveDeltaT

        htcList   = []
        gammaList = []
        stateList = []
        print('attempting to find heat transfer coefficient (htc) for given bound on gamma')
        if self.plot:
            plt.figure('gamma')

            if htc > 0:
                plt.ylim(0, 2 * gammaUpperBound)
            else:
                plt.ylim(2 * gammaLowerBound, 0)
            # plt.ion()

            gamma_ub = np.ones(m) * gammaUpperBound
            gamma_lb = np.ones(m) * gammaLowerBound

            plt.plot(self.__Z[1:-1], gamma_ub, 'r:')
            plt.plot(self.__Z[1:-1], gamma_lb, 'r:')
            plt.grid()

        maxIter = 100
        iter    = 0
        maxRetries = 4
        retries    = 0
        doSolve = True

        gammaAvg = 0
        htcOld   = 0

        while doSolve:
            if iter > maxIter:
                print('maximum iterations exceeded: ', maxIter)
                break

            if retries > maxRetries:
                print('maximum number of retries for finding bounding heat transfer coeff exceeded: ', maxRetries)
                break
            T_old = T_new.copy()
            iter += 1
            self.solverIputParams.convection_coeff = abs(htc)
            print(iter, ' htc: ', htc)
            state = self.flamelet_solve(state,
                                        propertyName='convection_coeff',
                                        oldVal=abs(htcOld),
                                        newVal=abs(htc) )
            gamma = self.calculate_gamma(state, state_ad, self.solverIputParams.fuel, self.solverIputParams.oxy)[1:-1]
            gammaList.append(gamma)
            htcList.append(htc)
            stateList.append(state.copy())

            maxGamma = np.max(gamma)
            minGamma = np.min(gamma)
            if maxGamma > 2 * gammaUpperBound or minGamma < 2 * gammaLowerBound:
                boundingHtc = htc
                print('gamma too much out of range (',minGamma,',',maxGamma,
                      '). Trying to find a better heat transfer coefficient')
                htc   = 0.5 * htcList.pop()   + 0.5 * htcList[-1]
                state = 0.5 * stateList.pop() + 0.5 * stateList[-1]
                gamma = gammaList.pop()
                retries += 1
                continue

            # reset retry count
            retries = 0

            if self.plot:
                plt.figure('gamma')
                plt.plot(self.__Z[1:-1], gamma)
                plt.draw()
                plt.pause(1.e-3)

            if abs(np.mean(gamma)) < gammaAvg:
                print('mean absolute value of gamma has decreased. breaking from loop')
                gamma = gammaList.pop()
                state = stateList.pop()
                htc   = htcList.pop()
                break
            gammaAvg = abs(np.mean(gamma))

            if np.mean(gamma) > 1.3 * gammaUpperBound or maxGamma > 1.5 * gammaUpperBound:
                print('mean(gamma) has exceeded a predetermined upper limit (', 1.3 * gammaUpperBound, ')')
                print('or is in the range {',1.5 * gammaUpperBound,',', 2 * gammaUpperBound,'}',
                '(', maxGamma, ')')

                break

            if np.mean(gamma) < gammaLowerBound:
                print('mean(gamma) has exceeded a predetermined lower limit (', gammaLowerBound, ')')
                break

            T_new = state[::nQ]
            maxT = np.max(T_new)
            if maxT > maxTLimit:
                print('Max temperature exceeded limit.')
                break

            dT_max = np.max( np.abs(T_new - T_old) )
            print('max delta T: ', dT_max )
            multiplier = np.max([(self.htcMaxMultiplier -1) * (1 - dT_max/20) + 1,
                                self.htcMinMultiplier])
            htc = sgn * min(multiplier * sgn * htc, sgn * (0.5 * htc + 0.5 * boundingHtc))

        posCoverage = np.sum(gamma >= gammaUpperBound) / m * 100
        negCoverage = np.sum(gamma <= gammaLowerBound) / m * 100
        print(negCoverage, '% gammma below lower bound')
        print(posCoverage, '% gammma above upper bound')
        plt.figure('gamma')
        plt.close()
        plt.figure('T')
        plt.close()

        return np.array(htcList), np.array(gammaList).transpose(), np.array(stateList).transpose()
    # ..................................................................................................................

    def do_solve_over_convective_coeff(self, iChiMax, iAlpha, convectiveCoeff, state = None):
        chiMax = self.table['params']['chiMax'][iChiMax]
        alpha  = self.table['params']['alpha' ][iAlpha ]

        oldConvectiveCoeff = self.solverIputParams.convection_coeff

        if state is None:
            state = self.state_from_table(chiMax=chiMax, alpha=alpha).copy()

        self.solverIputParams.fuel = self.set_fuel_BC(alpha)
        self.solverIputParams.chimax = chiMax
        state = self.flamelet_solve(state,
                                    propertyName='convection_coeff',
                                    oldVal=oldConvectiveCoeff,
                                    newVal=abs(convectiveCoeff))
        return state
    # ..................................................................................................................

    def bootstrap_nonadiabatic_table_generation(self, chiMax, alpha):
        '''
        Performs a series of nonadiabatic flamelet solves where the convective coefficient is made progressively large
        until the mean value of the enthalpy deficit reaches its predetermined limit.
        :param chiMax: max dissipation rate
        :param alpha:  fraction of coal volatiles in fuel stream
        '''
        gammaUpperBound = self.params.gammaUpperBound
        gammaLowerBound = self.params.gammaLowerBound

        # plt.ion()
        print('alpha:', alpha, 'max dissipation rate (Hz):', chiMax)

        m = self.__interiorGridPoints
        self.solverIputParams.chimax = chiMax
        self.solverIputParams.fuel = self.set_fuel_BC(alpha)

        # get state under adiabatic condtions
        state_ad = self.state_from_table(chiMax=chiMax, alpha=alpha)
        nQ = self.__mechanism.gas.n_species
        T_ad = state_ad[::nQ]
        htc = heat_transfer_coeff(chiMax, alpha, 1e4)
        htcArrayPos, gammaArrayPos, stateArrayPos = self.find_convective_coeff_bound(state_ad, htc)

        htc = heat_transfer_coeff(chiMax, alpha, -1e4)
        htcArrayNeg, gammaArrayNeg, stateArrayNeg = self.find_convective_coeff_bound(state_ad, htc)

        htcArray = np.hstack([htcArrayNeg, 0, htcArrayPos])

        zero = np.zeros(np.shape(gammaArrayPos[:,0]))
        gammaArray = np.hstack([gammaArrayNeg, zero[:,None], gammaArrayPos])

        stateArray = np.hstack([stateArrayNeg, state_ad[:, None], stateArrayPos])

        i = htcArray.argsort()
        htcArray = htcArray[i]
        gammaArray = gammaArray[:,i]
        stateArray = stateArray[:,i]
        deltaT_mean = np.mean(stateArray[::nQ], axis=0) - np.mean(state_ad[::nQ])

        # define interpolants for the system state and the heat transfer coefficient
        interpKind = 'cubic'
        f_htc   = interp1d(deltaT_mean, htcArray, kind=interpKind)
        f_state = interp1d(deltaT_mean, stateArray, kind=interpKind)

        if self.doFlameletSolveAferInterp:
            refinementFactor = self.gammaRefinementFactor
        else:
            refinementFactor = 1

        nPos = self.__gammaPointsPos * refinementFactor
        nNeg = self.__gammaPointsNeg * refinementFactor


        minDeltaT = np.min(deltaT_mean)
        maxDeltaT = np.max(deltaT_mean)

        deltaT_meanInterp = np.hstack([np.linspace(maxDeltaT     , maxDeltaT/nNeg, nNeg),
                                       0,
                                       np.linspace(minDeltaT/nPos, minDeltaT     , nPos)])

        htcInterp = f_htc(deltaT_meanInterp)

        i_ad = self.__adiabaticTableDim
        if abs(htcInterp[i_ad * refinementFactor]) > self.htcTol:
            print('\ni_ad :', i_ad,
                  '\nhtc  :', htcInterp[i_ad * refinementFactor])
            raise ValueError('htc at i_ad is nonzero! See preceding message.')



        for j in range(len(htcInterp)):
            if j == 0:
                absHtcOld = None
            else:
                absHtcOld = abs(htcInterp[j-1])

            htc = htcInterp[j]
            if abs(htc) < self.htcTol:
                print('skipping htc:', htc)
                continue
            print(j, ' : htc: ', htc)

            state = f_state(deltaT_meanInterp[j])

            self.solverIputParams.particle_temperature = T_ad + np.sign(htc) * self.params.convectiveDeltaT

            # only re-solve given an interpolated state from an earlier solve in 'find_convective_coeff_bound'
            # if specified. This solve can take A LOT of extra time
            if self.doFlameletSolveAferInterp:
                allowLinearGuess = True
                state = self.flamelet_solve(state,
                                            propertyName='convection_coeff',
                                            oldVal=absHtcOld,
                                            newVal=abs(htc),
                                            allowLinearGuess = allowLinearGuess)



            gamma = self.calculate_gamma(state, state_ad, self.solverIputParams.fuel, self.solverIputParams.oxy)

            if self.plot:
                plt.figure('gamma')
                gamma_ub = np.ones(m) * gammaUpperBound
                gamma_lb = np.ones(m) * gammaLowerBound

                plt.plot(self.__Z[1:-1], gamma_ub, 'r:')
                plt.plot(self.__Z[1:-1], gamma_lb, 'r:')
                plt.ylim(2 * gammaLowerBound, 2 * gammaUpperBound)
                plt.plot(self.__Z, gamma)
                plt.draw()
                plt.pause(1.e-3)

            k = int(j/refinementFactor)
            if k == j/refinementFactor:
                print('updating table')
                gammaProxy = self.table['params']['gammaProxy'][k]
                self.update_table(state = state, chiMax = chiMax, alpha = alpha, gammaProxy = gammaProxy,
                                  convectiveCoeff = htc, gamma = gamma)
        if self.plot:
            plt.figure('gamma')
            plt.close()
            plt.figure('T')
            plt.close()

    # ..................................................................................................................

    def generate_noadiabatic_table(self):

        if self.doFlameletSolveAferInterp and self.gammaRefinementFactor is None:
            msg = "if 'doFlameletSolveAferInterp' is set to True then 'gammaRefinementFactor' must be set to a " \
                + "positive integer"
            raise ValueError(msg)
        if not self.doFlameletSolveAferInterp and self.gammaRefinementFactor is not None:
            msg = "'gammaRefinementFactor' will not be used as 'doFlameletSolveAferInterp' is set to false"
            raise RuntimeWarning(msg)

        if np.shape(self.table['T'])[-1] == 1:
            self.extrude_table_for_gamma()

        gammaUpperBound = self.params.gammaUpperBound
        gammaLowerBound = self.params.gammaLowerBound

        # if self.gammaLowerBounds is None or self.gammaUpperBounds is None:
        #     self.gammaLowerBounds,  self.gammaUpperBounds = self.generate_gamma_bounds(tableVars)
        if 'progress' not in self.table.keys():
            self.table['progress'] = dict()
            self.table['progress']['alpha'     ] = 0
            self.table['progress']['chiMax'    ] = 0
            self.table['progress']['gammaProxy'] = 0
        nQ = self.__mechanism.gas.n_species
        m  = self.__interiorGridPoints

        iAlphaBegin  = self.table['progress']['alpha' ]
        iChiMaxBegin = self.table['progress']['chiMax']

        for alpha in self.params.alpha[iAlphaBegin::]:
            t1 = time.time()
            bootStrapped = False
            iAlpha = np.where(self.table['params']['alpha'] == alpha)[0][0]
            self.solverIputParams.fuel = self.set_fuel_BC(alpha)
            self.table['progress']['alpha'] = iAlpha

            chiMaxVals = self.params.chiMax[iChiMaxBegin::]
            # if chiMaxVals.min() <= 0.4:
            #     index = np.where(chiMaxVals<=0.4)[0][-1]
            #     chiMaxVals = np.hstack([chiMaxVals[index::-1], chiMaxVals[index + 1::]])

            count = 0
            chiMaxOld = None
            allowLinearGuess = True
            for chiMax in chiMaxVals:
                # reset iChiMax to zero
                iChiMaxBegin = 0
                iChiMax = np.where(self.table['params']['chiMax'] == chiMax)[0][0]
                if chiMax == 0.:
                    print('skipping nonadiabatic table generation for chiMax = 0')
                    continue

                self.solverIputParams.chimax = chiMax
                self.table['progress']['chiMax'] = iChiMax

                state_ad = self.state_from_table(chiMax=chiMax, alpha=alpha)
                T_ad     = state_ad[::nQ]


                # generate proper bounds for the heat transfer coefficient every so often. This will ensure the
                # tabulated states don't drift too far from the set bounds of gamma
                if count % self.findConvCoeffbounds == 0:
                    bootStrapped = False
                count += 1

                if not bootStrapped:
                    self.bootstrap_nonadiabatic_table_generation(chiMax, alpha)
                    bootStrapped = True
                else:
                    iChiMaxOld = np.where(self.table['params']['chiMax'] == chiMaxOld)[0][0]
                    htcVals = self.table['convective_coeff'][iChiMaxOld, iAlpha] * ((chiMax/chiMaxOld)**1.05 + 1e-3)

                    negCoverage = 0
                    posCoverage = 0
                    absHtcOld = None
                    for j in range(len(htcVals)):
                        htc = htcVals[j]
                        if abs(htc) < self.htcTol:
                            print('skipping htc:', htc)
                            continue

                        print('alpha:', alpha, 'max dissipation rate (Hz):', chiMax, 'conv coeff:', htc)

                        gammaProxy = self.table['params']['gammaProxy'][j]

                        self.solverIputParams.particle_temperature = T_ad + np.sign(htc) * self.params.convectiveDeltaT

                        state = self.state_from_table(chiMaxOld, alpha, gammaProxy)

                        state = self.flamelet_solve(state,
                                                    propertyName='convection_coeff',
                                                    newVal=abs(htc),
                                                    oldVal=absHtcOld,
                                                    allowLinearGuess = allowLinearGuess)
                        absHtcOld = abs(htc)

                        gamma = self.calculate_gamma(state, state_ad, self.solverIputParams.fuel,
                                                     self.solverIputParams.oxy)
                        if self.plot:
                            plt.figure('gamma')
                            gamma_ub = np.ones(m) * gammaUpperBound
                            gamma_lb = np.ones(m) * gammaLowerBound

                            plt.plot(self.__Z[1:-1], gamma_ub, 'r:')
                            plt.plot(self.__Z[1:-1], gamma_lb, 'r:')
                            plt.ylim(2 * gammaLowerBound, 2 * gammaUpperBound)
                            plt.plot(self.__Z, gamma)
                            plt.draw()
                            plt.pause(1.e-3)

                        self.update_table(state=state, chiMax=chiMax, alpha=alpha, gammaProxy=gammaProxy,
                                          convectiveCoeff=htc, gamma=gamma)
                        self.table['progress']['gammaProxy'] = j

                        posCoverage = max(np.sum(gamma >= gammaUpperBound), posCoverage)
                        negCoverage = max(np.sum(gamma <= gammaLowerBound), negCoverage)


                    negCoverage *= 100/m
                    posCoverage *= 100/m
                    print(negCoverage, '% gammma below lower bound')
                    print(posCoverage, '% gammma above upper bound')

                    if self.plot:
                        plt.close('all')
                    print('solve over chiMax, heat transfer coefficient took', (time.time()-t1)/60, 'minutes')
                chiMaxOld = chiMax
            self.save_table()

    def extinction_to_ignition(self):
        '''
        finds non-adiabatic portion of a table with a state generated on an extinction branch and finds the state on
        the iginition branch
        '''

        i_ad = self.table['adiabaticDim']

        for alpha in self.params.alpha:
            iAlpha = np.where(self.table['params']['alpha'] == alpha)[0][0]
            self.solverIputParams.fuel = self.set_fuel_BC(alpha)
            self.table['progress']['alpha'] = iAlpha
            for chiMax in self.params.chiMax:
                print('alpha:', alpha, 'max dissipation rate (Hz):', chiMax)
                iChiMax = np.where(self.table['params']['chiMax'] == chiMax)[0][0]
                if chiMax == 0.:
                    print('skipping nonadiabatic table generation for chiMax = 0')
                    continue

                # adiabatic system temperature
                T_ad = self.table['T'][:, iChiMax, iAlpha, i_ad    ]
                # temperature of system where h < h_ad (T should be greater than T_ad)
                T    = self.table['T'][:, iChiMax, iAlpha, i_ad - 1]

                if np.mean(T) - np.mean(T_ad) < -20 :
                    self.bootstrap_nonadiabatic_table_generation(chiMax, alpha)
