import cantera
import numpy

from enum                    import Enum
from numpy                   import linalg
from collections             import OrderedDict
from .element_mass_fractions import element_mass_fractions


class NormalizationBehavior(Enum):
    NONE               = 0
    NORMALIZE          = 1
    NORMALIZE_MIN_ZERO = 2 # normalize after shifting minimum to zero
    CLIP               = 3

class StreamMixture:

    def __init__(self, canteraInput):
        assert(type(canteraInput == str))

        self.__canteraInput     = canteraInput
        self.__streams          = OrderedDict()
        self.__mixture_fractions = OrderedDict()
        self.__mix              = []
        self.__transportModel   = 'Mix'
        self.__mixFracBehavior  = NormalizationBehavior.NONE

    @property
    def canteraInput(self):
        return self.__canteraInput

    @property
    def streams(self):
        return self.__streams

    @streams.setter
    def streams(self, streams):
        assert (type(streams) in [dict, OrderedDict])
        self.__streams = streams

    def stream_labels(self):
        return list(self.__streams.keys())

    @property
    def mixtureFractions(self):
        return self.__mixture_fractions

    @mixtureFractions.setter
    def mixtureFractions(self, mixFracDict):
        if len(self.streams.keys()) == 0:
            raise RuntimeError('streams need to be defined before adding mixture fractions')

        if len( mixFracDict.keys() )  != len( self.streams.keys()) - 1:
            raise RuntimeError('number of mixture fractions must be one less than the number of streams')

        for label, mixFrac in mixFracDict.items():
            if label not in self.stream_labels():
                raise RuntimeError("label '", label, "' not a stream label")
            self.__mixture_fractions[label] = mixFrac

# ......................................................................................................................

    @property
    def mixture_fraction_normaliztion(self):
        return self.__mixFracBehavior

# ......................................................................................................................

    @mixture_fraction_normaliztion.setter
    def mixture_fraction_normaliztion(self, behavior):
        try:
            self.__mixFracBehavior = NormalizationBehavior[behavior]
        except Exception:
            msg = 'unrecognized mixture fraction normalization behavior. Options are:\n'
            for i in range(len(NormalizationBehavior)):
                msg += NormalizationBehavior(i).name + '\n'
            print(msg)

# ......................................................................................................................
    @property
    def mix(self):
        if self.__mix == []:
            self.set_mix_from_stream_compositions()

        return self.__mix

# ......................................................................................................................
    def get_mix_copy(self):
        g = cantera.Solution(self.__canteraInput)

        # calling get_mix() ensures that the mix is initialized
        mix = self.mix
        s = numpy.shape(mix.P)
        g = cantera.SolutionArray(g, s)

        g.TPY = mix.T, mix.P, mix.Y
        return g
# ......................................................................................................................
    def set_mixture_fraction_normalization(self, behavior):
        try:
            self.__mixFracBehavior = NormalizationBehavior[behavior]
        except Exception:
            msg = 'unrecognized mixture fraction normalization behavior. Options are:\n'
            for i in range(len(NormalizationBehavior)):
                msg += NormalizationBehavior(i).name + '\n'
            print(msg)

# ......................................................................................................................

    def set_mixture_fractions_from_composition(self, mix, elements):
        assert (type(elements) == list)
        '''calculate a set of n-1 mixture fractions from a dictionary of n streams.'''

        # local function definitions
        # __________________________________________________________________________________________
        def ensure_tolerance(value):
            tol = 1e-9
            if abs(value) < tol:
                raise Exception('determinant value (' + repr(value) + ') is less than tolerance ('
                                + repr(tol) + ')')

        # __________________________________________________________________________________________
        def print_min_and_max(label, x):
            print('mixture fraction for stream "' + label + '":')
            print('min: ' + repr(numpy.min(x.flat)))
            print('max: ' + repr(numpy.max(x.flat)) + '\n')

        # __________________________________________________________________________________________

        assert (isinstance(mix, cantera.SolutionArray) or isinstance(mix, cantera.Solution))

        streams = self.__streams
        if streams == OrderedDict:
            raise Exception('no stream compositions have ben added. Call "add_stream" or "add streams" to do so ')

        if len(streams) < 2:
            raise Exception('A minimum of 2 stream compositions are needed to calculate a set of mixture fractions')

        # ensure each stream has a 1-D composition
        for item in streams:
            if not numpy.ndim(numpy.squeeze(streams[item].Y)) == 1:
                raise Exception('Each  stream in dictionary "streams" must be a cantera Solution or Solution or '
                                'SolutionArray with a 1-dimensional composition')
        n = len(streams)
        m = len(elements)

        if m < n - 1:
            raise Exception('the number of elements needs to be greater than or equal to the number of streams')

        streamLabels = list(streams.keys())

        # get selected element mass fractions from last item in "streams" as a column vector
        Z_m = element_mass_fractions(streams[streamLabels[-1]], elements)
        g = numpy.shape(Z_m)
        if len(g) == 1:
            g += (1,)
        if g[0] < g[1]:
            g = g[::-1]

        Z_m = numpy.reshape(Z_m, g)

        # construct a matrix with columns Z_i - Z_m,
        Zprime = numpy.matrix(numpy.zeros([m, n - 1]))
        for i in range(n - 1):
            Z_i = element_mass_fractions(streams[streamLabels[i]], elements)
            Z_i = numpy.reshape(Z_i, g)
            Zprime[:, i] = Z_i - Z_m

        # calculate the matrix, C used to get mixture fractions from element mass fractions.
        if m == n - 1:
            dt = linalg.det(Zprime)
            ensure_tolerance(dt)
            C = linalg.inv(Zprime)
        else:
            B = Zprime.T * Zprime
            dt = linalg.det(B)
            ensure_tolerance(dt)
            C = linalg.inv(B) * Zprime.T

        # calculate element mass fractions from the mixture composition
        # NOTE: Elements change along the last axis of Z
        Z = element_mass_fractions(mix, elements)
        s = numpy.shape(Z)[:-1] + (n - 1,)
        mixFracs = OrderedDict()

        for i in range(n - 1):
            fSum = 0
            f = 0
            for j in range(m):
                f += (Z[..., j] - Z_m[j]) * C[i, j]
            print('stream '+ str(i))
            print_min_and_max(streamLabels[i], f)
            mixFracs[ streamLabels[i] ] = f
            fSum += f

        print_min_and_max(streamLabels[n-1], (1. - fSum))

        fSum = 0
        if self.__mixFracBehavior in [NormalizationBehavior.NORMALIZE, NormalizationBehavior.NORMALIZE_MIN_ZERO]:
            print('clipping and normalizing mixture fractions')
            for i in range(n - 1):
                f = mixFracs[streamLabels[i]]

                if self.__mixFracBehavior == NormalizationBehavior.NORMALIZE_MIN_ZERO:
                    f -= numpy.max([f.min(), 0])

                mixFracs[streamLabels[i]] = numpy.clip(f, 0, 1)
                fSum += f

            # clip values of fSum to have a minimum value of 1. This ensures that normalization only occurs in
            # instances where the sum of mixture fractions is greater than 1.
            fSum = fSum.clip(1, None)
            print('Normalized mixture fractions:')
            for i in range(n - 1):
                mixFracs[ streamLabels[i] ] /= fSum
                print_min_and_max(streamLabels[i], mixFracs[ streamLabels[i] ])

        elif self.__mixFracBehavior is NormalizationBehavior.CLIP:
            print('Clipped mixture fractions:')
            for i in range(n - 1):
                f = mixFracs[streamLabels[i]].clip(0,1)
                mixFracs[streamLabels[i]] = f
                fSum += f
                print_min_and_max(streamLabels[i], mixFracs[streamLabels[i]])
            print_min_and_max(streamLabels[n-1],1-fSum)


        self.__mixture_fractions = mixFracs

# ......................................................................................................................
    def set_mix_from_stream_compositions(self):

        streams = self.streams
        labels  = self.stream_labels()
        mixFracs = self.mixtureFractions
        s = numpy.shape(mixFracs[ labels[0] ])
        g = cantera.Solution(self.canteraInput)
        mix  = cantera.SolutionArray(g, s)

        sY = s + (mix.n_species,)
        Y = numpy.zeros(sY)

        H    = 0
        P    = 0
        fSum = 0
        n = len(labels)
        # add contributions from first n-1 streams
        for i in range(n - 1):
            label = labels[i]
            fSum += mixFracs[label]
            H += mixFracs[label] * streams[label].enthalpy_mass
            P += mixFracs[label] * streams[label].P
            Ys = streams[label].mass_fraction_dict()

            for name in Ys.keys():
                j = mix.species_index(name)
                Y[...,j] += (mixFracs[label] * Ys[name])

        # add contributions from nth stream
        label = labels[n-1]
        H += (1 - fSum) * streams[label].enthalpy_mass
        P += (1 - fSum) * streams[label].P
        Ys = streams[label].mass_fraction_dict()
        for name in Ys.keys():
            j = mix.species_index(name)
            Y[..., j] += (1 - fSum) * Ys[name]

        mix.HPY = H, P, Y
        self.__mix = mix