from cantera import Solution, SolutionArray

def stoichiometric_oxidizer_to_fuel_mass_ratio(fuelStream, oxStream):
    '''
    calculates the stoichiometric mass of oxidizer per mass of fuel assuming O2 is the only oxidizing species
    '''
    assert (
            ( isinstance(fuelStream, Solution     ) and isinstance(oxStream, Solution     ) )
            or
            ( isinstance(fuelStream, SolutionArray) and isinstance(oxStream, SolutionArray) )
            )

    xC = fuelStream.elemental_mass_fraction('C')
    xH = fuelStream.elemental_mass_fraction('H')
    xO = fuelStream.elemental_mass_fraction('O')

    i  = oxStream.species_index('O2')

    # availableO2 is the mass fraction of O2 in the oxidizer stream
    availableO2 = oxStream.Y[..., i]
    assert( availableO2 > 0)

    # requiredO2 is the mass of O2 per mass of fuel required for stoichiometric conditions
    requiredO2  = (
                             xC/fuelStream.atomic_weight('C')
                    + 0.25 * xH/fuelStream.atomic_weight('H')
                    - 0.5  * xO/fuelStream.atomic_weight('O')
                   ) * oxStream.molecular_weights[i]

    assert(requiredO2 > 0)

    return requiredO2/availableO2


def stoichiometric_mixture_fraction(fuelStream, oxStream):
    '''
    calculates the stoichiometric mixture fraction given fuel and oxidizer streams.
    Assumes O2 is the only oxidizing species
    '''

    r = stoichiometric_oxidizer_to_fuel_mass_ratio(fuelStream, oxStream)

    return 1/(1 + r)

