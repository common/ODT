from cantera import SolutionArray, Solution

def calculate_gamma(observedMix, reconstructedMix, T_ref = 298 ):
    '''calculetes an enthalpy deficit between an observed state adiabatic state reconstructed from (n) stream
    compositions and (n-1) mixture fractions. The enthalpy deficit is normalized by sensible energy (i.e. the energy
    required to take the state from T_ref to T)'''

    assert ( ( isinstance(observedMix, Solution     ) and isinstance(reconstructedMix, Solution     ) )
            or
             ( isinstance(observedMix, SolutionArray) and isinstance(reconstructedMix, SolutionArray) )
            )

    P_obs = observedMix.P
    h_obs = observedMix.enthalpy_mass
    Y_rec = reconstructedMix.Y

    reconstructedMix.TPY = reconstructedMix.T, P_obs, Y_rec
    h_ad  = reconstructedMix.enthalpy_mass
    T_rec = reconstructedMix.T

    reconstructedMix.TPY = observedMix.T, P_obs, Y_rec
    h_T = reconstructedMix.enthalpy_mass

    reconstructedMix.TPY = T_ref, P_obs, Y_rec
    h_ref = reconstructedMix.enthalpy_mass

    gamma = (h_ad - h_obs)/(h_T - h_ref)

    # return reconstructedMix to its original enthalpy
    reconstructedMix.TPY = T_rec, P_obs, Y_rec

    return gamma
