import cantera as ct
import numpy as np
import warnings
from collections import OrderedDict
from .stoichiometric_values import stoichiometric_mixture_fraction, stoichiometric_oxidizer_to_fuel_mass_ratio

def burke_schumann_mixture(canteraInput, mixFracs, streams, systemPressure=ct.one_atm):
    '''
    :param mixFracs: a dict of n {mixture fraction: stream label} pairs. Excludes oxidizer stream
    :param streams: a dict of n+1 {cantera.Solution: stream label} pairs. Includes oxidizer stream.
                    The oxide stream entry must have key = 'oxidizer'
    :param systemPressure: pressure at which calculation is done. The default value is 1 atm.
    :return: a cantera.SolutionArray with a "mixed is burnt" composition and temperature.

    Each species mass fraction, Yi,  is determined by the following formula:

    Yi = (Yi_st - Yi_ox  ) * (z_tot    )/(z_st    ) + Yi_ox  ,     if z >= z_st
    Yi = (Yi_st - Yi_fuel) * (1 - z_tot)/(1 - z_st) + Yi_fuel,     if z <= z_st

    where:
    Yi_ox is the mass fraction of species i in the oxidizer stream
    Yi_fuel  = sum( Yi_fuel_j * z_j )
    z_tot    = sum( z_j )
    z_j  is the mixture fraction of fuel j
    z_st is the total stoichiometric mixture fraction of the combined fuel

    The mixture temperature is calculated assuming adiabatic, isobaric conditions

    Example of how to use this function to generate a Burke-Schumann flamelet:
    # -------------------------------------------------------------------------

    import numpy as np
    import cantera as ct
    import matplotlib.pyplot as plt

    npts = 501
    z = np.linspace(0,1,npts)
    a = np.linspace(1,0,npts) # mass fraction of fuel 2 in fuel mixture
    mixFracs = dict()
    mixFracs['fuel1'] = np.outer(z  ,a)
    mixFracs['fuel2'] = np.outer(z,1-a)

    canteraInput = 'gri30.xml'
    oxid  = ct.Solution(canteraInput); oxid.TPX  = 300, ct.one_atm, {'O2': 0.21, 'N2': 0.79}
    fuel1 = ct.Solution(canteraInput); fuel1.TPX = 300, ct.one_atm, {'CO': 1}
    fuel2 = ct.Solution(canteraInput); fuel2.TPX = 300, ct.one_atm, {'H2': 1}

    streams = dict()
    streams['oxidizer'] = oxid
    streams['fuel1'   ] = fuel1
    streams['fuel2'   ] = fuel2

    bsMix = burke_schumann_mixture(canteraInput, mixFracs, streams)

    Z,A=np.meshgrid(z,a,indexing='ij')
    plt.contourf(Z,A, bsMix.T,100)
    plt.xlabel('total mixture fraction')
    plt.ylabel('$Y_{fuel2}$ in fuel stream')
    plt.colorbar()
    plt.show()
    '''
    assert( type(streams) in [dict, OrderedDict] )
    for key in streams.keys():
        assert( isinstance(streams[key], ct.Solution) )

    n = len(mixFracs)
    assert( n + 1 == len(streams) )

    fuelNames   = set( mixFracs.keys() )
    streamNames = set( streams.keys()  )

    oxidName = list( streamNames.difference( fuelNames ) )[0]
    assert ( streams[oxidName].mass_fraction_dict()['O2'] > 0 )

    print('stream with label "' + oxidName + '" is assumed to be the oxide stream')

    stoichOxFuelRatio = dict()
    totStoichOxFuelRatio = 0
    z_tot                = 0
    h_tot                = 0 # mixture enthalpy
    for name in fuelNames:
        stoichOxFuelRatio[name] = stoichiometric_oxidizer_to_fuel_mass_ratio(streams[name], streams[oxidName])
        z = mixFracs[name]
        zMax = np.max(z)
        zMin = np.min(z)
        if zMax>1 or zMin<0:
            msg = "range of mixture fraction with label '" + name + "' is not bounded by [0,1]. clipping to [0,1]"
            z = z.clip(0,1)
            raise RuntimeWarning(msg)
        z_tot += z
        h_tot += z * streams[name].enthalpy_mass

    h_tot += (1 - z_tot) * streams[oxidName].enthalpy_mass

    specNames        = streams[oxidName].species_names
    nSpec            = len(specNames)
    YFuel            = np.zeros(np.shape(z_tot) + (nSpec,))
    Y                = np.zeros(np.shape(z_tot) + (nSpec,))
    oxidStream       = streams[oxidName]

    for name in fuelNames:
        # alpha is  the fraction of fuel with key 'name' in fuel stream. Doing the calculation like this ensures
        # any instances of 0/0 are replaced with 1/n. As a result, stoichMixFracTot = 1 when mixFracTot = 0;
        fuelStream        = streams[name]
        z                 = mixFracs[name].clip(0,1)
        zTotalIsZero      = (z_tot == 0)
        alpha             = (z + zTotalIsZero)/( z_tot + n * zTotalIsZero )
        totStoichOxFuelRatio += alpha * stoichOxFuelRatio[name]
        stoichBurntComp   = stoichiometric_burnt_composition(fuelStream, oxidStream)

        for specName in specNames:
            i = oxidStream.species_index(specName)
            # set fuel composition
            YFuel[..., i] += alpha * fuelStream.Y[i]
            # set Y to composition of a burnt stoichiometric mixture
            # if the species is a product, add the contribution from the stream. Otherwise, do nothing
            if specName in stoichBurntComp.keys():
                Y[..., i] += alpha * stoichBurntComp[specName]



    z_st = 1/(1 + totStoichOxFuelRatio)
    isRich = z_tot > z_st
    isLean = z_tot < z_st

    #  the variable hat has the following definition:
    # hat = (z_tot    )/(z_st    ) if z >= z_st
    #     = (1 - z_tot)/(1 - z_st) if z <= z_st

    hat = (1 - z_tot) / (1 - z_st)
    #lean values
    hat[isLean] = z_tot[isLean]/z_st[isLean]


    for specName in specNames:
        i = streams[oxidName].species_index(specName)
        YFi  = YFuel[...,i]
        YOi  = streams[oxidName].Y[i]
        Yi   = Y[...,i]

        Yi[isLean] = (Yi[isLean] - YOi)*hat[isLean] + YOi
        Yi[isRich] = (Yi[isRich] - YFi[isRich])*hat[isRich] + YFi[isRich]

    mix = ct.Solution(canteraInput)
    mix = ct.SolutionArray(mix, z_tot.shape)


    mix.HPY = h_tot, systemPressure, Y
    return mix
# ----------------------------------------------------------------------------------------------------------------------


def stoichiometric_burnt_composition(fuelStream, oxidStream):

    supportedElements = {'C', 'H', 'O', 'N', 'S', 'Ar'}
    elemToSpec = dict()
    elemToSpec['C' ] = 'CO2'
    elemToSpec['S' ] = 'SO2'
    elemToSpec['H' ] = 'H2O'
    elemToSpec['N' ] = 'N2'
    elemToSpec['Ar'] = 'Ar'

    elements = set(fuelStream.element_names)
    assert( 'O' in elements )

    if not supportedElements.issuperset( elements ):
        unsupportedElements = elements.difference(supportedElements)
        raise RuntimeError('elements ' + str(unsupportedElements) + ' are not supported')

    stoichMixFrac = stoichiometric_mixture_fraction(fuelStream, oxidStream)

    Y = dict()

    for elem in elements:
        if elem == 'O':
            continue

        spec = elemToSpec[elem]
        if spec not in fuelStream.species_names:
            msg = spec + ' not in cantera mechanism'
            warnings.warn(msg)
            continue

        i    = fuelStream.species_index(spec)
        nu   = fuelStream.n_atoms(spec, elem)
        ms   = fuelStream.molecular_weights[i]
        me   = fuelStream.atomic_weight(elem)
        z    =   stoichMixFrac       * fuelStream.elemental_mass_fraction(elem) \
               + (1 - stoichMixFrac) * oxidStream.elemental_mass_fraction(elem)

        Y[spec] = (z * ms)/(me * nu)

    return Y