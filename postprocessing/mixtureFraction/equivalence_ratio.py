import cantera as ct

def equivalence_ratio(mix):
    raise RuntimeError("don't use this function. it is incorrect")
    assert( isinstance(mix, ct.Solution) or isinstance(mix, ct.SolutionArray))
    assert( 'O' in mix.element_names )

    supportedElements = {'O','C','H','S','N','Ar'}
    if not supportedElements.issuperset(mix.element_names):
        msg = "found an unsupported element: \n"
        msg = msg + "suppoeted elements: \n"
        msg = msg + str(supportedElements)+ "\n\n"
        msg = msg + "mix elements:\n"
        msg = msg + str(mix.element_names)
        raise RuntimeError(msg)

    yo_st = 0.
    if 'C' in mix.element_names:
        yo_st += mix.elemental_mole_fraction('C') * 2

    if 'H' in mix.element_names:
        yo_st += mix.elemental_mole_fraction('H') * 0.5

    if 'O' in mix.element_names:
        yo_st -= mix.elemental_mole_fraction('O')

    yo = mix.elemental_mole_fraction('O')

    return (1 - yo) / yo * yo_st / (1 - yo_st)