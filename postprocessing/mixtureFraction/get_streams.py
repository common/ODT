import cantera
import warnings
from collections import OrderedDict

allowTruncation = True
P = cantera.one_atm
def get_volatile_stream(canteraInput, coalType, volatileT=600, tarIsC2H2 = False):
    volatileCompositions = dict()

    # # Eastern Bituminous
    # # these are calculated from CPD source terms from a simulation
    # EasternBituminousVolatiles = dict()
    # EasternBituminousVolatiles['CO2'] = 0.0098 + 0.0137 + 0.0139
    # EasternBituminousVolatiles['H2O'] = 0.0528 + 0.0303
    # EasternBituminousVolatiles['CO' ] = 0.0240 + 0.0148 + 0.0129
    # EasternBituminousVolatiles['HCN'] = 0.0736
    # EasternBituminousVolatiles['NH3'] = 4.8980e-04
    # EasternBituminousVolatiles['CH4'] = 0.1126
    # EasternBituminousVolatiles['H2' ] = 0.0285
    #    if tarIsC2H2:
    #     IllinoisNo6Volatiles['C2H2'] = 0.1559
    # volatileCompositions.update({'Eastern Bituminous': EasternBituminousVolatiles})

    # Illinois Bituminous
    # these are from CPDData.cpp in th ODT source code
    IllinoisBituminousVolatiles = dict()
    IllinoisBituminousVolatiles['CO2'] = 0.022 + 0.022 + 0.030
    IllinoisBituminousVolatiles['H2O'] = 0.045 + 0.00
    IllinoisBituminousVolatiles['CO' ] = 0.060 + 0.063 + 0.00
    IllinoisBituminousVolatiles['HCN'] = 0.010 + 0.016
    IllinoisBituminousVolatiles['NH3'] = 0.00
    IllinoisBituminousVolatiles['CH4'] = 0.011 + 0.011 + 0.022
    IllinoisBituminousVolatiles['H2' ] = 0.016
    if tarIsC2H2:
        IllinoisBituminousVolatiles['C2H2'] = 0.081

    volatileCompositions.update({'Illinois Bituminous': IllinoisBituminousVolatiles})
    # Illinois No. 6
    # these are from CPDData.cpp in th ODT source code
    IllinoisNo6Volatiles = dict()
    IllinoisNo6Volatiles['CO2'] = 0.0035 + 0.0086 + 0.0090
    IllinoisNo6Volatiles['H2O'] = 0.0165 + 0.0092
    IllinoisNo6Volatiles['CO' ] = 0.0516 + 0.0303 + 0.0168
    IllinoisNo6Volatiles['HCN'] = 0.0164 + 0.0155
    IllinoisNo6Volatiles['NH3'] = 0.00
    IllinoisNo6Volatiles['CH4'] = 0.0185 + 0.0144 + 0.0161
    IllinoisNo6Volatiles['H2' ] = 0.0126
    if tarIsC2H2:
        IllinoisNo6Volatiles['C2H2'] = 0.1559

    volatileCompositions.update({'Illinois No 6': IllinoisNo6Volatiles})

    # -----
    # -----

    if coalType not in volatileCompositions.keys():
        msg = '"' + coalType + '" not in the set of for coal volatiles that are defined Your choices are:'
        for name in volatileCompositions.keys():
            msg += '\n' + name
        raise ValueError(msg)

    volatileStream = cantera.Solution(canteraInput)
    composition = volatileCompositions[coalType].copy()
    speciesNotInMech = [species for species in composition if species not in volatileStream.species_names]

    if len(speciesNotInMech) > 0:
        msg = 'the following species were not found in the cantera input file:\n'
        for species in speciesNotInMech:
            msg += species + '\n'
        if allowTruncation:
            warnings.warn(msg)
            for spec in speciesNotInMech:
                del composition[spec]
        else:
            raise ValueError(msg)

    volatileStream.TPY = volatileT, P, composition

    return volatileStream
