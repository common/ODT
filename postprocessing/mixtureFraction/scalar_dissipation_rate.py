from cantera import SolutionArray, Solution
from scipy.special import erfinv
import numpy as np
import util.database_field_operations as op

def calculate_scalar_dissipation_rate(mixFrac, mix, db):
    '''
    calculates the scalar dissipation rate with the following assumptions:
    - data has a single spatial dimension and is the first axis of the "mixFrac array
    - Lewis number for all species is equal to 1
    '''

    assert ( isinstance(mix, Solution ) or isinstance(mix, SolutionArray  ) )

    # pad mixture fraction
    z = op.interpolate_to_faceType(mixFrac, padBoundaries=True)

    gradDotGradMixFrac = op.d_dx(db, z) ** 2

    # divGradMixFrac = np.zeros( np.shape(mixFrac) )
    # divGradMixFrac[1:-1] = (mixFrac[2:] - 2*mixFrac[1:-1] + mixFrac[0:-2])/dx**2
    # set boundary cell values to their neighboring values
    # divGradMixFrac[0]  = divGradMixFrac[1]
    # divGradMixFrac[-1] = divGradMixFrac[-2]

    Le = 1.
    return 2./Le \
           * mix.thermal_conductivity/( mix.density_mass * mix.cp_mass ) \
           * gradDotGradMixFrac
# ......................................................................................................................

def a_neat_function(Z):
    return np.exp(-2 * erfinv(2 * Z - 1) ** 2)
# ......................................................................................................................

def chiMax_to_chi(chiMax, mixtureFraction):
    return chiMax * a_neat_function(mixtureFraction)
# ......................................................................................................................

def chi_to_chiMax(chi, mixtureFraction):
    return chi / a_neat_function(mixtureFraction)
# ......................................................................................................................
