import numpy
from cantera import Solution, SolutionArray


def element_mass_fractions(stream, elements):
    '''
    returns a numpy.ndarray "Z" with element mass fractions along the last axis of Z.
    '''
    assert ( isinstance(stream, Solution) or isinstance(stream, SolutionArray) )
    assert (type(elements) == list)

    m = len(elements)
    Ztmp = stream.elemental_mass_fraction(elements[0])
    s = numpy.shape(Ztmp) + (m,)

    Z = numpy.ndarray(s)
    Z[..., 0] = Ztmp

    for i in range(1, m):
        Z[..., i] = stream.elemental_mass_fraction(elements[i])

    return Z
