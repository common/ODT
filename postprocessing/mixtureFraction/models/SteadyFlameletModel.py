from .ModelBase import ModelBase
import flamelet.TableInterpolator as tableInterpolator
import numpy as np
import cantera as ct

class SteadyFlameletModel(ModelBase):
    def __init__(self, canteraInput, streams, mixtureFractions, table):
        ModelBase.__init__(self, canteraInput, streams, mixtureFractions)

        if len(streams) > 3:
            raise RuntimeError('At most, 3 streams are suppored at this time')

        self.__table = table
        self.__interpolator = tableInterpolator.FlameletTableInterpolator(table,canteraInput)


    @property
    def interpolator(self):
        return self.__interpolator

    def values(self, chiMaxVal, alphaVal, gammaVal, fieldNames):
        '''
        :param chiMaxVal:
        :param alphaVal:
        :param gammaVal:
        :return: A dictionary of {name: value} pairs
        '''

        if type(fieldNames) == str:
            names = [fieldNames]
        elif type(fieldNames) == list:
            names  = fieldNames
        else:
            raise TypeError('argument "fieldNames" must be a string or list of strings')


        zTotal = self.totalMixtureFraction
        # get field from flamelet lookup table
        ones = np.ones(zTotal.shape)
        x = chiMaxVal * ones
        a = alphaVal  * ones
        g = gammaVal  * ones
        gp = self.interpolator.gamma_to_gamma_proxy(zTotal, x, a, g)

        fieldVals = dict()

        for name in names:
            fieldVals[name] = self.interpolator.interpolate(zTotal, x, a, gp, name)

        return fieldVals