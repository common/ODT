from collections import OrderedDict

class ModelBase:
    def __init__(self, canteraInput, streams, mixtureFractions):
        oxidName = 'oxidizer'
        validTypes = [dict, OrderedDict]
        assert( type(streams) in validTypes )
        assert( type(mixtureFractions) in validTypes )
        assert( len(streams) == len(mixtureFractions) + 1 )
        assert( 'oxidizer' in streams.keys() )

        self.__canteraInput     = canteraInput
        self.__oxidizerStream   = streams['oxidizer']
        self.__mixtureFractions = mixtureFractions
        self.__fuelStreams      = OrderedDict()
        self.__zTotal = 0

        for label in streams.keys():
            if label is oxidName:
                continue
            self.__fuelStreams[label] = streams[label]

        for z in mixtureFractions.values():
            self.__zTotal += z

    @property
    def canteraInput(self):
        return self.__canteraInput

    @property
    def oxidizerStream(self):
        return self.__oxidizerStream

    @property
    def fuelStreams(self):
        return self.__fuelStreams

    @property
    def mixtureFractions(self):
        return self.__mixtureFractions

    @property
    def totalMixtureFraction(self):
        return self.__zTotal
