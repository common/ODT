from .ModelBase import ModelBase
import numpy as np
import cantera as ct

class EquilibriumModel(ModelBase):
    def __init__(self, canteraInput, streams, mixtureFractions, T_ref):
        ModelBase.__init__(self, canteraInput, streams, mixtureFractions)

        self.__T_ref = T_ref

        zShape = (next(iter(mixtureFractions.values()))).shape
        self.__mix = ct.Solution(self.canteraInput)
        self.__mix = ct.SolutionArray(self.__mix, zShape)

        ns = self.__mix.n_species
        Y = np.zeros(zShape + (ns,))
        P = 0
        self.__adiabaticEnthalpy =  0
        # self.__zTotal            =  0
        for label, stream in self.fuelStreams.items():
            z = mixtureFractions[label]
            Yj = stream.Y
            for i in range(ns):
                Y[:,...,i] += Yj[i] * z

            P += stream.P * z
            self.__adiabaticEnthalpy += stream.enthalpy_mass * z
            # self.__zTotal += z

        Yj = self.oxidizerStream.Y
        zTotal  = self.totalMixtureFraction
        for i in range(ns):
            Y[:, ..., i] += Yj[i] * (1 - zTotal)

        P += self.oxidizerStream.P * (1 - zTotal)
        self.__adiabaticEnthalpy += self.oxidizerStream.enthalpy_mass * (1 - zTotal)

        self.__mix.HPY = self.__adiabaticEnthalpy, P, Y
        self.__mix.equilibrate('HP')

        g = ct.Solution(self.canteraInput)
        g = ct.SolutionArray(g, zShape)
        g.TPY = self.__T_ref, self.__mix.P, self.__mix.Y
        self.__refererenceEnthalpy = g.enthalpy_mass

    def mix_with_heat_loss(self, gamma):
        assert(np.min(gamma) > -1)
        h = self.__refererenceEnthalpy * gamma/(1+gamma) +  self.__adiabaticEnthalpy/(gamma + 1)

        g = ct.Solution(self.canteraInput)
        g = ct.SolutionArray(g, self.__mix.T.shape)

        g.HPY = h, self.__mix.P, self.__mix.Y
        return g

    def values(self, alphaVal, gammaVal, fieldNames):
        if type(fieldNames) == str:
            names = [fieldNames]
        elif type(fieldNames) == list:
            names = fieldNames
        else:
            raise TypeError('argument "fieldNames" must be a string or list of strings')

        g = self.mix_with_heat_loss(gammaVal)

        fieldVals = dict()
        for name in names:
            if name == 'T':
                fieldVals[name] = g.T
            else:
                i = g.species_index(name)
                fieldVals[name] = g.Y[..., i]

        return fieldVals