from .ModelBase import ModelBase
from mixtureFraction import burke_schumann_mixture
import cantera as ct
import numpy as np

class BurkeShumannModel(ModelBase):
    def __init__(self, canteraInput, streams, mixtureFractions, T_ref):
        ModelBase.__init__(self, canteraInput, streams, mixtureFractions)

        self.__T_ref = T_ref

        # todo: change signeture of burke_schumann_mixture to (canteraInput, mixFracs, fuelStreams, oxidStream)
        self.__mix = burke_schumann_mixture(canteraInput, mixtureFractions, streams)

        self.__adiabaticEnthalpy =  self.__mix.enthalpy_mass

        zShape = (next(iter(mixtureFractions.values()))).shape
        g = ct.Solution(self.canteraInput)
        g = ct.SolutionArray(g, zShape)
        g.TPY = self.__T_ref, self.__mix.P, self.__mix.Y
        self.__refererenceEnthalpy = g.enthalpy_mass

    def mix_with_heat_loss(self, gamma):
        assert(np.max(gamma) > -1)
        h = self.__refererenceEnthalpy * gamma/(1+gamma) +  self.__adiabaticEnthalpy/(gamma + 1)

        g = ct.Solution(self.canteraInput)
        g = ct.SolutionArray(g, self.__mix.T.shape)

        g.HPY = h, self.__mix.P, self.__mix.Y
        return g

    def values(self, alphaVal, gammaVal, fieldNames):
        if type(fieldNames) == str:
            names = [fieldNames]
        elif type(fieldNames) == list:
            names  = fieldNames
        else:
            raise TypeError('argument "fieldNames" must be a string or list of strings')

        g = self.mix_with_heat_loss(gammaVal)

        fieldVals = dict()
        for name in names:
            if name == 'T':
                fieldVals[name] = g.T
            else:
                 i = g.species_index(name)
                 fieldVals[name] = g.Y[...,i]

        return fieldVals