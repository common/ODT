import flamelet.CoalFlamelet as cf
import flamelet.generate_table_vars as gtv
import numpy as np
from spitfire2.flamelet.mechanism import ChemicalMechanismSpec
import cantera as ct
import pickle
import time
import os

chemicalMechFuel       = 'methane'
chemicalMechIdentifier = 'lu30'

mech = ChemicalMechanismSpec(chemicalMechFuel, chemicalMechIdentifier)

adiabaticGenerated = False
dbDir = '/Users/joshmcconnell/runs/OFC/tarStudy/tarIsC2H2=true-speciation=false-diffDiff=false'
streamSpecDir = os.path.join(dbDir,'streamSpec.p')

P = ct.one_atm
streamSpec = pickle.load(open(streamSpecDir,'rb'))

oxidizerStream = ct.Solution(mech.mech_xml_path)
volatileStream = ct.Solution(mech.mech_xml_path)
charStream     = ct.Solution(mech.mech_xml_path)

volatileStream.TPY = streamSpec['volatiles']['T'], P, streamSpec['volatiles']['Y']
charStream.TPY     = streamSpec['volatiles']['T'], P, streamSpec['volatiles']['Y'] #!!!!!!!!!!!!!!!!!!!!!
oxidizerStream.TPY = streamSpec['oxidizer' ]['T'], P, streamSpec['oxidizer' ]['Y']

print('volatile stream temperature:', volatileStream.T)
print('char stream temperature    :', charStream.T)
print('oxidizer stream temperature:', oxidizerStream.T)

# tableVars = None
# alphaVals  = np.linspace(0.0,1,26)
alphaVals  = np.linspace(0.,0.,1)  #!!!!!!!!!!!!!!!!!!!!!
chiMaxVals = np.hstack([1e-2,  np.linspace(1e-1, 0.5, 5), np.logspace(0, np.log10(2000), 20)])

# alphaVals  = np.array([0.14,0.3])
# chiMaxVals = np.array([0.001])

params = cf.coalFlameletParams(chemicalMechFuel       =chemicalMechFuel,
                               chemicalMechIdentifier = chemicalMechIdentifier,
                               chiMax                 = chiMaxVals,
                               alpha                  = alphaVals,
                               gammaSlices= 75,
                               refTemperature         = 298,
                               volatileStream         = volatileStream,
                               charStream             = charStream,
                               oxidizerStream         = oxidizerStream,
                               convectiveDeltaT       = -2000,
                               gammaUpperBound =  1.2,
                               gammaLowerBound = -0.1 )

tableGen = cf.coalFlameletTableGenerator(params         = params,
                                         gridPoints     = 128,
                                         equilibrateBCs = False)

tableGen.savePath = os.path.join(dbDir, 'table.p')
tableGen.solverSpecs.tolerance = 7e-7
tableGen.solverSpecs.ds_max = 1e5
tableGen.solverSpecs.ds_init = 1e0
tableGen.solverSpecs.residual_tol = 1e6
tableGen.solverSpecs.norm_order = np.inf
tableGen.plot = True
    # tableGen.solverSpecs = cf.SolverSpec(ds_init = 1e-3)
if not adiabaticGenerated:
    t = time.time()
    tableGen.generate_adiabatic_table()
    print('solve over alpha and chiMax took ', time.time() - t, 'seconds')
    tableGen.save_table()
    adiabaticGenerated = True

else:

    # if tableVars is None:
    #     tableVars = gtv.generate_table_vars(coalType = coalType,dbDir = dbDir, canteraInput = mech.mech_path)

    table = pickle.load(open(tableGen.savePath, 'rb'))

    tableGen.setup()
    tableGen.table = table.copy()
    tableGen.extrude_table_for_gamma()
    tableGen.savePath = os.path.join(dbDir, 'table-4D.p')
    tableGen.save_table()
    tableGen.gammaLowerBound = -0.4
    tableGen.gammaUpperBound = 1.1

    tableGen.htcMinMultiplier = 1.07
    tableGen.htcMaxMultiplier = 1.5
    tableGen.gammaRefinementFactor = 2
    tableGen.generate_noadiabatic_table()

