import ExprDatabase as exdb
import numpy as np
import cantera as ct
import util.coal as coal
import mixtureFraction as mf
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize, SymLogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from collections import OrderedDict
import time
import pickle
import flamelet.TableInterpolator as tableInterpolator
from spitfire2.flamelet.mechanism import ChemicalMechanismSpec

font = {'family' : 'sans-serif',
       'weight' : 'normal',
       'size'   : 12}

mpl.rc('font', **font)

# coalType = 'Eastern Bituminous'
# dbdir = '/Volumes/samsung250gb/runs/OFC/EB-Dry69.db/'

chemicalMechFuel       = 'methane'
chemicalMechIdentifier = 'lu30'
mech = ChemicalMechanismSpec(chemicalMechFuel, chemicalMechIdentifier)
canteraInput = mech.mech_xml_path

coalType = 'Illinois No 6'
# dbDir = '/Users/joshmcconnell/runs/OFC/no-HCN/C2H2-as-fg/illinois-no-6/50micron/0-no-char-oxidation-1200K.db/'
dbDir= '/Volumes/Samsung_T5/OFC/no-HCN/C2H2-as-fg/illinois-no-6/50micron/0-no-char-oxidation-simple-1200K.db'
tableDir = 'Volumes/Samsung_T5/' \
           'OFC/no-HCN/C2H2-as-fg/illinois-no-6/50micron/0-no-char-oxidation-1200K.db/table-4D.p'

minTime = 0.0
maxTime = 0.25
interval = 1

includeTar     = True
coal.tarIsC2H2 = True # False if tar surrogate is assumed to be C10H8

db = exdb.ExprDatabase(dbDir, interval, minTime, maxTime)
db.set_cantera_input_file(canteraInput)
db.set_state_variables()

# remove volatile species we aren't considering
coal.devSpec.remove('NH3')
coal.devSpec.remove('HCN')

coal.load_particle_variables(db)
P      = ct.one_atm
T_ox   = 1200   # oxidizer temperature
T_ref  = 298    # reference temperature for calculating enthalpy deficit

airMoleFracs = {'O2' : 0.21,'N2' : 0.79}
oxidizerStream = coal.oxidizer_stream(T_ox, P, airMoleFracs, canteraInput, 'mole')

volatileStream = mf.get_volatile_stream(canteraInput,
                                        coalType,
                                        volatileT=coal.mean_volatile_gas_temperature(db,includeTar),
                                        tarIsC2H2=True)
# volatileStream = coal.volatiles_stream(db,P,canteraInput, includeTar)
# charStream     = coal.theoretical_char_gas_stream(oxidizerStream,canteraInput,db=db)
# charStream     = coal.theoretical_char_gas_stream(oxidizerStream,canteraInput,coFraction=.84)
streams = OrderedDict()

streams['volatiles'] = volatileStream
# streams['char'     ] = charStream
streams['oxidizer' ] = oxidizerStream

# save stream specifications
# streamSpec = dict()
# for name in streams.keys():
#     streamSpec[name] = dict()
#     streamSpec[name]['T'] = streams[name].T
#     streamSpec[name]['Y'] = streams[name].mass_fraction_dict()
#
# pickle.dump(streamSpec, open(dbDir + '/streamSpec.p', 'wb'))
# del streamSpec

Z_volStoich = mf.stoichiometric_mixture_fraction(streams['volatiles'], streams['oxidizer'])
# Z_charStoich = mf.stoichiometric_mixture_fraction(streams['char'], streams['oxidizer'])

elements = ['C','H','O']
normBehav = 'NONE'

streamMix = mf.StreamMixture(canteraInput)
streamMix.mixture_fraction_normalization = normBehav
streamMix.streams = streams
streamMix.set_mixture_fractions_from_composition(db.mix(), elements)

Z_vol  = streamMix.mixtureFractions['volatiles'].clip(0, 1)
# Z_char = streamMix.mixture_fractions['char'     ]
Z_tot  = Z_vol.clip(0 ,1) + Z_vol.clip(0 ,1)
alpha  = Z_vol.clip(0,1)/Z_tot.clip(0,1); alpha[np.where(Z_tot < 1e-3)] = 0.


L    = np.max(db.get_field('xcoord'))
npts = np.shape(db.get_field('xcoord'))[0]
dx   = L/npts
ST, X = np.meshgrid(db.get_time(), np.linspace(-L/2, L/2, npts), indexing='xy' )

chi = mf.calculate_scalar_dissipation_rate(Z_tot, dx, db.mix())
chiMax=mf.chi_to_chiMax(chi,Z_tot)

# calculate gamma
gamma = mf.calculate_gamma(db.mix(), streamMix.mix(), T_ref=T_ref)

table = pickle.load(open(tableDir, 'rb'))
interp = tableInterpolator.FlameletTableInterpolator(table,canteraInput)
gammaProxy = interp.gamma_to_gamma_proxy(Z_tot, chiMax, alpha, gamma)


# equilibrium calculation at h_obs ----------------------
P_obs = db.get_field('P')
equilMix = ct.Solution(canteraInput)
equilMix = ct.SolutionArray(equilMix, np.shape(P_obs))
equilMix.HPY = streamMix.mix().enthalpy_mass, streamMix.mix().P, streamMix.mix().Y

ts = time.time()
equilMix.equilibrate('HP')
print('1st equilibration took ' + repr(time.time()-ts) + ' seconds')

equilMix.HPY = db.mix().enthalpy_mass, equilMix.P, equilMix.Y
ts = time.time()
equilMix.equilibrate('HP')
print('2nd equilibration took ' + repr(time.time()-ts) + ' seconds')

# relative error
k    = db.mix().species_index('CH4')
obs  = db.mix().Y[..., k]
pred = equilMix.Y[...,k]
eps = 0.0001

err = ( pred - obs )/(obs + eps)

# f.set_size_inches(10,5)
# f.set_size_inches(10,5,forward=True)
# ax=plt.gca()
# c = ax.pcolormesh(np.squeeze(gamma.clip(None,ub)),cmap=cm)
# cbar = plt.colorbar(c,cmap=cm)

from scipy import io
saveDir = dbDir + '/equilbriumData'

savedVars = dict()
g = equilMix
specNames = g.species_names

Y = g.Y
for name in specNames:
    j = db.mix().species_index(name)
    savedVars.update({'Y_'+name: np.squeeze(Y[...,j])})

savedVars.update({'density': np.squeeze( g.density_mass  )})
savedVars.update({'T'      : np.squeeze( g.T             )})
savedVars.update({'E0'     : np.squeeze( g.enthalpy_mass )})

# savedVars.update({'gamma'  : np.squeeze(gamma)})
# savedVars.update({'mf_vol' : np.squeeze(streamMix.get_mixture_fraction('volatiles') )})
# savedVars.update({'mf_char': np.squeeze(streamMix.get_mixture_fraction('char')      )})
del(Y, g)

io.savemat(saveDir,savedVars)
# -----------------------------------------------------------------------------------------------------------------

def contour(field, shades = 150, vmin = None, vmax = None):
    if type(field) is str:
        field = db.get_field(field)
    cm = plt.cm.jet
    figSize = (14, 5.4)
    f = plt.figure(figsize=figSize)

    if not vmin == vmax ==None:
        field.clip(vmin, vmax)

    ax = plt.contourf(ST, X, field, shades, cmap=cm)
    # plt.colorbar(ax, cax=cax)
    plt.colorbar(ax, aspect=15)
    plt.tight_layout()


    # -----------------------------------------------------------------------------------------------------------------
def parity_plot_flamelet(fields, color, cond = None, ms = 1, transparency = 1, savePath = None, sort = 'forward'):
    if cond is None:
        cond = np.ones(alpha.size, dtype=bool)

    if type(fields) is str:
        interpName = fields
        if len(fields) > 1 and fields[0:2] == 'Y_':
            interpName = fields[2::]

        iSort = np.argsort(color[cond])

        if sort == 'reverse':
            iSort = iSort[::-1]

        obs  = db.get_field(fields)[cond]
        pred = interp.interpolate(Z_tot[cond], chiMax[cond], alpha[cond], gammaProxy[cond], interpName)

        error = (pred - obs)/(obs + 0.01*( np.sign(obs) + (1-np.sign(np.abs(obs))) ))
        avgError = np.mean(np.abs(error))
        print(fields, ': average error:', avgError)

        plt.figure('error')
        plt.hist(error.clip(-0.5, 0.5), 101)

        if savePath is not None:
            pth = savePath + 'hist-' + fields
            print('saving histogram to ', pth)
            plt.savefig(pth)

        cm = plt.cm.jet
        plt.figure(interpName)
        parity = [np.min(obs), np.max(obs)]

        ax = plt.scatter(obs[iSort],
                         pred[iSort],
                         c=color[cond][iSort],
                         cmap=cm, s=ms, alpha=transparency)
        plt.plot(parity, parity, 'k--', linewidth=1)
        cb = plt.colorbar(aspect=20)
        cb.set_alpha(1)
        cb.draw_all()
        plt.tight_layout()

        if savePath is not None:
            pth = savePath + '-' + fields
            print('saving parity plot to ', pth)
            plt.savefig(pth)

    elif type(fields) is list:
        for field in fields:
            parity_plot_flamelet(field, color, cond, ms, transparency, savePath, sort)

    else:
        raise TypeError('fields must be either a list of strings or a single string')

