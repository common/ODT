import numpy as np

A0_ = []
A0_.append(0.81E13) # CO2 extra loose(1)
A0_.append(0.65E17) # CO2 loose(2)
A0_.append(0.11E16) # CO2 tight(3)
A0_.append(0.22E19) # H2O loose(4)
A0_.append(0.17E14) # H2O tight(5)
A0_.append(0.14E19) # CO ether loose(6)
A0_.append(0.15E16) # CO ether tight(7)
# A0_.append(0.17E14) # HCN loose(8)
# A0_.append(0.69E13) # HCN tight(9)
# A0_.append(0.12E13) # NH3(10)
A0_.append(0.84E15) # CH4 extra loose(12)
A0_.append(0.75E14) # CH4 loose(13)
A0_.append(0.34E12) # CH4 tight(14)
A0_.append(0.10E15) # H aromatic(15)
A0_.append(0.20E14) # CO extra tight(17)
A0_.append(0.84E15) # CHx
A0_ = np.array(A0_)

E0_ = []
E0_.append(22500)
E0_.append(33850)
E0_.append(38315)
E0_.append(30000)
E0_.append(32700)
E0_.append(40000) # 6
E0_.append(40500)
# E0_.append(30000)
# E0_.append(42500)
# E0_.append(27300)
E0_.append(30000)
E0_.append(30000)
E0_.append(30000)
E0_.append(40500)
E0_.append(45500)
E0_.append(30000)
E0_ = np.array(E0_)

sigma_ = []
sigma_.append(1500)
sigma_.append(1500)
sigma_.append(2000)
sigma_.append(1500)
sigma_.append(1500)
sigma_.append(6000)
sigma_.append(1500)
# sigma_.append(1500)
# sigma_.append(4750)
# sigma_.append(3000)
sigma_.append(1500)
sigma_.append(2000)
sigma_.append(2000)
sigma_.append(6000)
sigma_.append(1500)
sigma_.append(1500)
sigma_ = np.array(sigma_)

# case
# coal::Illinois_No_6: // Interpolation

fg_ = []
fg_.append(0.0035)
fg_.append(0.0086)
fg_.append(0.0090)
fg_.append(0.0165)
fg_.append(0.0092)
fg_.append(0.0516)
fg_.append(0.0303)
# fg_.append(0.0164)
# fg_.append(0.0155)
# fg_.append(0.0)
fg_.append(0.0185)
fg_.append(0.0144)
fg_.append(0.0161)
fg_.append(0.0126)
fg_.append(0.0168)
fg_.append(0.1559)
fg_ = np.array(fg_)
fg_ /= fg_.sum()

i = 0
meanA0    = np.prod(A0_   [i::] ** fg_[i::])
meanE0    = np.sum (E0_   [i::]  * fg_[i::])
meanSigma = np.sum (sigma_[i::]  * fg_[i::])

