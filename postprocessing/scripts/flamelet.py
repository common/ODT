import flamelet.CoalFlamelet as cf
import numpy as np
from spitfire2.flamelet.mechanism import ChemicalMechanismSpec
import cantera as ct
import pickle
import time
import matplotlib.pyplot as plt


chemicalMechFuel       = 'methane'
chemicalMechIdentifier = 'lu30'

mech = ChemicalMechanismSpec(chemicalMechFuel, chemicalMechIdentifier)

adiabaticGenerated = False
dbDir = '/Volumes/Samsung_T5/OFC/no-HCN/C2H2-as-fg/' \
        'illinois-no-6/50micron/' \
        '0-no-char-oxidation-simple-1200K.db/'
streamSpecDir = dbDir + '/streamSpec.p'

P = ct.one_atm
streamSpec = pickle.load(open(streamSpecDir,'rb'))

# remove HCN:#######################
# del streamSpec['volatiles']['Y']['HCN']
########################################


oxidizerStream = ct.Solution(mech.mech_xml_path)
volatileStream = ct.Solution(mech.mech_xml_path)
charStream     = ct.Solution(mech.mech_xml_path)

volatileStream.TPY = streamSpec['volatiles']['T'], P, streamSpec['volatiles']['Y']

# charStream.TPY     = streamSpec['char'     ]['T'], P, streamSpec['char'     ]['Y']
oxidizerStream.TPY = streamSpec['oxidizer' ]['T'], P, streamSpec['oxidizer' ]['Y']

print('volatile stream temperature:', volatileStream.T)
print('char stream temperature    :', charStream.T)
print('oxidizer stream temperature:', oxidizerStream.T)

# tableVars = None
alphaVals  = np.linspace(0.,0,1)
chiMaxVals = np.hstack([np.linspace(1e-1, 0.5, 5), np.logspace(0, np.log10(5e3 +1), 25)])

# alphaVals  = np.array([0.14,0.3])
# chiMaxVals = np.array([0.001])

params = cf.coalFlameletParams(chemicalMechFuel       =chemicalMechFuel,
                               chemicalMechIdentifier = chemicalMechIdentifier,
                               chiMax                 = chiMaxVals,
                               alpha                  = alphaVals,
                               gammaSlices            = 75,
                               gammaLowerBound        = -0.2,
                               gammaUpperBound        = 1.6,
                               refTemperature         = 298,
                               volatileStream         = volatileStream,
                               charStream             = volatileStream,  ######### THIS!!!!!!
                               oxidizerStream         = oxidizerStream,
                               convectiveDeltaT       = -2000)

tableGen = cf.coalFlameletTableGenerator(params         = params,
                                         gridPoints     = 128,
                                         equilibrateBCs = False)

tableGen.savePath = dbDir + 'table.p'
tableGen.solverSpecs.tolerance = 7e-7
tableGen.solverSpecs.ds_max = 1e5
tableGen.solverSpecs.ds_init = 1e0
tableGen.solverSpecs.max_residual = 1e7
tableGen.solverSpecs.norm_order = np.inf
tableGen.plot = True
    # tableGen.solverSpecs = cf.SolverSpec(ds_init = 1e-3)
if not adiabaticGenerated:
    t = time.time()
    tableGen.generate_adiabatic_table()
    print('solve over alpha and chiMax took ', time.time() - t, 'seconds')
    tableGen.save_table()
    adiabaticGenerated = True

else:

    # if tableVars is None:
    #     tableVars = gtv.generate_table_vars(coalType = coalType,dbDir = dbDir, canteraInput = mech.mech_path)

    table = pickle.load(open(dbDir+'/table.p','rb'))

    tableGen.setup()
    tableGen.table = table.copy()
    tableGen.extrude_table_for_gamma()
    tableGen.savePath = dbDir + 'table-4D-new.p'
    tableGen.save_table()

    tableGen.gammaRefinementFactor = 2
    tableGen.findConvCoeffbounds = 1
    tableGen.htcMinMultiplier = 1.07
    tableGen.htcMaxMultiplier = 1.5
    tableGen.generate_noadiabatic_table()

