import matlab
import matlab.engine
import numpy as np
import matplotlib.cm as cm
eng = matlab.engine.start_matlab()

sm = cm.ScalarMappable(norm=None, cmap=cm.jet)
# --------------------------------------------------------------------------------------------------
def ndarray_to_flat_matlab_double(npArray):
    assert type(npArray) == np.ndarray

    # note -- for a 2D ndarray with 1 million entries using flatten().tolist() is ~2x faster than
    # using flatten()
    return matlab.double(npArray.flatten().tolist())

# --------------------------------------------------------------------------------------------------
def scatter3(x, y, z, size=10, color='blue', opacity=1, cmap = cm.jet):
    assert type(x) == np.ndarray
    assert type(y) == np.ndarray
    assert type(z) == np.ndarray

    mat_x = ndarray_to_flat_matlab_double(x)
    mat_y = ndarray_to_flat_matlab_double(y)
    mat_z = ndarray_to_flat_matlab_double(z)

    if type(color) == np.ndarray:
        sm.cmap = cmap
        c = sm.to_rgba(color, alpha=opacity)
        c_r = ndarray_to_flat_matlab_double(c[..., 0])
        c_g = ndarray_to_flat_matlab_double(c[..., 1])
        c_b = ndarray_to_flat_matlab_double(c[..., 2])
        # c_a = ndarray_to_flat_matlab_double(c[..., 0])

        mat_c = eng.squeeze(eng.cat(3, c_r, c_g, c_b))
    elif type(color) is str:
        mat_c = color
    else:
        raise TypeError("argument 'color must either be a numpy.ndarray or a string")

    return eng.scatter3(mat_x,
                        mat_y,
                        mat_z,
                        size,
                        mat_c,
                        'filled',
                        'MarkerFaceAlpha', opacity,
                        'MarkerEdgeAlpha', opacity)
