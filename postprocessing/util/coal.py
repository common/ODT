import ExprDatabase as exdb
import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt
import cantera as ct
from scipy.interpolate import RectBivariateSpline

gas = ct.Solution('gri30.xml')

devOnDiskPrefix = 'devol_'
devOnDiskSuffix = '_rhs'
devIndbPrefix    = 'devol_'

devSpec   = ['CH4', 'CO', 'CO2', 'H2', 'H2O', 'C2H2', 'NH3', 'HCN']


charOnDiskPrefix = 'char_'
charOnDiskSuffix = '_RHS'
charIndbPrefix   = 'char_'

rhsSuffix = '_rhs'

charSpec   = ['CO', 'CO2', 'O2', 'H2O', 'H2']


p2cPrefix      = 'P2CSpeciesSrc_'
fromTarSuffix  = 'srcFromTar'
fromSootSuffix = 'srcFromSoot'

deltaHco = 9629.64E3  # J/(kg char reacted) CO as a product
deltaHco2 = 33075.72E3#  J/(kg char reacted) CO2 as a product
heatFracToGas = 0.3

class empiricalFuel:
    def __init__(self, C, H):
        self.__C = C
        self.__H = H
        self.__mw = C * gas.atomic_weight("C") + \
                    H * gas.atomic_weight("H")

    @property
    def C(self):
        return self.__C

    @property
    def H(self):
        return self.__H

    @property
    def molecularWeight(self):
        return self.__mw


tar  = empiricalFuel(C=10, H=10)
soot = empiricalFuel(C=1 , H=0)

#-----------------------------------------------------------------------------------------------------------------------

def load_p2c_species_source_terms(db):
    # assert (isinstance(db, exdb.ExprDatabase))
    n = len(p2cPrefix)
    for name in db.get_field_names():
        if p2cPrefix in name:
            alias = 'p2cSrc_' + name[n:]
            db.load_field(name, alias)
#-----------------------------------------------------------------------------------------------------------------------

def load_tar_and_soot_source_terms(db):
    # assert (isinstance(db, exdb.ExprDatabase))
    for name in db.get_field_names():
        if fromTarSuffix in name:
            if 'rhoE0' in name:
                alias = 'e0'+fromTarSuffix
            else:
                alias = name
            db.load_field(name, alias)

        if fromSootSuffix in name:
            if 'rhoE0' in name:
                alias = 'e0'+fromSootSuffix
            else:
                alias = name
            db.load_field(name, alias)
#-----------------------------------------------------------------------------------------------------------------------

def load_particle_variables(db):
    # assert(isinstance(db, exdb.ExprDatabase))

    # load particle, volatile, char masses
    db.load_field('p_mass','particle_mass')
    db.load_field('p_size', 'particle_size')
    db.load_field('char_mass')
    db.load_field('v_Mass', 'volatile_mass')
    db.load_field('p_temperature', 'particle_T')
    db.load_field('p_x', 'particle_x')
    db.load_field('char_oxidation_RHS', 'char_oxidation_rhs')

    # cpd species RHSs
    for spec in devSpec:
        nameOnDisk = devOnDiskPrefix + spec + devOnDiskSuffix
        alias      = devIndbPrefix + spec + rhsSuffix
        db.load_field(nameOnDisk, alias)
    # tar
    nameOnDisk = devOnDiskPrefix + 'tar' + devOnDiskSuffix
    alias = devIndbPrefix + 'tar' + rhsSuffix
    db.load_field(nameOnDisk, alias)

    # char RHSs
    for spec in charSpec:
        nameOnDisk = charOnDiskPrefix + spec + charOnDiskSuffix
        alias      = charIndbPrefix + spec + rhsSuffix
        db.load_field(nameOnDisk, alias)
#-----------------------------------------------------------------------------------------------------------------------

def load_all_coal_related_variables(db):
    load_particle_variables(db)
    load_p2c_species_source_terms(db)
    load_tar_and_soot_source_terms(db)
#-----------------------------------------------------------------------------------------------------------------------

def weighted_mean(val,weights):
    return np.sum(val*weights)/np.sum(weights)
#-----------------------------------------------------------------------------------------------------------------------

def volatiles_light_gas_rhs(db):
    vol_rhs = 0

    for spec in devSpec:
        s = db.get_field(devIndbPrefix + spec + rhsSuffix)[:]
        vol_rhs += s

    return vol_rhs
#-----------------------------------------------------------------------------------------------------------------------

def plot_volatile_composition_vs_time(db, legend=True):
    vrhs = volatiles_light_gas_rhs(db)
    fh = plt.figure()
    for spec in devSpec:
        y = np.sum(db.get_field(devIndbPrefix + spec + rhsSuffix),axis=0)/np.sum(vrhs ,axis=0)
        plt.plot(1000 * db.get_time(), y, label=spec)
        print(spec)

    print('tar')
    y = np.sum(db.get_field(devIndbPrefix + 'tar' + rhsSuffix), axis=0) / np.sum(vrhs, axis=0)
    plt.plot(1000 * db.get_time(), y, label='tar')

    if legend:
        plt.legend()

    return fh

#-----------------------------------------------------------------------------------------------------------------------
def plot_volatile_composition_vs_temperature(db, legend=True):
    vrhs = volatiles_light_gas_rhs(db)
    fh = plt.figure()

    pT = db.get_field('particle_T')
    for spec in devSpec:
        y = db.get_field(devIndbPrefix + spec + rhsSuffix)/vrhs
        plt.scatter(pT, y, s=2, label=spec)
        print(spec)

    if legend:
        plt.legend()

    return fh

#-----------------------------------------------------------------------------------------------------------------------

def CO_mass_fraction_in_char_oxidation_products(db, tarCorrection = False):
    charOxRHS = db.get_field('char_oxidation_rhs')
    phi = db.get_field('CO2CO_ratio')  # molar ratio of CO2 and CO in char oxidation products

    mwCO  = gas.molecular_weights[gas.species_index('CO' )]
    mwCO2 = gas.molecular_weights[gas.species_index('CO2')]
    fCO = 1 / (1 + phi)

    if  tarCorrection:
        tarRHS = db.get_field('devol_tar_rhs')
        # tar: C10H8 + 7*O2 --> 10*CO + 4*H2O
        coFromTar   = tarRHS * 10 * mwCO / tar.molecularWeight
        coFromChar  = fCO * charOxRHS
        co2FromChar = (1-fCO) * charOxRHS

        fCO = (coFromChar + coFromTar)/(co2FromChar + coFromChar + coFromTar)
        fCO[~np.isfinite(fCO)] = 1

    return weighted_mean(fCO, charOxRHS)
#-----------------------------------------------------------------------------------------------------------------------


def char_gas_composition(oxStream, coFraction = None, db = None):
    '''
    :param oxStream: cantera solution with properties of oxidizer stream
    :param coFraction: mass fraction of CO in char oxidation products. If no value is passed, an attempt will be made
                       calculate coFraction from the data
    :return: a dict with {species:massFraction} pairs
    '''

    moles = oxStream.mole_fraction_dict()

    if coFraction is None:
        if db is None:
            raise ValueError('a database must be passed if no coFraction is given')

        coFraction = CO_mass_fraction_in_char_oxidation_products(db)
        print('calculated CO mass fraction in char oxidation products:', coFraction)

    g = ct.Solution('gri30.xml')
    g.TPY = oxStream.T, oxStream.P, {'CO': coFraction, 'CO2': (1-coFraction)}
    if 'CO' in g.mole_fraction_dict().keys():
        xCO = g.mole_fraction_dict()['CO']
    else:
        xCO = 0

    # rxn 1: 2*C(s) + O2 --> 2*CO
    # rxn 2: 1*C(s) + O2 --> 1*CO2
    r = xCO/(2-xCO) # fraction or of r of rxn 1 & 2 required to have a CO mole fraction of xCO in the char products

    if 'CO' in moles.keys():
        moles['CO'] += 2 * r * moles['O2']
    else:
        moles['CO'] = 2 * r * moles['O2']

    if 'CO2' in moles.keys():
        moles['CO2'] += (1 - r) * moles['O2']
    else:
        moles['CO2'] = (1 - r) * moles['O2']

    moles['O2'] = 0.

    g.TPX =g.T, g.P, moles

    return g.mass_fraction_dict()
#-----------------------------------------------------------------------------------------------------------------------

def mean_volatiles_composition(db):
    # assert (isinstance(db, exdb.ExprDatabase))

    composition = dict()
    tot = volatiles_light_gas_rhs(db)[:]

    for spec in devSpec:
        s = 0
        s += db.get_field(devIndbPrefix + spec + rhsSuffix)
        composition[spec] = np.sum(s) / np.sum(tot)

    return composition
#-----------------------------------------------------------------------------------------------------------------------

def mean_volatile_gas_temperature(db):
    # assert (isinstance(db, exdb.ExprDatabase))

    vrhs = volatiles_light_gas_rhs(db)
    Tp   = db.get_field('particle_T')

    return weighted_mean(Tp,vrhs)
#-----------------------------------------------------------------------------------------------------------------------

def mean_char_gas_temperature(db):
    # assert (isinstance(db, exdb.ExprDatabase))

    crhs = db.get_field('char_oxidation_rhs')
    Tp   = db.get_field('particle_T')

    return weighted_mean(Tp,crhs)
#-----------------------------------------------------------------------------------------------------------------------

def theoretical_char_gas_stream(oxStream, canteraInput, coFraction = None, db = None):

    mwO2  = oxStream.molecular_weights[oxStream.species_index('O2' )]
    mwC   = oxStream.atomic_weight('C')
    nuCO  = 0.5 * mwO2/mwC  # kg O2 per kg char for C(s) + 1.0*O2 --> CO
    nuCO2 = 1.0 * mwO2/mwC  # kg O2 per kg char for C(s) + 0.5*O2 --> CO2

    if coFraction is None:
        fCO = CO_mass_fraction_in_char_oxidation_products(db)
    else:
        fCO = coFraction

    nu = fCO * nuCO + (1-fCO) * nuCO2
    deltaHRxn = fCO * deltaHco + (1-fCO) * deltaHco2


    charStream = ct.Solution(canteraInput)
    charComp = char_gas_composition(oxStream, coFraction, db)
    charStream.TPY = oxStream.T, oxStream.P, charComp

    h = charStream.enthalpy_mass

    yO2 = oxStream.mass_fraction_dict()['O2']

    gasMassPerCharMass = 1 + nu*(1 + (1-yO2)/yO2) # (kg gas produced + inerts)/(kg char reacted)
    deltaH = heatFracToGas * deltaHRxn/gasMassPerCharMass
    h = charStream.enthalpy_mass + deltaH

    charStream.HPY = h, charStream.P, charStream.Y

    return charStream
#-----------------------------------------------------------------------------------------------------------------------

def oxidizer_stream(temperature, pressure, composition, canteraInput, basis):
    g = ct.Solution(canteraInput)

    if basis == 'mass':
        g.TPY = temperature, pressure, composition
    elif basis == 'mole':
        g.TPX = temperature, pressure, composition
    else:
        raise ValueError('unrecognizable basis "', basis,'"')

    return g
#-----------------------------------------------------------------------------------------------------------------------

def volatiles_stream(db, pressure, canteraInput):
    g = ct.Solution(canteraInput)

    temperature = mean_volatile_gas_temperature(db)
    composition = mean_volatiles_composition(db)

    g.TPY = temperature, pressure, composition

    return g
#-----------------------------------------------------------------------------------------------------------------------

def tar_stream(db, pressure, canteraInput):
    '''
    defines a stream resulting from the reaction of tar + oxidizer given by the following reaction:

    tar + oxidizer --> CO + H2O + (inerts in oxidizer)

    currently, the oxdizer is assumed to be air
    '''
    g = ct.Solution(canteraInput)
    xO2 = 0.21
    xN2 = 1 - xO2

    tarOxRate  = db.get_field('tarOxRate' ) # tar oxidation rate
    sootOxRate = db.get_field('sootOxRate') # soot oxidation rate
    gT         = db.get_field('T'         ) # gas temperature

    streamT = weighted_mean(gT, tarOxRate + sootOxRate + mwH2/mwCO*sootFormRate)

    reqMolesO2 = tar.C/2 + tar.H/4
    productMoles = dict()
    productMoles['CO' ] = tar.C
    productMoles['H2O'] = tar.H/2
    productMoles['N2' ] = xN2/xO2 * reqMolesO2

    g.TPX = streamT, pressure, productMoles

    return g
#-----------------------------------------------------------------------------------------------------------------------

def char_stream(db, pressure, oxidizerStream, canteraInput, coFraction  = None):
    g = ct.Solution(canteraInput)

    temperature = mean_char_gas_temperature(db)
    if coFraction is None:
        composition = char_gas_composition(oxidizerStream, coFraction=coFraction, db=db)
    else:
        print('setting CO fraction in char stream from input value:', coFraction)
        composition = char_gas_composition(oxidizerStream, coFraction=coFraction, db=None)

    g.TPY = temperature, pressure, composition
    return g

#-----------------------------------------------------------------------------------------------------------------------

def interpolate_gas_to_particle(db,field):

    centerWeight = 0.5
    edgeWeight   = 0.25

    f = field
    if type(field) is str:
        f  = db.get_field(f)

    x = db.get_field('xcoord')[:,0]
    t = db.get_time()

    if 'particle_x' in db.get_loaded_field_names():
        px = db.get_field('particle_x')
    else:
        px = db.get_field('p_x')

    if 'particle_size' in db.get_loaded_field_names():
        pSize = db.get_field('particle_size')
    else:
        pSize = db.get_field('p_size')

    interp_fcn = RectBivariateSpline(x,t,f,kx=1,ky=1)

    pt = np.matlib.repmat(t, px.shape[0], 1)

    return 1/(centerWeight + 2 * edgeWeight) * \
           (centerWeight * interp_fcn.ev(px              , pt) +
            edgeWeight   * interp_fcn.ev(px + 0.5 * pSize, pt) +
            edgeWeight   * interp_fcn.ev(px - 0.5 * pSize, pt))

#-----------------------------------------------------------------------------------------------------------------------
