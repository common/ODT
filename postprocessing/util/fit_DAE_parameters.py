import numpy as np
import scipy.special
import scipy.optimize
import matplotlib.pyplot as plt
import types
import ExprDatabase

xv_lowerBound = 1e-2
xv_upperBound = 0.99
includeTar  = True
doPlot      = True
constantTa  = False

A_guess = 0.84E15
Ta_guess = 30000
sigma_guess = 2000

def weighted_rate_constant(params, pT, xv = None):
    '''
    returns a rate constant of the following form:
    kw = xv * A  * exp(-Ta/pT),
    Ta = Ta_mean - sigma * sqrt(2) * erfinv(1 - 2*xv),

    A      : pre-exponential factor
    Ta     : activation temperature distribution (assumed to be Gaussian)
    Ta_mean: mean of Ta
    sigma  : stanard deviation of Ta
    pT     : particle temperature
    xv     : fraction of volatiles remaining


    params: A list og guesses for parameters to be fitted
    params[0]: --> A
    params[1]: --> Ta_mean
    params[2]: --> sigma
    '''
    E = params[1]
    if xv is not None:
        E -=  2 ** 0.5 * params[2] * scipy.special.erfinv(1 - 2 * xv)

    return xv * params[0] * np.exp(-E/pT)

def residual_function_DAE(params,pT,xv,kw):
    return weighted_rate_constant(params, pT, xv) - kw

def residual_function_SR(params,pT,kw):
    return weighted_rate_constant(params, pT) - kw

def fit_DAE_parameters(weightedRateConstant, pT, xv = None):
    '''

    :param pT: particle temperature
    :param xv: coal volatiles fraction remaining
    :param weightedRateConstant: rate constant for devolatilization given as:

     kw = (rate of devolatilization) / (initial volatile mass)

    :return: a tuple "params" with

    params[0]: --> A
    params[1]: --> Ta_mean
    params[2]: --> sigma
    '''

    if xv is None:
       params = [A_guess, Ta_guess]
       args = (pT, weightedRateConstant)
       rf = residual_function_SR
    else:
        params = [A_guess, Ta_guess, sigma_guess]
        args = (pT, xv, weightedRateConstant)
        rf = residual_function_DAE

    res = scipy.optimize.least_squares(rf,
                                       params,
                                       args=args)

    return res.x

def fitted_DAE_parameters(db, coalUtil):
    assert( isinstance(coalUtil, types.ModuleType))
    assert (isinstance(db, ExprDatabase.ExprDatabase))

    mv     = db.get_field('v_Mass')
    mv_rhs = coalUtil.volatiles_light_gas_rhs(db,includeTar)
    pT     = db.get_field('particle_T')

    n = mv.shape[0]
    weightedRateConstant = np.ones(mv.shape)
    xv = np.ones(mv.shape)
    for j in range(n):
        weightedRateConstant[j] = -mv_rhs[j]/mv[j,0]
        xv[j] = mv[j]/mv[j].max()

    i = (xv < xv_upperBound) & (xv > xv_lowerBound)

    if constantTa:
        params = fit_DAE_parameters(weightedRateConstant[i], pT[i])
        x = None
    else:
        params = fit_DAE_parameters(weightedRateConstant[i], pT[i], xv[i])
        x = np.linspace(xv[i].min(), xv[i].max(), 201)

    if doPlot:
        plt.figure('rate constant')
        t = np.linspace(pT.min(), pT.max(), 201)
        kw_fit  = weighted_rate_constant(params, t, x)

        ax = plt.gca()
        ax.scatter(1/pT, weightedRateConstant,s=2, c=xv, label='observed', cmap=plt.cm.jet)
        ax.set_yscale('log')
        ax.plot(1/t, kw_fit, 'tab:orange', label='fitted')
        # plt.colorbar()
        plt.ylabel('rate constant (1/s)')
        plt.xlabel('particle temperature (K)')
        plt.legend()
        plt.grid()
        plt.tight_layout()

        print('pre-exponential factor          : ', params[0])
        print('mean activation temperature (Ta): ', params[1])
        if not constantTa:
            print('Ta standard deviation           : ', params[2])

    return params