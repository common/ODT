import numpy as np
import util.database_field_operations as op

fluxPrefix    = 'SpeciesDiffusionFlux_'
rxnRatePrefix = 'SpeciesSourceTerm_'
p2cPrefix     = 'P2CSpeciesSrc_'
tarSuffix     = 'srcFromTar'
sootSuffix    = 'srcFromSoot'

# ---------------------------------------------------------------------///
def diffusive_term(db, species):
    flux = db.get_field(fluxPrefix + species)
    return -op.d_dx(db,flux)

# ---------------------------------------------------------------------///
def convective_term(db, species):
    y = db.get_field('rhoY_' + species)
    u = db.get_field('x_velocity_advect')
    # we'll need to multiply 'y' (cell-centered) against the advectting velocity (face-centered), so we first need to
    # "pad" 'y' and then interpolqte 'y' to cell faces.
    # NOTE: it is assumed that the grid used has uniform spacing

    y = op.interpolate_to_faceType(y, padBoundaries=True)

    return -op.d_dx(db, u*y)

# ---------------------------------------------------------------------///
def chemistry_term(db, species):
    return db.get_field(rxnRatePrefix + species)

# ---------------------------------------------------------------------///
def interfacial_term(db, species):
    src = 0

    # apparently, all the interfacial source terms are "consumption" rates and so are subtracted from the RHS

    name = species + tarSuffix
    if name in db.get_field_names():
        src -= db.get_field(name)

    name = species + sootSuffix
    if name in db.get_field_names():
        src -= db.get_field(name)

    name = p2cPrefix + species
    if name in db.get_field_names():
        src -= db.get_field(name)

    return src

# ---------------------------------------------------------------------///
def source_term(db, species):
    return chemistry_term(db, species) + interfacial_term(db, species)

# ---------------------------------------------------------------------///
def total_rhs(db, species):
    rhs = diffusive_term  (db, species) \
        + convective_term (db, species) \
        + source_term     (db, species)

    return rhs

# ---------------------------------------------------------------------///
def relative_rhs_magnitude(db, species, tol=1e-6):
    norm = np.abs
    numerator = norm( total_rhs(db, species) ) + tol

    denominator = tol                                  \
                + norm( diffusive_term (db, species) ) \
                + norm( convective_term(db, species) ) \
                + norm( source_term    (db, species) )

    return numerator/denominator
