import numpy as np

def grid_spacing(db):
    # get spatial discretization length. It's assumed that the grid is one dimentional and spacing is uniform
    L = np.max(db.get_field('xcoord'))
    npts = np.shape(db.get_field('xcoord'))[0]
    return L / npts

def d_dx(db, field):
    if type(field) is str:
        f = db.get_field(field)
    else:
        f = field.copy()

    dx = grid_spacing(db)
    # 2nd order... wont be used here
    # df = np.zeros( np.shape(f) )
    # df[1:-1] = (f[2:] - f[0:-2]) /(2 * dx)
    #
    # # set boundary cell values to their neighboring values
    # df[0]  = df[1]
    # df[-1] = df[-2]
    df = (f[1:] - f[0:-1]) /(dx)

    return df

def d2_dx2(db, field):
    if type(field) is str:
        f = db.get_field(field)
    else:
        f = field.copy()

    dx = grid_spacing(db)
    d2f = np.zeros( np.shape(f) )
    d2f[1:-1] = (f[2:] - 2*f[1:-1] + f[0:-2]) /(dx**2)

    # set boundary cell values to their neighboring values
    d2f[0]  = d2f[1]
    d2f[-1] = d2f[-2]

    return d2f

def pad_boundaries(field):
    '''
    This function "pads" the inputted field like so:
    field = [b0][interior][bn] -->  [b0][b0][interior][bn][bn]
    If the inputed field has n rows, the output will have n+2 rows.
    '''
    return np.row_stack([field[0], field, field[-1]])

def __interp_fcn(field):
    return 0.5 * (field[1:] + field[0:-1])

def interpolate_to_faceType(field, padBoundaries=False):
    '''
    This function performs a linear cellType-to-faceType interpolation
    Note: this function assumes that grid is uniformly spaced.
    If the inputed field has n rows, the output will have n-1 rows if pad=False. Otherwise, the output will have
    n+1 rows
    '''

    if padBoundaries == True:
        return __interp_fcn( pad_boundaries(field) )
    else:
        return __interp_fcn(field)