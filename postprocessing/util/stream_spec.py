
import pickle
from collections import OrderedDict

def save_stream_spec(streams,filePath):
    streamSpec = OrderedDict()
    for name in streams.keys():
        streamSpec[name] = dict()
        streamSpec[name]['T'] = streams[name].T
        streamSpec[name]['Y'] = streams[name].mass_fraction_dict()

    pickle.dump(streamSpec, open(filePath, 'wb'))