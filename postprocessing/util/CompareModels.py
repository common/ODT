import ExprDatabase as exdb
import numpy as np
import cantera as ct
import util.coal as coal
import mixtureFraction as mf
import matplotlib as mpl
import matplotlib.pyplot as plt

import mixtureFraction.models.BurkeSchumannModel as BS
import mixtureFraction.models.EquilibriumModel as EQ
import mixtureFraction.models.SteadyFlameletModel as SF

class CompareModels:
    def __init__(self, database, flameletInterp, mech, mixFrac, chiMax, alpha, gamma):
        self.db = database
        if flameletInterp is not None:
            self.flameInterp = flameletInterp
            self.Z = flameletInterp.mixFrac

        self. equilMech = mech

        self.mixFac = mixFrac
        self.chiMax = chiMax
        self.alpha  = alpha
        self.gamma  = gamma

        self.streams  = None
        self.savePath = None

        # tolerances
        self.mixFracTol = 0.01
        self.chiMaxTol  = 0.01
        self.alphaTol   = 0.01
        self.gammaTol   = 0.01

        self.condition = np.ones(mixFrac.shape, dtype=bool)

        self.T_ref = 298

        self.markerSize = 0.5
        self.transparency = 0.5
        self.slfm_linestyle = 'k'
        self.eq_linestyle = 'k--'

    def get_solution_array(self, alph, gam=np.array([0])):

        alph = np.array(alph)
        gam = np.array(gam)

        Z, A, G = np.meshgrid(self.Z, alph, gam)
        Z = np.squeeze(Z)
        A = np.squeeze(A)
        G = np.squeeze(G)

        g_ox   = self.streams['oxidizer']
        g_vol  = self.streams['volatiles']
        g_char = self.streams['char']

        h_ox = g_ox.enthalpy_mass
        h_vol = g_vol.enthalpy_mass
        h_char = g_char.enthalpy_mass

        H_ad = (1 - Z) * h_ox + A * Z * h_vol + (1 - A) * Z * h_char

        p = ct.one_atm * np.ones(A.shape)

        s = ct.Solution(self.equilMech)

        g1 = ct.SolutionArray(s, A.shape)

        Y1 = np.zeros(g1.Y.shape)

        for name in g_ox.species_names:
            i1 = g1.species_index(name)

            if i1 == -1:
                raise ValueError('the mechanism is missing ', name)

            i0 = g_ox.species_index(name)

            Y1[..., i1] = (1 - Z) * g_ox.Y[i0] + A * Z * g_vol.Y[i0] + (1 - A) * Z * g_char.Y[i0]

        # g1.HPY = H_ad, p, Y1

        g_ref = ct.SolutionArray(s, A.shape)
        g_ref.TPY = self.T_ref, p, Y1
        H_ref = g_ref.enthalpy_mass

        H = (G * H_ref + H_ad) / (1 + G)
        g1.HPY = H, p, Y1

        return g1

    def compare(self, chiMaxVal, alphaVal, gammaVal, fields):
        fields_loc = fields
        # find data that matches the given values for chiMax, alpha, and gamma

        j = (np.abs(self.chiMax - chiMaxVal) <= self.chiMaxTol) \
            & (np.abs(self.alpha - alphaVal) <= self.alphaTol) \
            & (np.abs(self.gamma - gammaVal) <= self.gammaTol) \
            & self.condition

        # get field from flamelet lookup table
        ones = np.ones(self.Z.shape)
        x = chiMaxVal * ones
        a = alphaVal * ones
        g = gammaVal * ones
        gp = self.flameInterp.gamma_to_gamma_proxy(self.Z, x, a, g)

        # equilibrium
        g_eq = self.get_solution_array(alphaVal, gammaVal)
        g_eq.equilibrate('HP')

        if type(fields_loc) == str:
            fields_loc = [fields_loc]

        plt.ion()
        for field in fields_loc:

            interpName = field
            if len(field) > 1 and field[0:2] == 'Y_':
                interpName = field[2::]

            f_db = self.db.get_field(field)[j]
            f_fl = self.flameInterp.interpolate(self.Z, x, a, gp, interpName)

            if interpName in g_eq.species_names:
                f_eq = g_eq.Y[..., g_eq.species_index(interpName)]
            else:
                f_eq = getattr(g_eq, interpName)

            plt.figure(interpName)
            plt.scatter(self.mixFac[j], f_db, s=self.markerSize, alpha=self.transparency)
            plt.plot(self.Z, f_fl, self.slfm_linestyle)
            plt.plot(self.Z, f_eq, self.eq_linestyle)
            plt.pause(1e-3)

            if self.savePath is not None:
                pth = self.savePath + '-' + interpName
                print('saving plot to ', pth)
                plt.savefig(pth)

        plt.ioff()