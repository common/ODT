import numpy as np
def relative_error(observed, predicted, tol=1e-3):
    s = tol * ((observed == 0) + np.sign(observed))
    return (predicted - observed)/(observed + s)