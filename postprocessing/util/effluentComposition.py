import numpy as np
import cantera as ct
import mixtureFraction as mf
import subprocess as sp

class TarSpecies:
    def __init__(self, C, H, heatOfRxn):
        # here we assume the following reaction occurs between tar and O2:
        #  tar + O2 --> CO + H2O

        self.mix = ct.Solution('gri30.cti')
        self.C = C
        self.H = H
        self.molecular_weight = self.H*self.mix.atomic_weight('H') + self.C*self.mix.atomic_weight('C')
        self.nu_O2 = self.H/4 + self.C/2 # required moles of O2 per mole of tar
        self.heatOfRxn = heatOfRxn


# empirical Tar
tar = TarSpecies(C=10, H=8, heatOfRxn=-1.8131e+07)

# path to get_coal_info utility
gci_exe = "/Users/josh/development/builds/ODT/opt/src/coal/coaltest/get_coal_info"

# ----------------------------------------------------------------------------------------------------------------------
def tar_products(canteraInput, oxidizerMix, T_fuel):

    productMix = ct.Solution(canteraInput)
    productMoles = {specName:0 for specName in oxidizerMix.species_names}

    # add moles of CO and H2O from rxn of tar with O2
    productMoles['CO' ] = tar.C
    productMoles['H2O'] = tar.H/2

    xO2 = oxidizerMix.mole_fraction_dict()['O2']
    yO2 = oxidizerMix.mass_fraction_dict()['O2']
    assert(xO2 > 0)

    wO2  = oxidizerMix.molecular_weights[oxidizerMix.species_index('O2')]
    wTar = tar.molecular_weight

    # add moles of diluent species
    for specName in oxidizerMix.species_names:
        if specName == "O2": continue

        iSpec = oxidizerMix.species_index(specName)
        productMoles[specName] += oxidizerMix.X[iSpec]*tar.nu_O2/(xO2)

    productMix.TPX = T_fuel, ct.one_atm, productMoles

    # incorporate heat of rxn for tar with O2
    massFromTar = yO2/tar.nu_O2 * wTar/wO2 # (mass of tar)/mass of oxidizer
    yFromTar    = massFromTar/(1+massFromTar)
    print('y from tar:', yFromTar)
    deltaH = -tar.heatOfRxn * yFromTar
    H = productMix.enthalpy_mass + deltaH

    productMix.HPY = H, ct.one_atm, productMix.Y

    return productMix

# ----------------------------------------------------------------------------------------------------------------------
def char_products(canteraInput, oxidizerMix, T_fuel):

    productMix = ct.Solution(canteraInput)
    nu_O2 = 0.5 # C(s) + 0.5*O2 --> CO
    deltaHrxnKgChar = -9629.64E3;  # tar + O2 --> CO + H2O (J/kg tar)

    productMoles = {specName:0 for specName in oxidizerMix.species_names}

    # add moles of CO  from rxn of char with O2
    productMoles['CO' ] = 1

    xO2 = oxidizerMix.mole_fraction_dict()['O2']
    yO2 = oxidizerMix.mass_fraction_dict()['O2']
    assert(xO2 > 0)

    wO2  = oxidizerMix.molecular_weights[oxidizerMix.species_index('O2')]
    wC   = oxidizerMix.atomic_weight('C')

    # add moles of diluent species
    for specName in oxidizerMix.species_names:
        if specName == "O2": continue

        iSpec = oxidizerMix.species_index(specName)
        productMoles[specName] += oxidizerMix.X[iSpec]*nu_O2/(xO2)

    productMix.TPX = T_fuel, ct.one_atm, productMoles

    # incorporate heat of rxn for char with O2
    massFromChar = yO2/nu_O2 * wC/wO2  # (mass of char)/mass of oxidizer
    yFromChar = massFromChar / (1 + massFromChar)
    deltaH = -deltaHrxnKgChar * yFromChar
    H = productMix.enthalpy_mass + deltaH
    print('delta H:' ,deltaH)
    productMix.HPY = H, ct.one_atm, productMix.Y

    return productMix

# ----------------------------------------------------------------------------------------------------------------------
def parse_output(output,key):
    tmp = output.replace(" ", '')
    idx = tmp.find(key)
    tmp = tmp[idx::].replace(key+':','')
    idx = tmp.find('\\n')
    tmp = tmp[0:idx]
    assert(len(tmp)>0)
    return float(tmp)

# ----------------------------------------------------------------------------------------------------------------------
def effluent_composition(canteraInput,
                         oxidizerMix,
                         ofr,
                         T_particle,
                         coalType):
    P = ct.one_atm

    # stoichiometric coefficient (mole basis)
    nu_char = 0.5 # C(s) + 0.5O2 --> CO
    nu_tar  = tar.nu_O2

    output = sp.run([gci_exe,'--coal_type', 'Eastern_Bituminous'], stdout=sp.PIPE)
    msg = str(output.stdout)
    msg = msg.replace("b'", "")
    msg = msg.replace("'", '')

    # proximate analysis of coal
    y_char  = parse_output(msg, 'Y_char'     )
    y_vol   = parse_output(msg, 'Y_volatiles')
    y_moist = parse_output(msg, 'Y_moisture' )
    y_ash   = parse_output(msg, 'Y_ash'      )

    # mass fraction of fuel in coal
    y_fuel = y_char + y_vol

    # volatile light gas composition
    volatilesSpecies = ['CH4', 'CO', 'CO2', 'H2', 'H2O', 'HCN', 'NH3']
    volComp = dict()
    for spec in volatilesSpecies:
        volComp[spec] = parse_output(msg, spec)

    # mass fraction of tar in particle
    y_tar = parse_output(msg, 'tar')*y_vol

    # light gas mass fraction in particle
    y_lg  = y_vol - y_tar


    lightGasMix   = ct.Solution(canteraInput)
    lightGasMix.TPY = T_particle, P, volComp

    moistureMix   = ct.Solution(canteraInput)
    moistureMix.TPY = T_particle, P, {'H2O':1}

    charProductMix = char_products(canteraInput, oxidizerMix, T_particle)
    tarProductMix  = tar_products( canteraInput, oxidizerMix, T_particle)

    wC   = lightGasMix.atomic_weight('C')
    wO2  = lightGasMix.molecular_weights[lightGasMix.species_index('O2')]
    wTar = tar.molecular_weight

    yO2_ox = oxidizerMix.mass_fraction_dict()['O2']

    # mass tar products and inerts from oxidizer per mass of tar reacted
    rp_tar = 1 + nu_tar * wO2/wTar / yO2_ox
    # mass char products and inerts from oxidizer per mass of char reacted
    rp_char = 1 + nu_char*wO2/wC / yO2_ox

    # minimum oxidizer to fuel ratio, which is the oxidizer required to consume all tar and char
    ofr_min = (y_char*(rp_char-1) + y_tar*(rp_tar-1))/y_fuel

    if ofr < ofr_min:
        raise ValueError('oxidizer to fuel mass ratio (ofr) is smaller than the minimum allowable value (' +
                          str(ofr_min) + ') for ' + coalType + ' coal.' +
                         'Please ensure the ofr parameter is greater than or equal to this value')

    # total mass of stuff that will end up in the gas phase (oxidizer, fuel, and moisture)
    totalMass = 1 + ofr + y_moist/y_fuel

    # mass oxidizer consumed by (and remaining from) tar and char
    z_OxConsumed = ofr_min/totalMass
    z_OxRemain   = (ofr-ofr_min)/totalMass

    z_charProd = y_char * rp_char / ((1-y_ash)*totalMass)
    z_tarProd  = y_tar  * rp_tar  / ((1-y_ash)*totalMass)
    z_vol      = y_vol   / ((1-y_ash)*totalMass)
    z_moist    = y_moist / ((1-y_ash)*totalMass)

    z_ox = 1 - (z_charProd + z_tarProd + z_vol + z_moist)

    # print("sum of z's:", z_charProd + z_tarProd + z_vol + z_moist + z_OxRemain)
    print("z_cp + z_tp + z_OxConsumed:", z_charProd + z_tarProd + z_vol + z_moist + z_OxRemain)

    # mixture composition, enthalpy
    Ymix = z_ox       * oxidizerMix   .Y + \
           z_charProd * charProductMix.Y + \
           z_tarProd  * tarProductMix .Y + \
           z_vol      * lightGasMix   .Y + \
           z_moist    * moistureMix   .Y


    # latent heat of moisture vaporization at 350K (J/kg)
    h_vap = 2315.9e3

    # mixture enthalpy
    Hmix = z_ox       * oxidizerMix   .enthalpy_mass + \
           z_charProd * charProductMix.enthalpy_mass + \
           z_tarProd  * tarProductMix .enthalpy_mass + \
           z_vol      * lightGasMix   .enthalpy_mass + \
           z_moist    * (moistureMix.enthalpy_mass - h_vap)

    mix = ct.Solution(canteraInput)
    mix.HPY = Hmix, ct.one_atm, Ymix

    return mix
