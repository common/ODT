import ExprDatabase as exdb
import argparse
parser = argparse.ArgumentParser(description='Converts each field in an ODT database to a numpy binary')


parser.add_argument('--odt_db',
                    type     = str,
                    required = True,
                    help     = 'Path to ODT database')

parser.add_argument('--dest',
                    type     = str,
                    required = False,
                    help     = 'Path to directory where numpy binaries will be written '
                               '(set to odt_db by default).')

parser.add_argument('--interval',
                    type     = int,
                    default  = 1,
                    required = False,
                    help     = 'Database output interval. If interval=n, then every nth time output '
                               'is included in thr numpy binary for each field.')

args = parser.parse_args()

dbDir    = args.odt_db
npyDir   = args.dest
interval = args.interval

if npyDir is None:
    npyDir = dbDir


db = exdb.ExprDatabase(databaseDir=dbDir, timeInterval=interval, forceLoadFromODTDB=True)
db.save_all_vars_to_npy(saveDir=npyDir, deleteFields=True)
