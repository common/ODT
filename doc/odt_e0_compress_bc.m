clear;
syms d u v p T cp cv e0 hi W Wi Yi g R;
syms tauxx_x tauyx_x q_x Ji_x;

% cons = [ du dv d dh dYi ];
% prim = [ u  v  d p  Yi  ];

% assume 1D

% convective term for conserved variables
F = [ d*u*u; d*v*u; d*u; d*e0*u; d*Yi*u ];
D = [ tauxx_x; tauyx_x; 0; q_x; Ji_x ];

P = [ d, 0, u, 0, 0;
      0, d, v, 0, 0;
      0, 0, 1, 0, 0;
      d*u, d*v, e0-cv*T, 1/(g-1), d*(hi-cp*T*W/Wi);
      0, 0, Yi, 0, d ];
Pinv = simplify( inv(P) );
    
% Q = \frac{\partial F}{\partial \mathsf{U}}
Q = [ 2*d*u, 0,   u*u, 1, 0;
      d*v, d*u, v*u, 0, 0;
      d,   0,   u,   0, 0;
      d*e0+p+d*u*u, d*u*v, u*(e0-cv*T), u*g/(g-1), d*u*(hi-cp*T*W/Wi);
      d*Yi, 0,  u*Yi, 0, d*u ];

A = simplify( Pinv*Q );

A(4,1)=g*p;
[EVec,EVal,PP] = eig(A);


syms c;
%S = [ 0 0 0 c/d -c/d; 1 0 0 0 0; 0 1 0 1 1; 0 0 0 c^2 c^2; 0 0 1 0 0];
S = [ 0 0 0 1/d -1/d; 1 0 0 0 0; 0 1 0 1 1; 0 0 0 c^2 c^2; 0 0 1 0 0];
L = diag([u u u u+c u-c]);
LS=L*inv(S);
syms du dv dd dp dYi;
dUprimitive = [ du; dv; dd; dp; dYi ];
Lterms = LS*dUprimitive;
pretty(Lterms);
pretty(inv(S)*dUprimitive);

syms L1 L2 L3 L4 L5;
dterms = simple(S*[L1; L2; L3; L4; L5]);
pretty(dterms)

% convective term for primitive variables
f = simplify( Pinv*F );