#LyX 1.6.0rc3 created this file. For more info see http://www.lyx.org/
\lyxformat 340
\begin_document
\begin_header
\textclass article
\begin_preamble
\usepackage{cite}
\date{}
\end_preamble
\language english
\inputencoding auto
\font_roman palatino
\font_sans default
\font_typewriter courier
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize 11
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\leftmargin 1.8cm
\topmargin 1.8cm
\rightmargin 1.8cm
\bottommargin 1.8cm
\footskip 1cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Standard
Let's consider the governing equations in one dimension 
\begin_inset Formula $(x)$
\end_inset

 where we will also retain the 
\begin_inset Formula $y$
\end_inset

-momentum equation (the motivation here is that we will be shoving this
 into an ODT model).
\begin_inset Formula \begin{eqnarray*}
\frac{\partial\rho u}{\partial t}+\frac{\partial}{\partial x}\rho uu & = & -\frac{\partial\tau_{xx}}{\partial x}-\frac{\partial p}{\partial x}+\rho g_{x}\\
\frac{\partial\rho v}{\partial t}+\frac{\partial}{\partial x}\rho vu & = & -\frac{\partial\tau_{yx}}{\partial x}+\rho g_{y},\\
\frac{\partial\rho h}{\partial t}+\frac{\partial}{\partial x}\rho hu & = & -\frac{\partial q}{\partial x}-\tau_{xx}\frac{\partial u}{\partial x}-\tau_{yx}\frac{\partial v}{\partial x}+\frac{\mathrm{D}p}{\mathrm{D}t},\\
\frac{\partial\rho}{\partial t}+\frac{\partial}{\partial x}\rho u & = & 0,\\
\frac{\partial\rho Y_{i}}{\partial t}+\frac{\partial}{\partial x}\rho Y_{i}u & = & -\frac{\partial J_{i}}{\partial x}+\omega_{i}.\end{eqnarray*}

\end_inset

We can write the governing equations at the conserved, primitive, and characteri
stic variable levels as
\begin_inset Formula \[
\begin{array}{rcl}
\frac{\partial\mathbf{U}_{a}}{\partial t}+\nabla_{(n)}\cdot\mathbf{F}_{a}^{(n)}+\nabla_{(t)}\cdot\mathbf{F}_{a}^{(t)} & = & \mathbf{D}_{a}^{(n)}+\mathbf{D}_{a}^{(t)}+\mathbf{s}_{a},\\
\\\left(\mathbf{P}\right)_{ba}^{-1}\Downarrow &  & \Uparrow\mathbf{P}_{ab}\\
\\\frac{\partial\mathsf{U}_{b}}{\partial t}+\mathsf{A}_{bd}^{(n)}\cdot\left(\nabla_{(n)}\cdot\mathsf{U}\right)+\mathsf{A}_{bd}^{(t)}\cdot\left(\nabla_{(t)}\cdot\mathsf{U}\right) & = & \mathsf{D}_{b}+\mathsf{s}_{b},\\
\\\left(\mathsf{S}\right)_{cb}^{-1}\Downarrow &  & \Uparrow\mathsf{S}_{bc}\\
\\\left(\mathsf{S}\right)_{cb}^{-1}\frac{\partial\mathsf{U}_{b}}{\partial t}+\mathcal{L}_{c}^{(n)}+\mathcal{A}_{cd}^{(n)}\cdot\left(\nabla_{(n)}\cdot\mathsf{U}_{d}\right)+\mathcal{A}_{cd}^{(t)}\cdot\left(\nabla_{(t)}\cdot\mathsf{U}_{d}\right) & = & \mathcal{D}_{c}+\mathfrak{s}_{c}.\end{array}\]

\end_inset

 Here 
\begin_inset Formula $\mathsf{A}=\mathbf{P}^{-1}\mathbf{Q},$
\end_inset

 
\begin_inset Formula $\mathbf{Q}=\frac{\partial\mathbf{F}}{\partial\mathsf{U}},$
\end_inset

 
\begin_inset Formula $\mathbf{P}=\frac{\partial\mathbf{U}}{\partial\mathsf{U}},$
\end_inset

 and the columns of 
\begin_inset Formula $\mathsf{S}{}^{(n)}$
\end_inset

 are the right eigenvectors of 
\begin_inset Formula $\mathsf{A}^{(n)}.$
\end_inset

 In this case, there are no tangential terms, and the boundary-normal direction
 is 
\begin_inset Formula $x$
\end_inset

.
\end_layout

\begin_layout Standard
If we choose 
\begin_inset Formula $\mathbf{U}=\left\{ \rho u,\rho v,\rho,\rho h,\rho Y_{i}\right\} ^{\mathsf{T}}$
\end_inset

 and restrict ourselves to 1D (but keep the 
\begin_inset Formula $y$
\end_inset

-momentum equation) then we have 
\begin_inset Formula \[
\mathbf{F}_{a}=\left[\begin{array}{l}
\rho uu\\
\rho vu\\
\rho u\\
\rho hu\\
\rho Y_{i}u\end{array}\right],\quad\quad\mathbf{D}_{a}=\left[\begin{array}{c}
\frac{\partial\tau_{xx}}{\partial x}\\
\frac{\partial\tau_{yx}}{\partial x}\\
0\\
\frac{\partial q_{x}}{\partial x}\\
\frac{\partial J_{i,x}}{\partial x}\end{array}\right],\quad\mathrm{\mathbf{s}}_{a}=\left[\begin{array}{c}
-\frac{\partial p}{\partial x}+\rho g_{x}\\
\rho g_{y}\\
0\\
\frac{\mathrm{D}p}{\mathrm{D}t}-\tau:\nabla\mathbf{u}\\
\omega_{i}\end{array}\right]\]

\end_inset

 If we choose 
\begin_inset Formula $\mathsf{U}=\left\{ u,v,\rho,p,Y_{i}\right\} ^{\mathsf{T}}$
\end_inset

 then we can define 
\begin_inset Formula \[
\mathbf{P}_{ab}=\frac{\partial\mathbf{U}_{a}}{\partial\mathsf{U}_{b}}=\left[\begin{array}{ccccc}
\rho & 0 & u & 0 & 0\\
0 & \rho & v & 0 & 0\\
0 & 0 & 1 & 0 & 0\\
0 & 0 & h-c_{p}T & \frac{\gamma}{\gamma-1} & \rho\left(h_{i}-\frac{c_{p}TW}{W_{i}}\right)\\
0 & 0 & Y_{i} & 0 & \rho\end{array}\right]\]

\end_inset

 and
\begin_inset Formula \[
\mathbf{Q}^{(x)}=\frac{\partial\mathbf{F}}{\partial\mathsf{U}}=\left[\begin{array}{ccccc}
2\rho u & 0 & uu & 0 & 0\\
\rho v & \rho u & vu & 0 & 0\\
\rho & 0 & u & 0 & 0\\
\rho h & 0 & u\left(h-c_{p}T\right) & \frac{u\gamma}{\gamma-1} & \rho u\left(h_{i}-\frac{c_{p}TW}{W_{i}}\right)\\
\rho Y_{i} & 0 & Y_{i}u & 0 & \rho u\end{array}\right].\]

\end_inset

 In these equations, we have used the thermodynamic identity 
\begin_inset Formula \begin{eqnarray*}
\mathrm{d}h & = & c_{p}\mathrm{d}T+\sum_{i=1}^{n_{s}}h_{i}\mathrm{d}Y_{i}\\
 & = & \frac{-c_{p}T}{\rho}\mathrm{d}\rho+\frac{\gamma}{\rho\left(\gamma-1\right)}\mathrm{d}p+\sum_{i=1}^{n_{s}}\left(h_{i}-\frac{c_{p}TW}{W_{i}}\right)\mathrm{d}Y_{i},\end{eqnarray*}

\end_inset

with 
\begin_inset Formula $\gamma=\frac{c_{p}}{c_{v}}$
\end_inset

 and 
\begin_inset Formula $c_{p}=\frac{\gamma R}{\left(\gamma-1\right)W}.$
\end_inset

 From the above definitions for 
\begin_inset Formula $\mathbf{P}$
\end_inset

 and 
\begin_inset Formula $\mathbf{Q}$
\end_inset

 we obtain 
\end_layout

\begin_layout Standard
\begin_inset Formula \[
\mathsf{A}^{(x)}=\mathbf{P}^{-1}\mathbf{Q}^{(x)}=\left[\begin{array}{ccccc}
u & 0 & 0 & 0 & 0\\
0 & u & 0 & 0 & 0\\
\rho & 0 & u & 0 & 0\\
p & 0 & 0 & u & 0\\
0 & 0 & 0 & 0 & u\end{array}\right].\]

\end_inset

 The eigenvalues of 
\begin_inset Formula $\mathsf{A}$
\end_inset

 are 
\begin_inset Formula $\lambda=\left\{ u,u,u,u,u\right\} $
\end_inset

.
\end_layout

\begin_layout Standard

\lyxline

\end_layout

\begin_layout Standard
If we choose 
\begin_inset Formula $\mathbf{U}=\left\{ \rho u,\rho v,\rho,\rho e_{0},\rho Y_{i}\right\} ^{\mathsf{T}}$
\end_inset

 and restrict ourselves to 1D (but keep the 
\begin_inset Formula $y$
\end_inset

-momentum equation) then we have 
\begin_inset Formula \[
\mathbf{F}_{a}=\left[\begin{array}{l}
\rho uu+p\\
\rho vu\\
\rho u\\
\rho e_{0}u+pu\\
\rho Y_{i}u\end{array}\right],\quad\quad\mathbf{D}_{a}=\left[\begin{array}{c}
\frac{\partial\tau_{xx}}{\partial x}\\
\frac{\partial\tau_{yx}}{\partial x}\\
0\\
\frac{\partial q_{x}}{\partial x}\\
\frac{\partial J_{i,x}}{\partial x}\end{array}\right],\quad\mathrm{\mathbf{s}}_{a}=\left[\begin{array}{c}
\rho g_{x}\\
\rho g_{y}\\
0\\
-\tau_{xx}\frac{\partial u}{\partial x}\\
\omega_{i}\end{array}\right]\]

\end_inset

If we choose 
\begin_inset Formula $\mathsf{U}=\left\{ u,v,\rho,p,Y_{i}\right\} ^{\mathsf{T}}$
\end_inset

 then we can define 
\begin_inset Formula \[
\mathbf{P}_{ab}=\frac{\partial\mathbf{U}_{a}}{\partial\mathsf{U}_{b}}=\left[\begin{array}{ccccc}
\rho & 0 & u & 0 & 0\\
0 & \rho & v & 0 & 0\\
0 & 0 & 1 & 0 & 0\\
\rho u & \rho v & e_{0}-c_{v}T & \frac{1}{\gamma-1} & \rho\left(h_{i}-\frac{c_{p}TW}{W_{i}}\right)\\
0 & 0 & Y_{i} & 0 & \rho\end{array}\right]\]

\end_inset

and
\begin_inset Formula \[
\mathbf{Q}^{(x)}=\frac{\partial\mathbf{F}}{\partial\mathsf{U}}=\left[\begin{array}{ccccc}
2\rho u & 0 & uu & 1 & 0\\
\rho v & \rho u & vu & 0 & 0\\
\rho & 0 & u & 0 & 0\\
\rho e_{0}+p+\rho uu & \rho uv & u\left(e_{0}-c_{v}T\right) & \frac{u\gamma}{\gamma-1} & \rho u\left(h_{i}-\frac{c_{p}TW}{W_{i}}\right)\\
\rho Y_{i} & 0 & Y_{i}u & 0 & \rho u\end{array}\right].\]

\end_inset

 In these equations, we have used the thermodynamic identity 
\begin_inset Formula \begin{eqnarray*}
\mathrm{d}e & = & c_{v}\mathrm{d}T+\sum_{i=1}^{n_{s}}\left(h_{i}-\frac{pW}{\rho W_{i}}\right)\mathrm{d}Y_{i}\\
 & = & \frac{-c_{v}T}{\rho}\mathrm{d}\rho+\frac{1}{\rho\left(\gamma-1\right)}\mathrm{d}p+\sum_{i=1}^{n_{s}}\left(h_{i}-\frac{c_{p}TW}{W_{i}}\right)\mathrm{d}Y_{i},\end{eqnarray*}

\end_inset

with 
\begin_inset Formula $\gamma=\frac{c_{p}}{c_{v}}$
\end_inset

 and 
\begin_inset Formula $c_{v}=\frac{R}{\left(\gamma-1\right)W}.$
\end_inset

 From the above definitions for 
\begin_inset Formula $\mathbf{P}$
\end_inset

 and 
\begin_inset Formula $\mathbf{Q}$
\end_inset

 we obtain 
\begin_inset Formula \begin{eqnarray*}
\mathsf{A} & = & \left[\begin{array}{ccccc}
u & 0 & 0 & \frac{1}{\rho} & 0\\
0 & u & 0 & 0 & 0\\
\rho & 0 & u & 0 & 0\\
\left(\gamma-1\right)\left(\rho c_{v}T+p\right) & 0 & 0 & u & 0\\
0 & 0 & 0 & 0 & u\end{array}\right]\\
 & = & \left[\begin{array}{ccccc}
u & 0 & 0 & \frac{1}{\rho} & 0\\
0 & u & 0 & 0 & 0\\
\rho & 0 & u & 0 & 0\\
\gamma p & 0 & 0 & u & 0\\
0 & 0 & 0 & 0 & u\end{array}\right].\end{eqnarray*}

\end_inset

The speed of sound in an ideal gas is given as 
\begin_inset Formula $c=\sqrt{\gamma\frac{RT}{W}}=\sqrt{\gamma\frac{p}{\rho}}.$
\end_inset

 We can represent 
\begin_inset Formula $\Lambda$
\end_inset

 and 
\begin_inset Formula $\mathsf{S}$
\end_inset

 as 
\begin_inset Formula \begin{eqnarray*}
\Lambda & = & \left\{ \begin{array}{ccccc}
u, & u, & u, & u+c, & u-c\end{array}\right\} ,\\
\mathsf{S} & = & \left[\begin{array}{ccccc}
0 & 0 & 0 & \frac{c}{\rho} & -\frac{c}{\rho}\\
1 & 0 & 0 & 0 & 0\\
0 & 1 & 0 & 1 & 1\\
0 & 0 & 0 & c^{2} & c^{2}\\
0 & 0 & 1 & 0 & 0\end{array}\right],\\
\mathsf{S}^{-1} & = & \left[\begin{array}{ccccc}
0 & 1 & 0 & 0 & 0\\
0 & 0 & 1 & -\frac{1}{c^{2}} & 0\\
0 & 0 & 0 & 0 & 1\\
\frac{\rho}{2c} & 0 & 0 & \frac{1}{2c^{2}} & 0\\
\frac{\rho}{2c} & 0 & 0 & \frac{1}{2c^{2}} & 0\end{array}\right],\\
\Lambda\mathsf{S}^{-1} & = & \left[\begin{array}{ccccc}
0 & u & 0 & 0 & 0\\
0 & 0 & u & -\frac{u}{c^{2}} & 0\\
0 & 0 & 0 & 0 & u\\
\frac{\rho\left(u+c\right)}{2c} & 0 & 0 & \frac{u+c}{2c^{2}} & 0\\
-\frac{\rho\left(u+c\right)}{2c} & 0 & 0 & \frac{u-c}{2c^{2}} & 0\end{array}\right].\end{eqnarray*}

\end_inset

 Therefore, 
\begin_inset Formula \begin{eqnarray*}
\mathfrak{L}=\Lambda\mathsf{S}^{-1}\frac{\partial\mathsf{U}}{\partial x} & = & \left[\begin{array}{c}
u\frac{\partial v}{\partial x}\\
u\left(\frac{\partial\rho}{\partial x}-\frac{1}{c^{2}}\frac{\partial p}{\partial x}\right)\\
u\frac{\partial Y_{i}}{\partial x}\\
\frac{\left(u+c\right)}{2}\left(\frac{\rho}{c}\frac{\partial u}{\partial x}+\frac{1}{c^{2}}\frac{\partial p}{\partial x}\right)\\
\frac{\left(u-c\right)}{2}\left(-\frac{\rho}{c}\frac{\partial u}{\partial x}+\frac{1}{c^{2}}\frac{\partial p}{\partial x}\right)\end{array}\right],\\
\left(\mathsf{S}^{-1}\right)\frac{\partial\mathsf{U}}{\partial t} & = & \left[\begin{array}{c}
\frac{\partial v}{\partial t}\\
\frac{\partial\rho}{\partial t}-\frac{1}{c^{2}}\frac{\partial p}{\partial t}\\
\frac{\partial Y_{i}}{\partial t}\\
\frac{1}{2c}\left(\rho\frac{\partial u}{\partial t}+\frac{1}{c}\frac{\partial p}{\partial t}\right)\\
\frac{1}{2c}\left(-\rho\frac{\partial u}{\partial t}+\frac{1}{c}\frac{\partial p}{\partial t}\right)\end{array}\right].\end{eqnarray*}

\end_inset

 Alternatively, if we write 
\begin_inset Formula \[
\mathsf{S}=\left[\begin{array}{ccccc}
0 & 0 & 0 & \frac{1}{\rho} & -\frac{1}{\rho}\\
1 & 0 & 0 & 0 & 0\\
0 & 1 & 0 & 1 & 1\\
0 & 0 & 0 & c^{2} & c^{2}\\
0 & 0 & 1 & 0 & 0\end{array}\right],\]

\end_inset

 then 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\noun off
\color none

\begin_inset Formula \[
\mathsf{S}^{-1}=\left[\begin{array}{ccccc}
0 & 1 & 0 & 0 & 0\\
0 & 0 & 1 & -\frac{1}{c^{2}} & 0\\
0 & 0 & 0 & 0 & 1\\
\frac{\rho}{2} & 0 & 0 & \frac{1}{2c^{2}} & 0\\
\frac{\rho}{2} & 0 & 0 & \frac{1}{2c^{2}} & 0\end{array}\right],\quad\Lambda\mathsf{S}^{-1}=\left[\begin{array}{ccccc}
0 & u & 0 & 0 & 0\\
0 & 0 & u & -\frac{u}{c^{2}} & 0\\
0 & 0 & 0 & 0 & u\\
\frac{\rho\left(u+c\right)}{2} & 0 & 0 & \frac{u+c}{2c^{2}} & 0\\
-\frac{\rho\left(u+c\right)}{2} & 0 & 0 & \frac{u-c}{2c^{2}} & 0\end{array}\right]\]

\end_inset

 and 
\begin_inset Formula \[
\mathfrak{L}=\Lambda\mathsf{S}^{-1}\frac{\partial\mathsf{U}}{\partial x}=\left[\begin{array}{c}
u\frac{\partial v}{\partial x}\\
u\left(\frac{\partial\rho}{\partial x}-\frac{1}{c^{2}}\frac{\partial p}{\partial x}\right)\\
u\frac{\partial Y_{i}}{\partial x}\\
\frac{\left(u+c\right)}{2}\left(\rho\frac{\partial u}{\partial x}+\frac{1}{c^{2}}\frac{\partial p}{\partial x}\right)\\
\frac{\left(u-c\right)}{2}\left(-\rho\frac{\partial u}{\partial x}+\frac{1}{c^{2}}\frac{\partial p}{\partial x}\right)\end{array}\right],\quad\left(\mathsf{S}^{-1}\right)\frac{\partial\mathsf{U}}{\partial t}=\left[\begin{array}{c}
\frac{\partial v}{\partial t}\\
\frac{\partial\rho}{\partial t}-\frac{1}{c^{2}}\frac{\partial p}{\partial t}\\
\frac{\partial Y_{i}}{\partial t}\\
\frac{1}{2}\left(\rho\frac{\partial u}{\partial t}+\frac{1}{c^{2}}\frac{\partial p}{\partial t}\right)\\
\frac{1}{2}\left(-\rho\frac{\partial u}{\partial t}+\frac{1}{c^{2}}\frac{\partial p}{\partial t}\right)\end{array}\right].\]

\end_inset


\end_layout

\end_body
\end_document
