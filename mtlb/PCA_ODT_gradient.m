function x_grad = PCA_ODT_gradient (x, npt, nt, dx)
x_grad = zeros(npt,nt);
for i=1:nt
    for j=2:npt+1
        x_grad(j-1,i)= (x(j,i) - x(j-1,i)) / dx;
    end
end
