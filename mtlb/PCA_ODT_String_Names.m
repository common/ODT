function names=PCA_ODT_String_Names(var)

switch var
    case 'state_variables'
        names={'T','species_0','species_1','species_2','species_3',...
            'species_4','species_5','species_6','species_7',...
            'species_8','species_9','species_10'};
    case 'species'
        names={'species_0','species_1','species_2','species_3',...
            'species_4','species_5','species_6','species_7',...
            'species_8','species_9','species_10'};
    case 'SSVnames'
        names ={'T','H2','O2','O','OH','H2O','H','HO2','CO','CO2','HCO','N2'};
    case 'properties'
        names={'density','thermal_conductivity','viscosity', 'heat_capacity'};
    case 'FlVars'
        names={'mixturefraction','scalardissipation'};
    case 'StVars'
        names={'StateVar_0','StateVar_1','StateVar_2','StateVar_3',...
            'StateVar_4','StateVar_5','StateVar_6','StateVar_7',...
            'StateVar_8','StateVar_9','StateVar_10'};
    case '2PCs'
        names={'PC_0','PC_1'};        
    case '3PCs'
        names={'PC_0','PC_1','PC_2'};  
    case '2pcSrcs'
        names={'PCSourceTerm_0','PCSourceTerm_1'};  
    case '3pcSrcs'
        names={'PCSourceTerm_0','PCSourceTerm_1','PCSourceTerm_2'};  
    otherwise
        error('Wrong Input');
end

