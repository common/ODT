classdef ODTEnsemble < handle
  
  % The ODTEnsemble class provides a convenient way to deal with multiple
  % ODT simulations, each of which may be varying in time. 
  %
  % AUTHOR: James C. Sutherland
  % DATE: 2010

  properties( SetAccess=private, GetAccess=private )
    dbnames    = {};     % the names of the ODT databases that comprise this ensemble
    subdirs    = {};     % the active list of entries (times) from the database.
    fieldNames = {};     % the list of fields
    databases  = {};     % the databases corresponding to each ODT database requested
  end
    
  methods( Access=public )
    
    function self = ODTEnsemble( varargin )
      % Construct an ODTEnsemble object
      %
      % Examples:
      %   ds = ODTEnsemble;
      %
      %   ds = ODTEnsemble( databases );
      switch nargin
        case 0
          self.dbnames = ODTEnsemble.select_databases();
        case 1
          dbnames = varargin{1};
          if( ~iscell(dbnames) ) 
            self.dbnames{1} = dbnames;
          else
            self.dbnames = dbnames;
          end
        otherwise
          error('Invalid use of ODTEnsemble');
      end

      dbtmp = Database( self.dbnames{1} );
      
      self.fieldNames  = dbtmp.get_field_names();
      self.subdirs     = dbtmp.get_entry_names();
      
      for i=1:length(self.dbnames)
        db = Database( self.dbnames{i} );
        if( ~isempty( setdiff( db.get_entry_names(), self.subdirs ) ) )
          error( 'Database %s has different entries than %s\n',self.dbnames{1},self.dbnames{i} );
        end
        if( ~isempty( setdiff( db.get_field_names(), self.fieldNames ) ) )
          error( 'Database %s has different fields than %s',self.dbnames{1},self.dbnames{i} );
        end
        self.databases{i} = db;
      end
          
    end
    
    function sd = get_times(self)
      sd = self.subdirs;
    end
    
    function db = get_database_names(self)
      db = self.dbnames;
    end
    
    function d = extract_var( obj, varname, times )
      % d = obj.extract_var( varname, times )
      %
      % Extract the given variable at the requested time(s).
      %
      % INPUT:
      %   varname - the name of the variable to extract
      %   time    - the time(s) at which to extract the variable.
      %
      % OUTPUT:
      %   d - [npts,ntimes,ndatabases]  array containing the requested variable
      %
      if ~iscell(times) 
        times{1} = times;
      end
      tmp = obj.databases{1}.get_field_data(times{1},varname);
      npts = length(tmp);
      nt = length(times);
      clear tmp;
      nf = length(obj.dbnames);
      d = zeros(npts,nt,nf);
      for i=1:nf
        for it=1:nt
          d(:,it,i) = obj.databases{i}.get_field_data(times{it},varname);
        end
      end
    end
    
    function meanVal = ensemble_mean( obj, varname, times )
      % meanVal = obj.ensemble_mean( varname, time )
      % calculate the ensemble mean profile for the requested variable.
      %
      % INPUTS:
      %  varname - the name of the variable that we want the mean for.
      %  times   - the time(s) that we want to consider in calculating the mean.
      %
      % OUTPUTS:
      %  meanVal - the spatially-resolved ensemble mean calculated using
      %            all of the ODT data files at the requested time(s)
      %
      % Example:
      %  % calculate the mean at the first time snapshot
      %  Tmean = obj.mean('T', obj.subdirs(1));
      if ~iscell(times) 
        times{1} = times;
      end
      tmp = obj.databases{1}.get_field_data(times{1},varname);
      nn = 0;
      meanVal = 0*tmp;
      for i=1:length(obj.databases)
        for it=1:length(times)
          d = obj.databases{i}.get_field_data( times{it}, varname );
          meanVal = meanVal + d;
          nn = nn+1;
        end
      end
      meanVal = meanVal / nn;
    end
    
    function [rmsVal,meanVal] = ensemble_rms( obj, varname, times )
      % [rmsval,meanval] = obj.ensemble_rms( varname, times )
      % calculate the rms (and mean) of a variable at the requested time
      %
      % INPUTS:
      %  varname - the name of the variable to calculate statistics of
      %  times   - the time(s) at which we should calculate the rms.  All
      %            times will be combined in calculating the rms.
      %
      % OUTPUTS:
      %   rmsval  - the RMS of the requested variable (resolved in space)
      %   meanval - the mean of the requested variable (resolved in space)
      %
      % Examples:
      %   rms = obj.ensemble_rms( 'T', obj.subdirs(1) );  % calculate T rms at first time entry.
      %   [rms,avg] = obj.ensemble_rms('T',obj.subdirs(1:2) ); % mean and rms using data from first 2 times.
      %
      % see also ensemble_mean
      if ~iscell(times)
        times{1} = times;
      end
      
      meanVal = obj.ensemble_mean(varname,times);
      
      nn=0;
      nf = length( obj.databases );
      rmsVal = 0*meanVal;
      for i=1:nf
        for it=1:length(times)
          d = obj.databases{i}.get_field_data( times{it}, varname );
          rmsVal  = rmsVal + (meanVal - d).^2;
          nn = nn + length(d);
        end
      end
      rmsVal  = sqrt( rmsVal/nn );
    end
    
  
    function [pdf,xbins] = conditional_pdf( obj, varname, cvarname, cvarLo, cvarHi, bins, times )
      % calculate the conditional pdf of a variable
      %
      % INPUTS:
      %   varname  - the name of the variable that we want the PDF for.
      %   cvarname - the conditioning variable
      %   cvarLo   - the lower bound for the conditioning variable
      %   cvarHi   - the upper bound for the conditioning variable
      %   bins     - the bins to use in calculating the PDF
      %   times    - the times that we want to consider in constructing the
      %              conditional PDF.
      %
      %  OUTPUT:
      %   pdf - the conditional pdf
      %   xbins - the locations where the PDF is constructed.
      %
      % Example:
      %  Tpdf = obj.construct_pdf('T','mixturefraction',0.4,0.5,linspace(500,2000,20),obj.times );
      %
      % see also pdf

      %       nf = length( obj.dbnames );
      %       nt = length(times);
      %       npts = obj.dbs{i}.npts( varname );
      
      c = obj.extract_var(cvarname,times);
      d = obj.extract_var( varname,times);
      
      ix = ( c>=cvarLo & c<=cvarHi );
      [pdf,xbins] = ODTEnsemble.pdf( d(ix), bins );
    end
    
  end

  methods( Static )
    
    function db = select_databases()
      db = uipickfiles('Prompt','Select ODT Databases','Output','cell');
    end
    
    function [P,xbins] = pdf( y, varargin )
      % [P,xbins] = pdf( y, bins, lo, hi )
      %
      % Calculate the PDF of a variable
      %
      % Input:
      %   y    - [n] function values
      %   bins - OPTIONAL location of bin edges for discrete PDF
      %   lo   - OPTIONAL low bound for pdf
      %   hi   - OPTIONAL high bound for PDF
      %
      % Output:
      %   P     - PDF of y
      %   xbins - values at which discrete PDF is held
      %
      % see also conditional_pdf
      
      nbins = ceil( length(y)/100.0 );
      lo = min(y);
      hi = max(y);
      
      if(nargin>2)  lo   = varargin{2}; end
      if(nargin>3)  hi   = varargin{3}; end
      if(nargin>1)
        xbins = varargin{1};
      else
        xbins = linspace(lo,hi,nbins);
      end
      
      [n,xbins] = hist(y,xbins);
      
      P=n/sum(n);   % normalize to get a PDF
    end

  end
end