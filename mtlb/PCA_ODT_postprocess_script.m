clear;
clc;

base_dir_name='/scratch/ibrix/icse/amir/ODT_PCA_outputs';
dir_name_ext='/fromCaseM_NaveenOld_2pc/slfm_trained';
StateVars=PCA_ODT_String_Names('state_variables');
FlVars=PCA_ODT_String_Names('FlVars');
loadbigdata=true;
realN=128;
runN=2;
run_fields_name={'pc_SA','species'};
for j=1:runN
    run_dir_name=[base_dir_name,dir_name_ext,'/Run',num2str(j)];
    db_dir_name=[run_dir_name,'/Real1/',run_fields_name{j},'.db'];
    display(['Reading data from run ',num2str(j),', realization 1 ...']);
    sumVars=PCA_ODT_Data_extractor(db_dir_name,[StateVars,FlVars]);
    if (loadbigdata)
        bigdata_2=zeros(size(sumVars,1)*realN,size(sumVars,2),size(sumVars,3));
        bigdata_2(1:size(sumVars,1),:,:)=sumVars;
    end;
    for i=2:realN
        display(['Reading data from run ',num2str(j),', realization ',num2str(i),' ...']);
        db_dir_name=[run_dir_name,'/Real',num2str(i),'/',run_fields_name{j},'.db'];
        tempVars=PCA_ODT_Data_extractor(db_dir_name,[StateVars,FlVars]);
        sumVars=sumVars+tempVars;
        if (loadbigdata)
            bigdata_2(size(sumVars,1)*(i-1)+1:size(sumVars,1)*i,:,:)=tempVars;
        end
    end
    if (j==1)
        meanVars_1=sumVars/realN;
        bigdata_1=bigdata_2;
    else
        meanVars_2=sumVars/realN;
    end
end
meanSSVs_1=meanVars_1(:,:,1:12);
meanFlVs_1=meanVars_1(:,:,13:14);
meanSSVs_2=meanVars_2(:,:,1:12);
meanFlVs_2=meanVars_2(:,:,13:14);

stdSSVs_1=PCA_ODT_Std(bigdata_1,size(sumVars,1));
stdSSVs_2=PCA_ODT_Std(bigdata_2,size(sumVars,1));
stdFlVs_1=stdSSVs_1(:,:,13:14);
stdFlVs_2=stdSSVs_2(:,:,13:14);
stdSSVs_1=stdSSVs_1(:,:,1:12);
stdSSVs_2=stdSSVs_2(:,:,1:12);
clear temp_SSVs sum_SSVs meanVars_1 meanVars_2
     
nSec=20;
for i=1:size(bigdata_1,3)
    condMeanSSVs_1(:,:,i)=PCA_ODT_CondMean(bigdata_1(:,:,i),bigdata_1(:,:,13),0,1,nSec);
    condMeanSSVs_2(:,:,i)=PCA_ODT_CondMean(bigdata_2(:,:,i),bigdata_2(:,:,13),0,1,nSec);
    condStdSSVs_1(:,:,i)=PCA_ODT_CondStd(bigdata_1(:,:,i),bigdata_1(:,:,13),0,1,nSec);
    condStdSSVs_2(:,:,i)=PCA_ODT_CondStd(bigdata_2(:,:,i),bigdata_2(:,:,13),0,1,nSec);
%     condRmsSSVs_1(:,:,i)=PCA_ODT_CondRms(bigdata_1(:,:,i),bigdata_1(:,:,13),0,1,nSec);
%     condRmsSSVs_2(:,:,i)=PCA_ODT_CondRms(bigdata_2(:,:,i),bigdata_2(:,:,13),0,1,nSec);
end
%% analysing plots
plot_base_name='slfm_SA_2pc';
location='/scratch/ibrix/icse/amir/ODT_PCA_outputs/plots/';
location_ext='fromCaseM_NaveenOld_2pc/slfm_trained/';
location=[location,location_ext];
SSVnames=PCA_ODT_String_Names('SSVnames');
% contour plots
for i=1:length(SSVnames)
    fig=figure;contourf(meanSSVs_1(:,:,i)',10);
    cmin=min([min(min(meanSSVs_1(:,:,i))),min(min(meanSSVs_2(:,:,i)))]);
    cmax=max([max(max(meanSSVs_1(:,:,i))),max(max(meanSSVs_2(:,:,i)))]);
    if (i==1)
        title([SSVnames{i},'[K] (from tranported PCs)']);
    else
        title(['Y_{',SSVnames{i},'} (from tranported PCs)']);
    end
    caxis([cmin,cmax]);
    colorbar;
    print(fig,'-depsc2',[location,plot_base_name,'_pca_',SSVnames{i}]);

    fig=figure;contourf(meanSSVs_2(:,:,i)',10);
    if (i==1)
        title([SSVnames{i},'[K] (full chemistry)']);
    else
        title(['Y_{',SSVnames{i},'} (full chemistry)']);
    end
    caxis([cmin,cmax]);
    colorbar;
    print(fig,'-depsc2',[location,plot_base_name,'_fullchem_',SSVnames{i}]);
end
close all

% mixture fraction conditional mean plots
fpts=linspace(1/nSec/2,1-1/nSec/2,nSec);
for i=1:length(SSVnames)
    fig=figure;
    plot(fpts, condMeanSSVs_1(:,10,i)','b--',...
        fpts, condMeanSSVs_2(:,10,i)','b-',...
        fpts, condMeanSSVs_1(:,25,i)','r--',...
        fpts, condMeanSSVs_2(:,25,i)','r-',...
        fpts, condMeanSSVs_1(:,52,i)','g--',...
        fpts, condMeanSSVs_2(:,52,i)','g-');

    if (i==1)
        ylabel([SSVnames{i},'[K]']);
    else
        ylabel(['Y_{',SSVnames{i},'}']);
    end
    xlabel('f');
    title(['Conditional mean of ',SSVnames{i}]);
    axis tight;
    legend1=legend('PCA-ts=10','ODT-ts=10','PCA-ts=25','ODT-ts=25','PCA-ts=52','ODT-ts=52');
    print(fig,'-depsc2',[location,plot_base_name,'_CondMean_',SSVnames{i}]);
end
close all

% mixture fraction conditional std plots
fpts=linspace(1/nSec/2,1-1/nSec/2,nSec);
for i=1:length(SSVnames)
    fig=figure;
    plot(fpts, condStdSSVs_1(:,10,i)','b--',...
        fpts, condStdSSVs_2(:,10,i)','b-',...
        fpts, condStdSSVs_1(:,25,i)','r--',...
        fpts, condStdSSVs_2(:,25,i)','r-',...
        fpts, condStdSSVs_1(:,52,i)','g--',...
        fpts, condStdSSVs_2(:,52,i)','g-');

    if (i==1)
        ylabel([SSVnames{i},'[K]']);
    else
        ylabel(['Y_{',SSVnames{i},'}']);
    end
    xlabel('f');
    title(['Conditional STD of ',SSVnames{i}]);
    axis tight;
    legend1=legend('PCA-ts=10','ODT-ts=10','PCA-ts=25','ODT-ts=25','PCA-ts=52','ODT-ts=52');
    print(fig,'-depsc2',[location,plot_base_name,'_CondStd_',SSVnames{i}]);
end
close all

%% ensemble averaged lines plots
x=-(0.013425/896)/2:(0.013425/896):0.013425+(0.013425/896)/2;

for i=1:length(SSVnames)
    fig=figure;
    plot(x, meanSSVs_1(:,10,i)','b--',...
        x, meanSSVs_2(:,10,i)','b-',...
        x, meanSSVs_1(:,25,i)','r--',...
        x, meanSSVs_2(:,25,i)','r-',...
        x, meanSSVs_1(:,52,i)','g--',...
        x, meanSSVs_2(:,52,i)','g-');

    if (i==1)
        ylabel([SSVnames{i},'[K]']);
    else
        ylabel(['Y_{',SSVnames{i},'}']);
    end
    xlabel('f');
    title(['ensemble average of ',SSVnames{i}]);
    axis tight;
    legend1=legend('PCA-ts=10','ODT-ts=10','PCA-ts=25','ODT-ts=25','PCA-ts=52','ODT-ts=52');
    print(fig,'-depsc2',[location,plot_base_name,'_mean_',SSVnames{i}]);
end
close all

% ensemble std lines plots
x=-(0.013425/896)/2:(0.013425/896):0.013425+(0.013425/896)/2;

for i=1:length(SSVnames)
    fig=figure;
    plot(x, stdSSVs_1(:,10,i)','b--',...
        x, stdSSVs_2(:,10,i)','b-',...
        x, stdSSVs_1(:,25,i)','r--',...
        x, stdSSVs_2(:,25,i)','r-',...
        x, stdSSVs_1(:,52,i)','g--',...
        x, stdSSVs_2(:,52,i)','g-');

    if (i==1)
        ylabel([SSVnames{i},'[K]']);
    else
        ylabel(['Y_{',SSVnames{i},'}']);
    end
    xlabel('f');
    title(['ensemble average of ',SSVnames{i}]);
    axis tight;
    legend1=legend('PCA-ts=10','ODT-ts=10','PCA-ts=25','ODT-ts=25','PCA-ts=52','ODT-ts=52');
    print(fig,'-depsc2',[location,plot_base_name,'_std_',SSVnames{i}]);
end
close all
%%
% mixture fraction conditional rms plots
fpts=linspace(1/nSec/2,1-1/nSec/2,nSec);
for i=1:length(SSVnames)
    fig=figure;
    plot(fpts, condRmsSSVs_1(:,10,i)','b--',...
        fpts, condRmsSSVs_2(:,10,i)','b-',...
        fpts, condRmsSSVs_1(:,25,i)','r--',...
        fpts, condRmsSSVs_2(:,25,i)','r-',...
        fpts, condRmsSSVs_1(:,52,i)','g--',...
        fpts, condRmsSSVs_2(:,52,i)','g-');

    if (i==1)
        ylabel([SSVnames{i},'[K]']);
    else
        ylabel(['Y_{',SSVnames{i},'}']);
    end
    xlabel('f');
    title(['Conditional RMS of ',SSVnames{i}]);
    axis tight;
    legend1=legend('PCA-ts=10','ODT-ts=10','PCA-ts=25','ODT-ts=25','PCA-ts=52','ODT-ts=52');
    print(fig,'-depsc2',[location,plot_base_name,'_CondRms_',SSVnames{i}]);
end
close all
%%
x=-(0.013425/896)/2:(0.013425/896):0.013425+(0.013425/896)/2;
% conditional ensemble averaged plots
for i=1:length(SSVnames)
    fig=figure;
    plot(meanFlVs_1(:,10,1), meanSSVs_1(:,10,i)','b--',...
        meanFlVs_2(:,10,1), meanSSVs_2(:,10,i)','b-',...
        meanFlVs_1(:,25,1), meanSSVs_1(:,25,i)','r--',...
        meanFlVs_2(:,25,1), meanSSVs_2(:,25,i)','r-',...
        meanFlVs_1(:,52,1), meanSSVs_1(:,52,i)','g--',...
        meanFlVs_2(:,52,1), meanSSVs_2(:,52,i)','gr-');

    if (i==1)
        ylabel([SSVnames{i},'[K]']);
    else
        ylabel(['Y_{',SSVnames{i},'}']);
    end
    xlabel('f');
    title(['Conditional mean of ',SSVnames{i}]);
    axis tight;
    legend1=legend('PCA-ts=10','ODT-ts=10','PCA-ts=25','ODT-ts=25','PCA-ts=52','ODT-ts=52');
    print(fig,'-depsc2',[location,plot_base_name,'_CondMean_',SSVnames{i}]);
end
close all

