clear; clc; %close all;

nx = 100;
% x = sort( rand(nx,1) );
% x(1) = 0.0;
% x(nx) = 1.0;
%x = linspace(0,1,nx);
x = set_mesh(nx);
%phi = sin(5*x);
%phi = 5*x-1;
phi = linspace(0,0.99,nx);

fprintf('Conservation error:\n');
fprintf('Discrete\tContinuous\n-------------------------\n');

n = 30;
err = zeros(n,1);
errc=err;
for i=1:n
  okay = false;
  while ~okay
    xo = rand(1);
    L  = rand(1);
    if( xo+L < x(nx) & xo-L>0)
      okay = true;
    end
  end
%   xo = 0.51; L=0.205;
  phinewc = transform( phi, x, xo, L, 'function' );
%  phinew  = transform( phi, x, xo, L, '' );

%   % this is not right.  The new coordinates need to be determined...
%   xnew    = transform( x,   x, xo, L, '' );
%   I = trapz(x,phi);
%   err(i)  = (I-trapz(xnew,phinew))/I;
%   errc(i) = (I-trapz(x,phinewc))/I;
%   fprintf('%10.1e\t%10.1e\n',100*err(i),100*errc(i));
%   plot(x,phi,'k-',x,phinew,'r.-',x,phinewc,'bs:');
%   legend('original','discrete','continuous','Location','Best');
%   pause(0.2);
end

% fprintf('\naverage error:\n%10.1e\t%10.1e\n',mean(err),mean(errc));
% fprintf('\nstd deviation:\n%10.1e\t%10.1e\n',std(err),std(errc));
