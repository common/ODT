ds = ODTDataSet;   % interactively define the dataset
subset = ds.subdirs;
odt_Srcs(:,:,1) = ds.extract_var( 'Temperature_Source', subset );
odt_Srcs(:,:,2) = ds.extract_var( 'SpeciesSourceTerm_0', subset );
odt_Srcs(:,:,3) = ds.extract_var( 'SpeciesSourceTerm_1', subset );
odt_Srcs(:,:,4) = ds.extract_var( 'SpeciesSourceTerm_2', subset );
odt_Srcs(:,:,5) = ds.extract_var( 'SpeciesSourceTerm_3', subset );
odt_Srcs(:,:,6) = ds.extract_var( 'SpeciesSourceTerm_4', subset );
odt_Srcs(:,:,7) = ds.extract_var( 'SpeciesSourceTerm_5', subset );
odt_Srcs(:,:,8) = ds.extract_var( 'SpeciesSourceTerm_6', subset );
odt_Srcs(:,:,9) = ds.extract_var( 'SpeciesSourceTerm_7', subset );
odt_Srcs(:,:,10) = ds.extract_var( 'SpeciesSourceTerm_8', subset );
odt_Srcs(:,:,11) = ds.extract_var( 'SpeciesSourceTerm_9', subset );

odt_Diffs(:,:,1) = ds.extract_var( 'SpeciesDiffusionFlux_0', subset );
odt_Diffs(:,:,2) = ds.extract_var( 'SpeciesDiffusionFlux_1', subset );
odt_Diffs(:,:,3) = ds.extract_var( 'SpeciesDiffusionFlux_2', subset );
odt_Diffs(:,:,4) = ds.extract_var( 'SpeciesDiffusionFlux_3', subset );
odt_Diffs(:,:,5) = ds.extract_var( 'SpeciesDiffusionFlux_4', subset );
odt_Diffs(:,:,6) = ds.extract_var( 'SpeciesDiffusionFlux_5', subset );
odt_Diffs(:,:,7) = ds.extract_var( 'SpeciesDiffusionFlux_6', subset );
odt_Diffs(:,:,8) = ds.extract_var( 'SpeciesDiffusionFlux_7', subset );
odt_Diffs(:,:,9) = ds.extract_var( 'SpeciesDiffusionFlux_8', subset );
odt_Diffs(:,:,10) = ds.extract_var( 'SpeciesDiffusionFlux_9', subset );
odt_Diffs(:,:,11) = ds.extract_var( 'SpeciesDiffusionFlux_10', subset );

sv_DiffFlux(:,:,1) = ds.extract_var( 'heatflux', subset );
sv_DiffFlux(:,:,2:11)=odt_Diffs(:,:,1:10);
% sv_DiffFlux(:,:,2) = ds.extract_var( 'SpeciesDiffusionFlux_0', subset );
% sv_DiffFlux(:,:,3) = ds.extract_var( 'SpeciesDiffusionFlux_1', subset );
% sv_DiffFlux(:,:,4) = ds.extract_var( 'SpeciesDiffusionFlux_2', subset );
% sv_DiffFlux(:,:,5) = ds.extract_var( 'SpeciesDiffusionFlux_3', subset );
% sv_DiffFlux(:,:,6) = ds.extract_var( 'SpeciesDiffusionFlux_4', subset );
% sv_DiffFlux(:,:,7) = ds.extract_var( 'SpeciesDiffusionFlux_5', subset );
% sv_DiffFlux(:,:,8) = ds.extract_var( 'SpeciesDiffusionFlux_6', subset );
% sv_DiffFlux(:,:,9) = ds.extract_var( 'SpeciesDiffusionFlux_7', subset );
% sv_DiffFlux(:,:,10) = ds.extract_var( 'SpeciesDiffusionFlux_8', subset );
% sv_DiffFlux(:,:,11) = ds.extract_var( 'SpeciesDiffusionFlux_9', subset );
Cp(:,:) = ds.extract_var( 'heat_capacity', subset );
lambda(:,:) = ds.extract_var( 'thermal_conductivity', subset );
T(:,:) = ds.extract_var( 'T', subset );

% sv_Diffs(:,:,1) = ds.extract_var( 'StateVar_Diff_Term_0', subset );
% sv_Diffs(:,:,2) = ds.extract_var( 'StateVar_Diff_Term_1', subset );
% sv_Diffs(:,:,3) = ds.extract_var( 'StateVar_Diff_Term_2', subset );
% sv_Diffs(:,:,4) = ds.extract_var( 'StateVar_Diff_Term_3', subset );
% sv_Diffs(:,:,5) = ds.extract_var( 'StateVar_Diff_Term_4', subset );
% sv_Diffs(:,:,6) = ds.extract_var( 'StateVar_Diff_Term_5', subset );
% sv_Diffs(:,:,7) = ds.extract_var( 'StateVar_Diff_Term_6', subset );
% sv_Diffs(:,:,8) = ds.extract_var( 'StateVar_Diff_Term_7', subset );
% sv_Diffs(:,:,9) = ds.extract_var( 'StateVar_Diff_Term_8', subset );
% sv_Diffs(:,:,10) = ds.extract_var( 'StateVar_Diff_Term_9', subset );
% sv_Diffs(:,:,11) = ds.extract_var( 'StateVar_Diff_Term_10', subset );

PCs(:,:,1) = ds.extract_var( 'PC_0', subset );
PCs(:,:,2) = ds.extract_var( 'PC_1', subset );
% PCs(:,:,3) = ds.extract_var( 'PC_2', subset );
total_Spcs(:,:,1) = ds.extract_var( 'PCSourceTerm_0', subset );
total_Spcs(:,:,2) = ds.extract_var( 'PCSourceTerm_1', subset );
% total_Spcs(:,:,3) = ds.extract_var( 'PCSourceTerm_2', subset );
% PC_Diffs(:,:,1) = ds.extract_var( 'PC_Diffusive_Term_0', subset );
% PC_Diffs(:,:,2) = ds.extract_var( 'PC_Diffusive_Term_1', subset );
% PC_Diffs(:,:,3) = ds.extract_var( 'PC_Diffusive_Term_2', subset );
PC_Diffs(:,:,1) = ds.extract_var( 'PCDiffusionFlux_0', subset );
PC_Diffs(:,:,2) = ds.extract_var( 'PCDiffusionFlux_1', subset );
% PC_Diffs(:,:,3) = ds.extract_var( 'PCDiffusionFlux_2', subset );
% Spcs=total_Spcs-PC_Diffs;
Spcs=total_Spcs;
Ys(:,:,1) = ds.extract_var( 'species_0', subset );
Ys(:,:,2) = ds.extract_var( 'species_1', subset );
Ys(:,:,3) = ds.extract_var( 'species_2', subset );
Ys(:,:,4) = ds.extract_var( 'species_3', subset );
Ys(:,:,5) = ds.extract_var( 'species_4', subset );
Ys(:,:,6) = ds.extract_var( 'species_5', subset );
Ys(:,:,7) = ds.extract_var( 'species_6', subset );
Ys(:,:,8) = ds.extract_var( 'species_7', subset );
Ys(:,:,9) = ds.extract_var( 'species_8', subset );
Ys(:,:,10) = ds.extract_var( 'species_9', subset );
Ys(:,:,11) = ds.extract_var( 'species_10', subset );
odt_SSVs(:,:,1) = ds.extract_var( 'T', subset );
odt_SSVs(:,:,2:11)=Ys(:,:,1:10);

pc_SSVs(:,:,1) = ds.extract_var( 'StateVar_0', subset );
pc_SSVs(:,:,2) = ds.extract_var( 'StateVar_1', subset );
pc_SSVs(:,:,3) = ds.extract_var( 'StateVar_2', subset );
pc_SSVs(:,:,4) = ds.extract_var( 'StateVar_3', subset );
pc_SSVs(:,:,5) = ds.extract_var( 'StateVar_4', subset );
pc_SSVs(:,:,6) = ds.extract_var( 'StateVar_5', subset );
pc_SSVs(:,:,7) = ds.extract_var( 'StateVar_6', subset );
pc_SSVs(:,:,8) = ds.extract_var( 'StateVar_7', subset );
pc_SSVs(:,:,9) = ds.extract_var( 'StateVar_8', subset );
pc_SSVs(:,:,10) = ds.extract_var( 'StateVar_9', subset );
pc_SSVs(:,:,11) = ds.extract_var( 'StateVar_10', subset );

% h(:,:,1) = ds.extract_var( 'enthalpy_0', subset );
% h(:,:,2) = ds.extract_var( 'enthalpy_1', subset );
% h(:,:,3) = ds.extract_var( 'enthalpy_2', subset );
% h(:,:,4) = ds.extract_var( 'enthalpy_3', subset );
% h(:,:,5) = ds.extract_var( 'enthalpy_4', subset );
% h(:,:,6) = ds.extract_var( 'enthalpy_5', subset );
% h(:,:,7) = ds.extract_var( 'enthalpy_6', subset );
% h(:,:,8) = ds.extract_var( 'enthalpy_7', subset );
% h(:,:,9) = ds.extract_var( 'enthalpy_8', subset );
% h(:,:,10) = ds.extract_var( 'enthalpy_9', subset );
% h(:,:,11) = ds.extract_var( 'enthalpy_10', subset );

RPCs(:,:,1) = ds.extract_var( 'rhoPC_0', subset );
RPCs(:,:,2) = ds.extract_var( 'rhoPC_1', subset );
% RPCs(:,:,3) = ds.extract_var( 'rhoPC_2', subset );
%%


figure;contourf(etas(:,:,1)',7);figure;contourf(PCs(:,:,1)',7);
figure;contourf(etas(:,:,2)',7);figure;contourf(PCs(:,:,2)',7);

figure;plot(PCs(:,10,1),'b.');hold;plot(etas(:,10,1),'g.');
xlabel('x');ylabel('PC1 - ts=10');
legend('transported','extracted')
figure;plot(PCs(:,20,1),'b.');hold;plot(etas(:,20,1),'g.');
xlabel('x');ylabel('PC1 - ts=20');
legend('transported','extracted')
figure;plot(PCs(:,100,1),'b.');hold;plot(etas(:,100,1),'g.');
xlabel('x');ylabel('PC1 - ts=100');
legend('transported','extracted')
figure;plot(PCs(:,200,1),'b.');hold;plot(etas(:,200,1),'g.');
xlabel('x');ylabel('PC1 - ts=200');
legend('transported','extracted')
figure;plot(PCs(:,400,1),'b.');hold;plot(etas(:,400,1),'g.');
xlabel('x');ylabel('PC1 - ts=400');
legend('transported','extracted')
figure;plot(PCs(:,600,1),'b.');hold;plot(etas(:,600,1),'g.');
xlabel('x');ylabel('PC1 - ts=600');
legend('transported','extracted')
figure;plot(PCs(:,800,1),'b.');hold;plot(etas(:,800,1),'g.');
xlabel('x');ylabel('PC1 - ts=800');
legend('transported','extracted')
figure;plot(PCs(:,1001,1),'b.');hold;plot(etas(:,1001,1),'g.');
xlabel('x');ylabel('PC1 - ts=1001');
legend('transported','extracted')

figure;plot(PCs(:,500,2),'b.');hold;plot(etas(:,500,2),'g.');
xlabel('x');ylabel('PC2 - ts=500');
legend('transported','extracted')
figure;plot(PCs(:,1001,2),'b.');hold;plot(etas(:,1001,2),'g.');
xlabel('x');ylabel('PC2 - ts=1001');
legend('transported','extracted')

figure;scatter(eta_r(1:10:end,1),eta_r(1:10:end,2),'g.');
hold;
scatter(XPCs(1:10:end,1),XPCs(1:10:end,2),10,XSpcs(1:10:end,1),'Filled')
xlabel('PC1');ylabel('PC2');
legend('extracted manifold','transported manifold')

figure;scatter(eta_r(1:10:end,1),eta_r(1:10:end,2),'g.');
hold;
scatter(eta_slfm(1:2:end,1),eta_slfm(1:2:end,2),10,Seta_slfm(1:2:end,1),'Filled')
xlabel('PC1');ylabel('PC2');
legend('extracted manifold','slfm manifold')
