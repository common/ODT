function x = set_mesh( n )
coord = 0.0;
x = zeros(n,1);
cellsize = 0.01;

for i=0:n-1
  x(i+1) = coord;
  if( i~=0 & mod(i,2)==0 )
    coord = coord + cellsize;
  end
  coord = coord + cellsize;
end
