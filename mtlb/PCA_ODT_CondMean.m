function xbar = PCA_ODT_CondMean( x, y, ymin, ymax, n, varargin )
%
%phibar = PCA_ODT_CondMean( x, y, ymin, ymax, n, varargin )
%  x    - variable we want the average of
%  y    - variable that we condition x on
%  ymax - y maximum limit
%  ymin - y minimum limit
%  n    - number of sections in y
%
% computes the conditional mean of x with respect to y
%
% xbar = sum( blank(i)*wt(i)*phi(i) ) / sum( blank(i)*wt(i) )
%
assert(size(x,1)==size(y,1));
assert(size(x,2)==size(y,2));

sections = linspace(ymin,ymax,n+1);
xbar=zeros(n,size(x,2));
for j=1:size(x,2)
    for i=1:n
        if (i<n)
            Selx=x((y(:,j)>=sections(i))&(y(:,j)<sections(i+1)));
        else
            Selx=x((y(:,j)>=sections(i))&(y(:,j)<=sections(i+1)));
        end
        xbar(i,j)=mean(Selx);
    end
end
