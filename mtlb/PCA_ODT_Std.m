function xbar = PCA_ODT_Std( x, npts)
%
%phibar = PCA_ODT_Std( x, y, ymin, ymax, n )
%  x    - variable we want the its standard deviation
%  npts - number of points in the space of each ODT line
%
% computes the standard deviation of x over space
%
assert(mod(size(x,1),npts)==0);
nVar=size(x,3);
nt=size(x,2);
nDataPts=size(x,1);
nReal=nDataPts/npts;
for j=1:nVar
    for i=1:nt
        tempX=reshape(x(:,i,j),npts,nReal);
        xbar(:,i,j)=var(tempX,0,2);
    end
end
