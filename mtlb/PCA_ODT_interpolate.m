function x_interp = PCA_ODT_interpolate (x, npt, nt)
x_interp = zeros(npt,nt);
for i=1:nt
    for j=2:npt+1
        x_interp(j-1,i)= (x(j-1,i) + x(j,i)) / 2;
    end
end
