function fphi = transform( phi, x, xo, L, form );

xnew = triplet_map( x, xo, L, form );

% determine the function values at the y points.
fphi = interp1( x, phi, xnew, 'linear' );

if( strncmp(form,'fun',3) )
  % determine the kernel weight.
  g = compute_g(x,xo,L);
  t1 = trapz(x, phi);
  t2 = trapz(x,fphi);
  if( t1~=t2 )
    gamma = ( t1-t2 )./trapz(x,g);
    fphi = fphi + gamma.*g;
  end
  if( isnan(fphi) )
    fprintf('\nNAN Encountered\n');
  end

  % if( strncmp(form,'fun',3) )
  %   clf;
  %   plot(x,phi,'ko-',x,fphi,'r.-');
  %   pause;
end
end

%---------------------------------------

function g = compute_g( x, xo, L )
g = 0*x;
xlo = xo;
xhi = xo+L/3;
i = find( x>=xlo & x<xhi );
g(i) = 3/L*(x(i)-xo);

xlo = xhi;
xhi = xo+2/3*L;
i = find( x>=xlo & x<xhi );
g(i) = -3/L*(x(i)-xo-L/3)+1;

xlo = xhi;
xhi = xo+L;
i = find( x>=xlo & x<=xhi+1e-10 );
g(i) = 3/L*(x(i)-xo-2*L/3);
% g = 0*x;
% xlo = xo;
% xhi = xo+L/2;
% i = find( x>=xlo & x<xhi );
% g(i) = (x(i)-xo)/(L/2);
% 
% xlo=xhi;
% xhi =xlo+L/2;
% i = find( x>=xlo & x<xhi );
% g(i) = 1 - (x(i)-xo-L/2)/(L/2);
end
