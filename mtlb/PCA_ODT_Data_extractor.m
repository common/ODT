function vars=PCA_ODT_Data_extractor(db_address,VarNames)

% extracting the time folder names and sort them in the ascending order for
% visualization
temp_dirs=dir(db_address);
for(i=3:length(temp_dirs)-1)
    dirs{i-2}=temp_dirs(i).name;
    dirs_num(i-2)=str2double(dirs{i-2});
end
[sort_dirs_num,ind]=sort(dirs_num);
for i=1:length(ind)
sort_dirs{i}=dirs{ind(i)};
end

% extracting the required fields from the database
ds = ODTDataSet(db_address,sort_dirs,VarNames);
subset = ds.subdirs;
for i=1:length(VarNames)
    vars(:,:,i) = ds.extract_var( VarNames{i}, subset );
end
end