function y = triplet_map( x, xo, L, form )
% y = triplet_map( x, xo, L, form )

y = x;

if( nargin<4 ) form=''; end

if( strncmp(form,'fun',3) )

%   [ilo,ihi]=shift3(x,xo,L);
%   xo = x(ilo);
%   L = x(ihi)-xo;
 
  xlo = xo;
  xhi = xo+L/3;
  i = find( x>=xlo & x<xhi );
  y(i) = xo + 3*(x(i)-xo);

  xlo = xhi;
  xhi = xo+2/3*L;
  i = find( x>=xlo & x<xhi );
  y(i) = xo + 2*L - 3*(x(i)-xo);

  xlo = xhi;
  xhi = xo+L;
  i = find( x>=xlo & x<xhi );
  y(i) = xo + 3*(x(i)-xo) - 2*L;
  
else

  % require number of points to be divisible by 3.
  [j0,jL] = shift3(x,xo,L);
  n = jL-j0;
  
  ixmap = 1:length(x);

  j = j0 : j0+n/3-1;
  ixmap(j) = j0 + 3*(j-j0);
  
  j = j0+n/3 : j0+2/3*n-1;
  ixmap(j) = j0 + 2*(n-1)-3*(j-j0);

  j = j0+2/3*n : j0+n-1;
  ixmap(j) = j0 + 3*(j-j0)-2*(n-1);
  
  y = x(ixmap);
end

end


function [ilo, ihi] = shift3( x, xo, L )
ihi = nearest( xo+L, x );
ilo = nearest( xo, x );
n = ihi-ilo;
nextra = mod(n,3);
if( ihi+3-nextra > length(x) )
  ihi = ihi - nextra;
else
  ihi = ihi+(3-nextra);
end
end