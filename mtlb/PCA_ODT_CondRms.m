function xbar = PCA_ODT_CondRms( x, y, ymin, ymax, n )
%
%phibar = PCA_ODT_CondRms( x, y, ymin, ymax, n )
%  x    - variable we want the its rms
%  y    - variable that we condition x on
%  ymax - y maximum limit
%  ymin - y minimum limit
%  n    - number of sections in y
%
% computes the conditional rms of x with respect to y
%
assert(size(x,1)==size(y,1));
assert(size(x,2)==size(y,2));

sections = linspace(ymin,ymax,n+1);
xbar=zeros(n,size(x,2));
for j=1:size(x,2)
    for i=1:n
        if (i<n)
            Selx=x((y(:,j)>=sections(i))&(y(:,j)<sections(i+1)));
        else
            Selx=x((y(:,j)>=sections(i))&(y(:,j)<=sections(i+1)));
        end
        xbar(i,j)=rms(Selx);
%         xbar(i,j)=sqrt(sum(Selx.^2)/length(Selx));
    end
end
