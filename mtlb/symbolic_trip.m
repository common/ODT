clear;

syms xo L x;

f1 = xo + 3.*(x-xo);
g1 = 3/L*(x-xo);
xlo = xo;
xhi = xlo+L/3;
If1 = int(f1,xlo,xhi);
Ig1 = int(g1,xlo,xhi);

f2 = xo + 2.*L - 3.*(x-xo);
g2 = -3/L*(x-xo-L/3)+1;
xlo = xhi;
xhi = xlo + L/3;
If2 = int(f2,xlo,xhi);
Ig2 = int(g2,xlo,xhi);


f3 = xo + 3.*(x-xo) -2*L;
g3 = 3/L*(x-xo-2*L/3);
xlo = xhi;
xhi = xlo+L/3;
If3 = int(f3,xlo,xhi);
Ig3 = int(g3,xlo,xhi);

If = simplify( If1+If2+If3 );
Ig = Ig1+Ig2+Ig3;
fprintf('Integral of the G function:\n');
pretty(simple(Ig));
fprintf('Verifying that the triplet map is conservative:\n');
pretty(simple(If-int(x,xo,xo+L)));


xo = 1;
L  = 1;
x1 = linspace(xo,xo+1/3*L);
x2 = linspace(xo+1/3*L,xo+2/3*L);
x3 = linspace(xo+2/3*L,xo+L);

clf;
plot( linspace(xo,xo+L),linspace(xo,xo+L),'b-',...
      x1,subs(subs(f1),'x',x1),'k-',...
      x2,subs(subs(f2),'x',x2),'k-',...
      x3,subs(subs(f3),'x',x3),'k-' );

hold on;

plot( x1,subs(subs(g1),'x',x1),'r--',...
      x2,subs(subs(g2),'x',x2),'r--',...
      x3,subs(subs(g3),'x',x3),'r--' );
xlabel('x');

axis tight;
