set( output_test output.log )

file( REMOVE_RECURSE ${outfile} )
message( ${test_cmd} " -i ${input_file} > ${output_test}" )

execute_process(
  COMMAND ${test_cmd} -i ${input_file}
  OUTPUT_FILE ${output_test}
  )

string( COMPARE NOTEQUAL "${diff_cmd}" "" DO_COMPARISON )

if( ${DO_COMPARISON} )
  message( "${diff_cmd} ${outfile} ${outfile_blessed}" )
  execute_process( COMMAND ${diff_cmd} ${outfile} ${outfile_blessed} RESULT_VARIABLE test_result )
  
  message( "test result: ${test_result}" )
  if( test_result )
    message( SEND_ERROR "${outfile} and ${outfile_blessed} are different!" )
  endif()
else()
  message( "NOT PERFORMING COMPARISON because no GOLD_STANDARDS_DIR was specified" )
endif()
