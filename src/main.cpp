#include <ODTVersion.h>

#include <expression/ExprLib.h>
#include <GraphHelperTools.h>
#include <FileWriter.h>
#include <parser/ParseTools.h>
#include <parser/BasicExprBuilder.h>

#include <StringNames.h>
#include <eddies/ODT.h>

#include <expression/ExprLib.h>
#include <spatialops/SpatialOpsConfigure.h>
#include <spatialops/structured/FieldHelper.h>

#include <gas/RxnDiffusionSetup.h>

#include <particles/ParticleSetup.h>
#include <particles/ParticleStringNames.h>

//--- PCA/MARS stuff includes ---//
#include <gas/pca/PCA.h>
#include <gas/pca/PCTripletTerm.h>

#include <pokitt/CanteraObjects.h>

#include <boost/foreach.hpp>

#include <iostream>
#include <string>
#include <ctime>
#include <sstream>
#include <cmath>

//--------------------------------------------------------------------

std::string
getp(int& i, int argc, char** args)
{
  using std::string;
  string a="--";
  if( i < argc-1 ) a = string(args[i+1]);
  if( a[0] == '-') a = "<missing>";
  else i += 1;
  return a;
}

//--------------------------------------------------------------------

void
show_help()
{
  std::cout << std::endl
       << "" << std::endl << std::endl
       << " options:" << std::endl
       << "   -i <file name>  specify the name of the input file (required)" << std::endl
       << std::endl;
}

//-------------------------------------------------------------------

int main( int argc, char** argv )
{
  using std::cout; using std::endl;
  cout << "-------------------------------------------------------" << endl
       << " ODT - A code to solve the eulerian ODT equations" << endl
       << endl
       << " For more information, contact James.Sutherland@utah.edu" << endl
       << endl
       << " This executable was built from source code version:" << endl
       << "    Git Hash: " << ODTVersionHash << endl
       << "        Date: " << ODTVersionDate << endl
       << endl
       << " Built against:" << endl
       << "   SpatialOps Hash: " << SOPS_REPO_HASH << endl
       << "              Date: " << SOPS_REPO_DATE << endl
       << "      ExprLib Hash: " << EXPR_REPO_HASH << endl
       << "              Date: " << EXPR_REPO_DATE << endl
       << "-------------------------------------------------------" << endl
       << endl;

  std::string inputFileName = "";
  if( argc == 1 ){
    show_help();
    return -1;
  }

  int i=1;
  while (i < argc) {
    const std::string arg = std::string(argv[i]);
    if (arg == "-i") inputFileName = getp(i,argc,argv);
    else if( arg == "-h" || arg == "-H" ) show_help();
    else {
      cout << endl << endl << "unknown option: " << arg << endl;
      show_help();
      exit(-1);
    }
    ++i;
  }

  if( inputFileName == "" ){
    cout << "ERROR: No input file was specified!" << endl;
    show_help();
    return -1;
  }

  try{
    // build a parser and then parse the input file.
    const YAML::Node parser = YAML::LoadFile( inputFileName );

    const bool isRestarting = parser["restart"  ];
    const bool hasParticles = parser["Particles"];
    const bool hasPCSolver  = parser["PCSolver" ];
    bool standAlonePCSolver = false;
    
    int nparticles = 0;
    if( hasParticles ){
      nparticles = parser["Particles"]["NumParticles"].as<int>();
    }
    
    PCA pca;
    int nPCs;
    if( hasPCSolver ){
      try{
        standAlonePCSolver = parser["PCSolver"]["StandAlone"].as<bool>();
        const std::string pcaDataFile = parser["PCSolver"]["pcaDataFile"].as<std::string>();
        nPCs = parser["PCSolver"]["nPCs"].as<int>();
        pca = PCA( pcaDataFile, nPCs );
      }
      catch( std::exception& e ){
        std::cerr << e.what() <<endl;
        throw std::runtime_error("Error loading PCA class.\n");
      }
    }
    
    PatchT patch( parser["mesh"]["npts"].as<int>(), 1, 1, nparticles );

    GraphCategories gc;
    gc[INITIALIZATION  ] = new GraphHelper( new Expr::ExpressionFactory(false), patch.field_manager_list() );
    gc[ADVANCE_SOLUTION] = new GraphHelper( new Expr::ExpressionFactory(false), patch.field_manager_list() );

    // set up the Reaction-Diffusion problem from the parsed input.

    /* Currently, tar and soot models are turned on if coal models are turned on.The
     * ParticleSetup object determines whether coal models are turned on by checking
     * if the bool coalImplement_ is set to true or not. coalImplement is set to
     * "true" during construction, so coal models are turned on if particles are
     * present in the simulation. If inert particles are to be considered, setting
     * hasTarAndSoot = hasParticles will be incorrect and will result in the code
     * breaking and will need to be changed.
     */
    const bool hasTarAndSoot = hasParticles;
    RxnDiffusionSetup rxndiff( parser, patch, gc, isRestarting, pca, nPCs, hasTarAndSoot );

    // parse expressions created from the input file
    try{
      create_expressions_from_input( parser, gc );
    }
    catch( std::exception& err ){
      std::ostringstream msg;
      msg << "\n\nError trapped while parsing BasicExpression\n" << err.what() << endl
          << "\nIf you are having trouble with your input file, you can try pasting it\n"
          << "at the following URL: http://yamllint.com/\n\n";
      throw std::runtime_error( msg.str() );
    }

    Expr::TimeStepper& integrator = rxndiff.get_time_stepper();
    Expr::ExpressionFactory& exprfactory = rxndiff.get_expression_factory();

    Particle::ParticleSetup* particleSetup;
    if( hasParticles )  particleSetup = new Particle::ParticleSetup( &integrator, patch, exprfactory, rxndiff.grid(), parser );
    
    bool implementODT = false;
    const YAML::Node odtPG = parser["ODT"];
    if( odtPG ) implementODT = odtPG["implement"].as<bool>();
    if( implementODT ){
      
      if( hasPCSolver ){
        try{
//          const std::string pcaDataFile = parser["PCSolver"]["pcaDataFile"].as<std::string>();
//          const int nPCs = parser["PCSolver"]["nPCs"].as<int>();
          
          const StringNames& sName = StringNames::self();
//          std::cout << "This time for calculating PC Triplet mapping term : ";
//          PCA pca(pcaDataFile,nPCs);
          for( int i=0; i<nPCs; ++i ){
            std::ostringstream rhoPCName;
            rhoPCName << sName.rhoPC << "_" << i;
            Expr::Tag rhoPCTag_= Expr::Tag(rhoPCName.str(),Expr::STATE_N);
            std::ostringstream pcTripletTerm;
            pcTripletTerm << "pcTripletTerm_" << i;
            const Expr::Tag pcTripletTermTag = Expr::Tag(pcTripletTerm.str(),Expr::STATE_N);
            typedef PCTripletTerm<VolField>::Builder PCTriTerm;
            Expr::ExpressionID pcTriTermID = exprfactory.register_expression( new PCTriTerm( pcTripletTermTag, rhoPCTag_, Expr::Tag(sName.density,Expr::STATE_N), i, pca ) );
            integrator.get_tree()->insert_tree(pcTriTermID);
          }
        }catch (std::exception& e)
        {
          std::cerr << e.what() <<endl;
          throw std::runtime_error("Error setting PCA parameters.\n");
        }
      }
    }
    

    // set the boundary conditions
    rxndiff.set_bc();

    // finalize the integrator.  This creates the tree, registers
    // fields, and binds fields in expressions.
    integrator.finalize( patch.field_manager_list(),
                         patch.operator_database(),
                         patch.field_info());

    // initialize variables
    if( !isRestarting ){
      rxndiff.set_initial_conditions( parser );
      if( hasParticles ){
        // currently, particle initialization is quite primitive.
        particleSetup->set_initial_conditions( parser, rxndiff.get_icexpression_factory() );
      }
      rxndiff.build_inital_tree();
    }

    // write out the expression graph
    {
      std::ofstream fout("tree.dot");
      integrator.get_tree()->write_tree(fout);
    }

    // populate a vector of variable names required for restart
    std::vector<std::string> restartVars; integrator.get_variables(restartVars);
    restartVars.push_back( StringNames::self().temperature );

    /* Initialize a map of restart variable names (first entry) and their respective aliases (second entry).
     * The default alias will be the variable name.
     */
    typedef std::map<std::string, std::string> StringMap;
    StringMap varMap;

    // create the output database and request the mesh for output
    FileWriter* outputDatabase;
    try{
      const YAML::Node outputParser = parser["Output"];

      const std::string dbName = outputParser["DatabaseName"].as<std::string>("fields.db");
      outputDatabase = new FileWriter( rxndiff.patch().field_manager_list(), dbName, isRestarting );

      // request output of grid variables
      outputDatabase->request_field_output< VolField>( Expr::Tag(StringNames::self().xcoord,    Expr::STATE_N), "x"     );
      outputDatabase->request_field_output<FaceField>( Expr::Tag(StringNames::self().xsurfcoord,Expr::STATE_N), "xface" );

      // request output of all solution variables and temperature so that we can restart.
      for( std::vector<std::string>::const_iterator inm=restartVars.begin(); inm!=restartVars.end(); ++inm ){
        outputDatabase->request_field_output( Expr::Tag(*inm,Expr::STATE_N), *inm );
        varMap.insert( std::pair<std::string, std::string> (*inm, *inm) );
      }
      
      // request output of fields specified in input file
      BOOST_FOREACH( const YAML::Node& field, outputParser["Fields"] ){
        const Expr::Tag tag( parse_nametag(field["name"]) );
        const std::string alias = field["alias"].as<std::string>(tag.name());
        outputDatabase->request_field_output( tag, alias );

        // Check if the variable requested for output is one required for restarts. If so, update its alias.
        StringMap::iterator itr = varMap.find(tag.name());
        if( itr != varMap.end()) itr->second = alias;
      }
    }
    catch( std::exception& err ){
      std::ostringstream msg;
      msg << "\nError trapped while creating output database.\n" << err.what() << endl;
      throw std::runtime_error( msg.str() );
    }

    // restart if requested - restart must occur after all equations have been set up
    if( isRestarting ){
      const std::string restartTime = parser["restart"].as<std::string>();
      cout << "RESTARTING from time: " << restartTime << endl;
      outputDatabase->set_fields_from_file( restartTime, varMap );
    }

    // disallow memory recovery from graph - we can turn this on once
    // we get output fields locked so that they don't disappear.
    integrator.get_tree()->lock_fields(rxndiff.patch().field_manager_list());

    // Added by Naveen for state space statistics
    integrator.get_tree()->execute_tree();

    const double dt      = parser["TimeStepper"]["step"   ].as<double>();
    const double endTime = parser["TimeStepper"]["end"    ].as<double>();
    const double timeMon = parser["TimeStepper"]["monitor"].as<double>();
    double simTime       = parser["restart"].as<double>(0.0);
    integrator.set_time(simTime);

#   ifdef ENABLE_THREADS
    cout << "Executing with " << NTHREADS << " threads" << endl;
#   else
    cout << "Executing without threads" << endl;
#   endif
    cout << "Time step: " << dt << endl;

    time_t t1 = std::clock();

    ODTParameters odtparams;
    if( implementODT ){
      cout << "Triplet mapping is active!\n";
      odtparams.alpha        = odtPG["alpha"    ].as<double>();
      odtparams.z            = odtPG["Z"        ].as<double>();
      odtparams.beta         = odtPG["beta"     ].as<double>();
      odtparams.c            = odtPG["C"        ].as<double>();
      odtparams.stresstol    = odtPG["stresstol"].as<double>(0.0);
      odtparams.Re           = odtPG["Re"       ].as<int>();
      odtparams.integLS      = odtPG["integ_LS" ].as<double>();
      odtparams.domainLength = parser["mesh"]["length"].as<double>();
      odtparams.timestep     = dt;

      // set a random seed specified from the input file if provided.
      // Otherwise, use the system time.
      srand( odtPG["RandomSeed"].as<double>(time(0)) );
    }

    try{
      outputDatabase->write_file( simTime );  // write the initial condition to disk.
    }
    catch( std::exception& err ){
      std::cout << err.what() << std::endl;
      return -1;
    }

    // set source term for the particles
    if( hasParticles )  particleSetup->create_eddy_srcterm(&simTime,parser);

    int eddy_count;
    std::vector<int> sindexvec, eindexvec;
    std::vector<double> eddylifevec, eddylifestart;

    const std::string fname1 = "sindex.m";
    const std::string fname2 = "eindex.m";
    const std::string fname3 = "eddylife.m";
    const std::string fname4 = "eddystart.m";

    std::fstream fout1( fname1.c_str(), std::ofstream::app | std::ofstream::in | std::ofstream::out);
    std::fstream fout2( fname2.c_str(), std::ofstream::app | std::ofstream::in | std::ofstream::out);
    std::fstream fout3( fname3.c_str(), std::ofstream::app | std::ofstream::in | std::ofstream::out);
    std::fstream fout4( fname4.c_str(), std::ofstream::app | std::ofstream::in | std::ofstream::out);

    fout1 << std::scientific;
    fout2 << std::scientific;
    fout3 << std::scientific;
    fout4 << std::scientific;

    fout1.precision( 14 );
    fout2.precision( 14 );
    fout3.precision( 14 );
    fout4.precision( 14 );
    int ix;

    if (!isRestarting)
    {
      eddy_count = 0 ;
      sindexvec.push_back(0);
      eindexvec.push_back(0);
      eddylifevec.push_back(0.0);
      eddylifestart.push_back( 0.0);

      fout1 << "function x = sindex()" << std::endl;
      fout2 << "function x = eindex()" << std::endl;
      fout3 << "function x = eddylife()" << std::endl;
      fout4 << "function x = eddystart()" << std::endl;

      ix = 1;
      {
        fout1 << "x(" << ix << ") = " << 0 << ";" << std::endl;
        fout2 << "x(" << ix << ") = " << 0 << ";" << std::endl;
        fout3 << "x(" << ix << ") = " << 0.0 << ";" << std::endl;
        fout4 << "x(" << ix << ") = " << 0.0 << ";" << std::endl;
      }
    }
    else
    {
       std::ifstream fin( fname1.c_str());
       int count =0;
       std::string line;
       while (getline(fin, line))
         if (line.substr(0,2) == "x(")
           count++;
       eddy_count = count;
       ix = count;
    }


    while( integrator.get_time()<endTime ){

      integrator.step( dt );
      simTime = integrator.get_time();

      if( fmod(simTime,timeMon ) < dt && simTime>=dt ){
        outputDatabase->write_file( simTime );
      }

      if( implementODT ){

        //
        // jcs: note that we should probably push much of this into the ODT object itself...
        //
        const StringNames& sName = StringNames::self();
        Expr::FieldManagerList& fml = patch.field_manager_list();
        Expr::FieldMgrSelector<VolField>::type& volFM = fml.field_manager<VolField>();

        const VolField& coord     = volFM.field_ref( Expr::Tag(sName.xcoord,Expr::STATE_N   ) );
        const VolField& viscosity = volFM.field_ref( Expr::Tag(sName.viscosity,Expr::STATE_N) );

        // doing this for now because we need a vector going into the ODT
        // interface.  Need to clean this up.
        std::vector<double> coord_vec;
        for( VolField::const_iterator iter=coord.begin(); iter!=coord.end(); ++iter ){
          coord_vec.push_back(*iter);
        }

        // mapping will be applied on solution variables from which parameters will be deduced
        // jcs should be able to obtain these from the equations list on the RxnDiffusionSetup object.
        const Expr::FieldMgrSelector<FaceField>::type& faceFM = fml.field_manager<FaceField>();
        const FaceField& tauyx = faceFM.field_ref( Expr::Tag("tauyx",      Expr::STATE_N) );
        VolField&  xmom        =  volFM.field_ref( Expr::Tag(sName.xmom,   Expr::STATE_N) );
        VolField&  ymom        =  volFM.field_ref( Expr::Tag(sName.ymom,   Expr::STATE_N) );
        VolField&  density     =  volFM.field_ref( Expr::Tag(sName.density,Expr::STATE_N) );

        
        //std::cout<<"implement eddy is true"<<std::endl;
        ODT ODTobj(xmom,ymom,density,coord,viscosity,tauyx,coord_vec,odtparams);
        const double eddylife = ODTobj.return_eddylifetime();
        
        if( ODTobj.has_eddy() && (eddylife * odtparams.beta) <= simTime ){
          std::cout<<"Eddy selected : "<<simTime<<std::endl;

          //For eddy source term approch for particles...
          const int sindex = ODTobj.return_sindex();
          const int eindex = ODTobj.return_eindex();
          {
            //for dumping eddy statistics
            sindexvec.push_back(sindex);
            eindexvec.push_back(eindex);
            eddylifevec.push_back(eddylife);
            eddylifestart.push_back( simTime );
            ix += 1;
            fout1 << "x(" << ix << ") = " << sindex   << ";" << std::endl;
            fout2 << "x(" << ix << ") = " << eindex   << ";" << std::endl;
            fout3 << "x(" << ix << ") = " << eddylife << ";" << std::endl;
            fout4 << "x(" << ix << ") = " << simTime  << ";" << std::endl;
          }

          if( hasParticles )
            particleSetup->set_eddy_events("p_Xmom",simTime,eddylife,sindex,eindex);

          if(hasTarAndSoot){
            VolField& rhoTar   = volFM.field_ref( Expr::Tag( sName.rhoTar  , Expr::STATE_N ) );
            VolField& rhoSoot  = volFM.field_ref( Expr::Tag( sName.rhoSoot , Expr::STATE_N ) );
            VolField& rhoSootP = volFM.field_ref( Expr::Tag( sName.rhoSootP, Expr::STATE_N ) );

            ODTobj.transform( rhoTar   );
            ODTobj.transform( rhoSoot  );
            ODTobj.transform( rhoSootP );
          }

          // jcs need to fix this so that we don't build tags every time...
//          VolField& tempBefore_ = volFM.field_ref(Expr::Tag(sName.temperature, Expr::STATE_N));
//          VolField& cpBefore_ = volFM.field_ref(Expr::Tag(sName.heat_capacity, Expr::STATE_N));
//          print_field( tempBefore_, beforefile );
//          print_field( density, beforefile );
//          print_field( cpBefore_, beforefile );
          if( !hasPCSolver || !standAlonePCSolver ){
            const std::vector<std::string>& spNames = CanteraObjects::species_names();
            for( int i=0; i<CanteraObjects::number_species()-1; ++i ){
              std::ostringstream spname;
              spname << sName.rhoY << "_" << spNames[i];
              VolField& species_= volFM.field_ref( Expr::Tag(spname.str(),Expr::STATE_N) );
              //            print_field( species_, beforefile );
              ODTobj.transform(species_);
              //            print_field( species_, afterfile );
            }
          }
          if( hasPCSolver ){
            for( int i=0; i<nPCs; ++i ){
              std::ostringstream pcTriTermname;
              pcTriTermname << "pcTripletTerm_" << i;
              VolField& pcTriTerm_= volFM.field_ref( Expr::Tag(pcTriTermname.str(),Expr::STATE_N) );
//              print_field( pcTriTerm_, beforefile );
              ODTobj.transform(pcTriTerm_);
//              print_field( pcTriTerm_, afterfile );
            }
          }

//          std::ostringstream pcTriTermname;
//          pcTriTermname << "pcTripletTerm_0";
//          VolField& pcTriTerm_= volFM.field_ref( Expr::Tag(pcTriTermname.str(),Expr::STATE_N) );
//          print_field( pcTriTerm_, afterfile );
          if (!hasPCSolver || !standAlonePCSolver) {
            VolField&  energy      =  volFM.field_ref( Expr::Tag(sName.rhoE0,  Expr::STATE_N) );
            ODTobj.transform(energy);
          }
          ODTobj.transform(density);
          ODTobj.transform(xmom);
          ODTobj.transform(ymom);
          eddy_count++;
//          print_field( pcTriTerm_, afterfile );


          if( hasPCSolver ){
            try{
//              const std::string pcaDataFile = parser["PCSolver"]["pcaDataFile"].as<std::string>();
//              const int nPCs = parser["PCSolver"]["nPCs"].as<int>();
//              
//              std::cout << "This time for extracting rhoPC after Triplet mapping : ";
//              PCA pca(pcaDataFile,nPCs);
              
              for( int i=0; i<nPCs; ++i ){
                std::ostringstream rpcname;
                rpcname << sName.rhoPC << "_" << i;
                VolField& rpc_= volFM.field_ref( Expr::Tag(rpcname.str(),Expr::STATE_N) );
//                print_field( rpc_, beforefile );
                std::ostringstream pcTripletTerm;
                pcTripletTerm << "pcTripletTerm_" << i;
                VolField& pcTriTerm_= volFM.field_ref( Expr::Tag(pcTripletTerm.str(),Expr::STATE_N) );;
                pca.RhoPCFromTripletTerm(rpc_, pcTriTerm_, density, i);
//                print_field( rpc_, afterfile );
              }
              
            }
            catch (std::exception& e){
              std::cerr << e.what() <<endl;
              throw std::runtime_error("Error setting PCA parameters.\n");
            }
          }

//          integrator.get_tree()->execute_tree();
//          VolField& tempAfter_ = volFM.field_ref(Expr::Tag(sName.temperature, Expr::STATE_N));
//          print_field( tempAfter_, afterfile );
//          
//          VolField& cpAfter_ = volFM.field_ref(Expr::Tag(sName.heat_capacity, Expr::STATE_N));
//          print_field( density, afterfile );
//          print_field( cpAfter_, afterfile );

          //write_files(simTime+dt,rxndiff);
          //simTime = endTime- 1.5*dt;
          //write_files(simTime+dt,rxndiff);
        } //if object has eddy
      } //if implement ODT
//      if (eddy_count==4) {
//        beforefile.close();
//        afterfile.close();
//        std::cout << "finished writing the file" <<endl;
//        return 0;
//      }
    }//for loop

    fout1 << "x = zeros(" << eddy_count+1 << ",1);" << std::endl;
    fout2 << "x = zeros(" << eddy_count+1 << ",1);" << std::endl;
    fout3 << "x = zeros(" << eddy_count+1 << ",1);" << std::endl;
    fout4 << "x = zeros(" << eddy_count+1 << ",1);" << std::endl;

    fout1.close();
    fout2.close();
    fout3.close();
    fout4.close();

    delete outputDatabase;
    if( hasParticles ) delete particleSetup;

    std::cout << "# of eddies : " << eddy_count << std::endl << std::endl
              << "Execution time (sec): "
              << std::difftime( std::clock(), t1 )/CLOCKS_PER_SEC
              << std::endl;

  }  // try block

  catch( YAML::Exception& e ){
    std::cout << "\nPARSING ERROR!\n" << e.what() << endl
        << "\nIf you are having trouble with your input file, you can try pasting it\n"
        << "at the following URL: http://yamllint.com/\n\n";
    return -1;
  }
  catch( std::exception& e ){
    std::cout << e.what() << std::endl;
    return -1;
  }
  catch(...){
    std::cout << "An unknown error occurred\n";
    return -1;
  }

  return 0;
}
