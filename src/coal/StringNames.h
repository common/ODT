#ifndef StringNames_COAL_h
#define StringNames_COAL_h

#include <string>

/**
 *   Author : Babak Goshayeshi
 *   Date   : Jan 2011
 *   University of Utah - Institute for Clean and Secure Energy
 *
 *   Holds names for Tags.
 */
namespace coal {
  
class StringNames
{
public:
  static const StringNames& self();
  const std::string dev_char, cpd_l;
  const std::string cpd_G_RHSi, cpd_Delta_RHSi, cpd_gi, cpd_deltai, cpd_kgi, cpd_yi, cpd_dyi, dev_mv; 
  const std::string cpd_kb, cpd_L_RHS, cpd_C_RHS, dev_volatile_RHS, cpd_product_RHS, cpd_charProd_RHS;
  const std::string char_charmass, char_coco2ratio, char_heattogas;
  const std::string char_co2RHS, char_coRHS, char_o2RHS, char_h2RHS, char_h2oRHS, char_ch4RHS;
  const std::string char_oxid_RHS, char_gasifh2o, char_gasifco2, char_gasifh2;
  const std::string ash_density, ash_porosity, ash_thickness, core_diameter, core_density;
  const std::string ash_mass_frac, char_mass_frac, char_conversion, log_freq_dist, therm_anneal;
  const std::string coal_density, coal_prtmass, coal_mash, coal_cp, coal_temprhs, char_massRHS;
  const std::string cpd_tar, cpd_tarProdR, cpd_lbPopulation;
  const std::string sarofim_tar, sarofim_tarProdR;
  const std::string singlerate_tar, singlerate_tarProdR;
private:
  StringNames();    ~StringNames();
  StringNames(const StringNames&); 
  StringNames& operator=(const StringNames&); 
};

} // coal namespace 
#endif
