#ifndef CPDInterface_h
#define CPDInterface_h

/**
 *  \file CPDInterface.h
 *
 *  \defgroup CPD CPD Model
 */

#include <map>
#include <expression/ExprLib.h>
#include "CPDData.h"
#include <coal/Devolatilization/DevolatilizationBase.h>
#include <coal/StringNames.h>
#include <coal/CoalData.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace CPD{

  /**
   *  \ingroup CPD
   *  \class CPDInterface
   *  \brief Provides an interface to the CPD model
   */
  template< typename FieldT >
  class CPDInterface: public DEV::DevolatilizationBase
  {
    const CPD::CPDInformation cpdInfo_;
    const coal::CoalComposition& coalcomp_;
    const double c0, vmFrac0, tar0;
    const Expr::Tag tempt_, ct_, lt_, prtMassTag_, lrhstag_, initprtmast_,
                    lbPopTag_, lbPopRHSTag_, tarTag_; //tarProdRTag_;
    //Expr::Tag tarProdRTag_;

    Expr::TagList grhsit_, deltarhsit_, git_, deltait_, kgit_, yit_;
    Expr::ExpressionID mvID_, lrhsID_, crhsID_, grhsID_, drhsID_, yiID_, dyiID_, prdrhsID_, 
                       charrhsID_, tarProdRID_, tarID_, lbPopRHSID_;

    bool haveRegisteredExprs_;

    CPDInterface(); // no copying
    CPDInterface& operator=( const CPDInterface& ); // no assignment


  public:
    /**
     *  \param coalType the CoalType for this coal.
     *  \param temperature    Particle temperature
     *  \param prtMassTag     Particle mass
     *  \param initialprtmastag Initial particle mass
     */
    CPDInterface( const coal::CoalType coalType,
                  const Expr::Tag temperature,
                  const Expr::Tag prtMassTag,
                  const Expr::Tag initialprtmastag );

    /**
     *  \brief obtain a reference to the CPDInformation object.
     */
    const CPD::CPDInformation& get_cpd_info() const{ return cpdInfo_; }

    /**
     *  \brief Register all expressions required to implement the CPD model
     */
    void register_expressions( Expr::ExpressionFactory& );

    /**
     *  \brief Plug in equations to the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory );

    /**
     *  \brief Set the initial conditions for the CPD model
     */
    void set_initial_conditions( Expr::FieldManagerList& fml );

    /**
     *  \brief obtain the Tag for production rate of specified species
     */
    const Expr::Tag gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const;

    /**
     *  \brief obtain the Tag gi ( internal CPD calculation )
     */
    const Expr::TagList& gi_rhs_tags() const{ return grhsit_; }

    /**
     *  \brief obtain the Tag for labile bridge consumption rate ( internal CPD tag )
     */
    const Expr::Tag& l_tag()       const{ return lt_;     }
    const Expr::Tag& l_rhs_tag()   const{ return lrhstag_;}
    const Expr::Tag& tar_tag()     const{ return tarTag_; }
    //const Expr::Tag& tar_prodR_tag() { return Expr::Tag( coal::StringNames::self().cpd_tarProdR, Expr::STATE_N ); }

  // const std::string get_species_name(int specindex);

  };

} // namespace CPD

#endif // CPDInterface_h
