function fgx = fgInterpolation(Cx, Hx, Ox, n)

%% **********************************************
%  
%
%  Interpolation of functional group data based on :
% 
%     Carbon, Hydrogen and oxygen fraction from Ultimate Analysis
% 
%  AUTHOR : Babak Goshayeshi
%  Date   : Summer, 2011
%
%
%  **********************************************
%%  
if nargin<4
    n=4;
end;
if (n>6)
    display('The number of coal type you requested is more than library content');
    n=6;
end;

%case NorthDakota_Lignite:  ** 1 **
% Mole Fraction ( daf )0.4588    0.3946    0.0065    0.0028    0.1372
% Mass Fraction (daf) 0.665 0.048 0.011 0.011 0.265
C(1) = 0.665;
H(1) = 0.048;
N(1) = 0.011;
S(1) = 0.011;
O(1) = 0.265;
moisture_(1) = 0.299; % wt fraction
ash_(1)      = 0.072; % wt fraction
vm_(1)       = 0.295;
fixedc_(1)   = 0.334;

fg_(1,1) =0.065 ; %Y1  CO2 extra loose
fg_(1,2) =0.030 ; %Y2  CO2 loose
fg_(1,3) =0.005 ; %Y3  tight
fg_(1,4) =0.061 ; %Y4  H2o loose
fg_(1,5) =0.033 ; %Y5  tight
fg_(1,6) =0.060 ; %Y6  CO ether loose
fg_(1,7) =0.044 ; %Y7  CO ether tight
fg_(1,8) =0.006 ; %Y8  HCN loose
fg_(1,9) =0.012 ; %Y9  HCN tight
fg_(1,10) =0.001 ; %Y10 NH3
fg_(1,11) =0.095 ; %Y11 Chx aliphatic
fg_(1,12) =0.000 ; %Y12 methane extra loose
fg_(1,13) =0.016 ; %Y13 methane loose
fg_(1,14) =0.009 ; %Y14 methane tight
fg_(1,15) =0.017 ; %Y15 H aromatic
fg_(1,16) =0.090 ; %Y17 CO extra tight
%fg_(1,) =0.160 ; %Tar
%break;

%case Gillette_Subbituminous: ** 2 **
% Mole Fraction DAF 0.4950    0.3851    0.0071    0.0013    0.1115
% Mass Fraction (daf)  0.720 0.047 0.012 0.005 0.216
C(2) = 0.720;
H(2) = 0.047;
N(2) = 0.012;
S(2) = 0.005;
O(2) = 0.216;
moisture_(2) = 0.27930;  % Check again with other refrences
ash_(2) = 0.05570;% Check again with other refrences

fg_(2,1) =0.018;
fg_(2,2) =0.053;
fg_(2,3) =0.028;
fg_(2,4) =0.031;
fg_(2,5) =0.031;
fg_(2,6) =0.080;
fg_(2,7) =0.043;
fg_(2,8) =0.007;
fg_(2,9) =0.015;
fg_(2,10) =0.000;
fg_(2,11) =0.158;
fg_(2,12) =0.000;
fg_(2,13) =0.026;
fg_(2,14) =0.017;
fg_(2,15) =0.012;
fg_(2,16) =0.031;
%fg_(2,) =0.200 ;Tar



%case MontanaRosebud_Subbituminous : ** 3 **
% Mole Fraction DAF  0.4908    0.3959    0.0070    0.0030    0.1033
% Mass Fraction (daf) 0.724 0.049 0.012 0.012 0.203      C = 0.4908;
C(3) = 0.724;
H(3) = 0.049;
N(3) = 0.012;
S(3) = 0.012;
O(3) = 0.203;
moisture_(3) = 0.213;
ash_(3)      = 0.118;

fg_(3,1) =0.035 ; %Y1  CO2 extra loose
fg_(3,2) =0.035 ; %Y2  CO2 loose
fg_(3,3) =0.030 ; %Y3  tight
fg_(3,4) =0.051 ; %Y4  H2o loose
fg_(3,5) =0.051 ; %Y5  tight
fg_(3,6) =0.055 ; %Y6  CO ether loose
fg_(3,7) =0.013 ; %Y7  CO ether tight
fg_(3,8) =0.005 ; %Y8  HCN loose
fg_(3,9) =0.015 ; %Y9  HCN tight
fg_(3,10) =0.001 ; %Y10 NH3
fg_(3,11) =0.127 ; %Y11 Chx aliphatic
fg_(3,12) =0.000 ; %Y12 methane extra loose
fg_(3,13) =0.022 ; %Y13 methane loose
fg_(3,14) =0.012 ; %Y14 methane tight
fg_(3,15) =0.013 ; %Y15 H aromatic
fg_(3,16) =0.000 ; %Y17 CO extra tight
%fg_(3,) =0.430 ; %Tar
%break;


%case Illinois_Bituminous:   ** 4 **
% Mass Fraction (daf) 0.736 0.047 0.014 0.038 0.165
C(4) = 0.736;
H(4) = 0.047;
N(4) = 0.014;
S(4) = 0.038;
O(4) = 0.165;
moisture_(4) = 0.101;
ash_(4)      = 0.073;
vm_(4)       = 0.359;
fixedc_(4)   = 0.467;

fg_(4,1) =0.022 ; %Y1  CO2 extra loose
fg_(4,2) =0.022 ; %Y2  CO2 loose
fg_(4,3) =0.030 ; %Y3  tight
fg_(4,4) =0.045 ; %Y4  H2o loose
fg_(4,5) =0.000 ; %Y5  tight
fg_(4,6) =0.060 ; %Y6  CO ether loose
fg_(4,7) =0.063 ; %Y7  CO ether tight
fg_(4,8) =0.010 ; %Y8  HCN loose
fg_(4,9) =0.016 ; %Y9  HCN tight
fg_(4,10) =0.000 ; %Y10 NH3
fg_(4,11) =0.081 ; %Y11 Chx aliphatic
fg_(4,12) =0.011 ; %Y12 methane extra loose
fg_(4,13) =0.011 ; %Y13 methane loose
fg_(4,14) =0.022 ; %Y14 methane tight
fg_(4,15) =0.016 ; %Y15 H aromatic
fg_(4,16) =0.000 ; %Y17 CO extra tight
%fg_(4,) =0.430 ; %Tar
%break;

%case Kentucky_Bituminous: ** 5 **
% Mass Fraction (daf)  0.817 0.056 0.019 0.024 0.084
C(5) = 0.817;
H(5) = 0.056;
N(5)     = 0.019;
S(5)  = 0.024;
O(5)         = 0.084;
moisture_(5) = 0.086;
ash_(5)      = 0.233;
vm_(5)       = 0.352;
fixedc_(5)   = 0.415;

fg_(5,1) =0.000 ; %Y1  CO2 extra loose
fg_(5,2) =0.006 ; %Y2  CO2 loose
fg_(5,3) =0.005 ; %Y3  tight
fg_(5,4) =0.011 ; %Y4  H2o loose
fg_(5,5) =0.011 ; %Y5  tight
fg_(5,6) =0.050 ; %Y6  CO ether loose
fg_(5,7) =0.026 ; %Y7  CO ether tight
fg_(5,8) =0.026 ; %Y8  HCN loose
fg_(5,9) =0.009 ; %Y9  HCN tight
fg_(5,10) =0.000 ; %Y10 NH3
fg_(5,11) =0.183 ; %Y11 Chx aliphatic
fg_(5,12) =0.020 ; %Y12 methane extra loose
fg_(5,13) =0.015 ; %Y13 methane loose
fg_(5,14) =0.015 ; %Y14 methane tight
fg_(5,15) =0.012 ; %Y15 H aromatic
fg_(5,16) =0.020 ; %Y17 CO extra tight
%fg_(5,) =0.430 ; %Tar
%break;


%case Pittsburgh_Bituminous: ** 6 **
% Mass Fraction (daf) 0.821 0.056 0.017 0.024 0.082
C(6) = 0.821;
H(6) = 0.056;
N(6) = 0.017;
S(6) = 0.024;
O(6) = 0.082;
moisture_(6) = 0.01;
ash_(6)      = 0.069;
vm_(6)       = 0.289;
fixedc_(6)   = 0.632;

fg_(6,1) =0.000 ; %Y1  CO2 extra loose
fg_(6,2) =0.006 ; %Y2  CO2 loose
fg_(6,3) =0.005 ; %Y3  tight
fg_(6,4) =0.011 ; %Y4  H2o loose
fg_(6,5) =0.011 ; %Y5  tight
fg_(6,6) =0.050 ; %Y6  CO ether loose
fg_(6,7) =0.022 ; %Y7  CO ether tight
fg_(6,8) =0.009 ; %Y8  HCN loose
fg_(6,9) =0.022 ; %Y9  HCN tight
fg_(6,10) =0.000 ; %Y10 NH3
fg_(6,11) =0.190 ; %Y11 Chx aliphatic
fg_(6,12) =0.020 ; %Y12 methane extra loose
fg_(6,13) =0.015 ; %Y13 methane loose
fg_(6,14) =0.015 ; %Y14 methane tight
fg_(6,15) =0.012 ; %Y15 H aromatic
fg_(6,16) =0.020 ; %Y17 CO extra tigh

%% Interploation 

for i=1:6
    d(i)=((H(i)/C(i) - Hx/Cx)^2 + (O(i)/C(i) - Ox/Cx)^2)^0.5;
end
[B IX] = sort(d);

suml=sum(d(IX(1:n)));
fgx=zeros(1,16);
for i=1:n
    fgx=fgx+fg_(IX(i),:)*((suml-d(IX(i)))/(n-1)/suml);
end;

    





