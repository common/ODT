#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

#include <coal/Devolatilization/CPD/CPDInterface.h>

#include <expression/ExprLib.h>
typedef Expr::ExprPatch  PatchT;

#include <spatialops/particles/ParticleFieldTypes.h>
typedef SpatialOps::Particle::ParticleField  FieldT;

#include <spatialops/Nebo.h>
using SpatialOps::operator<<=;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

//--------------------------------------------------------------------

bool
setup_and_test_integrator( const Expr::TSMethod method,
                           const coal::CoalType coalType,
                           const double Temp1,
                           const double dt,
                           const double endt )
{
  using std::cout;
  using std::endl;

  PatchT patch(1,1,1,1);
  const double ParticleMass = 5.0E-10;
  Expr::ExpressionFactory exprFactory;

  const Expr::Tag tempTag       ("Particle_Temperature",  Expr::STATE_N);
  const Expr::Tag prtMassTag    ("Particle_Mass",Expr::STATE_N);
  const Expr::Tag initprtMassTag("intial_Particle_Mass",Expr::STATE_N);
  const Expr::ExpressionID TempID    = exprFactory.register_expression(new Expr::ConstantExpr<FieldT>::Builder(tempTag,        Temp1));
  const Expr::ExpressionID pMasID    = exprFactory.register_expression(new Expr::ConstantExpr<FieldT>::Builder(prtMassTag,     ParticleMass));
  const Expr::ExpressionID intpMasID = exprFactory.register_expression(new Expr::ConstantExpr<FieldT>::Builder(initprtMassTag, ParticleMass));

  CPD::CPDInterface<FieldT> cpd( coalType, tempTag, prtMassTag, initprtMassTag );
  const int nspec = cpd.get_cpd_info().get_nspec();

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::TimeStepper timeIntegrator( exprFactory, method, "timestepper", patch.id() );
  cpd.register_expressions( exprFactory );
  cpd.hook_up_time_integrator( timeIntegrator, exprFactory );
  timeIntegrator.get_tree()->insert_tree(pMasID);
  timeIntegrator.finalize( fml,
                           patch.operator_database(),
                           patch.field_info() );
  timeIntegrator.get_tree()->lock_fields( fml );
  FieldT& prtmas   = fml.field_manager<FieldT>().field_ref( prtMassTag   );
  prtmas <<= ParticleMass;
  cpd.set_initial_conditions( fml );

  const Expr::TagList& git = cpd.gas_species_rhs_tags();
  const Expr::TagList& girhst = cpd.gi_rhs_tags();

  typedef std::vector<const FieldT*> SpecT;
  SpecT dy_i, girhs;
  for( int i=0; i<7; ++i ){
    dy_i.push_back( &fml.field_manager<FieldT>().field_ref(git[i]) );
  }

  for( int i=0; i<15; ++i ){
    girhs.push_back( &fml.field_manager<FieldT>().field_ref(girhst[i]) );
  }

  const FieldT& l    = fml.field_manager<FieldT>().field_ref( cpd.l_tag() );
  const FieldT& c    = fml.field_manager<FieldT>().field_ref( cpd.char_production_rate_tag() );
  const FieldT& mv   = fml.field_manager<FieldT>().field_ref( cpd.volatile_tag() );
  const FieldT& dmv  = fml.field_manager<FieldT>().field_ref( cpd.volatile_consumption_rate_tag());
  const FieldT& lrhs = fml.field_manager<FieldT>().field_ref( cpd.l_rhs_tag());
  const FieldT& dTar = fml.field_manager<FieldT>().field_ref( cpd.tar_production_rate_tag() );
  // Writing tree plot in tree.dot
  {
    std::ofstream ofile("cpd_tree.dot");
    timeIntegrator.get_tree()->write_tree( ofile, false, true );
  }

  std::ostringstream savedname;
  savedname << "CPD_" << Temp1 <<".txt";
  cout << savedname.str() << endl;
  std::ofstream fout( savedname.str().c_str(), std::ios_base::trunc|std::ios_base::out );
  fout.width(10);
  fout.precision(5);

  fout << "  This is the result of CPD model" << endl;
  fout << "  Created by : Babak Goshayeshi - PhD student" << endl;
  fout << "               University of Utah - Chemical Engineering Department" << endl << endl << endl;
  fout << std::setw(10) << "time" << std::setw(12) << "l_0"<<std::setw(12) << "l_rhs" << std::setw(12) << "c_rhs" ;
  for (int i=0;i<7; i++){
    std::ostringstream name1;
    name1 << "dyi" << i;
    fout << std::setw(12) << name1.str();
  }

  //  for (int i=0;i<16; i++){
  //    std::ostringstream name1;
  //    name1 << "girhs" << i;
  //    fout << std::setw(12) << name1.str();
  //  }



  fout << std::setw(12) << "dTar"
      << std::setw(12) << "dmv"
      <<  std::setw(12) << "mv"
      << endl;
  for (double t=0.0; t<endt; t+=dt) {

    fout << std::setw(10) << std::setprecision(2) << t
        << std::setw(12) << std::setprecision(5) << l[0]*28;
    timeIntegrator.step(dt);

    fout<< std::setw(12) << std::setprecision(5) << lrhs[0]*28
        << std::setw(12) << std::setprecision(5) << c[0];

    double Sum = 0.0;
    for( SpecT::const_iterator idy_i = dy_i.begin(); idy_i != dy_i.end() ;++idy_i ){
      fout << std::setw(12) << std::setprecision(5) << *((*idy_i)->begin());
      Sum += *((*idy_i)->begin());
    }

    Sum += dTar[0];

    //     for( SpecT::const_iterator igrhs = girhs.begin(); igrhs != girhs.end() ;++igrhs ){
    //       fout << std::setw(12) << std::setprecision(5) << *((*igrhs)->begin());
    //     }
    fout << std::setw(12) << std::setprecision(5) << dTar[0]
         << std::setw(12) << std::setprecision(5) << dmv[0]
         << std::setw(12) << std::setprecision(5)  << mv[0]
         << endl;
    if( std::abs( dmv[0]+c[0]-Sum ) > 1e-16 ){
      cout << "Sum of rates != 0 (" << std::abs( dmv[0]+c[0]-Sum ) << ")\n";
      return false;
    }
  }
  fout.close();

  cout << "\n***  Integration of L_RHS and C_RHS was done over [ 0, " << endt << " ]" << endl;
  cout << "***  Also for delta_i and g_i" << endl;

  return true;
}

//--------------------------------------------------------------------

int main( int narg, char* arg[] )
{
  using std::cout;
  using std::endl;

  double temp, dt, tend;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
          ( "help", "print help message\n" )
          ( "temperature", po::value<double>(&temp)->default_value(500.0), "Temperature (K) of the system" )
          ( "timestep",    po::value<double>(&dt  )->default_value(2.0e-4), "Timestep (s)" )
          ( "endtime",     po::value<double>(&tend)->default_value(1e-1 ), "End time (s)" );

    po::variables_map vm;

    po::store( po::parse_command_line(narg,arg,desc), vm );
    po::notify( vm );

    if( vm.count("help") ) {
      cout << desc << endl;
      return 1;
    }
  }
  catch( po::unknown_option& e ){
    cout << e.what() << endl;
    return 1;
  }

  cout << "T  = " << temp << endl
      << "dt = " << dt << endl
      << "end time = " << tend << endl
      << endl;

  try{
    const bool passed = setup_and_test_integrator( Expr::SSPRK3,
                                                   coal::NorthDakota_Lignite,
                                                   temp, dt, tend );
    if( passed ) return 0;
  }
  catch( std::exception& e ){
    cout << e.what() << endl;
  }
  return -1;
};
