#include "CPDInterface.h"

#include <stdexcept>
#include <sstream>

#include <boost/foreach.hpp>

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/Nebo.h>

#include <expression/ClipValue.h>

// expressions we build here
#include "L_RHS.h"
#include "CPDData.h"
#include "C_RHS.h"
#include "Gi_RHS.h"
#include "Deltai_RHS.h"
#include "c0_fun.h"
#include "kb.h"
#include "kg_i.h"
#include "dy_gi.h"
#include "MvRHS.h"
#include "Mv.h"
#include "TarProductionRate.h"

using std::vector;
using std::cout;
using std::endl;
using std::ostringstream;
using std::string;

namespace CPD{

//------------------------------------------------------------------

vector<string> taglist_names( const Expr::TagList& tags )
{
  vector<string> fieldNames;
  for(  Expr::TagList::const_iterator it=tags.begin(); it!=tags.end(); ++it ){
    fieldNames.push_back( it->name() );
  }
  return fieldNames;
}
//------------------------------------------------------------------
	
CPDSpecies gas_dev2cpd( const DEV::DEVSpecies cspec )
{
  CPDSpecies s;
  switch( cspec ){
    case DEV::CO2:  s=CO2;             break;
    case DEV::H2O:  s=H2O;             break;
    case DEV::CO :  s=CO;              break;
    case DEV::HCN:  s=HCN;             break;
    case DEV::NH3:  s=NH3;             break;
    case DEV::CH4:  s=CH4;             break;
    case DEV::H2 :  s=H2;               break;
    default:   s=INVALID_SPECIES; break;
  }
  return s;
}
	
//--------------------------------------------------------------------

template< typename FieldT >
CPDInterface<FieldT>::CPDInterface( const coal::CoalType ct,
                                    const Expr::Tag temptag,
                                    const Expr::Tag prtMassTag,
                                    const Expr::Tag intialprtmastag)
  : DEV::DevolatilizationBase(), 
    cpdInfo_( ct ),

    coalcomp_   ( cpdInfo_.get_coal_composition() ),
    tempt_      ( temptag ),
    lt_         ( coal::StringNames::self().cpd_l,    Expr::STATE_N ),
    prtMassTag_ ( prtMassTag ),
    lrhstag_    ( coal::StringNames::self().cpd_L_RHS, Expr::STATE_N ),
    initprtmast_( intialprtmastag ),

    //tarProdRTag_( coal::StringNames::self().cpd_tarProdR,           Expr::STATE_N ),
    tarTag_     ( coal::StringNames::self().cpd_tar,                Expr::STATE_N ),
    lbPopTag_   ( coal::StringNames::self().cpd_lbPopulation,       Expr::STATE_N ),
    lbPopRHSTag_( coal::StringNames::self().cpd_lbPopulation+"RHS", Expr::STATE_N ),

    c0      ( c0_fun( coalcomp_.get_C(), coalcomp_.get_O() )),
    tar0    ( tar_0(cpdInfo_, c0) ),
    vmFrac0 ( coalcomp_.get_vm()*(1.0-c0) )
{
	this->tarProdRTag_ = Expr::Tag( coal::StringNames::self().cpd_tarProdR, Expr::STATE_N );

  if( temptag == Expr::Tag() ){
    ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << endl
        << "temperature tag is invalid." << endl;
    throw std::runtime_error( msg.str() );
  }

  haveRegisteredExprs_ = false;
  const coal::StringNames& sName = coal::StringNames::self();

  // Base variables
  charrhst_    = Expr::Tag( sName.dev_char,               Expr::STATE_N );
  mvrhst_      = Expr::Tag( sName.dev_volatile_RHS,       Expr::STATE_N );
  mvt_         = Expr::Tag( sName.dev_mv,                 Expr::STATE_N );

  const int nspec = cpdInfo_.get_nspec();
  for( int i=0; i<nspec; ++i ){
    ostringstream name1;
    name1 << sName.cpd_G_RHSi << i;
    grhsit_.push_back( Expr::Tag(name1.str(), Expr::STATE_N) );

    ostringstream name2;
    name2 << sName.cpd_Delta_RHSi << i;
    deltarhsit_.push_back( Expr::Tag(name2.str(),Expr::STATE_N) );

    ostringstream name3;
    name3 <<  sName.cpd_gi << i;
    git_.push_back( Expr::Tag(name3.str(),Expr::STATE_N) );

    ostringstream name4;
    name4 << sName.cpd_deltai << i;
    deltait_.push_back( Expr::Tag(name4.str(),Expr::STATE_N) );

    ostringstream name5;
    name5 << sName.cpd_kgi << i;
    kgit_.push_back( Expr::Tag(name5.str(),Expr::STATE_N) );
  }

  const SpeciesSum& scc1 = SpeciesSum::self();
  const int ncomp = scc1.get_ncomp();
  for( int i=1; i<=ncomp; i++ ){
    //const string speciesName = get_species_name(i);
    ostringstream name6;
    name6 << sName.cpd_yi << i;
    yit_.push_back( Expr::Tag(name6.str(), Expr::STATE_N) );

    ostringstream name7;
    name7 << sName.cpd_dyi << i;
    dyit_.push_back( Expr::Tag(name7.str(),Expr::STATE_N) );
  }
}

//----------------------------------------------------------------------

template< typename FieldT >
void
CPDInterface<FieldT>::register_expressions( Expr::ExpressionFactory& exprFactory )
{
  const coal::StringNames& sName = coal::StringNames::self();
  const Expr::Tag kbtag            ( sName.cpd_kb,          Expr::STATE_N );
  const Expr::Tag prdrhst          ( sName.cpd_product_RHS, Expr::STATE_N );
  //const Expr::Tag tarProdRTag_     ( sName.cpd_tarProdR, Expr::STATE_N );
  const Expr::ExpressionID kbID  = exprFactory.register_expression( new typename kb  <FieldT>::Builder(kbtag, tempt_, cpdInfo_));
  const Expr::ExpressionID kgiID = exprFactory.register_expression( new typename kg_i<FieldT>::Builder(kgit_, git_,tempt_, initprtmast_, cpdInfo_) );
  tarProdRID_ = exprFactory.register_expression( new typename TarProductionRate <FieldT>::
                                                 Builder(tarProdRTag_, lbPopTag_, lbPopRHSTag_, initprtmast_, vmFrac0, tar0, cpdInfo_ ) );
  lrhsID_     = exprFactory.register_expression( new typename L_RHS     <FieldT>::Builder(lrhstag_,   kbtag,lt_) );
  crhsID_     = exprFactory.register_expression( new typename C_RHS     <FieldT>::Builder(charrhst_,  kbtag, lt_, cpdInfo_ ));
  grhsID_     = exprFactory.register_expression( new typename Gi_RHS    <FieldT>::Builder(grhsit_,    kbtag,kgit_,deltait_,lt_,cpdInfo_) );
  drhsID_     = exprFactory.register_expression( new typename Deltai_RHS<FieldT>::Builder(deltarhsit_,kbtag,kgit_,deltait_,lt_,cpdInfo_) );
  mvrhsID_    = exprFactory.register_expression( new typename MvRHS     <FieldT>::Builder(mvrhst_,    charrhst_, dyit_));
  dyiID_      = exprFactory.register_expression( new typename dy_gi     <FieldT>::Builder(dyit_,      grhsit_) );
  mvID_       = exprFactory.register_expression( new typename Mv        <FieldT>::Builder(mvt_,       lt_, tarTag_, deltait_, cpdInfo_, coalcomp_ ));
  lbPopRHSID_ = exprFactory.register_expression( new typename L_RHS     <FieldT>::Builder(lbPopRHSTag_, kbtag, lbPopTag_) );
  haveRegisteredExprs_ = true;

 // std::cout<<"\n\n tarProdRTag:        "<<tarProdRTag_<<"\n\n";

  // adds tar production rate to volatiles production rate
  exprFactory.attach_dependency_to_expression( tarProdRTag_, mvrhst_, Expr::ADD_SOURCE_EXPRESSION );

  // Ensure positive values
  typedef Expr::ClipValue<FieldT> Clipper;
  BOOST_FOREACH( const Expr::Tag& tag, deltait_ ){
    const Expr::Tag clip( tag.name()+"_cip", Expr::STATE_NONE );
    exprFactory.register_expression( new typename Clipper::Builder( clip, 0.0, 0.0, Clipper::CLIP_MIN_ONLY ) );
    exprFactory.attach_modifier_expression( clip, tag );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
CPDInterface<FieldT>::hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory )
{
  if( ! haveRegisteredExprs_ ){
    ostringstream msg;
    msg << endl
        << __FILE__ << " : " << __LINE__ << endl
        << "You must call register_expression() on the CPDInterface object prior to calling hook_up_time_integrator()" << endl
        << endl;
    throw std::runtime_error( msg.str() );
  }
  const int nghost = 0;
  timestepper.add_equations<FieldT>( taglist_names(git_),     grhsit_,      nghost );
  timestepper.add_equations<FieldT>( taglist_names(deltait_), deltarhsit_,  nghost );
  timestepper.add_equation <FieldT>( lt_.name(),              lrhstag_,     nghost );
  timestepper.add_equation <FieldT>( tarTag_.name(),          tarProdRTag_, nghost );
  timestepper.add_equation <FieldT>( lbPopTag_.name(),        lbPopRHSTag_, nghost );

  timestepper.get_tree()->insert_tree( mvID_,    false );
  timestepper.get_tree()->insert_tree( mvrhsID_, false );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
CPDInterface<FieldT>::set_initial_conditions( Expr::FieldManagerList& fml )
{
  using namespace SpatialOps;

  typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();

  const FieldT& prtmas = fm.field_ref( prtMassTag_ );
  FieldT& prtvolmass   = fm.field_ref( mvt_ );
  FieldT& lbPop        = fm.field_ref( lbPopTag_ );

  const double vMassFrac = coalcomp_.get_vm();

  prtvolmass <<= prtmas * vMassFrac*(1.0-c0);

  FieldT& l   = fm.field_ref( lt_ );
  FieldT& tar = fm.field_ref( tarTag_ );

  l     <<= cpdInfo_.get_l0_mass() * prtmas * vMassFrac; // kg basis
  lbPop <<= 1.0;          // this value will vary with coal type. Fixed for now to ease debugging.
  tar   <<= tar0 * prtmas * vMassFrac; // kg basis

  const vector<double> deltai0 = deltai_0(cpdInfo_, c0);

  const int nspec = cpdInfo_.get_nspec();

  for( size_t i=0; i<nspec; ++i ){
    FieldT& di = fm.field_ref( deltait_[i] );
    di <<= deltai0[i] * prtmas * vMassFrac;

    FieldT& gi = fm.field_ref( git_[i] );
    gi <<= 0.0;
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
const Expr::Tag
CPDInterface<FieldT>::
gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const
{
  const CPDSpecies spec = gas_dev2cpd(devspec);
  if( spec == INVALID_SPECIES ) return Expr::Tag();
  return dyit_[ spec ];
}

//====================================================================
// Explicit template instantiation
template class CPDInterface< SpatialOps::Particle::ParticleField >;
//====================================================================

} // namespace CPD
