include_directories( ${PROJECT_SOURCE_DIR} )
add_executable( test_kobsarofim test_kobsarofim.cpp )
target_link_libraries( test_kobsarofim coal ${TPL_LIBRARIES} )

add_test(
  NAME Sarofim_500
  COMMAND ${CMAKE_COMMAND}
  -D temp:STRING=500
  -D timestep:STRING=2e-4
  -D endtime:STRING=1e-1
  -D bindir:STRING=${CMAKE_CURRENT_BINARY_DIR}
  -D exe:STRING=${CMAKE_CURRENT_BINARY_DIR}/test_kobsarofim
  -D GS_DIR=${GOLD_STANDARDS_DIR}
  -P ${CMAKE_CURRENT_SOURCE_DIR}/run_test.cmake
  )

add_test(
  NAME Sarofim_1000
  COMMAND ${CMAKE_COMMAND}
  -D temp:STRING=1000
  -D timestep:STRING=5e-9
  -D endtime:STRING=1e-6
  -D bindir:STRING=${CMAKE_CURRENT_BINARY_DIR}
  -D exe:STRING=${CMAKE_CURRENT_BINARY_DIR}/test_kobsarofim
  -D GS_DIR=${GOLD_STANDARDS_DIR}
  -P ${CMAKE_CURRENT_SOURCE_DIR}/run_test.cmake
  )

add_test(
  NAME Sarofim_1500
  COMMAND ${CMAKE_COMMAND}
  -D temp:STRING=1500
  -D timestep:STRING=1e-10
  -D endtime:STRING=1e-7
  -D bindir:STRING=${CMAKE_CURRENT_BINARY_DIR}
  -D exe:STRING=${CMAKE_CURRENT_BINARY_DIR}/test_kobsarofim
  -D GS_DIR=${GOLD_STANDARDS_DIR}
  -P ${CMAKE_CURRENT_SOURCE_DIR}/run_test.cmake
  )
