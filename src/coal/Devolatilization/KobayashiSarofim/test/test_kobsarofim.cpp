#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

#include <coal/Devolatilization/KobayashiSarofim/KobSarofimInterface.h>

#include <expression/ExprLib.h>
typedef Expr::ExprPatch  PatchT;

#include <spatialops/particles/ParticleFieldTypes.h>
typedef SpatialOps::Particle::ParticleField  FieldT;

#include <spatialops/Nebo.h>
using SpatialOps::operator<<=;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

using namespace SAROFIM;
//--------------------------------------------------------------------


bool
setup_and_test_integrator( const Expr::TSMethod method,
                           const coal::CoalType coalType,
                           const double Temp1,
                           const double dt,
                           const double endt )
{
  using std::cout;
  using std::endl;

  PatchT patch(1,1,1,1);
  const double ParticleMass = 5.0E-10;
	
  Expr::ExpressionFactory exprFactory;
	
  const Expr::Tag tempTag       ("Particle_Temperature",  Expr::STATE_N);
  const Expr::Tag prtMassTag    ("Particle_Mass",Expr::STATE_N);
  const Expr::Tag initprtMassTag("intial_Particle_Mass",Expr::STATE_N);
		
  const Expr::ExpressionID TempID    = exprFactory.register_expression(new Expr::ConstantExpr<FieldT>::Builder(tempTag,        Temp1));
  const Expr::ExpressionID pMasID    = exprFactory.register_expression(new Expr::ConstantExpr<FieldT>::Builder(prtMassTag,     ParticleMass));
  const Expr::ExpressionID intpMasID = exprFactory.register_expression(new Expr::ConstantExpr<FieldT>::Builder(initprtMassTag, ParticleMass));
  
  SAROFIM::KobSarofimInterface<FieldT> kobsarofim( coalType, tempTag, prtMassTag, initprtMassTag);
  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::TimeStepper timeIntegrator( exprFactory, method, "timestepper", patch.id() );

  kobsarofim.register_expressions( exprFactory );

  kobsarofim.hook_up_time_integrator( timeIntegrator, exprFactory );
  timeIntegrator.get_tree()->insert_tree(pMasID, false);

  timeIntegrator.finalize( fml,
                           patch.operator_database(),
                           patch.field_info() );
  timeIntegrator.get_tree()->lock_fields( fml );
  FieldT& prtmas   = fml.field_manager<FieldT>().field_ref( prtMassTag   );
  prtmas <<= ParticleMass;

  kobsarofim.set_initial_conditions( fml );
	
  typedef std::vector<const FieldT*> SpecT;
  SpecT dy_i;
  const Expr::TagList dyit_= kobsarofim.gas_species_rhs_tags();
  for (int i=0; i<dyit_.size(); ++i) {
    dy_i.push_back( &fml.field_manager<FieldT>().field_ref( dyit_[i] ));
  }
  const Expr::Tag tarProdRTag = Expr::Tag( coal::StringNames::self().sarofim_tarProdR, Expr::STATE_N );

  const FieldT& mv   = fml.field_manager<FieldT>().field_ref( kobsarofim.volatile_tag()                  );
  const FieldT& dmv  = fml.field_manager<FieldT>().field_ref( kobsarofim.volatile_consumption_rate_tag() );
  const FieldT& dchar= fml.field_manager<FieldT>().field_ref( kobsarofim.char_production_rate_tag()      );
  const FieldT& dTar = fml.field_manager<FieldT>().field_ref( kobsarofim. tar_production_rate_tag()      );
  {
    std::ofstream ofile("sarofim_tree.dot");
    timeIntegrator.get_tree()->write_tree( ofile );
  }
	
  std::ostringstream savedname;
  savedname << "Sarofim_" << Temp1 <<".txt";
  cout << savedname.str() << endl;
  std::ofstream fout( savedname.str().c_str(), std::ios_base::trunc|std::ios_base::out );
  fout.width(10);
  fout.precision(5);

  fout << "  This is the result of Kobayashi_Sarofim model" << endl;
  fout << "  Created by : Babak Goshayeshi - PhD student" << endl;
  fout << "               University of Utah - Chemical Engineering Department" << endl << endl << endl;
  fout << std::setw(10) << "time" ;

  fout << std::setw(12) << "CO"
       << std::setw(12) << "H2"
       << std::setw(12) << "dTar"
       << std::setw(12) << "dmv"
       << std::setw(12) << "dchar"
       << std::setw(12) << "mv"
       << endl;
   for (double t=0.0; t<endt; t+=dt) {

     fout << std::setw(10) << std::setprecision(5) << t;
     timeIntegrator.step(dt);

     double Sum = 0.0;

     for( SpecT::const_iterator idy_i = dy_i.begin(); idy_i != dy_i.end() ;++idy_i ){
       fout << std::setw(12) << std::setprecision(5) << *((*idy_i)->begin());
       Sum += *((*idy_i)->begin());
     }
     Sum +=dchar[0] + dTar[0];

     fout << std::setw(12) << std::setprecision(5) << dTar[0]
          << std::setw(12) << std::setprecision(5) << dmv[0]
          << std::setw(12) << std::setprecision(5) << dchar[0]
          << std::setw(12) << std::setprecision(5) << mv[0]
          << endl;
   }
   fout.close();
  return true;
}

//--------------------------------------------------------------------

int main( int narg, char* arg[] )
{
  using std::cout;
  using std::endl;

  double temp, dt, tend;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message\n" )
      ( "temperature", po::value<double>(&temp)->default_value(500.0), "Temperature (K) of the system" )
      ( "timestep",    po::value<double>(&dt  )->default_value(2.0e-7), "Timestep (s)" )
      ( "endtime",     po::value<double>(&tend)->default_value(1e-4 ), "End time (s)" );

    po::variables_map vm;

    po::store( po::parse_command_line(narg,arg,desc), vm );
    po::notify( vm );

    if( vm.count("help") ) {
      cout << desc << endl;
      return 1;
    }
  }
  catch( po::unknown_option& e ){
    cout << e.what() << endl;
    return 1;
  }

  cout << "T  = " << temp << endl
       << "dt = " << dt << endl
       << "end time = " << tend << endl
       << endl;

  try{
    const bool passed = setup_and_test_integrator( Expr::SSPRK3,
                                                   coal::NorthDakota_Lignite,
                                                   temp, dt, tend );
    if( passed ) return 0;
  }
  catch( std::exception& e ){
    cout << e.what() << endl;
  }
  return -1;
};
