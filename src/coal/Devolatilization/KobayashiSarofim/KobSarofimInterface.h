#ifndef DEV_KOBSAROFIM_INTERFACE_h
#define DEV_KOBSAROFIM_INTERFACE_h

/**
 *  \class   KobSarofimInterface
 *  \ingroup Devolatilization - KobayashiSarofim
 *  \brief   Provide an interface to connect Kobbayashi-Sarofim two-step model
 *           as a devolatiization model to coal models. 
 *
 *  \author  Babak Goshayeshi
 *  \date    Jun 2012 
 *
 *  \param   coalType        : the CoalType for this coal.
 *  \param   temperature      : particle temperature.
 *  \param   prtMassTag       : particle mass
 *  \param   initialprtmastag : initial particle mass 
 */

#include <map>
#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>

#include <coal/StringNames.h>
#include <coal/CoalData.h>
#include <coal/Devolatilization/DevolatilizationBase.h>

#include "KobayashiSarofim.h"
#include "KobSarofimData.h"

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }
namespace DEV{ class DevolatilizationBase;}

namespace SAROFIM{
  

  /**
   *  \ingroup SAROFIM
   *  \class KobSarofimInterface
   *  \brief Provides an interface to the Kobayashi-Sarofim devolatilization model
   */
  template< typename FieldT >
  class KobSarofimInterface: public DEV::DevolatilizationBase
  {
    const coal::CoalComposition coalcomp_;
    const Expr::Tag tempt_, prtMassTag_, initprtmast_;
    Expr::TagList dcoalElementstag_, coalelementstag_;

    bool haveRegisteredExprs_;

    KobSarofimInterface(); // no copying
    KobSarofimInterface& operator=( const KobSarofimInterface& ); // no assignment


  public:
    /**
     *  \param coalType the CoalType for this coal.
     *  \param temperature    Particle temperature
     *  \param prtMassTag     Particle mass
     *  \param initialprtmastag Initial particle mass
     */
    KobSarofimInterface( const coal::CoalType& coalType,
                         const Expr::Tag temperature,
                         const Expr::Tag prtMassTag,
                         const Expr::Tag initialprtmastag );


    /**
     *  \brief Register all expressions required to implement the Kobayashi-Sarofim devolatilization model
     */
    void register_expressions( Expr::ExpressionFactory& );

    /**
     *  \brief Plug in equations to the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory );

    /**
     *  \brief Set the initial conditions for the Kobayashi-Sarofim devolatilization model
     */
    void set_initial_conditions( Expr::FieldManagerList& fml );

    /**
     *  \brief obtain the Tag for production rate of specified species
     */
    const Expr::Tag gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const;

  };

} // namespace SAROFIM

#endif // DEV_KOBSAROFIM_INTERFACE_h
