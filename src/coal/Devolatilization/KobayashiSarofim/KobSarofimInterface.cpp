#include "KobSarofimInterface.h"

#include <stdexcept>
#include <sstream>

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/Nebo.h>

// expressions we build here

using std::vector;
using std::cout;
using std::endl;
using std::ostringstream;
using std::string;

namespace SAROFIM{

//------------------------------------------------------------------	
  
KobSarofimSpecies gas_dev2sarrofim( const DEV::DEVSpecies cspec )
  {
KobSarofimSpecies s;
switch( cspec ){
  case DEV::CO:   s=CO;             break;
  case DEV::H2:   s=H2;             break;
  default:   s=INVALID_SPECIES;break;
}
return s;
  }
	
//--------------------------------------------------------------------

template< typename FieldT >
KobSarofimInterface<FieldT>::
KobSarofimInterface( const coal::CoalType& ct,
                     const Expr::Tag temptag,
                     const Expr::Tag prtMassTag,
                     const Expr::Tag intialprtmastag )
  : DEV::DevolatilizationBase(),
	  coalcomp_(ct),
    tempt_      ( temptag ),
    prtMassTag_ ( prtMassTag ),
    initprtmast_( intialprtmastag )
{
	
  if( temptag == Expr::Tag() || prtMassTag == Expr::Tag() || intialprtmastag ==Expr::Tag()){
    ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << endl
        << "Tags entered into KobSarofimInterface are invalid." << endl;
    throw std::runtime_error( msg.str() );
  }
	
	const coal::StringNames& sName = coal::StringNames::self();

		// Base variables 
	charrhst_    = Expr::Tag(sName.dev_char,         Expr::STATE_N );
	mvrhst_      = Expr::Tag(sName.dev_volatile_RHS, Expr::STATE_N );
	mvt_         = Expr::Tag(sName.dev_mv,           Expr::STATE_N );
	tarProdRTag_ = Expr::Tag(sName.sarofim_tarProdR, Expr::STATE_N );
	

  dyit_.push_back(Expr::Tag("sarofim_CO_RHS",   Expr::STATE_N));
  dyit_.push_back(Expr::Tag("sarofim_H2_RHS",   Expr::STATE_N));


  coalelementstag_.clear();
  coalelementstag_.push_back(Expr::Tag("Hydrogen_Volatile_Element",  Expr::STATE_N));
  coalelementstag_.push_back(Expr::Tag("Oxygen_Volatile_Element",    Expr::STATE_N));


  dcoalElementstag_.clear();
  dcoalElementstag_.push_back(Expr::Tag("Hydrogen_Volatile_Element_RHS", Expr::STATE_N));
  dcoalElementstag_.push_back(Expr::Tag("Oxygen_Volatile_Element_RHS",   Expr::STATE_N));

  haveRegisteredExprs_ = false;
}

//----------------------------------------------------------------------

template< typename FieldT >
void
KobSarofimInterface<FieldT>::register_expressions( Expr::ExpressionFactory& exprFactory )
{

  KobSarofimInformation sarrofimdata(coalcomp_);
	
	Expr::TagList mvchartags;
	mvchartags.push_back(mvrhst_); // 0- Volatile RHS
	mvchartags.push_back(charrhst_);// 1- Char RHS

	mvchartags.push_back(dyit_[0]    );  // 2 CO   Consumption rate ( for gas Phase )
	mvchartags.push_back(dyit_[1]    );  // 3 H2   Consumption rate ( for gas Phase )
	mvchartags.push_back(tarProdRTag_);  // 4 tar  Consumption rate ( for gas Phase )

  mvchartags.push_back(dcoalElementstag_[0]);  // 4 Hydrogen Consumption rate ( for gas Phase )
  mvchartags.push_back(dcoalElementstag_[1]);  // 4 Oxygen Consumption rate ( for gas Phase )

  mvrhsID_   = exprFactory.register_expression( new typename KobayashiSarofim <FieldT>::Builder(mvchartags, tempt_,
                                               mvt_, coalelementstag_,  sarrofimdata));
  haveRegisteredExprs_ = true;
}

//--------------------------------------------------------------------

template< typename FieldT >
void
KobSarofimInterface<FieldT>::hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory )
{
  if( ! haveRegisteredExprs_ ){
    ostringstream msg;
    msg << endl
        << __FILE__ << " : " << __LINE__ << endl
        << "You must call register_expression() on the KobSarrofimInterface object prior to calling hook_up_time_integrator()" << endl
        << endl;
    throw std::runtime_error( msg.str() );
  }

  const int nghost = 0;
  timestepper.add_equation<FieldT>(mvt_.name(), mvrhst_, nghost );

  timestepper.add_equation<FieldT>( coalelementstag_[0].name(), dcoalElementstag_[0], nghost );
  timestepper.add_equation<FieldT>( coalelementstag_[1].name(), dcoalElementstag_[1], nghost );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
KobSarofimInterface<FieldT>::set_initial_conditions( Expr::FieldManagerList& fml )
{
  using namespace SpatialOps;

  typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();

  const FieldT& prtmas = fm.field_ref( prtMassTag_ );
  FieldT& prtvolmass   = fm.field_ref( mvt_ );

  prtvolmass <<= prtmas * coalcomp_.get_vm();

  FieldT& u = fm.field_ref( coalelementstag_[0] );
  FieldT& o = fm.field_ref( coalelementstag_[1] );

  KobSarofimInformation sarrofimdata(coalcomp_);

  const double mw = sarrofimdata.get_molecularweight();

  u <<=  sarrofimdata.get_hydrogen_coefficient();
  o <<=  sarrofimdata.get_oxygen_coefficient();
}

//--------------------------------------------------------------------

template< typename FieldT >
const Expr::Tag
KobSarofimInterface<FieldT>::
gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const
{
	const KobSarofimSpecies spec = gas_dev2sarrofim(devspec); 
  if( spec == INVALID_SPECIES ) return Expr::Tag();
  return dyit_[ spec ];
}


//--------------------------------------------------------------------
//====================================================================
// Explicit template instantiation
template class KobSarofimInterface< SpatialOps::Particle::ParticleField >;
//====================================================================

} // namespace SARROFIM
