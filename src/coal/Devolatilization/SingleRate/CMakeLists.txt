get_filename_component( dir ${CMAKE_CURRENT_LIST_FILE} DIRECTORY )

set( DEVOL_SRC 
  ${dir}/SingleRateInterface.cpp
  ${dir}/SingleRateData.cpp
  PARENT_SCOPE
  ) 
  
add_subdirectory( test ) 