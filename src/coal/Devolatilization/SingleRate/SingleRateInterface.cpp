#include "SingleRateInterface.h"

#include <stdexcept>
#include <sstream>

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/Nebo.h>

#include "SingleRateData.h"


// expressions we build here

using std::vector;
using std::cout;
using std::endl;
using std::ostringstream;
using std::string;

namespace SNGRATE{

//------------------------------------------------------------------	
  
  SNGRATE::SingleRateSpecies gas_dev2singlerate( const DEV::DEVSpecies cspec )
  {
    SNGRATE::SingleRateSpecies s;
    switch( cspec ){
      case DEV::CO:   s=CO;             break;
      case DEV::H2:   s=H2;             break;
      default:   s=INVALID_SPECIES;break;
    }
    return s;
  }
	
//--------------------------------------------------------------------

template< typename FieldT >
SingleRateInterface<FieldT>::
SingleRateInterface( const coal::CoalType& ct,
                     const Expr::Tag temptag,
                     const Expr::Tag prtMassTag,
                     const Expr::Tag intialprtmastag,
                     const bool isDAE )
  : DEV::DevolatilizationBase(),
	  coalcomp_(ct),
    tempt_      ( temptag ),
    prtMassTag_ ( prtMassTag ),
    initprtmast_( intialprtmastag ),
    isDAE_      ( isDAE)
{
	
  if( temptag == Expr::Tag() || prtMassTag == Expr::Tag() || intialprtmastag ==Expr::Tag()){
    ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << endl
        << "Tags entered into SingleRateInterface are invalid." << endl;
    throw std::runtime_error( msg.str() );
  }

  const coal::StringNames& sName = coal::StringNames::self();

  // Base variables
  charrhst_         = Expr::Tag(sName.dev_char,            Expr::STATE_N );
  mvrhst_           = Expr::Tag(sName.dev_volatile_RHS,    Expr::STATE_N );
  mvt_              = Expr::Tag(sName.dev_mv,              Expr::STATE_N );
  tarProdRTag_      = Expr::Tag(sName.singlerate_tarProdR, Expr::STATE_N );


  dyit_.push_back(Expr::Tag("singlerate_CO_RHS", Expr::STATE_N));
  dyit_.push_back(Expr::Tag("singlerate_H2_RHS", Expr::STATE_N));

  haveRegisteredExprs_ = false;
}

//----------------------------------------------------------------------

template< typename FieldT >
void
SingleRateInterface<FieldT>::register_expressions( Expr::ExpressionFactory& exprFactory )
{
  SingleRateInformation singleratedata(coalcomp_);
	
  Expr::TagList mvchartags;
  mvchartags.push_back(mvrhst_); // 0- Volatile RHS
  // Char will be registered as a Constant Experssion
  mvchartags.push_back( dyit_[0]     );  // 1
  mvchartags.push_back( dyit_[1]     );  // 2
  mvchartags.push_back( tarProdRTag_ );  // 3

  mvrhsID_   = exprFactory.register_expression( new typename SingleRateModel<FieldT>::Builder(mvchartags, tempt_, mvt_, initprtmast_, coalcomp_.get_vm(), singleratedata, isDAE_));

  exprFactory.register_expression( new typename Expr::ConstantExpr<FieldT>::Builder( charrhst_, 0.0));

  haveRegisteredExprs_ = true;
}

//--------------------------------------------------------------------

template< typename FieldT >
void
SingleRateInterface<FieldT>::hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory )
{
  if( ! haveRegisteredExprs_ ){
    ostringstream msg;
    msg << endl
        << __FILE__ << " : " << __LINE__ << endl
        << "You must call register_expression() on the SingleRateInterface object prior to calling hook_up_time_integrator()" << endl
        << endl;
    throw std::runtime_error( msg.str() );
  }
  const int nghost = 0;
  timestepper.add_equation<FieldT>(mvt_.name(), mvrhst_, nghost );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
SingleRateInterface<FieldT>::set_initial_conditions( Expr::FieldManagerList& fml )
{
  using namespace SpatialOps;
  typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();
  const FieldT& prtmas = fm.field_ref( prtMassTag_ );
  FieldT& prtvolmass   = fm.field_ref( mvt_ );
  prtvolmass <<= prtmas * coalcomp_.get_vm();
}

//--------------------------------------------------------------------

template< typename FieldT >
const Expr::Tag
SingleRateInterface<FieldT>::
gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const
{
  const SingleRateSpecies spec = gas_dev2singlerate(devspec);
  if( spec == INVALID_SPECIES ) return Expr::Tag();
  return dyit_[ spec ];
}


//------------------------------------------------------------------

//====================================================================
// Explicit template instantiation
template class SingleRateInterface< SpatialOps::Particle::ParticleField >;
//====================================================================

} // namespace SNGRATE
