set( blessed ${GS_DIR}/SingleRate_${temp}.txt )
set( output  ${bindir}/SingleRate_${temp}.txt )

message( ${blessed} )
message( ${output} )

file( REMOVE ${output} )

message( "${exe} --temperature ${temp} --timestep ${timestep}" )

execute_process(
  COMMAND ${exe} --temperature ${temp} --timestep ${timestep} --endtime ${endtime}
  OUTPUT_FILE "test_output.log"
  RESULT_VARIABLE result
)
if( NOT ${result} EQUAL 0 )
  message( SEND_ERROR "error executing ${exe}" )
endif()

string( COMPARE NOTEQUAL "${GS_DIR}" "" HAVE_GS )

if( ${HAVE_GS} )
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E compare_files ${output} ${blessed}
    RESULT_VARIABLE result 
 )
 
 if( NOT ${result} EQUAL 0 )
   message( SEND_ERROR "${output} does not match ${blessed}!" )
 endif()
endif( ${HAVE_GS} )   