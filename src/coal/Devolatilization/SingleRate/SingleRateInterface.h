#ifndef DEV_SINGLERATE_INTERFACE_h
#define DEV_SINGLERATE_INTERFACE_h

/**
 *  \class   SingleRateInterface
 *  \ingroup Devolatilization - Single rate
 *  \brief   Provide an interface to connect Single Rate One-step model
 *           as a devolatiization model to coal models. 
 *
 *  \author  Babak Goshayeshi
 *  \date    May 2013 
 *
 *  \param   coalType         : the CoalType for this coal.
 *  \param   temperature      : particle temperature.
 *  \param   prtMassTag       : particle mass
 *  \param   initialprtmastag : initial particle mass 
 */

#include <map>
#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>

#include <coal/StringNames.h>
#include <coal/CoalData.h>
#include <coal/Devolatilization/DevolatilizationBase.h>

#include "SingleRateModel.h"


namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }
namespace DEV{ class DevolatilizationBase;}

namespace SNGRATE{
  

  /**
   *  \ingroup SNGRATE
   *  \class SingleRateInterface
   *  \brief Provides an interface to the Single Rate devolatilization model
   */
  template< typename FieldT >
  class SingleRateInterface: public DEV::DevolatilizationBase
  {
    const coal::CoalComposition coalcomp_;
    const Expr::Tag tempt_, prtMassTag_, initprtmast_;
    const bool isDAE_;

    bool haveRegisteredExprs_;

    SingleRateInterface(); // no copying
    SingleRateInterface& operator=( const SingleRateInterface& ); // no assignment


  public:
    /**
     *  \param coalType the CoalType for this coal.
     *  \param temperature    Particle temperature
     *  \param prtMassTag     Particle mass
     *  \param initialprtmastag Initial particle mass
     */
    SingleRateInterface( const coal::CoalType& coalType,
                         const Expr::Tag temperature,
                         const Expr::Tag prtMassTag,
                         const Expr::Tag initialprtmastag,
                         const bool isDAE);


    /**
     *  \brief Register all expressions required to implement the Single Rate devolatilization model
     */
    void register_expressions( Expr::ExpressionFactory& );

    /**
     *  \brief Plug in equations to the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory );

    /**
     *  \brief Set the initial conditions for the Single Rate devolatilization model
     */
    void set_initial_conditions( Expr::FieldManagerList& fml );

    /**
     *  \brief obtain the Tag for production rate of specified species
     */
    const Expr::Tag gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const;

  };

} // namespace SNGRATE

#endif // DEV_SINGLERATE_INTERFACE_h
