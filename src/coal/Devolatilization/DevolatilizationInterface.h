#ifndef DEVOLATILIZATION_h
#define DEVOLATILIZATION_h

/**
 *  \ingroup Devolatilization - KobayashiSarofim
 *  \brief   Provide an interface to be connected to an devolatilization model 
 *           
 *
 *  \author  Babak Goshayeshi
 *  \date    Jun 2012 
 *
 *  
 *  \how to specify the devolatiliztion model : 
 * 
 */

#include <map>
#include <coal/CoalData.h>
#include <expression/ExprLib.h>
#include <coal/StringNames.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>

#include "CPD/CPDInterface.h"
#include "KobayashiSarofim/KobSarofimInterface.h"
#include "SingleRate/SingleRateInterface.h"

#include "DevolatilizationBase.h"

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace DEV{
  


  /**
   *  \ingroup DEV
   *  \class DevolatilizationInterface
   *  \brief Provides an interface Devolatilization models
   */			
	
  template< typename FieldT >
  class DevolatilizationInterface
  {

DEV::DevolatilizationBase*  devmodel_;
CPD::CPDInterface<FieldT>*  cpdmodel_;
SAROFIM::KobSarofimInterface<FieldT>* kobmodel_;
SNGRATE::SingleRateInterface<FieldT>* singleratemdel_;


const coal::CoalType ct_;
const Expr::Tag tempt_, prtMassTag_, initprtmast_;
DevolatilizationInterface(); // no copying
DevolatilizationInterface& operator=( const DevolatilizationInterface& ); // no assignment


  public:
/**
 *  \param coalType the CoalType for this coal.
 *  \param temperature    Particle temperature
 *  \param prtMassTag     Particle mass
 *  \param initialprtmastag Initial particle mass
 */
DevolatilizationInterface( const coal::CoalType coalType,
                           const DevModel  dvm,
                           const Expr::Tag temperature,
                           const Expr::Tag prtMassTag,
                           const Expr::Tag initialprtmastag );

/**
 *  \brief obtain a reference to the CPDInformation object.
 */
  //const DEV::DEVInformation& get_Dev_info() const{ return devInfo_; };

/**
 *  \brief Register all expressions required to implement the DEV model
 */
void register_expressions( Expr::ExpressionFactory& );

/**
 *  \brief Plug in equations to the time integrator
 */
void hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory );

/**
 *  \brief Set the initial conditions for the DEV model
 */
void set_initial_conditions( Expr::FieldManagerList& fml );


/**
 *  \brief obtain the TagList for the gas phase species production rates (kg/s)
 */
const Expr::TagList& gas_species_rhs_tags();

/**
 *  \brief obtain the Tag for production rate of specified species
 */
const Expr::Tag gas_species_rhs_tag(const DEVSpecies spec );

/**
 *  \brief obtain the Tag for production rate of tar
 */
const Expr::Tag& tar_production_rate_tag();


const Expr::Tag& char_production_rate_tag();

/**
 *  \brief obtain the Tag for volatile consumption rate rhs ( to add in rhs of particle mass change and volatile mass )
 */
const Expr::Tag& volatile_consumption_rate_tag();

/**
 *  \brief obtain the Tag of volatile matter
 */
const Expr::Tag& volatile_tag();

  // const std::string get_species_name(int specindex);

  };
  
} // namespace DEV

#endif // DEVOLATILIZATION_h
