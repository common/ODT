#ifndef DEVOLATILIZATIONBase_h
#define DEVOLATILIZATIONBase_h

#include <expression/Tag.h>
#include <expression/ExpressionID.h>

/**
 *  \file DevolatilizationBase.h
 *  \ingroup Devolatilization 
 *
 *  \author  Babak Goshayeshi
 *  \date    Feb 2013 
 */

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace DEV{


  enum DEVSpecies {
    CO2  = 0,
    H2O  = 1,
    CO   = 2,
    HCN  = 3,
    NH3  = 4,
    C2H4 = 5,
    C2H6 = 6,
    C3H8 = 7,
    CH4  = 8,
    H    = 9,
    H2   = 10,
    INVALID_SPECIES = 99
  };


  enum DevModel{
    CPDM,
    KOBAYASHIM,
    SINGLERATE,
    DAE,
    INVALID_DEVMODEL
  };

  /**
   * @fn std::string dev_model_name( const DevModel model )
   * @param model the devolatilization model
   * @return the string name of the model
   */
  std::string dev_model_name( const DevModel model );

  /**
   * @fn DevModel devol_model( const std::string& modelName )
   * @param modelName the string name of the model
   * @return the corresponding enum value
   */
  DevModel devol_model( const std::string& modelName );

  /**
   * \class DevolatilizationBase
   * \brief Base class for devolatilization models
   */
  class DevolatilizationBase{
  public:

    Expr::TagList dyit_;

    Expr::Tag charrhst_, mvrhst_, mvt_, tarProdRTag_;

    Expr::ExpressionID mvrhsID_;
    /**
     *  \brief obtain the TagList for the gas phase species production rates (kg/s)
     */
    const Expr::TagList& gas_species_rhs_tags() const{ return dyit_; };
    
    
    /**
       *  \brief obtain the Tag for tar production rate ( during devolatilization of coal )
       */
      const Expr::Tag& tar_production_rate_tag() const{ return tarProdRTag_; }; // jtm


    /**
     *  \brief obtain the Tag for char production rate in CPD model ( during devolatilization of coal )
     */
    const Expr::Tag& char_production_rate_tag() const{ return charrhst_;};

    /**
     *  \brief obtain the Tag for volatile consumption rate rhs ( to add in rhs of particle mass change and volatile mass )
     */
    const Expr::Tag& volatile_consumption_rate_tag() const {return mvrhst_;};

    /**
     *  \brief obtain the Tag of volatile matter
     */
    const Expr::Tag& volatile_tag() const {return mvt_;};


    /**
     *  \brief Register all expressions required to implement the CPD model
     */
    virtual void register_expressions( Expr::ExpressionFactory& ) = 0;

    /**
     *  \brief Plug in equations to the time integrator
     */
    virtual void hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory ) = 0;

    /**
     *  \brief Set the initial conditions for the CPD model
     */
    virtual void set_initial_conditions( Expr::FieldManagerList& fml ) = 0;


    /**
     *  \brief obtain the Tag for production rate of specified species
     */
    virtual const Expr::Tag gas_species_rhs_tag( const DEV::DEVSpecies devspec ) const = 0;

    virtual ~DevolatilizationBase(){}
  };

} // namespace DEV

#endif // DEVOLATILIZATION_h
