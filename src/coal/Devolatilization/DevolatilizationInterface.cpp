#include "DevolatilizationInterface.h"
#include <stdexcept>
#include <sstream>
#include <cstdlib>

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/Nebo.h>

// expressions we build here

using std::vector;
using std::cout;
using std::endl;
using std::ostringstream;
using std::string;

namespace DEV{


  template< typename FieldT >
  DevolatilizationInterface<FieldT>::
  DevolatilizationInterface( const coal::CoalType ct,
                             const DevModel  dvm,
                             const Expr::Tag temptag,
                             const Expr::Tag prtMassTag,
                             const Expr::Tag intialprtmastag)
  : ct_( ct ),
    tempt_      ( temptag ),
    prtMassTag_ ( prtMassTag ),
    initprtmast_( intialprtmastag )
  {
    switch (dvm) {
      case CPDM:
        cpdmodel_ = new CPD::CPDInterface<FieldT>(ct, temptag, prtMassTag, intialprtmastag);
        devmodel_ = cpdmodel_;
        break;
      case KOBAYASHIM:
        kobmodel_ = new SAROFIM::KobSarofimInterface<FieldT>(ct, temptag, prtMassTag, intialprtmastag);
        devmodel_ = kobmodel_;
        break;
      case SINGLERATE:
        singleratemdel_ = new SNGRATE::SingleRateInterface<FieldT>(ct, temptag, prtMassTag, intialprtmastag, false);
        devmodel_ = singleratemdel_;
        break;
      case DAE:
        singleratemdel_ = new SNGRATE::SingleRateInterface<FieldT>(ct, temptag, prtMassTag, intialprtmastag, true);
        devmodel_ = singleratemdel_;
        break;

      case INVALID_DEVMODEL:
        throw std::invalid_argument( "Invalid model in DevolatilizationInterface constructor" );
    }
  }
  
  //----------------------------------------------------------------------

  template< typename FieldT >
  void

  DevolatilizationInterface<FieldT>::register_expressions( Expr::ExpressionFactory& exprFactory )
  {
     using std::cout; using std::endl;
    cout<<std::endl<<"DEV: registering expressions...     ";
    devmodel_->register_expressions( exprFactory );
    cout<<endl<<"Registered expressions for DEV model";
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void
  DevolatilizationInterface<FieldT>::hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory )
  {
    devmodel_->hook_up_time_integrator( timestepper, factory );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void
  DevolatilizationInterface<FieldT>::set_initial_conditions( Expr::FieldManagerList& fml )
  {
    devmodel_->set_initial_conditions(fml);
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  const Expr::Tag
  DevolatilizationInterface<FieldT>::
  gas_species_rhs_tag(const DEVSpecies spec)
  {  
    return devmodel_->gas_species_rhs_tag(spec);
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  const Expr::TagList&
  DevolatilizationInterface<FieldT>::
  gas_species_rhs_tags()
  {    
    return devmodel_->gas_species_rhs_tags();
  }
  
  //--------------------------------------------------------------------


  template< typename FieldT > // jtm
  const Expr::Tag&
  DevolatilizationInterface<FieldT>::
  tar_production_rate_tag()
  {
    return devmodel_->tar_production_rate_tag();
  }
  

  //--------------------------------------------------------------------


  template< typename FieldT >
  const Expr::Tag&
  DevolatilizationInterface<FieldT>::
  char_production_rate_tag()
  {
    return devmodel_->char_production_rate_tag();
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  const Expr::Tag&
  DevolatilizationInterface<FieldT>::
  volatile_consumption_rate_tag()
  {
    return devmodel_->volatile_consumption_rate_tag();
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  const 
  Expr::Tag&
  DevolatilizationInterface<FieldT>::
  volatile_tag()
  {
    return devmodel_->volatile_tag();
  }

  //====================================================================
  // Explicit template instantiation
  template class DevolatilizationInterface< SpatialOps::Particle::ParticleField >;
  //====================================================================

} // namespace DEV
