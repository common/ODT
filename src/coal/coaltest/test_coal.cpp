#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

#include <coal/CoalInterface.h>
#include <coal/CoalData.h>
#include <coal/Devolatilization/DevolatilizationInterface.h>
#include <pokitt/CanteraObjects.h> // include cantera wrapper

#include <expression/ExprLib.h>
typedef Expr::ExprPatch  PatchT;

#include <spatialops/particles/ParticleFieldTypes.h>
typedef SpatialOps::Particle::ParticleField  FieldT;

#include <spatialops/Nebo.h>
using SpatialOps::operator<<=;

#include <boost/program_options.hpp>
namespace po = boost::program_options;
using namespace coal;
//--------------------------------------------------------------------

/**
 *   Author : Babak Goshayeshi
 *   Date   : Mar 7 2011
 *   University of Utah - Institute for Clean and Secure Energy
 */
bool
setup_and_test_integrator( const Expr::TSMethod method,
                           const coal::CoalType coalType,
                           const double pdiam,
                           const double ptemp,
                           const double dt,
                           const double endt )
{
  using std::cout;
  using std::endl;
  PatchT patch(1,1,1,1);

  Expr::ExpressionFactory exprFactory;

  const CanteraObjects::Setup options("Mix", "gri30.cti", "gri30");
  CanteraObjects::setup_cantera(options, 1);

  const double pdens = 1800;
  const double PI = 3.141592653589793;
  const double pmass = pdens * 4./3. * PI * std::pow( pdiam/2, 3.0 );

  const Expr::Tag prtDiamTag     ( "Particle_Diameter",     Expr::STATE_N );
  const Expr::Tag initPrtDiamTag ( "init_Particle_Diameter",Expr::STATE_N );
  const Expr::Tag tempPTag       ( "Particle_Temperature",  Expr::STATE_N );
  const Expr::Tag tempGTag       ( "Gas_Temperature",       Expr::STATE_N );
  const Expr::Tag totalMWTag     ( "Gas_Total_Mw",          Expr::STATE_N );
  const Expr::Tag prtDensTag     ( "Particle Density",      Expr::STATE_N );
  const Expr::Tag initPrtDensTag ( "init_Particle_Density", Expr::STATE_N );
  const Expr::Tag intGasPressTag ( "Gas_Pressure" ,         Expr::STATE_N );
  const Expr::Tag prtMassTag     ( "Particle_Mass",         Expr::STATE_N );
  const Expr::Tag initprtMassTag ( "init_Particle_Mass",    Expr::STATE_N );
  const Expr::Tag rePtag         ( "Re_of_Particle",        Expr::STATE_N );
  const Expr::Tag scGTag         ( "Sc_of_Gas",             Expr::STATE_N );

  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( prtDiamTag,    pdiam  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( initPrtDiamTag,pdiam  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( tempGTag,      ptemp  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( totalMWTag,    29     ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( prtDensTag,    pdens  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( initPrtDensTag,pdens  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( intGasPressTag,101325 ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( prtMassTag,    pmass  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( initprtMassTag,pmass  ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( rePtag,        1E5    ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( scGTag,        1      ) );

  coal::SpeciesTagMap specTagMap;
  specTagMap[ coal::O2  ] = Expr::Tag( "Gas_O2", Expr::STATE_N );
  specTagMap[ coal::CO2 ] = Expr::Tag( "Gas_CO2",Expr::STATE_N );
  specTagMap[ coal::CO  ] = Expr::Tag( "Gas_CO", Expr::STATE_N );
  specTagMap[ coal::H2  ] = Expr::Tag( "Gas_H2", Expr::STATE_N );
  specTagMap[ coal::H2O ] = Expr::Tag( "Gas_H2O",Expr::STATE_N );
  specTagMap[ coal::CH4 ] = Expr::Tag( "Gas_CH4",Expr::STATE_N );

  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder(specTagMap[coal::O2 ],0.01) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder(specTagMap[coal::CO2],0.0 ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder(specTagMap[coal::CO ],0.0 ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder(specTagMap[coal::H2 ],0.0 ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder(specTagMap[coal::H2O],0.0 ) );
  exprFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder(specTagMap[coal::CH4],0.0 ) );

  coal::CoalInterface<FieldT> coaltest( coalType,
      DEV::CPDM, CHAR::LH,
      prtDiamTag, tempPTag, tempGTag, totalMWTag, prtDensTag, intGasPressTag,
      prtMassTag, rePtag, scGTag, specTagMap, initprtMassTag, initPrtDensTag,
      initPrtDiamTag );

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::TimeStepper timeIntegrator( exprFactory, method, "timestepper", patch.id());
  coaltest.register_expressions( exprFactory );

  const Expr::Tag prttempRHSt = coaltest.particle_temperature_rhs_tag();
  const Expr::Tag coalcpt     = coaltest.coal_heat_capacity_tag();
  const Expr::Tag heattogast  = coaltest.heat_released_to_gas_tag();

  const int nghost = 0;
  timeIntegrator.add_equation<FieldT>( tempPTag.name(), prttempRHSt, nghost );

  coaltest.hook_up_time_integrator( timeIntegrator, exprFactory );
  timeIntegrator.get_tree()->insert_tree(exprFactory.get_id(heattogast), false);

  timeIntegrator.finalize( fml,
                           patch.operator_database(),
                           patch.field_info() );
  timeIntegrator.get_tree()->lock_fields(fml);

  fml.field_manager<FieldT>().field_ref(prtMassTag) <<= pmass;
  fml.field_manager<FieldT>().field_ref(tempPTag) <<= ptemp;
  coaltest.set_initial_conditions( fml );

  const Expr::TagList& prtmassRHSt = coaltest.particle_mass_rhs_taglist();
  const Expr::Tag& mvtag       = coaltest.mass_volatile_tag();
  const Expr::Tag& chartag     = coaltest.mass_char_tag();
  const Expr::Tag& moisturetag = coaltest.mass_moisture();
  const Expr::Tag& volrhsrag   = Expr::Tag("dev_volatile_RHS", Expr::STATE_N);

  const FieldT& prttemp   = fml.field_manager<FieldT>().field_ref(tempPTag);
  const FieldT& prtcp     = fml.field_manager<FieldT>().field_ref(coalcpt);
  const FieldT& prttrhs   = fml.field_manager<FieldT>().field_ref(prttempRHSt);
  const FieldT& heattogas = fml.field_manager<FieldT>().field_ref(heattogast);
  const FieldT& mv        = fml.field_manager<FieldT>().field_ref( mvtag );
  const FieldT& charm     = fml.field_manager<FieldT>().field_ref( chartag);
  const FieldT& mmoisture = fml.field_manager<FieldT>().field_ref( moisturetag );
  const FieldT& volrhs    = fml.field_manager<FieldT>().field_ref( volrhsrag );
//  const FieldT& co2rhs  = fml.field_manager<FieldT>().field_ref();
//  const FieldT& corhs   = fml.field_manager<FieldT>().field_ref(prttempRHSt);
//  const FieldT& evaorhs = fml.field_manager<FieldT>().field_ref(prttempRHSt);


  // Writing tree plot in tree.dot
  {
    std::ofstream ofile("tree.dot");
    timeIntegrator.get_tree()->write_tree( ofile );
  }

  std::ostringstream savedname;
  savedname << "coal_" << ptemp <<".txt";
  cout << savedname.str() << endl;
  std::ofstream fout( savedname.str().c_str(), std::ios_base::trunc|std::ios_base::out );
  fout.width(10);
  fout.precision(5);

  fout << "#  Results for Coal model " << coal_type_name( coalType ) << endl
       << "#"
       << std::setw(9 ) << "time (s)"
       << std::setw(10) << "ptemp (K)"
       << std::setw(12) << "cp (J/kg)"
       << std::setw(12) << "dT/dt (K/s)"
       << std::setw(12) << "Htogas (J/s)"
       << std::setw(12) << "mv (kg)"
       << std::setw(12) << "char (kg)"
       << std::setw(13) << "moisture (kg)"
       << std::setw(13) << "volrhs (kg/s)"
       << endl;

  for( double t=0.0; t<endt; t+=dt ){

    timeIntegrator.step(dt);

    fout << std::setw(10) << std::setprecision(2) << t
         << std::setw(10) << std::setprecision(6) << prttemp[0]
         << std::setw(12) << std::setprecision(5) << prtcp[0]
         << std::setw(12) << std::setprecision(5) << prttrhs[0]
         << std::setw(12) << std::setprecision(5) << heattogas[0]
         << std::setw(12) << std::setprecision(5) << mv[0]
         << std::setw(12) << std::setprecision(5) << charm[0]
         << std::setw(13) << std::setprecision(5) << mmoisture[0]
         << std::setw(13) << std::setprecision(5) << volrhs[0]
         << endl;
  }

  fout.close();

  cout << "\n***  Integration is done over 0 to " << endt << endl;

  return true;
}

//--------------------------------------------------------------------

int main( int narg, char* arg[] )
{
  using std::cout;
  using std::endl;

  double pdiam, ptemp, dt, tend;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message\n" )
      ( "pdiam",       po::value<double>(&pdiam)->default_value(50.0e-6), "Particle diameter (m)" )
      ( "temperature", po::value<double>(&ptemp)->default_value(1000.0 ), "Temperature (K) of the system" )
      ( "timestep",    po::value<double>(&dt   )->default_value(2.0e-7 ), "Timestep (s)" )
      ( "endtime",     po::value<double>(&tend )->default_value(1.0e-4 ), "End time (s)" );

    po::variables_map vm;

    po::store( po::parse_command_line(narg,arg,desc), vm );
    po::notify( vm );

    if( vm.count("help") ) {
      cout << desc << endl;
      return 1;
    }
  }
  catch( po::unknown_option& e ){
    cout << e.what() << endl;
    return 1;
  }

  cout << "T  = " << ptemp << endl
       << "dt = " << dt << endl
       << "end time = " << tend << endl
       << endl;

  try{
    const bool passed = setup_and_test_integrator( Expr::SSPRK3,
                                                   coal::NorthDakota_Lignite,
                                                   pdiam, ptemp, dt, tend );
    if( passed ) return 0;
  }
  catch( std::exception& e ){
    cout << e.what() << endl;
  }
  return -1;
};
