#include <iostream>
#include <stdexcept>

#include <expression/ExprLib.h>
#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include "CoalInterface.h"


#include <spatialops/particles/ParticleFieldTypes.h>
typedef SpatialOps::Particle::ParticleField  FieldT;

typedef Expr::ExprPatch  PatchT;

using std::string;
using std::vector;
using std::cout;
using std::endl;


std::ostream& operator<<( std::ostream& os, const Expr::TagList tags )
{
  for( Expr::TagList::const_iterator i=tags.begin(); i!=tags.end(); ++i ){
    os << *i << endl;
  }
  return os;
}

//--------------------------------------------------------------------

int main()
{
  Expr::ExpressionFactory factory;

  const CanteraObjects::Setup options("Mix", "gri30.cti", "gri30");
  CanteraObjects::setup_cantera(options, 1);

  Expr::Tag
    prtDiamTag    ("Particle Diameter",              Expr::STATE_N ),
    initPrtDiamTag("initial Particle Diameter",      Expr::STATE_N ),
    tempPTag      ("Particle temperature",           Expr::STATE_N ),
    tempPGag      ("Gas temperature",                Expr::STATE_N ),
    mwTag         ("molecular weight",               Expr::STATE_N ),
    prtDensTag    ("particle density",               Expr::STATE_N ),
    initPrtDensTag("initial particle density",       Expr::STATE_N ),
    pressTag      ("pressure",                       Expr::STATE_N ),
    prtMassTag    ("particle mass",                  Expr::STATE_N ),
    initPrtMassTag("initial particle mass",          Expr::STATE_N ),
    rePTag        ("Re of Particle",                 Expr::STATE_N ),
    scGTag        ("Sc of Gas",                      Expr::STATE_N ),
    h2oMassTag    ("h2o mass fraction in gas phase", Expr::STATE_N );
   

  coal::SpeciesTagMap specTagMap;
  specTagMap[ coal::O2  ] = Expr::Tag("y1", Expr::STATE_N );
  specTagMap[ coal::CO2 ] = Expr::Tag("y3", Expr::STATE_N );
  specTagMap[ coal::CO  ] = Expr::Tag("y2", Expr::STATE_N );
  specTagMap[ coal::H2  ] = Expr::Tag("y5", Expr::STATE_N );
  specTagMap[ coal::H2O ] = Expr::Tag("y6", Expr::STATE_N );
  specTagMap[ coal::CH4 ] = Expr::Tag("y7", Expr::STATE_N );

  try{
    std::cout<<"Going to call CoalInterface ! "<<std::endl;
    coal::CoalInterface<FieldT> coal( coal::NorthDakota_Lignite, DEV::KOBAYASHIM, CHAR::LH, prtDiamTag, tempPTag, tempPGag, mwTag,
                                      prtDensTag, pressTag, prtMassTag, rePTag, scGTag,specTagMap, initPrtMassTag, initPrtDensTag,
                                      initPrtDiamTag );

    Expr::TagList tags;
    std::cout<<"Going to doing the Species source term"<<std::endl;
    /*
    tags = coal.gas_species_source_term( coal::CO2, true  );  cout << "CO2 rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::CH4, true  );  cout << "CH4 rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::H,   true  );  cout << "H rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::O2,  false );  cout << "O2 rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::CO,  true  );  cout << "CO rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::H2,  false );  cout << "H2 rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::H2O, true  );  cout << "H2O rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::HCN, true  );  cout << "HCN rhs tags: " << endl << tags << endl;
    tags = coal.gas_species_source_term( coal::NH3, false );  cout << "NH3 rhs tags: " << endl << tags << endl;
  */}
  catch( std::exception& e ){
    cout << endl
         << "ERROR FOUND!" << endl
         << e.what() << endl
         << endl;
    return -1;
  }
  return 0;
}
