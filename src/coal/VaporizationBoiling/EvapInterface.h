#ifndef EvapInterface_h
#define EvapInterface_h


/**
 *  \file EvapInterface.h
 *  \
 *
 */
#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>

#include <coal/CoalData.h>


namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace EVAP{

  enum EvapSpecies{
    H2O = 0,
    INVALID_SPECIES = 99
  };


  /**
   *  \ingroup Evaporation
   *  \class EvapInterface
   *
   */
  template< typename FieldT >
  class EvapInterface
  {
    const coal::CoalComposition coalcomp_;
    const coal::CoalType ct_;
    const Expr::Tag tempGTag_, tempPTag_, diamPTag_, rePTag_, scGTag_, dgTag_, waterMasFracTag_, totalMWTag_, gasPressureTag_, prtmasst_;
    const Expr::Tag moistureMasst_, evaporationRHSt_;

    EvapInterface(); // no copying
    EvapInterface& operator=( const EvapInterface& );  // no assignment

  public:

    /**
     *  \param tempGTag gas phase temperature at the particle location (K)
     *  \param tempPTag particle temperature (K)
     *  \param diamPTag particle diameter (m)
     *  \param rePTag   particle reynolds number
     *  \param scGTag   schmidt number of Gas
     *  \param dgTag    Diffusivity of H2O in the gas pahse -air, (m2/s)
     *  \param waterMasFracTag : Mass Fraction of Water in the gas phase
     *  \param totalMWTag : Total molecular weight of gas pahse - Equation requires partial pressure of water which could be calculated by mass fraction and total molecular weight
     *  \param gasPressureTag : Total Pressure of Gas Pahse
     */
     EvapInterface(const Expr::Tag tempGTag,
                   const Expr::Tag tempPTag,
                   const Expr::Tag diamPTag,
                   const Expr::Tag rePTag,
                   const Expr::Tag scGTag,
                   const Expr::Tag waterMasFracTag,
                   const Expr::Tag totalMWTag,
                   const Expr::Tag gasPressureTag,
                   const Expr::Tag prtmasstag,
                   const coal::CoalType ct);

    /**
     *  \brief retrieve the moisture tag.
     */
    const Expr::Tag retrieve_moisture_mass_tag() const{ return moistureMasst_; }
    /**
     *  \brief obtain the moisture production rate of particle
     */
    const Expr::Tag moisture_rhs_tag() const{ return evaporationRHSt_;}
    /**
     *  \brief obtain consumption rate of gas
     */
    Expr::Tag gas_species_rhs_tag( const EvapSpecies spec ) const;

    void register_expressions( Expr::ExpressionFactory& );

    void hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory );

    void set_initial_conditions( Expr::FieldManagerList& );

  };

} // namespace EVAP

#endif // EvapInterface_h
