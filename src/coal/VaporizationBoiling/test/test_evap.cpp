#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <coal/CoalData.h>
#include <expression/ExprLib.h>

#include <coal/VaporizationBoiling/EvapInterface.h>

typedef Expr::ExprPatch  PatchT;
#include <spatialops/particles/ParticleFieldTypes.h>
typedef SpatialOps::Particle::ParticleField  FieldT;

#include <boost/program_options.hpp>
namespace po = boost::program_options;


bool
setup_and_test_integrator( const Expr::TSMethod method,
                           const double temperature,
                           const double dt,
                           const double endt )
{
  using std::cout;
  using std::endl;

  PatchT patch(1,1,1,1);

  Expr::ExpressionFactory exprFactory;

  const double temperatureG = 1000;

  const Expr::Tag tempPTag        ( "Particle Temperature", Expr::STATE_N );
  const Expr::Tag tempGTag        ( "Gas Temperature",      Expr::STATE_N );
  const Expr::Tag waterMasFracTag ( "H2O_massFraction_Gas",     Expr::STATE_N );
  const Expr::Tag totalMWTag      ( "Total_Molecularweight_Gas",Expr::STATE_N );
  const Expr::Tag diamPTag        ( "Particle Diameter",    Expr::STATE_N );
  const Expr::Tag rePTag          ( "Re of Particle",       Expr::STATE_N );
  const Expr::Tag scGTag          ( "Sc of Gas",            Expr::STATE_N );
  const Expr::Tag gasPressureTag  ( "Pressure",             Expr::STATE_N );
  const Expr::Tag prtmasstag      ( "Particle Mass",        Expr::STATE_N );
  const Expr::Tag timeTag         ( "time",                 Expr::STATE_N );

  typedef Expr::ConstantExpr<FieldT>::Builder ConstExpr;

  exprFactory.register_expression( new ConstExpr(tempPTag,       temperature ) );
  exprFactory.register_expression( new ConstExpr(tempGTag,       temperatureG) );
  exprFactory.register_expression( new ConstExpr(diamPTag,       100E-6      ) );
  exprFactory.register_expression( new ConstExpr(rePTag,         10000.0     ) );
  exprFactory.register_expression( new ConstExpr(scGTag,         1.0         ) );
  exprFactory.register_expression( new ConstExpr(waterMasFracTag,0.0         ) );
  exprFactory.register_expression( new ConstExpr(totalMWTag,     29.0        ) );
  exprFactory.register_expression( new ConstExpr(gasPressureTag, 1.013E5     ) );

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::TimeStepper timeIntegrator( exprFactory, method, "timestepper", patch.id(), timeTag );

  EVAP::EvapInterface<FieldT>* evapInterface;
  try{
    evapInterface = new EVAP::EvapInterface<FieldT>(tempGTag,tempPTag,diamPTag,rePTag,scGTag,waterMasFracTag,totalMWTag,gasPressureTag, prtmasstag, coal::NorthDakota_Lignite);
    evapInterface->register_expressions( exprFactory );
    evapInterface->hook_up_time_integrator( timeIntegrator, exprFactory );
    fml.field_manager<FieldT>().register_field( prtmasstag, 0 );
    timeIntegrator.finalize( fml, patch.operator_database(), patch.field_info() );
    timeIntegrator.get_tree()->lock_fields(fml);

    {
      std::ofstream fout("evap_tree.dot");
      timeIntegrator.get_tree()->write_tree( fout );
    }

    FieldT& prtmas = fml.field_manager<FieldT>().field_ref( prtmasstag );
    prtmas <<= 5.0e-10;  // kg

    evapInterface->set_initial_conditions( fml );
  }
  catch( std::exception& e ){
    cout << endl
         << "ERROR in seting up the evapInterface" << endl
         << e.what()
         << endl;
    return false;
  }

  const FieldT& moistureMass = fml.field_manager<FieldT>().field_ref( evapInterface->retrieve_moisture_mass_tag());
  const FieldT& H2Orhsfield  = fml.field_manager<FieldT>().field_ref( evapInterface->moisture_rhs_tag()          );

  {
    std::ofstream ofile("tree.dot");
    timeIntegrator.get_tree()->write_tree( ofile );
  }

  std::ostringstream savedname;
  savedname << "EVAP_" << temperature <<".txt";
  cout << savedname.str() << endl;
  std::ofstream fout( savedname.str().c_str(), std::ios_base::trunc|std::ios_base::out );

  fout.width(10);
  fout.precision(7);

  fout << "  This is the result of EVAP Oxidation" << endl;
  fout << "  Created by : Babak Goshayeshi - PhD student" << endl;
  fout << "               University of Utah - Institute for Clean and Secure Energy" << endl << endl << endl;
  fout << std::setw(15) << "time" << std::setw(15) << "Moist kg" << std::setw(15) << "Moist rhs kg/s" << endl;

  for( double t=0.0; t<endt; t+=dt ){

    fout << std::setw(15) << std::setprecision(7) << t
         << std::setw(15) << std::setprecision(7) << moistureMass[0];

    timeIntegrator.step(dt);

    fout << std::setw(15) << std::setprecision(7) << H2Orhsfield[0];
    fout << endl;

  }

  fout.close();
  cout << "\n***  Integeration was done over 0 to " << endt << endl;

  delete evapInterface;

  return true;
}



int main( int narg, char* arg[] )
{
  using std::cout;
  using std::endl;

  double temp, dt, tend;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message\n" )
      ( "temperature", po::value<double>(&temp)->default_value(1000.0), "Temperature (K) of the Gas" )
      ( "timestep",    po::value<double>(&dt  )->default_value(2e-7), "Timestep (s)" )
      ( "endtime",     po::value<double>(&tend)->default_value(1e-3 ), "End time (s)" );

    po::variables_map vm;

    po::store( po::parse_command_line(narg,arg,desc), vm );
    po::notify( vm );

    if( vm.count("help") ) {
      cout << desc << endl;
      return 1;
    }
  }
  catch( po::unknown_option& e ){
    cout << e.what() << endl;
    return 1;
  }

  cout << "T  = " << temp << endl
       << "dt = " << dt << endl
       << "end time = " << tend << endl
       << endl;

  try{
    const bool passed = setup_and_test_integrator( Expr::SSPRK3, temp, dt, tend );
    if( passed ) return 0;
  }
  catch( std::exception& e ){
    cout << e.what() << endl;
  }
  return -1;
};
