#include <stdexcept>
#include <sstream>

#include "EvapInterface.h"
#include "Vaporization_RHS.h"

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/Nebo.h>

#include "cantera/thermo/VPStandardStateTP.h"
#include "cantera/thermo/PDSS_Water.h"

#include <expression/ClipValue.h>

using std::ostringstream;
using std::endl;

namespace EVAP{

  //------------------------------------------------------------------

  template< typename FieldT >
  EvapInterface<FieldT>::
  EvapInterface( const Expr::Tag tempGTag,
                 const Expr::Tag tempPTag,
                 const Expr::Tag diamPTag,
                 const Expr::Tag rePTag,
                 const Expr::Tag scGTag,
                 const Expr::Tag waterMasFracTag,
                 const Expr::Tag totalMWTag,
                 const Expr::Tag gasPressureTag,
                 const Expr::Tag prtmasstag,
                 const coal::CoalType ct)
    : coalcomp_( ct       ),
      ct_      ( ct       ),
      tempGTag_( tempGTag ),
      tempPTag_( tempPTag ),
      diamPTag_( diamPTag ),
      rePTag_  ( rePTag   ),
      scGTag_  ( scGTag   ),
      waterMasFracTag_( waterMasFracTag ),
      totalMWTag_     ( totalMWTag      ),
      gasPressureTag_ ( gasPressureTag  ),
      prtmasst_       ( prtmasstag      ),
      moistureMasst_  ( "moisture_mass",   Expr::STATE_N ),  // jcs should these be hard-coded here???
      evaporationRHSt_( "evaporation_rhs", Expr::STATE_N )
  {
    ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << endl;
    bool foundError = false;

    if( tempGTag        == Expr::Tag() ){ foundError=true;  msg << "gas temperature tag is invalid." << endl; }
    if( tempPTag        == Expr::Tag() ){ foundError=true;  msg << "particle temperature tag is invalid" << endl; }
    if( diamPTag        == Expr::Tag() ){ foundError=true;  msg << "particle diameter tag is invalid" << endl; }
    if( rePTag          == Expr::Tag() ){ foundError=true;  msg << "particle reynolds number tag is invalid" << endl; }
    if( scGTag          == Expr::Tag() ){ foundError=true;  msg << "shmidt number tag is invalid" << endl; }
    if( waterMasFracTag == Expr::Tag() ){ foundError=true;  msg << "water mass fraction tag is invalid" << endl; }
    if( totalMWTag      == Expr::Tag() ){ foundError=true;  msg << "total molecular weight tag is invalid" << endl; }
    if( gasPressureTag  == Expr::Tag() ){ foundError=true;  msg << "gas pressure tag is invalid" << endl; }
    if( prtmasstag      == Expr::Tag() ){ foundError=true;  msg << "particle mass tag is invalid" << endl; }

    if( foundError ){
      throw std::runtime_error( msg.str() );
    }
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  EvapInterface<FieldT>::
  register_expressions( Expr::ExpressionFactory& factory )
  {
    factory.register_expression(
        new typename Vaporization_RHS<FieldT>::Builder( evaporationRHSt_,
                                                        tempGTag_, tempPTag_, diamPTag_, rePTag_,
                                                        scGTag_, waterMasFracTag_, totalMWTag_,
                                                        gasPressureTag_, moistureMasst_ ) );

    // ensure that values do not become negative.
    const Expr::Tag clip( moistureMasst_.name()+"_cip", Expr::STATE_NONE );
    typedef Expr::ClipValue<FieldT> Clipper;
    factory.register_expression( new typename Clipper::Builder( clip, 0.0, 0.0, Clipper::CLIP_MIN_ONLY ) );
    factory.attach_modifier_expression( clip, moistureMasst_ );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  Expr::Tag
  EvapInterface<FieldT>::
  gas_species_rhs_tag( const EvapSpecies spec ) const
  {
    if( spec == INVALID_SPECIES ) return Expr::Tag();
    return evaporationRHSt_;
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  EvapInterface<FieldT>::
  hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory )
  {
    const int nghost = 0;
    integrator.add_equation<FieldT>( moistureMasst_.name(), evaporationRHSt_, nghost );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  EvapInterface<FieldT>::
  set_initial_conditions( Expr::FieldManagerList& fml )
  {
    using namespace SpatialOps;
    FieldT& moistureMass = fml.field_manager<FieldT>().field_ref( moistureMasst_ );
    const FieldT& prtmas = fml.field_manager<FieldT>().field_ref( prtmasst_      );
    moistureMass <<= coalcomp_.get_moisture() * prtmas;
  }

  //------------------------------------------------------------------


  //========================================================================
  // Explicit template instantiation for supported versions of this expression
  template class EvapInterface< SpatialOps::Particle::ParticleField >;
  //========================================================================


} // namespace EVAP
