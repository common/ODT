#include "CharInterface.h"

#include <expression/ClipValue.h>

#include <stdexcept>
#include <sstream>

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

using std::ostringstream;
using std::endl;
using std::cout;
using Expr::Tag;
using Expr::TagList;
using Expr::STATE_N;
using Expr::STATE_NONE;

namespace CHAR{

  template< typename FieldT >
  CharInterface<FieldT>::
  CharInterface( const Tag& prtDiamTag,
                 const Tag& tempPTag,
                 const Tag& tempGTag,
                 const Tag& co2MassFracTag,
                 const Tag& coMassFracTag,
                 const Tag& o2MassFracTag,
                 const Tag& h2MassFracTag,
                 const Tag& h2oMassFracTag,
                 const Tag& ch4MassFracTag,
                 const Tag& mixMwTag,
                 const Tag& prtDensTag,
                 const Tag& intGasPressureTag,
                 const Tag& prtMassTag,
                 const Tag& initPrtmassTag,
                 const Tag& initPrtDensTag,
                 const Tag& initPrtDiamTag,
                 const Tag& volatilesTag,
                 const coal::CoalType coalType,
                 const DEV::DevModel  dvmodel,
                 const CharModel      chmodel )
  {
    const coal::StringNames& sName = coal::StringNames::self();

    switch (chmodel) {
    case LH:
    case FRACTAL:
      lhModel_ = new LH::LangmuirInterface<FieldT>
                     ( prtDiamTag,
                       tempPTag,
                       tempGTag,
                       co2MassFracTag,
                       o2MassFracTag,
                       h2oMassFracTag,
                       mixMwTag,
                       prtDensTag,
                       intGasPressureTag,
                       prtMassTag,
                       initPrtmassTag,
                       coalType,
                       dvmodel,
                       chmodel );

      charModel_ = lhModel_;
      break;

    case FIRST_ORDER:
      firstOrderModel_ = new FOA::FirstOrderInterface<FieldT>
                     ( prtDiamTag,
                       tempPTag,
                       tempGTag,
                       co2MassFracTag,
                       coMassFracTag,
                       o2MassFracTag,
                       h2MassFracTag,
                       h2oMassFracTag,
                       ch4MassFracTag,
                       mixMwTag,
                       prtDensTag,
                       intGasPressureTag,
                       prtMassTag,
                       initPrtmassTag,
                       coalType,
                       dvmodel );

      charModel_ = firstOrderModel_;
      break;

    case CCK:
      cckModel_ = new CCK::CCKInterface<FieldT>
                     ( prtDiamTag,
                       tempPTag,
                       tempGTag,
                       co2MassFracTag,
                       coMassFracTag,
                       o2MassFracTag,
                       h2MassFracTag,
                       h2oMassFracTag,
                       ch4MassFracTag,
                       mixMwTag,
                       intGasPressureTag,
                       prtMassTag,
                       initPrtmassTag,
                       initPrtDensTag,
                       initPrtDiamTag,
                       volatilesTag,
                       coalType,
                       dvmodel );

      charModel_ = cckModel_;
      break;

    case INVALID_CHARMODEL:
      throw std::invalid_argument( "Invalid model in CharInterface constructor" );
    }

    // Ensure tags passed to CharInterface are valid.
    bool foundError = false;
    ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << endl;
    if( prtDiamTag        == Tag() ){ foundError=true;  msg << "Particle diameter tag is invalid." << endl; }
    if( prtMassTag        == Tag() ){ foundError=true;  msg << "Particle mass tag is invalid" << endl; }
    if( initPrtDensTag    == Tag() ){ foundError=true;  msg << "Initial particle mass tag is invalid" << endl; }
    if( prtDensTag        == Tag() ){ foundError=true;  msg << "Particle density tag is invalid" << endl; }
    if( initPrtDensTag    == Tag() ){ foundError=true;  msg << "Initial particle density tag is invalid" << endl; }
    if( volatilesTag      == Tag() ){ foundError=true;  msg << "Volatiles tag is invalid" << endl; }
    if( tempPTag          == Tag() ){ foundError=true;  msg << "Particle temperature tag is invalid." << endl; }
    if( tempGTag          == Tag() ){ foundError=true;  msg << "Gas temperature tag is invalid." << endl; }
    if( o2MassFracTag     == Tag() ){ foundError=true;  msg << "O2 mass fraction tag is invalid" << endl; }
    if( co2MassFracTag    == Tag() ){ foundError=true;  msg << "CO2 mass fraction tag is invalid" << endl; }
    if( coMassFracTag     == Tag() ){ foundError=true;  msg << "CO mass fraction tag is invalid" << endl; }
    if( h2MassFracTag     == Tag() ){ foundError=true;  msg << "H2 mass fraction tag is invalid" << endl; }
    if( h2oMassFracTag    == Tag() ){ foundError=true;  msg << "H2O mass fraction tag is invalid" << endl; }
    if( ch4MassFracTag    == Tag() ){ foundError=true;  msg << "CH4 mass fraction tag is invalid" << endl; }
    if( mixMwTag          == Tag() ){ foundError=true;  msg << "mixture molecular weight tag is invalid" << endl; }
    if( intGasPressureTag == Tag() ){ foundError=true;  msg << "Pressure tag is invalid" << endl; }

    if( foundError ) throw std::invalid_argument( msg.str() );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  CharInterface<FieldT>::
  register_expressions( Expr::ExpressionFactory& factory )
  {
   cout<<std::endl<<"registering char model expressions...     ";
   charModel_->register_expressions( factory );

   // Ensure that values of Char do not become negative.
   const Tag charMassTag = charModel_->char_mass_tag();
   const Tag charMassClip( charMassTag.name()+"_clip", STATE_NONE );
   typedef Expr::ClipValue<FieldT> Clipper;
   factory.register_expression( new typename Clipper::Builder( charMassClip, 0.0, 0.0, Clipper::CLIP_MIN_ONLY ) );
   factory.attach_modifier_expression( charMassTag, charMassClip );

   cout<<endl<<"Registered expressions for char model";
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  CharInterface<FieldT>::
  hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory )
  {
    charModel_->hook_up_time_integrator( integrator, factory );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  CharInterface<FieldT>::
  set_initial_conditions( Expr::FieldManagerList& fml )
  {
    charModel_->set_initial_conditions(fml);
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const TagList&
  CharInterface<FieldT>::
  gas_species_rhs_tags() const
  {
    return charModel_->gas_species_rhs_tags();
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  Tag
  CharInterface<FieldT>::
  gas_species_rhs_tag( const CharGasSpecies spec ) const
  {
    return charModel_->gas_species_rhs_tag(spec);
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag&
  CharInterface<FieldT>::
  char_mass_tag() const
  {
    return charModel_->char_mass_tag();
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag&
  CharInterface<FieldT>::
  char_consumption_rate_tag() const
  {
    return charModel_->char_consumption_rate_tag();
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag&
  CharInterface<FieldT>::
  char_gasification_co2_rate_tag() const
  {
    return charModel_->char_gasification_co2_rate_tag();
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag&
  CharInterface<FieldT>::
  char_gasification_h2o_rate_tag() const
  {
    return charModel_->char_gasification_h2o_rate_tag();
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag&
  CharInterface<FieldT>::
  co2coratio_tag() const
  {
    return charModel_->co2coratio_tag();
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag&
  CharInterface<FieldT>::
  oxidation_tag() const
  {
    return charModel_->oxidation_tag();
  }

//==========================================================================
// Explicit template instantiation for supported versions of this expression
template class CharInterface< SpatialOps::Particle::ParticleField >;
//==========================================================================


} // namespace CHAR
