#ifndef LangmuirInterface_h
#define LangmuirInterface_h

#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>
#include <coal/StringNames.h>
#include <coal/Devolatilization/DevolatilizationBase.h>
#include <coal/CharOxidation/CharBase.h>
#include <coal/CharOxidation/CharData.h>

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }
namespace CHAR{ class CharBase;}

namespace LH{


/**
 *  \ingroup CharOxidation
 *  \class   LangmuirInterface
 *  \author  Josh McConnell
 *  \date    December 2015
 *
 *  \brief Provides an interface for the Langmuir-Hinshelwood char oxidation model
 *
 */
  template< typename FieldT >
  class LangmuirInterface: public CHAR::CharBase
  {
    //particle and gas values
    const Expr::Tag tempPTag_,       tempGTag_,     mixMwTag_,    prtDensTag_,
                    gasPressureTag_, prtDiamTag_,   prtMassTag_,  initPrtMassTag_;

    //mass fractions and species RHSs
    const Expr::Tag o2MassFracTag_, h2oMassFracTag_, co2MassFracTag_,
                    o2RhsTag_,      h2oRhsTag_,      h2RhsTag_,
                    co2RhsTag_,     coRhsTag_;

    Expr::TagList co2CoTags_, char_co2coTags_, h2andh2oRhsTags_;

    Expr::ExpressionID oxidationRHSID_, co2coRHSID_, o2RHSID_, gasifco2ID_, gasifh2oID_,
                 charRHSID_, h2Andh2oRHSID_;

    LangmuirInterface(); // no copying
    LangmuirInterface& operator=( const LangmuirInterface& );  // no assignment

    const bool initDevChar_;  ///< Initial char in volatile matter (Only with CPD Model)
    const CHAR::CharModel         charModel_;
    const CHAR::CharOxidationData charData_;

  public:

    /**
     *  \param prtDiamTag     particle diameter
     *  \param tempPTag       particle temperature
     *  \param tempGTag       gas phase temperature
     *  \param co2MassFracTag gas-phase CO2 mass fraction at the particle surface
     *  \param o2MassFracTag  gas-phase O2 mass fraction at the particle surface
     *  \param h2oMassFracTag gas-phase H2O mass fraction at the particle surface
     *  \param mixMwTag       gas-phase mixture molecular weight at the particle surface
     *  \param prtDensTag     gas-phase mixture mass density at the particle surface
     *  \param gasPressureTag gas phase pressure
     *  \param prtMassTag     particle mass
     *  \param initPrtmassTag initial particle mass
     *  \param coalType       the name of the coal
     *  \param devModel       devolatilization model
     *  \param chModel        char oxidation model
     */
    LangmuirInterface(   const Expr::Tag& prtDiamTag,
                         const Expr::Tag& tempPTag,
                         const Expr::Tag& tempGTag,
                         const Expr::Tag& co2MassFractag,
                         const Expr::Tag& o2MassFracTag,
                         const Expr::Tag& h2oMassFractag,
                         const Expr::Tag& mixMwTag,
                         const Expr::Tag& prtDensTag,
                         const Expr::Tag& gasPressureTag,
                         const Expr::Tag& prtMassTag,
                         const Expr::Tag& initPrtMassTag,
                         const coal::CoalType coalType,
                         const DEV::DevModel dvmodel,
                         const CHAR::CharModel chmodel );

    /**
     *  \brief registers all expressions relevant to evaluation of the
     *         specified char oxidation model
     */
    void register_expressions( Expr::ExpressionFactory& );


    /**
     *  \brief add char oxidation equations onto the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory );

    /**
     *  \brief set the initial conditions for char
     */
    void set_initial_conditions( Expr::FieldManagerList& );

    /**
     *  \brief obtain the Tag for the requested species
     */
    const Expr::Tag gas_species_rhs_tag( const CHAR::CharGasSpecies spec ) const;

   /**
    * \brief Return the ID of Co2 and Co consumption in gas pahse
    */
    Expr::ExpressionID co2co_rhs_ID() const{ return co2coRHSID_; }

    /**
     * \brief Return the ID for Oxygen consumption is gas pahse
     */
    Expr::ExpressionID o2_rhs_ID() const{ return o2RHSID_; }

    /**
     * \brief Return the ID of H2 and H2O consumption of gas
     */
    Expr::ExpressionID h2andh2orhsID() const{ return h2Andh2oRHSID_; }

  };

} // namespace LH

#endif // LangmuirInterface_h
