#ifndef CCKInterface_h
#define CCKInterface_h

#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>
#include <coal/StringNames.h>
#include <coal/Devolatilization/DevolatilizationBase.h>
#include <coal/CharOxidation/CharBase.h>
#include <coal/CharOxidation/CharData.h>
#include "CCKData.h"

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }
namespace CHAR{ class CharBase;}

namespace CCK{


  /**
   *  \ingroup CharOxidation
   *  \class   LangmuirInterface
   *  \author  Josh McConnell
   *  \date    December 2015
   *
   *  \brief Provides an interface for the CCK model
   *
   */
  template< typename FieldT >
  class CCKInterface: public CHAR::CharBase
  {
    //particle and gas property tags;
    const Expr::Tag tempGTag_, mixMwTag_, gasPressureTag_;

    //mass fractions and species RHSs
    const Expr::Tag o2MassFracTag_, h2oMassFracTag_, h2MassFracTag_, ch4MassFracTag_, co2MassFracTag_, coMassFracTag_,
                    co2RhsTag_,     coRhsTag_,       o2RhsTag_,      h2oRhsTag_,      h2RhsTag_,       ch4RhsTag_;

    // particle property tags
    const Expr::Tag initPrtDiamTag_,  ashPorosityTag_,    ashThicknessTag_,  coreDiamTag_,     coreDensityTag_,
                    thermAnnealTag_,  charConversionTag_, ashDensityTag_,    ashMassFracTag_,  charMassFracTag_,
                    devolDensityTag_, prtDiamModTag_,     initDevolDensTag_, initCoreDensTag_, tempPTag_,
                    initPrtDensTag_,  prtDiamTag_,       prtMassTag_,        initPrtMassTag_,  volatilesTag_,
                    devolAshMassFracTag_, devolCharMassFracTag_;

    Expr::TagList massFracTags_, prtPropertyTags_, gasPropertyTags_, charDepletionTags_,
                  charRhsTags_,  logFreqDistTags_, logFreqDistRHSTags_;

    CCKInterface(); // no copying
    CCKInterface& operator=( const CCKInterface& );  // no assignment

    const bool initDevChar_;  ///< Initial char in volatile matter (Only with CPD Model)
    const CCKData cckData_;

  public:

    /**
     *  \param prtDiamTag     particle diameter
     *  \param tempPTag       particle temperature
     *  \param tempGTag       gas phase temperature
     *  \param co2MassFracTag gas-phase CO2 mass fraction at the particle surface
     *  \param coMassFracTag  gas-phase CO mass fraction at the particle surface
     *  \param o2MassFracTag  gas-phase O2 mass fraction at the particle surface
     *  \param h2MassFracTag  gas-phase H2 mass fraction at the particle surface
     *  \param h2oMassFracTag gas-phase H2O mass fraction at the particle surface
     *  \param ch4MassFracTag gas-phase CH4 mass fraction at the particle surface
     *  \param mixMwTag       gas-phase mixture molecular weight at the particle surface
     *  \param prtDensTag     gas-phase mixture mass density at the particle surface
     *  \param gasPressureTag gas phase pressure
     *  \param prtMassTag     particle mass
     *  \param initPrtmassTag initial particle mass
     *  \param initPrtDensTag initial particle density
     *  \param initPrtDiamTag initial particle diameter
     *  \param volatilesTag   volatile mass within the coal
     *  \param coalType       the name of the coal
     *  \param devModel       devolatilization model
     *  \param chModel        char oxidation model
     */
    CCKInterface( const Expr::Tag& prtDiamTag,
                  const Expr::Tag& tempPTag,
                  const Expr::Tag& tempGTag,
                  const Expr::Tag& co2MassFractag,
                  const Expr::Tag& coMassFractag,
                  const Expr::Tag& o2MassFracTag,
                  const Expr::Tag& h2MassFractag,
                  const Expr::Tag& h2oMassFractag,
                  const Expr::Tag& ch4MassFractag,
                  const Expr::Tag& totalMWTag,
                  const Expr::Tag& gasPressureTag,
                  const Expr::Tag& prtmast,
                  const Expr::Tag& initPrtmassTag,
                  const Expr::Tag& initPrtDensTag,
                  const Expr::Tag& initPrtDiamTag,
                  const Expr::Tag& volatilesTag,
                  const coal::CoalType coalType,
                  const DEV::DevModel devModel );

    /**
     *  \brief registers all expressions relevant to evaluation of the
     *         specified char oxidation model
     */
    void register_expressions( Expr::ExpressionFactory& );

    /**
     *  \brief add char oxidation equations onto the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory );

    /**
     *  \brief set the initial conditions for char
     */
    void set_initial_conditions( Expr::FieldManagerList& );

    /**
     *  \brief obtain the Tag for the requested species
     */
    const Expr::Tag gas_species_rhs_tag( const CHAR::CharGasSpecies spec ) const;

  };

} // namespace CCK

#endif // CCKInterface_h
