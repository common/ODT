#include <stdexcept>
#include <sstream>

#include <expression/ClipValue.h>

#include "coal/Devolatilization/CPD/c0_fun.h"
#include <coal/CoalData.h>

// includes for char conversion kinetics (CCK) model
#include "CCK.h"
#include "LogFrequencyDistributionRHS.h"
#include "InitialDevolatilizedDensity.h"
#include "InitialCoreDensity.h"
#include "DevolatilizedDensity.h"
#include "DevolatilizedMassFracs.h"
#include "AshFilm.h"
#include "CoreDensity.h"
#include "CharConversion.h"
#include "CharSpeciesRHS.h"
#include "ParticleDiameter.h"
#include "ThermalAnnealing.h"
#include "CCKInterface.h"

using std::ostringstream;
using std::endl;
using Expr::Tag;
using Expr::STATE_N;
using Expr::STATE_NONE;

namespace CCK{

  template< typename FieldT >
  CCKInterface<FieldT>::
  CCKInterface( const Tag& prtDiamTag,
                const Tag& tempPTag,
                const Tag& tempGTag,
                const Tag& co2MassFracTag,
                const Tag& coMassFracTag,
                const Tag& o2MassFracTag,
                const Tag& h2MassFracTag,
                const Tag& h2oMassFracTag,
                const Tag& ch4MassFracTag,
                const Tag& mixMwTag,
                const Tag& intGasPressureTag,
                const Tag& prtMassTag,
                const Tag& initPrtmassTag,
                const Tag& initPrtDensTag,
                const Tag& initPrtDiamTag,
                const Tag& volatilesTag,
                const coal::CoalType coalType,
                const DEV::DevModel devModel )
    : tempPTag_            ( tempPTag          ),
      tempGTag_            ( tempGTag          ),
      o2MassFracTag_       ( o2MassFracTag     ),
      co2MassFracTag_      ( co2MassFracTag    ),
      coMassFracTag_       ( coMassFracTag     ),
      h2MassFracTag_       ( h2MassFracTag     ),
      ch4MassFracTag_      ( ch4MassFracTag    ),
      h2oMassFracTag_      ( h2oMassFracTag    ),
      mixMwTag_            ( mixMwTag          ),
      gasPressureTag_      ( intGasPressureTag ),
      prtDiamTag_          ( prtDiamTag        ),
      prtMassTag_          ( prtMassTag        ),
      initPrtMassTag_      ( initPrtmassTag    ),
      initPrtDensTag_      ( initPrtDensTag    ),
      initPrtDiamTag_      ( initPrtDiamTag    ),
      volatilesTag_        (volatilesTag       ),
      o2RhsTag_            ( coal::StringNames::self().char_o2RHS,             STATE_N    ),
      co2RhsTag_           ( coal::StringNames::self().char_co2RHS,            STATE_N    ),
      coRhsTag_            ( coal::StringNames::self().char_coRHS,             STATE_N    ),
      h2RhsTag_            ( coal::StringNames::self().char_h2RHS,             STATE_N    ),
      h2oRhsTag_           ( coal::StringNames::self().char_h2oRHS,            STATE_N    ),
      ch4RhsTag_           ( coal::StringNames::self().char_ch4RHS,            STATE_N    ),
      ashPorosityTag_      (coal::StringNames::self().ash_porosity,            STATE_N    ),
      ashThicknessTag_     (coal::StringNames::self().ash_thickness,           STATE_N    ),
      coreDiamTag_         (coal::StringNames::self().core_diameter,           STATE_N    ),
      coreDensityTag_      (coal::StringNames::self().core_density,            STATE_N    ),
      thermAnnealTag_      (coal::StringNames::self().therm_anneal,            STATE_N    ),
      charConversionTag_   (coal::StringNames::self().char_conversion,         STATE_N    ),
      ashDensityTag_       (coal::StringNames::self().ash_density,             STATE_NONE ),
      ashMassFracTag_      (coal::StringNames::self().ash_mass_frac,           STATE_NONE ),
      charMassFracTag_     (coal::StringNames::self().char_mass_frac,          STATE_NONE ),
      devolDensityTag_     ("p_DevolatilizedDensity",                          STATE_NONE ),
      prtDiamModTag_       ("cck_p_size",                                      STATE_NONE ),
      initDevolDensTag_    ("Initial_p_DevolatilizedDensity",                  STATE_NONE ),
      devolAshMassFracTag_ (coal::StringNames::self().ash_mass_frac +"_d",     STATE_NONE ),
      devolCharMassFracTag_(coal::StringNames::self().char_mass_frac+"_d",     STATE_NONE ),
      initCoreDensTag_     ("Initial_"+coal::StringNames::self().core_density, STATE_NONE ),
      initDevChar_         ( devModel == DEV::CPDM ),
      cckData_             ( coalType             )
  {
    const coal::StringNames& sName = coal::StringNames::self();

    charMassTag_     = Tag( sName.char_charmass,  STATE_N );
    oxidationRhsTag_ = Tag( sName.char_oxid_RHS,  STATE_N );
    heteroCo2Tag_    = Tag( sName.char_gasifco2,  STATE_N );
    heteroH2oTag_    = Tag( sName.char_gasifh2o,  STATE_N );
    charRhsTag_      = Tag( sName.char_massRHS,   STATE_N );
    co2CoRatioTag_   = Tag( sName.char_coco2ratio,STATE_N );

    dyiTags_.clear();
    dyiTags_.push_back( co2RhsTag_ );
    dyiTags_.push_back( coRhsTag_  );
    dyiTags_.push_back( o2RhsTag_  );
    dyiTags_.push_back( h2RhsTag_  );
    dyiTags_.push_back( h2oRhsTag_ );

    // order here matters: [ CO2, CO, O2, H2, H2O, CH4 ]
    massFracTags_.clear();
    massFracTags_.push_back( co2MassFracTag_ );
    massFracTags_.push_back( coMassFracTag_  );
    massFracTags_.push_back( o2MassFracTag_  );
    massFracTags_.push_back( h2MassFracTag_  );
    massFracTags_.push_back( h2oMassFracTag_ );
    massFracTags_.push_back( ch4MassFracTag_ );

    // gas property tags
    gasPropertyTags_.clear();
    gasPropertyTags_.push_back( tempGTag_       ); // 0
    gasPropertyTags_.push_back( gasPressureTag_ ); // 1
    gasPropertyTags_.push_back( mixMwTag_     );   // 2

    prtPropertyTags_.clear();
    prtPropertyTags_.push_back( tempPTag_          ); //0
    prtPropertyTags_.push_back( initPrtMassTag_    ); //1
    prtPropertyTags_.push_back( prtMassTag_        ); //2
    prtPropertyTags_.push_back( charMassTag_       ); //3
    prtPropertyTags_.push_back( charConversionTag_ ); //4
    prtPropertyTags_.push_back( coreDensityTag_    ); //5
    prtPropertyTags_.push_back( coreDiamTag_       ); //6
    prtPropertyTags_.push_back( prtDiamTag_        ); //7
    prtPropertyTags_.push_back( ashPorosityTag_    ); //8
    prtPropertyTags_.push_back( ashThicknessTag_   ); //9
    prtPropertyTags_.push_back( thermAnnealTag_    ); //10

    // char depletion tags
    charDepletionTags_.clear();
    // these are in (kg char consumed by species i)/sec
    charDepletionTags_.push_back( heteroCo2Tag_                                 );
    charDepletionTags_.push_back( Tag("char_Depletion_CO", STATE_N) );
    charDepletionTags_.push_back( oxidationRhsTag_                              );
    charDepletionTags_.push_back( Tag(sName.char_gasifh2,  STATE_N) );
    charDepletionTags_.push_back( heteroH2oTag_                                 );

    // CO2-CO ratio (mole basis)
    charDepletionTags_.push_back( co2CoRatioTag_ );

    // RHS tags (kg/s)
    charRhsTags_.clear();
    charRhsTags_.push_back( co2RhsTag_  ); //CO2
    charRhsTags_.push_back( coRhsTag_   ); //CO
    charRhsTags_.push_back( o2RhsTag_   ); //O2
    charRhsTags_.push_back( h2RhsTag_   ); //H2
    charRhsTags_.push_back( h2oRhsTag_  ); //H2O
    charRhsTags_.push_back( ch4RhsTag_  ); //H2O
    charRhsTags_.push_back( charRhsTag_ ); //char

    // tags for log(freqency distribution) RHSs
    logFreqDistTags_.clear(); logFreqDistRHSTags_.clear();
    for( size_t i = 0; i<cckData_.get_eD_vec().size(); ++i)
    {
      const std::string fnam = "log_frequency_distribution";
      std::ostringstream frqName, frqRHSName;
      frqName    << sName.log_freq_dist << "_" << i;
      frqRHSName << sName.log_freq_dist << "_RHS_" << i;

      logFreqDistTags_.push_back   (Tag(frqName.str(),    STATE_N));
      logFreqDistRHSTags_.push_back(Tag(frqRHSName.str(), STATE_N));
    }
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  CCKInterface<FieldT>::
  register_expressions( Expr::ExpressionFactory& factory )
  {
    const coal::StringNames& sName = coal::StringNames::self();

    factory.register_expression( new typename CCK::InitialDevolatilizedDensity<FieldT>::
                                 Builder(initDevolDensTag_, initPrtDensTag_, cckData_ ));

    factory.register_expression( new typename CCK::DevolatilizedDensity<FieldT>::
                                 Builder(devolDensityTag_, coreDensityTag_, ashMassFracTag_, devolAshMassFracTag_,
                                         charMassFracTag_, ashDensityTag_,  cckData_ ));

    factory.register_expression( new typename CCK::ParticleDiameter<FieldT>::
                                       Builder(prtDiamModTag_,  charMassTag_, initPrtMassTag_,
                                               devolDensityTag_, initDevolDensTag_, initPrtDiamTag_,
                                                cckData_ ));

    factory.attach_modifier_expression(prtDiamModTag_,
                                       Tag("p_size", STATE_N ),
                                       ALL_PATCHES, true);

    factory.register_expression( new typename CCK::InitialCoreDensity<FieldT>::
                                 Builder(initCoreDensTag_, initDevolDensTag_, initPrtDiamTag_, cckData_ ));

    factory.register_expression( new typename CCK::CoreDensity<FieldT>::
                                 Builder( coreDensityTag_, charConversionTag_, initCoreDensTag_,
                                          cckData_ ));

    factory.register_expression( new typename CCK::AshFilm<FieldT>::
                                 Builder( Expr::tag_list(ashPorosityTag_, ashThicknessTag_, coreDiamTag_),
                                          prtDiamTag_, devolDensityTag_, initDevolDensTag_, devolAshMassFracTag_,
                                          cckData_ ));

    factory.register_expression( new typename CCK::DevolatilizedMassFracs<FieldT>::
                                 Builder( Expr::tag_list( charMassFracTag_,      ashMassFracTag_,
                                                          devolCharMassFracTag_, devolAshMassFracTag_ ),
                                                          initPrtMassTag_, prtMassTag_, charMassTag_, cckData_ ));

    factory.register_expression( new typename Expr::ConstantExpr<FieldT>::
                                 Builder( ashDensityTag_,
                                          cckData_.get_nonporous_ash_density()
                                         *(1 - cckData_.get_min_ash_porosity()) ) );

    factory.register_expression( new typename CCK::LogFrequencyDistributionRHS<FieldT>::
                                 Builder( logFreqDistRHSTags_, tempPTag_,       initPrtMassTag_,
                                          prtMassTag_,          volatilesTag_, cckData_ ));

    factory.register_expression( new typename CCK::ThermalAnnealing<FieldT>::
                                 Builder( thermAnnealTag_, logFreqDistTags_, cckData_ ));

    factory.register_expression( new typename CCK::CCKModel<FieldT>::
                                 Builder( charDepletionTags_,  massFracTags_, gasPropertyTags_, prtPropertyTags_,
                                          cckData_ ) );

    factory.register_expression( new typename CCK::CharConversion<FieldT>::
                                 Builder( charConversionTag_, charMassTag_, initPrtMassTag_, cckData_ ));

    factory.register_expression( new typename CCK::CharSpeciesRHS<FieldT>::
                                 Builder( charRhsTags_, charDepletionTags_, cckData_ ));
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  CCKInterface<FieldT>::
  hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory )
  {
    const int nghost = 0;
    integrator.add_equation<FieldT>( charMassTag_.name(), charRhsTag_, nghost );

      for( size_t i = 0; i<cckData_.get_eD_vec().size(); ++i){
      const std::string fnam = coal::StringNames::self().log_freq_dist;
      std::ostringstream frqName, frqRHSName;
      frqName    << fnam << "_" << i;
      frqRHSName << fnam << "_RHS_" << i;

      Tag logFreqDistTag   (frqName.str(),    STATE_N);
      Tag logFreqDistRHSTag(frqRHSName.str(), STATE_N);

      integrator.add_equation<FieldT>( logFreqDistTag.name(), logFreqDistRHSTag, nghost );
    }
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  CCKInterface<FieldT>::
  set_initial_conditions( Expr::FieldManagerList& fml )
  {
    using namespace SpatialOps;
    FieldT& charMass = fml.field_manager<FieldT>().field_ref( charMassTag_ );
    FieldT& prtmas   = fml.field_manager<FieldT>().field_ref( prtMassTag_   );
    
    double c0 = 0.0;
    if( initDevChar_ ){
      c0 = CPD::c0_fun(cckData_.get_C(), cckData_.get_O());
    }
    std::cout << "Initial char in volatile matter is : " << cckData_.get_vm()*c0
              << " Initial Char is : " << cckData_.get_fixed_C() + cckData_.get_vm()*c0 << "\n";
    charMass <<= prtmas * (cckData_.get_fixed_C() + cckData_.get_vm()*c0);

    const double s  = cckData_.get_neD_std_dev();
    const double mu = cckData_.get_neD_mean();

    for( size_t i = 0; i<cckData_.get_eD_vec().size(); ++i){
      std::ostringstream frqName;
      frqName << coal::StringNames::self().log_freq_dist<< "_" << i;
      Tag logFreqDistTag(frqName.str(), STATE_N);

      FieldT& lnf = fml.field_manager<FieldT>().field_ref( logFreqDistTag );
      const double eD    = cckData_.get_eD_vec()[i];

      lnf <<= -pow(log(eD) - mu, 2)/(2*pow(s,2)) - log(sqrt(2*PI)*s*eD);
    }
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag
  CCKInterface<FieldT>::
  gas_species_rhs_tag( const CHAR::CharGasSpecies spec ) const
  {
    if( spec == CHAR::O2 ) return o2RhsTag_;
    if( spec == CHAR::CO2) return co2RhsTag_;
    if( spec == CHAR::CO ) return coRhsTag_;
    if( spec == CHAR::H2 ) return h2RhsTag_;
    if( spec == CHAR::H2O) return h2oRhsTag_;
    if( spec == CHAR::CH4) return ch4RhsTag_;
    return Tag();
  }

//==========================================================================
// Explicit template instantiation for supported versions of this expression
template class CCKInterface< SpatialOps::Particle::ParticleField >;
//==========================================================================


} // namespace CCK
