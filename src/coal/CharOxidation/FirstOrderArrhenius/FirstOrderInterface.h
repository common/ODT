#ifndef FirstOrderInterface_h
#define FirstOrderInterface_h

#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>
#include <coal/StringNames.h>
#include <coal/Devolatilization/DevolatilizationBase.h>
#include <coal/CharOxidation/CharBase.h>
#include <coal/CharOxidation/CharData.h>
#include "FirstOrderData.h"


namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }
namespace CHAR{ class CharBase;}

namespace FOA{


/**
 *  \ingroup CharOxidation
 *  \class   FirstOrderInterface
 *  \author  Josh McConnell
 *  \date    December 2015
 *
 *  \brief Provides an interface for the 1st-order Arrhenius gasification
 *         model using a Langmuir-Hinshelwood expression for char oxidation.
 *
 */
	
  template< typename FieldT >
  class FirstOrderInterface: public CHAR::CharBase
  {
    //particle and gas values
    const Expr::Tag tempPTag_,       tempGTag_,     mixMwTag_,    prtDensTag_,
                    gasPressureTag_, prtDiamTag_,   prtMassTag_,  initPrtMassTag_;

    //mass fractions and species RHSs
    const Expr::Tag o2MassFracTag_,  h2oMassFracTag_, h2MassFracTag_, co2MassFracTag_, coMassFracTag_,
                    ch4MassFracTag_, o2RhsTag_,       h2oRhsTag_,     h2RhsTag_,       ch4RhsTag_,
                    co2RhsTag_,      coRhsTag_;

    Expr::TagList co2CoTags_, char_co2coTags_, h2andh2oRhsTags_, massFracTags_;

    Expr::ExpressionID oxidationRHSID_, co2coRHSID_, o2RHSID_,
                 charRHSID_, h2Andh2oRHSID_, gasifID_;

    FirstOrderInterface(); // no copying
    FirstOrderInterface& operator=( const FirstOrderInterface& );  // no assignment

    const bool initDevChar_;  ///< Initial char in volatile matter (Only with CPD Model)
    const CHAR::CharModel charModel_;
    const CHAR::CharOxidationData charData_;
    const FirstOrderData    firstOrderData_;

  public:

    /**
     *  \param prtDiamTag     particle diameter
     *  \param tempPTag       particle temperature
     *  \param tempGTag       gas phase temperature
     *  \param co2MassFracTag gas-phase CO2 mass fraction at the particle surface
     *  \param coMassFracTag  gas-phase CO mass fraction at the particle surface
     *  \param o2MassFracTag  gas-phase O2 mass fraction at the particle surface
     *  \param h2MassFracTag  gas-phase H2 mass fraction at the particle surface
     *  \param h2oMassFracTag gas-phase H2O mass fraction at the particle surface
     *  \param ch4MassFracTag gas-phase CH4 mass fraction at the particle surface
     *  \param totalMWTag     gas-phase mixture molectular weight at the particle surface
     *  \param prtDensTag     gas-phase mixture mass density at the particle surface
     *  \param gasPressureTag gas phase pressure
     *  \param prtMassTag     particle mass
     *  \param initPrtmassTag initial particle mass
     *  \param coalType       name of the coal type
     *  \param devModel        devolatilization model
     */
    FirstOrderInterface( const Expr::Tag& prtDiamTag,
                         const Expr::Tag& tempPTag,
                         const Expr::Tag& tempGTag,
                         const Expr::Tag& co2MassFractag,
                         const Expr::Tag& coMassFractag,
                         const Expr::Tag& o2MassFracTag,
                         const Expr::Tag& h2MassFractag,
                         const Expr::Tag& h2oMassFractag,
                         const Expr::Tag& ch4MassFractag,
                         const Expr::Tag& totalMWTag,
                         const Expr::Tag& prtDensTag,
                         const Expr::Tag& gasPressureTag,
                         const Expr::Tag& prtMassTag,
                         const Expr::Tag& initPrtmassTag,
                         const coal::CoalType coalType,
                         const DEV::DevModel devModel );

    /**
     *  \brief registers all expressions relevant to evaluation of the
     *         specified char oxidation model
     */
    void register_expressions( Expr::ExpressionFactory& );


    /**
     *  \brief add char oxidation equations onto the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory );

    /**
     *  \brief set the initial conditions for char
     */
    void set_initial_conditions( Expr::FieldManagerList& );

    /**
     *  \brief obtain the Tag for the requested species
     */
    const Expr::Tag gas_species_rhs_tag( const CHAR::CharGasSpecies spec ) const;

   /**
    * \brief Returns the ID of Co2 and Co consumption in gas phase
    */
    Expr::ExpressionID co2co_rhs_ID() const{ return co2coRHSID_; }

    /**
     * \brief Return the ID for Oxygen consumption is gas phase
     */
    Expr::ExpressionID o2_rhs_ID() const{ return o2RHSID_; }

    /**
     * \brief Returns the ID of H2 and H2O consumption of gas
     */
    Expr::ExpressionID h2andh2orhsID() const{ return h2Andh2oRHSID_; }

  };

} // namespace FOA

#endif // FirstOrderInterface_h
