#include "FirstOrderInterface.h"

#include <stdexcept>
#include <sstream>

#include "FirstOrderInterface.h"
#include "coal/CharOxidation/LangmuirHinshelwood/CharOxidation.h"
#include "coal/CharOxidation/CO2andCO_RHS.h"
#include "coal/CharOxidation/O2RHS.h"
#include "coal/Devolatilization/CPD/c0_fun.h"
#include <coal/CoalData.h>
#include "coal/CharOxidation/CharRHS.h"
#include "coal/CharOxidation/H2andH2ORHS.h"
#include "FirstOrderArrhenius.h"

using std::ostringstream;
using std::endl;
using Expr::Tag;
using Expr::STATE_N;

namespace FOA{

  //------------------------------------------------------------------

  template< typename FieldT >
  FirstOrderInterface<FieldT>::
  FirstOrderInterface( const Tag& prtDiamTag,
                       const Tag& tempPTag,
                       const Tag& tempGTag,
                       const Tag& co2MassFracTag,
                       const Tag& coMassFracTag,
                       const Tag& o2MassFracTag,
                       const Tag& h2MassFracTag,
                       const Tag& h2oMassFracTag,
                       const Tag& ch4MassFracTag,
                       const Tag& mixMwTag,
                       const Tag& prtDensTag,
                       const Tag& intGasPressureTag,
                       const Tag& prtMassTag,
                       const Tag& initPrtmassTag,
                       const coal::CoalType coalType,
                       const DEV::DevModel  dvmodel )
    : tempPTag_      ( tempPTag          ),
      tempGTag_      ( tempGTag          ),
      o2MassFracTag_ ( o2MassFracTag     ),
      co2MassFracTag_( co2MassFracTag    ),
      coMassFracTag_ ( coMassFracTag     ),
      h2MassFracTag_ ( h2MassFracTag     ),
      h2oMassFracTag_( h2oMassFracTag    ),
      ch4MassFracTag_( ch4MassFracTag    ),
      mixMwTag_      ( mixMwTag          ),
      prtDensTag_    ( prtDensTag        ),
      gasPressureTag_( intGasPressureTag ),
      prtDiamTag_    ( prtDiamTag        ),
      prtMassTag_    ( prtMassTag        ),
      initPrtMassTag_( initPrtmassTag    ),
      o2RhsTag_      ( coal::StringNames::self().char_o2RHS,  STATE_N ),
      co2RhsTag_     ( coal::StringNames::self().char_co2RHS, STATE_N ),
      coRhsTag_      ( coal::StringNames::self().char_coRHS,  STATE_N ),
      h2RhsTag_      ( coal::StringNames::self().char_h2RHS,  STATE_N ),
      h2oRhsTag_     ( coal::StringNames::self().char_h2oRHS, STATE_N ),
      ch4RhsTag_     ( coal::StringNames::self().char_ch4RHS, STATE_N ),
      initDevChar_   ( dvmodel == DEV::CPDM ),
      charModel_     ( CHAR::FIRST_ORDER    ),
      charData_      ( coalType ),
      firstOrderData_( coalType )
  {
    const coal::StringNames& sName = coal::StringNames::self();

    charMassTag_     = Tag( sName.char_charmass,  STATE_N );
    oxidationRhsTag_ = Tag( sName.char_oxid_RHS,  STATE_N );
    heteroCo2Tag_    = Tag( sName.char_gasifco2,  STATE_N );
    heteroH2oTag_    = Tag( sName.char_gasifh2o,  STATE_N );
    charRhsTag_      = Tag( sName.char_massRHS,   STATE_N );
    co2CoRatioTag_   = Tag( sName.char_coco2ratio,STATE_N );

    dyiTags_.clear();
    dyiTags_.push_back( co2RhsTag_ );
    dyiTags_.push_back( coRhsTag_  );
    dyiTags_.push_back( o2RhsTag_  );
    dyiTags_.push_back( h2RhsTag_  );
    dyiTags_.push_back( h2oRhsTag_ );

    // order here matters: [ CO2, CO, O2, H2, H2O, CH4 ]
    massFracTags_.clear();
    massFracTags_.push_back( co2MassFracTag_ );
    massFracTags_.push_back( coMassFracTag_  );
    massFracTags_.push_back( o2MassFracTag_  );
    massFracTags_.push_back( h2MassFracTag_  );
    massFracTags_.push_back( h2oMassFracTag_ );
    massFracTags_.push_back( ch4MassFracTag_ );

    co2CoTags_.clear();
    co2CoTags_.push_back( co2RhsTag_ );
    co2CoTags_.push_back( coRhsTag_  );
    
    char_co2coTags_.clear();
    char_co2coTags_.push_back( oxidationRhsTag_ );
    char_co2coTags_.push_back( co2CoRatioTag_ );

    h2andh2oRhsTags_.clear();
    h2andh2oRhsTags_.push_back( h2RhsTag_  );
    h2andh2oRhsTags_.push_back( h2oRhsTag_ );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  FirstOrderInterface<FieldT>::
  register_expressions( Expr::ExpressionFactory& factory )
  {
    factory.register_expression( new typename Expr::ConstantExpr<FieldT>::Builder(ch4RhsTag_, 0));

    oxidationRHSID_ = factory.register_expression( new typename CHAR::CharOxidation  <FieldT>::
                                                   Builder( char_co2coTags_, prtDiamTag_, tempPTag_, tempGTag_, o2MassFracTag_, mixMwTag_, prtDensTag_,
                                                            charMassTag_, gasPressureTag_, initPrtMassTag_, charData_, charModel_) );

    gasifID_ =        factory.register_expression( new typename FirstOrderArrhenius<FieldT>::
                                                   Builder( Expr::tag_list(heteroH2oTag_,heteroCo2Tag_),
                                                            massFracTags_,
                                                            gasPressureTag_,mixMwTag_, tempGTag_, tempPTag_, charMassTag_,
                                                            prtDiamTag_,charData_, firstOrderData_) );

    h2Andh2oRHSID_  = factory.register_expression( new typename CHAR::H2andH2ORHS    <FieldT>::
                                                   Builder( h2andh2oRhsTags_, heteroH2oTag_) );

    co2coRHSID_     = factory.register_expression( new typename CHAR::CO2andCO_RHS   <FieldT>::
                                                   Builder( co2CoTags_, oxidationRhsTag_, co2CoRatioTag_, heteroCo2Tag_, heteroH2oTag_) );

    o2RHSID_        = factory.register_expression( new typename O2RHS          <FieldT>::
                                                   Builder( o2RhsTag_, oxidationRhsTag_, co2CoRatioTag_) );

    charRHSID_      = factory.register_expression( new typename CHAR::CharRHS        <FieldT>::
                                                   Builder( charRhsTag_, oxidationRhsTag_, heteroCo2Tag_, heteroH2oTag_) );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  FirstOrderInterface<FieldT>::
  hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory )
  {
    const int nghost = 0;
    integrator.add_equation<FieldT>( charMassTag_.name(), charRhsTag_, nghost );
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  void
  FirstOrderInterface<FieldT>::
  set_initial_conditions( Expr::FieldManagerList& fml )
  {
    using namespace SpatialOps;
    FieldT& charMass = fml.field_manager<FieldT>().field_ref( charMassTag_ );
    FieldT& prtmas   = fml.field_manager<FieldT>().field_ref( prtMassTag_   );
    
    double c0 = 0.0;
    if( initDevChar_ ){
      c0 = CPD::c0_fun(charData_.get_C(), charData_.get_O());
    }
    std::cout << "Initial char in volatile matter is : " << charData_.get_vm()*c0
              << " Initial Char is : " << charData_.get_fixed_C() + charData_.get_vm()*c0 << "\n";
    charMass <<= prtmas * (charData_.get_fixed_C() + charData_.get_vm()*c0);
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const Tag
  FirstOrderInterface<FieldT>::
  gas_species_rhs_tag( const CHAR::CharGasSpecies spec ) const
  {
    if( spec == CHAR::O2 ) return o2RhsTag_;
    if( spec == CHAR::CO2) return co2RhsTag_;
    if( spec == CHAR::CO ) return coRhsTag_;
    if( spec == CHAR::H2 ) return h2RhsTag_;
    if( spec == CHAR::H2O) return h2oRhsTag_;
    return Tag();
  }

//==========================================================================
// Explicit template instantiation for supported versions of this expression
template class FirstOrderInterface< SpatialOps::Particle::ParticleField >;
//==========================================================================


} // namespace FOA
