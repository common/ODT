#ifndef CharInterface_h
#define CharInterface_h


/**
 *  \file CharInterface.h
 *  \defgroup CharOxidation Char Oxidation Model
 *
 */
#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>
#include <coal/StringNames.h>
#include <coal/Devolatilization/DevolatilizationBase.h>

#include "LangmuirHinshelwood/LangmuirInterface.h"
#include "FirstOrderArrhenius/FirstOrderInterface.h"
#include "CCK/CCKInterface.h"

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace CHAR{
	

  /**
   *  \ingroup CharOxidation
   *  \class CharInterface
   *
   *  \brief Provides an interface to the Char oxidation models
   *
   */
  template< typename FieldT >
  class CharInterface
  {
    CharBase* charModel_;
    LH::LangmuirInterface   <FieldT>* lhModel_;
    FOA::FirstOrderInterface<FieldT>* firstOrderModel_;
    CCK::CCKInterface       <FieldT>* cckModel_;


    CharInterface(); // no copying
    CharInterface& operator=( const CharInterface& );  // no assignment

  public:

    /**
     *  \param prtDiamTag     particle diameter
     *  \param tempPTag       particle temperature
     *  \param tempGTag       gas phase temperature
     *  \param co2MassFracTag gas-phase CO2 mass fraction at the particle surface
     *  \param coMassFracTag  gas-phase CO mass fraction at the particle surface
     *  \param o2MassFracTag  gas-phase O2 mass fraction at the particle surface
     *  \param h2MassFracTag  gas-phase H2 mass fraction at the particle surface
     *  \param h2oMassFracTag gas-phase H2O mass fraction at the particle surface
     *  \param ch4MassFracTag gas-phase CH4 mass fraction at the particle surface
     *  \param mixMwTag       gas-phase mixture molecular weight at the particle surface
     *  \param prtDensTag     gas-phase mixture mass density at the particle surface
     *  \param gasPressureTag gas phase pressure
     *  \param prtMassTag     particle mass
     *  \param initPrtmassTag initial particle mass
     *  \param initPrtDensTag initial particle density
     *  \param initPrtDiamTag initial particle diameter
     *  \param volatilesTag   volatile mass within the coal
     *  \param coalType       the name of the coal
     *  \param devModel       devolatilization model
     *  \param chModel        char oxidation model
     */
    CharInterface( const Expr::Tag& prtDiamTag,
                   const Expr::Tag& tempPTag,
                   const Expr::Tag& tempGTag,
                   const Expr::Tag& co2MassFractag,
                   const Expr::Tag& coMassFractag,
                   const Expr::Tag& o2MassFracTag,
                   const Expr::Tag& h2MassFractag,
                   const Expr::Tag& h2oMassFractag,
                   const Expr::Tag& ch4MassFractag,
                   const Expr::Tag& mixMwTag,
                   const Expr::Tag& prtDensTag,
                   const Expr::Tag& gasPressureTag,
                   const Expr::Tag& prtMassTag,
                   const Expr::Tag& initPrtmassTag,
                   const Expr::Tag& initPrtDensTag,
                   const Expr::Tag& initPrtDiamTag,
                   const Expr::Tag& volatilesTag,
                   const coal::CoalType coalType,
                   const DEV::DevModel devModel,
                   const CHAR::CharModel chModel );

    /**
     *  \brief registers all expressions relevant to evaluation of the
     *         specified char oxidation model
     */
    void register_expressions( Expr::ExpressionFactory& );

    /**
     *  \brief add char oxidation equations onto the time integrator
     */
    void hook_up_time_integrator( Expr::TimeStepper& integrator, Expr::ExpressionFactory& factory );

    /**
     *  \brief set the initial conditions for char
     */
    void set_initial_conditions( Expr::FieldManagerList& );

    /**
     *  \brief obtain the Tag for the char mass (kg)
     */
    const Expr::Tag& char_mass_tag() const;

    /**
     *  \brief obtain the Tag for the char consumption rate.
     */
    const Expr::Tag& char_consumption_rate_tag() const;

    /**
     *  \brief obtain the Tag for the char consumption rate due to reaction of
     *           \f$ C_{(char)} + CO_{2}\rightarrow 2CO \f$
     */
    const Expr::Tag& char_gasification_co2_rate_tag() const;

    /**
     *  \brief obtain the Tag for the char consumption rate due to reaction of
     *           \f$ C_{(char)} + H_{2}O \rightarrow CO + H_{2} \f$
     */
    const Expr::Tag& char_gasification_h2o_rate_tag() const;

    /**
     *  \brief Obtain the ratio of CO2/CO due to char oxidation tag.
     */
    const Expr::Tag& co2coratio_tag() const;

    /**
     *  \brief Obtain the char consumption due to char oxidation
     */
    const Expr::Tag& oxidation_tag() const;

    /**
     *  \brief Obtain the list of all gas species production rate tags (kg/s)
     */
    const Expr::TagList& gas_species_rhs_tags() const;

    /**
     *  \brief obtain the Tag for the requested species
     */
    Expr::Tag gas_species_rhs_tag( const CHAR::CharGasSpecies spec ) const;

  };

} // namespace CHAR

#endif // CharInterface_h
