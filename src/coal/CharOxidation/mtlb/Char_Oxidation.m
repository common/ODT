function Char_Oxidation()

clc;


O_coal = 0.09;
H_char = 0.02;
Psi = 0.2;
e_0 = 0.3;
t_0 = 2;
r_pore = 170e-10;  % mean pore radius ????? table 1. [1]
d = 90e-9;  % particle diameter table 1. [1]


for i = 1:1900
    T = 500 + i;
    
    X = 1 - 0.85;
    P_O = 17.2e3; % Pa
    
    % Table 1. [1]
    if T < 1050
        
        A_0 = 686; %/ (nu_fun()*Mp_fun()*Cs_fun(P_O,T));
        
        Ai = A_0 * exp(1.4*H_char*100)*exp(0.263*O_coal*100);
        
        % In Low Temperature
        
        % rp = S_fun(X, Psi, O_coal)*k_ifun(T)*nu_fun()*Mp_fun()*Cs_fun(P_O,T);
        
        if O_coal/100 < 0.13 % hogh rank coal
            %     rp = 8.22 * exp(-15000/T)*exp(1.4*H_char)*exp(0.263*O_coal)*...
            %         S_fun(X, Psi, O_coal)
            rp =  Ai*exp(-15000/T)*nu_fun()*Mp_fun()*Cs_fun(P_O,T)*...
                S_fun(X, Psi, O_coal);
            
        end
        if O_coal/100 > 0.13 % Low rank coal
            rp = 686*exp(-15000/T)*exp(1.4*H_char)*exp(1.54*Ca_d)*...
                S_fun(X, Psi, O_coal);
        end
        OT(i) = 1/T;
        Lrp(i) = log10(rp);
        
    else
        S_0 = 300; % m2/g
        
        % in high Temperature regim
        A_0 = 686 / (nu_fun()*Mp_fun()*Cs_fun(P_O,T))
        
        Ai = A_0 * exp(1.4*H_char)*exp(0.263*O_coal);
        
        
        
        
        rp = 6 / d*(De_fun(T,r_pore, e_0, t_0)*nu_fun()*M_p_fun()/M_o_fun()*S_0*...
            ki_fun(Ai,T)/sigma_fun())^0.5*nu_fun()* M_p_fun() * Cs_fun(P_O,T);
        OT(i) = 1/T;
        Lrp(i) = log10(rp);
    end
    
end

plot(OT,Lrp);

function S = S_fun(X, Psi, Oc)

S_0 = 300; % m2/g
if Oc < 0.13
    % High rank coal
    S = S_0 * (1 - X)*(1 - Psi*log(1-X))^0.5 ; % [1]
else
    % Low rank coal
    S = S_0 * (1 - X); % [1]
end



function n = nu_fun()
n= 0.93;


function m = Mp_fun()
% Molecular weight of carbon
m = 12; % g/mol


% Concentation of oxidant
function cs = Cs_fun(P, T)
R = 8.314;
cs = P / R / T ;


function De = De_fun(T,r_pore, e_0, t_0)
De = 9.7e2 *r_pore*(T/M_o_fun())^0.5*e_0/t_0;


function y = M_o_fun()
y = 16; %molecular mass of reactive gas. g/mol
%   Talk with james about this


function y = M_p_fun()

y = 12; % Molecular mass of carbon;

function y = sigma_fun()
y = 1.2e+6; %% aparant particle density g/m3

function ki = ki_fun(Ai,T)

ER = 15000;

ki = Ai * exp(-ER / T);





% [1] "The predection of coal char reactivity under combustion condition"
