#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

#include <expression/ExprLib.h>

#include <coal/ParticleTemperatureRHS.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper

#include <coal/Devolatilization/DevolatilizationBase.h>
#include "../LangmuirHinshelwood/LangmuirInterface.h"

typedef Expr::ExprPatch  PatchT;
#include <spatialops/Nebo.h>
#include <spatialops/particles/ParticleFieldTypes.h>
typedef SpatialOps::Particle::ParticleField  FieldT;
using SpatialOps::operator<<=;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

bool
setup_and_test_integrator( const Expr::TSMethod method,
                           const coal::CoalType coalType,
                           const double temperature,
                           const double dt,
                           const double endt )
{
  using std::cout;
  using std::endl;
  double temperatureG = temperature;
  PatchT patch(1,1,1,1);

  Expr::ExpressionFactory exprFactory;

  const CanteraObjects::Setup options("Mix", "gri30.cti", "gri30");
  CanteraObjects::setup_cantera(options, 1);

  const Expr::Tag prtDiamt      ( "Particle Diameter",    Expr::STATE_N );
  const Expr::Tag tempPTag      ( "Particle Temperature", Expr::STATE_N );
  const Expr::Tag tempGTag      ( "Gas Temperature",      Expr::STATE_N );
  const Expr::Tag massFracCOTag ( "CO_massFraction",      Expr::STATE_N );
  const Expr::Tag massFracCO2Tag( "CO2_massFraction",     Expr::STATE_N );
  const Expr::Tag massFracO2Tag ( "O2_massFraction",      Expr::STATE_N );
  const Expr::Tag massFracH2Tag ( "H2_massFraction",      Expr::STATE_N );
  const Expr::Tag massFracH2OTag( "H2O_massFraction",     Expr::STATE_N );
  const Expr::Tag massFracCH4Tag( "CH4_massFraction",     Expr::STATE_N );
  const Expr::Tag totalMWTag    ( "Total_Molecularweight",Expr::STATE_N );
  const Expr::Tag densityTag    ( "Particle_Density",     Expr::STATE_N );
  const Expr::Tag initDensTag   ( "Init_Particle_Density",Expr::STATE_N );
  const Expr::Tag pressureTag   ( "Gas Pressure",         Expr::STATE_N );
  const Expr::Tag prtmasst      ( "Particle_Mass",        Expr::STATE_N );
  const Expr::Tag initprtmasst  ( "Init_Particle_Mass",   Expr::STATE_N );
  const Expr::Tag volatilesTag  ( "volatiles",            Expr::STATE_N );
  const Expr::Tag timeTag       ( "time",                 Expr::STATE_N );

  const double particleMass = 5.0E-10;

  typedef Expr::ConstantExpr<FieldT>::Builder ConstExpr;

  exprFactory.register_expression( new ConstExpr(prtDiamt,      150e-9) );
  exprFactory.register_expression( new ConstExpr(tempPTag,      temperature ));
  exprFactory.register_expression( new ConstExpr(tempGTag,      1500));
  exprFactory.register_expression( new ConstExpr(totalMWTag,    28.0)  );
  exprFactory.register_expression( new ConstExpr(massFracCO2Tag,0.1)   );
  exprFactory.register_expression( new ConstExpr(massFracCOTag, 0.0)   );
  exprFactory.register_expression( new ConstExpr(massFracO2Tag, 0.2)   );
  exprFactory.register_expression( new ConstExpr(massFracH2Tag, 0.0)   );
  exprFactory.register_expression( new ConstExpr(massFracH2OTag,0.05)  );
  exprFactory.register_expression( new ConstExpr(massFracCH4Tag,0.0)   );
  exprFactory.register_expression( new ConstExpr(densityTag,    750) );
  exprFactory.register_expression( new ConstExpr(initDensTag,   750) );
  exprFactory.register_expression( new ConstExpr(initprtmasst,  particleMass));
  exprFactory.register_expression( new ConstExpr(pressureTag,   101325.0) );
  exprFactory.register_expression( new ConstExpr(volatilesTag,  0.289*particleMass));

  const Expr::ExpressionID prtmassID = exprFactory.register_expression( new ConstExpr(prtmasst,particleMass) );

  std::cout << "All the inputs of langmuirInterface are registered \n";

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::TimeStepper timeIntegrator( exprFactory, method, "timestepper", patch.id() );
  std::cout << "Time Stepper was Called \n";

  LH::LangmuirInterface<FieldT>* langmuirInterface;
  try{
    langmuirInterface = new LH::LangmuirInterface<FieldT>( prtDiamt, tempPTag, tempGTag,
                                                     massFracCO2Tag, massFracO2Tag,massFracH2OTag,
                                                     totalMWTag, densityTag,pressureTag, prtmasst, initprtmasst,
                                                     coalType, DEV::CPDM, CHAR::LH );
    langmuirInterface->register_expressions( exprFactory );

    std::cout << "langmuirInterface->register_expressions was called \n";
    langmuirInterface->hook_up_time_integrator( timeIntegrator, exprFactory );
    timeIntegrator.get_tree()->insert_tree( prtmassID, false );

    timeIntegrator.get_tree()->insert_tree(langmuirInterface->co2co_rhs_ID(), false);
    timeIntegrator.get_tree()->insert_tree(langmuirInterface->o2_rhs_ID(),    false);
    timeIntegrator.get_tree()->insert_tree(langmuirInterface->h2andh2orhsID(),false);


    std::cout << "lamgmuirInterface->hook_up_time_integrator was called \n";
    timeIntegrator.finalize( fml,
                             patch.operator_database(),
                             patch.field_info() );
    std::cout << "Finalized !!! \n";
    fml.field_manager<FieldT>().field_ref(prtmasst) <<= particleMass;
    langmuirInterface->set_initial_conditions( fml );
    std::cout << "Inputs are initialized !  \n";
  }
  catch( std::exception& e ){
    cout << endl
         << "ERROR in setting up the LangmuirInterface" << endl
         << e.what()
         << endl;
    return false;
  }

  timeIntegrator.get_tree()->lock_fields(fml);

  Expr::TagList gasrhstag =  langmuirInterface->gas_species_rhs_tags();
  const FieldT& charMass = fml.field_manager<FieldT>().field_ref( langmuirInterface->char_mass_tag() );
  const FieldT& charrhs  = fml.field_manager<FieldT>().field_ref( langmuirInterface->char_consumption_rate_tag());
  const FieldT& co2rhs   = fml.field_manager<FieldT>().field_ref( gasrhstag[0] );
  const FieldT& corhs    = fml.field_manager<FieldT>().field_ref( gasrhstag[1] );
  const FieldT& o2rhs    = fml.field_manager<FieldT>().field_ref( gasrhstag[2] );
  const FieldT& h2rhs    = fml.field_manager<FieldT>().field_ref( gasrhstag[3] );
  const FieldT& h2orhs   = fml.field_manager<FieldT>().field_ref( gasrhstag[4] );
  {
    std::ofstream ofile("tree.dot");
    timeIntegrator.get_tree()->write_tree( ofile );
  }

  std::ostringstream savedname;
  savedname << "CHAR_" << temperature <<".txt";
  cout << savedname.str() << endl;
  std::ofstream fout( savedname.str().c_str(), std::ios_base::trunc|std::ios_base::out );

  fout.width(10);
  fout.precision(7);

  fout << "  This is the result of Char Oxidation" << endl;
  fout << "  Created by : Babak Goshayeshi - PhD student" << endl;
  fout << "               University of Utah - Institute for Clean and Secure Energy" << endl << endl << endl;
  fout << std::setw(15) << "time (sec)" << std::setw(15) << "char rhs (kg/s)" <<std::setw(15) << "charMass (kg)"
  << std::setw(15) << "CO2 rhs (kg/s)" << std::setw(15) << "CO rhs (kg/s)" << std::setw(15) << "O2 rhs (kg/s)"
  << std::setw(15) << "H2 rhs (kg/s)" << std::setw(15) << "H2O rhs(kg/s)" << endl;

  for( double t=0.0; t<endt; t+=dt ){
    timeIntegrator.step(dt);
    fout << std::setw(15) << std::setprecision(7) << t
         << std::setw(15) << std::setprecision(7) << charrhs[0]
         << std::setw(15) << std::setprecision(7) << charMass[0]
         << std::setw(15) << std::setprecision(7) << co2rhs[0]
         << std::setw(15) << std::setprecision(7) << corhs[0]
         << std::setw(15) << std::setprecision(7) << o2rhs[0]
         << std::setw(15) << std::setprecision(7) << h2rhs[0]
         << std::setw(15) << std::setprecision(7) << h2orhs[0]
         << endl;
    const double sumRates = std::abs( co2rhs[0] + corhs[0] + o2rhs[0]+ h2rhs[0] + h2orhs[0] - charrhs[0] );
    if( sumRates > 1e-16 ){
      cout << "\nFAIL: sum of rates is not zero! (" << sumRates << ")\n";
      return false;
    }
  }
  fout.close();
  cout << "\n***  Integration was done over 0 to " << endt << endl;

  delete langmuirInterface;

  return true;
}








int main( int narg, char* arg[] )
{
  using std::cout;
  using std::endl;

  double temp, dt, tend;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message\n" )
      ( "temperature", po::value<double>(&temp)->default_value(1000.0), "Temperature (K) of the system" )
      ( "timestep",    po::value<double>(&dt  )->default_value(1.0e-6), "Timestep (s)" )
      ( "endtime",     po::value<double>(&tend)->default_value(1e-4 ), "End time (s)" );

    po::variables_map vm;

    po::store( po::parse_command_line(narg,arg,desc), vm );
    po::notify( vm );

    if( vm.count("help") ) {
      cout << desc << endl;
      return 1;
    }
  }
  catch( po::unknown_option& e ){
    cout << e.what() << endl;
    return 1;
  }

  cout << "T  = " << temp << endl
       << "dt = " << dt << endl
       << "end time = " << tend << endl
       << endl;

  try{
    const bool passed = setup_and_test_integrator( Expr::SSPRK3,
                                                   coal::Pittsburgh_Bituminous,
                                                   temp,
                                                   dt,
                                                   tend );
    if( passed )
      return 0;
  }
  catch( std::exception& e ){
    cout << e.what() << endl;
  }
  return -1;
};
