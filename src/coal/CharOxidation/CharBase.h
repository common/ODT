/*
 * CharBase.h
 *
 *  Created on: Dec 12, 2015
 *      Author: josh
 */

#ifndef CharBase_h
#define CharBase_h

/**
 *  \author  Josh McConnell
 *  \date    December 2015
 */

#include <expression/Tag.h>
#include <expression/ExpressionID.h>

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace CHAR{

enum CharGasSpecies{
  O2,
  CO2,
  CO,
  H2,
  H2O,
  CH4,
  INVALID_SPECIES = 99
};


  enum CharModel{
    LH,
    FRACTAL,
    FIRST_ORDER,
    CCK,
    INVALID_CHARMODEL
  };

  /**
   * @fn std::string char_model_name( const CharModel model )
   * @param model: char chemistry model
   * @return the string name of the model
   */
  std::string char_model_name( const CharModel model );

  /**
   * @fn CharModel char_model( const std::string& modelName )
   * @param modelName: the string name of the char model
   * @return the corresponding enum value
   */
  CharModel char_model( const std::string& modelName );

  /**
   * \class CharBase
   * \brief Base class for char chemistry models
   */
  class CharBase{
  public:

    Expr::TagList dyiTags_;

    Expr::Tag charMassTag_,  charRhsTag_;
    Expr::Tag heteroCo2Tag_, heteroH2oTag_, oxidationRhsTag_, co2CoRatioTag_;

    /**
     *  \brief obtain the TagList for the gas phase species production rates (kg/s)
     */
    const Expr::TagList& gas_species_rhs_tags() const{ return dyiTags_; };

    /**
     *  \brief obtain the Tag for char mass
     */
    const Expr::Tag& char_mass_tag() const{ return charMassTag_;};

    /**
     *  \brief obtain the Tag for char consumption rate
     */
    const Expr::Tag& char_consumption_rate_tag() const{ return charRhsTag_;};


    /**
     *  \brief obtain the Tag for char consumption by oxygen
     */
    const Expr::Tag& oxidation_tag() const{ return oxidationRhsTag_;};

    /**
     *  \brief obtain the Tag for char consumption by CO2 gasification
     */
    const Expr::Tag& char_gasification_co2_rate_tag() const{ return heteroCo2Tag_;};

    /**
     *  \brief obtain the Tag for char consumption by H2O gasification
     */
    const Expr::Tag& char_gasification_h2o_rate_tag() const{ return heteroH2oTag_;};

    /**
     *  \brief obtain the Tag for CO2-CO ratio
     */
    const Expr::Tag& co2coratio_tag() const{ return co2CoRatioTag_;};

    /**
     *  \brief Register all expressions required to implement the CPD model
     */
    virtual void register_expressions( Expr::ExpressionFactory& ) = 0;

    /**
     *  \brief Plug in equations to the time integrator
     */
    virtual void hook_up_time_integrator( Expr::TimeStepper& timestepper, Expr::ExpressionFactory& factory ) = 0;

    /**
     *  \brief Set the initial conditions for the CPD model
     */
    virtual void set_initial_conditions( Expr::FieldManagerList& fml ) = 0;


    /**
     *  \brief obtain the Tag for production rate of specified species
     */
    virtual const Expr::Tag gas_species_rhs_tag( const CHAR::CharGasSpecies charSpec ) const = 0;

    virtual ~CharBase(){}
  };

} // namespace CHAR




#endif /* CharBase_h */
