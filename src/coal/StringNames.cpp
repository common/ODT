#include "StringNames.h"

using std::string;

namespace coal {

const StringNames&
StringNames::self()
{
  static StringNames s;
  return s;
}
//--------------------------------------------------------------------
StringNames::StringNames()
  :dev_char("dev_char_production"), cpd_l("cpd_l"),
  cpd_G_RHSi("cpd_G_RHS_"), cpd_Delta_RHSi("cpd_Delta_RHS_"), cpd_gi("cpd_g_"), cpd_deltai("cpd_delta_"), 
  cpd_kgi("cpd_kg_"), cpd_yi("cpd_y_"), cpd_dyi("cpd_dy_"), dev_mv("Volatile_Mass"),
  cpd_kb("cpd_kb"), cpd_L_RHS("cpd_L_RHS"), cpd_C_RHS("cpd_C_RHS"), dev_volatile_RHS("dev_volatile_RHS"), 
  cpd_product_RHS ("cpd_product_RHS"), cpd_charProd_RHS("cpd_char_production_rhs"),
  char_charmass("char_mass"),
  char_oxid_RHS("char_oxidation_RHS"),
  char_gasifh2o("H2O_Gasification_rate"),
  char_gasifco2("CO2_Gasification_rate"),
  char_gasifh2("H2_Gasification_rate"),
  char_coco2ratio("char_Mole_CO2/CO"),
  char_o2RHS("char_O2_RHS"),
  char_h2RHS("char_H2_RHS"),
  char_co2RHS("char_CO2_RHS"),
  char_coRHS("char_CO_RHS"),
  char_h2oRHS("char_H2O_RHS"),
  char_ch4RHS("char_CH4_RHS"),
  char_heattogas("heat_released_to_gas"),
  ash_density("ash_density"), ash_porosity("ash_porosity"), ash_thickness("ash_film_thickness"),
  core_diameter("p_core_diameter"), core_density("p_core_density"), ash_mass_frac("ash_mass_fraction"),
  char_mass_frac("char_mass_fraction"), char_conversion("char_conversion"),
  log_freq_dist("log_frequency_distribution"), therm_anneal("thermal_annealing_factor"),
  coal_density ("Particle_Density"), coal_prtmass("Particle_Mass"),
  coal_mash("Ash_content"), coal_cp("Heat_Capacity_of_Coal"), coal_temprhs("coal_Temperature_rhs"),

  char_massRHS("char_mass_RHS"),
  cpd_tar("cpd_tar"), cpd_tarProdR("cpd_tarProdR"), cpd_lbPopulation("cpd_lbPopulation"),
  sarofim_tar("sarofim_tar"), sarofim_tarProdR("sarofim_tarProdR"),
  singlerate_tar("singlerate_tar"), singlerate_tarProdR("singlerate_tarProdR")
{}
//--------------------------------------------------------------------
StringNames::~StringNames()
{
}
//--------------------------------------------------------------------
} // coal namespace
