#ifndef UT_StringNames_h
#define UT_StringNames_h

#include <string>

/**
 *  @class  StringNames
 *  @author James C. Sutherland
 *  @date   November, 2006
 *
 *  Holds names for variables.
 */
class StringNames
{
public:
  static const StringNames& self();

  const std::string xcoord, xsurfcoord;
  const std::string xvel, yvel, zvel;
  const std::string xmom, ymom, zmom;
  const std::string density;
  const std::string pressure, dpdx, dpdy, dpdz;
  const std::string temperature;
  const std::string enthalpy, rhoH, SpeciesSourceTerm;
  const std::string e0, rhoE0;
  const std::string heatFromRad;
  const std::string ke;
  const std::string mixturefraction,scalardissipation, rhoF;
  const std::string species, rhoY, flsspecies, flsdensity;

  const std::string tarSpecies, rhoTar, tarSourceTerm, tarDiffFlux;
  const std::string tarOxR, tarGasR, soot, sootParticle;
  const std::string sootAgR, sootOxR, sootSourceTerm, rhoSoot;
  const std::string sootDiffFlux, sootPSourceTerm, rhoSootP;
  const std::string sootFormR, sootEmissivity;

  const std::string sootAbsorpCoeff, tarAbsorpCoeff;
  const std::string e0SrcFromSoot, e0SrcFromTar;

  const std::string specDiffFluxX;
  const std::string pcDiffFluxX;
  const std::string pc, pcSourceTerm, rhoPC;

  // position of the ODT line
  const std::string position,pos;

  const std::string heat_fluxX, heat_fluxY, heat_fluxZ;

  // properties
  const std::string thermal_conductivity;
  const std::string heat_capacity, cv;
  const std::string viscosity;
  const std::string mixtureMW ;

private:
  StringNames();  // implemented as a sigleton
  ~StringNames();
  StringNames(const StringNames&);            // no copying
  StringNames& operator=(const StringNames&); // no assignment
};

#endif
