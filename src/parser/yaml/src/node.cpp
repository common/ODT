#include "yaml-cpp/node/node.h"
#include "nodebuilder.h"
#include "nodeevents.h"
#include "yaml-cpp/node/detail/node.h"

namespace YAML
{
    Node Clone(const Node& node)
    {
		NodeEvents events(node);
        NodeBuilder builder;
        events.Emit(builder);
        return builder.Root();
    }

    const std::string&
    Node::get_node_label() const
    {
      return m_pNode->get_node_label();
    }

    void
    Node::set_node_label( const std::string& label )
    {
      m_pNode->set_node_label( label );
    }

}
