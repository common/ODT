/*
 * Copyright (c) 2012-2017 The University of Utah
 * Copyright (c) 2012-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <OperatorsAndFields.h>
#include <parser/BasicExprBuilder.h>
#include <parser/ParseTools.h>

//--- initial condition support ---//
#include <expression/Functions.h>
#include <gas/EvattICReader.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/IdealGasMix.h>
#include <StringNames.h>

#include <expression/ExprLib.h>

#include <boost/foreach.hpp>

#include <string>
#include <vector>

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

using std::cout;
using std::endl;
using std::string;


/**
 *  \author Babak Goshayeshi
 *  \date   Mar 2012
 *  \class BiasdDTanhFunction
 */
template< typename ValT >
class BiasdDTanhFunction : public Expr::FunctionExpr1D<ValT>
{
public:

  /**
   *  @brief Builds a Biased DoubleTanhFunction Expression.
   */
  struct Builder : public Expr::ExpressionBuilder
  {
    /**
     *  Builds a Biased DoubleTanhFunction object
     *  \f[
     *     f(x) = \beta + \frac{A}{2} \left(1+\tanh\left(\frac{x-L_1}{w}\right)\right) \left(1-\frac{1}{2}\tanh\left(\frac{x-L_2}{w}\right)\right)
     *  \f]
     */
    Builder( const Expr::Tag& depVarTag,   ///< The dependent variable
        const Expr::Tag& indepVarTag, ///< The independent variable
        const double L1,    ///< The midpoint for the upward transition
        const double L2,    ///< The midpoint for the downward transition
        const double w,     ///< The width of the transition
        const double A,     ///< The amplitude of the transition
        const double B )    ///< The Bias
        : Expr::ExpressionBuilder(depVarTag),
          L1_(L1), L2_(L2), w_(w), A_(A), B_(B),
          ivarTag_( indepVarTag )
    {}

    ~Builder(){}
    Expr::ExpressionBase* build() const
    {
      return new BiasdDTanhFunction<ValT>( ivarTag_, L1_, L2_, w_, A_, B_);
    }

  private:
    const double L1_, L2_, w_, A_, B_;
    const Expr::Tag ivarTag_;
  };

  void evaluate()
  {
    using namespace SpatialOps;
    ValT& phi = this->value();
    const ValT& x = this->x_->field_ref();
    phi <<= B_+(0.5*A_) * ( 1.0 + tanh( (x-L1_)/w_ ) ) * ( 1.0-0.5*(1.0+tanh( (x-L2_)/w_ ) ) );
  }

  private:

  BiasdDTanhFunction( const Expr::Tag& indepVarTag,
                      const double L1,
                      const double L2,
                      const double w,
                      const double A,
                      const double B)
  : Expr::FunctionExpr1D<ValT>( indepVarTag, true ),
    L1_(L1), L2_(L2), w_(w), A_(A), B_(B)
    {}

  const double L1_, L2_, w_, A_, B_;
};


//-----------------------------------------------------------------

template<typename FieldT>
Expr::ExpressionBuilder*
build_basic_expr( const YAML::Node& params )
{
  Expr::Tag tag( params["name"].as<string>(), Expr::STATE_N );

  if( params["MoleFraction"] || params["MassFraction"] ){
    if( params["MoleFraction"] ){
      tag.reset_name( tag.name() + "_MOLEFRAC" );
    }
  }

  Expr::ExpressionBuilder* builder = NULL;

  if( params["Constant"] ){
    typedef typename Expr::ConstantExpr<FieldT>::Builder Builder;
    builder = new Builder( tag, params["Constant"].as<double>() );
  }

  else if( params["LinearFunction"] ){
    const YAML::Node valParams = params["LinearFunction"];
    typedef typename Expr::LinearFunction<FieldT>::Builder Builder;
    builder = new Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        valParams["slope"    ].as<double>(),
        valParams["intercept"].as<double>() );
  }

  else if( params["SineFunction"] ) {
    const YAML::Node valParams = params["SineFunction"];
    typedef typename Expr::SinFunction<FieldT>::Builder Builder;
    builder = new Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        valParams["amplitude"].as<double>(),
        valParams["frequency"].as<double>(),
        valParams["offset"   ].as<double>() );
  }

  else if( params["ParabolicFunction"] ) {
    const YAML::Node valParams = params["ParabolicFunction"];
    typedef typename Expr::ParabolicFunction<FieldT>::Builder Builder;
    builder = new Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        valParams["a"].as<double>(),
        valParams["b"].as<double>(),
        valParams["c"].as<double>() );
  }

  else if( params["GaussianFunction"] ) {
    const YAML::Node valParams = params["GaussianFunction"];
    typedef typename Expr::GaussianFunction<FieldT>::Builder Builder;
    builder = new Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        valParams["amplitude"].as<double>(),
        valParams["stddev"   ].as<double>(),
        valParams["mean"     ].as<double>(),
        valParams["baseline" ].as<double>() );
  }

  else if( params["DoubleTanhFunction"] ) {
    const YAML::Node valParams = params["DoubleTanhFunction"];
    typedef typename Expr::DoubleTanhFunction<FieldT>::Builder Builder;
    builder = new Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        valParams["midpointUp"  ].as<double>(),
        valParams["midpointDown"].as<double>(),
        valParams["width"       ].as<double>(),
        valParams["amplitude"   ].as<double>() );
  }
  else if( params["hypertan"] ){
    const YAML::Node valParams = params["hypertan"];
    const double a  = valParams["amplitude"].as<double>();
    const double w  = valParams["width"    ].as<double>();
    const double r1 = valParams["r1"       ].as<double>();
    const double L  = valParams["L"        ].as<double>();
    const double L1  = L/2.0 - r1 + w/2;
    const double L2  = L/2.0 + r1 - w/2;

    builder = new Expr::DoubleTanhFunction<VolField>::Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        L1, L2, w, a );
  }
  else if( params["biashypertan"] ){
    const YAML::Node valParams = params["biashypertan"];
    const double a  = valParams["amplitude"].as<double>();
    const double w  = valParams["width"    ].as<double>();
    const double r1 = valParams["r1"       ].as<double>();
    const double L  = valParams["L"        ].as<double>();
    const double b  = valParams["base"     ].as<double>();
    const double L1  = L/2.0 - r1 + w/2;
    const double L2  = L/2.0 + r1 - w/2;
    builder = new BiasdDTanhFunction<VolField>::Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        L1, L2, w, a, b );
  }
  else if( params["coaxialjet"] ){
    const YAML::Node valParams = params["coaxialjet"];
    const double a1   =     valParams["jetvalue"     ].as<double>();
    const double a2   =     valParams["coaxialvalue" ].as<double>();
    const double w    =     valParams["width"        ].as<double>();
    const double wall =     valParams["wallthickness"].as<double>();
    const double r1   = 0.8*valParams["innerjet"     ].as<double>();
    const double r2   = 0.8*valParams["outerjet"     ].as<double>();
    const double B    =     valParams["base"         ].as<double>();
    const double L    =     valParams["L"            ].as<double>();

    const double L1  = L/2.0 - r1 - wall - r2 - w/2;
    const double L2  = L/2.0 - r1 - wall  + w/2;

    const double L3  = L/2.0 - r1 + w/2;
    const double L4  = L/2.0 + r1 - w/2;

    const double L5  = L/2.0 + r1 + wall - w/2;
    const double L6  = L/2.0 + r1 +wall + r2 + w/2;

    builder = new Expr::CoaxialJet<VolField>::Builder( tag,
        parse_nametag( valParams["IndepVar"] ),
        a1,a2,w,L1, L2, L3,L4,L5,L6, B );
  }
  else if( params["evatt"] ){
    const YAML::Node valParams = params["evatt"];
    const string fnam = valParams["filename" ].as<string>();
    const int    npts = valParams["npts" ].as<int>();
    const int    nspec= valParams["nspec"].as<int>();
    static FilePtr icfile( new EvattICReader( fnam, npts, nspec ) );
    builder = new EvattICExpr::Builder( icfile, tag );
  }


  if( builder == NULL ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << endl
        << "ERROR: did not resolve a builder for " << params["name"] << endl;
    throw std::invalid_argument( msg.str() );
  }

  return builder;
}

//------------------------------------------------------------------

void
create_expressions_from_input( const YAML::Node& parser,
                               GraphCategories& gc )
{
  //___________________________________
  // parse and build basic expressions
  const YAML::Node exprInfo( parser["BasicExpression"] );
  BOOST_FOREACH( const YAML::Node& exprParams, exprInfo ){
    try{
      const  YAML::Node n = exprParams["type"];
    }
    catch( std::exception& err ){
      std::ostringstream msg;
      msg << err.what() << endl << endl
          << "***ERROR while parsing BasicExpression.\n\tEach entry should start with \"- type : ...\n\n";
      throw std::invalid_argument( msg.str() );
    }
    const string fieldType = exprParams["type"].as<string>();
    Expr::ExpressionBuilder* builder;
    switch( get_field_type(fieldType) ){
      case VOLUME: builder = build_basic_expr<VolField >(exprParams); break;
      case FACE  : builder = build_basic_expr<FaceField>(exprParams); break;
    }

    const string taskListName = exprParams["TaskList"].as<string>();
    Category cat = INITIALIZATION;
    if     ( taskListName == "initialization"   )   cat = INITIALIZATION;
    else if( taskListName == "advance_solution" )   cat = ADVANCE_SOLUTION;
    else{
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "ERROR: unsupported task list '" << taskListName << "'" << endl
          << "       encountered while parsing '"
          << exprParams["name"].as<string>()
          << "'" << endl;
      throw std::invalid_argument( msg.str() );
    }
    Expr::ExpressionFactory* exprFactory = gc[cat]->exprFactory;
    exprFactory->register_expression( builder, false );
  } // basic expression loop
}

//-----------------------------------------------------------------
