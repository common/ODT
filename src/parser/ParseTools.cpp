/*
 * Copyright (c) 2012-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <parser/ParseTools.h>

#include <expression/Tag.h>

#include <boost/foreach.hpp>

#include <map>
#include <stdexcept>
#include <sstream>
using std::string;


Expr::Tag
parse_nametag( const YAML::Node& param )
{
  return Expr::Tag( param.as<string>(), Expr::STATE_N );
}

//===================================================================

Expr::TagList
parse_taglist( const YAML::Node& param )
{
  Expr::TagList tags;
  for( YAML::Node::const_iterator ipg=param.begin(); ipg!=param.end(); ++ipg ){
    tags.push_back( parse_nametag(*ipg) );
  }
  return tags;
}

//===================================================================

typedef std::map<string,FieldType> StringMap;
static StringMap validStrings;

void set_string_map()
{
  if( !validStrings.empty() ) return;

  validStrings["VOLUME"] = VOLUME;
  validStrings["FACE"  ] = FACE;
}

FieldType
get_field_type( std::string key )
{
  // \todo jcs: convert this to use boost::algorithm::iequals for case comparison.
  set_string_map();
  std::transform( key.begin(), key.end(), key.begin(), ::toupper );

  StringMap::iterator i = validStrings.find( key );
  if( i == validStrings.end() ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "Invalid field type encountered ('" << key << "')" << std::endl
        << "  Valid field type names:" << std::endl;
    for( StringMap::const_iterator i=validStrings.begin(); i!=validStrings.end(); ++i ){
      msg << "    " << i->first << std::endl;
    }
    throw std::invalid_argument( msg.str() );
  }
  return i->second;
}

//===================================================================
