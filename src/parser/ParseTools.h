/**
 *  \file   ParseTools.h
 *  \brief  General tools helpful when parsing
 *  \author James C. Sutherland
 *
 * Copyright (c) 2012-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ParseTools_h
#define ParseTools_h

#include <string>

#include <expression/ExprFwd.h>

#include <yaml-cpp/yaml.h>


/** \addtogroup Parser
 *  @{
 */

/**
 *  \fn Expr::Tag parse_nametag(const ParseGroup&);
 *
 *  \brief Parses a name tag, comprised of a variable name and state. The
 *         bundle direction information is appended to the name.
 *
 *  \param param The parser block for this name tag.
 *  \return the Expr::Tag.
 */
Expr::Tag parse_nametag( const YAML::Node& param );

/**
 *  \fn Expr::TagList parse_taglist(const ParseGroup&);
 *
 *  \brief Parses a list of name tags.
 *
 *  \param param The parser block for this name tag.
 *  \return the Expr::TagList.
 */
Expr::TagList
parse_taglist( const YAML::Node& param );

/**
 *  \enum FieldType
 *  \brief Enumerate the field types that can be handled by parsing.
 *  See also get_field_type
 */
enum FieldType{
  VOLUME,  ///< Volume field - corresponds to string "VOLUME"
  FACE     ///< Face field - corresponds to string "FACE"
};

/**
 *  \fn FieldType get_field_type( std::string key );
 *  \brief Convert a (case-insensitive) string into a FieldType enum.
 *   See the documentation of \link FieldType \endlink for allowed string names.
       Invalid strings result in an exception being thrown.
 *  \param key the string
 *  \return the enum value
 */
FieldType get_field_type( std::string key );

/** @} */


#endif // ParseTools_h
