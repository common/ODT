#include <StringNames.h>

using std::string;

const StringNames&
StringNames::self()
{
  static StringNames s;
  return s;
}
//--------------------------------------------------------------------
StringNames::StringNames()
  : xcoord("xcoord"), xsurfcoord("xsurfcoord"), 
    xvel("x_velocity"), yvel("y_velocity"), zvel("z_velocity"),
    xmom("x_momentum"), ymom("y_momentum"), zmom("z_momentum"),

    density("density"),
    pressure("pressure"), dpdx("dpdx"), dpdy("dpdy"), dpdz("dpdz"),
    temperature("temperature"),
    enthalpy("enthalpy"),  rhoH("rhoH"),
    SpeciesSourceTerm("SpeciesSourceTerm"),
    e0("e0"), rhoE0("rhoE0"),
    ke("ke"),
    heatFromRad("heatFromRadiation"),

    mixturefraction("mixturefraction"),scalardissipation("scalardissipation"),  rhoF("rhoF"),

    species("species"), rhoY("rhoY"), flsspecies("flsspecies"), flsdensity("flsdensity"),

    tarSpecies("tarSpecies"), tarSourceTerm("tarSourceTerm"), tarDiffFlux("tarDiffFlux"),
    rhoTar("rhoTar"), tarOxR("tarOxR"), tarGasR("tarGasR"),

    sootFormR("sootFormR"),
    sootAgR("sootAgR"), soot("soot"), sootParticle("sootParticle"),
    sootSourceTerm("sootSourceTerm"),rhoSoot("rhoSoot"), sootDiffFlux("sootDiffFlux"),
    sootPSourceTerm("sootPSourceTerm"), rhoSootP("rhoSootP"), sootOxR("sootOxR"),
    sootEmissivity("sootEmissivity"),

    sootAbsorpCoeff("sootAbsorptionCoeff"),tarAbsorpCoeff("tarAbsorptionCoeff"),
    e0SrcFromSoot("e0SrcFromSoot"), e0SrcFromTar("e0SrcFromTar"),

    specDiffFluxX("SpeciesDiffusionFlux"),

    pcDiffFluxX("PCDiffusionFlux"),
    pc("PC"),
    pcSourceTerm("PCSourceTerm"),
    rhoPC("rhoPC"),

    position("position"),pos("pos"),

    heat_fluxX("heat_fluxX"), heat_fluxY("heat_fluxY"), heat_fluxZ("heat_fluxZ"),

    thermal_conductivity("thermal_conductivity"),
    heat_capacity("heat_capacity"),
    cv("cv"),
    viscosity("viscosity"),
    mixtureMW("mixtureMW")
{
}
//--------------------------------------------------------------------
StringNames::~StringNames()
{
}
//--------------------------------------------------------------------
