#ifndef FILEWRITER_H_
#define FILEWRITER_H_

#include <expression/FieldWriter.h>
#include <string>

/**
 *  \class FileWriter
 *  \author James C. Sutherland
 *  \date   Oct 18, 2011
 *
 *  \brief Manages file IO for ODT
 */
class FileWriter
{
  Expr::FieldOutputDatabase db_;
  const Expr::FieldManagerList& fml_;

public:

  /**
   * \param fml the FieldManagerList that holds all of the fields that will be considered for output.
   * \param dbName the name of the output database
   * \param overWriteExisting if true, existing database files will be overwritten
   */
  explicit FileWriter( Expr::FieldManagerList& fml,
                       const std::string dbName = "fields.db",
                       const bool overWriteExisting = false );

  ~FileWriter();

  /**
   * \brief request output of a field of the given type
   * \param fieldTag the Expr::Tag for the desired field.
   * \param fieldAlias the name of the field in the output file.
   * \tparam FieldT the type of field
   */
  template<typename FieldT>
  void
  request_field_output( const Expr::Tag& fieldTag, const std::string & fieldAlias )
  {
    db_.request_field_output<FieldT>(fieldTag, fieldAlias);
  }

  /**
   * \brief request a field for output
   * \param fieldTag the Expr::Tag for the desired field.
   * \param fieldAlias the name of the field in the output file.
   */
  void
  request_field_output( const Expr::Tag & fieldTag,
                        const std::string & fieldAlias );

  /**
   * \brief write a file to the database at the given value of the simulation time.
   * \param timeStamp the simulation time - name of the file
   */
  void write_file( const double timeStamp ) const;

  /**
   * \brief set fields with the given names by loading values from the
   *        output database with the supplied name.
   * \param restartTime the time (as a string) to load the fields from
   * \param varNames names of fields to load
   *
   * Note that all fields should be registered in the FieldManagerList
   * given to this object at construction, and must have the same name
   * (case sensitive) as those supplied here.
   */
  void set_fields_from_file( const std::string & restartTime,
                             const std::map<std::string,std::string>& varMap );

  /**
   * \brief obtain the underlying FieldOutputDatabase object.
   *        This should not normally need to be called.
   */
  Expr::FieldOutputDatabase& get_db(){ return db_; }
};

#endif /* FILEWRITER_H_ */
