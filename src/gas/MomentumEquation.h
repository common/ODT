#ifndef MomentumEquation_h
#define MomentumEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types

#include <gas/ScalarRHS.h>
#include <gas/RxnDiffusionSetup.h>

// forward declarations
namespace Cantera{ class Transport; };

/**
 *  @class  MomentumEquation
 *  @author James C. Sutherland
 *  @date   December, 2008
 *  @brief  Implements the momentum transport equation
 */
class MomentumEquation : public Expr::TransportEquation
{
public:

  enum Direction{ XDIR, YDIR, ZDIR };

  MomentumEquation( Expr::ExpressionFactory& exprFactory,
                    const Direction dir,
                    const bool doAdvection,
                    const GasChemistry gaschem );

  ~MomentumEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  Cantera::Transport* trans_;
  const Direction dir_;

  static std::string get_var_name( const Direction dir );

};

#endif
