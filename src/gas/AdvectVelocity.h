#ifndef AdvectVelocity_h
#define AdvectVelocity_h

#include <expression/ExprLib.h>
#include <spatialops/OperatorDatabase.h>


/**
 *  @class AverageAdvectVelocity
 *
 *  @brief Calculates the averaged advecting velocity for use in
 *         determining the downstream position of the ODT line.
 */
template< typename FieldT >
class AverageAdvectVelocity
  : public Expr::Expression<FieldT>
{
public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag vT_, xT_;
  public:
    /**
     *  @param avgVelTag the result of this expression
     *  @param xcoordTag The Expr::Tag that defines the coordinates
     *         corresponding to the locations where the velocity is
     *         stored.
     *  @param velTag The Expr::Tag that defines the velocity
     *         components transported by the ODT equations.
     *
     *  @brief Builds an advecting velocity that is the average of the
     *         transported velocity.
     */
    Builder( const Expr::Tag& avgVelTag,
             const Expr::Tag& xcoordTag,
             const Expr::Tag& velTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new AverageAdvectVelocity<FieldT>(xT_,vT_); }
  };

  void evaluate();

protected:

  AverageAdvectVelocity( const Expr::Tag& xcoordTag,
                         const Expr::Tag& velTag );

  DECLARE_FIELDS( FieldT, vel_, x_ )
};


//====================================================================


/**
 *  @class AdvectVelocity
 *  @author James C. Sutherland
 *  @date   September, 2008
 *
 *  @brief Calculates the advecting velocity for use in ODT.
 */
template< typename InterpT >
class AdvectVelocity
  : public Expr::Expression<typename InterpT::DestFieldType>
{

  typedef typename InterpT::DestFieldType AdvelT;
  typedef typename InterpT::SrcFieldType  VelT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag vT_;
  public:

    /**
     *  @param advelTag the result of this expression
     *  @param velTag The Expr::Tag that describes the transported
     *         velocity that is to be used in defining the advecting
     *         velocity.
     */
    Builder( const Expr::Tag& advelTag,
             const Expr::Tag& velTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new AdvectVelocity<InterpT>(vT_); }
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  AdvectVelocity( const Expr::Tag& velTag );

  DECLARE_FIELD( VelT, vel_ )

  const InterpT* interpOp_;

};







// ###################################################################
//
//                           Implementation
//
// ###################################################################




template< typename FieldT >
AverageAdvectVelocity<FieldT>::
AverageAdvectVelocity( const Expr::Tag& xcoordTag,
                       const Expr::Tag& velTag )
  : Expr::Expression<FieldT>()
{
  this->set_gpu_runnable(false);

  x_   = this->template create_field_request<FieldT>( xcoordTag );
  vel_ = this->template create_field_request<FieldT>( velTag    );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
AverageAdvectVelocity<FieldT>::
evaluate()
{
  using SpatialOps::operator<<=;

  FieldT& advectVel = this->value();

  const FieldT& x   = x_  ->field_ref();
  const FieldT& vel = vel_->field_ref();

  double L = 0.0;
  double v = 0.0;

  // integrate  $V=\frac{1}{L} \int_0^L u \mathrm{d} x$
  typename FieldT::const_iterator ix   = x.begin();
  typename FieldT::const_iterator ixp1 = ix; ++ixp1;
  typename FieldT::const_iterator iv   = vel.begin();
  typename FieldT::const_iterator ivp1 = iv; ++ivp1;
  for( ; ixp1!=x.end(); ++ix, ++ixp1, ++iv, ++ivp1 ){
    const double term1 = 0.5*(*ixp1-*ix);
    v += term1 * ( *ivp1 + *iv );
    L += term1 * ( *ixp1 + *ix );
  }
  advectVel <<= v/L;
}

//--------------------------------------------------------------------

template< typename FieldT >
AverageAdvectVelocity<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& xcoordTag,
         const Expr::Tag& velTag )
  : ExpressionBuilder(result),
    vT_(velTag),
    xT_(xcoordTag)
{}

//--------------------------------------------------------------------



//====================================================================



//--------------------------------------------------------------------

template< typename InterpT >
AdvectVelocity<InterpT>::
AdvectVelocity( const Expr::Tag& velTag )
  : Expr::Expression<AdvelT>()
{
  this->set_gpu_runnable(true);
  vel_ = this->template create_field_request<VelT>(velTag);
}

//--------------------------------------------------------------------

template< typename InterpT >
void
AdvectVelocity<InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename InterpT >
void
AdvectVelocity<InterpT>::
evaluate()
{
  using namespace SpatialOps;
  this->value() <<= (*interpOp_)( vel_->field_ref() );
}

//--------------------------------------------------------------------

template< typename InterpT >
AdvectVelocity<InterpT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& velTag )
  : ExpressionBuilder(result),
    vT_(velTag)
{}



#endif // AdvectVelocity_h
