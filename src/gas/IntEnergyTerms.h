#ifndef IntEnergyTerms_h
#define IntEnergyTerms_h


#include <expression/ExprLib.h>

/**
 *  @class IntEnergySrc
 *  @author James C. Sutherland
 *  @date  October, 2008
 *
 *  Calculates source terms for the internal energy equation:
 *  \f[
 *     - \nabla \cdot \left( \tau \cdot \mathbf{u} + p \mathbf{u} \right)
 *  \f]
 *
 *  For two-component ODT this becomes
 *  \f[
 *    -\frac{\partial }{\partial x}
 *    \left(
 *       \tau_{xx} u + \tau_{xy} v + \tau_{xz} w
 *        + p u
 *     \right)
 *  \f]
 */
template< typename DivT, typename InterpT >
class IntEnergySrc
  : public Expr::Expression< typename DivT::DestFieldType >
{
  typedef typename DivT::SrcFieldType   FluxT;
  typedef typename DivT::DestFieldType  ScalarT;

  const bool isPCell_;

  const bool haveYVel_, haveZVel_;

  DECLARE_FIELDS( FluxT,  pface_, xVelFace_, tauxx_, tauxy_, tauxz_ )
  DECLARE_FIELDS( ScalarT, yVel_, zVel_, pcell_ )

  const DivT*    divOp_;
  const InterpT* interpOp_;

  IntEnergySrc( const bool isPCell,
                const Expr::Tag& pTag,
                const Expr::Tag& xVelTag,  ///< advecting velocity (at face)
                const Expr::Tag& tauxxTag,
                const Expr::Tag& yVelTag,  ///< cell centered velocity
                const Expr::Tag& tauxyTag,
                const Expr::Tag& zVelTag,  ///< cell centered velocity
                const Expr::Tag& tauxzTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const bool isPCell_;
    const Expr::Tag pTag_, xVelTag_, tauxxTag_, yVelTag_, tauxyTag_, zVelTag_, tauxzTag_;
  public:
    Builder( const Expr::Tag& result,
             const bool isPCell,
             const Expr::Tag& pTag,
             const Expr::Tag& xVelTag,
             const Expr::Tag& tauxxTag,
             const Expr::Tag& yVelTag,
             const Expr::Tag& tauxyTag,
             const Expr::Tag& zVelTag = Expr::Tag(),
             const Expr::Tag& tauxzTag = Expr::Tag() );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};




template< typename FieldT >
class KineticEnergy
  : public Expr::Expression< FieldT >
{
  const bool doY_, doZ_;
  DECLARE_FIELDS( FieldT, xVel_, yVel_, zVel_ )

  KineticEnergy( const Expr::Tag& xVelTag,
                 const Expr::Tag& yVelTag,
                 const Expr::Tag& zVelTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag xVelTag_, yVelTag_, zVelTag_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& xVelTag,
             const Expr::Tag& yVelTag,
             const Expr::Tag& zVelTag );

    Builder( const Expr::Tag& result,
             const Expr::Tag& xVelTag,
             const Expr::Tag& yVelTag );

    Expr::ExpressionBase* build() const;
  };

  void evaluate();
};



// ###################################################################
//
//                           Implementation
//
// ###################################################################




template< typename DivT, typename InterpT >
IntEnergySrc<DivT,InterpT>::
IntEnergySrc( const bool isPCell,
              const Expr::Tag& pTag,
              const Expr::Tag& xVelTag,
              const Expr::Tag& tauxxTag,
              const Expr::Tag& yVelTag,
              const Expr::Tag& tauxyTag,
              const Expr::Tag& zVelTag,
              const Expr::Tag& tauxzTag )
  : Expr::Expression<ScalarT>(),

    isPCell_( isPCell ),

    haveYVel_( yVelTag != Expr::Tag() ),
    haveZVel_( zVelTag != Expr::Tag() )
{
  this->set_gpu_runnable(true);

  xVelFace_= this->template create_field_request<FluxT>( xVelTag  );
  tauxx_   = this->template create_field_request<FluxT>( tauxxTag );

  if( haveYVel_ ){
    tauxy_ = this->template create_field_request<  FluxT>( tauxyTag );
    yVel_  = this->template create_field_request<ScalarT>( yVelTag  );
  }
  if( haveZVel_ ){
    tauxz_ = this->template create_field_request<  FluxT>( tauxzTag );
    zVel_  = this->template create_field_request<ScalarT>( zVelTag  );
  }

  if( isPCell ) pcell_ = this->template create_field_request<ScalarT>( pTag );
  else          pface_ = this->template create_field_request<  FluxT>( pTag );
}

//--------------------------------------------------------------------

template< typename DivT, typename InterpT >
void
IntEnergySrc<DivT,InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_    = opDB.retrieve_operator<DivT   >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename DivT, typename InterpT >
void
IntEnergySrc<DivT,InterpT>::
evaluate()
{
  using namespace SpatialOps;

  ScalarT& src = this->value();

  const FluxT& tauxx = tauxx_   ->field_ref();
  const FluxT& xvel  = xVelFace_->field_ref();

  SpatialOps::SpatFldPtr<FluxT> faceTmp  = SpatialOps::SpatialFieldStore::get<FluxT>( tauxx );
  SpatialOps::SpatFldPtr<FluxT> faceTmp2 = SpatialOps::SpatialFieldStore::get<FluxT>( tauxx );

  if( isPCell_ ){
    *faceTmp <<= (*interpOp_)( pcell_->field_ref() );
  }
  else{
    *faceTmp <<= pface_->field_ref();
  }

  *faceTmp <<= ( *faceTmp + tauxx ) * xvel;

  if( haveYVel_ ){
    const FluxT& tauxy  = tauxy_->field_ref();
    const ScalarT& yvel = yVel_ ->field_ref();
    *faceTmp2 <<= (*interpOp_)( yvel );
    *faceTmp <<= *faceTmp + (*faceTmp2 * tauxy);
  }

  if( haveZVel_ ){
    const FluxT& tauxz  = tauxz_->field_ref();
    const ScalarT& zvel = zVel_ ->field_ref();
    *faceTmp2 <<= (*interpOp_)( zvel );
    *faceTmp <<= *faceTmp + (*faceTmp2 * tauxz);
  }

  src <<= -(*divOp_)( *faceTmp );
}

//--------------------------------------------------------------------

template< typename DivT, typename InterpT >
IntEnergySrc<DivT,InterpT>::Builder::
Builder( const Expr::Tag& result,
         const bool isPCell,
         const Expr::Tag& pTag,
         const Expr::Tag& xVelTag,
         const Expr::Tag& tauxxTag,
         const Expr::Tag& yVelTag,
         const Expr::Tag& tauxyTag,
         const Expr::Tag& zVelTag,
         const Expr::Tag& tauxzTag )
  : ExpressionBuilder(result),
    isPCell_( isPCell ),
    pTag_( pTag ),
    xVelTag_ ( xVelTag  ),
    tauxxTag_( tauxxTag ),
    yVelTag_ ( yVelTag  ),
    tauxyTag_( tauxyTag ),
    zVelTag_ ( zVelTag  ),
    tauxzTag_( tauxzTag )
{}

//--------------------------------------------------------------------

template< typename DivT, typename InterpT >
Expr::ExpressionBase*
IntEnergySrc<DivT,InterpT>::Builder::build() const
{
  return new IntEnergySrc<DivT,InterpT>( isPCell_, pTag_,
                                         xVelTag_, tauxxTag_,
                                         yVelTag_, tauxyTag_,
                                         zVelTag_, tauxzTag_ );
}

//--------------------------------------------------------------------






//--------------------------------------------------------------------

template< typename FieldT >
KineticEnergy<FieldT>::
KineticEnergy( const Expr::Tag& xVelTag,
               const Expr::Tag& yVelTag,
               const Expr::Tag& zVelTag )
  : Expr::Expression<FieldT>(),
    doY_( yVelTag != Expr::Tag() ),
    doZ_( zVelTag != Expr::Tag() )
{
  this->set_gpu_runnable(true);

  xVel_ = this->template create_field_request<FieldT>( xVelTag );
  if( doY_ ) yVel_ = this->template create_field_request<FieldT>( yVelTag );
  if( doZ_ ) zVel_ = this->template create_field_request<FieldT>( zVelTag );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
KineticEnergy<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& ke = this->value();
  const FieldT& xvel = xVel_->field_ref();
  ke <<= 0.5 * xvel * xvel;
  if( doY_ ){
    const FieldT& yvel = yVel_->field_ref();
    ke <<= ke + 0.5 * yvel * yvel;
  }
  if( doZ_ ){
    const FieldT& zvel = zVel_->field_ref();
    ke <<= ke + 0.5 * zvel * zvel;
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
KineticEnergy<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& xVelTag,
         const Expr::Tag& yVelTag,
         const Expr::Tag& zVelTag )
  : ExpressionBuilder(result),
    xVelTag_( xVelTag ),
    yVelTag_( yVelTag ),
    zVelTag_( zVelTag )
{}

//--------------------------------------------------------------------

template< typename FieldT >
KineticEnergy<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& xVelTag,
         const Expr::Tag& yVelTag )
  : ExpressionBuilder(result),
    xVelTag_( xVelTag ),
    yVelTag_( yVelTag ),
    zVelTag_( "", Expr::INVALID_CONTEXT )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
KineticEnergy<FieldT>::Builder::build() const
{
  return new KineticEnergy<FieldT>( xVelTag_, yVelTag_, zVelTag_ );
}

//--------------------------------------------------------------------

#endif
