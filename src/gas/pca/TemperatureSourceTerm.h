#ifndef TemperatureSourceTerm_h
#define TemperatureSourceTerm_h


#include <expression/ExprLib.h>
#include <OperatorsAndFields.h>  // defines field types


/**
 *  @class  TemperatureSourceTerm
 *  @author Amir Biglari
 *
 *  @brief Calculates the temperature source term in the alternate form
 *         of the energy transport equation
 *  
 */

class TemperatureSourceTerm
: public Expr::Expression< VolField >
{
public:
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag result,
             const Expr::TagList& svSourceTermTagList,
             const Expr::TagList& svEnthalpyTagList,
             const Expr::Tag& heatCapacityTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList svSrcTermTs_, svEntpyTs_;
    const Expr::Tag heatCapT_;
  };
  
  
  ~TemperatureSourceTerm(){}
  void evaluate();
  
private:
  
  TemperatureSourceTerm( const Expr::TagList& svSourceTermTagList,
                         const Expr::TagList& svEnthalpyTagList,
                         const Expr::Tag& heatCapacityTag );

  DECLARE_VECTOR_OF_FIELDS( VolField, svSrcTerms_   )
  DECLARE_VECTOR_OF_FIELDS( VolField, svEnthalpies_ )
  
  DECLARE_FIELD( VolField, heatCap_ )
  
  std::vector< VolField::const_iterator > svSrcTermIVec_;
  std::vector< VolField::const_iterator > svEnthalpyIVec_;
};

#endif // TemperatureSourceTerm_h
