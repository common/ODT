#ifndef GridX2Y_h
#define GridX2Y_h


#include <expression/ExprLib.h>
#include <OperatorsAndFields.h>  // defines field types

#include <gas/pca/Map2GridVec.h>


/**
 *  @class  GridX2Y
 *  @author Amir Biglari
 *
 *  @brief Applies the the grid interpolation on any dataset consistent with the Map2Grid class
 *
 */

class GridX2Y
: public Expr::Expression< VolField >
{
public:
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const Expr::TagList& indepVarTags,
             const Map2GridVec& m2gVec);
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList indepVarTs_;
    const Map2GridVec m2gVec_;
  };

  ~GridX2Y(){}
  void evaluate();
  
private:
  
  GridX2Y( const Expr::TagList& indepVarTags,
           const Map2GridVec& m2gVec);
  
  std::vector<double> indepVarPtVals_;
  
  const Map2GridVec m2gVec_;
  
  DECLARE_VECTOR_OF_FIELDS( VolField, indepVars_ )
  
  std::vector< VolField::const_iterator > indepVarIVec_;
  std::vector< VolField::iterator > depVarIVec_;
  
};

#endif	// GridX2Y_h
