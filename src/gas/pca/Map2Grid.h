//
//  Map2Grid.h
//  M2G_test
//
//  Created by abiglari on 6/26/14.
//

#ifndef __M2G_test__Map2Grid__
#define __M2G_test__Map2Grid__

#include <iosfwd>
#include <string>
#include <vector>

class Map2Grid{
  std::vector<double> gridLims_, gridVals_;
  int ndims_;
  int nPts_;

  void readFromFile( const std::string gridFile );

public:

  Map2Grid( const std::vector<double>& gridLims,
            const std::vector<double>& gridVals,
            const int ndims,
            const int nPts );

  Map2Grid( const std::string gridFile,
            const int ndims );

  ~Map2Grid();

  double evaluate( const std::vector<double>& ivar ) const;

  void display( std::ostream& outputStrm ) const;
};

#endif
