/*
 *  PCA.h
 *  testODT
 *
 *  Created by amir on 8/27/13.
 *  Copyright (c) 2013 abiglari. All rights reserved.
 *
 */

#include <OperatorsAndFields.h>

#include <iosfwd>
#include "gas/pca/Matrix.h"
#include <string>

#ifndef PCA_H
#define PCA_H

class PCA  {
  Matrix eigenvectors_, transEigenvectors_, normalEigVecs_;
  std::vector<double> centers_, scales_, eigVecSums_;
  int nEta_;
  void readFromFile (const std::string pcaFile);
public:
  std::vector<double> mappedCenters_;
  PCA (const std::string pcaFile, const int nEta);
  PCA();
  ~PCA();
  void x2eta (const Matrix& X, Matrix& eta, bool center=true) const;
  void eta2x (const Matrix& eta, Matrix& X, bool center=true) const;
  void display( std::ostream& outputStrm ) const;
  void diffusionMatrixTransform (const std::vector<double>& Dsv, Matrix& Dpc, std::string model) const;
  void PCTripletTerm( VolField& tripletTerm, const VolField& rhoPC, const VolField& density, const int pcNum ) const;
  void RhoPCFromTripletTerm( VolField& rhoPC, const VolField& tripletTerm, const VolField& density, const int pcNum ) const;
  
};

#endif
