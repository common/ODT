#ifndef StateVarDiffusiveTerms_h
#define StateVarDiffusiveTerms_h


#include <expression/ExprLib.h>
#include <OperatorsAndFields.h>  // defines field types


/**
 *  @class  StateVarDiffusiveTerms
 *  @author Amir Biglari
 *
 *  @brief Calculates the diffusive terms for all of the state-variable 
 *         including the temperature diffusive term in the alternate form
 *         of the energy transport equation
 *  
 */

template< typename DivT >
class StateVarDiffusiveTerms
: public Expr::Expression< typename DivT::DestFieldType >
{

public:
  
  typedef typename DivT::DestFieldType FieldT;
  typedef typename DivT::SrcFieldType FluxT;
  typedef typename std::vector< const FieldT* > VolFieldVec;
  typedef typename std::vector< const FluxT* > FluxFieldVec;

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const Expr::Tag heatFluxTag,
             const Expr::TagList& specFluxTagList,
             const Expr::TagList& spcEnthalpyTagList,
             const Expr::Tag heatCapacityTag,
             const int nspecies );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList spcFluxTs_, spcEntpyTs_;
    const Expr::Tag heatCapT_, heatFluxT_;
    const int nspec_;
  };
  
  
  ~StateVarDiffusiveTerms(){}
  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  
private:
  
  StateVarDiffusiveTerms( const Expr::Tag heatFluxTag,
                          const Expr::TagList& specFluxTagList,
                          const Expr::TagList& spcEnthalpyTagList,
                          const Expr::Tag heatCapacityTag,
                          const int nspecies );
  
  const int nspec_;
  const DivT* divOp_;

  DECLARE_FIELD( FieldT, heatCap_  )
  DECLARE_FIELD( FluxT,  heatFlux_ )
  
  DECLARE_VECTOR_OF_FIELDS( FluxT,  spcFluxVec_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, spcEnthVec_ )

};

#endif // StateVarDiffusiveTerms_h
