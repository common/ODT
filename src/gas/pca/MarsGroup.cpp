/*
 *  untitled.cpp
 *  testODT
 *
 *  Created by amir on 8/27/13.
 *  Copyright (c) 2013 abiglari. All rights reserved.
 *
 */

#include <iostream>
#include <sstream>
#include <stdexcept>
#include "gas/pca/MarsGroup.h"

using namespace std;

MarsGroup::MarsGroup( const string marsGroupFile )
{
  ifstream mgFStream;
  mgFStream.open(marsGroupFile.c_str());

  string line, field, ivName;

  if(!mgFStream)                                   // file couldn't be opened
  {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "MarsGroup data file could not be opened";
    throw runtime_error(errMsg.str());
  }
  else
    cout << "MarsGroup data file SUCCESSFULLY opened!\n";

  getline(mgFStream, line);
  stringstream ss(line);
  getline(ss, field, ':');
  if (field!="nivar")
  {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "nivars could not be found in its place in the MarsGroup data file";
    throw runtime_error(errMsg.str());
  }
  getline(ss, field, ':');
  stringstream ss2(field);
  ss2 >> nivar_;

  getline(mgFStream, line);
  stringstream ss3(line);
  getline(ss3, field, ':');
  if (field!="iVarNames")
  {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "iVarNames could not be found in its place in the MarsGroup data file";
    throw runtime_error(errMsg.str());
  }
  while (getline(ss3,field,',') )                    // break line into comma delimitted fields
  {
    stringstream ss2(field);
    ss2 >> ivName;
    iVarNames_.push_back(ivName);                  // add the next independent variable into the vector
  }

  bool finish=false;
  dVarNames_.clear();
  do {
    getline(mgFStream,line);
    if (line.length()<20 || line.substr(0,20)!="mars properties for ")
    {
      ostringstream errMsg;
      errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "This is probably not a MarsGroup data file. Each mars object block should start with 'mars properties for dvar:'";
      throw runtime_error(errMsg.str());
    }
    dVarNames_.push_back(line.substr(20,line.length()-21));
    mgFStream.seekg(-(line.length()+1),mgFStream.cur);

    m_.push_back(Mars(mgFStream));

    if (!getline(mgFStream, line))
      finish=true;
    else
      mgFStream.seekg(-(line.length()+1),mgFStream.cur);

  } while (!finish);

  mgFStream.close();

  ndvar_ = (int)m_.size();
}

MarsGroup::~MarsGroup()
{}

void MarsGroup::display(ostream& outputStrm) const
{
  cout << "Number independent variables: " << nivar_ << endl;
  cout << "Number dependent   variables: " << ndvar_ << endl;
  for (int i=0; i<m_.size(); i++) {
    (m_[i]).display(outputStrm);
  }
}

//Mars& MarsGroup::operator[] (const unsigned i) const
//{
//    if (i>=ndvar_)
//    {
//        ostringstream errMsg;
//        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Index exceedes number of mars objects in the mars group";
//        throw runtime_error(errMsg.str());
//    }
//    Mars *m = new Mars(m_[i]);
//    
//    return *m;
//}

const Mars& MarsGroup::operator[] (const string dVarName) const
{
  bool found=false;
  int index;
  for (int i=0; i<dVarNames_.size(); i++) {
    if (dVarName==dVarNames_[i])
    {
      found = true;
      index = i;
      break;
    }
  }
  if (!found) {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The MarsGroup does not have " << dVarName << " as one of its dependent variables";
    throw runtime_error(errMsg.str());
  }
  return m_[index];
}

//vector<double>& MarsGroup::operator() (const vector<double>& iVars) const
//{
//    if (iVars.size()!=nivar_) {
//        ostringstream errMsg;
//        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Inconsistent number of indpendent variables";
//        throw runtime_error(errMsg.str());
//    }
//    return (evaluate(iVars));
//}
//
//vector<double>& MarsGroup::evaluate (const vector<double>& iVars) const
//{
//    if (iVars.size()!=nivar_) {
//        ostringstream errMsg;
//        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Inconsistent number of indpendent variables";
//        throw runtime_error(errMsg.str());
//    }
//    vector<double> *dVars = new vector<double>;
//    for (int i=0; i<m_.size(); i++) {
//        dVars->push_back(m_[i](iVars));
//    }
//    return *dVars;
//}

const vector<string>& MarsGroup::get_dvar_names() const
{
  return dVarNames_;
}

const vector<string>& MarsGroup::get_ivar_names() const
{
  return iVarNames_;
}
