#include <gas/pca/PCEquation.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper

//--- Expressions for Species related expressions ---//
#include <gas/SpeciesDiffusion.h>
#include <gas/PrimVar.h>
#include <gas/ScalarRHS.h>
#include <pokitt/SpeciesN.h>
#include <pokitt/MixtureMolWeight.h>

//--- String Names ---//
#include <StringNames.h>

#include "gas/pca/Matrix.h"
#include "gas/pca/PCA.h"
#include "gas/pca/PCAX2Eta.h"
#include "gas/pca/PCDiffusion.h"
#include "gas/pca/PCExtraTerm.h"
#include "gas/pca/TemperatureSourceTerm.h"
#include "gas/pca/StateVarDiffusiveTerms.h"
#include "gas/pca/MGX2Y.h"
#include "gas/pca/GridX2Y.h"

using Expr::Tag;
using Expr::STATE_NONE;
using Expr::STATE_N;
using std::cout;
using namespace std;

//====================================================================

class RhoPCICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Tag& result,
             const Tag& rhoTag,
             const Tag& pcTag,
             const int pcNum )
    : ExpressionBuilder(result),
    rhoT_(rhoTag), pcT_(pcTag), pcNum_(pcNum)
    {}
    Expr::ExpressionBase* build() const{ return new RhoPCICExpr( rhoT_, pcT_, pcNum_ ); }
  private:
    const Tag rhoT_, pcT_;
    const int pcNum_;
  };

  void evaluate()
  {
    using namespace SpatialOps;
    VolField& rhoPCi = this->value();
    const VolField& rho = rho_->field_ref();
    const VolField& pci = pci_->field_ref();
    rhoPCi <<= rho * pci;
  }
  
  ~RhoPCICExpr(){}
  
private:
  RhoPCICExpr( const Tag rhoTag,
               const Tag pcTag,
               const int pcNum )
  : Expr::Expression<VolField>()
  {
    rho_ = this->create_field_request<VolField>( rhoTag );
    const std::string name = pcTag.name() + "_" + boost::lexical_cast<std::string>( pcNum );
    pci_ = this->create_field_request<VolField>( Expr::Tag(name, pcTag.context()) );
  }

  DECLARE_FIELDS( VolField, rho_, pci_ )
};

//====================================================================

PCEquation::
PCEquation( Expr::ExpressionFactory& exprFactory,
            const bool doAdvection,
            const int pcNum,
            const int numPCs,
            const int nSpecies,
            const PCA& pca,
            const MarsGroup& pcSrcMG,
            const MarsGroup& stVarMG,
            const Map2GridVec& m2gVec,
            const Tag pcSrcTag,
            const bool modelSrcTerms,
            const bool modelStateVars,
            const bool mapSrcTerms,
            const bool includeExTempTerm,
            const bool calculateTempSrc,
            const bool standAlonePCSolver,
            const std::string pcDiffFluxModel,
            std::vector<Expr::ExpressionID>& pcIDs,
            Expr::ExpressionID& stVarsID,
            Expr::ExpressionID& tempSrcID )
: Expr::TransportEquation( get_soln_var_name( pcNum ),
                           get_rhs_tag( pcNum ) ),
  pcNum_   ( pcNum    ),
  nPCs_    ( numPCs   ),
  nSpecies_( nSpecies ),
  pca_     ( pca      ),
  pcSrcMG_ ( pcSrcMG  ),
  stVarMG_ ( stVarMG  ),
  m2gVec_  ( m2gVec   ),
  standAlonePCSolver_( standAlonePCSolver )
{
  typedef PrimVar<VolField>::Builder    PC;
  
  // register required expressions
  static bool haveBuiltOneTimeExpressions = false;
  
  const StringNames& sName = StringNames::self();

  if( !haveBuiltOneTimeExpressions ){
    
    haveBuiltOneTimeExpressions = true;
    
    // register (n) PCs expressions
    for( int i=0; i<nPCs_; ++i ){
      std::ostringstream name1,name2;
      name1<< sName.pc << "_" << i;
      name2<< sName.rhoPC << "_" << i;
      Expr::ExpressionID pcID = exprFactory.register_expression( new PC( Tag( name1.str(), STATE_N ),
                                                                         Tag( name2.str(),    STATE_N ),
                                                                         Tag( sName.density, STATE_N ) ));
      pcIDs.push_back(pcID);
    }
    
    Expr::TagList pcTagList;
    for( int i=0; i<nPCs_; ++i ){
      std::ostringstream pcName;
      pcName<< sName.pc << "_" << i;
      pcTagList.push_back( Tag(pcName.str(),STATE_N) );
    }
    
    // register state variable expressions here if they are supposed to be modeled with MARS regression
    if( standAlonePCSolver_ || modelStateVars ){
      Expr::TagList stateVarTags;
      if( standAlonePCSolver_ ){
        stateVarTags.push_back( Expr::Tag(sName.temperature, STATE_N) );
        
        for( int i=0; i<nSpecies_-1; i++ ){
          stateVarTags.push_back( Expr::Tag(CanteraObjects::species_name(i), STATE_N) );
        }

        typedef pokitt::MixtureMolWeight<VolField>::Builder MixMW;
        exprFactory.register_expression( new MixMW( Tag( sName.mixtureMW, STATE_N ),
                                                    tag_list( CanteraObjects::species_names(), STATE_N ),
                                                    pokitt::MASS ) );
      }
      else if( modelStateVars ){
        for( int i=0; i<nSpecies_; i++ ){
          std::ostringstream varName;
          varName << "StateVar_" << i;
          stateVarTags.push_back( Expr::Tag(varName.str(), STATE_N) );
        }
      }
      const vector<string>& mgYNames = stVarMG_.get_dvar_names();
      const vector<string>& mgXNames = stVarMG_.get_ivar_names();
      
      if( mgXNames.size()!=pcTagList.size() ){
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The number of independent variables in the MarsGroup does not match the number of independent variables passed to the MarsGroup";
        throw runtime_error(errMsg.str());
      }
      if( mgYNames.size()!=stateVarTags.size() ){
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The number of dependent variables in the MarsGroup does not match the number of dependent variables passed to the MarsGroup";
        throw runtime_error(errMsg.str());
      }
      for( int i=0; i<pcTagList.size(); i++ ){
        if( (pcTagList[i]).name()!=mgXNames[i] ){
          ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The names of independent variables passed to the MarsGroup should be the same and in the same order as the MarsGroup independent variable names";
          throw runtime_error(errMsg.str());
        }
      }
      
      // register Temperature and (n-1) species expressions (state variables)
      typedef MGX2Y::Builder StateVars;
      stVarsID = exprFactory.register_expression( new StateVars( stateVarTags,
                                                                 pcTagList,
                                                                 stVarMG_,
                                                                 true ) );
      if( standAlonePCSolver_ ){
        // register nth species expression
        const string spnName = CanteraObjects::species_name(nSpecies_-1);
        exprFactory.register_expression( new pokitt::SpeciesN<VolField>::Builder( Tag( spnName, STATE_N ),
                                                                                  Expr::tag_list( CanteraObjects::species_names(), STATE_N),
                                                                                  pokitt::CLIPSPECN ) );
      }
    }
    
    const Expr::TagList specEnthalpyTagList = tag_list( CanteraObjects::species_names(), STATE_N, sName.enthalpy+"_" );
    const Tag heatCapacity( sName.heat_capacity,STATE_N);
    
    cout << "****************** PC Source Term ******************\n";
    Expr::TagList pcSourceTermTagList;
    for( int i=0; i<nPCs_; ++i ){
      std::ostringstream pcSource;
      pcSource << sName.pcSourceTerm << "_" << i;
      pcSourceTermTagList.push_back( Tag(pcSource.str(),STATE_N) );
    }

    const Expr::TagList specSourceTermTagList = tag_list( CanteraObjects::species_names(), STATE_N, sName.SpeciesSourceTerm+"_" );

    const Tag tempSrcTag = Tag("Temperature_Source",STATE_N);

    if( (!modelSrcTerms && !mapSrcTerms) || calculateTempSrc ){
      tempSrcID = exprFactory.register_expression( new TemperatureSourceTerm::Builder( tempSrcTag,
                                                                                       specSourceTermTagList,
                                                                                       specEnthalpyTagList,
                                                                                       heatCapacity ) );
    }
    if( !modelSrcTerms && !mapSrcTerms ){
      Expr::TagList svSourceTermTagList;
      svSourceTermTagList.push_back(tempSrcTag);
      svSourceTermTagList.insert(svSourceTermTagList.begin()+1,specSourceTermTagList.begin(),specSourceTermTagList.end()-1);
      
      typedef PCAX2Eta<VolField>::Builder SourceTerm;
      exprFactory.register_expression( new SourceTerm( pcSourceTermTagList,
                                                       svSourceTermTagList,
                                                       pca_,
                                                       false ) );
    }
    else if( modelSrcTerms ){
      const vector<string>& mgYNames = pcSrcMG_.get_dvar_names();
      const vector<string>& mgXNames = pcSrcMG_.get_ivar_names();
      
      if( mgXNames.size()!=pcTagList.size() ){
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The number of independent variables in the MarsGroup does not match the number of independent variables passed to the MarsGroup";
        throw runtime_error(errMsg.str());
      }
      if( mgYNames.size()!=pcSourceTermTagList.size() ){
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The number of dependent variables in the MarsGroup does not match the number of dependent variables passed to the MarsGroup";
        throw runtime_error(errMsg.str());
      }
      for( int i=0; i<pcTagList.size(); i++ ){
        if( (pcTagList[i]).name()!=mgXNames[i] ){
          ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The names of independent variables passed to the MarsGroup should be the same and in the same order as the MarsGroup independent variable names";
          throw runtime_error(errMsg.str());
        }
      }
      for( int i=0; i<pcSourceTermTagList.size(); i++ ){
        if( (pcSourceTermTagList[i]).name()!=mgYNames[i] ){
          ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The names of dependent variables passed to the MarsGroup should be the same and in the same order as the MarsGroup dependent variable names";
          throw runtime_error(errMsg.str());
        }
      }
      
      typedef MGX2Y::Builder PCSrcTerm;
      exprFactory.register_expression( new PCSrcTerm( pcSourceTermTagList,
                                                      pcTagList,
                                                      pcSrcMG_) );
      
    }
    else{
      const vector<string>& m2gYNames = m2gVec_.get_dvar_names();
      const vector<string>& m2gXNames = m2gVec_.get_ivar_names();
      
      if( m2gXNames.size()!=pcTagList.size() ){
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The number of independent variables in the Map2GridVec does not match the number of independent variables passed to the MarsGroup";
        throw runtime_error(errMsg.str());
      }
      if( m2gYNames.size()!=pcSourceTermTagList.size() ){
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The number of dependent variables in the Map2GridVec does not match the number of dependent variables passed to the MarsGroup";
        throw runtime_error(errMsg.str());
      }
      for( int i=0; i<pcTagList.size(); i++ ){
        if( (pcTagList[i]).name()!=m2gXNames[i] ){
          ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The names of independent variables passed to the Map2GridVec should be the same and in the same order as the MarsGroup independent variable names";
          throw runtime_error(errMsg.str());
        }
      }
      for( int i=0; i<pcSourceTermTagList.size(); i++ ){
        if( (pcSourceTermTagList[i]).name()!=m2gYNames[i] ){
          ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The names of dependent variables passed to the Map2GridVec should be the same and in the same order as the MarsGroup dependent variable names";
          throw runtime_error(errMsg.str());
        }
      }
      
      typedef GridX2Y::Builder PCSrcTerm;
      exprFactory.register_expression( new PCSrcTerm( pcSourceTermTagList,
                                                     pcTagList,
                                                     m2gVec_) );
      
    }
    
    cout << "****************** PC Diffusive Term ******************\n";
    
    string DiffCoefModel = "Direct_Closure";
    
    if( pcDiffFluxModel=="Weighted" || pcDiffFluxModel=="Analytic" ){
   
      Expr::TagList pcDiffFluxTagList;
      for( int i=0; i<nPCs_; ++i ){
        std::ostringstream diff;
        diff << sName.pcDiffFluxX << "_" << i;
        pcDiffFluxTagList.push_back( Tag(diff.str(),STATE_N) );
      }

      typedef SimplePCDiffFlux<FaceField>::Builder Flux;
      exprFactory.register_expression( new Flux( pcDiffFluxTagList,
                                                 Tag(sName.temperature,          STATE_N),
                                                 Tag(sName.heat_capacity,        STATE_N), /// what should we do for this diffusion coefs?
                                                 Tag(sName.thermal_conductivity, STATE_N),
                                                 Tag(sName.pc,                   STATE_N),
                                                 tag_list( CanteraObjects::species_names(), STATE_N ),
                                                 Tag(sName.density,              STATE_N),
                                                 pca_,
                                                 nPCs_,
                                                 pcDiffFluxModel ) );
      
      if( includeExTempTerm ){
        Expr::TagList diffFluxTagList;
        for( int i=0; i<nSpecies_; ++i ){
          string dif = sName.specDiffFluxX + "_" + CanteraObjects::species_name(i);
          diffFluxTagList.push_back( Tag(dif,STATE_N) );
        }
        
        Expr::TagList pcExTermTagList;
        for( int i=0; i<nPCs_; ++i ){
          string term = "ExtraTerm_PC_" + boost::lexical_cast<string>(i);
          pcExTermTagList.push_back( Tag(term,STATE_N) );
        }
        
        typedef PCExtraTerm<FaceField>::Builder PCExTerm;
        exprFactory.register_expression( new PCExTerm( pcExTermTagList,
                                                       Tag(sName.temperature,   STATE_N),
                                                       Tag(sName.heat_capacity, STATE_N), /// what should we do for this diffusion coefs?
                                                       Tag(sName.pc,            STATE_N),
                                                       tag_list( CanteraObjects::species_names(), STATE_N ),
                                                       pca_,
                                                       nPCs_,
                                                       diffFluxTagList ) );
        
        // Adding the PC extra terms to the source term so they can be added to the RHS withouth any change
        for( int i=0; i<nPCs_; ++i ){
          exprFactory.attach_dependency_to_expression(pcExTermTagList[i], pcSourceTermTagList[i]);
        }
      }
    } // Diffusion coefficient models (Analytic)
    else if( pcDiffFluxModel=="Direct_Closure" ){
      
      Expr::TagList pcDiffTermTagList;
      for( int i=0; i<nPCs_; ++i ){
        std::ostringstream diff;
        diff << "PC_Diffusive_Term_" << i;
        pcDiffTermTagList.push_back( Tag(diff.str(),STATE_N) );
      }
      
      const Expr::TagList specDiffFluxTagList = tag_list( CanteraObjects::species_names(), STATE_N, sName.specDiffFluxX+"_" );
      
      Expr::TagList svDiffTermTagList;
      for( int i=0; i<nSpecies_; ++i ){
        std::ostringstream svDiffTerm;
        svDiffTerm << "StateVar_Diff_Term_" << i ;
        svDiffTermTagList.push_back( Tag(svDiffTerm.str(),STATE_N) );
      }

      exprFactory.register_expression( new StateVarDiffusiveTerms<DivOp>::Builder( svDiffTermTagList,
                                                                                   Tag("HeatFlux",STATE_N),
                                                                                   specDiffFluxTagList,
                                                                                   specEnthalpyTagList,
                                                                                   heatCapacity,
                                                                                   nSpecies_) );

      typedef PCAX2Eta<VolField>::Builder DiffTerm;
      exprFactory.register_expression( new DiffTerm( pcDiffTermTagList,
                                                     svDiffTermTagList,
                                                     pca_,
                                                     false ) );
      
      // Adding the PC diffusive terms to the source term so they can be added to the RHS without any change
      for( int i=0; i<nPCs_; ++i ){
        exprFactory.attach_dependency_to_expression(pcDiffTermTagList[i], pcSourceTermTagList[i]);
      }

    } // Diffusion coefficient models (Direct_Closure)
    else {
      ostringstream errMsg;
      errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The PC diffusive flux model " << pcDiffFluxModel << " is not supported, please revise your input file.";
      throw runtime_error(errMsg.str());

    } // Diffusion coefficient models
    
  } // if haveBuiltOneTimeExpressions


  // RHS Expression
  {
    Expr::TagList pcSrcs;
    const bool doReaction = pcSrcTag != Tag();
    if( doReaction ) pcSrcs.push_back( pcSrcTag );

    typedef ScalarRHS<VolField> RHS;
    RHS::FieldTagInfo pcFieldTagInfo;

    if( pcDiffFluxModel != "Direct_Closure" ){
      std::ostringstream fnam;
      fnam << sName.pcDiffFluxX << "_" << pcNum;
      pcFieldTagInfo[ RHS::DIFFUSION_FLUX ] = Tag( fnam.str(), STATE_N );
    }

    if( doAdvection ){
      pcFieldTagInfo[ RHS::SOLUTION_VARIABLE ] = Tag( get_soln_var_name(pcNum), STATE_N );
      pcFieldTagInfo[ RHS::ADVECTING_VELOCITY] = Tag( sName.xvel+"_advect",     STATE_N );
    }

    exprFactory.register_expression( new RHS::Builder( get_rhs_tag(pcNum), pcFieldTagInfo, pcSrcs ) );
  }
}

//--------------------------------------------------------------------

PCEquation::~PCEquation()
{}

//--------------------------------------------------------------------

void
PCEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{
  // I think we need to apply PCA mapping on BCs here as well but I don't know how
}

//--------------------------------------------------------------------

Expr::ExpressionID
PCEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  // register IC expression
  static bool haveBuiltOneTimeExpressions = false;
  const StringNames& sName = StringNames::self();
  
  if( !haveBuiltOneTimeExpressions ){
    
    haveBuiltOneTimeExpressions = true;
    
    Expr::TagList stateVarTags;
    stateVarTags.push_back( Expr::Tag(sName.temperature, STATE_N) ); //***** we probably need to add this to the sName strings
    
    for( int i=0; i<nSpecies_-1; i++ ){
      stateVarTags.push_back( Expr::Tag(CanteraObjects::species_name(i), STATE_N) );
    }
    
    Expr::TagList pcTags;
    for( int i=0; i<nPCs_; i++ ){
      std::ostringstream pcName;
      pcName << sName.pc << "_" << i;
      pcTags.push_back( Expr::Tag(pcName.str(), STATE_N) );
    }

    typedef PCAX2Eta<VolField>::Builder PCIC;
    exprFactory.register_expression( new PCIC( pcTags,
                                               stateVarTags,
                                               pca_,
                                               true ) );

    if( standAlonePCSolver_ ){
      typedef pokitt::MixtureMolWeight<VolField>::Builder MixMW;
      exprFactory.register_expression( new MixMW( Tag( sName.mixtureMW, STATE_N ),
                                                  tag_list( CanteraObjects::species_names(), STATE_N ),
                                                  pokitt::MASS ) );
    }
  }
  
  typedef RhoPCICExpr::Builder Builder;
  return exprFactory.register_expression( new Builder( Tag( get_soln_var_name(pcNum_), STATE_N ),
                                                       Tag( sName.density,STATE_N ),
                                                       Tag( sName.pc,STATE_N ),
                                                       pcNum_ ) );
}

//--------------------------------------------------------------------

Expr::Tag
PCEquation::
get_rhs_tag( const int pcNum )
{
  return Expr::Tag( get_soln_var_name(pcNum)+"_RHS", STATE_N );
}

//--------------------------------------------------------------------

std::string
PCEquation::
get_soln_var_name( const int pcNum )
{
  std::ostringstream nam;
  nam << StringNames::self().rhoPC << "_" << pcNum;
  return nam.str();
}

//--------------------------------------------------------------------
