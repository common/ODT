#ifndef PCDiffusion_h
#define PCDiffusion_h


#include <gas/pca/PCA.h>
#include <gas/pca/Matrix.h>

#include <expression/ExprLib.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/transport.h>


/**
 *  @class  PCDiffFlux
 *  @author Amir Biglari
 *
 *  @brief Calculates the diffusive fluxes for all principal components.
 *  This uses the mass mixture-averaged form for the species diffusion
 *  coefficients and maps them into PC space using the PCA mapping to be 
 *  useable for the PCs.
 */
template< typename FluxT >
class SimplePCDiffFlux
: public Expr::Expression< FluxT >
{
  typedef typename SpatialOps::VolType<FluxT>::VolField  ScalarT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Gradient,   ScalarT,FluxT>::type GradT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Interpolant,ScalarT,FluxT>::type InterpT;

  SimplePCDiffFlux( const Expr::Tag& temperatureTag,
                    const Expr::Tag& heatCapacityTag,
                    const Expr::Tag& thermCondTag,
                    const Expr::Tag& pcTag,
                    const Expr::TagList& speciesTags,
                    const Expr::Tag& densTag,
                    const PCA pca,
                    const int nPCs,
                    const std::string model );

  ~SimplePCDiffFlux();

  Cantera::MixTransport* const trans_;
  Cantera::ThermoPhase& gas_;

  const PCA pca_;
  const int nPCs_, nspec_;
  const std::string model_;
  const double pressure_;
  std::vector<double> ptSpec_, ptSvDiffCoeff_, ptPCGrad_;
  
  const GradT* gradOp_;
  const InterpT* interpOp_;
  DECLARE_VECTOR_OF_FIELDS( ScalarT, species_ )
  DECLARE_VECTOR_OF_FIELDS( ScalarT, pcs_     )
  DECLARE_FIELDS( ScalarT, temperature_, density_, heatCap_, tCond_ )

  std::vector< typename FluxT::const_iterator > specIVec_;
  std::vector< typename FluxT::iterator       > fluxIVec_;

public:
  
  typedef typename Expr::Expression<FluxT>::ValVec  SpecFluxT;
  typedef SpatialOps::SpatFldPtr<FluxT> FluxPtrT;
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& heatCapacityTag,
             const Expr::Tag& thermCondTag,
             const Expr::Tag& pcTag,
             const Expr::TagList& speciesTag,
             const Expr::Tag& densTag,
             const PCA pca,
             const int nPCs,
             const std::string model );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const PCA pca_;
    const int nPCs_;
    const std::string mod_;
    const Expr::TagList specT_;
    const Expr::Tag pcT_, densT_, tT_, heatCapT_, tcT_;
  };
  
  
  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  
};


// ###################################################################
//
//                         Implementation
//
// ###################################################################



template< typename FluxT >
SimplePCDiffFlux<FluxT>::
SimplePCDiffFlux( const Expr::Tag& temperatureTag,
                  const Expr::Tag& heatCapacityTag,
                  const Expr::Tag& thermCondTag,
                  const Expr::Tag& pcTag,
                  const Expr::TagList& specTags,
                  const Expr::Tag& densTag,
                  const PCA pca,
                  const int nPCs,
                  const std::string model )
  : Expr::Expression<FluxT>(),
    trans_   ( dynamic_cast<Cantera::MixTransport*>(CanteraObjects::get_transport()) ),
    gas_     ( trans_->thermo() ),
    pca_     ( pca              ),
    nPCs_    ( nPCs             ),
    nspec_   ( gas_.nSpecies()  ),
    model_   ( model            ),
    pressure_( gas_.pressure()  )
{
  Expr::TagList pcTags;
  for( int i=0; i<nPCs_; ++i ){
    std::ostringstream name;
    name << pcTag.name() << "_" << i;
    pcTags.push_back( Expr::Tag(name.str(),pcTag.context()) );
  }

  this->template create_field_vector_request<ScalarT>( specTags, species_ );
  this->template create_field_vector_request<ScalarT>( pcTags,   pcs_     );

  temperature_ = this->template create_field_request<ScalarT>( temperatureTag  );
  density_     = this->template create_field_request<ScalarT>( densTag         );
  heatCap_     = this->template create_field_request<ScalarT>( heatCapacityTag );
  tCond_       = this->template create_field_request<ScalarT>( thermCondTag    );
  
  ptSpec_.resize( nspec_ );
  ptSvDiffCoeff_.resize( nspec_ );
  ptPCGrad_.resize( nPCs_ );
}
//--------------------------------------------------------------------
template< typename FluxT >
SimplePCDiffFlux<FluxT>::~SimplePCDiffFlux()
{
  CanteraObjects::restore_transport( trans_ );
}
//--------------------------------------------------------------------
template< typename FluxT >
void
SimplePCDiffFlux<FluxT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}
//--------------------------------------------------------------------
template< typename FluxT >
void
SimplePCDiffFlux<FluxT>::evaluate()
{
  using namespace SpatialOps;
  SpecFluxT& fluxes = this->get_value_vec();
  
  FluxPtrT tempInterp    = SpatialFieldStore::get<FluxT>( *(fluxes[0]) );
  FluxPtrT densInterp    = SpatialFieldStore::get<FluxT>( *(fluxes[0]) );
  FluxPtrT heatCapInterp = SpatialFieldStore::get<FluxT>( *(fluxes[0]) );
  FluxPtrT tCondInterp   = SpatialFieldStore::get<FluxT>( *(fluxes[0]) );
  
  std::vector<FluxPtrT> yInterp;
  for( size_t i=0; i<nspec_; ++i ){
    yInterp.push_back( SpatialFieldStore::get<FluxT>( *(fluxes[0]) ) );
  }

  // interpolate species mass fractions to faces
  for( size_t i=0; i<species_.size(); ++i ){
    *yInterp[i] <<= (*interpOp_)( species_[i]->field_ref() );
  }
  // interpolate temperature, density, termal-conductivity and heat capacity to faces
  *tempInterp    <<= (*interpOp_)( temperature_->field_ref() );
  *densInterp    <<= (*interpOp_)( density_    ->field_ref() );
  *heatCapInterp <<= (*interpOp_)( heatCap_    ->field_ref() );
  *tCondInterp   <<= (*interpOp_)( tCond_      ->field_ref() );

  // pack a vector with species iterators.
  specIVec_.clear();
  for( size_t i=0; i<yInterp.size(); ++i ){
    specIVec_.push_back( yInterp[i]->interior_begin() );
  }
  
  // calculate the negative of the gradient of each principal component.
  for( size_t i=0; i<pcs_.size(); ++i ){
    *fluxes[i] <<= - (*gradOp_) ( pcs_[i]->field_ref() );
  }

  // pack a vector with PCs flux iterators.
  fluxIVec_.clear();
  for( size_t i=0; i<fluxes.size(); ++i ){
    fluxIVec_.push_back( fluxes[i]->interior_begin() );
    *fluxes[i]->begin() = 0.0;
    *(fluxes[i]->end()-1) = 0.0;
  }

  
  typename       FluxT::const_iterator itemp    = tempInterp->interior_begin();
  const typename FluxT::const_iterator itempe   = tempInterp->interior_end();
  typename       FluxT::const_iterator idens    = densInterp->interior_begin();
  typename       FluxT::const_iterator iheatcap = heatCapInterp->interior_begin();
  typename       FluxT::const_iterator itcond   = tCondInterp->interior_begin();
  
  
  if (model_ == "Weighted") {
    // loop over points to calculate fluxes.
    Matrix svDiffCoeffM;
    Matrix pcDiffCoeffM;
    for( ; itemp!=itempe; ++itemp, ++idens, ++iheatcap, ++itcond ){
      // extract compositions and pack them into the temporary buffer
      std::vector<double>::iterator ipt = ptSpec_.begin();
      for( typename std::vector<typename FluxT::const_iterator>::const_iterator ispec = specIVec_.begin();
          ispec!=specIVec_.end();
          ++ipt, ++ispec )
      {
        *ipt = **ispec;
      }
      
      // extract state variable diffusion coefficients and pack them into the temporary buffer
      try{
        // note that we should really use the spatially varying pressure here...
        gas_.setState_TPY( *itemp, pressure_, &ptSpec_[0] );
        trans_->getMixDiffCoeffsMass(&ptSvDiffCoeff_[0]);
      }
      catch( Cantera::CanteraError ){
        std::cout << "Error in SpeciesDiffFluxFromYs" << std::endl
        << " T=" << *itemp << ", p=" << pressure_ << std::endl
        << " Yi=";
        double ysum=0;
        for( int i=0; i<nspec_; ++i ){
          std::cout << ptSpec_[i] << " ";
          ysum += ptSpec_[i];
        }
        std::cout << std::endl << "sum Yi = " << ysum << std::endl;
        throw std::runtime_error("Problems in SpeciesDiffFluxFromYs expression.");
      }
      // remove the diffusion coefficient of the last species
      ptSvDiffCoeff_.pop_back();
      // multiply the species diffusion coefficients by the density
      for (int i=0; i<nspec_-1; i++) {
        ptSvDiffCoeff_[i] *= *idens;
      }
      // insert the thermal conductivity over the heat capacity in to the beginnig of the diffusion coefficients vector
      ptSvDiffCoeff_.insert (ptSvDiffCoeff_.begin(), *itcond / *iheatcap );
      
      // Transform the state variables diffusion coefficients matrix to the PC diffusion coefficiets matrix using the PCA mapping
      pca_.diffusionMatrixTransform (ptSvDiffCoeff_, pcDiffCoeffM, model_);
      
      // Transform the state variables diffusion coefficients to the PC diffusion coefficiets using the PCA mapping
//      svDiffCoeffM = Matrix(ptSvDiffCoeff_,1,ptSvDiffCoeff_.size());
//      pca_.x2eta(svDiffCoeffM,pcDiffCoeffM, false);
      
      // calculate the PC fluxes and increment their iterators to next mesh point.
      int i=0;
      for( typename std::vector<typename FluxT::iterator>::iterator iflux = fluxIVec_.begin();
          iflux!=fluxIVec_.end();
          ++iflux, i++ )
      {
        **iflux *= pcDiffCoeffM(0,i);
        ++(*iflux);  // increment flux iterator to the next mesh point
      }
      
      // increment iterators for species to the next point
      for( typename std::vector<typename FluxT::const_iterator>::iterator specVecIter=specIVec_.begin();
          specVecIter!=specIVec_.end();
          ++specVecIter )
      {
        ++(*specVecIter);
      }
      
      
    } // loop over grid points.
  }
  else if (model_ == "Analytic") {
    // loop over points to calculate fluxes.
    Matrix pcDiffCoeffM, pcGradM, fluxM;

    for( ; itemp!=itempe; ++itemp, ++idens, ++iheatcap, ++itcond ){
      // extract compositions and pack them into the temporary buffer
      std::vector<double>::iterator ipt = ptSpec_.begin();
      for( typename std::vector<typename FluxT::const_iterator>::const_iterator ispec = specIVec_.begin();
          ispec!=specIVec_.end();
          ++ipt, ++ispec )
      {
        *ipt = **ispec;
      }
      // extract state variable diffusion coefficients and pack them into the temporary buffer
      try{
        // note that we should really use the spatially varying pressure here...
        gas_.setState_TPY( *itemp, pressure_, &ptSpec_[0] );
        trans_->getMixDiffCoeffsMass(&ptSvDiffCoeff_[0]);
      }
      catch( Cantera::CanteraError ){
        std::cout << "Error in SpeciesDiffFluxFromYs" << std::endl
        << " T=" << *itemp << ", p=" << pressure_ << std::endl
        << " Yi=";
        double ysum=0;
        for( int i=0; i<nspec_; ++i ){
          std::cout << ptSpec_[i] << " ";
          ysum += ptSpec_[i];
        }
        std::cout << std::endl << "sum Yi = " << ysum << std::endl;
        throw std::runtime_error("Problems in SpeciesDiffFluxFromYs expression.");
      }
      // Here the diffusion coefficient matrix of the Pcs willl be calculated by transforming the
      // statevariables mixture averaged diffusion coefficients placed in a diagonal matrix. This
      // tranform uses the PCA mapping in the following equation
      //
      //      ⎡ EV1_1 EV1_2 EV1_3 . . . ⎤   ⎡ D_1  0   0  ... ⎤   ⎡ EV1_1 EV2_1 EV3_1 . . . ⎤      ⎡ Dpc_11 Dpc_12 Dpc_13  . . . ⎤
      //      ⎜ EV2_1 EV2_2 EV2_3 . . . ⎥   ⎜  0  D_2  0  ... ⎥   ⎜ EV1_2 EV2_2 EV3_2 . . . ⎥      ⎜ Dpc_21 Dpc_22 Dpc_23  . . . ⎥
      //      ⎜ EV3_1 EV3_2 EV3_3 . . . ⎥ * ⎜  0   0  D_3 ... ⎥ * ⎜ EV1_3 EV2_3 EV3_3 . . . ⎥  --  ⎜ Dpc_31 Dpc_32 Dpc_33  . . . ⎥
      //      ⎜   .     .               ⎥   ⎜  .   .          ⎥   ⎜   .     .               ⎥  --  ⎜   .      .                  ⎥
      //      ⎜   .           .         ⎥   ⎜  .       .      ⎥   ⎜   .           .         ⎥      ⎜   .             .           ⎥
      //      ⎣   .                 .   ⎦   ⎣  .           .  ⎦   ⎣   .                 .   ⎦      ⎣   .                     .   ⎦
      //
      // Note that we should take the scalings into account in the actual calculations.
      //
      //          T      -1
      // [eig_vec]  *  [S]  *  [D_sv]  * [S] * [eig_vec]  =  [D_pc]
      //
      //             -1                                         -1
      // But since [S] , [D_sv] and [S] are diagonal matrices [S] and [S] will cancell each other out.
      // Only the first nPCs_ rows and column of the result is needed for the calculation.
      // Therefore, the reduced eigen vector matrices can be used.
      
      // remove the diffusion coefficient of the last species
      ptSvDiffCoeff_.pop_back();
      // multiply the species diffusion coefficients by the density
      for (int i=0; i<nspec_-1; i++) {
        ptSvDiffCoeff_[i] *= *idens;
      }
      // insert the thermal conductivity over the heat capacity in to the beginnig of the diffusion coefficients vector
      ptSvDiffCoeff_.insert (ptSvDiffCoeff_.begin(), *itcond / *iheatcap );
      
      // Transform the state variables diffusion coefficients matrix to the PC diffusion coefficiets matrix using the PCA mapping
      pca_.diffusionMatrixTransform (ptSvDiffCoeff_, pcDiffCoeffM, model_);
      
      // extract pc gradients and pack them into the temporary buffer
      std::vector<double>::iterator iptpcg = ptPCGrad_.begin();
      for( typename std::vector<typename FluxT::iterator>::iterator ipcgrad = fluxIVec_.begin();
          ipcgrad!=fluxIVec_.end();
          ++iptpcg, ++ipcgrad )
      {
        *iptpcg = **ipcgrad;
      }
      
      // calculate the PC fluxes
      pcGradM = Matrix(ptPCGrad_,nPCs_,1);
      fluxM = pcDiffCoeffM * pcGradM;
      
      // calculate the PC fluxes and increment their iterators to next mesh point.
      int i=0;
      for( typename std::vector<typename FluxT::iterator>::iterator iflux = fluxIVec_.begin();
          iflux!=fluxIVec_.end();
          ++iflux, i++ )
      {
        **iflux = fluxM(i,0);
        ++(*iflux);  // increment flux iterator to the next mesh point
      }
      
      // increment iterators for species to the next point
      for( typename std::vector<typename FluxT::const_iterator>::iterator specVecIter=specIVec_.begin();
          specVecIter!=specIVec_.end();
          ++specVecIter )
      {
        ++(*specVecIter);
      }
      
      
    } // loop over grid points.
    
  }
}
//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------
template< typename FluxT >
SimplePCDiffFlux<FluxT>::Builder::
Builder( const Expr::TagList& result,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& heatCapacityTag,
         const Expr::Tag& thermCondTag,
         const Expr::Tag& pcTag,
         const Expr::TagList& speciesTag,
         const Expr::Tag& densTag,
         const PCA pca,
         const int nPCs,
         const std::string model )
  : ExpressionBuilder(result),
    pca_     ( pca             ),
    nPCs_    ( nPCs            ),
    mod_     ( model           ),
    specT_   ( speciesTag      ),
    pcT_     ( pcTag           ),
    densT_   ( densTag         ),
    tT_      ( temperatureTag  ),
    heatCapT_( heatCapacityTag ),
    tcT_     ( thermCondTag    )
{}
//--------------------------------------------------------------------
template< typename FluxT >
Expr::ExpressionBase*
SimplePCDiffFlux<FluxT>::Builder::build() const
{
  return new SimplePCDiffFlux<FluxT>( tT_, heatCapT_, tcT_, pcT_, specT_, densT_, pca_, nPCs_, mod_ );
}
//--------------------------------------------------------------------

#endif
