/*
 *  PCA.cpp
 *  testODT
 *
 *  Created by amir on 8/27/13.
 *  Copyright (c) 2013 abiglari. All rights reserved.
 *
 */
//#include <expression/ExprLib.h>
//#include <FileWriter.h>
//#include <spatialops/SpatialOpsConfigure.h>
//#include <spatialops/structured/FieldHelper.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include "gas/pca/PCA.h"
#include "assert.h"

using namespace std;

PCA::PCA (const string pcaFile, const int nEta)
: nEta_ ( nEta )
{
  readFromFile (pcaFile);
  
  // PCA mapping of the centering vector, needed for triplet mapping
  Matrix centeringM = Matrix(centers_,1,(centers_).size());
  Matrix mappedCentersM;
  x2eta(centeringM,mappedCentersM,false);
  
  mappedCenters_.resize(nEta_);
  eigVecSums_.resize(nEta);
  for (int i=0; i<nEta_; i++) {
    mappedCenters_[i] = mappedCentersM(0,i);
    eigVecSums_[i]=0.0;
    for (int j=0; j<centers_.size(); j++) {
//      eigVecSums_[i] += abs(eigenvectors_(j,i));
      eigVecSums_[i] += eigenvectors_(j,i);      // still not sure to use this or the ABS form
    }
  }

  vector<double> tempEigV;
  tempEigV.resize(nEta_ * centers_.size() );
  for (int i=0; i<nEta_; i++) {
    for (int j=0; j<centers_.size(); j++) {
//      tempEigV[j*nEta_+i] = abs(eigenvectors_(j,i)) / eigVecSums_[i];
      tempEigV[j*nEta_+i] = eigenvectors_(j,i) / eigVecSums_[i];      // still not sure to use this or the ABS form
    }
  }
  normalEigVecs_ = Matrix(tempEigV,centers_.size(),nEta_);
}

PCA::PCA()
: nEta_(0)
{}

PCA::~PCA()
{}

void PCA::readFromFile (const string pcaFile)
{
  ifstream pcaFStream;
  pcaFStream.open(pcaFile.c_str());

  string line, field;
  vector<double> v;
  v.clear();
  double num;

  if( !pcaFStream ){                                 // file couldn't be opened
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "PCA data file could not be opened";
    throw runtime_error(errMsg.str());
  }
  else
    cout << "PCA data file SUCCESSFULLY opened!\n";

  // extracting the eigen vector values
  getline(pcaFStream,line);                         // get the first line in file
  if( line!="Eigenvectors:" ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Eigenvectors could not be found in its place in the PCA data file";
    throw runtime_error(errMsg.str());
  }
  int nspecies=0;
  getline(pcaFStream,line);
  while ( line != "" ){                             // get next line in file and reading the eigenvectors
    nspecies++;
    stringstream ss(line);
    int count = 0;
    while (getline(ss,field,',') && (count<nEta_) ){ // break line into comma delimitted fields
      count++;
      stringstream ss2(field);
      ss2 >> num;
      v.push_back(num);                             // add the next member of the eigenvector matrix to a 1D array
    }
    getline(pcaFStream,line);
  }
  eigenvectors_ = Matrix(v,nspecies,nEta_);
  transEigenvectors_ = eigenvectors_.trans();
  // extracting the centering factors
  getline(pcaFStream,line);                         // get the next line in file
  if( line!="Centering Factors:" ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Centering Factors could not be found in its place in the PCA data file";
    throw runtime_error(errMsg.str());
  }
  centers_.clear();
  getline(pcaFStream,line);
  stringstream ss(line);
  while (getline(ss,field,',') ){                    // break line into comma delimitted fields
    stringstream ss2(field);
    ss2 >> num;
    centers_.push_back(num);                               // add the next member of the centering factors to a 1D array
  }

  getline(pcaFStream,line);                         // get the next line in file
  // extracting the scaling factors
  getline(pcaFStream,line);                         // get the next line in file
  if( line!="Scaling Factors:" ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Scaling Factors could not be found in its place in the PCA data file";
    throw runtime_error(errMsg.str());
  }
  scales_.clear();
  getline(pcaFStream,line);
  stringstream ss3(line);
  while (getline(ss3,field,',') ){                    // break line into comma delimitted fields
    stringstream ss2(field);
    ss2 >> num;
    scales_.push_back(num);                               // add the next member of the scaling factors to a 1D array
  }

  pcaFStream.close();
}

void PCA::x2eta(const Matrix& X, Matrix& eta, bool center) const
{
  if( eigenvectors_.row_ != X.col_ ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "PCA mapping can not be used on this dataset. The number of original variables in the PCA mapping is not equal to the number of variables in the input dataset";
    throw runtime_error(errMsg.str());
  }
  vector<double> tempM(X.row_ * X.col_, 0.0 );
  // preprocessing the dataset
  for( int i=0; i<X.row_; i++ ){
    for( int j=0; j<X.col_; j++ ){
      if( center ){
        tempM[i*X.col_+j] = (X(i,j) - centers_[j]) / scales_[j];
      }
      else{
        tempM[i*X.col_+j] = X(i,j) / scales_[j];
      }
    }
  }
  // jcs this is slow since it forces construction of a new Matrix.
  Matrix modX(tempM,X.row_,X.col_);
  eta = modX * eigenvectors_;

}

void PCA::eta2x(const Matrix& eta, Matrix& X, bool center) const
{
  if( nEta_ != eta.col_ ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "PCA mapping can not be used on this dataset. Number of PC's are not consistent.";
    throw runtime_error(errMsg.str());
  }
  X = eta * transEigenvectors_;
  // post-processing the dataset
  for( int i=0; i<X.row_; i++ ){
    for( int j=0; j<X.col_; j++ ){
      if( !center ){
        X.matrix_[i*X.col_+j] *= scales_[j];
      }
      else{
        X.matrix_[i*X.col_+j] = (( X(i,j) * scales_[j]) + centers_[j]);
      }
    }
  }

}

void PCA::display(ostream& outputStrm) const
{
  cout << "PCA object:\n"<<endl;
  cout << "Eigenvectors:"<<endl;
  eigenvectors_.display(outputStrm);
  cout << "\nCentering Factors:"<<endl;
  for (int i=0; i < centers_.size(); i++)
    cout << setprecision(12) << centers_[i] <<",\t";
  cout << endl;
  cout << "\nScaling Factors:"<<endl;
  for( int i=0; i<scales_.size(); i++ )
    cout << setprecision(12) << scales_[i] <<",\t";
  cout << endl;
}

// This method is written for the exclusive use of the PCDiffusion expression in the ODT code.
// Here the diffusion coefficient matrix of the PCs willl be calculated by transforming the
// state-variables' mixture averaged diffusion coefficients placed in a diagonal matrix. This
// tranform uses the PCA eigenvectors in the following equation for the Analytic mode:
//
// ⎡ EV1_1 EV1_2 EV1_3 . . . ⎤   ⎡ D_1  0   0  ... ⎤   ⎡ EV1_1 EV2_1 EV3_1 . . . ⎤      ⎡ Dpc_11 Dpc_12 Dpc_13  . . . ⎤
// ⎜ EV2_1 EV2_2 EV2_3 . . . ⎥   ⎜  0  D_2  0  ... ⎥   ⎜ EV1_2 EV2_2 EV3_2 . . . ⎥      ⎜ Dpc_21 Dpc_22 Dpc_23  . . . ⎥
// ⎜ EV3_1 EV3_2 EV3_3 . . . ⎥ * ⎜  0   0  D_3 ... ⎥ * ⎜ EV1_3 EV2_3 EV3_3 . . . ⎥  --  ⎜ Dpc_31 Dpc_32 Dpc_33  . . . ⎥
// ⎜   .     .               ⎥   ⎜  .   .          ⎥   ⎜   .     .               ⎥  --  ⎜   .      .                  ⎥
// ⎜   .           .         ⎥   ⎜  .       .      ⎥   ⎜   .           .         ⎥      ⎜   .             .           ⎥
// ⎣   .                 .   ⎦   ⎣  .           .  ⎦   ⎣   .                 .   ⎦      ⎣   .                     .   ⎦
//
// Note that we should take the scalings into account in the actual calculations.
//
//          T      -1
// [eig_vec]  *  [S]  *  [D_sv]  * [S] * [eig_vec]  =  [D_pc]
//
//             -1                                         -1
// But since [S] , [D_sv] and [S] are diagonal matrices [S] and [S] will cancell each other out.
// Only the first nPCs_ rows and column of the result is needed for the calculation.
// Therefore, the reduced eigen vector matrices can be used.
//
// And for the Weighted mode it uses this equation:
//
//  EV1⎡ EV1_1/sum(EV1) EV1_2/sum(EV1) EV1_3/sum(EV1) . . . ⎤   ⎡ D_1 ⎤      ⎡ Dpc_1 ⎤
//  EV2⎜ EV2_1/sum(EV2) EV2_2/sum(EV2) EV2_3/sum(EV2) . . . ⎥   ⎜ D_2 ⎥      ⎜ Dpc_2 ⎥
//  EV3⎜ EV3_1/sum(EV3) EV3_2/sum(EV3) EV3_3/sum(EV3) . . . ⎥ * ⎜ D_3 ⎥  --  ⎜ Dpc_3 ⎥
//   . ⎜   .                  .                             ⎥   ⎜  .  ⎥  --  ⎜   .   ⎥
//   . ⎜   .                                 .              ⎥   ⎜  .  ⎥      ⎜   .   ⎥
//   . ⎣   .                                            .   ⎦   ⎣  .  ⎦      ⎣   .   ⎦
//
// where it actually calculates a weight average of the state variable diffusion coefficients based
// on their weight in each eigen vector to calculate the corresponding PC mixture avergaed difusion coefficient.
// coefficient.

void PCA::diffusionMatrixTransform (const std::vector<double>& Dsv, Matrix& Dpc, string model) const
{
  int nSV = Dsv.size();
  if( eigenvectors_.row_ != nSV ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "PCA can not transform these dissuision coefficients. The number of original variables in the PCA mapping is not equal to the number of variables in the input dataset";
    throw runtime_error(errMsg.str());
  }
  if (model == "Analytic") {
  vector<double> tempV;
  tempV.resize(nEta_ * nSV);
  for (int i=0; i<nEta_; i++){
    for (int j=0; j<nSV; j++) {
      tempV[i*nSV+j] = transEigenvectors_(i,j) * Dsv[j];
    }
  }
  Matrix tempM(tempV,nEta_,nSV);
  Dpc = tempM * eigenvectors_;
  }
  else if (model == "Weighted") {
    Matrix DsvM(Dsv, 1, nSV);
    Dpc = DsvM * normalEigVecs_;
  }
}

// This method is written for the exclusive use of the PCtriplet mapping algorithm in the ODT code
// and it is not a required method for the PCA class by its own.
void PCA::PCTripletTerm( VolField& tripletTerm, const VolField& rhoPC, const VolField& density, const int pcNum ) const
{
//  VolField::iterator itrit = tripletTerm.begin();
//  VolField::const_iterator irpc  = rhoPC.begin();
//  VolField::const_iterator irho  = density.begin();
//  const VolField::const_iterator irhoe = rhoPC.end();

  tripletTerm <<= rhoPC + density * mappedCenters_[pcNum];
  // loop over each grid point and calculate the reaction rate.
//  for( ; irho!=irhoe; ++irpc, ++irho, ++itrit ){
//    *itrit = *irpc + *irho * mappedCenters_[pcNum];
//  }
}

// This method is written for the exclusive use of the PCtriplet mapping algorithm in the ODT code
// and it is not a required method for the PCA class by its own.
void PCA::RhoPCFromTripletTerm( VolField& rhoPC, const VolField& tripletTerm, const VolField& density, const int pcNum ) const
{
//  VolField::iterator irpc  = rhoPC.begin();
//  VolField::const_iterator itrit = tripletTerm.begin();
//  VolField::const_iterator irho  = density.begin();
//  const VolField::const_iterator irhoe = rhoPC.end();
  
  // loop over each grid point and calculate the reaction rate.
//  std::ofstream testfile;
//  std::ostringstream name;
//  name << "./test_file_" << pcNum <<".txt";
//  testfile.open(name.str());
//  print_field( tripletTerm, testfile );

   rhoPC <<= tripletTerm - density * mappedCenters_[pcNum];
//  print_field( tripletTerm, testfile );
//  testfile.close();
//  for( ; irho!=irhoe; ++irpc, ++irho, ++itrit, i++ ){
//    *irpc = *itrit - *irho * mappedCenters_[pcNum];
//  }

}

