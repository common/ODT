#ifndef PCEquation_h
#define PCEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types
#include "gas/pca/PCA.h"
#include "gas/pca/MarsGroup.h"
#include "gas/pca/Map2GridVec.h"


/**
 *  @class PCEquation
 *  @author Amir Biglari
 *  @date July, 2013
 *
 *  @brief Defines a principle component transport equation, along with the
 *         expressions required by it.
 *
 *  @todo Need a methodology for applying PCA and MARS process
 */
class PCEquation : public Expr::TransportEquation
{
public:

  PCEquation( Expr::ExpressionFactory& exprFactory,
              const bool doAdvection,
              const int pcNum,
              const int numPCs,
              const int nSpecies,
              const PCA& pca,
              const MarsGroup& pcSrcmg,
              const MarsGroup& stVarMG,
              const Map2GridVec& m2gVec,
              const Expr::Tag SrcTag,
              const bool modelSrcTerms,
              const bool modelStateVars,
              const bool mapSrcTerms,
              const bool includeExTempTerm,
              const bool calculateTempSrc,
              const bool standAlonePCSolver,
              const std::string pcDiffFluxModel,
              std::vector<Expr::ExpressionID>& pcIDs,
              Expr::ExpressionID& stVarsID,
              Expr::ExpressionID& tempSrcID );

  ~PCEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  static Expr::Tag get_rhs_tag( const int pcNum );

  static std::string get_soln_var_name( const int pcNum );

  // disallow copy and assignment.
  PCEquation( const PCEquation& );
  PCEquation operator=( const PCEquation& );  

  const int pcNum_, nPCs_, nSpecies_;
  
  const PCA pca_;
  const MarsGroup pcSrcMG_, stVarMG_;
  const Map2GridVec m2gVec_;
  const bool standAlonePCSolver_;
};


#endif // PCEquation_h
