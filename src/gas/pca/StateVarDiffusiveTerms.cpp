#include <gas/pca/StateVarDiffusiveTerms.h>

//--------------------------------------------------------------------
template< typename DivT >
StateVarDiffusiveTerms<DivT>::
StateVarDiffusiveTerms( const Expr::Tag heatFluxTag,
                        const Expr::TagList& specFluxTagList,
                        const Expr::TagList& spcEnthalpyTagList,
                        const Expr::Tag heatCapacityTag,
                        const int nspecies )
: Expr::Expression<FieldT>(),
  nspec_( nspecies )
{
  heatCap_  = this->template create_field_request<FieldT>( heatCapacityTag );
  heatFlux_ = this->template create_field_request< FluxT>( heatFluxTag     );
  
  this->template create_field_vector_request<FluxT >( specFluxTagList,    spcFluxVec_ );
  this->template create_field_vector_request<FieldT>( spcEnthalpyTagList, spcEnthVec_ );
}
//--------------------------------------------------------------------
template< typename DivT >
void
StateVarDiffusiveTerms<DivT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivT>();
}
//--------------------------------------------------------------------
template< typename DivT >
void
StateVarDiffusiveTerms<DivT>::evaluate()
{
  using namespace SpatialOps;
  typedef typename Expr::Expression<FieldT>::ValVec ResFieldVec;
  ResFieldVec& svDiffTerms = this->get_value_vec();

  const FieldT& heatCap  = heatCap_ ->field_ref();
  const FluxT&  heatFlux = heatFlux_->field_ref();

  typename ResFieldVec::iterator isvDiffTerms = svDiffTerms.begin();

  // Here we calculate the diffusive term for the temperature equation
  **isvDiffTerms <<= - (*divOp_) (heatFlux) ;
  
  for( size_t i=0; i<nspec_; ++i ){
    const FieldT& spEnth = spcEnthVec_[i]->field_ref();
    const FluxT&  spFlux = spcFluxVec_[i]->field_ref();
    **isvDiffTerms <<= **isvDiffTerms + spEnth * (*divOp_) ( spFlux );
  }
  
  **isvDiffTerms <<= **isvDiffTerms / heatCap;

  ++isvDiffTerms;
  
  for( size_t i=0; i<spcFluxVec_.size(); ++i ){
    const FluxT& spFlux = spcFluxVec_[i]->field_ref();
    **isvDiffTerms <<= - (*divOp_)( spFlux );
  }  // This loop calculates the diffusive terms of all the species
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
template< typename DivT >
StateVarDiffusiveTerms<DivT>::Builder::
Builder( const Expr::TagList& result,
         const Expr::Tag heatFluxTag,
         const Expr::TagList& specFluxTagList,
         const Expr::TagList& spcEnthalpyTagList,
         const Expr::Tag heatCapacityTag,
         const int nspecies )
: ExpressionBuilder(result),
  nspec_     ( nspecies           ),
  heatFluxT_ ( heatFluxTag        ),
  spcFluxTs_ ( specFluxTagList    ),
  spcEntpyTs_( spcEnthalpyTagList ),
  heatCapT_  ( heatCapacityTag    )
{}
//--------------------------------------------------------------------
template< typename DivT >
Expr::ExpressionBase*
StateVarDiffusiveTerms<DivT>::Builder::build() const
{
  return new StateVarDiffusiveTerms<DivT>( heatFluxT_, spcFluxTs_, spcEntpyTs_, heatCapT_, nspec_ );
}
//--------------------------------------------------------------------


template class StateVarDiffusiveTerms< DivOp >;

