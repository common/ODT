//
//  mars.cpp
//  testODT1
//
//  Created by abiglari on 9/4/13.
//  Copyright (c) 2013 abiglari. All rights reserved.
//

#include <iostream>
#include <sstream>
#include <stdexcept>
#include "gas/pca/Mars.h"

using namespace std;

Mars::Mars (ifstream& mFStream)
{
    readFromFile (mFStream);
}

Mars::Mars(const string marsFile)
{
    ifstream mFStream;
    mFStream.open(marsFile.c_str());
    if(!mFStream)                                   // file couldn't be opened
    {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Mars data file could not be opened";
        throw runtime_error(errMsg.str());
    }
    else
        cout << "Mars data file SUCCESSFULLY opened!\n";
    
    readFromFile (mFStream);
    mFStream.close();
}

void Mars::readFromFile (ifstream& mFStream)
{
    string line, field;
    double num;
    getline(mFStream,line);
    if (line.length()<20 || line.substr(0,20)!="mars properties for ")
    {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "This is probably not a Mars data file. Each mars object block should start with 'mars properties for dvar:'";
        throw runtime_error(errMsg.str());
    }
    
    getline(mFStream, line);
    stringstream ss(line);
    getline(ss, field, ':');
    if (field!="nivar")
    {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "nivars could not be found in its place in the Mars block";
        throw runtime_error(errMsg.str());
    }
    getline(ss, field, ':');
    stringstream ss2(field);
    ss2 >> nivar_;
    
    getline(mFStream,line);
    if (line!="coefs:")
    {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "coefs could not be found in its place in the Mars block";
        throw runtime_error(errMsg.str());
    }
    
    coefs_.clear();
    getline(mFStream,line);
    stringstream ss3(line);
    while (getline(ss3,field,',') )                    // break line into comma delimitted fields
    {
        stringstream ss2(field);
        ss2 >> num;
        coefs_.push_back(num);                        // add the next member of the coeficients
    }
    getline(mFStream, line);
    
    bool finish=false;
    do {
        basis_.push_back(BasisFunction(mFStream));
        getline(mFStream, line);
        getline(mFStream, line);
        if (line=="" || line.substr(0,20)=="mars properties for ")
            finish=true;
        else
            mFStream.seekg(-(line.length()+1),mFStream.cur);
    } while (!finish);
    
    if (line.substr(0,20)=="mars properties for ") {
        mFStream.seekg(-(line.length()+1),mFStream.cur);
    }
}

Mars::~Mars()
{}

void Mars::display(ostream& outputStrm) const
{
    cout << "coefs:\n";
    for(int i=0; i<coefs_.size(); i++)
    {
        cout << coefs_[i] << " ";
    }
    cout << "\n" << endl;
    
    for (int i=0; i<basis_.size(); i++ )
    {
        cout << "basis " << i+1 << ":" << endl;
        (basis_[i]).display(outputStrm);
        cout << endl;
    }
}

double Mars::operator() (const vector<double>& iVars) const
{
    if (iVars.size()!=nivar_) {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "inconsistent number of indpendent variables";
        throw runtime_error(errMsg.str());
    }
        double dVar;
        dVar = coefs_[0] * basis_[0](iVars);
        int nb = (int)basis_.size();
        for (int i=1; i<nb ; i++)
            dVar += coefs_[i] * basis_[i](iVars);
    
        return dVar;
//    return (evaluate(iVars));
}

//double Mars::evaluate (const vector<double>& iVars) const
//{
//    double dVar;
//    dVar = coefs_[0] * basis_[0](iVars);
//    int nb = (int)basis_.size();
//    for (int i=1; i<nb ; i++)
//        dVar += coefs_[i] * basis_[i](iVars);
//    
//    return dVar;
//
//}

