//
//  BasisFunction.h
//  testODT1
//
//  Created by abiglari on 9/4/13.
//  Copyright (c) 2013 abiglari. All rights reserved.
//

#ifndef BASIS_FUNCTION_H
#define BASIS_FUNCTION_H

#include <iosfwd>
#include <fstream>
#include <boost/shared_ptr.hpp>
#include <vector>


class BasisFunction {
  int dim_,sense_,order_;
  double xknot_;
  bool haveParent_;
  typedef boost::shared_ptr<BasisFunction> BasisFnPtr;
  BasisFnPtr parent_;
  void readFromFile( std::ifstream& bfFStream );
public:
  BasisFunction( const std::string basisFile );
  BasisFunction();
  BasisFunction( std::ifstream& bfFStream );
  BasisFunction( const std::vector<int>& dims,
                 const std::vector<int>& senses,
                 const std::vector<double>& xknots,
                 const std::vector<int>& orders,
                 int i );
  ~BasisFunction();
  void display(std::ostream& outputStrm) const;
  double operator() (const std::vector<double>& iVars) const;
  
};
#endif /* defined(BASIS_FUNCTION_H) */
