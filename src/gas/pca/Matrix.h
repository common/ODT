/*
 *  Matrix.h
 *  testODT
 *
 *  Created by amir on 8/27/13.
 *  Copyright (c) 2013 abiglari. All rights reserved.
 *
 */

#include <iostream>
#include <vector>

#ifndef MATRIX_H
#define MATRIX_H

class Matrix{
//  \todo col_ and row_ should be private.
public:
  std::vector<double> matrix_;
  int col_,row_;
  Matrix (const std::vector<double>& array, const int row, const int col);
  Matrix ();
  ~Matrix();
  Matrix trans() const;
  Matrix operator+ (const Matrix& M) const;
  Matrix operator* (const Matrix& M) const;
  double operator() (const unsigned i, const unsigned j) const;
  void display(std::ostream& outputStrm) const;
};

#endif
