//
//  Map2Grid.cpp
//  M2G_test
//
//  Created by abiglari on 6/26/14.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include "assert.h"
#include "gas/pca/Map2Grid.h"

using namespace std;

Map2Grid::Map2Grid( const std::vector<double>& gridLims,
                    const std::vector<double>& gridVals,
                    const int ndims,
                    const int nPts )
: ndims_ ( ndims ),
  nPts_(nPts),
  gridLims_(gridLims),
  gridVals_(gridVals)
{}

Map2Grid::Map2Grid( const string gridFile,
                    const int ndims )
: ndims_ ( ndims )
{
  if( ndims_>3 ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "More the 3 dimensions is not supported in the Map2Grid class" ;
    throw runtime_error(errMsg.str());
  }
  readFromFile( gridFile );
}

Map2Grid::~Map2Grid()
{}

void Map2Grid::readFromFile( const string gridFile )
{
  ifstream gridFStream;
  gridFStream.open(gridFile.c_str());
  
  if( !gridFStream ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << gridFile << " data file could not be opened";
    throw runtime_error(errMsg.str());
  }
  else{
    cout << gridFile << " data file SUCCESSFULLY opened!\n";
  }
  
  string line, field;
  gridLims_.clear();
  double num;

  for( int i=0; i<ndims_; i++ ){   // loop over dimensions of the grid limits
    getline(gridFStream,line);
    stringstream ss(line);
    while( getline(ss,field,',') ){ // break line into comma delimitted fields
      stringstream ss2(field);
      ss2 >> num;
      gridLims_.push_back(num);    // add the next grid limit value to a 1D array
    }
  }
  nPts_ = (gridLims_.size()/ndims_) - 1;
  
  int gridSize=1;
  for( int i=0; i<ndims_; i++ ){
    gridSize *= nPts_;
  }
  gridVals_.resize(gridSize);

  int vecIndex=0;
  getline(gridFStream,line);
  while( line != "" ){    // get next line in file and reading each line of the grid
    stringstream ss(line);
    int count = 0;
    while (getline(ss,field,',') && (count<nPts_) ){ // break line into comma delimitted fields
      count++;
      stringstream ss2(field);
      ss2 >> num;
      gridVals_[vecIndex++]=num;   // add the next grid value to a 1D array
    }
    getline(gridFStream,line);
  }
  
  gridFStream.close();
}

double Map2Grid::evaluate( const std::vector<double>& ivar ) const
{
  if( ndims_!=ivar.size() ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Dimensions of the dependent variable is not compatible with this Map2Grid class" ;
    throw runtime_error(errMsg.str());
  }
  vector<int> index;
  index.resize(ndims_);
  
  for( int i=0; i<ndims_; i++ ){
    for( int j=0; j<nPts_; j++ ){
      if( (ivar[i]>=gridLims_[i*(nPts_+1)+j]) && (ivar[i]<gridLims_[i*(nPts_+1)+j+1]) ){
        index[i]=j;
        break;
      }
    }
  }

  int indRef = index[0] + (ndims_==3? index[1]*nPts_ + index[2] * nPts_ * nPts_ : (ndims_==2?index[1]*nPts_:0));
  return (gridVals_[indRef]);
}

void Map2Grid::display(ostream& outputStrm) const
{
  outputStrm << "Grid limits:\n"<<endl;

  for( int i=0; i<ndims_; i++ ){
    outputStrm << "Dimension "<< i <<":"<<endl;
    for( int j=0; j<=nPts_; j++ ){
      outputStrm << gridLims_[j+(nPts_+1)*i] << ", ";
    }
    outputStrm << "\n" << endl;
  }

  outputStrm << "Grid Values:\n"<<endl;

  for( int k=0; k<(ndims_==3?nPts_:1); k++ ){
    if( ndims_==3 ) outputStrm << "Values in (:,:,"<< k <<"):"<<endl;
    for( int i=0; i<nPts_; i++ ){
      for( int j=0; j<nPts_; j++ ){
        outputStrm << gridVals_[i*(nPts_) + j + k*(nPts_*nPts_)] << ", ";
      }
      outputStrm << endl;
    }
  }
}

