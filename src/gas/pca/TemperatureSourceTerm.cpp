#include <gas/pca/TemperatureSourceTerm.h>

//--------------------------------------------------------------------
TemperatureSourceTerm::
TemperatureSourceTerm( const Expr::TagList& svSourceTermTagList,
                       const Expr::TagList& svEnthalpyTagList,
                       const Expr::Tag& heatCapacityTag )
: Expr::Expression<VolField>()
{
  heatCap_ = this->create_field_request<VolField>( heatCapacityTag );
  
  this->create_field_vector_request<VolField>( svSourceTermTagList, svSrcTerms_   );
  this->create_field_vector_request<VolField>( svEnthalpyTagList,   svEnthalpies_ );
}
//--------------------------------------------------------------------
void
TemperatureSourceTerm::evaluate()
{
  using namespace SpatialOps;
  VolField& tempSrc = this->value();
  
  VolField::iterator itempSrc = tempSrc.begin();
  const VolField::iterator itempSrce = tempSrc.end();
  VolField::const_iterator iheatCap = heatCap_->field_ref().begin();

  // pack a vector with state variable source term iterators.
  svSrcTermIVec_.clear();
  for( size_t i=0; i<svSrcTerms_.size(); ++i ){
    svSrcTermIVec_.push_back( svSrcTerms_[i]->field_ref().begin() );
  }
  
  // pack a vector with state variable enthalpy iterators.
  svEnthalpyIVec_.clear();
  for( size_t i=0; i<svEnthalpies_.size(); ++i ){
    svEnthalpyIVec_.push_back( svEnthalpies_[i]->field_ref().begin() );
  }

  // calculate temperature source term at each point
  for( ; itempSrc!=itempSrce; ++itempSrc, ++iheatCap ){
    
    *itempSrc =0.0;
    std::vector<VolField::const_iterator>::iterator istateVarEntpy = svEnthalpyIVec_.begin();
    for( std::vector<VolField::const_iterator>::iterator istateVarSrc = svSrcTermIVec_.begin();
        istateVarSrc!=svSrcTermIVec_.end();
        ++istateVarSrc, ++istateVarEntpy )
    {
      *itempSrc += (**istateVarEntpy) * (**istateVarSrc);
      ++(*istateVarEntpy); // increment iterators for species enthalpy to the next point
      ++(*istateVarSrc);   // increment iterators for species source term to the next point
    }
    
    *itempSrc = - *itempSrc / *iheatCap;
    
  }
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
TemperatureSourceTerm::Builder::
Builder( const Expr::Tag result,
         const Expr::TagList& svSourceTermTagList,
         const Expr::TagList& svEnthalpyTagList,
         const Expr::Tag& heatCapacityTag  )
: ExpressionBuilder(result),
  svSrcTermTs_( svSourceTermTagList),
  svEntpyTs_  ( svEnthalpyTagList  ),
  heatCapT_   ( heatCapacityTag       )
{}
//--------------------------------------------------------------------
Expr::ExpressionBase*
TemperatureSourceTerm::Builder::build() const
{
  return new TemperatureSourceTerm( svSrcTermTs_, svEntpyTs_, heatCapT_ );
}
//--------------------------------------------------------------------
