//
//  Map2GridVec.h
//  M2G_test
//
//  Created by abiglari on 6/30/14.
//

#ifndef Map2GridVec_h
#define Map2GridVec_h

#include <iosfwd>

#include "gas/pca/Map2Grid.h"

#include <boost/shared_ptr.hpp>

/**
 * \class Map2GridVec
 * \brief Provides storage for a basic tabulated function on a cartesian mesh
 */
class Map2GridVec  {
  boost::shared_ptr< std::vector<Map2Grid> > gVec_;  // shared pointer prevents memory bloat when copying
  std::vector<std::string> iVarNames_;
  std::vector<std::string> dVarNames_;
  int ndims_;
  int ndvar_;
  int nPts_;
  
public:

  /**
   * @brief Load data from disk to create this object
   * @param gridVecFile the name of the file
   * @param ndims the number of independent variables
   */
  Map2GridVec( const std::string gridVecFile, const int ndims );

  ~Map2GridVec();

  void display( std::ostream& outputStrm ) const;

  const Map2Grid& operator[] (const std::string dVarName) const;

  inline const std::vector<std::string>& get_dvar_names() const{ return dVarNames_; }
  inline const std::vector<std::string>& get_ivar_names() const{ return iVarNames_; }
};

#endif
