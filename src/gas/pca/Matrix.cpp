/*
 *  Matrix.cpp
 *  testODT
 *
 *  Created by amir on 8/27/13.
 *  Copyright (c) 2013 abiglari. All rights reserved.
 *
 */

#include <sstream>
#include <stdexcept>
#include "gas/pca/Matrix.h"

using namespace std;

Matrix::Matrix( const vector<double>& array, const int row, const int col )
: matrix_( array ),
  row_   ( row   ),
  col_   ( col   )
{
  if( matrix_.size()!=row_*col_ ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Matrix dimensions is not agree with the assigned array size";
    throw runtime_error(errMsg.str());
  }
}

Matrix::Matrix()
{}

Matrix::~Matrix()
{}

Matrix
Matrix::trans() const
{
  vector<double> transM;
  for (int i=0; i<col_; i++) {
    for (int j=0; j<row_; j++) {
      transM.push_back(matrix_[j*col_+i]);
    }
  }
  return Matrix(transM,col_,row_);
}

Matrix
Matrix::operator+ ( const Matrix& M ) const
{
  if (row_!=M.row_ || col_!=M.col_) {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Matrix dimensions do not agree for summation";
    throw runtime_error(errMsg.str());
  }
  vector<double> sum;
  sum.resize(row_*col_);

  for( int i=0; i<row_; i++ ){
    for( int j=0; j<col_; j++ ){
      sum[i*col_+j] = matrix_[i*col_+j] + M(i,j);
    }
  }
  return Matrix(sum, row_, col_);
}

Matrix
Matrix::operator* ( const Matrix& M ) const
{
  if( col_!=M.row_ ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Matrix dimensions do not agree for multiplication";
    throw runtime_error(errMsg.str());
  }
  vector<double> prod;
  prod.resize(row_ * M.col_);

  for( int i=0; i<row_; i++ ){
    for( int j=0; j<M.col_; j++ ){
      for( int k=0; k<col_; k++ ){
        prod[i*M.col_+j] += matrix_[i*col_+k] * M(k,j);
      }
    }
  }
  return Matrix(prod, row_, M.col_);
}

double
Matrix::operator() (const unsigned i, const unsigned j) const
{
  if( i>=row_ || j>=col_ ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "Matrix subscription out of bound";
    throw runtime_error(errMsg.str());
  }
  return (matrix_[i*col_+j]);
}

void
Matrix::display( ostream& os ) const
{
  for( int i=0; i<row_; i++ ){
    for( int j=0; j<col_; j++ ){
      os << "("<<i+1<<","<<j+1<<")="<<matrix_[i*col_+j]<<", ";
    }
    os << endl;
  }
}
