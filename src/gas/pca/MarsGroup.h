/*
 *  untitled.h
 *  testODT
 *
 *  Created by amir on 8/27/13.
 *  Copyright (c) 2013 abiglari. All rights reserved.
 *
 */

#include <iosfwd>
#include "gas/pca/Mars.h"

#ifndef MARS_GROUP_H
#define MARS_GROUP_H

class MarsGroup  {
  std::vector<Mars> m_;
  std::vector<std::string> iVarNames_;
  std::vector<std::string> dVarNames_;
  int ndvar_, nivar_;

public:
  MarsGroup (const std::string marsGroupFile);
  ~MarsGroup();
  void display(std::ostream& outputStrm) const;
  //    Mars& operator[] (const unsigned i) const;
  const Mars& operator[] (const std::string dVarName) const;
  const std::vector<std::string>& get_dvar_names() const;
  const std::vector<std::string>& get_ivar_names() const;
  //    std::vector<double>& operator() (const std::vector<double>& iVars) const;
  //    std::vector<double>& evaluate (const std::vector<double>& iVars) const;

};

#endif /* defined(MARS_GROUP_H) */
