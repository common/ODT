//
//  mars.h
//  testODT1
//
//  Created by abiglari on 9/4/13.
//  Copyright (c) 2013 abiglari. All rights reserved.
//

#ifndef MARS_H
#define MARS_H

#include <iosfwd>
#include <vector>
#include "gas/pca/BasisFunction.h"

class Mars{
  std::vector<double> coefs_;
  std::vector<BasisFunction> basis_;
  int nivar_;
  void readFromFile (std::ifstream& mFStream);
public:
  Mars(const std::string marsFile);
  Mars (std::ifstream& mFStream);
  ~Mars();
  void display(std::ostream& outputStrm) const;
  double operator() (const std::vector<double>& iVars) const;
  //    double evaluate (const std::vector<double>& iVars) const;

};
#endif /* defined(MARS_H) */
