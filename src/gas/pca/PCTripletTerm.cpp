#include <gas/pca/PCTripletTerm.h>
#include "gas/pca/Matrix.h"

//--------------------------------------------------------------------
template< typename FieldT >
PCTripletTerm<FieldT>::
PCTripletTerm( const Expr::Tag& rhoPCTag,
               const Expr::Tag& rhoTag,
               const int pcNum,
               const PCA& pca )
: Expr::Expression<FieldT>(),
  pcNum_   ( pcNum    ),
  pca_     ( pca      )
{
  rhoPC_ = this->template create_field_request<FieldT>( rhoPCTag );
  rho_   = this->template create_field_request<FieldT>( rhoTag   );
}
//--------------------------------------------------------------------
template< typename FieldT >
void
PCTripletTerm<FieldT>::evaluate()
{
  using namespace SpatialOps;
  
  FieldT& res = this->value();
  
  const FieldT& rhoPC = rhoPC_->field_ref();
  const FieldT& rho   = rho_  ->field_ref();

  res <<= rhoPC + rho * pca_.mappedCenters_[pcNum_];

}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
template< typename FieldT >
PCTripletTerm<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& rhoPCTag,
         const Expr::Tag& rhoTag,
         const int pcNum,
         const PCA& pca )
: ExpressionBuilder(result),
  rhoPCT_(rhoPCTag),
  rhoT_  (rhoTag  ),
  pcNum_ ( pcNum  ),
  pca_   ( pca    )
{}
//--------------------------------------------------------------------
template< typename FieldT >
Expr::ExpressionBase*
PCTripletTerm<FieldT>::Builder::build() const
{
  return new PCTripletTerm<FieldT>( rhoPCT_, rhoT_, pcNum_, pca_);
}
//--------------------------------------------------------------------


template class PCTripletTerm<VolField>;
