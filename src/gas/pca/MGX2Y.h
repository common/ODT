#ifndef MGX2Y_h
#define MGX2Y_h


#include <expression/ExprLib.h>
#include <OperatorsAndFields.h>  // defines field types

#include "gas/pca/MarsGroup.h"


/**
 *  @class  MGX2Y
 *  @author Amir Biglari
 *
 *  @brief Applies the MARS regression on any dataset consistent with the marsgroup
 *
 */

class MGX2Y
: public Expr::Expression< VolField >
{
public:
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const Expr::TagList& indepVarTags,
             const MarsGroup& mg,
             const bool clip=false);
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList indepVarTs_;
    const MarsGroup mg_;
    const bool clip_;
  };
  
  
  ~MGX2Y(){}
  void evaluate();
  
private:
  
  MGX2Y( const Expr::TagList& indepVarTags,
         const MarsGroup& mg,
         const bool clip=false);
  
  
  std::vector<double> indepVarPtVals_;
  
  const MarsGroup mg_;
  const bool clip_;

  DECLARE_VECTOR_OF_FIELDS( VolField, indepVars_ )
  
  std::vector< VolField::const_iterator > indepVarIVec_;
  std::vector< VolField::iterator > depVarIVec_;
  
};

#endif	// MGX2Y_h
