#include <gas/pca/PCAX2Eta.h>
#include "gas/pca/Matrix.h"

//--------------------------------------------------------------------
template< typename FieldT >
PCAX2Eta<FieldT>::
PCAX2Eta( const Expr::TagList& ssCoordVarTags,
          const PCA& pca,
          const bool centering )
: Expr::Expression<FieldT>(),
  pca_ ( pca  ),
  centering_ ( centering )
{
  this->create_field_vector_request( ssCoordVarTags, ssCoordVars_ );
}
//--------------------------------------------------------------------
template< typename FieldT >
void
PCAX2Eta<FieldT>::evaluate()
{
  using namespace SpatialOps;
  
  typedef typename Expr::Expression<FieldT>::ValVec ResTVec;
  ResTVec& pcCoordVars = this->get_value_vec();
  
  // pack a vector with variables in state-space coordinate iterators.
  ssCoordVarIVec_.clear();
  for( size_t i=0; i!=ssCoordVars_.size(); ++i ){
    ssCoordVarIVec_.push_back( ssCoordVars_[i]->field_ref().begin() );
  }
  
  // pack a vector with variables in principal component coordinate iterators.
  pcCoordVarIVec_.clear();
  for( typename ResTVec::iterator ipcVar = pcCoordVars.begin();
      ipcVar!=pcCoordVars.end();
      ++ipcVar )
  {
    pcCoordVarIVec_.push_back( (*ipcVar)->begin() ); 
  }
  
  Matrix ssCoordVarM;
  Matrix pcCoordVarM;

  const FieldT& ssCoord0 = ssCoordVars_[0]->field_ref();
  typename FieldT::const_iterator iGridPoint = ssCoord0.begin();
  while( iGridPoint != ssCoord0.end() )
  {
    // extract state-space coordinate variabes and pack them into a temporary buffer at each point in the grid.
    ssCoordVarPtVals_.resize(ssCoordVarIVec_.size());
    std::vector<double>::iterator issptv = ssCoordVarPtVals_.begin();
    for( typename std::vector<typename FieldT::const_iterator>::iterator issCoordVar = ssCoordVarIVec_.begin();
        issCoordVar!=ssCoordVarIVec_.end();
        ++issptv, ++issCoordVar )
    {
      *issptv = **issCoordVar;
      ++(*issCoordVar); // increment iterators for state-space coordinate variables to the next point
    }
    
    // PCA mapping happens here
    ssCoordVarM = Matrix(ssCoordVarPtVals_,1,ssCoordVars_.size());
    pca_.x2eta(ssCoordVarM,pcCoordVarM,centering_);
    
    // unpack the PC coordinate variables into the result
    typename std::vector<typename FieldT::iterator>::iterator ipcCoordVar = pcCoordVarIVec_.begin();
    for( int i=0; i<pcCoordVarM.col_; ++i, ++ipcCoordVar )
    {
      **ipcCoordVar = pcCoordVarM(0,i);
      ++(*ipcCoordVar);  // advance iterator to next grid point.
    }
    ++iGridPoint;
  }
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
template< typename FieldT >
PCAX2Eta<FieldT>::Builder::
Builder( const Expr::TagList& result,
         const Expr::TagList& ssCoordVarTags,
         const PCA& pca,
         const bool centering )
: ExpressionBuilder(result),
  ssCoordVarTs_(ssCoordVarTags),
  pca_ ( pca  ),
  centering_ ( centering )
{}
//--------------------------------------------------------------------
template< typename FieldT >
Expr::ExpressionBase*
PCAX2Eta<FieldT>::Builder::build() const
{
  return new PCAX2Eta<FieldT>( ssCoordVarTs_, pca_, centering_ );
}
//--------------------------------------------------------------------


template class PCAX2Eta<VolField>;
