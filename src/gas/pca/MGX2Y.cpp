#include <gas/pca/MGX2Y.h>

using std::vector;
using std::string;

//--------------------------------------------------------------------
MGX2Y::MGX2Y( const Expr::TagList& indepVarTags,
              const MarsGroup& mg,
              const bool clip )
: Expr::Expression<VolField>(),
  mg_  ( mg   ),
  clip_( clip )
{
  this->create_field_vector_request<VolField>( indepVarTags, indepVars_ );
}
//--------------------------------------------------------------------
void
MGX2Y::evaluate()
{
  using namespace SpatialOps;
  
  typedef typename Expr::Expression<VolField>::ValVec ResTVec;
  ResTVec& depVars = this->get_value_vec();
  
  // pack a vector with independent variables iterators.
  indepVarIVec_.clear();
  for( size_t i=0; i<indepVars_.size(); ++i ){
    indepVarIVec_.push_back( indepVars_[i]->field_ref().begin() );
  }
  
  // pack a vector with dependent variables iterators.
  depVarIVec_.clear();
  for( ResTVec::iterator idVar = depVars.begin();
      idVar!=depVars.end();
      ++idVar )
  {
    depVarIVec_.push_back( (*idVar)->begin() );
  }
  
  vector<double> depVarV;
  const int ndVar = depVars.size();
  depVarV.resize(ndVar);

  vector<const Mars*> mgVec;
  const vector<string>& depVarNames = mg_.get_dvar_names();
  for (int i=0; i<ndVar; i++) {
    mgVec.push_back( &(mg_[depVarNames[i]]) );
  }

  const VolField& indepVar0 = indepVars_[0]->field_ref();
  VolField::const_iterator iGridPoint = indepVar0.begin();
  while( iGridPoint != indepVar0.end() )
  {
    // extract independent variabes and pack them into a temporary buffer at each point in the grid.
    indepVarPtVals_.resize(indepVarIVec_.size());
    vector<double>::iterator iivptv = indepVarPtVals_.begin();
    for( vector<VolField::const_iterator>::iterator iiVar = indepVarIVec_.begin();
        iiVar!=indepVarIVec_.end();
        ++iivptv, ++iiVar )
    {
      *iivptv = **iiVar;
      ++(*iiVar); // increment iterators for independent variables to the next point
    }
    
    // MARS mapping happens here
    //    const vector<string>& depVarNames = mg_.get_dvar_names();
    for( int i=0; i<ndVar; i++ ){
      depVarV[i] = (*mgVec[i])(indepVarPtVals_);
      //      depVarV[i] = depVarVal;
    }

    // unpack the dependent variables into the result
    vector<VolField::iterator>::iterator idVar = depVarIVec_.begin();
    for( int i=0; i<depVarV.size(); ++i, ++idVar ){
      if (clip_ && i!=0)
        **idVar = std::min( 1.0,std::max(0.0,depVarV[i]));
      else
      **idVar = depVarV[i];
      ++(*idVar);  // advance iterator to next grid point.
    }
    ++iGridPoint;
  }

}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
MGX2Y::Builder::
Builder( const Expr::TagList& result,
         const Expr::TagList& indepVarTags,
         const MarsGroup& mg,
         const bool clip )
: ExpressionBuilder(result),
  indepVarTs_(indepVarTags),
  mg_        ( mg         ),
  clip_      ( clip       )
{}
//--------------------------------------------------------------------
Expr::ExpressionBase*
MGX2Y::Builder::build() const
{
  return new MGX2Y( indepVarTs_, mg_, clip_ );
}
//--------------------------------------------------------------------
