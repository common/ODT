//
//  Map2GridVec.cpp
//  M2G_test
//
//  Created by abiglari on 6/30/14.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include "gas/pca/Map2GridVec.h"

using namespace std;

Map2GridVec::Map2GridVec (const string gridVecFile, const int ndims)
{
  gVec_ = boost::shared_ptr< std::vector<Map2Grid> >( new std::vector<Map2Grid>() );

  std::vector<double> gridLims, gridVals;

  ifstream gridVecFStream;
  gridVecFStream.open(gridVecFile.c_str());
  
  string line, field, ivName;
  double num;
  
  if( !gridVecFStream ){              // file couldn't be opened
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << gridVecFile << " data file could not be opened";
    throw runtime_error(errMsg.str());
  }
  else{
    cout << "Map2GridVec data file SUCCESSFULLY opened!\n";
  }
  
  getline(gridVecFStream, line);
  stringstream ss(line);
  getline(ss, field, ':');
  if( field!="ndims" ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "ndims could not be found in its place in the Map2GridVec data file";
    throw runtime_error(errMsg.str());
  }
  getline(ss, field, ':');
  stringstream ss2(field);
  ss2 >> ndims_;
  if( ndims_>3 ){                 // dimensions more than 3 are not supported
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "More the 3 dimensions is not supported in the Map2GridVec class" ;
    throw runtime_error(errMsg.str());
  }
  
  getline(gridVecFStream, line);
  stringstream ss3(line);
  getline(ss3, field, ':');  // getting independent variable names
  if( field!="iVarNames" ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "iVarNames could not be found in its place in the Map2GridVec data file";
    throw runtime_error(errMsg.str());
  }

  while( getline(ss3,field,',') ){  // break line into comma delimitted fields
    stringstream ss2(field);
    ss2 >> ivName;
    iVarNames_.push_back(ivName);  // add the next independent variable into the vector
  }
  
  assert( iVarNames_.size() == ndims_ );  // jcs note that we could eliminate ndims_ as an argument to the constructor.

  for( int i=0; i<ndims_; i++ ){   // loop over dimensions of the grid limits
    getline(gridVecFStream,line);
    stringstream ss(line);
    while (getline(ss,field,',') ){ // break line into comma delimitted fields
      stringstream ss2(field);
      ss2 >> num;
      gridLims.push_back(num);    // add the next grid limit value to a 1D array
    }
  }
  nPts_ = (gridLims.size()/ndims_) - 1;

  dVarNames_.resize(ndims_);
  int gridSize=1;
  for( int i=0; i<ndims_; i++ ){
    gridSize *= nPts_;
  }
  getline(gridVecFStream,line);
  int count=0;
  while( line!="" ){
    if( line.length()<9 || line.substr(0,9)!="Grid for " ){
      ostringstream errMsg;
      errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "This is probably not a Map2GridVec data file. Each grid object block should start with 'Grid for dvar:'";
      cout << line << endl;
      throw runtime_error(errMsg.str());
    }
    dVarNames_[count++] = line.substr( 9, line.length()-10 );
    
    gridVals.clear();
    gridVals.resize(gridSize);
    
    int nline=1;
    for( int i=0; i<ndims_; i++ ){
      if( i!=0 ) nline*=nPts_;
    }
    
    int vecIndex=0;
    for( int i=0; i<nline; i++ ){  // loop over all of the lines of the grid
      getline(gridVecFStream,line);
      stringstream ss(line);
      while( getline(ss,field,',') ){ // break line into comma delimitted fields
        stringstream ss2(field);
        ss2 >> num;
        gridVals[vecIndex++]=num;   // add the next grid value to a 1D array
      }
    }
    
    gVec_->push_back( Map2Grid(gridLims, gridVals, ndims_, nPts_) );
    getline(gridVecFStream,line);
  }
  
  gridVecFStream.close();
  
  ndvar_ = (int)gVec_->size();
}

Map2GridVec::~Map2GridVec()
{}

void Map2GridVec::display( ostream& outputStrm ) const
{
  cout << "Number of Dimensions: " << ndims_ << endl;
  cout << "Number of dependent variables: " << ndvar_ << endl;
  for( int i=0; i<gVec_->size(); i++ ){
    (*gVec_)[i].display(outputStrm);
  }
}

const Map2Grid& Map2GridVec::operator[] (const string dVarName) const
{
  bool found=false;
  int index;
  for( int i=0; i<dVarNames_.size(); i++ ){
    if( dVarName==dVarNames_[i] ){
      found = true;
      index = i;
      break;
    }
  }
  if( !found ){
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The Map2GridVec does not have " << dVarName << " as one of its dependent variables";
    throw runtime_error(errMsg.str());
  }
  return (*gVec_)[index];
}
