#ifndef PCTripletTerm_h
#define PCTripletTerm_h

#include <expression/ExprLib.h>
#include <OperatorsAndFields.h>  // defines field types

#include "gas/pca/PCA.h"

/**
 *  @class  PCTripletTerm
 *  @author Amir Biglari
 *
 *  @brief Calculates the proper term for the PCs that triplet mapping needs
 *  to be applied on. This term is \f[\rho\eta + \rho M [\alpha][A] \f]. 
 *  Where \f[ \alpha \f] and \f[ [A] \f] are the scaling factor and the 
 *  eigenvector matrices of the PCA mapping.
 *
 */
template< typename FieldT >
class PCTripletTerm
: public Expr::Expression< FieldT >
{
  const PCA pca_;
  const int pcNum_;
  DECLARE_FIELDS( FieldT, rhoPC_, rho_ )
  
  PCTripletTerm( const Expr::Tag& rhoPCTag,
                 const Expr::Tag& rhoTag,
                 const int pcNum,
                 const PCA& pca );
public:
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& rhoPCTag,
             const Expr::Tag& rhoTag,
             const int pcNum,
             const PCA& pca );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::Tag rhoPCT_, rhoT_;
    const PCA pca_;
    const int pcNum_;
  };

  ~PCTripletTerm(){}
  void evaluate();
};

#endif	// PCTripletTerm_h
