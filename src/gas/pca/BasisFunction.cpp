//
//  BasisFunction.cpp
//  testODT1
//
//  Created by abiglari on 9/4/13.
//  Copyright (c) 2013 abiglari. All rights reserved.
//

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include "gas/pca/BasisFunction.h"

using namespace std;

BasisFunction::BasisFunction( ifstream& bfFStream )
{
  readFromFile (bfFStream);
}

BasisFunction::BasisFunction( const string basisFile )
{
  ifstream bfFStream;
  bfFStream.open(basisFile.c_str());
  if(!bfFStream)                                   // file couldn't be opened
  {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "BasisFunction data file could not be opened";
    throw runtime_error(errMsg.str());
  }
  else
    cout << "BasisFunction file SUCCESSFULLY opened!\n";

  readFromFile (bfFStream);

  bfFStream.close();
}

BasisFunction::BasisFunction( const vector<int>& dims,
                              const vector<int>& senses,
                              const vector<double>& xknots,
                              const vector<int>& orders,
                              int i )
{
  if (i==dims.size()){
    haveParent_ = false;
    dim_    = 0;
    order_  = 0;
    xknot_  = 0;
    sense_  = 1;
    parent_ = BasisFnPtr();
  }
  else{
    dim_        = dims[i];
    sense_      = senses[i];
    order_      = orders[i];
    xknot_      = xknots[i];
    parent_    = BasisFnPtr( new BasisFunction(dims,senses,xknots,orders,i+1) );
    haveParent_ = true;
  }
}

BasisFunction::BasisFunction()
: dim_   (-1  ),
  sense_ (0   ),
  order_ (-1  ),
  xknot_ (0.0 ),
  haveParent_ (false),
  parent_ (BasisFnPtr())
{}

void BasisFunction::readFromFile (ifstream& bfFStream)
{
  string line, field;
  int num;
  double num_double;
  vector<int> dims,senses,orders;
  vector<double> xknots;

  getline(bfFStream,line);
  if (line.length()<5 || line.substr(0,5)!="basis")
  {
    ostringstream errMsg;
    errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "This is probably not a basis data file. Each basis object block should start with 'basis#'";
    throw runtime_error(errMsg.str());
  }

  getline(bfFStream,line);

  if (line=="UnitBasis") {
    haveParent_ = false;
    dim_    = 0;
    order_  = 0;
    xknot_  = 0;
    sense_  = 1;
    parent_ = BasisFnPtr();
  }
  else{
    {
      stringstream ss(line);
      getline(ss, field, ':');
      if (field!="dims")
      {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "dims could not be found in its place in the basis block";
        throw runtime_error(errMsg.str());
      }
      while (getline(ss,field,',') )                    // break line into comma delimitted fields
      {
        stringstream ss2(field);
        ss2 >> num;
        dims.push_back(num);                        // add the next member of the dims
      }
    }
    getline(bfFStream,line);
    {
      stringstream ss(line);
      getline(ss, field, ':');
      if (field!="senses")
      {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "senses could not be found in its place in the basis block";
        throw runtime_error(errMsg.str());
      }
      while (getline(ss,field,',') )                    // break line into comma delimitted fields
      {
        stringstream ss2(field);
        ss2 >> num;
        senses.push_back(num);                        // add the next member of the dims
      }
    }
    getline(bfFStream,line);
    {
      stringstream ss(line);
      getline(ss, field, ':');
      if (field!="xknots")
      {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "xknots could not be found in its place in the basis block";
        throw runtime_error(errMsg.str());
      }
      while (getline(ss,field,',') )                    // break line into comma delimitted fields
      {
        stringstream ss2(field);
        ss2 >> num_double;
        xknots.push_back(num_double);                        // add the next member of the dims
      }
    }
    getline(bfFStream,line);
    {
      stringstream ss(line);
      getline(ss, field, ':');
      if (field!="orders")
      {
        ostringstream errMsg;
        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "orders could not be found in its place in the basis block";
        throw runtime_error(errMsg.str());
      }
      while (getline(ss,field,',') )                    // break line into comma delimitted fields
      {
        stringstream ss2(field);
        ss2 >> num;
        orders.push_back(num);                        // add the next member of the dims
      }
    }
    dim_       = dims[0];
    sense_     = senses[0];
    order_     = orders[0];
    xknot_     = xknots[0];
    parent_    = BasisFnPtr( new BasisFunction(dims,senses,xknots,orders,1) );
    haveParent_= true;
  }
}

BasisFunction::~BasisFunction()
{}

void BasisFunction::display(ostream& outputStrm) const
{
  if (!haveParent_)
    cout << "Unit basis function" << endl;
  else
  {
    cout << "xknot: " << xknot_ << endl;
    cout << "order: " << order_ << endl;
    cout << "sense: " << sense_ << endl;
    cout << "dim: "   << dim_   << endl;
    cout << "Parent:" << endl;
    parent_->display(outputStrm);
  }
}

double BasisFunction::operator() (const vector<double>& iVars) const
{
  //    if (dim_>iVars.size()) {
  //        ostringstream errMsg;
  //        errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The iVars vector is not compatible with this basis. Required dimension by the BasisFunction exceeds the number of dependent variables";
  //        throw runtime_error(errMsg.str());
  //    }
  double dVar;
  if (haveParent_)
  {
    if (dim_>iVars.size()) {
      ostringstream errMsg;
      errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The iVars vector is not compatible with this basis. Required dimension by the BasisFunction exceeds the number of dependent variables";
      throw runtime_error(errMsg.str());
    }

    double bkterm = sense_ * (iVars[dim_-1] - xknot_);
    bkterm = (bkterm<=0 ? 0 : bkterm);

    dVar = bkterm;
    for (int i=1; i<order_; i++)
      dVar *= bkterm;


    if( haveParent_ )
      dVar *= (*parent_)(iVars);
  }
  else
    dVar=1;

  return dVar;
  //    return (evaluate(iVars));
}

//double BasisFunction::evaluate (const vector<double>& iVars) const
//{
//    double dVar;
//    if (haveParent_)
//    {
//        if (dim_>iVars.size()) {
//            ostringstream errMsg;
//            errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "The iVars vector is not compatible with this basis. Required dimension by the BasisFunction exceeds the number of dependent variables";
//            throw runtime_error(errMsg.str());
//        }
//        
//        double bkterm = sense_ * (iVars[dim_-1] - xknot_);
//        bkterm = (bkterm<=0 ? 0 : bkterm);
//        
//        dVar = bkterm;
//        for (int i=1; i<order_; i++)
//            dVar *= bkterm;
//        
//        
//        if( haveParent_ )
//            dVar *= parent_->evaluate(iVars);
//    }
//    else
//        dVar=1;
//
//    return dVar;
//}
