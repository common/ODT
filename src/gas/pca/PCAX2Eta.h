#ifndef PCAX2Eta_h
#define PCAX2Eta_h


#include <expression/ExprLib.h>
#include <OperatorsAndFields.h>  // defines field types

#include "gas/pca/PCA.h"


/**
 *  @class  PCAX2Eta
 *  @author Amir Biglari
 *
 *  @brief Applies the PCA mapping on any dataset consistent with the mapping,
 *  to move it from the state-space coordinates to the PC-coordinates.
 *
 */

template< typename FieldT >
class PCAX2Eta
: public Expr::Expression< FieldT >
{
public:
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const Expr::TagList& ssCoordVarTags,
             const PCA& pca,
             const bool centering );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList ssCoordVarTs_;
    const PCA pca_;
    const bool centering_;
  };
  
  
  ~PCAX2Eta(){}
  void evaluate();
  
private:
  
  PCAX2Eta( const Expr::TagList& ssCoordVarTags,
            const PCA& pca,
            const bool centering );
  
  std::vector<double> ssCoordVarPtVals_;
  
  const PCA pca_;
  const bool centering_;
  
  DECLARE_VECTOR_OF_FIELDS( FieldT, ssCoordVars_ )
  
  std::vector< typename FieldT::const_iterator > ssCoordVarIVec_;
  std::vector< typename FieldT::iterator > pcCoordVarIVec_;
  
};

#endif	// PCAX2Eta_h
