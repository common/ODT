#ifndef PCExtraTerm_h
#define PCExtraTerm_h


#include <gas/pca/PCA.h>
#include <gas/pca/Matrix.h>

#include <expression/ExprLib.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/transport.h>


template< typename FluxT >
class PCExtraTerm
: public Expr::Expression< typename SpatialOps::VolType<FluxT>::VolField >
{
  typedef typename SpatialOps::VolType<FluxT>::VolField  ScalarT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Gradient,   ScalarT,FluxT>::type GradT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Interpolant,FluxT,ScalarT>::type InterpT;
  
  typedef SpatialOps::SpatFldPtr<ScalarT> ScalarPtrT;
  
  PCExtraTerm( const Expr::Tag& temperatureTag,
               const Expr::Tag& heatCapacityTag,
               const Expr::Tag& pcTag,
               const Expr::TagList& speciesTag,
               const PCA pca,
               const int nPCs,
               const Expr::TagList& spFluxTags );
  
  ~PCExtraTerm();
  
  Cantera::Transport* const trans_;
  Cantera::ThermoPhase& gas_;

  const PCA pca_;
  const int nPCs_, nspec_;
  const double pressure_;
  std::vector<double> ptSpec_, ptPCGrad_, ptSpFlux_, ptFactors_;
  std::vector<double> ptSpCp_, spmw_;
  
  std::vector<ScalarPtrT> fluxInterp_;

  const GradT* gradOp_;
  const InterpT* interpOp_;

  DECLARE_VECTOR_OF_FIELDS( ScalarT, species_  )
  DECLARE_VECTOR_OF_FIELDS( ScalarT, pcs_      )
  DECLARE_VECTOR_OF_FIELDS( FluxT,   spFluxes_ )

  DECLARE_FIELDS( ScalarT, temperature_, heatCap_ )

  std::vector< typename ScalarT::const_iterator > specIVec_, spFluxIVec_;
  std::vector< typename ScalarT::iterator       > extermIVec_;


public:


  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& heatCapacityTag,
             const Expr::Tag& pcTag,
             const Expr::TagList& speciesTag,
             const PCA pca,
             const int nPCs,
             const Expr::TagList& spFluxTags );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const PCA pca_;
    const int nPCs_;
    const Expr::Tag pcT_, tT_, heatCapT_;
    const Expr::TagList specT_, spFluxTs_;
  };


  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  
};


// ###################################################################
//
//                         Implementation
//
// ###################################################################



//--------------------------------------------------------------------
template< typename FluxT >
PCExtraTerm<FluxT>::
PCExtraTerm( const Expr::Tag& temperatureTag,
             const Expr::Tag& heatCapacityTag,
             const Expr::Tag& pcTag,
             const Expr::TagList& specTags,
             const PCA pca,
             const int nPCs,
             const Expr::TagList& spFluxTags)
  : Expr::Expression<ScalarT>(),
    nPCs_( nPCs ),
    pca_ ( pca  ),

    trans_( CanteraObjects::get_transport() ),
    gas_     ( trans_->thermo() ),
    nspec_   ( gas_.nSpecies()  ),
    pressure_( gas_.pressure()  )
{
  Expr::TagList pcTags;
  for( int i=0; i<nPCs_; ++i ){
    const std::string name = pcTag.name() + "_" + boost::lexical_cast<std::string>(i);
    pcTags.push_back( Expr::Tag(name,pcTag.context()) );
  }
  
  this->template create_field_vector_request<ScalarT>( specTags,   species_  );
  this->template create_field_vector_request<ScalarT>( pcTags,     pcs_      );
  this->template create_field_vector_request<FluxT  >( spFluxTags, spFluxes_ );

  temperature_ = this->template create_field_request<ScalarT>( temperatureTag  );
  heatCap_     = this->template create_field_request<ScalarT>( heatCapacityTag );

  ptSpec_.resize( nspec_ );
  ptPCGrad_.resize( nPCs_ );
  ptSpCp_.resize( nspec_ );
  spmw_.resize( nspec_ );
  ptSpFlux_.resize( nspec_ );
  ptFactors_.assign( nspec_, 0.0 );
}
//--------------------------------------------------------------------
template< typename FluxT >
PCExtraTerm<FluxT>::~PCExtraTerm()
{
  CanteraObjects::restore_transport( trans_ );
}
//--------------------------------------------------------------------
template< typename FluxT >
void
PCExtraTerm<FluxT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}
//--------------------------------------------------------------------
template< typename FluxT >
void
PCExtraTerm<FluxT>::evaluate()
{
  using namespace SpatialOps;
  typename Expr::Expression<ScalarT>::ValVec& results = this->get_value_vec();
  
  fluxInterp_.clear();
  for( size_t i=0; i<nspec_; ++i ){
    fluxInterp_.push_back( SpatialFieldStore::get<ScalarT>( *(results[0]) ) );
  }

  // interpolate species diffusive fluxes to SVol
  // pack a vector with species diff flux iterators.
  spFluxIVec_.clear();
  for( size_t i=0; i<spFluxes_.size(); ++i ){
    ScalarT& fluxInterp = *fluxInterp_[i];
    fluxInterp <<= (*interpOp_)( spFluxes_[i]->field_ref() );
    spFluxIVec_.push_back( fluxInterp.begin() );
  }

  // pack a vector with species iterators.
  specIVec_.clear();
  for( size_t i=0; i<species_.size(); ++i ){
    specIVec_.push_back( species_[i]->field_ref().interior_begin() );
  }
  
  // calculate the gradient of each principal component.
  for( size_t i=0; i<pcs_.size(); ++i ){
    *results[i] <<= (*interpOp_)( (*gradOp_)( pcs_[i]->field_ref() ) );
  }

  // pack a vector with PCs extra term iterators.
  extermIVec_.clear();
  for( size_t i=0; i<results.size(); ++i ){
    extermIVec_.push_back( results[i]->begin() );
  }
  
  typename       ScalarT::const_iterator itemp    = temperature_->field_ref().begin();
  const typename ScalarT::const_iterator itempe   = temperature_->field_ref().end();
  typename       ScalarT::const_iterator iheatcap = heatCap_    ->field_ref().begin();
  
  double sumTerm;
//  if (model_ == "Weighted") {
//    // loop over points to calculate fluxes.
//    Matrix svDiffCoeffM;
//    Matrix pcDiffCoeffM;
//    for( ; itemp!=itempe; ++itemp, ++idens, ++iheatcap, ++itcond ){
//      // extract compositions and pack them into the temporary buffer
//      std::vector<double>::iterator ipt = ptSpec_.begin();
//      for( typename std::vector<typename FluxT::const_iterator>::const_iterator ispec = specIVec_.begin();
//          ispec!=specIVec_.end();
//          ++ipt, ++ispec )
//      {
//        *ipt = **ispec;
//      }
//      
//      // extract state variable diffusion coefficients and pack them into the temporary buffer
//      try{
//        // note that we should really use the spatially varying pressure here...
//        gas_.setState_TPY( *itemp, pressure_, &ptSpec_[0] );
//        trans_->getMixDiffCoeffsMass(&ptSvDiffCoeff_[0]);
//      }
//      catch( Cantera::CanteraError ){
//        std::cout << "Error in SpeciesDiffFluxFromYs" << std::endl
//        << " T=" << *itemp << ", p=" << pressure_ << std::endl
//        << " Yi=";
//        double ysum=0;
//        for( int i=0; i<nspec_; ++i ){
//          std::cout << ptSpec_[i] << " ";
//          ysum += ptSpec_[i];
//        }
//        std::cout << std::endl << "sum Yi = " << ysum << std::endl;
//        throw std::runtime_error("Problems in SpeciesDiffFluxFromYs expression.");
//      }
//      // remove the diffusion coefficient of the last species
//      ptSvDiffCoeff_.pop_back();
//      // multiply the species diffusion coefficients by the density
//      for (int i=0; i<nspec_-1; i++) {
//        ptSvDiffCoeff_[i] *= *idens;
//      }
//      // insert the thermal conductivity over the heat capacity in to the beginnig of the diffusion coefficients vector
//      ptSvDiffCoeff_.insert (ptSvDiffCoeff_.begin(), *itcond / *iheatcap );
//      
//      // Transform the state variables diffusion coefficients matrix to the PC diffusion coefficiets matrix using the PCA mapping
//      pca_.diffusionMatrixTransform (ptSvDiffCoeff_, pcDiffCoeffM, model_);
//      
//      // Transform the state variables diffusion coefficients to the PC diffusion coefficiets using the PCA mapping
////      svDiffCoeffM = Matrix(ptSvDiffCoeff_,1,ptSvDiffCoeff_.size());
////      pca_.x2eta(svDiffCoeffM,pcDiffCoeffM, false);
//      
//      // calculate the PC fluxes and increment their iterators to next mesh point.
//      int i=0;
//      for( typename std::vector<typename FluxT::iterator>::iterator iflux = fluxIVec_.begin();
//          iflux!=fluxIVec_.end();
//          ++iflux, i++ )
//      {
//        **iflux *= pcDiffCoeffM(0,i);
//        ++(*iflux);  // increment flux iterator to the next mesh point
//      }
//      
//      // increment iterators for species to the next point
//      for( typename std::vector<typename FluxT::const_iterator>::iterator specVecIter=specIVec_.begin();
//          specVecIter!=specIVec_.end();
//          ++specVecIter )
//      {
//        ++(*specVecIter);
//      }
//      
//      
//    } // loop over grid points.
//  }
//  else if (model_ == "Analytic") {
    // loop over points to calculate fluxes.
    Matrix pcGradM, exTermM;
    Matrix pcFactorsM;
  //    Matrix pcDiffCoeffM;

    for( ; itemp!=itempe; ++itemp, ++iheatcap ){
      // extract compositions and pack them into the temporary buffer
      std::vector<double>::iterator ipt = ptSpec_.begin();
      for( typename std::vector<typename ScalarT::const_iterator>::const_iterator ispec = specIVec_.begin();
          ispec!=specIVec_.end();
          ++ipt, ++ispec )
      {
        *ipt = **ispec;
      }

      // extract species diff fluxes and pack them into the temporary buffer
      std::vector<double>::iterator iptdiff = ptSpFlux_.begin();
      for( typename std::vector<typename ScalarT::const_iterator>::const_iterator ispFlux = spFluxIVec_.begin();
          ispFlux!=spFluxIVec_.end();
          ++iptdiff, ++ispFlux )
      {
        *iptdiff = **ispFlux;
      }

      // extract state variable diffusion coefficients and pack them into the temporary buffer
      try{
        // note that we should really use the spatially varying pressure here...
        gas_.setState_TPY( *itemp, pressure_, &ptSpec_[0] );
        gas_.getPartialMolarCp(&ptSpCp_[0]);
        spmw_ = gas_.molecularWeights();
      }
      catch( Cantera::CanteraError ){
        std::cout << "Error in SpeciesDiffFluxFromYs" << std::endl
        << " T=" << *itemp << ", p=" << pressure_ << std::endl
        << " Yi=";
        double ysum=0;
        for( int i=0; i<nspec_; ++i ){
          std::cout << ptSpec_[i] << " ";
          ysum += ptSpec_[i];
        }
        std::cout << std::endl << "sum Yi = " << ysum << std::endl;
        throw std::runtime_error("Problems in SpeciesDiffFluxFromYs expression.");
      }
      // Here the diffusion coefficient matrix of the Pcs willl be calculated by transforming the
      // statevariables mixture averaged diffusion coefficients placed in a diagonal matrix. This
      // tranform uses the PCA mapping in the following
      //
      //      ⎡ EV1_1 EV1_2 EV1_3 . . . ⎤   ⎡ D_1  0   0  ... ⎤   ⎡ EV1_1 EV2_1 EV3_1 . . . ⎤      ⎡ Dpc_11 Dpc_12 Dpc_13  . . . ⎤
      //      ⎜ EV2_1 EV2_2 EV2_3 . . . ⎥   ⎜  0  D_2  0  ... ⎥   ⎜ EV1_2 EV2_2 EV3_2 . . . ⎥      ⎜ Dpc_21 Dpc_22 Dpc_23  . . . ⎥
      //      ⎜ EV3_1 EV3_2 EV3_3 . . . ⎥ * ⎜  0   0  D_3 ... ⎥ * ⎜ EV1_3 EV2_3 EV3_3 . . . ⎥  --  ⎜ Dpc_31 Dpc_32 Dpc_33  . . . ⎥
      //      ⎜   .     .               ⎥   ⎜  .   .          ⎥   ⎜   .     .               ⎥  --  ⎜   .      .                  ⎥
      //      ⎜   .           .         ⎥   ⎜  .       .      ⎥   ⎜   .           .         ⎥      ⎜   .             .           ⎥
      //      ⎣   .                 .   ⎦   ⎣  .           .  ⎦   ⎣   .                 .   ⎦      ⎣   .                     .   ⎦
      //
      // Note that we should take the scalings into account in the actual calculations.
      //
      //          T      -1
      // [eig_vec]  *  [S]  *  [D_sv]  * [S] * [eig_vec]  =  [D_pc]
      //
      //             -1                                         -1
      // But since [S] , [D_sv] and [S] are diagonal matrices [S] and [S] will cancell each other out.
      // Only the first nPCs_ rows and column of the result is needed for the calculation.
      // Therefore, the reduced eigen vector matrices can be used.
      
      sumTerm = 0.0;
      for (int i=0; i<ptSpFlux_.size(); i++){
        sumTerm += (ptSpCp_[i] / spmw_[i]) * ptSpFlux_[i];
      }
      // insert the thermal conductivity over the heat capacity in to the beginnig of the diffusion coefficients vector
      ptFactors_[0] = -(sumTerm) / *iheatcap ;
      
      // Transform the state variables diffusion coefficients matrix to the PC diffusion coefficiets matrix using the PCA mapping
      pca_.diffusionMatrixTransform (ptFactors_, pcFactorsM, "Analytic");
      
      // extract pc gradients and pack them into the temporary buffer
      std::vector<double>::iterator iptpcg = ptPCGrad_.begin();
      for( typename std::vector<typename ScalarT::iterator>::iterator ipcgrad = extermIVec_.begin();
          ipcgrad!=extermIVec_.end();
          ++iptpcg, ++ipcgrad )
      {
        *iptpcg = **ipcgrad;
      }
      
      // calculate the PC fluxes
      pcGradM = Matrix(ptPCGrad_,nPCs_,1);
      exTermM = pcFactorsM * pcGradM;
      
      // calculate the PC extra terms and increment their iterators to next mesh point.
      int i=0;
      for( typename std::vector<typename ScalarT::iterator>::iterator iexterm = extermIVec_.begin();
          iexterm!=extermIVec_.end();
          ++iexterm, i++ )
      {
        **iexterm = exTermM(i,0);
        ++(*iexterm);  // increment extra terms iterator to the next mesh point
      }
      
      // increment iterators for species and their fluxes to the next point
      typename std::vector<typename ScalarT::const_iterator>::iterator spFluxVecIter=spFluxIVec_.begin();
      for( typename std::vector<typename ScalarT::const_iterator>::iterator specVecIter=specIVec_.begin();
          specVecIter!=specIVec_.end();
          ++specVecIter, ++spFluxVecIter )
      {
        ++(*specVecIter);
        ++(*spFluxVecIter);
      }
      
      
    } // loop over grid points.
    
//  }
}
//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------
template<typename FluxT>
PCExtraTerm<FluxT>::Builder::
Builder( const Expr::TagList& result,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& heatCapacityTag,
         const Expr::Tag& pcTag,
         const Expr::TagList& speciesTag,
         const PCA pca,
         const int nPCs,
         const Expr::TagList& spFluxTags )
  : ExpressionBuilder(result),
    tT_      ( temperatureTag  ),
    pca_     ( pca             ),
    nPCs_    ( nPCs            ),
    specT_   ( speciesTag      ),
    pcT_     ( pcTag           ),
    heatCapT_( heatCapacityTag ),
    spFluxTs_( spFluxTags      )
{
}
//--------------------------------------------------------------------
template<typename FluxT>
Expr::ExpressionBase*
PCExtraTerm<FluxT>::Builder::build() const
{
  return new PCExtraTerm<FluxT>( tT_, heatCapT_, pcT_, specT_, pca_, nPCs_, spFluxTs_ );
}
//--------------------------------------------------------------------

#endif
