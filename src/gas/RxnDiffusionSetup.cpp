#include <gas/RxnDiffusionSetup.h>
#include <OperatorsAndFields.h>

//--- PCA/MARS stuff includes ---//
#include <gas/pca/PCA.h>
#include <gas/pca/MarsGroup.h>
#include <gas/pca/Map2GridVec.h>
#include <gas/pca/PCEquation.h>

//--- Transport Equations we will solve ---//
#include <gas/MomentumEquation.h>
#include <gas/TotalInternalEnergyEquation.h>
#include <gas/SpeciesEquation.h>
#include <gas/DensityEquation.h>
#include <gas/PositionEquation.h>
#include <gas/Pressure.h>
#include <gas/DiffusiveFlux.h>
#include <gas/ScalarDissipationExpr.h>
#include <gas/MassFromMoleExpr.h> // initial condition on species
#include <gas/TarAndSootInfo.h>


#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/IdealGasMix.h>
#include <pokitt/thermo/HeatCapacity_Cp.h>
#include <pokitt/thermo/HeatCapacity_Cv.h>
#include <pokitt/thermo/Enthalpy.h>
#include <pokitt/thermo/Pressure.h>
#include <pokitt/transport/ThermalCondMix.h>
#include <pokitt/MixtureFractionExpr.h>

#include <StringNames.h>

#include <gas/TarEquation.h>
#include <gas/SootEquation.h>
#include <gas/SootParticleEquation.h>

#include <gas/TarGasificationRate.h>
#include <gas/TarOxidationRate.h>
#include <gas/SootFormationRate.h>
#include <gas/SootOxidationRate.h>
#include <gas/SootAgglomerationRate.h>

#include <gas/TarSource.h>
#include <gas/SootSource.h>
#include <gas/SootParticleSource.h>

#include <gas/SootAbsorptionCoeff.h>
#include <gas/RadiativeHeatFlux.h>
#include <gas/HeatFromRadiation.h>

//to implement boundary conditions
#include <expression/BoundaryConditionExpression.h>
#include <expression/MaskedExpression.h>
#include <gas/DensityBCEvaluator.h>
#include <gas/XmomBCEvaluator.h>
#include <gas/YmomBCEvaluator.h>
#include <gas/SpeciesBCEvaluator.h>
#include <gas/TotalInternalEnergyBCEvaluator.h>

//--- SpatialOps ---//
#include <spatialops/OperatorDatabase.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/structured/stencil/StencilBuilder.h>

//---  includes for the parser support ---//
#include <parser/ParseTools.h>

#include <math.h>
#include <algorithm>

using std::string;
using std::cout;
using std::endl;
using namespace Expr;

//--------------------------------------------------------------------

  std::string gaschem_model_name( const GasChemistry model )
  {
    std::string name;
    switch (model){
      case COLD_FLOW         : name="cold_flow";         break;
      case DETAILED_KINETICS : name="detailed_kinetics"; break;
      case FLAME_SHEET       : name="flamesheet";        break;
      case EQUILIBRIUM       : name="equilibrium";       break;
      case INVALID_GASMODEL  : name="INVALID";           break;
    }
    return name;
  }

  GasChemistry gaschem_model( const std::string& name )
  {
    if     ( name == gaschem_model_name( COLD_FLOW         ) ) return COLD_FLOW;
    else if( name == gaschem_model_name( DETAILED_KINETICS ) ) return DETAILED_KINETICS;
    else if( name == gaschem_model_name( FLAME_SHEET       ) ) return FLAME_SHEET;
    else if( name == gaschem_model_name( EQUILIBRIUM       ) ) return EQUILIBRIUM;
    else{
      std::ostringstream msg;
      msg << std::endl
          << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported Gas Chemistry model: '" << name << "'\n\n"
          << " Supported models:"
          << "\n\t" << gaschem_model_name(COLD_FLOW        )
          << "\n\t" << gaschem_model_name(DETAILED_KINETICS)
          << "\n\t" << gaschem_model_name(FLAME_SHEET      )
          << "\n\t" << gaschem_model_name(EQUILIBRIUM      )
          << std::endl;
      throw std::invalid_argument( msg.str() );
    }
    return INVALID_GASMODEL;
  }

//--------------------------------------------------------------------  

RxnDiffusionSetup::RxnDiffusionSetup( const YAML::Node& parser,
                                      PatchT& patch,
                                      GraphCategories& gc,
                                      const bool isRestart,
                                      PCA& pca,
                                      const int nPCs,
                                      const bool hasTarAndSoot )
: patch_( patch ),
  exprFactory_  ( *(gc[ADVANCE_SOLUTION]->exprFactory) ),
  icExprFactory_( *(gc[INITIALIZATION  ]->exprFactory) ),
  integrator_     ( NULL ),
  hasTarAndSoot_  ( hasTarAndSoot ),
  pInf_( parser["Pressure"].as<double>( 101325 ) )
{
  const StringNames& sName = StringNames::self();
  FieldManagerList& fml = patch_.field_manager_list();

  //
  // set the mesh
  //
  const YAML::Node meshpg = parser["mesh"];
  const int    npts = meshpg["npts"  ].as<int   >();
  const double len  = meshpg["length"].as<double>();

  // get wall temperature for radiative heat flux
  const YAML::Node particlespg = parser["Particles"];
  const double wallTemp = particlespg["WallTemperature"].as<double>(500.0);

  // get specification for detailed transport
  const bool detailedTransport = parser["DetailedTransport"].as<bool>( true );
  if(detailedTransport) cout << "********* Detailed transport enabled  *********" << endl;
  else                  cout << "********* Detailed transport disabled *********" << endl;

  {
    typedef SpatialOps::IntVec IntVec;
    std::vector<double> length(3,0); length[0]=len;

    grid_ = new SpatialOps::Grid( SpatialOps::IntVec(npts,1,1), length );

    const double dx = grid_->spacing<SpatialOps::XDIR>();
    SpatialOps::OperatorDatabase& opDB = patch_.operator_database();
    SpatialOps::build_stencils( *grid_, opDB );

    // register fields to hold the mesh.
    FieldMgrSelector<VolField >::type& cellFM = fml.field_manager<VolField >();
    FieldMgrSelector<FaceField>::type& faceFM = fml.field_manager<FaceField>();

    const Expr::Tag xvolTag ( sName.xcoord,     STATE_N );
    const Expr::Tag xsurfTag( sName.xsurfcoord, STATE_N );

    cellFM.register_field( xvolTag, 1  );
    faceFM.register_field( xsurfTag, 1 );

    fml.allocate_fields( patch_.field_info() );

    FaceField& xsurfpts = faceFM.field_ref( xsurfTag );
    VolField& xvolpts   = cellFM.field_ref( xvolTag  );
    grid_->set_coord<SpatialOps::XDIR>( xsurfpts );
    grid_->set_coord<SpatialOps::XDIR>( xvolpts  );

    exprFactory_  .register_expression( new Expr::PlaceHolder<VolField>::Builder(xvolTag) );
    icExprFactory_.register_expression( new Expr::PlaceHolder<VolField>::Builder(xvolTag) );
  }

  setup_cantera( parser["Cantera"] );
  const int nspec = CanteraObjects::number_species();

  solveSpecies_ = true;
  solveEnergy_  = false;
  
  const GasChemistry gaschem = gaschem_model( parser["GasChemistry"].as<std::string>() );
  
  const bool doAdvection = true;

  bool doReaction = false;
  std::string speciesPrefix;
  switch (gaschem) {
    case COLD_FLOW:
      speciesPrefix = "";
      doReaction = false;
      break;
    case DETAILED_KINETICS:
      speciesPrefix = "";
      doReaction  = true;
      break;
    case FLAME_SHEET:
    case EQUILIBRIUM:
      speciesPrefix = sName.flsspecies + "_";
      doReaction = false;
      break;
    case INVALID_GASMODEL:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid selection for gas-phase chemistry model" << std::endl << std::endl;
      throw std::runtime_error( msg.str() );
      break;
  }

  const Expr::TagList prodMassFracs = tag_list( CanteraObjects::species_names(), STATE_N, speciesPrefix );

  //
  // Build the equations that we will solve:
  //
  const bool hasPCSolver = parser["PCSolver"];
  bool standAlonePCSolver = false;
  bool modelStateVars = false;
  bool modelSrcTerms,mapSrcTerms,calculateTempSrc;
  if( hasPCSolver ){
    standAlonePCSolver = parser["PCSolver"]["StandAlone"].as<bool>();
    if( standAlonePCSolver ){
      cout << "********* PC solver is in stand alone mode *********" << endl;
      modelStateVars = true;
      doReaction = false;
    }
  }

  std::vector<Expr::ExpressionID> pcIDs;
  Expr::ExpressionID tempSrcID;
  Expr::ExpressionID stVarsID;
  {
    if( !standAlonePCSolver ){
      // Species equations (n-1 equations)
      for( int i=0; i<nspec-1; ++i ){
        const std::string rxnName = sName.SpeciesSourceTerm + "_" + CanteraObjects::species_name(i);
        const Tag reactionTag = doReaction ? Tag(rxnName,STATE_N) : Tag();
        equations_.push_back( new SpeciesEquation( exprFactory_, doAdvection, i, nspec, gaschem,
                                                   hasPCSolver, detailedTransport, reactionTag ) );
      }
      
      // Total internal energy equation
      equations_.push_back( new TotalInternalEnergyEquation( exprFactory_, doAdvection, gaschem) );

      const Expr::Tag absCoeffTag  ( "gas_absorption_coeff",           Expr::STATE_N );
      const Expr::Tag e0RHSfromRadTag( "rhoE0RHS_gas_rad_to_surr", Expr::STATE_N );

      exprFactory_.register_expression(new Expr::ConstantExpr<VolField>::Builder(absCoeffTag, 0.0));

      exprFactory_.register_expression( new RadiativeHeatFlux<VolField>::
                                        Builder( e0RHSfromRadTag,
                                                 Expr::Tag(sName.temperature, Expr::STATE_N ),
                                                 Expr::Tag(sName.pressure,    Expr::STATE_N ),
                                                 Expr::Tag(sName.xcoord,      Expr::STATE_N ),
                                                 absCoeffTag,
                                                 wallTemp ));

      exprFactory_.attach_dependency_to_expression( e0RHSfromRadTag,
                                                    Tag( sName.rhoE0+"RHS", STATE_N ),
                                                    Expr::SUBTRACT_SOURCE_EXPRESSION );
    }
    
    // Principal Component equations (nPCs equations)
    if( hasPCSolver ){
      try {
        const string pcSrcMarsDataFile = parser["PCSolver"]["marsDataFile"].as<string>();
        const string stateVarMarsDataFile = parser["PCSolver"]["stateVarMarsDataFile"].as<string>();
        const string map2GridDataFile = parser["PCSolver"]["mapToGridDataFile"].as<string>();
        const string pcDiffFluxModel = parser["PCSolver"]["pcDiffFluxModel"].as<string>();
        const bool includeExTempTerm = parser["PCSolver"]["IncludeExTempTerm"].as<bool>();
        modelSrcTerms = parser["PCSolver"]["ModelSourceTerms"].as<bool>();
        mapSrcTerms = parser["PCSolver"]["MapSourceTerms"].as<bool>();
        calculateTempSrc = parser["PCSolver"]["CalculateTempSrc"].as<bool>();
        modelStateVars = parser["PCSolver"]["ModelStateVars"].as<bool>();

        if( modelSrcTerms && mapSrcTerms ){
          std::ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "There is a conflict in your input file. The regression model and the mapping can not be activated at the same time. Please revisit your input file PCSolver part!";
          throw std::runtime_error(errMsg.str());
        }
        if( !modelStateVars && standAlonePCSolver ){
          std::ostringstream errMsg;
          errMsg << "Error in file: " << __FILE__ << " line: " << __LINE__ << "\n" << "There is a conflict in your input file. When the PC Solver is in stand alone mode, it automatically models the state variables with regression and the ModelStateVars can not be false. Please revisit your input file PCSolver part!";
          throw std::runtime_error(errMsg.str());
        }
        if( modelSrcTerms  ) cout << "The Model for PC source terms is activated" << endl;
        if( mapSrcTerms    ) cout << "The Mapping for PC source terms is activated" << endl;
        if( standAlonePCSolver || modelStateVars ) cout << "The regression model for state space variables is activated" << endl;

        MarsGroup pcSrcMG(pcSrcMarsDataFile);
        MarsGroup stVarMG(stateVarMarsDataFile);
        Map2GridVec m2gVec(map2GridDataFile, nPCs);

        for( int i=0; i<nPCs; ++i ){
          std::ostringstream pcSource;
          pcSource << sName.pcSourceTerm << "_" << i;
          const Tag pcSrcTag = Tag(pcSource.str(),STATE_N);
          equations_.push_back( new PCEquation( exprFactory_, doAdvection, i, nPCs, nspec,
                                                pca, pcSrcMG, stVarMG, m2gVec, pcSrcTag, modelSrcTerms, modelStateVars,
                                                mapSrcTerms, includeExTempTerm, calculateTempSrc, standAlonePCSolver, pcDiffFluxModel, pcIDs, stVarsID, tempSrcID) );
        }
        
        // registering thermal conductivity for the stand alone PC solver case
        if( standAlonePCSolver ){
          if( detailedTransport && (gaschem != FLAME_SHEET) && (gaschem != EQUILIBRIUM) ){
            typedef pokitt::ThermalConductivity<VolField>::Builder ThermCond;
            exprFactory_.register_expression( new ThermCond( Tag(sName.thermal_conductivity,STATE_N),
                                                             Tag(sName.temperature,         STATE_N ),
                                                             prodMassFracs,
                                                             Tag(sName.mixtureMW,           STATE_N ) ) );
          }
          else{
            exprFactory_.register_expression( new Expr::ConstantExpr<GradOp::SrcFieldType>::Builder(Tag(sName.thermal_conductivity,STATE_N),0.035) );
          }
          
          // face pressure (for internal energy equation)
          typedef PressureInterp<InterpOp>::Builder PFace;
          exprFactory_.register_expression( new PFace( Tag( sName.pressure+"_face", STATE_N ), Tag( sName.pressure, STATE_N ) ) );
          
        }
      }
      catch( std::exception& e ){
        std::cerr << e.what() <<endl;
        throw std::runtime_error("Error setting PC Solver parameters.\n");
      }
    }

    // density equation
    equations_.push_back( new DensityEquation( exprFactory_, doAdvection) );

    //position equation to correlate temporal and spatial evolution
    equations_.push_back( new PositionEquation( exprFactory_, parser["CoFlowVelocity"].as<double>(0.0) ) );

    // momentum equations
    equations_.push_back( new MomentumEquation( exprFactory_, MomentumEquation::XDIR, doAdvection, gaschem ) );
    equations_.push_back( new MomentumEquation( exprFactory_, MomentumEquation::YDIR, doAdvection, gaschem ) );

    // Tar and soot, and soot particle transport equations
    if( hasTarAndSoot_ ){

      std::cout<<"Registering tar and soot expressions\n";
      const string o2Name("O2");

      const Expr::Tag densityTag       = Tag(sName.density,         STATE_N);
      const Expr::Tag temperatureTag   = Tag(sName.temperature,     STATE_N);
      const Expr::Tag pressureTag      = Tag(sName.pressure,        STATE_N);
      const Expr::Tag mixMWTag         = Tag(sName.mixtureMW,       STATE_N);

      const Expr::Tag yO2Tag           = Tag(o2Name,                STATE_N);
      const Expr::Tag tarTag           = Tag(sName.tarSpecies,      STATE_N);
      const Expr::Tag sootTag          = Tag(sName.soot,            STATE_N);
      const Expr::Tag sootPrtTag       = Tag(sName.sootParticle,    STATE_N);

      const Expr::Tag tarGasRTag       = Tag(sName.tarGasR,         STATE_N);
      const Expr::Tag tarOxRTag        = Tag(sName.tarOxR,          STATE_N);
      const Expr::Tag sootFormRTag     = Tag(sName.sootFormR,       STATE_N);
      const Expr::Tag sootAgRTag       = Tag(sName.sootAgR,         STATE_N);
      const Expr::Tag sootOxRTag       = Tag(sName.sootOxR,         STATE_N);

      const Expr::Tag tarSourceTag     = Tag(sName.tarSourceTerm,   STATE_N);
      const Expr::Tag sootSourceTag    = Tag(sName.sootSourceTerm,  STATE_N);
      const Expr::Tag sootPrtSourceTag = Tag(sName.sootPSourceTerm, STATE_N);

      const Expr::Tag sootAbsCoeffTag  = Tag(sName.sootAbsorpCoeff, STATE_N);
      const Expr::Tag tarAbsCoeffTag   = Tag(sName.tarAbsorpCoeff,  STATE_N);

      const Expr::Tag e0SrcFromSootTag = Tag(sName.e0SrcFromSoot,   STATE_N);
      const Expr::Tag e0SrcFromTarTag  = Tag(sName.e0SrcFromTar,    STATE_N);

      // register expressions required for tar, soot, and soot particle equations
      exprFactory_.register_expression( new TarGasificationRate<VolField>::Builder
                                       ( tarGasRTag,
                                         tarTag,
                                         densityTag,
                                         temperatureTag ));

      /*
       * set tar and soot oxidation rates to zero if gas phase the chemistry model is set to COLD_FLOW.
       */
      if(gaschem == COLD_FLOW){
        exprFactory_.register_expression(new Expr::ConstantExpr<VolField>::Builder( tarOxRTag,  0 ));
        exprFactory_.register_expression(new Expr::ConstantExpr<VolField>::Builder( sootOxRTag, 0 ));
      }
      else{
        exprFactory_.register_expression( new TarOxidationRate<VolField>::Builder
                                          ( tarOxRTag,
                                            densityTag,
                                            yO2Tag,
                                            tarTag,
                                            temperatureTag ));

        exprFactory_.register_expression( new SootOxidationRate<VolField>::Builder
                                          ( sootOxRTag,
                                            yO2Tag,
                                            sootTag,
                                            sootPrtTag,
                                            mixMWTag,
                                            pressureTag,
                                            densityTag,
                                            temperatureTag ));
    }

      exprFactory_.register_expression( new SootFormationRate<VolField>::Builder
                                      ( sootFormRTag,
                                        tarTag,
                                        densityTag,
                                        temperatureTag ));

     exprFactory_.register_expression( new SootAgglomerationRate<VolField>::Builder
                                      ( sootAgRTag,
                                        densityTag,
                                        temperatureTag,
                                        sootTag,
                                        sootPrtTag ));


     exprFactory_.register_expression( new TarSource<VolField>::Builder
                                       ( tarSourceTag,
                                         densityTag,
                                         sootFormRTag,
                                         tarOxRTag,
                                         tarGasRTag ));

     exprFactory_.register_expression( new SootSource<VolField>::Builder
                                       ( sootSourceTag,
                                         densityTag,
                                         sootFormRTag,
                                         sootOxRTag ));

     exprFactory_.register_expression( new SootParticleSource<VolField>::Builder
                                       ( sootPrtSourceTag,
                                         sootFormRTag,
                                         sootOxRTag,
                                         sootAgRTag ));

     exprFactory_.register_expression( new SootAbsorptionCoeff<VolField>::Builder
                                       ( sootAbsCoeffTag,
                                         densityTag,
                                         temperatureTag,
                                         sootTag,
                                         len ));

     // Calculated tar absorption coeff assumes radiative properties are identical to soot
     exprFactory_.register_expression( new SootAbsorptionCoeff<VolField>::Builder
                                       ( tarAbsCoeffTag,
                                         densityTag,
                                         temperatureTag,
                                         tarTag,
                                         len ));

     exprFactory_.register_expression( new RadiativeHeatFlux<VolField>::
                                       Builder( e0SrcFromSootTag,
                                                Expr::Tag(sName.temperature, Expr::STATE_N ),
                                                Expr::Tag(sName.pressure,    Expr::STATE_N ),
                                                Expr::Tag(sName.xcoord,      Expr::STATE_N ),
                                                sootAbsCoeffTag,
                                                wallTemp ));

     exprFactory_.register_expression( new RadiativeHeatFlux<VolField>::
                                       Builder( e0SrcFromTarTag,
                                                Expr::Tag(sName.temperature, Expr::STATE_N ),
                                                Expr::Tag(sName.pressure,    Expr::STATE_N ),
                                                Expr::Tag(sName.xcoord,      Expr::STATE_N ),
                                                tarAbsCoeffTag,
                                                wallTemp ));

     /*
      * Subtract rate of heat radiated (from soot and tar to walls) from the total internal energy RHS.
      */
     exprFactory_.attach_dependency_to_expression( e0SrcFromSootTag,
                                                   Tag( sName.rhoE0+"RHS", STATE_N ),
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );

     exprFactory_.attach_dependency_to_expression( e0SrcFromTarTag,
                                                   Tag( sName.rhoE0+"RHS", STATE_N ),
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );

      equations_.push_back( new TarEquation         ( exprFactory_, doAdvection, tarSourceTag     ));
      equations_.push_back( new SootEquation        ( exprFactory_, doAdvection, sootSourceTag    ));
      equations_.push_back( new SootParticleEquation( exprFactory_, doAdvection, sootPrtSourceTag ));
      
      std::cout<<"finished\n\n";
    }
    else{
      std::cout<<"\nTar and soot models turned OFF\n";
    }
  }

  //register an expression to evaluate gas phase heat capacity....
  typedef pokitt::HeatCapacity_Cp<VolField>::Builder CpMix;
  exprFactory_.register_expression( new CpMix( Tag( sName.heat_capacity, STATE_N),
                                               Tag( sName.temperature, STATE_N ),
                                               prodMassFracs ) );

  //
  //Build and register the expressions for Scalar dissipation and Mixture Fraction
  //
  ExpressionID scaldisID;
  const YAML::Node mfinitialpg = parser["MixtureFraction"];
  if( mfinitialpg ){

    const YAML::Node fuelCompPG = mfinitialpg["FuelComp"];
    const YAML::Node oxidCompPG = mfinitialpg["OxidComp"];

    std::vector<double> fuelMassFracs, oxidMassFracs;
    set_comp( fuelCompPG, fuelMassFracs );
    set_comp( oxidCompPG, oxidMassFracs );
    if( !fuelCompPG["MassFraction"] ){
      mole_to_mass( CanteraObjects::molecular_weights(), fuelMassFracs );
    }

    if( !oxidCompPG["MassFraction"] ){
      mole_to_mass( CanteraObjects::molecular_weights(), oxidMassFracs );
    }

    typedef pokitt::SpeciesToMixtureFraction<VolField>::Builder mixfrac;
    exprFactory_.register_expression( new mixfrac( Tag( sName.mixturefraction, STATE_N ),
                                                   prodMassFracs,
                                                   fuelMassFracs, oxidMassFracs) );

    typedef ScalarDissipationExpr<VolField>::Builder ScalDis;
    scaldisID = exprFactory_.register_expression( new ScalDis( Tag(sName.scalardissipation,    STATE_N),
                                                               Tag(sName.mixturefraction,      STATE_N),
                                                               Tag(sName.thermal_conductivity, STATE_N),
                                                               Tag(sName.density,              STATE_N),
                                                               Tag(sName.heat_capacity,        STATE_N) ) );
  }

  integrator_ = new Expr::TimeStepper( exprFactory_, Expr::SSPRK3, "timestepper", patch_.id() );

  // plug the equations into the time integrator
  const int nghost = 1;
  for( EqnList::iterator ieqn=equations_.begin(); ieqn!=equations_.end(); ++ieqn ){
    integrator_->add_equation<VolField>( (*ieqn)->solution_variable_name(),
                                         (*ieqn)->get_rhs_tag(),
                                         nghost );
  }

  for( int i=0; i<nspec; ++i ){
    const Expr::Tag htag   ( sName.enthalpy+"_"+CanteraObjects::species_name(i), STATE_N );
    const Expr::Tag tempTag( sName.temperature, STATE_N );
    typedef pokitt::SpeciesEnthalpy<VolField>::Builder SpecEnth;
    const ExpressionID enthalpyID = exprFactory_.register_expression( new SpecEnth( htag, tempTag, i ) );
    integrator_->get_tree()->insert_tree(enthalpyID, false);  //insert enthalpy expression in to the tree
  }

  //insert scalar dissipation expression in to expression tree
  //when none of the transported equations RHS depends on any expression we need to insert them in to the tree explicitly
  if( mfinitialpg ) integrator_->get_tree()->insert_tree(scaldisID, false);

  set_charateristic_BC(len, doReaction );  //implement the new boundary conditions

  if( hasPCSolver ){
    if( standAlonePCSolver || modelStateVars )
      integrator_->get_tree()->insert_tree(stVarsID,false);
    const int nPCs = parser["PCSolver"]["nPCs"].as<int>();
    for( int i=0; i<nPCs; ++i ){
      integrator_->get_tree()->insert_tree(pcIDs[i], false);  //insert PC expressions in to the tree
    }
    if( (!modelSrcTerms && !mapSrcTerms) || calculateTempSrc )
      integrator_->get_tree()->insert_tree(tempSrcID,false);
  }

}

//--------------------------------------------------------------------

RxnDiffusionSetup::~RxnDiffusionSetup()
{
  if( integrator_ != NULL ) delete integrator_;

  // wipe out the transport equations
  for( EqnList::iterator ieqn=equations_.begin(); ieqn!=equations_.end(); ++ieqn ){
    delete *ieqn;
  }
}

//--------------------------------------------------------------------

void
RxnDiffusionSetup::parse_species_ic( const YAML::Node& parser )
{
  using namespace std;

  const StringNames& sName = StringNames::self();

  bool haveMoleFrac = false;
  bool foundSpecies = false;

  // look for all species that have initial conditions set on them:
  std::set<int> visitedSpecies;
  const vector<string>& specNames = CanteraObjects::species_names();
  const int nspec = CanteraObjects::number_species();
  cout << "Species initialization for:\n";

  const YAML::Node exprs = parser["BasicExpression"];
  for( YAML::Node::const_iterator iexpr=exprs.begin(); iexpr!=exprs.end(); ++iexpr ){
    const YAML::Node& entry = *iexpr;
    // see if this matches a species name.  If it does, record that.
    const string name = entry["name"].as<string>();

    const vector<string>::const_iterator ispec = find( specNames.begin(), specNames.end(), name );

    if( ispec == specNames.end() ) continue;  // not a species

    cout << "\t" << name << " (species #" << ispec-specNames.begin() << ")\n";
    const bool moleFrac = entry["MoleFraction"];
    if( !foundSpecies ){
      foundSpecies = true;
      haveMoleFrac = moleFrac;
    }
    else{
      if( haveMoleFrac != moleFrac ){
        ostringstream msg;
        msg << "ERROR: species '" << name << "' has a different mole/mass fraction\n"
            <<"\t specification than previously parsed values.\n";
        throw runtime_error(msg.str());
      }
    }
    // stash the species number that we found
    visitedSpecies.insert( ispec - specNames.begin() );
  }

  // set remaining species as zero.
  for( int i=0; i<nspec; ++i ){

    // if we already set this, then skip.
    if( visitedSpecies.count(i) == 1 )  continue;

    // we didn't set it from the input, so set it to zero
    string name = CanteraObjects::species_name(i);
    if( haveMoleFrac ) name += "_MOLEFRAC";
    icExprFactory_.register_expression( new Expr::ConstantExpr<VolField>::Builder( Tag( name, STATE_N ), 0.0 ) );
  }

  if( haveMoleFrac ){
    //register individual species mass fraction expressions...depends on mole fraction expressions
    for( int i=0; i<nspec; ++i ){
      const Expr::TagList xiTags = Expr::tag_list( CanteraObjects::species_names(), STATE_N, "", "_MOLEFRAC" );
      const std::string spnam = CanteraObjects::species_name(i);
      MassFromMoleExpr::Builder* spbuilder = new MassFromMoleExpr::Builder(Tag(spnam,STATE_N),xiTags,i);
      icExprFactory_.register_expression( spbuilder );
    }
  }
}

//--------------------------------------------------------------------

void
RxnDiffusionSetup::set_initial_conditions( const YAML::Node& parser )
{
  using namespace std;
  std::cout<<"RxnDiffusionSetup: setting ICs\n";

  const StringNames& sName = StringNames::self();
  Expr::FieldManagerList& fml = patch_.field_manager_list();
  Expr::FieldMgrSelector<VolField >::type& volFM  = fml.field_manager<VolField>();
  Expr::FieldMgrSelector<FaceField>::type& faceFM = fml.field_manager<FaceField>();

  const Expr::Tag xvolTag( sName.xcoord, STATE_N );

  using namespace SpatialOps;

  if( hasTarAndSoot_ ){
    const Expr::Tag tarTag( sName.tarSpecies, STATE_N );
    VolField& tar = volFM.field_ref( tarTag );

    const Expr::Tag sootTag( sName.soot, STATE_N );
    VolField& soot = volFM.field_ref( sootTag );

    const Expr::Tag sootPrtTag( sName.sootParticle, STATE_N );
    VolField& sootPrt = volFM.field_ref( sootPrtTag );

    tar      <<= 0.0;
    soot     <<= 0.0;
    sootPrt  <<= 0.0;
  }

  std::cout<<"\n\npressure set to "<<pInf_<<" Pa.\n\n";

  // pressure - hard-code pressure for now.
  icExprFactory_.register_expression( new Expr::ConstantExpr<VolField>::Builder( Tag(sName.pressure,STATE_N), pInf_ ) );

  parse_species_ic( parser );
  std::cout<<"finished\n\n";
}

//--------------------------------------------------------------------

void RxnDiffusionSetup::build_inital_tree()
{
  // build the expression tree for the initial conditions
  std::vector<ExpressionID> icID;
  for( EqnList::iterator ieq=equations_.begin(); ieq!=equations_.end(); ++ieq ){
    icID.push_back( (*ieq)->initial_condition( icExprFactory_ ) );
  }
  assert( icID.size() >= equations_.size() );

  try{
    Expr::ExpressionTree icTree( icExprFactory_, patch_.id() );
    for( std::vector<ExpressionID>::const_iterator iid=icID.begin(); iid!=icID.end(); ++iid ){
      icTree.insert_tree( *iid, false );
    }
    icTree.compile_expression_tree();
    FieldManagerList& fml = patch_.field_manager_list();
    icTree.register_fields( fml );
    icTree.lock_fields(fml);
    fml.allocate_fields( patch_.field_info() );
    icTree.bind_fields( fml );
    icTree.bind_operators( patch_.operator_database() );
    icTree.execute_tree();

    std::ofstream fout("icTree.dot",std::ios_base::out);
    icTree.write_tree(fout);
  }
  catch( std::runtime_error& e ){
    std::ostringstream err;
    err << "Error while setting intitial conditions." << endl
        << e.what() << endl;
    throw std::runtime_error( err.str() );
  }
}

//--------------------------------------------------------------------

void
RxnDiffusionSetup::mole_to_mass( const std::vector<double>& mw,
                                 std::vector<double>& spec )
{
  double mmw=0;
  std::vector<double>::iterator isp = spec.begin();
  std::vector<double>::const_iterator imw = mw.begin();
  for( ; isp!=spec.end(); ++isp, ++imw ){
    mmw += *isp * *imw;
  }
  for( isp=spec.begin(), imw=mw.begin(); isp!=spec.end(); ++isp, ++imw ){
    *isp = *isp * *imw / mmw;
  }
}

//--------------------------------------------------------------------

void
RxnDiffusionSetup::set_comp( const YAML::Node& compParser,
                             std::vector<double>& species ) const
{
  using namespace std;
  const vector<string>& specNames = CanteraObjects::species_names();
  BOOST_FOREACH( const string& nam, specNames ){
    species.push_back( compParser[nam].as<double>(0.0) );
  }
}

//--------------------------------------------------------------------

void
RxnDiffusionSetup::setup_cantera( const YAML::Node& parser )
{
  const string inputFile   = parser["InputFile"].as<string>();
  const string inputGroup  = parser["GroupName"].as<string>("");
  const string transOption = parser["transport type"  ].as<string>("Mix");

  try{
    cout << "using cantera file: " << inputFile << endl
         << "         and group: " << inputGroup << endl;
    CanteraObjects::Setup options( transOption, inputFile, inputGroup );

#   ifndef NTHREADS
#    define NTHREADS 1
#   endif
    CanteraObjects::setup_cantera( options, NTHREADS );

    cout << "Number of Reactions: " << CanteraObjects::number_rxns() << endl
         << "Number of Species  : " << CanteraObjects::number_species() << endl;
    cout << "Species Names:" << endl;
    int isp = 0;
    BOOST_FOREACH( const string& nam, CanteraObjects::species_names() ){
      cout << std::left << std::setw(5) << ++isp << std::setw(15) << nam << endl;
    }
    cout << endl;
  }
  catch( Cantera::CanteraError ){
    Cantera::showErrors();
    throw std::runtime_error("Error initializing cantera.\n");
  }
}
//--------------------------------------------------------------------

Expr::TimeStepper&
RxnDiffusionSetup::get_time_stepper()
{
  return *integrator_;
}

//--------------------------------------------------------------------------------
//
//    Set boundary conditions for conserved variables and diffusive fluxes
//    For the characteristic boundary treatment setting the BCs on conserved variables is not needed...however
//            in FV approach if BCs are not set the ghost cells will carry junk values creating unnecessary noise at
//            the
//---------------------------------------------------------------------------------
void
RxnDiffusionSetup::set_bc()
{
  namespace so=SpatialOps;
  std::cout<<"RxnDiffusionSetup: seting BCs for conserved variables\n";

  const StringNames& sName = StringNames::self();
  const int nspec = CanteraObjects::number_species();
  const std::vector<int>& dim = patch_.dim();

  const Expr::FieldAllocInfo& info = patch_.field_info();
  const so::IntVec hasBC( info.bcplus[0], info.bcplus[1], info.bcplus[2] );

  const so::GhostData ghosts(1);  // for now, we hard-code one ghost cell for all field types.
  {
    typedef Expr::ConstantBCOpExpression<VolField,so::XDIR> BCOpExpr;
    typedef BCOpExpr::BCValFieldT BCFieldT;

    const so::BoundaryCellInfo bcInfo = so::BoundaryCellInfo::build<BCFieldT>( hasBC, hasBC );
    const so::MemoryWindow mw = get_window_with_ghost( info.dim, ghosts, bcInfo );
    const so::SpatFldPtr<BCFieldT> bcFieldProto = so::SpatialFieldStore::get_from_window<BCFieldT>( mw, bcInfo, ghosts, CPU_INDEX );

    const std::vector<so::IntVec> maskPoints0( 1, so::IntVec(0,0,0) );
    const std::vector<so::IntVec> maskPointsL( 1, so::IntVec( dim[0], 0, 0 ) );

    BCOpExpr::MaskPtr mask0( new BCOpExpr::MaskType( *bcFieldProto, maskPoints0 ) );
    BCOpExpr::MaskPtr maskL( new BCOpExpr::MaskType( *bcFieldProto, maskPointsL ) );

    // density
    const Expr::Tag densNeumannZeroBCTag0( "density-zeroNeumannBC-left", Expr::STATE_NONE );
    const Expr::Tag densNeumannZeroBCTagL( "density-zeroNeumannBC-right", Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(densNeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
    exprFactory_.register_expression( new BCOpExpr::Builder(densNeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
    exprFactory_.attach_modifier_expression( densNeumannZeroBCTag0, Expr::Tag(sName.density, STATE_NP1) );
    exprFactory_.attach_modifier_expression( densNeumannZeroBCTagL, Expr::Tag(sName.density, STATE_NP1) );

    // rho*u
    const Expr::Tag rhouNeumannZeroBCTag0( "rhou-zeroNeumannBC-left", Expr::STATE_NONE );
    const Expr::Tag rhouNeumannZeroBCTagL( "rhou-zeroNeumannBC-right", Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(rhouNeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
    exprFactory_.register_expression( new BCOpExpr::Builder(rhouNeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
    exprFactory_.attach_modifier_expression( rhouNeumannZeroBCTag0, Expr::Tag(sName.xmom, STATE_NP1) );
    exprFactory_.attach_modifier_expression( rhouNeumannZeroBCTagL, Expr::Tag(sName.xmom, STATE_NP1) );

    // rho*v
    const Expr::Tag rhovNeumannZeroBCTag0( "rhov-zeroNeumannBC-left", Expr::STATE_NONE );
    const Expr::Tag rhovNeumannZeroBCTagL( "rhov-zeroNeumannBC-right", Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(rhovNeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
    exprFactory_.register_expression( new BCOpExpr::Builder(rhovNeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
    exprFactory_.attach_modifier_expression( rhovNeumannZeroBCTag0, Expr::Tag(sName.ymom, STATE_NP1) );
    exprFactory_.attach_modifier_expression( rhovNeumannZeroBCTagL, Expr::Tag(sName.ymom, STATE_NP1) );

    // rho*e0
    const Expr::Tag rhoe0NeumannZeroBCTag0( "rhoe0-zeroNeumannBC-left", Expr::STATE_NONE );
    const Expr::Tag rhoe0NeumannZeroBCTagL( "rhoe0-zeroNeumannBC-right", Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(rhoe0NeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
    exprFactory_.register_expression( new BCOpExpr::Builder(rhoe0NeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
    exprFactory_.attach_modifier_expression( rhoe0NeumannZeroBCTag0, Expr::Tag(sName.rhoE0, STATE_NP1) );
    exprFactory_.attach_modifier_expression( rhoe0NeumannZeroBCTagL, Expr::Tag(sName.rhoE0, STATE_NP1) );

    // species rho*yi
    for( int i=0; i<nspec-1; ++i ){
      const std::string spNam = CanteraObjects::species_name(i);
      const Expr::Tag neumannZeroBCTag0( sName.rhoY + "_" + spNam + "-zeroNeumannBC-left", Expr::STATE_NONE );
      const Expr::Tag neumannZeroBCTagL( sName.rhoY + "_" + spNam + "-zeroNeumannBC-right", Expr::STATE_NONE );
      exprFactory_.register_expression( new BCOpExpr::Builder(neumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
      exprFactory_.register_expression( new BCOpExpr::Builder(neumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
      Expr::Tag rhoytag( sName.rhoY + "_" + spNam, STATE_NP1 );
      exprFactory_.attach_modifier_expression( neumannZeroBCTag0, rhoytag );
      exprFactory_.attach_modifier_expression( neumannZeroBCTagL, rhoytag );
    }

    if( hasTarAndSoot_ ){
      // rho*y_tar
      const Expr::Tag rhoTarNeumannZeroBCTag0( sName.rhoTar + "_" + "-zeroNeumannBC-left",  Expr::STATE_NONE );
      const Expr::Tag rhoTarNeumannZeroBCTagL( sName.rhoTar + "_" + "-zeroNeumannBC-right", Expr::STATE_NONE );
      exprFactory_.register_expression( new BCOpExpr::Builder(rhoTarNeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
      exprFactory_.register_expression( new BCOpExpr::Builder(rhoTarNeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
      Expr::Tag rhoTarTag( sName.rhoTar, STATE_NP1 );
      exprFactory_.attach_modifier_expression( rhoTarNeumannZeroBCTag0, rhoTarTag );
      exprFactory_.attach_modifier_expression( rhoTarNeumannZeroBCTagL, rhoTarTag );

      // rho*y_soot
      const Expr::Tag rhoSootNeumannZeroBCTag0( sName.rhoSoot + "_" + "-zeroNeumannBC-left",  Expr::STATE_NONE );
      const Expr::Tag rhoSootNeumannZeroBCTagL( sName.rhoSoot + "_" + "-zeroNeumannBC-right", Expr::STATE_NONE );
      exprFactory_.register_expression( new BCOpExpr::Builder(rhoSootNeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
      exprFactory_.register_expression( new BCOpExpr::Builder(rhoSootNeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
      Expr::Tag rhoSootTag( sName.rhoSoot, STATE_NP1 );
      exprFactory_.attach_modifier_expression( rhoSootNeumannZeroBCTag0, rhoSootTag );
      exprFactory_.attach_modifier_expression( rhoSootNeumannZeroBCTagL, rhoSootTag );

      // rho*(soot particle)
      const Expr::Tag rhoSootPNeumannZeroBCTag0( sName.rhoSootP + "_" + "-zeroNeumannBC-left",  Expr::STATE_NONE );
      const Expr::Tag rhoSootPNeumannZeroBCTagL( sName.rhoSootP + "_" + "-zeroNeumannBC-right", Expr::STATE_NONE );
      exprFactory_.register_expression( new BCOpExpr::Builder(rhoSootPNeumannZeroBCTag0, mask0, so::NEUMANN, so::MINUS_SIDE, 0.0 ) );
      exprFactory_.register_expression( new BCOpExpr::Builder(rhoSootPNeumannZeroBCTagL, maskL, so::NEUMANN, so::PLUS_SIDE,  0.0 ) );
      Expr::Tag rhoSootPTag( sName.rhoSootP, STATE_NP1 );
      exprFactory_.attach_modifier_expression( rhoSootPNeumannZeroBCTag0, rhoSootPTag );
      exprFactory_.attach_modifier_expression( rhoSootPNeumannZeroBCTagL, rhoSootPTag );
    }

    std::cout<<"finished\n\n";
  }

  // construct boundary conditions for diffusive fluxes
  std::cout<<"RxnDiffusionSetup: DiffFlux BC expressions\n";

  const so::BoundaryCellInfo bcInfo = so::BoundaryCellInfo::build<VolField>( hasBC, hasBC );
  const so::MemoryWindow mw = get_window_with_ghost( info.dim, ghosts, bcInfo );
  const so::SpatFldPtr<VolField> bcFieldProto = so::SpatialFieldStore::get_from_window<VolField>( mw, bcInfo, ghosts, CPU_INDEX );

  std::vector<so::IntVec> maskPoints0( 1, so::IntVec(0,0,0) );
  std::vector<so::IntVec> maskPointsL( 1, so::IntVec( dim[0]-1, 0, 0 ) );

  typedef Expr::ConstantBCOpExpression<FaceField,so::XDIR> BCOpExpr;
  BCOpExpr::MaskPtr mask0( new BCOpExpr::MaskType( *bcFieldProto, maskPoints0 ) );
  BCOpExpr::MaskPtr maskL( new BCOpExpr::MaskType( *bcFieldProto, maskPointsL ) );

  const Expr::Tag zeroFluxBCTag0_tauxx( "tauxx_zeroFluxBC-", Expr::STATE_NONE );
  const Expr::Tag zeroFluxBCTagL_tauxx( "tauxx_zeroFluxBC+", Expr::STATE_NONE );
  exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTag0_tauxx,mask0,so::NEUMANN,so::MINUS_SIDE,0.0) );
  exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTagL_tauxx,maskL,so::NEUMANN,so::PLUS_SIDE, 0.0) );
  exprFactory_.attach_modifier_expression( zeroFluxBCTagL_tauxx, Expr::Tag("tauxx",   STATE_N) );
  exprFactory_.attach_modifier_expression( zeroFluxBCTag0_tauxx, Expr::Tag("tauxx",   STATE_N) );

  const Expr::Tag zeroFluxBCTag0_tauyx( "tauyx_zeroFluxBC-", Expr::STATE_NONE );
  const Expr::Tag zeroFluxBCTagL_tauyx( "tauyx_zeroFluxBC+", Expr::STATE_NONE );
  exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTag0_tauyx,mask0,so::NEUMANN,so::MINUS_SIDE,0.0) );
  exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTagL_tauyx,maskL,so::NEUMANN,so::PLUS_SIDE, 0.0) );
  exprFactory_.attach_modifier_expression( zeroFluxBCTag0_tauyx, Expr::Tag("tauyx",   STATE_N) );
  exprFactory_.attach_modifier_expression( zeroFluxBCTagL_tauyx, Expr::Tag("tauyx",   STATE_N) );

  const Expr::Tag zeroFluxBCTag0_heatflux( "heatflux_zeroFluxBC-", Expr::STATE_NONE );
  const Expr::Tag zeroFluxBCTagL_heatflux( "heatflux_zeroFluxBC+", Expr::STATE_NONE );
  exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTag0_heatflux,mask0,so::NEUMANN,so::MINUS_SIDE,0.0) );
  exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTagL_heatflux,maskL,so::NEUMANN,so::PLUS_SIDE, 0.0) );
  exprFactory_.attach_modifier_expression( zeroFluxBCTag0_heatflux, Expr::Tag("HeatFlux",STATE_N) );
  exprFactory_.attach_modifier_expression( zeroFluxBCTagL_heatflux, Expr::Tag("HeatFlux",STATE_N) );

  for( int i=0; i<nspec; ++i ){
    const Expr::Tag specfluxtag( sName.specDiffFluxX + "_" + CanteraObjects::species_name(i), STATE_N );
    const Expr::Tag zeroFluxBCTag0( "zeroFlux-_" + specfluxtag.name(), Expr::STATE_NONE );
    const Expr::Tag zeroFluxBCTagL( "zeroFlux+_" + specfluxtag.name(), Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTag0,mask0,so::NEUMANN,so::MINUS_SIDE,0.0) );
    exprFactory_.register_expression( new BCOpExpr::Builder(zeroFluxBCTagL,maskL,so::NEUMANN,so::PLUS_SIDE, 0.0) );
    exprFactory_.attach_modifier_expression( zeroFluxBCTag0, specfluxtag );
    exprFactory_.attach_modifier_expression( zeroFluxBCTagL, specfluxtag );
  }

  if ( hasTarAndSoot_ ){
    // tar
    const Expr::Tag tarFluxTag( sName.tarDiffFlux, STATE_N );
    const Expr::Tag tarZeroFluxBCTag0( "zeroFlux-_" + tarFluxTag.name(), Expr::STATE_NONE );
    const Expr::Tag tarZeroFluxBCTagL( "zeroFlux+_" + tarFluxTag.name(), Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(tarZeroFluxBCTag0,mask0,so::NEUMANN,so::MINUS_SIDE,0.0) );
    exprFactory_.register_expression( new BCOpExpr::Builder(tarZeroFluxBCTagL,maskL,so::NEUMANN,so::PLUS_SIDE, 0.0) );
    exprFactory_.attach_modifier_expression( tarZeroFluxBCTag0, tarFluxTag );
    exprFactory_.attach_modifier_expression( tarZeroFluxBCTagL, tarFluxTag );

    // soot
    const Expr::Tag sootFluxTag( sName.sootDiffFlux, STATE_N );
    const Expr::Tag sootZeroFluxBCTag0( "zeroFlux-_" + sootFluxTag.name(), Expr::STATE_NONE );
    const Expr::Tag sootZeroFluxBCTagL( "zeroFlux+_" + sootFluxTag.name(), Expr::STATE_NONE );
    exprFactory_.register_expression( new BCOpExpr::Builder(sootZeroFluxBCTag0,mask0,so::NEUMANN,so::MINUS_SIDE,0.0) );
    exprFactory_.register_expression( new BCOpExpr::Builder(sootZeroFluxBCTagL,maskL,so::NEUMANN,so::PLUS_SIDE, 0.0) );
    exprFactory_.attach_modifier_expression( sootZeroFluxBCTag0, sootFluxTag );
    exprFactory_.attach_modifier_expression( sootZeroFluxBCTagL, sootFluxTag );
  }

  std::cout<<"finished\n\n";
}

// ----------------setting the characteristic boundary conditions--------------------
void
RxnDiffusionSetup::set_charateristic_BC( const double L,
                                         const bool doReaction )
{
  //register the expressions needed for BC's : cv and mixture molecular weight
  // First create tags for all the expressions needed
  std::cout<<"RxnDiffusionSetup: characteristic BC expressions\n";

  using Expr::Tag;

  const int nspec = CanteraObjects::number_species();
  const StringNames& sName = StringNames::self();

  const Tag temperatureTag   ( sName.temperature,  Expr::STATE_N );
  const Tag pressureTag      ( sName.pressure,     Expr::STATE_N );
  const Tag densityTag       ( sName.density,      Expr::STATE_N );
  const Tag xvelTag          ( sName.xvel,         Expr::STATE_N );
  const Tag yvelTag          ( sName.yvel,         Expr::STATE_N );
  const Tag xcoordTag        ( sName.xcoord,       Expr::STATE_N );
  const Tag enthalpyTag      ( sName.enthalpy,     Expr::STATE_N );
  const Tag cpTag            ( sName.heat_capacity,Expr::STATE_N );
  const Tag internalenergyTag( sName.e0,           Expr::STATE_N );
  const TagList speciesTags  = tag_list( CanteraObjects::species_names(), Expr::STATE_N );
  const Tag rratesTag = doReaction ? Tag(sName.SpeciesSourceTerm,Expr::STATE_N) : Tag();

  //register these two expressions
  const Expr::Tag cvTag(sName.cv,Expr::STATE_N);
  const Expr::Tag mixmolweightTag(sName.mixtureMW,Expr::STATE_N);

  typedef pokitt::HeatCapacity_Cv<VolField>::Builder CV;
  exprFactory_.register_expression( new CV( cvTag, temperatureTag, speciesTags ) );

  typedef DensityBCEvaluator<VolField>::Builder densityBCadd;
  const Expr::Tag densBCTag( "densityBCadd", Expr::STATE_N );
  exprFactory_.register_expression( new densityBCadd( densBCTag,      temperatureTag, pressureTag, densityTag,
                                                      xvelTag,        xcoordTag,      cpTag,       cvTag,
                                                      mixmolweightTag, enthalpyTag,    rratesTag,
                                                      CanteraObjects::molecular_weights(),
                                                      nspec, L, pInf_ ) );
  exprFactory_.attach_modifier_expression( densBCTag, Expr::Tag(sName.density+"RHS", Expr::STATE_N) );

  typedef XmomBCEvaluator<VolField>::Builder XmomBCadd;
  const Expr::Tag xmomBCTag("XmomBCadd",Expr::STATE_N);
  exprFactory_.register_expression( new XmomBCadd( xmomBCTag,       temperatureTag, pressureTag, densityTag,
                                                   xvelTag,         xcoordTag,      cpTag,       cvTag,
                                                   mixmolweightTag, enthalpyTag,    rratesTag,
                                                   CanteraObjects::molecular_weights(),
                                                   nspec, L, pInf_ ) );
  exprFactory_.attach_modifier_expression( xmomBCTag, Expr::Tag(sName.xmom+"RHS", Expr::STATE_N) );

  typedef YmomBCEvaluator<VolField>::Builder YmomBCadd;
  const Expr::Tag ymomBCTag("YmomBCadd",Expr::STATE_N);
  exprFactory_.register_expression( new YmomBCadd( ymomBCTag, temperatureTag,  pressureTag, densityTag,
                                                   xvelTag,   yvelTag,         xcoordTag,   cpTag,
                                                   cvTag,     mixmolweightTag, enthalpyTag, rratesTag,
                                                   CanteraObjects::molecular_weights(),
                                                   nspec, L, pInf_ ) );
  exprFactory_.attach_modifier_expression( ymomBCTag, Expr::Tag(sName.ymom+"RHS", Expr::STATE_N) );

  for( int i=0; i<nspec-1; ++i ){
    const std::string spNam = CanteraObjects::species_name( i );
    const string specBCaddnam = "speciesBCadd_" + spNam;
    const Expr::Tag specTag(spNam,Expr::STATE_N);

    typedef SpeciesBCEvaluator<VolField>::Builder speciesBCadd;
    const Expr::Tag specBCTag(specBCaddnam,Expr::STATE_N);
    exprFactory_.register_expression( new speciesBCadd( specBCTag,  temperatureTag,   pressureTag,  densityTag,
                                                        xvelTag,    specTag,          xcoordTag,    cpTag,
                                                        cvTag,      mixmolweightTag,  enthalpyTag,  rratesTag,
                                                        CanteraObjects::molecular_weights(),
                                                        nspec, L, i, pInf_ ) );
    exprFactory_.attach_modifier_expression( specBCTag, Expr::Tag("rhoY_"+spNam+"_RHS", Expr::STATE_N) );
  }


  if( hasTarAndSoot_ ){
    // tar
    const Expr::Tag tarBCTag("tarBCadd",       Expr::STATE_N );
    const Expr::Tag tarTag  ( sName.tarSpecies,Expr::STATE_N );
    const int maxSpecIndex = CanteraObjects::number_species()-1; // jtm,- This is used to guarantee that no reactions are
    // run for tar in Cantera. This needs to be addressed.
    typedef SpeciesBCEvaluator<VolField>::Builder tarBCadd;
    exprFactory_.register_expression( new tarBCadd( tarBCTag,  temperatureTag,   pressureTag,  densityTag,
                                                    xvelTag,    tarTag,          xcoordTag,    cpTag,
                                                    cvTag,      mixmolweightTag,  enthalpyTag,  Tag(),
                                                    CanteraObjects::molecular_weights(),
                                                    nspec, L, maxSpecIndex+1, pInf_ ) );

    exprFactory_.attach_modifier_expression( tarBCTag, Expr::Tag(sName.rhoTar+"RHS", Expr::STATE_N) );


    // soot
    const Expr::Tag sootBCTag("sootBCadd",       Expr::STATE_N );
    const Expr::Tag sootTag  ( sName.soot,       Expr::STATE_N );

    typedef SpeciesBCEvaluator<VolField>::Builder sootBCadd;
    exprFactory_.register_expression( new sootBCadd( sootBCTag,  temperatureTag,   pressureTag,  densityTag,
                                                     xvelTag,    sootTag,          xcoordTag,    cpTag,
                                                     cvTag,      mixmolweightTag,  enthalpyTag,  Tag(),
                                                     CanteraObjects::molecular_weights(),
                                                     nspec, L, maxSpecIndex+1, pInf_ ) );

    exprFactory_.attach_modifier_expression( sootBCTag, Expr::Tag(sName.rhoSoot+"RHS", Expr::STATE_N) );

    // soot particles
    const Expr::Tag sootPBCTag("sootPBCadd",        Expr::STATE_N );
    const Expr::Tag sootPTag  ( sName.sootParticle, Expr::STATE_N );

    typedef SpeciesBCEvaluator<VolField>::Builder sootPBCadd;
    exprFactory_.register_expression( new sootPBCadd( sootPBCTag,  temperatureTag,   pressureTag,  densityTag,
                                                      xvelTag,    sootPTag,          xcoordTag,    cpTag,
                                                      cvTag,      mixmolweightTag,  enthalpyTag,  Tag(),
                                                      CanteraObjects::molecular_weights(),
                                                      nspec, L, maxSpecIndex+1, pInf_ ) );

    exprFactory_.attach_modifier_expression( sootPBCTag, Expr::Tag(sName.rhoSootP+"RHS", Expr::STATE_N) );
  }

  typedef TotalInternalEnergyBCEvaluator<VolField>::Builder energyBCadd;
  const Expr::Tag e0BCTag("e0BCadd",Expr::STATE_N);
  exprFactory_.register_expression( new energyBCadd( e0BCTag,    temperatureTag,  pressureTag,       densityTag,
                                                     xvelTag,    yvelTag,         xcoordTag,         cpTag,
                                                     cvTag,      mixmolweightTag, internalenergyTag, enthalpyTag,
                                                     rratesTag,
                                                     CanteraObjects::molecular_weights(),
                                                     nspec, L, pInf_ ) );
  const Expr::Tag rhoE0RHSTag(sName.rhoE0+"RHS", Expr::STATE_N);
  exprFactory_.attach_modifier_expression( e0BCTag, rhoE0RHSTag );

  std::cout<<"finished\n\n";

}
