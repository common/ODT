#ifndef MassFromMoleExpr_h
#define MassFromMoleExpr_h

#include <expression/ExprLib.h>
#include <pokitt/CanteraObjects.h>

#include <OperatorsAndFields.h>            // defines ODT field types

//====================================================================

/**
 *  @class MassFromMoleExpr
 *  @date May, 2010
 *  @author Naveen Punati
 *  @brief Calculates individual species mass fraction given mole fraction and mixture molecular weight.
 *         Used for Initial conditions
 */
class MassFromMoleExpr : public Expr::Expression< VolField >
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Expr::Tag& result,
             const Expr::TagList& xiTags,
             const int specNum )
    : ExpressionBuilder(result),
      xiT_( xiTags ),
      specNum_(specNum)
    {}
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new MassFromMoleExpr(xiT_,specNum_); }

  private:
    const Expr::TagList xiT_;
    const int specNum_;
  };

  void evaluate();

private:

  MassFromMoleExpr( const Expr::TagList& xiTag, const int specNum );
  ~MassFromMoleExpr();
  const int specNum_;
  DECLARE_VECTOR_OF_FIELDS( VolField, xi_ )
};

//====================================================================

MassFromMoleExpr::
MassFromMoleExpr( const Expr::TagList& xiTags,
                  const int specNum )
  : Expr::Expression<VolField>(),
    specNum_(specNum)
{
  this->create_field_vector_request<VolField>( xiTags, xi_ );
}

//--------------------------------------------------------------------

MassFromMoleExpr::~MassFromMoleExpr()
{}

//--------------------------------------------------------------------

void
MassFromMoleExpr::
evaluate()
{
  using namespace SpatialOps;
  VolField& yi = this->value();

  SpatialOps::SpatFldPtr<VolField> mmw = SpatialOps::SpatialFieldStore::get<VolField>( yi );

  const std::vector<double>& specMW = CanteraObjects::molecular_weights();

  *mmw <<= 0.0;
  const int nspec = CanteraObjects::number_species();
  for( size_t i=0; i<nspec; ++i ){
    const VolField& xi = xi_[i]->field_ref();
    *mmw <<= *mmw + xi * specMW[i];
  }
  const VolField& xi = xi_[specNum_]->field_ref();
  yi <<= xi * specMW[specNum_] / *mmw;
}

#endif // MassFromMoleExpr_h
