#ifndef TarGasificationRate_h
#define TarGasificationRate_h

#include <expression/Expression.h>

/**
 *  @class TarGasificationRate
 *  @author Josh McConnell
 *  @date   January, 2015
 *  @brief Calculates the rate of tar gasification in (kg tar)/(kg tot)-s, which is required to
 *         calculate source terms for the transport equations  of soot
 *         mass, tar mass, and soot particles per volume.
 *
 *  The rate of tar gasification is given as
 * \f[

 *     r_{\mathrm{G,tar}} = y_{\mathrm{tar}} * A_{G,\mathrm{tar}} * exp(-E_{G,\mathrm{tar}} / RT),
 *
 * \f]
 *  <ul>
 *
 *  <li> where \f$ y_{\mathrm{tar}} \f$ is the concentration of tar.
 *
 *  </ul>
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 * @param temp_:           system temperature
 * @param yTar_:           (kg tar)/(kg total)
 * @param A_               (1/s) Arrhenius preexponential factor [1]
 * @param E_               (J/mol) Activation energy             [1]
 * @param gasCon_          J/(mol*K)
 */
template< typename ScalarT >
class TarGasificationRate
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS( ScalarT, yTar_, temp_, density_ )

  const double A_, E_, gasCon_;

    TarGasificationRate( const Expr::Tag& yTarTag,
    		             const Expr::Tag& densityTag,
                         const Expr::Tag& tempTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a TarGasificationRate expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& yTarTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& tempTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag yTarTag_, tempTag_, densityTag_;
  };

  ~TarGasificationRate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
TarGasificationRate<ScalarT>::
TarGasificationRate( const Expr::Tag& yTarTag,
		             const Expr::Tag& densityTag,
                     const Expr::Tag& tempTag )
  : Expr::Expression<ScalarT>(),
    A_     ( 9.77E+10  ),
    E_     ( 286.9E+3  ),
    gasCon_( 8.3144621 )
{
	yTar_    = this-> template create_field_request<ScalarT>( yTarTag    );
	density_ = this-> template create_field_request<ScalarT>( densityTag );
	temp_    = this-> template create_field_request<ScalarT>( tempTag    );
}

//--------------------------------------------------------------------

template< typename ScalarT >
TarGasificationRate<ScalarT>::
~TarGasificationRate()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TarGasificationRate<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TarGasificationRate<ScalarT>::
evaluate()
{
  using namespace SpatialOps;

  ScalarT& result = this->value();

  const ScalarT& yTar    = yTar_   ->field_ref();
  const ScalarT& density = density_->field_ref();
  const ScalarT& temp    = temp_   ->field_ref();

  result <<= density * yTar *  A_ * exp( -E_ / (gasCon_ * temp) );

}

//--------------------------------------------------------------------

template< typename ScalarT >
TarGasificationRate<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& yTarTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& tempTag )
  : ExpressionBuilder( resultTag ),
    yTarTag_   ( yTarTag    ),
    densityTag_( densityTag ),
    tempTag_   ( tempTag    )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
TarGasificationRate<ScalarT>::
Builder::build() const
{
  return new TarGasificationRate<ScalarT>( yTarTag_, densityTag_, tempTag_ );
}


#endif // TarGasificationRate_Expr_h
