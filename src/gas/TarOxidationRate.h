#ifndef TarOxidationRate_h
#define TarOxidationRate_h

#include <expression/Expression.h>


/**
 *  @class TarOxidationRate
 *  @author Josh McConnell
 *  @date   January, 2015
 *  @brief Calculates the rate of tar oxidation in (kg tar)/(m^3-s), which is required to
 *         calculate source terms for the transport equations  of soot
 *         mass, tar mass, and soot particles per volume.
 *
 *  The rate of tar oxidation is given as
 * \f[

 *     r_{\mathrm{O,tar}} = [\mathrm{tar}] [O_{2}]* A_{O,\mathrm{tar}} * exp(-E_{O,\mathrm{tar}} / RT),
 *
 * \f]
 *  <ul>
 *
 *  <li> where \f$ [\mathrm{tar}] \f$ is the concentration of tar.
 *
 *  </ul>
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *  [1]    Josephson, A.J.; Lignell,D.O.; Brown, A.L.; Fletcher, T.H.
 *         "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 2016, 30, 5198-5199
 *
 *
 * @param density_:        system density
 * @param temp_:           system temperature
 * @param yO2_:            (kg O2)/(kg total)
 * @param yTar_:           (kg tar)/(kg total)
 * @param A_               (m^3/kmol-s) Arrhenius preexponential factor [1]
 * @param E_               (J/mol) Activation energy                    [1]
 * @param gasCon_          J/(mol*K)
 * @param o2MW_            (g/mol) molecular weight of O2
 */

template< typename ScalarT >
class TarOxidationRate
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS(ScalarT, density_, yO2_, yTar_, temp_ )
  const double A_, E_, gasCon_;

  /* declare operators associated with this expression here */

    TarOxidationRate( const Expr::Tag& densityTag,
                 const Expr::Tag& yO2Tag,
                 const Expr::Tag& yTarTag,
                 const Expr::Tag& tempTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a TarOxidationRate expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& yO2Tag,
             const Expr::Tag& yTarTag,
             const Expr::Tag& tempTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag densityTag_, yO2Tag_, yTarTag_, tempTag_;
  };

  ~TarOxidationRate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
TarOxidationRate<ScalarT>::
TarOxidationRate( const Expr::Tag& densityTag,
               const Expr::Tag& yO2Tag,
               const Expr::Tag& yTarTag,
               const Expr::Tag& tempTag )
  : Expr::Expression<ScalarT>(),
    A_     ( 6.77e5    ),
    E_     ( 52.3e3    ),
    gasCon_( 8.3144621 )
{
	density_ = this->template create_field_request<ScalarT>( densityTag );
	yO2_     = this->template create_field_request<ScalarT>( yO2Tag     );
	yTar_    = this->template create_field_request<ScalarT>( yTarTag    );
	temp_    = this->template create_field_request<ScalarT>( tempTag    );
}

//--------------------------------------------------------------------

template< typename ScalarT >
TarOxidationRate<ScalarT>::
~TarOxidationRate()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TarOxidationRate<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TarOxidationRate<ScalarT>::
evaluate()
{
	using namespace SpatialOps;

  ScalarT& result = this->value();

  const ScalarT& density = density_->field_ref();
  const ScalarT& yO2     = yO2_    ->field_ref();
  const ScalarT& yTar    = yTar_   ->field_ref();
  const ScalarT& temp    = temp_   ->field_ref();

  result <<= square(density) * yTar * yO2
             * A_ * exp( -E_ / ( gasCon_* temp ) );
}

//--------------------------------------------------------------------

template< typename ScalarT >
TarOxidationRate<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& yO2Tag,
                  const Expr::Tag& yTarTag,
                  const Expr::Tag& tempTag )
  : ExpressionBuilder( resultTag ),
    densityTag_( densityTag ),
    yO2Tag_     ( yO2Tag    ),
    yTarTag_    ( yTarTag   ),
    tempTag_    ( tempTag   )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
TarOxidationRate<ScalarT>::
Builder::build() const
{
  return new TarOxidationRate<ScalarT>( densityTag_, yO2Tag_, yTarTag_,tempTag_ );
}


#endif // TarOxidationRate_h
