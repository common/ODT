/*
 * The MIT License
 *
 * Copyright (c) 2012-2018 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "TarAndSootInfo.h"
#include <string>
#include <sstream>
#include <iostream>


  TarAndSootInfo::TarAndSootInfo():
    tarDiffusivity_     ( 8.6e-6     ),  // (m^2/s), diffusivity of naphthalene in air (303 K, 101 kPa). Caldwell, 1984
    tarHydrogen_        ( 8.         ),
    tarCarbon_          ( 10.        ),
    tarMW_              ( 128.17352  ),
    tarHeatOfFormation_ ( 1.175e+06  ),
    tarHeatOfOxidation_ ( -1.735e+07 ),
    sootDensity_        ( 1950.      ),  // kg/m^3 [1]
    sootCMin_           ( 1000       ),  // (carbon atoms)/(incipient soot particle)
    sootHydrogen_       ( 0.         ),
    sootCarbon_         ( 1.         ),
    sootMW_             ( 12.011     ),
	sootHeatOfFormation_( 0.0        ),
    sootHeatOfOxidation_( -9.2e+06   )
  {
    tarRequiredMolesO2_   = tarHydrogen_/4. + tarCarbon_/2.;
    tarProducedMolesCO_   = tarCarbon_;
    tarProducedMolesH2O_  = tarHydrogen_/2.;

    sootRequiredMolesO2_  = sootHydrogen_/4. + sootCarbon_/2.;
    sootProducedMolesCO_  = sootCarbon_;
    sootProducedMolesH2O_ = sootHydrogen_/2.;

    std::cout << "\n TarAndSootInfo: properties: \n"
              << std::endl
              << "tar hydrogen  : " << tar_hydrogen()            << std::endl
              << "tar carbon    : " << tar_carbon()              << std::endl
              << "tar MW        : " << tar_mw()                  << std::endl
              << "tar req. O2   : " << tar_required_moles_O2()   << std::endl
              << "tar prod. CO  : " << tar_produced_moles_CO()   << std::endl
              << "tar prod. H2O : " << tar_produced_moles_H2O()  << std::endl
              << "tar deltaH_f  : " << tar_heat_of_formation()   << std::endl
              << "tar deltaH_ox : " << tar_heat_of_oxidation()   << std::endl
              << "tar diff coef : " << tar_diffusivity()         << std::endl
              << std::endl
              << "soot hydrogen : " << soot_hydrogen()           << std::endl
              << "soot carbon   : " << soot_carbon()             << std::endl
              << "soot MW       : " << soot_mw()                 << std::endl
              << "soot req. O2  : " << soot_required_moles_O2()  << std::endl
              << "soot prod. CO : " << soot_produced_moles_CO()  << std::endl
              << "soot prod. H2O: " << soot_produced_moles_H2O() << std::endl
              << "soot deltaH_ox: " << soot_heat_of_oxidation()  << std::endl
              << "soot density  : " << soot_density()            << std::endl
              << "soot cMin     : " << soot_cMin()               << std::endl
              << "-------------------------------------------------------\n";
  }

  //------------------------------------------------------------------


  const TarAndSootInfo&
  TarAndSootInfo::self()
  { 
    static const TarAndSootInfo& tsi = TarAndSootInfo();
    return tsi;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_hydrogen() const
  {
    return tarHydrogen_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_carbon() const
  {
    return tarCarbon_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_required_moles_O2() const
  {
    return tarRequiredMolesO2_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_produced_moles_CO() const
  {
    return tarProducedMolesCO_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_produced_moles_H2O() const
  {
    return tarProducedMolesH2O_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_mw() const
  {
    return tarMW_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_diffusivity() const
  {
    return tarDiffusivity_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::tar_heat_of_formation() const
  {
    return tarHeatOfFormation_;
  }

  //------------------------------------------------------------------
  const double&
  TarAndSootInfo::tar_heat_of_oxidation() const
  {
    return tarHeatOfOxidation_;
  }

  //------------------------------------------------------------------


  const double&
  TarAndSootInfo::soot_hydrogen() const
  {
    return sootHydrogen_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_carbon() const
  {
    return sootCarbon_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_required_moles_O2() const
  {
    return sootRequiredMolesO2_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_produced_moles_CO() const
  {
    return sootProducedMolesCO_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_produced_moles_H2O() const
  {
    return sootProducedMolesH2O_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_mw() const
  {
    return sootMW_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_density() const
  {
    return sootDensity_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_cMin()  const
  {
    return sootCMin_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_heat_of_oxidation() const
  {
    return sootHeatOfOxidation_;
  }

  //------------------------------------------------------------------

  const double&
  TarAndSootInfo::soot_heat_of_formation() const
  {
    return sootHeatOfFormation_;
  }

  //------------------------------------------------------------------



/*
 * source for parameters:
 *  [1]    Josephson, A.J.; Lignell,D.O.; Brown, A.L.; Fletcher, T.H.
 *         "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 2016, 30, 5198-5199
 */
