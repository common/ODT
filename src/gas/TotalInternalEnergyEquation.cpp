#include <gas/TotalInternalEnergyEquation.h>

//--- Expressions required for internal energy ---//
#include <gas/HeatFlux.h>
#include <gas/IntEnergyTerms.h>            // for internal energy
#include <gas/TemperatureFromIntEnergy.h>  // for internal energy
#include <gas/Pressure.h>
#include <gas/PrimVar.h>

#include <expression/Functions.h>

#include <StringNames.h>

#include <pokitt/CanteraObjects.h>
#include <pokitt/transport/ThermalCondMix.h>
#include <cantera/IdealGasMix.h>
#include <cantera/transport.h>

using Expr::Tag;
using Expr::STATE_N;

//====================================================================

class RhoE0ICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Tag& result,
             const Tag& tempTag,
             const Tag& pressTag,
             const Tag& xvelTag,
             const Tag& yvelTag,
             const Tag& zvelTag )
      : ExpressionBuilder(result),
        tT_   ( tempTag ),
        pT_   ( pressTag),
        xvelT_( xvelTag ),
        yvelT_( yvelTag ),
        zvelT_( zvelTag )
    {}
    Expr::ExpressionBase* build() const{ return new RhoE0ICExpr( tT_, pT_, xvelT_, yvelT_, zvelT_ ); }
  private:
    const Tag tT_, pT_, xvelT_, yvelT_, zvelT_;
  };

  void evaluate()
  {
    VolField& rhoE0 = this->value();

    std::vector<double> ptSpec( gas_->nSpecies(), 0.0 );

    // pack a vector with species iterators.
    std::vector<VolField::const_iterator> specIVec;
    for( size_t i=0; i<species_.size(); ++i ){
      specIVec.push_back( species_[i]->field_ref().begin() );
    }
    VolField::const_iterator itemp = temp_ ->field_ref().begin();
    VolField::const_iterator ipres = press_->field_ref().begin();
    VolField::const_iterator ixvel = haveX_ ? xvel_->field_ref().begin() : itemp;
    VolField::const_iterator iyvel = haveY_ ? yvel_->field_ref().begin() : itemp;
    VolField::const_iterator izvel = haveZ_ ? zvel_->field_ref().begin() : itemp;
    for( VolField::iterator ire0 = rhoE0.begin();
         ire0!=rhoE0.end();
         ++ire0, ++itemp, ++ipres )
    {
      // calculate kinetic energy
      double ke = 0.0;
      if( haveX_ ){ ke += 0.5 * *ixvel**ixvel;  ++ixvel; }
      if( haveY_ ){ ke += 0.5 * *iyvel**iyvel;  ++iyvel; }
      if( haveZ_ ){ ke += 0.5 * *izvel**izvel;  ++izvel; }

      // extract composition and pack it into a temporary buffer.
      std::vector<VolField::const_iterator>::iterator isp=specIVec.begin();
      const std::vector<VolField::const_iterator>::iterator ispe=specIVec.end();
      std::vector<double>::iterator ipt = ptSpec.begin();
      for( ; isp!=ispe; ++ipt, ++isp ){
        *ipt = **isp;
        ++(*isp);
      }
      gas_->setState_TPY( *itemp, *ipres, &ptSpec[0] );

      const double dens = gas_->density();
      const double e0 = gas_->intEnergy_mass() + ke;
      *ire0 = dens * e0;
    }
  }

  ~RhoE0ICExpr()
  {
    CanteraObjects::restore_gasmix( gas_ );
  }

private:
  RhoE0ICExpr( const Tag tempTag,
               const Tag pressTag,
               const Tag xvelTag,
               const Tag yvelTag,
               const Tag zvelTag )
    : Expr::Expression<VolField>(),
      gas_( CanteraObjects::get_gasmix() ),
      haveX_( xvelTag != Tag() ),
      haveY_( yvelTag != Tag() ),
      haveZ_( zvelTag != Tag() )
  {
    const Expr::TagList specTags = Expr::tag_list( CanteraObjects::species_names(), STATE_N );

    temp_  = this->create_field_request<VolField>( tempTag  );
    press_ = this->create_field_request<VolField>( pressTag );

    if( haveX_ ) xvel_ = this->create_field_request<VolField>( xvelTag );
    if( haveY_ ) yvel_ = this->create_field_request<VolField>( yvelTag );
    if( haveZ_ ) zvel_ = this->create_field_request<VolField>( zvelTag );

    this->create_field_vector_request<VolField>( specTags, species_ );
  }

  Cantera::IdealGasMix* const gas_;
  const bool haveX_, haveY_, haveZ_;
  DECLARE_FIELDS( VolField, temp_, press_, xvel_, yvel_, zvel_ )
  DECLARE_VECTOR_OF_FIELDS( VolField, species_ )
};

//====================================================================

TotalInternalEnergyEquation::
TotalInternalEnergyEquation( Expr::ExpressionFactory& exprFactory,
                             const bool doAdvection,
                             const GasChemistry gaschem )
  : Expr::TransportEquation( StringNames::self().rhoE0,
                             Tag( StringNames::self().rhoE0+"RHS", STATE_N ) )
{
  const bool detailedTransport = true; //false;

  const StringNames& sName = StringNames::self();
  const std::vector<std::string>& spNames = CanteraObjects::species_names();

  const std::string speciesPrefix = (gaschem==FLAME_SHEET || gaschem==EQUILIBRIUM) ? sName.flsspecies+"_" : "";
  const std::string density = (gaschem==FLAME_SHEET || gaschem==EQUILIBRIUM) ? sName.flsdensity : sName.density;
  const Expr::TagList prodMassFracs = Expr::tag_list( spNames, STATE_N, speciesPrefix );

  if( doAdvection ){
    exprFactory.register_expression( new IntEnergySrc<DivOp,InterpOp>::Builder
                                     ( Tag("e0src",STATE_N),
                                       false,
                                       Tag(sName.pressure+"_face",STATE_N),
                                       Tag(sName.xvel+"_advect",  STATE_N),
                                       Tag("tauxx",               STATE_N),
                                       Tag(sName.yvel,            STATE_N),
                                       Tag("tauyx",               STATE_N) ) );

    // face pressure (for internal energy equation)
    typedef PressureInterp<InterpOp>::Builder PFace;
    exprFactory.register_expression( new PFace( Tag( sName.pressure+"_face", STATE_N ), Tag( sName.pressure, STATE_N ) ) );
  }

  if( gaschem != EQUILIBRIUM ){
    // in case of equilibrium calculation temperature expression is registered in SpeciesEquation.cpp
    exprFactory.register_expression( new TemperatureFromE0<VolField>::Builder
                                     ( Tag(sName.temperature,STATE_N),
                                       Tag( sName.e0,        STATE_N ),
                                       Tag( sName.ke,        STATE_N ),
                                       Tag( density,         STATE_N ),
                                       prodMassFracs ) );
  }
  exprFactory.register_expression( new KineticEnergy<VolField>::Builder
                                   ( Tag( sName.ke,   STATE_N ),
                                     Tag( sName.xvel, STATE_N ),
                                     Tag( sName.yvel, STATE_N ) ) );

  exprFactory.register_expression( new PrimVar<VolField>::Builder
                                   ( Tag( sName.e0,      STATE_N ),
                                     Tag( sName.rhoE0,   STATE_N ),
                                     Tag( sName.density, STATE_N ) ));

  exprFactory.register_expression( new HeatFlux<FaceField>::Builder
                                   ( Tag("HeatFlux",                STATE_N),
                                     Tag(sName.thermal_conductivity,STATE_N),
                                     Tag(sName.temperature,         STATE_N),
                                     Tag(sName.specDiffFluxX,       STATE_N),
                                     tag_list( spNames, STATE_N, sName.enthalpy+"_" ) ) );

  typedef pokitt::ThermalConductivity<VolField>::Builder TCond;
  exprFactory.register_expression( new TCond(Tag( sName.thermal_conductivity,STATE_N),
                                             Tag( sName.temperature,         STATE_N ),
                                             prodMassFracs,
                                             Tag( sName.mixtureMW,           STATE_N )) );

  // Build RHS Expression
  {
    typedef ScalarRHS<VolField>  EnergyRHS;
    EnergyRHS::FieldTagInfo fieldTagInfo;

    fieldTagInfo[ EnergyRHS::DIFFUSION_FLUX ] = Tag("HeatFlux",STATE_N);

    if( doAdvection ){
      fieldTagInfo[ EnergyRHS::SOLUTION_VARIABLE ] = Tag(sName.rhoE0,STATE_N );
      fieldTagInfo[ EnergyRHS::ADVECTING_VELOCITY] = Tag(sName.xvel+"_advect",STATE_N );
      fieldTagInfo[ EnergyRHS::SOURCE_TERM       ] = Tag("e0src",STATE_N );
    }

    exprFactory.register_expression( new EnergyRHS::Builder( Tag( sName.rhoE0+"RHS", STATE_N ), fieldTagInfo ) );
  }
}

//--------------------------------------------------------------------

TotalInternalEnergyEquation::~TotalInternalEnergyEquation()
{}

//--------------------------------------------------------------------

void
TotalInternalEnergyEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{}

//--------------------------------------------------------------------

Expr::ExpressionID
TotalInternalEnergyEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const StringNames& sName = StringNames::self();

  const Tag xvelTag( sName.xvel, STATE_N );
  const Tag yvelTag( sName.yvel, STATE_N );
#ifdef THREE_DIMENSIONAL
  const Tag zvelTag( sName.zvel, STATE_N );
#else
  const Tag zvelTag;
#endif

  typedef RhoE0ICExpr::Builder Builder;
  return exprFactory.register_expression( new Builder( Tag( sName.rhoE0,       STATE_N),
                                                       Tag( sName.temperature, STATE_N ),
                                                       Tag( sName.pressure,    STATE_N ),
                                                       xvelTag, yvelTag, zvelTag ) );
}

//--------------------------------------------------------------------
