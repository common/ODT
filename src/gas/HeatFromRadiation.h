/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef HeatFromRadiation_Expr_h
#define HeatFromRadiation_Expr_h

#include <expression/Expression.h>

/**
 *  @class HeatFromRadiation
 *  @author Josh McConnell
 *  @date   March, 2015
 *  @brief Estimates the rate of heat transfer (W/m^3) from the gas to the
 *  domain boundary as:
 * \f$ \frac{A,V}\sigma\epsilon_{\mathrm{gas}}[T_{\mathrm{gas}}^{4}-T_{\mathrm{wall}}^{4}] \f$
 *
 * Division by volume is necessary to convert from extensive to intensive.
 *
 * @param emissivity  : emissivity             (kg/m^3)
 * @param gasTemp     : gas temperature        (K)
 * @param wallTemp    : wall temperature       (K)
 * @param sigma       : Stefan-Boltzman const  W/(m^2*K^4)
 * @param area        : domain surface area    (m^2)
 * @param volume      : domain volume          (m^3)
 */
template< typename FieldT >
class HeatFromRadiation
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, emissivity_, gasTemp_ )
  const double& area_, volume_, wallTemp_;
  const double sigma;

  /* declare any operators associated with this expression here */
  
  HeatFromRadiation( const Expr::Tag& emissivityTag,
                     const Expr::Tag& gasTempTag,
                     const double&    wallTemp,
                     const double&    area,
                     const double&    volume );

public:

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a HeatFromRadiation expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& emissivityTag,
             const Expr::Tag& gasTempTag,
             const double&    wallTemp,
             const double&    area,
             const double&    volume,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag emissivityTag_, gasTempTag_;
    const double area_, volume_, wallTemp_;
  };

  ~HeatFromRadiation(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB ){}
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FieldT >
HeatFromRadiation<FieldT>::

HeatFromRadiation( const Expr::Tag& emissivityTag,
                   const Expr::Tag& gasTempTag,
                   const double&    wallTemp,
                   const double&    area,
                   const double&    volume )
  : Expr::Expression<FieldT>(),
    area_    ( area      ),
    volume_  ( volume    ),
    wallTemp_( wallTemp  ),
    sigma    ( 5.6704e-8 )
{
  emissivity_ = this->template create_field_request<FieldT>( emissivityTag );
  gasTemp_    = this->template create_field_request<FieldT>( gasTempTag    );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
HeatFromRadiation<FieldT>::
evaluate()
{
  FieldT& result = this->value();

  const FieldT& emissivity = emissivity_->field_ref();
  const FieldT& gasTemp    = gasTemp_   ->field_ref();

  result <<= area_/volume_*sigma*emissivity
           * ( pow(gasTemp, 4) - pow(wallTemp_, 4) );
}

//--------------------------------------------------------------------

template< typename FieldT >
HeatFromRadiation<FieldT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& emissivityTag,
                  const Expr::Tag& gasTempTag,
                  const double&    wallTemp,
                  const double&    area,
                  const double&    volume,
                  const int nghost )
  : ExpressionBuilder( resultTag, nghost ),
    emissivityTag_   ( emissivityTag ),
    gasTempTag_      ( gasTempTag    ),
    wallTemp_        ( wallTemp      ),
    area_            ( area          ),
    volume_          ( volume        )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
HeatFromRadiation<FieldT>::
Builder::build() const
{
  return new HeatFromRadiation<FieldT>( emissivityTag_, gasTempTag_, wallTemp_, area_, volume_ );
}


#endif // HeatFromRadiation_Expr_h
