#include <gas/EquilibriumCalculation.h>

//--------------------------------------------------------------------

Equilibrium::Equilibrium(const Expr::Tag yiTag,
                         const Expr::Tag e0tag,
                         const Expr::Tag keTag,
                         const Expr::Tag densitytag)
: Expr::Expression<VolField>(),
  gas_( CanteraObjects::get_gasmix() ),
  nspec_   ( gas_->nSpecies() ),
  pressure_( gas_->pressure() )
{
  const Expr::TagList specTags = Expr::tag_list( CanteraObjects::species_names(), Expr::STATE_N );

  e0_      = this->create_field_request<FieldT>( e0tag      );
  ke_      = this->create_field_request<FieldT>( keTag      );
  density_ = this->create_field_request<FieldT>( densitytag );

  this->create_field_vector_request<VolField>( specTags, species_ );

  eqc_.resize( nspec_ );
  ptSpec_.resize( nspec_ );
}

//--------------------------------------------------------------------

Equilibrium::~Equilibrium()
{
  CanteraObjects::restore_gasmix( gas_ );
}

//--------------------------------------------------------------------

void
Equilibrium::evaluate()
{
  using SpatialOps::operator<<=;
  typedef typename Expr::Expression<FieldT>::ValVec FieldVec;
  FieldVec& eqcomp = this->get_value_vec();

  FieldT& eqTemp = *eqcomp[0];
  FieldT& eqDens = *eqcomp[1];

  const FieldT& e0      = e0_     ->field_ref();
  const FieldT& ke      = ke_     ->field_ref();
  const FieldT& density = density_->field_ref();

  // Zero the values so that the "ghost" cells have zero values in them.
  // This helps for regression testing.
  for( size_t i=2; i<eqcomp.size(); ++i )
    (*eqcomp[i]) <<= 0.0;

  // pack a vector with species iterators.
  specIVec_.clear();
  for( size_t i=0; i<species_.size(); ++i ){
    specIVec_.push_back( species_[i]->field_ref().interior_begin() );
  }

  // pack a vector with equilibrium composition iterators.
  eqIVec_.clear();
  for( size_t i=0; i<eqcomp.size(); ++i ){
    eqIVec_.push_back( eqcomp[i]->interior_begin() );
  }

  FieldT::const_iterator ie0   = e0.interior_begin();
  FieldT::const_iterator ike   = ke.interior_begin();
  FieldT::const_iterator idens = density.interior_begin();

  FieldT::iterator ieqtemp = eqTemp.interior_begin();
  FieldT::iterator ieqdens = eqDens.interior_begin();

  const FieldT::const_iterator ie0e = e0.interior_end();

  double tmprtr = gas_->temperature();
  double densp;
  // loop over each grid point and calculate the reaction rate.
  for(int i=0; ie0!=ie0e; ++ie0, ++i, ++ieqtemp, ++ieqdens ){

    // extract composition and pack it into a temporary buffer.
    std::vector<double>::iterator ipt = ptSpec_.begin();
    for( std::vector<FieldT::const_iterator>::iterator ispec = specIVec_.begin();
        ispec!=specIVec_.end();	++ipt, ++ispec )
    {
      *ipt = **ispec;
      ++(*ispec); // increment iterators for species to the next point
    }


    // calculate the reaction rates.
    const double intEnerg = *ie0 - *ike;

    // gas_->setMassFractions(&ptSpec_[0]);
    // gas_->setState_UV( intEnerg, 1.0/(*irho), 1e-8 );
    // gas_->setState_HP( *ie0, pressure_);

    gas_->setState_TPY( tmprtr, 101325, &ptSpec_[0] );
    gas_->setState_UV( intEnerg, 1.0/ *idens, 1e-8 );
    //int retnSub;
    try {
      const int retnSub = Cantera::equilibrate(*gas_, "HP", 0,  1.0e-7, 50000,100);
      gas_->getMassFractions( &eqc_[0] );

      // Use this value for a initial guess in next step.
      tmprtr = gas_->temperature();
      densp  = gas_->density();

      if (retnSub != 1) {
        //std::cout << "ERROR: EquilibriumCalculation.cpp step failed! - retnSub != 1 \n";
        notcoverged();

      }
    } catch (Cantera::CanteraError) {
      //std::cout << "ERROR: EquilibriumCalculation.cpp step failed! - CanteraError\n";
      notcoverged();
    }
    // unpack the equilinim composition into the result
    std::vector<FieldT::iterator>::iterator ieqcomp = eqIVec_.begin();
    for( std::vector<double>::const_iterator itmp = eqc_.begin();
        itmp!=eqc_.end();
        ++itmp, ++ieqcomp )
    {
      **ieqcomp = *itmp;
      ++(*ieqcomp);  // advance iterator to next grid point.
    }
    *ieqtemp = tmprtr;
    *ieqdens = densp;
  } // point loop

}
//--------------------------------------------------------------------


//--------------------------------------------------------------------
Equilibrium::Builder::Builder( const Expr::TagList result,
                               const Expr::Tag yiTag,
                               const Expr::Tag e0tag,
                               const Expr::Tag keTag,
                               const Expr::Tag densitytag )
: ExpressionBuilder(result),
  yiT_( yiTag ),
  e0t_ ( e0tag ),
  ket_ ( keTag ),
  denst_( densitytag )
{}
//--------------------------------------------------------------------


//--------------------------------------------------------------------
void
Equilibrium::notcoverged()
{
  try {
    const int retnSub = Cantera::equilibrate(*gas_, "TP", 1,  1.0e-5, 100000, 200);
    if( retnSub != 1 ){
      std::cout << " 'I have no intention to converge on the equilibirum cacluation' says Cantera::equilibrate\n";
    }
  }
  catch( Cantera::CanteraError ){
    std::cout << " 'I have no intention to converge on the equilibirum cacluation' says Cantera::equilibrate\n";
  }
}
//--------------------------------------------------------------------
