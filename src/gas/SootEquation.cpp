
#include <gas/SootEquation.h>
#include <gas/TarAndSootInfo.h>
#include <expression/ExprLib.h>     // Base class definition

//--- Expressions required for SootEquation ---//
#include <gas/DiffusiveFlux.h>
#include <gas/SpeciesPrimVar.h>

#include <expression/Functions.h>

#include <StringNames.h>

#include <cantera/IdealGasMix.h>
#include <pokitt/CanteraObjects.h>

using Expr::Tag;
using Expr::STATE_N;
using Expr::STATE_NONE;

//====================================================================

class RhoSootICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Expr::Tag& result )

      : ExpressionBuilder( result )
    {}
    Expr::ExpressionBase* build() const{ return new RhoSootICExpr(); }
  private:

  };

  void evaluate()
  {
	
    using namespace SpatialOps;
    VolField& rhoSoot = this->value();

    rhoSoot <<= 0.0;
  }

  ~RhoSootICExpr(){}

private:
  RhoSootICExpr(  )

    : Expr::Expression<VolField>()

  {}

};
//====================================================================

SootEquation::
SootEquation( Expr::ExpressionFactory& exprFactory,
                 const bool doAdvection,
                 const Expr::Tag sootSourceTag )

  : Expr::TransportEquation( StringNames::self().rhoSoot,
                             get_rhoSootRHS_tag() )
{
  std::cout<<"SootEquation: registering expressions\n";

  const TarAndSootInfo& tarSootInfo = TarAndSootInfo::self();
  const double sootMW = tarSootInfo.soot_mw();

  const StringNames& sName = StringNames::self();

  const Expr::Tag sootTag           = Tag(sName.soot,             STATE_N);
  const Expr::Tag sootOxRateTag     =Tag(sName.sootOxR,           STATE_N);
  const Expr::Tag densityTag        = Tag(sName.density,          STATE_N);
  const Expr::Tag densityRHSTag     = Tag(sName.density + "RHS",  STATE_N);
  const Expr::Tag rhoSootTag        = get_rhoSoot_tag();
  const Expr::Tag o2ConsumptionTag  = Tag("O2srcFromSoot",        STATE_N);
  const Expr::Tag coConsumptionTag  = Tag("COsrcFromSoot",        STATE_N);
  const Expr::Tag h2oConsumptionTag = Tag("H2OsrcFromSoot",       STATE_N);
  const Expr::Tag IntEnergySrcTag   = Tag("rhoE0srcFromSoot",     STATE_N);

  Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();

  const double mwO2  = gas->molecularWeight( gas->speciesIndex("O2" ) );
  const double mwCO  = gas->molecularWeight( gas->speciesIndex("CO" ) );
  const double mwH2O = gas->molecularWeight( gas->speciesIndex("H2O") );

  CanteraObjects::restore_gasmix(gas);

  //-------------------------------------------------------------------------//
  //------------------------------Species Source Terms-----------------------//
  //-------------------------------------------------------------------------//
     // calculate the rate at which O2 and CO source terms due to tar oxidation under the following assumptions:
     // - the empirical formula of soot and naphthalene (C10H8) are equivalent.
     // - oxidation reaction: C10H8 + 7*O2 --> 10*CO + 4*H2O (no CO2 is produced)

    typedef typename DerivedRxnRate<VolField>::Builder RxnRate;

    exprFactory.register_expression( new RxnRate
                                     ( o2ConsumptionTag,
                                       sootMW,
                                       mwO2,
                                       -1.0,
                                       -tarSootInfo.soot_required_moles_O2(),
                                       sootOxRateTag ));

    exprFactory.register_expression( new RxnRate
                                       ( coConsumptionTag,
                                         sootMW,
                                         mwCO,
                                         -1.0,
                                         tarSootInfo.soot_produced_moles_CO(),
                                         sootOxRateTag ));

    exprFactory.register_expression( new RxnRate
                                       ( h2oConsumptionTag,
                                         sootMW,
                                         mwH2O,
                                         -1.0,
                                         tarSootInfo.soot_produced_moles_H2O(),
                                         sootOxRateTag ));

  // The species rates calculated are rates of consumption, so they are subtracted from from their
  // respective RHSs.

  exprFactory.attach_dependency_to_expression( o2ConsumptionTag,
		                                             get_species_rhs_tag( "O2" ),
                                               Expr::SUBTRACT_SOURCE_EXPRESSION );

  exprFactory.attach_dependency_to_expression( coConsumptionTag,
		                                             get_species_rhs_tag( "CO" ),
                                               Expr::SUBTRACT_SOURCE_EXPRESSION );

  exprFactory.attach_dependency_to_expression( h2oConsumptionTag,
                                               get_species_rhs_tag( "H2O" ),
                                               Expr::SUBTRACT_SOURCE_EXPRESSION );

  // Add tar oxidation rate to density RHS
    exprFactory.attach_dependency_to_expression( sootOxRateTag,
                                                 densityRHSTag,
                                                 Expr::ADD_SOURCE_EXPRESSION );

  // calculate contribution to internal energy due to soot combustion and then add it to the internal
  // energy RHS
  const double deltaEsootOx = tarSootInfo.soot_heat_of_oxidation(); //(J/kg soot)

  exprFactory.register_expression( new IntEnergySrcFromRxn<VolField>::Builder
                                     ( IntEnergySrcTag,
                                       deltaEsootOx,
                                       sootOxRateTag ));

  exprFactory.attach_dependency_to_expression( IntEnergySrcTag,
		                                       Expr::Tag( sName.rhoE0+"RHS", STATE_N ),
                                               Expr::SUBTRACT_SOURCE_EXPRESSION );
//-------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------//

  exprFactory.register_expression( new SpeciesPrimVar<VolField>::Builder
                                   ( sootTag,
                                     rhoSootTag,
                                     densityTag ));

  {
    typedef ScalarRHS<VolField>  SootRHS;
    SootRHS::FieldTagInfo fieldTagInfo;

    fieldTagInfo[ SootRHS::SOURCE_TERM        ] = sootSourceTag;

    if( doAdvection ){
      fieldTagInfo[ SootRHS::SOLUTION_VARIABLE  ] = rhoSootTag;
      fieldTagInfo[ SootRHS::ADVECTING_VELOCITY ] = Tag( sName.xvel+"_advect", STATE_N );
    }

    exprFactory.register_expression( new SootRHS::Builder( get_rhoSootRHS_tag(), fieldTagInfo ) );
  }

  std::cout<<"finished\n\n";


}

//--------------------------------------------------------------------

SootEquation::~SootEquation()
{}

//--------------------------------------------------------------------

void
SootEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{

}

//--------------------------------------------------------------------

Expr::ExpressionID
SootEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const Expr::Tag rhoSootTag = get_rhoSoot_tag();

  return exprFactory.register_expression( new RhoSootICExpr::Builder( rhoSootTag ));
}

//--------------------------------------------------------------------
Expr::Tag
SootEquation::
get_rhoSoot_tag( )
{
  return Expr::Tag(StringNames::self().rhoSoot, STATE_N);
}

//--------------------------------------------------------------------
Expr::Tag
SootEquation::
get_rhoSootRHS_tag( )
{
  return Expr::Tag(StringNames::self().rhoSoot+"RHS", STATE_N);
}
//--------------------------------------------------------------------
Expr::Tag
SootEquation::
get_sootDiffFlux_tag( )
{
  return Expr::Tag(StringNames::self().sootDiffFlux, STATE_N);
}
//--------------------------------------------------------------------

Expr::Tag
SootEquation::
get_species_rhs_tag( const std::string& speciesName )

{
  return Expr::Tag( "rhoY_"+speciesName+"_RHS", STATE_N );
}

//--------------------------------------------------------------------
