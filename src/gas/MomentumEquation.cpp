#include <gas/MomentumEquation.h>

#include <StringNames.h>

//--- Expressions for momentum ---//
#include <gas/Stress.h>
#include <gas/AdvectVelocity.h>
#include <gas/Pressure.h>
#include <gas/PrimVar.h>

#include <pokitt/CanteraObjects.h>
#include <pokitt/transport/ViscosityMix.h>
#include <pokitt/thermo/Pressure.h>

using Expr::Tag;
using Expr::STATE_N;

//====================================================================

class MomICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Expr::Tag& result,
             const Tag& rhoTag,
             const Tag& velTag )
      : ExpressionBuilder(result),
        rhoT_(rhoTag), velT_(velTag)
    {}
    Expr::ExpressionBase* build() const{ return new MomICExpr( rhoT_, velT_ ); }
  private:
    const Tag rhoT_, velT_;
  };

  void evaluate()
  {
    using namespace SpatialOps;
    VolField& mom = this->value();
    const VolField& rho = rho_->field_ref();
    const VolField& vel = vel_->field_ref();
    mom <<= rho * vel;
  }

  ~MomICExpr(){}

private:
  MomICExpr( const Tag rhoTag, const Tag velTag )
    : Expr::Expression<VolField>()
  {
    rho_ = this->create_field_request<VolField>(rhoTag);
    vel_ = this->create_field_request<VolField>(velTag);
  }

  DECLARE_FIELDS( VolField, rho_, vel_ )
};

//====================================================================

MomentumEquation::
MomentumEquation( Expr::ExpressionFactory& exprFactory,
                  const Direction dir,
                  const bool doAdvection,
                  const GasChemistry gaschem )
  : Expr::TransportEquation( get_var_name(dir),
                             Expr::Tag( get_var_name(dir) + "RHS", STATE_N ) ),
    trans_( NULL ),
    dir_( dir )
{
  typedef Stress<VolField>::Builder   Tau;
  typedef PrimVar<VolField>::Builder  Vel;
  typedef ScalarRHS<VolField>         MomRHS;

  const StringNames& sName = StringNames::self();
  const bool detailedTransport = true; //false;

  const std::string speciesPrefix = (gaschem==FLAME_SHEET || gaschem==EQUILIBRIUM) ? sName.flsspecies+"_" : "";
  const std::string density       = (gaschem==FLAME_SHEET || gaschem==EQUILIBRIUM) ? sName.flsdensity : sName.density;

  const Expr::TagList speciesTags = Expr::tag_list( CanteraObjects::species_names(), STATE_N, speciesPrefix );

  switch (dir) {

  case XDIR: {
    const Tag tauTag ( "tauxx",    STATE_N );
    const Tag xmomTag( sName.xmom, STATE_N );

    exprFactory.register_expression( new Tau( tauTag,
                                              true,
                                              Tag(sName.xvel,      STATE_N),
                                              Tag(sName.viscosity, STATE_N) ) );

    exprFactory.register_expression( new Vel( Tag( sName.xvel, STATE_N ),
                                              xmomTag,
                                              Tag( sName.density, STATE_N ) ));

    // x Advecting velocity
    typedef AdvectVelocity<InterpOp>::Builder XAdvVel;
    exprFactory.register_expression( new XAdvVel( Tag( sName.xvel+"_advect", STATE_N ),
                                                  Tag( sName.xvel, STATE_N )) );

    // cell pressure
    typedef pokitt::Pressure<VolField>::Builder Press;
    exprFactory.register_expression( new Press( Tag( sName.pressure,    STATE_N ),
                                                Tag( sName.temperature, STATE_N ),
                                                Tag( density,           STATE_N ),
                                                Tag( sName.mixtureMW,   STATE_N ) ) );

    // pressure term for momentum RHS
    //
    // jcs we really should use a temporary variable from
    // SpatialFieldStore internally rather than force storage of this
    // "pressure_face" field.
    typedef PressureRHSTerm<DivOp>::Builder PTerm;
    exprFactory.register_expression( new PTerm( Tag( sName.pressure+"_xmomrhs", STATE_N ),
                                                Tag( sName.pressure+"_face",    STATE_N )) );


    Expr::TagList xMomSrcTags;
    MomRHS::FieldTagInfo fieldTagInfo;
    fieldTagInfo[ MomRHS::DIFFUSION_FLUX ] = Expr::Tag("tauxx", STATE_N);
    if( doAdvection ){
      xMomSrcTags.push_back( Tag( sName.pressure+"_xmomrhs", STATE_N ) );
      fieldTagInfo[ MomRHS::SOLUTION_VARIABLE  ] = xmomTag;
      fieldTagInfo[ MomRHS::ADVECTING_VELOCITY ] = Tag(sName.xvel+"_advect",STATE_N);
    }

    exprFactory.register_expression( new MomRHS::Builder( get_rhs_tag(), fieldTagInfo, xMomSrcTags ) );

    break;
  } // case XDIR

  case YDIR: {
    const Tag tauTag ( "tauyx",    STATE_N );
    const Tag ymomTag( sName.ymom, STATE_N );
    exprFactory.register_expression( new Tau( tauTag,
                                              false,
                                              Tag( sName.yvel,      STATE_N ),
                                              Tag( sName.viscosity, STATE_N )) );

    exprFactory.register_expression( new Vel( Tag( sName.yvel, STATE_N ),
                                              ymomTag,
                                              Tag( sName.density, STATE_N ) ));

    // y Advecting velocity
    typedef AverageAdvectVelocity<VolField>::Builder YAdvVel;
    exprFactory.register_expression( new YAdvVel( Tag( sName.yvel+"_advect", STATE_N ),
                                                  Tag( sName.xcoord, STATE_N ),
                                                  Tag( sName.yvel,   STATE_N )) );

    // RHS
    Expr::TagList yMomSrcTags;
    MomRHS::FieldTagInfo fieldTagInfo;
    fieldTagInfo[ MomRHS::DIFFUSION_FLUX ] = tauTag;

    if( doAdvection ){
      fieldTagInfo[ MomRHS::SOLUTION_VARIABLE  ] = ymomTag;
      fieldTagInfo[ MomRHS::ADVECTING_VELOCITY ] = Tag(sName.xvel+"_advect",STATE_N);
    }
    exprFactory.register_expression( new MomRHS::Builder( get_rhs_tag(), fieldTagInfo, yMomSrcTags ) );

    break;
  } // case YDIR

  case ZDIR:
    assert(0);

  } // switch (dir)


  static bool haveBuiltOneTimeExpressions = false;
  if( !haveBuiltOneTimeExpressions ){
    haveBuiltOneTimeExpressions = true;
    if( detailedTransport ){
      typedef pokitt::Viscosity<VolField>::Builder Visc;
      exprFactory.register_expression( new Visc( Tag( sName.viscosity,   STATE_N ),
                                                 Tag( sName.temperature, STATE_N ),
                                                 speciesTags ) );
    }
    else{
      typedef pokitt::SutherlandViscosity<VolField>::Builder Visc;
      exprFactory.register_expression( new Visc( Tag( sName.viscosity,   STATE_N ),
                                                 Tag( sName.temperature, STATE_N ),
                                                 20.0,      // constant for air (K)
                                                 18.27e-6,  // ref visc for air (Pa s)
                                                 291.15 ) );// ref temp for air (K)
    }
  }
}

//--------------------------------------------------------------------

MomentumEquation::~MomentumEquation()
{
  if( trans_ != NULL ){
    CanteraObjects::restore_transport( trans_ );
    trans_ = NULL;
  }
}

//--------------------------------------------------------------------

void
MomentumEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{}

//--------------------------------------------------------------------

Expr::ExpressionID
MomentumEquation::initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const StringNames& sName = StringNames::self();
  std::string vel="";
  switch( dir_ ){
    case XDIR : vel=sName.xvel; break;
    case YDIR : vel=sName.yvel; break;
    case ZDIR : vel=sName.zvel; break;
  }
  return exprFactory.register_expression( new MomICExpr::Builder( Tag( solnVarName_, STATE_N ),
                                                                  Tag(sName.density,STATE_N),
                                                                  Tag(vel,STATE_N) ) );
}

//--------------------------------------------------------------------

std::string
MomentumEquation::
get_var_name( const Direction dir )
{
  switch( dir ){
    case XDIR:  return StringNames::self().xmom; break;
    case YDIR:  return StringNames::self().ymom; break;
    case ZDIR:  return StringNames::self().zmom; break;
  }
  return "INVALID";  // to quiet compiler warnings.
}
