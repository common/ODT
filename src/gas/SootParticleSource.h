#ifndef SootParticleSource_Expr_h
#define SootParticleSource_Expr_h

#include <expression/Expression.h>
#include <gas/TarAndSootInfo.h>

/**
 *  @class SootParticleSource
 *  @author Josh McConnell
 *  @date   February, 2015
 *  @brief Calculates the source term, in (particles)/(m^3)-s, for soot
 *  particle transport.
 *
 *
 *  @param rSootForm_:      soot formation rate     (kg/m^3-s)
 *  @param rSootOx_  :      soot oxidation rate     (kg/m^3-s)
 *  @param rSootAgg_:       soot agglomeration rate (kg/m^3-s)
 *  @param cMin_:           minimum # of carbon atoms per incipient soot particle
 *  @param carbonAW_        carbon atomic weight
 */
template< typename ScalarT >
class SootParticleSource
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS( ScalarT, rSootForm_, rSootOx_, rSootAgg_ )
  const double cMin_, carbonAW_, na_;

		  /*
		   *   rSootForm_:      soot formation rate (kg/m^3-s)
		   *   rSootOx_  :      soot oxidation rate (kg/m^3-s)
		   *   rSootAgg_ :      soot agglomeration rate (kg/m^3-s)
		   */

    SootParticleSource( const Expr::Tag& rSootFormTag,
                        const Expr::Tag& rSootOxTag,
                        const Expr::Tag& rSootAggTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a SootParticleSource expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& rSootFormTag,
             const Expr::Tag& rSootOxTag,
             const Expr::Tag& rSootAggTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag rSootFormTag_, rSootOxTag_, rSootAggTag_;
  };

  ~SootParticleSource();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
SootParticleSource<ScalarT>::
SootParticleSource( const Expr::Tag& rSootFormTag,
                    const Expr::Tag& rSootOxTag,
                    const Expr::Tag& rSootAggTag )
  : Expr::Expression<ScalarT>(),
    cMin_     ( TarAndSootInfo::self().soot_cMin() ),
    carbonAW_ ( 12.01                              ),
    na_       ( 6.02E+26                           )
{
	rSootForm_ = this-> template create_field_request<ScalarT>( rSootFormTag );
	rSootOx_   = this-> template create_field_request<ScalarT>( rSootOxTag   );
	rSootAgg_  = this-> template create_field_request<ScalarT>( rSootAggTag  );
}

//--------------------------------------------------------------------

template< typename ScalarT >
SootParticleSource<ScalarT>::
~SootParticleSource()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootParticleSource<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootParticleSource<ScalarT>::
evaluate()
{
  using namespace SpatialOps;
    ScalarT& result = this->value();

    const ScalarT& rSootForm = rSootForm_->field_ref();
    const ScalarT& rSootOx   = rSootOx_  ->field_ref();
    const ScalarT& rSootAgg  = rSootAgg_ ->field_ref();

    result <<= (na_ / (cMin_ * carbonAW_) * (rSootForm) - rSootAgg);

}

//--------------------------------------------------------------------

template< typename ScalarT >
SootParticleSource<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& rSootFormTag,
                  const Expr::Tag& rSootOxTag,
                  const Expr::Tag& rSootAggTag )
  : ExpressionBuilder( resultTag ),
    rSootFormTag_( rSootFormTag   ),
    rSootOxTag_  ( rSootOxTag     ),
    rSootAggTag_ ( rSootAggTag    )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
SootParticleSource<ScalarT>::
Builder::build() const
{
  return new SootParticleSource<ScalarT>( rSootFormTag_, rSootOxTag_, rSootAggTag_ );
}


#endif // SootParticleSource_Expr_h
