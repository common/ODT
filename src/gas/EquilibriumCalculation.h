#ifndef EquilibriumCalculation_h
#define EquilibriumCalculation_h

#include <expression/ExprLib.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/IdealGasMix.h>
#include <cantera/equilibrium.h>

#include <OperatorsAndFields.h>

namespace Cantera{ class IdealGasMix; }


/**
 *  @class  Equilibrium
 *  @author Babak Goshayeshi (www.bgoshayeshi.com)
 *  @data   April 2014
 *
 *  
 */
class Equilibrium : public Expr::Expression<VolField>
{
public:
  typedef VolField  FieldT;

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList result,
             const Expr::Tag yiTag,
             const Expr::Tag e0tag,
             const Expr::Tag keTag,
             const Expr::Tag densitytag);

    Expr::ExpressionBase* build() const { return new Equilibrium( yiT_, e0t_, ket_, denst_); };
  private:
    const Expr::Tag yiT_, e0t_, denst_, ket_;
  };

  void evaluate();

  protected:


  Equilibrium( const Expr::Tag yiTag,
               const Expr::Tag e0tag,
               const Expr::Tag keTag,
               const Expr::Tag densitytag );

  ~Equilibrium();

  Equilibrium( const Equilibrium& );
  Equilibrium operator=( const Equilibrium& );

  void notcoverged();

  Cantera::IdealGasMix* gas_;

  const int nspec_;
  const double pressure_;

  DECLARE_FIELDS( FieldT, e0_, density_, ke_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, species_ )

  std::vector<double> eqc_, ptSpec_;
  std::vector< FieldT::const_iterator > specIVec_;
  std::vector< FieldT::iterator > eqIVec_;
};

#endif //EquilibriumCalculation_h
