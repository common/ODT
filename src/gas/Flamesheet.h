#ifndef FlamesheetCalculation_h
#define FlamesheetCalculation_h

#include <expression/ExprLib.h>

#include <OperatorsAndFields.h>

namespace Cantera_CXX{ class IdealGasMix; }


/**
 *  @class  Flamesheet
 *  @author Babak Goshayeshi
 *  @data   Nov 2012
 *
 *  
 */
class Flamesheet
    : public Expr::Expression<VolField>
{
public:
  typedef VolField  FieldT;
  typedef std::vector<const FieldT*> SpecT;
  typedef std::vector<FieldT*> FieldVec;

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     * @param result The product mass fractions
     * @param yiTags The reactant mass fractions
     * @param densTag The density
     */
    Builder( const Expr::TagList& result,
             const Expr::TagList& yiTags,
             const Expr::Tag& densTag );

    Expr::ExpressionBase* build() const { return new Flamesheet( yiT_, denst_); };
  private:
    const Expr::TagList yiT_;
    const Expr::Tag denst_;
  };


  void evaluate();

  protected:

  enum ElementsIndex {
    C  = 0,
    H  = 1,
    N  = 2,
    AR = 3,
    O  = 4
  };

  Flamesheet(const Expr::TagList& yiTags,
             const Expr::Tag& densTag);

  ~Flamesheet();

  Flamesheet( const Flamesheet& );
  Flamesheet operator=( const Flamesheet& );

  double mole_to_mass_frac(const std::vector<double>& mole, std::vector<double>& mass);

  double mass_to_mole_frac(std::vector<double>& mole, const std::vector<double>& mass);

  void normalize_comp(std::vector<double>& comp);

  std::vector< std::vector<double> > elementindex_;
  int nspec_;
  int iCO2_, iH2O_, iN2_, iO2_, iARs_;

  int iC_, iH_, iN_, iAR_, iO_;
  // in case of CPD


  DECLARE_VECTOR_OF_FIELDS( FieldT, species_ )
  DECLARE_FIELD( FieldT, dens_ )

  std::vector<double> mwt_, stioRquiredOxygen_;

  std::vector< FieldT::const_iterator > specIVec_;
  std::vector< FieldT::iterator > prodIVec_;

  double sums_;

};

#endif //FlamesheetCalculation_h
