#ifndef Position_h
#define Position_h

#include <expression/ExprLib.h>


/**
 *  @class Postition
 *  @author James C. Sutherland
 *  @date   Septemner, 2008
 *  @brief Calculates the RHS for the ODE governing a particle's
 *         position, given the advecting velocity and mesh information.
 *
 *  @par Template Parameter
 *   \b FieldT Specifies the type of field that we are integrating.
 *    The velocity field that advects it is the same type of field.
 */
template< typename FieldT >
class Position
  : public Expr::Expression< FieldT >
{
public:

  /**
   *  @class Position::Builder
   *  @brief The mechanism for building a Position object.
   */
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /** @brief construct a Position::Builder
    *  @param velTag - the Expr::Tag for the velocity component.
    *  @param densityTag - the Expr::Tag for the density component.
    *  @param coordTag - the Expr::Tag for the mesh information.
   */
    Builder( const Expr::Tag& result,
             const double,
             const Expr::Tag& velTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& coordTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::Tag velT_, densityT_, coordT_;
    const double coflowb_;
  };

  void evaluate();

protected:

  /**
   *  @brief Construct a Position expression.
   */
  Position( const double,
            const Expr::Tag& velTag,
            const Expr::Tag& densityTag,
            const Expr::Tag& coordTag );

  ~Position(){}

  const double coflow_;
  DECLARE_FIELDS( FieldT, vel_, density_, coord_ )
};




// ###################################################################
//
//                           Implementation
//
// ###################################################################


template<typename FieldT>
Position<FieldT>::Position( const double coflow,
                            const Expr::Tag& velTag,
                            const Expr::Tag& densityTag,
                            const Expr::Tag& coordTag )
  : Expr::Expression<FieldT>(),
    coflow_    ( coflow     )
{
  vel_     = this->template create_field_request<FieldT>( velTag     );
  density_ = this->template create_field_request<FieldT>( densityTag );
  coord_   = this->template create_field_request<FieldT>( coordTag   );
}

//--------------------------------------------------------------------

template<typename FieldT>
void
Position<FieldT>::evaluate()
{
  using SpatialOps::operator<<=;

  const FieldT& vel     = vel_    ->field_ref();
  const FieldT& density = density_->field_ref();
  const FieldT& coord   = coord_  ->field_ref();

  typename FieldT::const_iterator veliterstart  = vel.begin();
  typename FieldT::const_iterator veliterend    = vel.end();
  typename FieldT::const_iterator veliternext   = veliterstart+1;
  typename FieldT::const_iterator densiterstart = density.begin();
  typename FieldT::const_iterator densiternext  = densiterstart+1;
  typename FieldT::const_iterator coorditerstart= coord.begin();
  typename FieldT::const_iterator coorditernext = coorditerstart+1;

  double momflux  = 0.0;
  double massflux = 0.0;
  //applying the trapazoidal rule to compute momentum and mass flux over the domain
  for( ; veliternext!=veliterend; ++veliterstart, ++veliternext, ++densiterstart, ++densiternext, ++coorditerstart, ++coorditernext ){

    double mom_fb = *densiternext * (*veliternext-coflow_) * (*veliternext-coflow_);
    double mom_fa = *densiterstart * (*veliterstart-coflow_) * (*veliterstart-coflow_);

    double mass_fb = *densiternext * (*veliternext-coflow_) ;
    double mass_fa = *densiterstart * (*veliterstart-coflow_) ;

    momflux += 0.5 * (*coorditernext - *coorditerstart) * (mom_fb + mom_fa);
    massflux += 0.5 *(*coorditernext - *coorditerstart) * (mass_fb + mass_fa);
  }

  const double meanvel = coflow_ + momflux/massflux;

  FieldT& pos = this->value();
  pos <<= meanvel;
}

//--------------------------------------------------------------------

template<typename FieldT>
Position<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const double coflowvel,
         const Expr::Tag& velTag,
         const Expr::Tag& densityTag,
         const Expr::Tag& coordTag )
  : ExpressionBuilder(result),
    coflowb_ ( coflowvel  ),
    velT_    ( velTag     ),
    densityT_( densityTag ),
    coordT_  ( coordTag   )
{}

//--------------------------------------------------------------------

template<typename FieldT>
Expr::ExpressionBase*
Position<FieldT>::Builder::build() const
{
  return new Position<FieldT>( coflowb_, velT_, densityT_, coordT_ );
}

//--------------------------------------------------------------------

#endif
