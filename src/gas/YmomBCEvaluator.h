#ifndef YmomBCEvaluator_h
#define YmomBCEvaluator_h

//====================================================================

#include <expression/ExprLib.h>
#include <spatialops/OperatorDatabase.h>

#include <sstream>
#include <cmath>


/**
 *  @class YmomBCEvaluator
 *  @author Naveen Punati
 *
 *  @brief Constructs Boundary condition evaluator for Y-momentum (James Sutherland-2003)
 *         Add the BC's such that acoustic waves can propagate through the domain...nonreflecting
 */
template< typename FieldT >
class YmomBCEvaluator
  : public Expr::Expression<FieldT>
{
  const bool doReaction_;
  const std::vector<double> specMw_;
  const int nspec_;
  const double domainL_;
  const double pInf_; // far-field pressure

  DECLARE_VECTOR_OF_FIELDS( FieldT, enthalpies_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, rrates_     )

  DECLARE_FIELDS( FieldT, temperature_, pressure_, density_, xvel_, yvel_, xcoord_, cp_, cv_, mixmolweight_ )

  double l1LB_,l2LB_,l3LB_,l5LB_;
  double l1RB_,l2RB_,l3RB_,l5RB_;

  void compute_L() ;

  YmomBCEvaluator(const Expr::Tag& temperatureTag,
                  const Expr::Tag& pressureTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& xvelTag,
                  const Expr::Tag& yvelTag,
                  const Expr::Tag& xcoordTag,
                  const Expr::Tag& cpTag,
                  const Expr::Tag& cvTag,
                  const Expr::Tag& mixmolweightTag,
                  const Expr::Tag& enthalpyTag,
                  const Expr::Tag& rratesTag,
                  const std::vector<double>& specMw,
                  const int nspec,
                  const double domainL,
                  const double pInf );

  ~YmomBCEvaluator(){}

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag temperatureT_,pressureT_,densityT_,xvelT_,yvelT_,xcoordT_,cpT_,cvT_,mixmolweightT_,enthalpyT_,rratesT_;
    const std::vector<double> specMw_;
    const int nspec_;
    const double domainL_, pInf_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& pressureTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& xvelTag,
             const Expr::Tag& yvelTag,
             const Expr::Tag& xcoordTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& mixmolweightTag,
             const Expr::Tag& enthalpyTag,
             const Expr::Tag& rratesTag,
             const std::vector<double>& specMw,
             const int nspec,
             const double domainL,
             const double pInf );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################

//--------------------------------------------------------------------
template< typename FieldT >
YmomBCEvaluator<FieldT>::
YmomBCEvaluator( const Expr::Tag& temperatureTag,
                 const Expr::Tag& pressureTag,
                 const Expr::Tag& densityTag,
                 const Expr::Tag& xvelTag,
                 const Expr::Tag& yvelTag,
                 const Expr::Tag& xcoordTag,
                 const Expr::Tag& cpTag,
                 const Expr::Tag& cvTag,
                 const Expr::Tag& mixmolweightTag,
                 const Expr::Tag& enthalpyTag,
                 const Expr::Tag& rratesTag,
                 const std::vector<double>& specMw,
                 const int nspec,
                 const double domainL,
                 const double pInf )
  : Expr::Expression<FieldT>(),
    doReaction_( rratesTag != Expr::Tag() ),
    specMw_    ( specMw  ),
    nspec_     ( nspec   ),
    domainL_   ( domainL ),
    pInf_      ( pInf    )
{
  Expr::TagList specEnthTags, rxnTags;
  for( size_t i=0; i<nspec_; ++i ){
    const std::string suffix = "_" + CanteraObjects::species_name(i);
    specEnthTags.push_back( Expr::Tag( enthalpyTag.name() + suffix, enthalpyTag.context() ) );
    if( doReaction_ ) rxnTags.push_back( Expr::Tag( rratesTag.name() + suffix, rratesTag.context() ) );
  }

  temperature_ = this->template create_field_request<FieldT>( temperatureTag  );
  pressure_    = this->template create_field_request<FieldT>( pressureTag     );
  density_     = this->template create_field_request<FieldT>( densityTag      );
  xvel_        = this->template create_field_request<FieldT>( xvelTag         );
  yvel_        = this->template create_field_request<FieldT>( yvelTag         );
  xcoord_      = this->template create_field_request<FieldT>( xcoordTag       );
  cp_          = this->template create_field_request<FieldT>( cpTag           );
  cv_          = this->template create_field_request<FieldT>( cvTag           );
  mixmolweight_= this->template create_field_request<FieldT>( mixmolweightTag );

  this->template create_field_vector_request<FieldT>( specEnthTags, enthalpies_ );

  if( doReaction_ ) this->template create_field_vector_request<FieldT>( rxnTags, rrates_ );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
YmomBCEvaluator<FieldT>::
evaluate()
{
  using SpatialOps::operator<<=;

  FieldT& rhs = this->value();

  //computing the source terms is bit tricky...
  // 0 1/2 1 3/2 2
  // |-----|-----|---------    --------|-----|-----|
  // |  o  |  *  |                     |  *  |  o  |
  // |-----|-----|---------    --------|-----|-----|
  //  begin begin+1                     end-2  end-1
  //
  // o-ghost value : *-first inner and last inner values
  // We need to set the bounday(1), but the boundary and volume fields are at different locations, so set the first & last inner point values
  // For setting first inner point value use values at begin+1 from all fields....
  // For setting last inner point value use values at end-2 from all fields....

  const FieldT& temperature  = temperature_ ->field_ref();
  const FieldT& pressure     = pressure_    ->field_ref();
  const FieldT& density      = density_     ->field_ref();
  const FieldT& xvel         = xvel_        ->field_ref();
  const FieldT& yvel         = yvel_        ->field_ref();
  const FieldT& xcoord       = xcoord_      ->field_ref();
  const FieldT& cp           = cp_          ->field_ref();
  const FieldT& cv           = cv_          ->field_ref();
  const FieldT& mixmolweight = mixmolweight_->field_ref();

  const double dx = xcoord[2] - xcoord[1] ;
  const double sigma = 0.287; //James Sutherland - 2003

  int eindex = -1;
  typename FieldT::const_iterator iter = xcoord.begin();
  const typename FieldT::const_iterator itere = xcoord.end();
  for( ; iter!=itere; ++iter )
    eindex += 1;

  const double c_LB = sqrt( (cp[1] / cv[1]) * pressure[1] / density[1] );
  const double c_RB = sqrt( (cp[eindex-1] / cv[eindex-1]) * pressure[eindex-1] / density[eindex-1] );

  const double mach_LB = std::fabs(xvel[1] / c_LB) ;
  const double mach_RB = std::fabs(xvel[eindex-1] / c_RB) ;

  //compute l's at the first and last inner points....first order ....James thesis Table 2.4...
  //forward differencing for left boundary
  //backward differencing for right boundary
  compute_L();

  //compute source term...Eq 2.9, 2.10, 2.6, 2.12 and Table 2.3
  const FieldT& nthspecenth = enthalpies_[nspec_-1]->field_ref(); //nth species enthelpy field

  double hdiff_rrates_sum_LB = 0.0 ;
  double hdiff_rrates_sum_RB = 0.0 ;

  for( size_t i=0; i<nspec_-1; ++i ){
    const FieldT& fenth = enthalpies_[i]->field_ref();

    const double hdiff_LB = ( fenth[1] - cp[1]*temperature[1]* (mixmolweight[1]/specMw_[i]) ) -
        ( nthspecenth[1] - cp[1]*temperature[1]*(mixmolweight[1]/specMw_[nspec_-1]) ) ;


    const double hdiff_RB = ( fenth[eindex-1] - cp[eindex-1]*temperature[eindex-1]*(mixmolweight[eindex-1]/specMw_[i]) ) -
        ( nthspecenth[eindex-1] - cp[eindex-1]*temperature[eindex-1]*(mixmolweight[eindex-1]/specMw_[nspec_-1]) ) ;

    if( doReaction_ ){
      const FieldT& frrates = rrates_[i]->field_ref();
      hdiff_rrates_sum_LB += hdiff_LB * frrates[1];
      hdiff_rrates_sum_RB += hdiff_RB * frrates[eindex-1];
    }
  }

  //now calculate the source terms to change the l5 for left boundary and l1 for the right boundary
  const double Sp_LB = (1.0- cp[1] / cv[1] ) * hdiff_rrates_sum_LB ;
  const double Sp_RB = (1.0- cp[eindex-1] / cv[eindex-1] ) * hdiff_rrates_sum_RB ;

  // the following few lines of code is based on James Fortran code....thesis is not clear....
  l5LB_ =  0.5 * Sp_LB + (sigma*c_LB * (1.0-mach_LB*mach_LB) *( pressure[1]-pInf_)) / (2*domainL_) ;
  if( xvel[1] > 0.0){
    l2LB_ = - (1.0/(c_LB * c_LB)) * Sp_LB ;
    l3LB_ = 0.0 ;
    l5LB_ = 0.5 * Sp_LB + 10 * density[1] * c_LB * c_LB * (1 - mach_LB*mach_LB ) * ( xvel[1]- 0.0 ) / (2*domainL_) ;
  }

  l1RB_ =  0.5 * Sp_RB + (sigma*c_RB * (1.0-mach_RB*mach_RB) *( pressure[eindex-1]-pInf_)) / (2*domainL_) ;
  if( xvel[eindex-1] < 0.0){
    l1RB_ = 0.5 * Sp_RB - 10 * density[eindex-1] * c_RB * c_RB * (1 - mach_RB*mach_RB ) * ( xvel[eindex-1]- 0.0 ) / (2*domainL_) ;
    l2RB_ = - (1.0/(c_RB * c_RB)) * Sp_RB ;
    l3RB_ = 0.0 ;
  }
  const double d1_LB = ( 1.0/(c_LB*c_LB) ) * ( c_LB*c_LB*l2LB_ + l5LB_ + l1LB_ );
  const double d4_LB = l3LB_;

  const double d1_RB = ( 1.0/(c_RB*c_RB) ) * ( c_RB*c_RB*l2RB_ + l5RB_ + l1RB_ );
  const double d4_RB = l3RB_;

  const double delta_Fn_LB =  yvel[1] * d1_LB + density[1] * d4_LB ;
  const double delta_Fn_RB =  yvel[eindex-1] * d1_RB + density[eindex-1] * d4_RB ;

  rhs[1]        = -delta_Fn_LB ;
  rhs[eindex-1] = -delta_Fn_RB ;
}
//--------------------------------------------------------------------

template< typename FieldT >
void
YmomBCEvaluator<FieldT>::
compute_L()
{
  const FieldT& temperature  = temperature_ ->field_ref();
  const FieldT& pressure     = pressure_    ->field_ref();
  const FieldT& density      = density_     ->field_ref();
  const FieldT& xvel         = xvel_        ->field_ref();
  const FieldT& yvel         = yvel_        ->field_ref();
  const FieldT& xcoord       = xcoord_      ->field_ref();
  const FieldT& cp           = cp_          ->field_ref();
  const FieldT& cv           = cv_          ->field_ref();
  const FieldT& mixmolweight = mixmolweight_->field_ref();

  int eindex = -1;
  typename FieldT::const_iterator iter = xcoord.begin();
  const typename FieldT::const_iterator itere = xcoord.end();
  for( ; iter!=itere; ++iter )
    eindex += 1;

  const double dx = xcoord[2] - xcoord[1] ;

  const double dpdx_LB_right =  ( pressure[2] - pressure[1] ) / dx ;
  const double drhodx_LB_right =  ( density[2] - density[1] ) / dx ;
  const double dudx_LB_right =  ( xvel[2] - xvel[1] ) / dx ;
  const double dvdx_LB_right =  ( yvel[2] - yvel[1] ) / dx ;

  const double u_LB_right = xvel[1] ;
  const double rho_LB_right = density[1]  ;
  const double pressure_LB_right =  pressure[1]  ;
  const double gamma_LB_right =  cp[1] / cv[1] ;
  const double c_LB_right =  sqrt( (cp[1] / cv[1]) * pressure[1] / density[1] );

  const double l1_LB_right =  ( (u_LB_right-c_LB_right)/2.0 ) * ( dpdx_LB_right - rho_LB_right*c_LB_right*dudx_LB_right) ;
  const double l2_LB_right =  ( u_LB_right / (c_LB_right * c_LB_right) ) * (c_LB_right * c_LB_right *drhodx_LB_right - dpdx_LB_right) ;
  const double l5_LB_right =  ( (u_LB_right+c_LB_right)/2.0 ) * ( dpdx_LB_right + rho_LB_right*c_LB_right*dudx_LB_right) ;
  const double l3_LB_right =  u_LB_right * dvdx_LB_right ;

  l1LB_ = l1_LB_right ;
  l2LB_ = l2_LB_right ;
  l3LB_ = l3_LB_right ;
  l5LB_ = l5_LB_right ;


  const double dpdx_RB_left =  ( pressure[eindex-1] - pressure[eindex-2] ) / dx ;
  const double drhodx_RB_left =  ( density[eindex-1] - density[eindex-2] ) / dx ;
  const double dudx_RB_left =  ( xvel[eindex-1] - xvel[eindex-2] ) / dx ;
  const double dvdx_RB_left =  ( yvel[eindex-1] - yvel[eindex-2] ) / dx ;

  const double u_RB_left =  xvel[eindex-1]  ;
  const double rho_RB_left =  density[eindex-1]  ;
  const double pressure_RB_left =  pressure[eindex-1]  ;	const double gamma_RB_left =  cp[eindex-1] / cv[eindex-1]  ;
  const double c_RB_left =  sqrt( (cp[eindex-1] / cv[eindex-1]) * pressure[eindex-1] / density[eindex-1] ) ;

  const double l1_RB_left  =  ( (u_RB_left-c_RB_left)/2.0 ) * ( dpdx_RB_left - rho_RB_left*c_RB_left*dudx_RB_left) ;
  const double l2_RB_left  =  ( u_RB_left / (c_RB_left * c_RB_left) ) * (c_RB_left * c_RB_left *drhodx_RB_left - dpdx_RB_left) ;
  const double l5_RB_left  =  ( (u_RB_left+c_RB_left)/2.0 ) * ( dpdx_RB_left + rho_RB_left*c_RB_left*dudx_RB_left) ;
  const double l3_RB_left  =  u_RB_left * dvdx_RB_left ;

  l1RB_ = l1_RB_left ;
  l2RB_ = l2_RB_left ;
  l3RB_ = l3_RB_left ;
  l5RB_ = l5_RB_left ;
}

//--------------------------------------------------------------------

template< typename FieldT >
YmomBCEvaluator<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& pressureTag,
         const Expr::Tag& densityTag,
         const Expr::Tag& xvelTag,
         const Expr::Tag& yvelTag,
         const Expr::Tag& xcoordTag,
         const Expr::Tag& cpTag,
         const Expr::Tag& cvTag,
         const Expr::Tag& mixmolweightTag,
         const Expr::Tag& enthalpyTag,
         const Expr::Tag& rratesTag,
         const std::vector<double>& specMw,
         const int nspec,
         const double domainL,
         const double  pInf )
  : ExpressionBuilder(result),
    temperatureT_ ( temperatureTag  ),
    pressureT_    ( pressureTag     ),
    densityT_     ( densityTag      ),
    xvelT_        ( xvelTag         ),
    yvelT_        ( yvelTag         ),
    xcoordT_      ( xcoordTag       ),
    cpT_          ( cpTag           ),
    cvT_          ( cvTag           ),
    mixmolweightT_( mixmolweightTag ),
    enthalpyT_    ( enthalpyTag     ),
    rratesT_      ( rratesTag       ),
    specMw_       ( specMw          ),
    nspec_        ( nspec           ),
    domainL_      ( domainL         ),
    pInf_         ( pInf            )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
YmomBCEvaluator<FieldT>::Builder::
build() const
{
  return new YmomBCEvaluator<FieldT>( temperatureT_, pressureT_, densityT_, xvelT_, yvelT_, xcoordT_, cpT_, cvT_,
                                      mixmolweightT_, enthalpyT_, rratesT_, specMw_, nspec_, domainL_, pInf_ );
}

//--------------------------------------------------------------------



#endif // YmomBCEvaluator_h
