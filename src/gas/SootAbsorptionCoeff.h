/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SootAbsorptionCoeff_Expr_h
#define SootAbsorptionCoeff_Expr_h

#include <expression/Expression.h>
//#include <radprops/Particles.h>

/*
 *  @class SootAbsorptionCoeff
 *  @author Josh McConnell
 *  @date   March, 2015
 *  @brief Calculates Soot absorption coefficient using a model by Hottel and Sarofim.
 *  The model uses the grey gas assumption.
 *
 *  Sarofim, A. F. and Hottel, H. C.,
 *  Radiative heat transfer in combustion chambers: influence of alternative fuels,
 *  In: Heat Transfer, Vol. 6, Hemisphere Publishing Corp., Washington DC, 1978,pp. 199-217.
 *
 *  due to the gas temperature
 *
 *
 * @param density   : system density         (kg/m^3)
 * @param sootDens  : soot density           (kg/m^3)
 * @param beamLength: mean beam length       (m)
 * @param temp      : temperature            (K)
 * @param ySoot     : soot mass fraction     (kg/kg)
 */
template< typename FieldT >
class SootAbsorptionCoeff
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, density_, temp_, ySoot_ )
  const double beamLength_;

  /* declare any operators associated with this expression here */
  
  SootAbsorptionCoeff( const Expr::Tag& densityTag,
                       const Expr::Tag& tempTag,
                       const Expr::Tag& ySootTag,
                       const double& beamLength );
  const double sootDens;
//  ParticleRadCoeffs prtRadCoeffs;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag densityTag_, tempTag_, ySootTag_;
    const double beamLength_;
  public:
    /**
     *  @brief Build a SootAbsorptionCoeff expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& tempTag,
             const Expr::Tag& ySootTag,
             const double& beamLength,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( resultTag, nghost ),
        densityTag_( densityTag ),
        tempTag_( tempTag ),
        ySootTag_( ySootTag ),
        beamLength_( beamLength )
    {}

    Expr::ExpressionBase* build() const{
      return new SootAbsorptionCoeff<FieldT>( densityTag_,tempTag_,ySootTag_, beamLength_ );
    }
  };

  ~SootAbsorptionCoeff(){}
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FieldT >
SootAbsorptionCoeff<FieldT>::

SootAbsorptionCoeff( const Expr::Tag& densityTag,
                     const Expr::Tag& tempTag,
                     const Expr::Tag& ySootTag,
                     const double& beamLength )
  : Expr::Expression<FieldT>(),
    beamLength_( beamLength ),
    sootDens   (1950.0)
{
  density_ = this->template create_field_request<FieldT>( densityTag );
  temp_    = this->template create_field_request<FieldT>( tempTag    );
  ySoot_   = this->template create_field_request<FieldT>( ySootTag   );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
SootAbsorptionCoeff<FieldT>::
evaluate()
{
  FieldT& result = this->value();

  const FieldT& density = density_->field_ref();
  const FieldT& temp    = temp_   ->field_ref();
  const FieldT& ySoot   = ySoot_  ->field_ref();
  //const double absCoeff = prtRadCoeffs.ross_abs_coeff(1.0e-8,1000.0);

  result <<= cond(beamLength_>0,
             4*log(1.0 + 350.0 * ySoot * density / sootDens * temp * beamLength_)/beamLength_)
             (0);
}

//--------------------------------------------------------------------

#endif // SootAbsorptionCoeff_Expr_h
