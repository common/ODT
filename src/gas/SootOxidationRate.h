#ifndef SootOxidationRate_h
#define SootOxidationRate_h

#include <expression/Expression.h>
#include <gas/TarAndSootInfo.h>

#include <math.h> //for pi

/**
 *  @class SootOxidationRate
 *  @author Josh McConnell
 *  @date   January, 2015
 *  @brief Calculates the rate of soot oxidation in (kg soot)/(m^3-s), which is required to
 *         calculate source terms for the transport equations  of soot
 *         mass, tar mass, and soot particles per volume.
 *
 *  The rate of soot formation is given as
 * \f[
 *     SA_{\mathrm{soot}}\frac{P_{\mathrm{O_{2}}}}{T^{1/2}}A_{O,\mathrm{soot}}\exp\left(\frac{-E_{O,\mathrm{soot}}}{RT}\right),
 * \f]
 *  <ul>
 *
 *  <li> where \f$ SA_{\mathrm{soot}} \f$ is the total surface area of the soot particles.
 *
 *  </ul>
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 * source for parameters:
 *  [1]    Josephson, A.J.; Lignell,D.O.; Brown, A.L.; Fletcher, T.H.
 *         "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 2016, 30, 5198-5199
 *
 *
 * @param density_:        system density
 * @param temp_:           system temperature
 * @param yO2_:            (kg O2)/(kg total)
 * @param ySoot_:          (kg soot)/(kg total)
 * @param mixMW_           (g/mol) molecular weight of mixture
 * @param A_               (kgK^0.5/m^2−Pa−s) Arrhenius preexponential factor [1]
 * @param E_               (J/mol) Activation energy                [1]
 * @param gasCon_          J/(mol*K)
 * @param o2MW_            (g/mol) molecular weight of O2
 * @param sootDens         (kg/m^3) soot density                    [1]
 */

template< typename ScalarT >
class SootOxidationRate
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS(ScalarT, yO2_, ySoot_, nSootParticle_,
		         mixMW_, press_, density_, temp_ )
  const double A_, E_, gasCon_, o2MW_, sootDens_;

  /* declare operators associated with this expression here */

    SootOxidationRate( const Expr::Tag& yO2Tag,
                const Expr::Tag& ySootTag,
                const Expr::Tag& nSootParticleTag,
                const Expr::Tag& mixMWTag,
                const Expr::Tag& pressTag,
                const Expr::Tag& densityTag,
                const Expr::Tag& tempTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a SootOxidationRate expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& yO2Tag,
             const Expr::Tag& ySootTag,
             const Expr::Tag& nSootParticleTag,
             const Expr::Tag& mixMWTag,
             const Expr::Tag& pressTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& tempTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag yO2Tag_, ySootTag_, nSootParticleTag_, mixMWTag_, pressTag_, densityTag_, tempTag_;
  };

  ~SootOxidationRate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
SootOxidationRate<ScalarT>::
SootOxidationRate( const Expr::Tag& yO2Tag,
              const Expr::Tag& ySootTag,
              const Expr::Tag& nSootParticleTag,
              const Expr::Tag& mixMWTag,
              const Expr::Tag& pressTag,
              const Expr::Tag& densityTag,
              const Expr::Tag& tempTag )
  : Expr::Expression<ScalarT>(),
    A_       ( 1.09E+5/101325.                       ),
    E_       ( 164.5E+3                              ),
    gasCon_  ( 8.3144621                             ),
    o2MW_    ( 32.0                                  ),
    sootDens_( TarAndSootInfo::self().soot_density() )
{
	yO2_           = this->template create_field_request<ScalarT>( yO2Tag           );
	ySoot_         = this->template create_field_request<ScalarT>( ySootTag         );
	nSootParticle_ = this->template create_field_request<ScalarT>( nSootParticleTag );
	mixMW_         = this->template create_field_request<ScalarT>( mixMWTag         );
	press_         = this->template create_field_request<ScalarT>( pressTag         );
	density_       = this->template create_field_request<ScalarT>( densityTag       );
	temp_          = this->template create_field_request<ScalarT>( tempTag          );
}

//--------------------------------------------------------------------

template< typename ScalarT >
SootOxidationRate<ScalarT>::
~SootOxidationRate()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootOxidationRate<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootOxidationRate<ScalarT>::
evaluate()
{
  using namespace SpatialOps;

  ScalarT& result = this->value();

  const ScalarT& yO2           = yO2_          ->field_ref();
  const ScalarT& ySoot         = ySoot_        ->field_ref();
  const ScalarT& nSootParticle = nSootParticle_->field_ref();
  const ScalarT& mixMW         = mixMW_        ->field_ref();
  const ScalarT& press         = press_        ->field_ref();
  const ScalarT& density       = density_      ->field_ref();
  const ScalarT& temp          = temp_         ->field_ref();

  result <<= density
             * pow( 36.0*M_PI * nSootParticle * square(ySoot / sootDens_), 1.0/3.0 ) // (total surface area of soot particles)/density
             * mixMW/o2MW_ * yO2                                                    // converts O2 from mass to mole fraction
             * press
             / sqrt(temp) * A_ * exp(-E_ / (gasCon_ * temp));

}

//--------------------------------------------------------------------

template< typename ScalarT >
SootOxidationRate<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& yO2Tag,
                  const Expr::Tag& ySootTag,
                  const Expr::Tag& nSootParticleTag,
                  const Expr::Tag& mixMWTag,
                  const Expr::Tag& pressTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& tempTag )
  : ExpressionBuilder( resultTag ),
    yO2Tag_          ( yO2Tag           ),
    ySootTag_        ( ySootTag         ),
    nSootParticleTag_( nSootParticleTag ),
    mixMWTag_        ( mixMWTag         ),
    pressTag_        ( pressTag         ),
    densityTag_      ( densityTag       ),
    tempTag_         ( tempTag          )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
SootOxidationRate<ScalarT>::
Builder::build() const
{
  return new SootOxidationRate<ScalarT>( yO2Tag_, ySootTag_, nSootParticleTag_, mixMWTag_, pressTag_, densityTag_, tempTag_ );
}


#endif // SootOxidationRate_h
