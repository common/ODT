#ifndef TotalInternalEnergy_h
#define TotalInternalEnergy_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types

#include <gas/ScalarRHS.h>

#include <gas/RxnDiffusionSetup.h>

/**
 *  @class  TotalInternalEnergyEquation
 *  @author James C. Sutherland
 *  @date   December, 2008
 *  @brief  Implements the total internal energy transport equation
 */
class TotalInternalEnergyEquation : public Expr::TransportEquation
{
public:

  TotalInternalEnergyEquation( Expr::ExpressionFactory& exprFactory,
                               const bool doAdvection,
                               const GasChemistry gaschem );

  ~TotalInternalEnergyEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );
};


#endif
