#ifndef SootParticleEquation_h
#define SootParticleEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types

#include <gas/ScalarRHS.h>
#include <gas/RxnDiffusionSetup.h>


/**
 *  @class SootParticleEquation
 *  @author Josh McConnell
 *  @date   December, 2014
 *  @brief Calculates the right hand side (RHS) of the transport equation for soot.
 *
 *  The RHS for soot transport is given as
 * \f[
 *
 *     RHS=\\nabla\cdot\left(D_{soot}\nabla Y_{\mathrm{soot}}\right)-\\nabla\cdot\left(\rho_{\mathrm{g}}\vec{u}Y_{\mathrm{soot}}\right)+S_{\mathrm{soot}}.
 *
 * \f]
 *
 *  <li>  \f[Y_{\mathrm{soot}} \f] is the mass fraction of soot,
 *
 *
 *  <li>  \f[vec{u} \f] is the gas velocity,
 *
 *
 *  <li>  \f[ D_{soot} \f] is the mass diffusivity of soot,
 *
 *
 *  <li>  \f[ \rho_{g} \f] is the gas phase density, and
 *
 *
 *  <li>  \f[ S_{mathrm{soot}} \f] is the source term for reactions involving soot.
 *
 *
 *  </ul>
 *
 */
//====================================================================
class SootParticleEquation : public Expr::TransportEquation
{
public:


  SootParticleEquation( Expr::ExpressionFactory& exprFactory,
                    const bool doAdvection,
                    const Expr::Tag sootPSourceTag );


  ~SootParticleEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  static Expr::Tag get_rhoSootP_tag();
  static Expr::Tag get_rhoSootPRHS_tag();

};

#endif
