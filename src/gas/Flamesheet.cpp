#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/IdealGasMix.h>

#include <gas/Flamesheet.h>

//--------------------------------------------------------------------

Flamesheet::Flamesheet( const Expr::TagList& specTags,
                        const Expr::Tag& densTag )
: Expr::Expression<VolField>()
{
  Cantera::IdealGasMix* const gas=CanteraObjects::get_gasmix();
  nspec_    = gas->nSpecies();
  mwt_      = gas->molecularWeights();

  dens_ = this->create_field_request<FieldT>( densTag );
  this->create_field_vector_request<FieldT>( specTags, species_ );

  // To be consistent with every mechanism.
  iC_ = gas->elementIndex( "C" );
  iH_ = gas->elementIndex( "H" );
  iN_ = gas->elementIndex( "N" );
  iO_ = gas->elementIndex( "O" );
  iAR_= gas->elementIndex( "Ar");

  std::string msg;
  if( iC_ == -1 || iH_ == -1 || iN_ == -1 || iO_ == -1 ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "Flamesheet calculation requires C, H, N, Ar and O elements in the mechanism" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  // Product species
  iCO2_ = gas->speciesIndex("CO2");
  iH2O_ = gas->speciesIndex("H2O");
  iN2_  = gas->speciesIndex("N2" );
  iO2_  = gas->speciesIndex("O2" );
  iARs_ = gas->speciesIndex("AR" );

  if( iCO2_ == -1 || iH2O_ == -1 || iN2_ == -1 || iO2_ == -1 || iARs_ * iAR_ < 0 ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "Flamesheet calculation requires CO2, H2O, N2, AR, and O2 species in the mechanism" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  std::vector<double> s_index(5,0);

  elementindex_.clear();
  stioRquiredOxygen_.clear();

  sums_ = 0.0;
  // Species composition
  for( int i=0; i<nspec_; i++ ){

    // making a fixed elements index to be consistent.
    s_index[C ] = gas->nAtoms( i, iC_ );
    s_index[H ] = gas->nAtoms( i, iH_ );
    s_index[N ] = gas->nAtoms( i, iN_ );
    s_index[O ] = gas->nAtoms( i, iO_ );

    if (iAR_ > 0){
      s_index[AR] = gas->nAtoms( i, iAR_);
    }

    elementindex_.push_back(s_index);

    const double ro = std::max(s_index[C] * 2.0 + s_index[H] / 2 - s_index[O],0.0);
    stioRquiredOxygen_.push_back(ro);
    sums_ += ro;
  }
  CanteraObjects::restore_gasmix( gas );
}

//--------------------------------------------------------------------

Flamesheet::~Flamesheet(){}

//--------------------------------------------------------------------

void
Flamesheet::evaluate()
{
  using SpatialOps::operator<<=;
  typedef typename Expr::Expression<FieldT>::ValVec FieldVec;
  FieldVec& flscomp = this->get_value_vec();

  // Zero the values so that the "ghost" cells have zero values in them.
  // This helps for regression testing.
  for( size_t i=0; i<flscomp.size(); ++i )
    (*flscomp[i]) <<= 0.0;

  // pack a vector with species iterators.
  std::vector< FieldT::const_iterator > specIVec_;
  specIVec_.clear();
  for( size_t i=0; i<species_.size(); ++i ){
    specIVec_.push_back( species_[i]->field_ref().begin() );
  }

  // pack a vector with flamesheet products.
  prodIVec_.clear();
  for( size_t i=0; i<flscomp.size(); ++i ){
    prodIVec_.push_back( flscomp[i]->begin() );
  }

  const FieldT& dens = dens_->field_ref();
  FieldT::const_iterator idens = dens.begin();

  for( int nn=0; idens != dens.end() ; ++idens, ++nn ){

    std::vector<double> masscompr(nspec_, 0.0), masscompp(nspec_, 0.0);
    std::vector<double> molecompr(nspec_, 0.0), molecompp(nspec_, 0.0);

    // extract composition and pack it into a temporary buffer.
    std::vector<double>::iterator imassr = masscompr.begin();
    for( std::vector<FieldT::const_iterator>::iterator ispec = specIVec_.begin();
        ispec!=specIVec_.end();
        ++imassr, ++ispec )
    {
      *imassr = **ispec;
      ++(*ispec); // increment iterators for species to the next point
    }

    double mixmw = mass_to_mole_frac( molecompr, masscompr );

    // Change from mole fraction to mole.
    const double concentrationr = *idens / mixmw;
    for (int i=0; i<nspec_; ++i) {
      molecompr[i] *= concentrationr;
    }


    double reqO = 0.0, nC= 0.0, nN = 0.0, nH = 0.0;
    for( int i=0; i<nspec_; ++i ){

      // Required Oxygen to burn all the species
      reqO += stioRquiredOxygen_[i] * molecompr[i];

      nC += elementindex_[i][C] * molecompr[i];
      nH += elementindex_[i][H] * molecompr[i];
      nN += elementindex_[i][N] * molecompr[i];
    }

    const double aO = molecompr[iO2_] * 2.0; // species have been added to O2

    const double stratio = aO/reqO;
    if( stratio >=1.0 ){
      // Products
      molecompp[iCO2_] = nC;
      molecompp[iH2O_] = nH / 2.0;
      molecompp[iN2_ ] = nN / 2.0;
      if( iARs_ > 0 ){
        molecompp[iARs_] = molecompr[iARs_];
      }
      molecompp[iO2_ ] = molecompr[iO2_] - reqO/2.0;
    }
    else {
      double prodC=0.0, prodH=0.0, prodN =0.0, prodAR =0.0;
      for( int i=0; i<nspec_; ++i ){
        if( stioRquiredOxygen_[i] == 0.0 ){
          molecompp[i] = molecompr[i];
          continue;
        }
        const double burned = molecompr[i] * stratio;
        molecompp[i] = molecompr[i] - burned;

        prodC  += burned * elementindex_[i][C ];
        prodH  += burned * elementindex_[i][H ];
        prodN  += burned * elementindex_[i][N ];
      }

      molecompp[iCO2_] = molecompr[iCO2_] + prodC;
      molecompp[iH2O_] = molecompr[iH2O_] + prodH / 2.0;
      molecompp[iN2_ ] = molecompr[iN2_ ] + prodN / 2.0;
      if( iARs_ > 0 ){
        molecompp[iARs_] = molecompr[iARs_];
      }
      
      // And the oxygen is zero !
      molecompp[iO2_ ] = 0.0;
    }

    // Concentration of Product !
    double concentrationp = 0.0;
    for( int i=0; i<nspec_; ++i ){
      concentrationp += molecompp[i];
    }

    // transform from mole to mole fraction,
    // and calculating mixture molecular weight.
    for( int i=0; i<nspec_; ++i ){
      molecompp[i] /= concentrationp;
    }

    mixmw = mole_to_mass_frac( molecompp, masscompp );

    // Density of Product
    const double densp = concentrationp * mixmw;

    // unpack the flamesheet composition into the result
    std::vector<FieldT::iterator>::iterator iflscomp = prodIVec_.begin();

    for( std::vector<double>::const_iterator itmp = masscompp.begin(); itmp!=masscompp.end();
        ++itmp, ++iflscomp ){
      **iflscomp = *itmp;
      ++(*iflscomp);  // advance iterator to next grid point.
    }
    // Adding product density to the results
    **iflscomp = densp;
    ++(*iflscomp);
  } // end for - node
}
//--------------------------------------------------------------------


//--------------------------------------------------------------------
Flamesheet::Builder::Builder( const Expr::TagList& result,
                              const Expr::TagList& yiTag,
                              const Expr::Tag& densTag )
: ExpressionBuilder(result),
  yiT_  ( yiTag   ),
  denst_( densTag )
{}
//--------------------------------------------------------------------

//====================================================================

double
Flamesheet::mole_to_mass_frac( const std::vector<double>& mole,
                               std::vector<double>& mass )
{
  const int ns = (int)mole.size();

  //  assert( ns == (int)mass.size() );
  //  assert( ns == (int)mwt_.size()    );

  double mixMW = 0.0;
  int n;
  for( n=0; n<ns; n++ )
    mixMW += mole[n]*mwt_[n];

  for( n=0; n<ns; n++ )
    mass[n] = mole[n] * mwt_[n] / mixMW;

  return mixMW;
}

//--------------------------------------------------------------------
double
Flamesheet::mass_to_mole_frac( std::vector<double>& mole,
                               const std::vector<double>& mass )
{
  const int ns = (int)mole.size();

  //  assert( ns == (int)mass.size() );
  //  assert( ns == (int)mwt_.size()    );
  double mixMW = 0.0;
  int n;
  for( n=0; n<ns; n++ ){
    mixMW += mass[n]/mwt_[n];
  }
  mixMW = (std::isnormal(1.0/mixMW)) ? 1.0/mixMW : 1.0;

  for( n=0; n<ns; n++ )		
    mole[n] = mixMW * mass[n] / mwt_[n];

  return mixMW;
}

//------------------------------------------------------------------
void
Flamesheet::normalize_comp( std::vector<double>& comp )
{
  double sum=0.0;

  const int ns = (int)comp.size();
  for( int i=0; i<ns; ++i ){
    sum += comp[i];
  }
  for( int i=0; i<ns; ++i ){
    comp[i] /= sum;
  }
}
