#ifndef SpeciesEquation_h
#define SpeciesEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <gas/RxnDiffusionSetup.h>


/**
 *  @class SpeciesEquation
 *  @author James C. Sutherland
 *  @date December, 2008
 *
 *  @brief Defines a species transport equation, along with the
 *         expressions required by it.
 *
 *  @param modelStateSpaceVars : A boolean which shows wheter the species composiotions are
 *                               calculated from the transport equations (false) or are extracted
 *                               from the PC solutions.
 *  @param haPCSolver : a Boolean which shows that the PCSolver is turned on or not. If it is,
 *                      then the mass based diffuision coefficient should be used for the 
 *                      diffusion flux calculation (This is because of the inconsistancy that we found in the 
 *                      results when using the mass or molar based diffusion coefficients extracted from 
 *                      Cantera).
 *  @todo Need a methodology for setting options (configuring) this equation
 */
class SpeciesEquation : public Expr::TransportEquation
{
public:

  SpeciesEquation( Expr::ExpressionFactory& exprFactory,
                   const bool doAdvection,
                   const int specNum,
                   const int numSpecies,
                   const GasChemistry gaschem,
                   const bool haPCSolver,
                   const bool detailedTransport,
                   const Expr::Tag reactionTag = Expr::Tag());

  ~SpeciesEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  static Expr::Tag get_rhs_tag( const int specNum );

  static std::string get_var_name( const int specNum );

  // disallow copy and assignment.
  SpeciesEquation( const SpeciesEquation& );
  SpeciesEquation operator=( const SpeciesEquation& );  

  const int specNum_,nspecies_;
};


#endif // SpeciesEquation_h
