#include <gas/SpeciesEquation.h>
#include <gas/SpeciesDiffusivityFromLewisNumber.h>
#include <pokitt/CanteraObjects.h> // include cantera wrapper

#include <OperatorsAndFields.h>  // defines field types

//--- Expressions for Species related expressions ---//
#include <gas/Flamesheet.h>
#include <gas/EquilibriumCalculation.h>
#include <gas/SpeciesDiffusion.h>
#include <gas/SpeciesPrimVar.h>
#include <gas/ScalarRHS.h>

#include <pokitt/MixtureMolWeight.h>
#include <pokitt/SpeciesN.h>
#include <pokitt/transport/DiffusionCoeffMix.h>
#include <pokitt/kinetics/ReactionRates.h>

//--- String Names ---//
#include <StringNames.h>

#include <stdexcept>

using Expr::Tag;
using Expr::STATE_N;
using std::string;
using std::cout;
using std::endl;

//====================================================================

class RhoYiICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Tag& result,
             const Tag& rhoTag,
             const Tag& yiTag )
      : ExpressionBuilder(result),
        rhoT_(rhoTag), yiT_(yiTag)
    {}
    Expr::ExpressionBase* build() const{ return new RhoYiICExpr( rhoT_, yiT_ ); }
  private:
    const Tag rhoT_, yiT_;
  };

  void evaluate()
  {
    using namespace SpatialOps;
    VolField& rhoYi = this->value();
    const VolField& rho = rho_->field_ref();
    const VolField& yi  = yi_ ->field_ref();
    rhoYi <<= rho * yi;
  }

  ~RhoYiICExpr(){}

private:
  RhoYiICExpr( const Tag rhoTag, const Tag yiTag )
    : Expr::Expression<VolField>()
  {
    yi_  = this->create_field_request<VolField>( yiTag );
    rho_ = this->create_field_request<VolField>( rhoTag );
  }
  DECLARE_FIELDS( VolField, rho_, yi_ )
};


//====================================================================

SpeciesEquation::
SpeciesEquation( Expr::ExpressionFactory& exprFactory,
                 const bool doAdvection,
                 const int specNum,
                 const int numSpecies,
                 const GasChemistry gaschem,
                 const bool hasPCSolver,
                 const bool detailedTransport,
                 const Tag reactionTag )
  : Expr::TransportEquation( get_var_name( specNum ),
                             get_rhs_tag( specNum ) ),
    specNum_( specNum ),
    nspecies_(numSpecies)
{
  typedef SpeciesPrimVar<VolField>::Builder Spec;

  const StringNames& sName = StringNames::self();
  const std::vector<std::string>& spNames = CanteraObjects::species_names();
  const std::string speciesPrefix = (gaschem==FLAME_SHEET || gaschem==EQUILIBRIUM) ? sName.flsspecies+"_" : "";

  const Expr::TagList diffFluxTagList= Expr::tag_list( spNames, STATE_N, sName.specDiffFluxX + "_" );
  const Expr::TagList diffCoeffTags  = Expr::tag_list( spNames, STATE_N, "D_" );
  const Expr::TagList massFracTags   = Expr::tag_list( spNames, STATE_N       );
  // product mass fraction tags - only different for EQUILIBRIUM and FLAME_SHEET models
  const Expr::TagList prodMassFracs = Expr::tag_list( spNames, STATE_N, speciesPrefix );
  const Expr::Tag prodDensity = Tag( (gaschem==FLAME_SHEET || gaschem==EQUILIBRIUM) ? sName.flsdensity : sName.density, STATE_N );

  const bool doReaction = reactionTag != Tag();

  // register required expressions
  static bool haveBuiltOneTimeExpressions = false;

  if( !haveBuiltOneTimeExpressions ){

    haveBuiltOneTimeExpressions = true;

    typedef pokitt::MixtureMolWeight<VolField>::Builder MixMW;
    exprFactory.register_expression( new MixMW( Tag(sName.mixtureMW,STATE_N), prodMassFracs, pokitt::MASS ) );

    // register (n-1) species expressions
    for( int i=0; i<nspecies_-1; ++i ){
      const string name2 = sName.rhoY + "_" + spNames[i];
      exprFactory.register_expression( new Spec( massFracTags[i],
                                                 Tag( name2,         STATE_N ),
                                                 Tag( sName.density, STATE_N ) ));
    }
    // register nth species expression
    typedef pokitt::SpeciesN<VolField>::Builder SpecN;
    const bool disableBoundsCheck = true;
    exprFactory.register_expression( new SpecN( massFracTags[nspecies_-1], massFracTags, pokitt::CLIPSPECN ) );

    if( detailedTransport ){
      if( !hasPCSolver ){
        typedef pokitt::DiffusionCoeffMol<VolField>::Builder Di;
        exprFactory.register_expression( new Di( diffCoeffTags,
                                                Tag( sName.temperature, STATE_N ),
                                                Tag( sName.pressure, STATE_N ),
                                                massFracTags,
                                                Tag( sName.mixtureMW,   STATE_N)));
        
        typedef SpeciesDiffFlux<FaceField>::Builder Flux;
        exprFactory.register_expression( new Flux( diffFluxTagList,
                                                  massFracTags,
                                                  Tag( sName.density,     STATE_N ),
                                                  Tag( sName.mixtureMW,   STATE_N),
                                                  diffCoeffTags) );
      }
      else {
        cout << "********* Species diffusion flux is using the mass based mixture averaged diffusion coefficients. \n"; // This is necessary to be consistant with PC equations
        // todo: replace with pokitt
        typedef SpeciesDiffFluxFromYs<FaceField>::Builder Flux;
        exprFactory.register_expression( new Flux( diffFluxTagList,
                                                  Tag( sName.temperature, STATE_N ),
                                                  massFracTags,
                                                  Tag( sName.density,     STATE_N ) ) );
      }
    }
    else{
      /* Curently, if detailed transport is turned off, species diffusivities are set based on the assumption of a
       * unity Lewis Number for each species.
       * TODO: add capability to parse a Lewis number (or numbers)
       * TODO: change SimpleSpeciesDiffussiveFlux to accept an Expr::Taglist for individual species diffusivities
       */
      const double lewisNumber = 1.;
      const Tag diffusivityFromLeTag = Tag( "diffusivityFromLewisNumber", STATE_N);
      typedef SpeciesDiffusivityFromLewisNumber<VolField>::Builder DiffusivityFromLe;
      exprFactory.register_expression(new DiffusivityFromLe( diffusivityFromLeTag,
                                                             Tag( sName.density             , STATE_N ),
                                                             Tag( sName.thermal_conductivity, STATE_N ),
                                                             Tag( sName.heat_capacity       , STATE_N ),
                                                             lewisNumber ));

      typedef SimpleSpeciesDiffFlux<FaceField>::Builder Flux;
      exprFactory.register_expression( new Flux( diffFluxTagList,
                                                 nspecies_,
                                                 diffusivityFromLeTag,
                                                 massFracTags,
                                                 Tag( sName.density, STATE_N) ) );
    }


    switch( gaschem ){
      case COLD_FLOW :{
        cout << "****************** Cold Flow \n"; // or use gaschem_model_name (gaschem)
        break;
      }

      case FLAME_SHEET:{
        cout << "****************** Flamesheet chemistry in gas phase\n";
        Expr::TagList flmPrdTaglist = prodMassFracs;
        flmPrdTaglist.push_back(Tag(sName.flsdensity, STATE_N) );

        exprFactory.register_expression( new Flamesheet::Builder( flmPrdTaglist,
                                                                  massFracTags,
                                                                  Tag( sName.density, STATE_N )) );
        break;
      }

      case EQUILIBRIUM:{
        cout << "****************** Equilibrium chemistry in gas phase\n";
        Expr::TagList flmPrdTaglist;
        // Add gas phase density tag
        flmPrdTaglist.push_back(Tag(sName.temperature,STATE_N));
        flmPrdTaglist.push_back(Tag(sName.flsdensity, STATE_N));

        // Add equilibrium species composition
        BOOST_FOREACH( const Expr::Tag& t, prodMassFracs ){
          flmPrdTaglist.push_back(t);
        }

        exprFactory.register_expression( new  Equilibrium::Builder( flmPrdTaglist,
                                                                    Tag( sName.species, STATE_N ),
                                                                    Tag( sName.e0,      STATE_N ),
                                                                    Tag( sName.ke,      STATE_N ),
                                                                    Tag( sName.density, STATE_N )) );
        break;
      }

      case DETAILED_KINETICS:{
        Expr::TagList rxnRateTagList;
        cout << "****************** Detailed kinetics in gas phase.\n";
        for( int i=0; i<nspecies_; ++i ){
          const string rxn = sName.SpeciesSourceTerm + "_" + spNames[i];
          rxnRateTagList.push_back( Tag(rxn,STATE_N) );
        }
        typedef pokitt::ReactionRates<VolField>::Builder RxnRates;
        exprFactory.register_expression( new RxnRates( rxnRateTagList,
                                                       Tag( sName.temperature, STATE_N ),
                                                       Tag( sName.density, STATE_N ),
                                                       massFracTags,
                                                       Tag( sName.mixtureMW, STATE_N ) ) );
        break;
      }

      case INVALID_GASMODEL:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__
            << "\nInvalid selection for gas-phase chemistry model\n\n";
        throw std::runtime_error( msg.str() );
    }

  } // if haveBuiltOneTimeExpressions

  // BUILD RHS Expression
  {
    Expr::TagList srcs;
    if( doReaction ) srcs.push_back( reactionTag );

    typedef ScalarRHS<VolField> RHS;
    RHS::FieldTagInfo fieldTagInfo;
    fieldTagInfo[ RHS::DIFFUSION_FLUX ] = diffFluxTagList[specNum];

    if( doAdvection ){
      fieldTagInfo[ RHS::SOLUTION_VARIABLE ] = Tag( get_var_name(specNum),  STATE_N );
      fieldTagInfo[ RHS::ADVECTING_VELOCITY] = Tag( sName.xvel+"_advect",   STATE_N );
    }

    exprFactory.register_expression( new RHS::Builder( get_rhs_tag(specNum), fieldTagInfo, srcs ) );
  }
}

//--------------------------------------------------------------------

SpeciesEquation::~SpeciesEquation()
{}

//--------------------------------------------------------------------

void
SpeciesEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{}

//--------------------------------------------------------------------

Expr::ExpressionID
SpeciesEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const StringNames& sName = StringNames::self();
  const Expr::TagList spTags = Expr::tag_list( CanteraObjects::species_names(), STATE_N );
  if( specNum_ == 0 ){
    typedef pokitt::MixtureMolWeight<VolField>::Builder MixMW;
    exprFactory.register_expression( new MixMW( Tag( sName.mixtureMW, STATE_N ),
                                                spTags,
                                                pokitt::MASS ) );
  }
  typedef RhoYiICExpr::Builder ICExpr;
  return exprFactory.register_expression( new ICExpr( Tag( get_var_name(specNum_), STATE_N ),
                                                      Tag(sName.density,STATE_N),
                                                      spTags[specNum_] ) );
}

//--------------------------------------------------------------------

Expr::Tag
SpeciesEquation::
get_rhs_tag( const int specNum )
{
  return Expr::Tag( "rhoY_"+CanteraObjects::species_name( specNum )+"_RHS", STATE_N );
}

//--------------------------------------------------------------------

std::string
SpeciesEquation::
get_var_name( const int specNum )
{
  return StringNames::self().rhoY + "_" + CanteraObjects::species_name(specNum);
}

//--------------------------------------------------------------------
