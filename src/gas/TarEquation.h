#ifndef TarEquation_h
#define TarEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types

#include <gas/ScalarRHS.h>
#include <gas/RxnDiffusionSetup.h>
#include <gas/DerivedRxnTerms.h>

/**
 *  @class TarEquation
 *  @author Josh McConnell
 *  @date   December, 2014
 *  @brief Calculates the right hand side (RHS) of the transport equation for tar.
 *
 *  The RHS for tar transport is given as
 * \f[
 *
 *     RHS=\\nabla\cdot\left(D_{tar}\nabla Y_{\mathrm{tar}}\right)-\\nabla\cdot\left(\rho_{\mathrm{g}}\vec{u}Y_{\mathrm{tar}}\right)+S_{\mathrm{tar}}.
 *
 * \f]
 *
 *  <li>  \f[Y_{\mathrm{tar}} \f] is the mass fraction of tar,
 *
 *
 *  <li>  \f[vec{u} \f] is the gas velocity,
 *
 *
 *  <li>  \f[ D_{tar} \f] is the mass diffusivity of tar,
 *
 *
 *  <li>  \f[ \rho_{g} \f] is the gas phase density, and
 *
 *
 *  <li>  \f[ S_{mathrm{tar}} \f] is the source term for reactions involving tar.
 *
 *
 *  </ul>
 *
 */
//====================================================================
class TarEquation : public Expr::TransportEquation
{
public:
//	const Tar_Data tarData_;

  TarEquation( Expr::ExpressionFactory& exprFactory,
                    const bool doAdvection,
                    const Expr::Tag tarSourceTag );


  ~TarEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  static Expr::Tag get_rhoTar_tag();
  static Expr::Tag get_rhoTarRHS_tag();
  static Expr::Tag get_tarDiffFlux_tag();
  static Expr::Tag get_species_rhs_tag( const std::string& speciesName );


};

#endif
