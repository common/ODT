#ifndef SootFormationRate_h
#define SootFormationRate_h

#include <expression/Expression.h>

/**
 *  @class SootFormationRate
 *  @author Josh McConnell
 *  @date   January, 2015
 *  @brief Calculates the rate of soot formation in (kg soot)/(m^3)-s, which is required to
 *         calculate source terms for the transport equations  of soot 
 *         mass, tar mass, and soot particles per volume.
 *
 *  The rate of soot formation is given as
 * \f[

 *     r_{\mathrm{F,soot}} = [\mathrm{tar}] * A_{F,\mathrm{soot}} * exp(-E_{F,\mathrm{soot}} / RT),
 *
 * \f]
 *  <ul>
 *
 *  <li> where \f$ [\mathrm{tar}] \f$ is the concentration of tar.
 *
 *  </ul>
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 * @param density_:     system density
 * @param temp:         system temperature (K)
 * @param ytar:         (kg tar)/(kg total)
 * @param A_            (1/s) Arrhenius preexponential factor [1]
 * @param E_            (J/mol) Activation energy             [1]
 * @param gasCon_       J/(mol*K)
 */

template< typename ScalarT >
class SootFormationRate
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS( ScalarT, yTar_, temp_, density_ )
  const double A_, E_, gasCon_;

    SootFormationRate( const Expr::Tag& yTarTag,
    		           const Expr::Tag& densityTag,
                       const Expr::Tag& tempTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a SootFormationRate expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& yTarTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& tempTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag yTarTag_, tempTag_, densityTag_;
  };

  ~SootFormationRate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
SootFormationRate<ScalarT>::
SootFormationRate( const Expr::Tag& yTarTag,
		           const Expr::Tag& densityTag,
                   const Expr::Tag& tempTag )
  : Expr::Expression<ScalarT>(),
    A_     ( 5.02E+8    ),
    E_     ( 198.9E+3   ),
    gasCon_( 8.3144621  )
{
	yTar_    = this->template create_field_request<ScalarT>( yTarTag    );
	density_ = this->template create_field_request<ScalarT>( densityTag );
	temp_    = this->template create_field_request<ScalarT>( tempTag    );
}

//--------------------------------------------------------------------

template< typename ScalarT >
SootFormationRate<ScalarT>::
~SootFormationRate()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootFormationRate<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootFormationRate<ScalarT>::
evaluate()
{

    using namespace SpatialOps;
    
    ScalarT& result = this->value();

    const ScalarT& yTar    = yTar_   ->field_ref();
    const ScalarT& density = density_->field_ref();
    const ScalarT& temp    = temp_   ->field_ref();

    result <<= density * yTar * A_ *exp(-E_ / (gasCon_ * temp) );
}

//--------------------------------------------------------------------

template< typename ScalarT >
SootFormationRate<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& yTarTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& tempTag )
  : ExpressionBuilder( resultTag ),
    yTarTag_   ( yTarTag    ),
    densityTag_( densityTag ),
    tempTag_   ( tempTag    )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
SootFormationRate<ScalarT>::
Builder::build() const
{
  return new SootFormationRate<ScalarT>( yTarTag_, densityTag_, tempTag_ );
}


#endif // SootFormationRate_h
