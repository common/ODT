#ifndef ScalarRHS_h
#define ScalarRHS_h

#include <map>

#include <expression/ExprLib.h>

/**
 *  @class ScalarRHS
 *  @author James C. Sutherland
 *
 *  @brief Support for a basic scalar transport equation involving
 *         any/all of advection, diffusion and reaction.
 *
 *  The ScalarRHS Expression defines a template class for basic
 *  transport equations.  Each equation is templated on an interpolant
 *  and divergence operator, from which the field types are deduced.
 *
 *  The user provides expressions to calculate the advecting velocity,
 *  diffusive fluxes and/or source terms.  This will then calculate
 *  the full RHS for use with the time integrator.
 */
template< typename ScalarT >
class ScalarRHS
  : public Expr::Expression< ScalarT >
{
  typedef typename SpatialOps::FaceTypes<ScalarT>::XFace          FluxT;
  typedef typename SpatialOps::BasicOpTypes<ScalarT>::DivX        DivT;
  typedef typename SpatialOps::BasicOpTypes<ScalarT>::InterpC2FX  InterpT;

public:

  enum FieldSelector{
    SOLUTION_VARIABLE,
    ADVECTING_VELOCITY,
    DIFFUSION_FLUX,
    SOURCE_TERM
    };

  typedef std::map< FieldSelector, Expr::Tag >  FieldTagInfo;

  class Builder : public Expr::ExpressionBuilder
  {
  public:

    Builder( const Expr::Tag& result,
             const FieldTagInfo& fieldInfo );

    Builder( const Expr::Tag& result,
             const FieldTagInfo& fieldInfo,
             const Expr::TagList& srcTags );

    virtual ~Builder(){}

    virtual Expr::ExpressionBase* build() const;
  protected:
    const FieldTagInfo info_;
    std::vector<Expr::Tag> srcT_;
  };

  virtual void evaluate();
  virtual void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  const bool haveAdvection_, haveDiffusion_;

  const DivT* divOp_;
  const InterpT* interpOp_;

  DECLARE_FIELD( ScalarT, phi_ )
  DECLARE_FIELDS( FluxT, advectVelX_, diffFlux_ )

  DECLARE_VECTOR_OF_FIELDS( ScalarT, srcTerm_ )

  static Expr::Tag resolve_field_tag( const FieldSelector,
                                      const FieldTagInfo& fieldTags );

  ScalarRHS( const FieldTagInfo& fieldTags,
             Expr::TagList srcTags );

  virtual ~ScalarRHS();

};



// ###################################################################
//
//                           Implementation
//
// ###################################################################



template< typename ScalarT >
ScalarRHS<ScalarT>::
ScalarRHS( const FieldTagInfo& fieldTags,
           Expr::TagList srcTags )
  : Expr::Expression<ScalarT>(),

    haveAdvection_( resolve_field_tag( ADVECTING_VELOCITY, fieldTags ) != Expr::Tag() ),
    haveDiffusion_( resolve_field_tag( DIFFUSION_FLUX,     fieldTags ) != Expr::Tag() )
{
  if( haveAdvection_ ){
    phi_        = this->template create_field_request<ScalarT>( resolve_field_tag( SOLUTION_VARIABLE,  fieldTags ) );
    advectVelX_ = this->template create_field_request<  FluxT>( resolve_field_tag( ADVECTING_VELOCITY, fieldTags ) );
  }

  if( haveDiffusion_ ) diffFlux_ = this->template create_field_request<FluxT>( resolve_field_tag( DIFFUSION_FLUX, fieldTags ) );

  const Expr::Tag singleSrcTag = resolve_field_tag( SOURCE_TERM, fieldTags );
  if( singleSrcTag != Expr::Tag() ) srcTags.push_back( singleSrcTag );

  if( !srcTags.empty() ) this->template create_field_vector_request<ScalarT>( srcTags, srcTerm_ );
}

//--------------------------------------------------------------------

template< typename ScalarT >
ScalarRHS<ScalarT>::
~ScalarRHS()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
ScalarRHS<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivT>();

  if( haveAdvection_ )
    interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename ScalarT >
void
ScalarRHS<ScalarT>::
evaluate()
{
  using namespace SpatialOps;

  ScalarT& rhs = this->value();

  if( haveAdvection_ ){
    const ScalarT& phi = phi_       ->field_ref();
    const FluxT& xvel  = advectVelX_->field_ref();
    rhs <<= -(*divOp_)( (*interpOp_)(phi) * xvel );
  }
  else rhs <<= 0.0;

  if( haveDiffusion_ ){
    const FluxT& diffFlux = diffFlux_->field_ref();
    rhs <<= rhs - (*divOp_)( diffFlux );
  }

  for( size_t i=0; i<srcTerm_.size(); ++i ){
    rhs <<= rhs + srcTerm_[i]->field_ref();
  }

  *(rhs.begin()  ) = 0.0; // ghost cell
  *(rhs.end()  -1) = 0.0; // ghost cell
}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::Tag
ScalarRHS<ScalarT>::
resolve_field_tag( const FieldSelector field,
                   const ScalarRHS<ScalarT>::FieldTagInfo& info )
{
  Expr::Tag tag;
  const typename FieldTagInfo::const_iterator ifld = info.find( field );
  if( ifld != info.end() ){
    tag = ifld->second;
  }
  return tag;
}

//--------------------------------------------------------------------

template< typename ScalarT >
ScalarRHS<ScalarT>::Builder::
Builder( const Expr::Tag& result,
         const FieldTagInfo& fieldInfo,
         const std::vector<Expr::Tag>& sources )
  : ExpressionBuilder(result),
    info_( fieldInfo ),
    srcT_( sources )
{}

template< typename ScalarT >
ScalarRHS<ScalarT>::Builder::
Builder( const Expr::Tag& result,
         const FieldTagInfo& fieldInfo )
  : ExpressionBuilder(result),
    info_( fieldInfo )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
ScalarRHS<ScalarT>::Builder::build() const
{
  return new ScalarRHS<ScalarT>( info_, srcT_ );
}


#endif
