#include <gas/DensityEquation.h>

#include <StringNames.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <pokitt/thermo/Density.h>

#include <cantera/IdealGasMix.h>

using Expr::Tag;
using Expr::STATE_N;

//--------------------------------------------------------------------

DensityEquation::
DensityEquation( Expr::ExpressionFactory& exprFactory,
                 const bool doAdvection)
  : Expr::TransportEquation( StringNames::self().density,
                             Tag( StringNames::self().density+"RHS", STATE_N ) )
{
  typedef ScalarRHS<VolField>  RHS;
  RHS::FieldTagInfo fieldTagInfo;
  const StringNames& sName = StringNames::self();

  if( doAdvection ){
    fieldTagInfo[ RHS::SOLUTION_VARIABLE  ] = Tag( sName.density, STATE_N );
    fieldTagInfo[ RHS::ADVECTING_VELOCITY ] = Tag( sName.xvel+"_advect", STATE_N );
  }

  exprFactory.register_expression( new RHS::Builder( Tag( StringNames::self().density+"RHS", STATE_N ), fieldTagInfo ) );
}

//--------------------------------------------------------------------

DensityEquation::~DensityEquation()
{}

//--------------------------------------------------------------------

void
DensityEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{}

//--------------------------------------------------------------------

Expr::ExpressionID
DensityEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const StringNames& sName = StringNames::self();
  typedef pokitt::Density<VolField>::Builder DensIC;
  return exprFactory.register_expression( new DensIC( Tag( sName.density,     STATE_N ),
                                                      Tag( sName.temperature, STATE_N ),
                                                      Tag( sName.pressure,    STATE_N ),
                                                      Tag( sName.mixtureMW,   STATE_N ) ) );
}

//--------------------------------------------------------------------
