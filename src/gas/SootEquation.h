#ifndef SootEquation_h
#define SootEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types

#include <gas/ScalarRHS.h>
#include <gas/RxnDiffusionSetup.h>
#include <gas/DerivedRxnTerms.h>


/**
 *  @class SootEquation
 *  @author Josh McConnell
 *  @date   December, 2014
 *  @brief Calculates the right hand side (RHS) of the transport equation for soot.
 *
 *  The RHS for soot transport is given as
 * \f[
 *
 *     RHS=\\nabla\cdot\left(D_{soot}\nabla Y_{\mathrm{soot}}\right)-\\nabla\cdot\left(\rho_{\mathrm{g}}\vec{u}Y_{\mathrm{soot}}\right)+S_{\mathrm{soot}}.
 *
 * \f]
 *
 *  <li>  \f[Y_{\mathrm{soot}} \f] is the mass fraction of soot,
 *
 *
 *  <li>  \f[vec{u} \f] is the gas velocity,
 *
 *
 *  <li>  \f[ D_{soot} \f] is the mass diffusivity of soot,
 *
 *
 *  <li>  \f[ \rho_{g} \f] is the gas phase density, and
 *
 *
 *  <li>  \f[ S_{mathrm{soot}} \f] is the source term for reactions involving soot.
 *
 *
 *  </ul>
 *
 */
//====================================================================
class SootEquation : public Expr::TransportEquation
{
public:


  SootEquation( Expr::ExpressionFactory& exprFactory,
                    const bool doAdvection,
                    const Expr::Tag sootSourceTag);


  ~SootEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  static Expr::Tag get_rhoSoot_tag();
  static Expr::Tag get_rhoSootRHS_tag();
  static Expr::Tag get_sootDiffFlux_tag();
  static Expr::Tag get_species_rhs_tag( const std::string& speciesName );


};

#endif
