#ifndef TarSource_Expr_h
#define TarSource_Expr_h

#include <expression/Expression.h>

/**
 *  @class TarSource
 *  @author Josh McConnell
 *  @date   February, 2015
 *  @brief Calculates the source term, in (kg tar)/(m^3)-s, for tar transport.
 *
 *
 *  @param density_:        system density
 *  @param rTarGas_:        tar gasification rate (1/s)
 *  @param rTarOx_:         tar oxidation rate (1/s)
 *  @param rSootForm_:      soot formation rate (1/s)
 */
template< typename ScalarT >
class TarSource
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS( ScalarT, density_, rSootForm_, rTarOx_, rTarGas_ )

    TarSource( const Expr::Tag& densityTag,
               const Expr::Tag& rSootFormTag,
               const Expr::Tag& rTarOxTag,
               const Expr::Tag& rTarGasTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a TarSource expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& rSootFormTag,
             const Expr::Tag& rTarOxTag,
             const Expr::Tag& rTarGasTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag densityTag_, rSootFormTag_, rTarOxTag_, rTarGasTag_;
  };

  ~TarSource();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
TarSource<ScalarT>::
TarSource( const Expr::Tag& densityTag,
            const Expr::Tag& rSootFormTag,
            const Expr::Tag& rTarOxTag,
            const Expr::Tag& rTarGasTag )
  : Expr::Expression<ScalarT>()
{
	density_   = this->template create_field_request<ScalarT>( densityTag   );
	rSootForm_ = this->template create_field_request<ScalarT>( rSootFormTag );
	rTarOx_    = this->template create_field_request<ScalarT>( rTarOxTag    );
	rTarGas_   = this->template create_field_request<ScalarT>( rTarGasTag   );
}

//--------------------------------------------------------------------

template< typename ScalarT >
TarSource<ScalarT>::
~TarSource()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TarSource<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TarSource<ScalarT>::
evaluate()
{
  using namespace SpatialOps;
    ScalarT& result = this->value();

    const ScalarT& density   = density_  ->field_ref();
    const ScalarT& rSootForm = rSootForm_->field_ref();
    const ScalarT& rTarGas   = rTarGas_  ->field_ref();
    const ScalarT& rTarOx    = rTarOx_   ->field_ref();

    result <<= ( -rSootForm - rTarOx - rTarGas );

  /* evaluation code goes here - be sure to assign the appropriate value to 'result' */
}

//--------------------------------------------------------------------

template< typename ScalarT >
TarSource<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& rSootFormTag,
                  const Expr::Tag& rTarOxTag,
                  const Expr::Tag& rTarGasTag )
  : ExpressionBuilder( resultTag ),
    densityTag_  ( densityTag   ),
    rSootFormTag_( rSootFormTag ),
    rTarOxTag_   ( rTarOxTag    ),
    rTarGasTag_  ( rTarGasTag   )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
TarSource<ScalarT>::
Builder::build() const
{
  return new TarSource<ScalarT>( densityTag_, rSootFormTag_, rTarOxTag_, rTarGasTag_ );
}


#endif // TarSource_Expr_h
