#ifndef RxnDiffusionSetup_h
#define RxnDiffusionSetup_h

#include <parser/ParseTools.h>
#include <GraphHelperTools.h>

#include <expression/ExprLib.h>
#include <expression/ExprPatch.h>

#include <gas/pca/PCA.h>
typedef Expr::ExprPatch  PatchT;

//====================================================================
// forward declarations

namespace Expr{
  class ExpressionFactory;
  class TimeStepper;
}

namespace SpatialOps{
  class Grid;
}

//====================================================================

enum GasChemistry {
  COLD_FLOW         = 0,  ///< No reaction
  DETAILED_KINETICS = 1,  ///< detailed chemical kinetics via cantera
  FLAME_SHEET       = 2,  ///< infinitely fast chemistry
  EQUILIBRIUM       = 3,  ///< equilibrium chemistry
  INVALID_GASMODEL  = 99
};

//====================================================================

class RxnDiffusionSetup
{

public:

  RxnDiffusionSetup( const YAML::Node& pg,
                     PatchT& patch,
                     GraphCategories& gc,
                     const bool isRestart,
                     PCA& pca,
                     const int nPCs,
                     const bool hasTarAndSoot);

  ~RxnDiffusionSetup();

  Expr::TimeStepper& get_time_stepper();

  void parse_species_ic( const YAML::Node& );

  void set_initial_conditions( const YAML::Node& );

  void set_bc();

  PatchT& patch(){return patch_;}
  Expr::ExpressionFactory& get_expression_factory(){return exprFactory_;}
  Expr::ExpressionFactory& get_icexpression_factory(){return icExprFactory_;}

  void build_inital_tree();

  const SpatialOps::Grid& grid() const{ return *grid_; }

  bool solveSpecies_;
  bool solveEnergy_;

private:

  void set_charateristic_BC( const double L,
                             const bool doReaction );

  void setup_cantera( const YAML::Node& pg );

  void set_comp( const YAML::Node& pg,
                 std::vector<double>& spec ) const;

  void mole_to_mass( const std::vector<double>& mw,
                     std::vector<double>& spec );

  PatchT& patch_;
  Expr::ExpressionFactory &exprFactory_, &icExprFactory_;
  SpatialOps::Grid* grid_;

  typedef std::list<Expr::TransportEquation*> EqnList;
  EqnList equations_;

  Expr::TimeStepper* integrator_;

  std::vector<bool> hasPlusSideBoundary_;

  Expr::ExpressionID diffID_, rxnID_;

  const bool   hasTarAndSoot_;
  const double pInf_;
};

std::string gaschem_model_name( const GasChemistry model );

GasChemistry gaschem_model( const std::string& name );


#endif
