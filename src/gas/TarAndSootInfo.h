/*
 * The MIT License
 *
 * Copyright (c) 2012-2018 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TarAndSootInfo_h
#define TarAndSootInfo_h

  /**
   *  \class  TarAndSootInfo
   *  \author Josh McConnell
   *  \date   November, 2016
   *
   */
  class  TarAndSootInfo
  {
  public:

    static const TarAndSootInfo& self();

    const double& tar_hydrogen()           const;
    const double& tar_carbon()             const;
    const double& tar_required_moles_O2()  const;
    const double& tar_produced_moles_CO()  const;
    const double& tar_produced_moles_H2O() const;
    const double& tar_mw()                 const;
    const double& tar_diffusivity()        const;
    const double& tar_heat_of_formation()  const;
    const double& tar_heat_of_oxidation()  const;

    const double& soot_hydrogen()          const;
    const double& soot_carbon()            const;
    const double& soot_required_moles_O2() const;
    const double& soot_produced_moles_CO() const;
    const double& soot_produced_moles_H2O()const;
    const double& soot_mw()                const;
    const double& soot_density()           const;
    const double& soot_cMin()              const;
    const double& soot_heat_of_oxidation() const;
    const double& soot_heat_of_formation() const;

  private:
    TarAndSootInfo();
    TarAndSootInfo(const TarAndSootInfo&);              // No copying
      TarAndSootInfo& operator=(const TarAndSootInfo&); // No assignment
    void setup();

    // Tar and soot properties. It is assumed that coal tar is Naphthalene.
    // Oxidation of tar is assumed to occur by the following reaction:
    // tar + O2 --> CO + H2O
    //
    // Oxidation of soot is assumed to occur via
    // soot + O2 --> CO
    const double
    tarDiffusivity_,
    tarHydrogen_,
    tarCarbon_,
    tarMW_,
    tarHeatOfFormation_,
    tarHeatOfOxidation_,
	sootDensity_,
	sootCMin_,
	sootHydrogen_,
    sootCarbon_,
    sootMW_,
    sootHeatOfFormation_,
    sootHeatOfOxidation_;


    double
    tarRequiredMolesO2_,   // (moles O2 )/(moles tar) required for oxidation rxn
    tarProducedMolesCO_,   // (moles CO )/(moles tar) produced by oxidation rxn
    tarProducedMolesH2O_,  // (moles H2O)/(moles tar) produced by oxidation rxn
    sootRequiredMolesO2_,  // (moles O2 )/(moles soot) required for oxidation rxn
    sootProducedMolesCO_,  // (moles CO )/(moles soot) produced by oxidation rxn
    sootProducedMolesH2O_; // (moles H2O)/(moles soot) produced by oxidation rxn

  };
#endif /* TarAndSootInfo_h */
