
#include <gas/TarEquation.h>
#include <gas/TarAndSootInfo.h>
#include <expression/ExprLib.h>     // Base class definition

//--- Expressions required for TarEquation ---//
#include <gas/DiffusiveFlux.h>
#include <gas/SpeciesPrimVar.h>

#include <expression/Functions.h>
#include <StringNames.h>

#include <cantera/IdealGasMix.h>
#include <pokitt/CanteraObjects.h>


using Expr::Tag;
using Expr::STATE_N;
using Expr::STATE_NONE;

//====================================================================

class RhoTarICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder{
    Builder( const Expr::Tag& result ) : ExpressionBuilder( result ) {}
    Expr::ExpressionBase* build() const{ return new RhoTarICExpr(); }
  };

  void evaluate()
  {
    using namespace SpatialOps;
    VolField& rhoTar = this->value();
    rhoTar <<= 0.0;
  }

  ~RhoTarICExpr(){}

private:
  RhoTarICExpr() : Expr::Expression<VolField>()
  {}
};

//====================================================================

TarEquation::
TarEquation( Expr::ExpressionFactory& exprFactory,
             const bool doAdvection,
             const Expr::Tag tarSourceTag )

  : Expr::TransportEquation( StringNames::self().rhoTar,
                             get_rhoTarRHS_tag() )
{
  std::cout<<"\n\nTarEquqation: registering expressions\n";

  const TarAndSootInfo& tarSootInfo = TarAndSootInfo::self();
  std::cout<<"\nCheckpoint 1\n";

  const double tarDiffusivity = tarSootInfo.tar_diffusivity();
  const double tarMW          = tarSootInfo.tar_mw();

  const StringNames& sName = StringNames::self();

  const Tag tarTag            = Tag(sName.tarSpecies,       STATE_N);
  const Tag tarOxRateTag      = Tag(sName.tarOxR,           STATE_N);
  const Tag sootFormRateTag   = Tag(sName.sootFormR,        STATE_N);
  const Tag densityTag        = Tag(sName.density,          STATE_N);
  const Tag densityRHSTag     = Tag(sName.density + "RHS",  STATE_N);
  const Tag rhoTarTag         = get_rhoTar_tag();
  const Tag tarDiffFluxTag    = get_tarDiffFlux_tag();
  const Tag o2ConsumptionTag  = Tag("O2srcFromTar",         STATE_N);
  const Tag coConsumptionTag  = Tag("COsrcFromTar",         STATE_N);
  const Tag h2oConsumptionTag = Tag("H2OsrcFromTar",        STATE_N);
  const Tag IntEnergySrcTag   = Tag("rhoE0srcFromTar",      STATE_N);
  const Tag energyRHSTag      = Tag( sName.rhoE0+"RHS",     STATE_N);

  Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();

  const double mwO2  = gas->molecularWeight( gas->speciesIndex("O2" ) );
  const double mwCO  = gas->molecularWeight( gas->speciesIndex("CO" ) );
  const double mwH2O = gas->molecularWeight( gas->speciesIndex("H2O") );
  const double mwH2  = gas->molecularWeight( gas->speciesIndex("H2" ) );

  CanteraObjects::restore_gasmix(gas);

//-------------------------------------------------------------------------//
//------------------------------Species Source Terms-----------------------//
//-------------------------------------------------------------------------//
   // calculate the rate at which O2 and CO source terms due to tar oxidation under the following assumptions:
   // - tar is composed completely of naphthalene (C10H8)
   // - oxidation reaction: C10H8 + 7*O2 --> 10*CO + 4*H2O (no CO2 is produced)
  // - soot formation: C10H8(g) --> C10H8(s)

  typedef typename DerivedRxnRate<VolField>::Builder RxnRate;

   exprFactory.register_expression( new RxnRate
                                    ( o2ConsumptionTag,
                                      tarMW,
                                      mwO2,
                                      -1.0,
                                      -tarSootInfo.tar_required_moles_O2(),
                                      tarOxRateTag ));

   exprFactory.register_expression( new RxnRate
                                      ( coConsumptionTag,
                                        tarMW,
                                        mwCO,
                                        -1.0,
                                        tarSootInfo.tar_produced_moles_CO(),
                                        tarOxRateTag ));

   exprFactory.register_expression( new RxnRate
                                      ( h2oConsumptionTag,
                                        tarMW,
                                        mwH2O,
                                        -1.0,
                                        tarSootInfo.tar_produced_moles_H2O(),
                                        tarOxRateTag ));

   // The species rates calculated are rates of consumption, so they are subtracted from from their
   // respective RHSs.

   exprFactory.attach_dependency_to_expression( o2ConsumptionTag,
                                                get_species_rhs_tag( "O2" ),
                                                Expr::SUBTRACT_SOURCE_EXPRESSION );

   exprFactory.attach_dependency_to_expression( coConsumptionTag,
                                                get_species_rhs_tag( "CO" ),
                                                Expr::SUBTRACT_SOURCE_EXPRESSION );

   exprFactory.attach_dependency_to_expression( h2oConsumptionTag,
                                                get_species_rhs_tag( "H2O" ),
                                                Expr::SUBTRACT_SOURCE_EXPRESSION );

   // Add tar oxidation rate to density RHS
     exprFactory.attach_dependency_to_expression( tarOxRateTag,
                                                  densityRHSTag,
                                                  Expr::ADD_SOURCE_EXPRESSION );

   // calculate contribution to internal energy due to tar combustion and then add it to the internal
   // energy RHS
    const double deltaEtarOx = tarSootInfo.tar_heat_of_oxidation(); //(J/kg tar)

    exprFactory.register_expression( new IntEnergySrcFromRxn<VolField>::Builder
                                       ( IntEnergySrcTag,
                                         deltaEtarOx,
                                         tarOxRateTag ));

    exprFactory.attach_dependency_to_expression( IntEnergySrcTag,
                                                 energyRHSTag,
                                                 Expr::SUBTRACT_SOURCE_EXPRESSION );

    // stuff to do in case soot is assumed to be (solid) carbon rather than identical to tar
    if(tarSootInfo.soot_hydrogen() == 0){
      const Tag h2ConsumptionTag                = Tag("H2srcFromTar",              STATE_N);
      const Tag energySrcFromSootFormationTag   = Tag("rhoE0srcFromSootFormation", STATE_N);
      const Tag energySrcFromH2FormationTag     = Tag("rhoE0srcTarH2"            , STATE_N);
      const Tag h2Enthalpy                      = Tag("enthalpy_H2"              , STATE_N);


      const double tar2SootH2StoichCoeff = tarSootInfo.tar_hydrogen() - tarSootInfo.soot_hydrogen();
      exprFactory.register_expression( new RxnRate
                                         ( h2ConsumptionTag,
                                           tarMW,
                                           mwH2,
                                           -1.0,
                                           tar2SootH2StoichCoeff,
                                           sootFormRateTag ));

      exprFactory.attach_dependency_to_expression( h2ConsumptionTag,
                                                   get_species_rhs_tag( "H2" ),
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );

      exprFactory.attach_dependency_to_expression( h2ConsumptionTag,
                                                   densityRHSTag,
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );

      // heat of formation of solid carbon is zero
      const double deltaEsootForm = tarSootInfo.soot_heat_of_formation() - tarSootInfo.tar_heat_of_formation();
      exprFactory.register_expression( new IntEnergySrcFromRxn<VolField>::Builder
                                         ( energySrcFromSootFormationTag,
                                           deltaEsootForm,
                                           sootFormRateTag ));

      exprFactory.attach_dependency_to_expression( energySrcFromSootFormationTag,
                                                   energyRHSTag,
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );

      exprFactory.register_expression( new IntEnergySrcFromRxn<VolField>::Builder
                                         ( energySrcFromH2FormationTag,
                                           h2Enthalpy,
                                           h2ConsumptionTag ));

      exprFactory.attach_dependency_to_expression( energySrcFromH2FormationTag,
                                                   energyRHSTag,
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );

    }
//-------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------//


  exprFactory.register_expression( new DiffusiveFlux<VolField, FaceField>::Builder
                                   ( tarDiffFluxTag,
                                     tarTag,
                                     tarDiffusivity,
                                     densityTag ));

  exprFactory.register_expression( new SpeciesPrimVar<VolField>::Builder
                                   ( tarTag,
                                     rhoTarTag,
                                     densityTag ));


  {
    typedef ScalarRHS<VolField> TarRHS;
    TarRHS::FieldTagInfo fieldTagInfo;

    fieldTagInfo[ TarRHS::DIFFUSION_FLUX ] = tarDiffFluxTag;
    fieldTagInfo[ TarRHS::SOURCE_TERM    ] = tarSourceTag;

    if( doAdvection ){
      fieldTagInfo[ TarRHS::SOLUTION_VARIABLE  ] = rhoTarTag;
      fieldTagInfo[ TarRHS::ADVECTING_VELOCITY ] = Tag( sName.xvel+"_advect", STATE_N );
    }

    exprFactory.register_expression( new TarRHS::Builder( get_rhoTarRHS_tag(), fieldTagInfo ) );
  }

  std::cout<<"finished\n\n";
}

//--------------------------------------------------------------------

TarEquation::~TarEquation()
{}

//--------------------------------------------------------------------

void
TarEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{}

//--------------------------------------------------------------------

Expr::ExpressionID
TarEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const Expr::Tag rhoTarTag = get_rhoTar_tag();
  return exprFactory.register_expression( new RhoTarICExpr::Builder( rhoTarTag ));
}

//--------------------------------------------------------------------
Expr::Tag
TarEquation::
get_rhoTar_tag( )
{
  return Expr::Tag(StringNames::self().rhoTar, STATE_N);
}
//--------------------------------------------------------------------
Expr::Tag
TarEquation::
get_rhoTarRHS_tag( )
{
  return Expr::Tag(StringNames::self().rhoTar+"RHS", STATE_N);
}
//--------------------------------------------------------------------
Expr::Tag
TarEquation::
get_tarDiffFlux_tag( )
{
  return Expr::Tag(StringNames::self().tarDiffFlux, STATE_N);
}
//--------------------------------------------------------------------

Expr::Tag
TarEquation::
get_species_rhs_tag( const std::string& speciesName )
{
  return Expr::Tag( "rhoY_"+speciesName+"_RHS", STATE_N );
}

//--------------------------------------------------------------------
