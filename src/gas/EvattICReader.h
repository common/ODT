#ifndef EvattICReader_h
#define EvattICReader_h

#include <string>
#include <vector>

#include <expression/ExprLib.h>

#include <OperatorsAndFields.h>

#include <boost/shared_ptr.hpp>

class EvattICReader;
typedef boost::shared_ptr< EvattICReader >  FilePtr;


/**
 *  @class EvattICReader
 *  @date  April, 2009
 *  @author James C. Sutherland
 *
 *  @brief Reads files supplied by Evatt that set the initial
 *         conditions for the variables required for the ODT
 *         simulation.
 */
class EvattICReader
{
public:

  enum FieldSelector{
    TEMPERATURE,
    PRESSURE,
    X_VELOCITY,
    Y_VELOCITY,
    Z_VELOCITY,
    MASS_FRACTION
  };

  /**
   *  @param filename  Specifies the name of the file to read.
   *  @param npts      Specifies the number of points in a field.
   *  @param nspec     Specifies the number of species
   */
  EvattICReader( const std::string& filename,
                 const unsigned int npts,
                 const unsigned int nspec );

  ~EvattICReader();

  /**
   *  @param field   The field to be set.
   *  @param fs      The FieldSelector specifying which field to set.
   *  @param specNum In the case where \c fs is \c MASS_FRACTION, this
   *                 specifies which species to load.
   */
  template< typename FieldIter >
  void set_field_value( FieldIter field,
                        const FieldSelector fs,
                        const int specNum = 0 ) const
  {
    // note that we switch the x and y components of velocity here
    // since we have x as the direction of the odt line by convention,
    // and the DNS is set up with y as the spanwise direction.
    using namespace std;
    vector<double>::const_iterator src;
    switch( fs ){
    case TEMPERATURE  : src=temp_ .begin(); break;
    case PRESSURE     : src=press_.begin(); break;
    case X_VELOCITY   : src=vvel_ .begin(); break;
    case Y_VELOCITY   : src=uvel_ .begin(); break;
    case Z_VELOCITY   : src=wvel_ .begin(); break;
    case MASS_FRACTION: src=ysp_[specNum].begin(); break;
    }
    for( unsigned int i=0; i<npts_; ++i, ++field, ++src ){
      *field = *src;
    }
  }

  size_t get_npts() const{ return npts_; }

private:

  const unsigned int npts_, nspec_;
  std::vector<double> temp_, press_, uvel_, vvel_, wvel_;
  std::vector< std::vector<double> > ysp_;

};




/**
 *  @class EvattICExpr
 *  @author James C. Sutherland
 *  @date  April, 2009
 *  @brief Set a field based on input from a file.
 */
class EvattICExpr : public Expr::Expression<VolField>
{
public:

  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const FilePtr file,
             const Expr::Tag& resultTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const FilePtr file_;
    EvattICReader::FieldSelector fs_;
    int specNum_;
  };

  void evaluate();

protected:
  ~EvattICExpr();
  EvattICExpr( const FilePtr file,
               const EvattICReader::FieldSelector fs,
               const int specNum );
  const EvattICReader::FieldSelector fs_;
  const FilePtr file_;
  const int specNum_;
};



//====================================================================


#endif // EvattICReader_h
