#ifndef Stress_h
#define Stress_h

#include <expression/ExprLib.h>
#include <spatialops/OperatorDatabase.h>


/**
 *
 * \f[
 *    \tau_{ij} = -\mu \left( \frac{\partial u_i}{\partial x_j} + \frac{\partial u_j}{\partial x_i} \right)
 *                + \delta_{ij} \frac{2}{3} \mu \frac{\partial u_k}{\partial x_k}
 * \f]
 * Since we are in 1-D, this becomes (letting \f$x\f$ be the ODT line direction)
 * \f[
 *    \tau_{xx} = -\frac{4}{3} \mu \frac{\partial u_x}{\partial x}
 *    \tau_{yx} = -\mu \frac{\partial u_y}{\partial x}
 * \f]
 */
template< typename VelT >
class Stress
  : public Expr::Expression< typename SpatialOps::FaceTypes<VelT>::XFace >
{
  typedef typename SpatialOps::FaceTypes<VelT>::XFace         TauT;
  typedef typename SpatialOps::BasicOpTypes<VelT>::GradX      GradT;
  typedef typename SpatialOps::BasicOpTypes<VelT>::InterpC2FX InterpT;

  const bool isNormalStress_;
  const GradT* gradOp_;
  const InterpT* interpOp_;
  DECLARE_FIELDS( VelT, vel_, visc_ )

  Stress( const bool isNormalStress,
          const Expr::Tag& velTag,
          const Expr::Tag& viscTag );

public:

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  class Builder : public Expr::ExpressionBuilder
  {
    const bool isNormalStress_;
    const Expr::Tag velT_, viscT_;
  public:
    Builder( const Expr::Tag& result,
             const bool isNormalStress,
             const Expr::Tag& velTag,
             const Expr::Tag& viscTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };
};




// ###################################################################
//
//                           Implementation
//
// ###################################################################



template< typename VelT >
Stress<VelT>::Builder::
Builder( const Expr::Tag& result,
         const bool isNormalStress,
         const Expr::Tag& velTag,
         const Expr::Tag& viscTag )
  : ExpressionBuilder(result),
    isNormalStress_( isNormalStress ),
    velT_ ( velTag  ),
    viscT_( viscTag )
{
}

//--------------------------------------------------------------------

template< typename VelT >
Expr::ExpressionBase*
Stress<VelT>::Builder::build() const
{
  return new Stress<VelT>( isNormalStress_, velT_, viscT_ );
}

//--------------------------------------------------------------------

template< typename VelT >
Stress<VelT>::
Stress( const bool isNormalStress,
        const Expr::Tag& velTag,
        const Expr::Tag& viscTag )
  : Expr::Expression<TauT>(),
    isNormalStress_( isNormalStress )
{
  vel_  = this->template create_field_request<VelT>( velTag  );
  visc_ = this->template create_field_request<VelT>( viscTag );
}

//--------------------------------------------------------------------

template< typename VelT >
void
Stress<VelT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename VelT >
void
Stress<VelT>::
evaluate()
{
  using namespace SpatialOps;
  TauT& tau = this->value();

  const VelT& vel  = vel_ ->field_ref();
  const VelT& visc = visc_->field_ref();

  // form stress.  -2 is due to symmetry in the stress tensor, even
  // though we don't have two dimensions.
  const double factor = isNormalStress_ ? -4.0/3.0 : -1.0;
  tau <<= factor * (*gradOp_)(vel) * (*interpOp_)(visc);
  *(tau.begin()) = 0.0;
  *(tau.end()-1) = 0.0;
}

//--------------------------------------------------------------------

#endif // Stress_h
