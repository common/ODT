#ifndef PositionEquation_h
#define PositionEquation_h

#include <expression/ExprLib.h>  // Base class definition
#include <OperatorsAndFields.h>  // defines field types

#include <gas/Position.h>


/**
 *  @class PositionEquation
 *  @brief Intended for use in the temporal formulation of ODT, this
 *         TransportEquation solves an ODE for the position of the ODT
 *         line as a function of time.
 */
class PositionEquation : public Expr::TransportEquation
{
public:

  PositionEquation( Expr::ExpressionFactory& exprFactory, const double );

  ~PositionEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& factory );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

private:

  typedef Position<VolField>::Builder  RHSBuilder;
};


#endif // PositionEquation_h
