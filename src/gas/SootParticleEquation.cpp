
#include <gas/SootParticleEquation.h>

#include <expression/ExprLib.h>     // Base class definition

//#include <gas/AdvectVelocity.h>

//--- Expressions required for SootParticleEquation ---//
#include <gas/SootAgglomerationRate.h>
#include <gas/SootFormationRate.h>
#include <gas/SootParticleSource.h>
#include <gas/ParticlePrimVar.h>

#include <expression/Functions.h>

#include <StringNames.h>

using Expr::Tag;
using Expr::STATE_N;

//====================================================================

class RhoSootParticleICExpr : public Expr::Expression<VolField>
{
public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Expr::Tag& result )

      : ExpressionBuilder( result )
    {}
    Expr::ExpressionBase* build() const{ return new RhoSootParticleICExpr(); }
  };

  void evaluate()
  {
    using namespace SpatialOps;
    VolField& rhoSootP = this->value();
    rhoSootP <<= 0.0;
  }

  ~RhoSootParticleICExpr(){}

private:
  RhoSootParticleICExpr()
    : Expr::Expression<VolField>()
  {}
};

//====================================================================

SootParticleEquation::
SootParticleEquation( Expr::ExpressionFactory& exprFactory,
                 const bool doAdvection,
                 const Expr::Tag sootPSourceTag)


  : Expr::TransportEquation( StringNames::self().rhoSootP,
                             get_rhoSootPRHS_tag() )
{

  const StringNames& sName = StringNames::self();


  std::cout<<"\nSootParticleEquation: registering expressions\n";

  const Expr::Tag sootPTag        = Tag(sName.sootParticle,         STATE_N);
  const Expr::Tag densityTag      = Tag(sName.density,             STATE_N);
  const Expr::Tag rhoSootPTag     = get_rhoSootP_tag();


  exprFactory.register_expression( new ParticlePrimVar<VolField>::Builder
                                   ( sootPTag,
                                     rhoSootPTag,
                                     densityTag ));

  {
    typedef ScalarRHS<VolField>  SootPrtRHS;
    SootPrtRHS::FieldTagInfo fieldTagInfo;

    fieldTagInfo[ SootPrtRHS::SOURCE_TERM        ] = sootPSourceTag;

    if( doAdvection ){
      fieldTagInfo[ SootPrtRHS::SOLUTION_VARIABLE  ] = rhoSootPTag;
      fieldTagInfo[ SootPrtRHS::ADVECTING_VELOCITY ] = Tag( sName.xvel+"_advect", STATE_N );
    }

    exprFactory.register_expression( new SootPrtRHS::Builder( get_rhoSootPRHS_tag(), fieldTagInfo ) );
  }

  std::cout<<"finished\n\n";


}

//--------------------------------------------------------------------

SootParticleEquation::~SootParticleEquation()
{}

//--------------------------------------------------------------------

void
SootParticleEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{

}

//--------------------------------------------------------------------

Expr::ExpressionID
SootParticleEquation::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  //const StringNames& sName = StringNames::self();
  const Expr::Tag rhoSootPTag = get_rhoSootP_tag();

  return exprFactory.register_expression( new RhoSootParticleICExpr::Builder( rhoSootPTag ));
}

//--------------------------------------------------------------------
Expr::Tag
SootParticleEquation::
get_rhoSootP_tag( )
{
  return Expr::Tag(StringNames::self().rhoSootP, STATE_N);
}
//--------------------------------------------------------------------
Expr::Tag
SootParticleEquation::
get_rhoSootPRHS_tag( )
{
  return Expr::Tag(StringNames::self().rhoSootP+"RHS", STATE_N);
}
//--------------------------------------------------------------------
