#ifndef PrandtlNumber_h
#define PrandtlNumber_h

#include <expression/ExprLib.h>


/**
 *  @class PrandtlNumber
 *  @author Naveen K Punati
 *
 *  @brief Calculates the Prandtl Number \f$Pr = \frac{c_p \mu}{\lambda} \f$
 */
template< typename FieldT >
class PrandtlNumber
  : public Expr::Expression< FieldT >
{  
public:

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *
     * @param heatcapacityTag the mixture heat capacity
     * @param viscosityTag the mixture viscosity
     * @param conductivityTag the mixture thermal conductivity
     */
    Builder( const Expr::Tag heatcapacityTag,
	     const Expr::Tag viscosityTag,
	     const Expr::Tag conductivityTag );

    Expr::ExpressionBase* build( const Expr::ExpressionID& id,
				 const Expr::ExpressionRegistry& reg ) const;
  private:
    const Expr::Tag cpT_, muT_,lambdaT_;
  };

  void evaluate();

protected:

  PrandtlNumber(const Expr::ExpressionID& id,
                const Expr::ExpressionRegistry& reg,
                const Expr::Tag heatcapacityTag,
	              const Expr::Tag viscosityTag,
                const Expr::Tag conductivityTag );

  ~PrandtlNumber();
  
  DECLARE_FIELDS( FieldT, cp_, mu_, lambda_ )
};



//--------------------------Implementation---------------------------
template< typename FieldT >
PrandtlNumber<FieldT>::
PrandtlNumber( const Expr::ExpressionID& id,
               const Expr::ExpressionRegistry& reg,
               const Expr::Tag heatcapacityTag,
               const Expr::Tag viscosityTag,
               const Expr::Tag conductivityTag )
  : Expr::Expression<FieldT>( id, reg )
{
  this->set_gpu_runnable(true);

  cp_     = this->template create_field_request<FieldT>( heatcapacityTag );
  mu_     = this->template create_field_request<FieldT>( viscosityTag    );
  lambda_ = this->template create_field_request<FieldT>( conductivityTag );
}

//--------------------------------------------------------------------
template< typename FieldT >
PrandtlNumber<FieldT>::~PrandtlNumber()
{}

//--------------------------------------------------------------------

template< typename FieldT > void
PrandtlNumber<FieldT>::evaluate()
{
  FieldT& pr = this->value();
  const FieldT& cp     = cp_    ->field_ref();
  const FieldT& mu     = mu_    ->field_ref();
  const FieldT& lambda = lambda_->field_ref();
  pr <<= cp * mu / lambda;
}

//--------------------------------------------------------------------

template< typename FieldT >
PrandtlNumber<FieldT>::Builder::
Builder( const Expr::Tag heatcapacityTag,
         const Expr::Tag viscosityTag,
         const Expr::Tag conductivityTag )
  : cpT_    ( heatcapacityTag ),
    muT_    ( viscosityTag    ),
    lambdaT_( conductivityTag )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
PrandtlNumber<FieldT>::Builder::
build( const Expr::ExpressionID& id,
       const Expr::ExpressionRegistry& reg ) const
{
  return new PrandtlNumber( id, reg, cpT_, muT_,lambdaT_ );
}
//--------------------------------------------------------------------

#endif
