#ifndef SootSource_Expr_h
#define SootSource_Expr_h

#include <gas/TarAndSootInfo.h>
#include <expression/Expression.h>

/**
 *  @class SootSource
 *  @author Josh McConnell
 *  @date   February, 2015
 *  @brief Calculates the source term, in (kg)/(m^3)-s, for soot transport
 *
 *
 *  @param density_:        system density
 *  @param rSootForm_:      soot formation rate     (1/s)
 *  @param rSootAgg_:       soot agglomeration rate (1/s)
 */
template< typename ScalarT >
class SootSource
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS( ScalarT, density_, rSootForm_, rSootOx_ )

  /*
   *   density_:        system density
   *   rSootForm_:      soot formation rate (1/s)
   *   rSootAgg_:       soot agglomeration rate (1/s)
   */
  
  const TarAndSootInfo& tarSootInfo_;
  const double mwRatio_; // (mw_soot/mw_tar)

    SootSource( const Expr::Tag& densityTag,
              const Expr::Tag& rSootFormTag,
              const Expr::Tag& rSootOxTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a SootSource expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& rSootFormTag,
             const Expr::Tag& rSootOxTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag densityTag_, rSootFormTag_, rSootOxTag_;
  };

  ~SootSource();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
SootSource<ScalarT>::
SootSource( const Expr::Tag& densityTag,
            const Expr::Tag& rSootFormTag,
            const Expr::Tag& rSootOxTag )
  : Expr::Expression<ScalarT>(),
tarSootInfo_( TarAndSootInfo::self() ),
  mwRatio_    ( tarSootInfo_.soot_mw()*tarSootInfo_.tar_carbon()/tarSootInfo_.tar_mw() )
{
	density_    = this->template create_field_request<ScalarT>( densityTag     );
	rSootForm_  = this->template create_field_request<ScalarT>( rSootFormTag   );
	rSootOx_    = this->template create_field_request<ScalarT>( rSootOxTag     );
}

//--------------------------------------------------------------------

template< typename ScalarT >
SootSource<ScalarT>::
~SootSource()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootSource<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootSource<ScalarT>::
evaluate()
{
  using namespace SpatialOps;
    ScalarT& result = this->value();

    const ScalarT& density   = density_  ->field_ref();
    const ScalarT& rSootForm = rSootForm_->field_ref();
    const ScalarT& rSootOx   = rSootOx_  ->field_ref();

    result <<= ( mwRatio_*rSootForm - rSootOx );

}

//--------------------------------------------------------------------

template< typename ScalarT >
SootSource<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& rSootFormTag,
                  const Expr::Tag& rSootOxTag )
  : ExpressionBuilder( resultTag    ),
    densityTag_      ( densityTag   ),
    rSootFormTag_    ( rSootFormTag ),
    rSootOxTag_      ( rSootOxTag   )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
SootSource<ScalarT>::
Builder::build() const
{
  return new SootSource<ScalarT>( densityTag_,rSootFormTag_, rSootOxTag_ );
}


#endif // SootSource_Expr_h
