#include <gas/EvattICReader.h>

#include <pokitt/CanteraObjects.h>

#include <algorithm>
#include <fstream>
#include <iomanip>

#include <StringNames.h>

using std::endl;
using std::cout;
using std::string;

//--------------------------------------------------------------------

EvattICReader::
EvattICReader( const std::string& filename,
               const unsigned int npts,
               const unsigned int nspec )
  : npts_ ( npts ),
    nspec_( nspec ),

    temp_ ( npts, 0.0 ),
    press_( npts, 0.0 ),
    uvel_ ( npts, 0.0 ),
    vvel_ ( npts, 0.0 ),
    wvel_ ( npts, 0.0 )
{
  using namespace std;

  for( unsigned int i=0; i<nspec; ++i )
    ysp_.push_back( std::vector<double>(npts,0.0) );


  // open and read this binary file.  Note that the "reinterpret_cast"
  // business seems to be required for binary file i/o in c++.

  ifstream file( filename.c_str(), ios_base::in | ios_base::binary );
  if( !file.good() ){
    std::ostringstream errmsg;
    errmsg << "ERROR: could not open file '" << filename << "'" << std::endl;
    throw std::runtime_error( errmsg.str() );
  }

  for( unsigned int i=0; i<npts; ++i ){

    // species mass fractions
    for( unsigned int isp=0; isp!=nspec; ++isp ){
      file.read( reinterpret_cast<char*>(&ysp_[isp][i]), sizeof(double) );
    }

    file.read( reinterpret_cast<char*>(&temp_ [i]), sizeof(double) );   // temperature
    file.read( reinterpret_cast<char*>(&press_[i]), sizeof(double) );   // pressure
    file.read( reinterpret_cast<char*>(&uvel_ [i]), sizeof(double) );   // x-velocity
    file.read( reinterpret_cast<char*>(&vvel_ [i]), sizeof(double) );   // y-velocity
    file.read( reinterpret_cast<char*>(&wvel_ [i]), sizeof(double) );   // z-velocity

    if( !file.good() ){
      std::ostringstream errmsg;
      errmsg << "ERROR reading file '" << filename << "' for grid point " << i+1
             << " of " << npts << " (" << get_npts() << ")" << std::endl;
      throw std::runtime_error( errmsg.str() );
    }

  } // grid loop

  file.close();

  cout << "Read file: " << filename << endl;

}

//--------------------------------------------------------------------

EvattICReader::~EvattICReader()
{}

//--------------------------------------------------------------------

//====================================================================

//--------------------------------------------------------------------

EvattICExpr::
EvattICExpr( const FilePtr file,
             const EvattICReader::FieldSelector fs,
             const int specNum )
  : Expr::Expression<VolField>(),
    fs_( fs ),
    file_( file ),
    specNum_( specNum )
{}

//--------------------------------------------------------------------

EvattICExpr::~EvattICExpr()
{}

//--------------------------------------------------------------------

void
EvattICExpr::evaluate()
{
  VolField& field = this->value();

  const unsigned int npts = field.window_without_ghost().local_npts();
  if( npts != file_->get_npts() ){
    std::ostringstream msg;
    msg << "ERROR - incompatible number of points" << endl
        << "  Data file: " << file_->get_npts() << endl
        << "  Field    : " << npts << endl;
    throw std::runtime_error( msg.str() );
  }

  file_->set_field_value( field.interior_begin(), fs_, specNum_ );

  // now we need to set the ghost cells.  For now let's set them to be
  // equal to the first interior point
  assert( field.get_ghost_data().get_minus() == field.get_ghost_data().get_plus() );

  VolField::const_iterator f2=field.begin();
  VolField::iterator f1=field.begin();
  ++f2;
  *f1 = *f2;
  f1 = field.end() - 1;
  f2 = f1 - 1;
  *f1 = *f2;
}

//--------------------------------------------------------------------

EvattICExpr::Builder::
Builder( const FilePtr file,
         const Expr::Tag& resultTag )
  : ExpressionBuilder(resultTag),
    file_( file ),
    specNum_( -1 )
{
  const std::string& varName = resultTag.name();
  const StringNames& sName = StringNames::self();
  if     ( varName == sName.temperature )  fs_ = EvattICReader::TEMPERATURE;
  else if( varName == sName.pressure    )  fs_ = EvattICReader::PRESSURE;
  else if( varName == sName.xvel        )  fs_ = EvattICReader::X_VELOCITY;
  else if( varName == sName.yvel        )  fs_ = EvattICReader::Y_VELOCITY;
  else if( varName == sName.zvel        )  fs_ = EvattICReader::Z_VELOCITY;
  else{
    fs_ = EvattICReader::MASS_FRACTION;

    typedef std::vector<std::string> StringVec;
    const StringVec& specNames = CanteraObjects::species_names();
    const StringVec::const_iterator ispec = std::find( specNames.begin(), specNames.end(), varName );
    if( ispec == specNames.end() ){
      std::ostringstream msg;
      msg << "ERROR - '" << varName << "' is not available for extraction from the input file." << endl
          << "Available variables follow:" << endl
          << "  " << sName.temperature
          << "  " << sName.pressure
          << "  " << sName.xvel
          << "  " << sName.yvel
          << "  " << sName.zvel;
      BOOST_FOREACH( const std::string& sp, specNames ){
          msg << "  " << sp;
      }
      msg << std::endl;
      throw std::runtime_error( msg.str() );
    }

    // determine which species.
    specNum_ = ispec - specNames.begin();
  }
}

//--------------------------------------------------------------------

Expr::ExpressionBase*
EvattICExpr::Builder::build() const
{
  return new EvattICExpr( file_, fs_, specNum_ );
}

//--------------------------------------------------------------------
