#include <StringNames.h>
#include <gas/PositionEquation.h>

using Expr::STATE_N;
using Expr::Tag;

//--------------------------------------------------------------------

PositionEquation::
PositionEquation( Expr::ExpressionFactory& exprFactory,
                  const double coflowvel)
  : Expr::TransportEquation( StringNames::self().position,
                             Tag( StringNames::self().position+"RHS", STATE_N ) )
{
  const StringNames& sName = StringNames::self();
  exprFactory.register_expression( new RHSBuilder( Tag( StringNames::self().position+"RHS", STATE_N ),
                                                   coflowvel,
                                                   Tag( sName.yvel,    STATE_N ),
                                                   Tag( sName.density, STATE_N ),
                                                   Tag( sName.xcoord,  STATE_N ) ) );
}

//--------------------------------------------------------------------

PositionEquation::~PositionEquation()
{}

//--------------------------------------------------------------------

void
PositionEquation::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{}

//--------------------------------------------------------------------

Expr::ExpressionID
PositionEquation::initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const StringNames& sName = StringNames::self();
  return exprFactory.get_id( Tag(sName.position,STATE_N) );
}

//--------------------------------------------------------------------
