#ifndef SootAgglomerationRate_h
#define SootAgglomerationRate_h

#include <expression/Expression.h>
#include <gas/TarAndSootInfo.h>

/**
 *  @class SootFormationRate
 *  @author Josh McConnell
 *  @date   January, 2015
 *  @brief Calculates the rate of soot agglomeration in (kg soot)/(m^3-s), which is required to
 *         calculate source term for the transport equations  of soot particle number density
 *
 *  The rate of soot formation is given as
 * \f[

 *     r_{Agg\mathrm{soot}} = SA_{/mathrm{soot particle}}* P_{O_{2}}* T^{-1/2} * A_{Agg\mathrm{soot}} * exp(-E_{Agg\mathrm{soot}} / RT),
 *
 * \f]
 *  <ul>
 *
 *  <li> where \f$ SA_{/mathrm{soot particle}} \f$ is the total surface area of the soot particles
 *
 *   <li> and \f$ P_{O_{2}} \f$ is the partial pressure of dioxygen.
 *
 *  </ul>
 *
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 *
 * @param density_:        system density
 * @param temp_:           system temperature
 * @param ySoot_:          (kg soot)/(kg total)
 * @param nSootParticle_:  (soot particles)/(kg total)
 * @param boltzConst_      (J/K) Boltzmann's constant
 * @param ca_              collision constant                    [1]
 * @param sootDens_        (kg/m^3) soot density                 [1]
 */
template< typename ScalarT >
class SootAgglomerationRate
 : public Expr::Expression<ScalarT>
{
  DECLARE_FIELDS(ScalarT, density_, temp_, ySoot_, nSootParticle_ )
  const double boltzConst_, ca_, pi_, sootDens_;

    SootAgglomerationRate( const Expr::Tag& densityTag,
                           const Expr::Tag& tempTag,
                           const Expr::Tag& ySootTag,
                           const Expr::Tag& nSootParticleTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a SootAgglomerationRate expression
     *  @param result the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& tempTag,
             const Expr::Tag& ySootTag,
             const Expr::Tag& nSootParticleTag );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag densityTag_, tempTag_, ySootTag_, nSootParticleTag_;
  };

  ~SootAgglomerationRate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ScalarT >
SootAgglomerationRate<ScalarT>::
SootAgglomerationRate( const Expr::Tag& densityTag,
                 const Expr::Tag& tempTag,
                 const Expr::Tag& ySootTag,
                 const Expr::Tag& nSootParticleTag )
  : Expr::Expression<ScalarT>(),
    boltzConst_( 1.3806488*10E-23                     ),
    ca_        ( 3.0                                  ), //[1]
    pi_        ( 3.141592654                          ),
    sootDens_  ( TarAndSootInfo::self().soot_density() )
{
	density_       = this->template create_field_request<ScalarT>( densityTag       );
	temp_          = this->template create_field_request<ScalarT>( tempTag          );
	ySoot_         = this->template create_field_request<ScalarT>( ySootTag         );
	nSootParticle_ = this->template create_field_request<ScalarT>( nSootParticleTag );
}

//--------------------------------------------------------------------

template< typename ScalarT >
SootAgglomerationRate<ScalarT>::
~SootAgglomerationRate()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootAgglomerationRate<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB ){}

//--------------------------------------------------------------------

template< typename ScalarT >
void
SootAgglomerationRate<ScalarT>::

evaluate()
{
  using namespace SpatialOps;
  ScalarT& result = this->value();

  const ScalarT& density       = density_      ->field_ref();
  const ScalarT& temp          = temp_         ->field_ref();
  const ScalarT& ySoot         = ySoot_        ->field_ref();
  const ScalarT& nSootParticle = nSootParticle_->field_ref();


  result <<= 2 * density * ca_
               * pow (6.0 / (pi_ * sootDens_),              1.0/6.0  )
               * sqrt(6.0 * boltzConst_ * temp / sootDens_           )
               * pow (density * ySoot,                      1.0/6.0  )
               * pow (density * nSootParticle,              11.0/6.0 );

}

//--------------------------------------------------------------------

template< typename ScalarT >
SootAgglomerationRate<ScalarT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& tempTag,
                  const Expr::Tag& ySootTag,
                  const Expr::Tag& nSootParticleTag )
  : ExpressionBuilder( resultTag ),
    densityTag_      ( densityTag        ),
    tempTag_         ( tempTag           ),
    ySootTag_        ( ySootTag          ),
    nSootParticleTag_( nSootParticleTag  )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
SootAgglomerationRate<ScalarT>::
Builder::build() const
{
  return new SootAgglomerationRate<ScalarT>( densityTag_,tempTag_,ySootTag_,nSootParticleTag_ );
}


#endif // SootAgglomerationRate_h
