#ifndef TemperatureFromIntEnergy_h
#define TemperatureFromIntEnergy_h

#include <expression/ExprLib.h>

#include <vector>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/IdealGasMix.h>

#include <spatialops/structured/FieldHelper.h>  // for write_matlab

template< typename FieldT >
class TemperatureFromE0
  : public Expr::Expression<FieldT>
{
  Cantera::IdealGasMix* const gas_;
  const int nspec_;

  DECLARE_FIELDS( FieldT, e0_, ke_, rho_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, species_ )

  typedef typename FieldT::iterator       FieldIter;
  typedef typename FieldT::const_iterator ConstFieldIter;

  std::vector<double> ptSpec_;
  std::vector<ConstFieldIter> specIVec_;

  TemperatureFromE0( const Expr::Tag& e0Tag,
                     const Expr::Tag& keTag,
                     const Expr::Tag& rhoTag,
                     const Expr::TagList& yiTag );

public:

  ~TemperatureFromE0();

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag e0Tag_, keTag_, rhoTag_;
    const Expr::TagList yiTags_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& e0Tag,
             const Expr::Tag& keTag,
             const Expr::Tag& rhoTag,
             const Expr::TagList& yiTag );
    Expr::ExpressionBase* build() const;
  };

  void evaluate();
};





// ###################################################################
//
//                           Implementation
//
// ###################################################################





template< typename FieldT >
TemperatureFromE0<FieldT>::
TemperatureFromE0( const Expr::Tag& e0Tag,
                   const Expr::Tag& keTag,
                   const Expr::Tag& rhoTag,
                   const Expr::TagList& yiTags )
  : Expr::Expression<FieldT>(),
    gas_( CanteraObjects::get_gasmix() ),
    nspec_( gas_->nSpecies() )
{
  e0_  = this->template create_field_request<FieldT>( e0Tag  );
  ke_  = this->template create_field_request<FieldT>( keTag  );
  rho_ = this->template create_field_request<FieldT>( rhoTag );

  this->template create_field_vector_request<FieldT>( yiTags, species_ );

  ptSpec_.resize(nspec_,0.0);
}

//--------------------------------------------------------------------

template< typename FieldT >
TemperatureFromE0<FieldT>::
~TemperatureFromE0()
{
  assert( gas_ != NULL );
  CanteraObjects::restore_gasmix( gas_ );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
TemperatureFromE0<FieldT>::
evaluate()
{
  FieldT& temp = this->value();

  FieldIter itemp = temp.begin();
  const FieldIter itempe = temp.end();

  ConstFieldIter ie0  = e0_ ->field_ref().begin();
  ConstFieldIter ike  = ke_ ->field_ref().begin();
  ConstFieldIter irho = rho_->field_ref().begin();

  specIVec_.clear();
  for( size_t i=0; i<species_.size(); ++i ){
    specIVec_.push_back( species_[i]->field_ref().begin() );
  }

  // calculate temperature at each point
  for( size_t i=0; itemp!=itempe; ++itemp, ++ie0, ++ike, ++irho, ++i ){

    // extract composition and pack it into a temporary buffer.
    typedef typename std::vector<ConstFieldIter>::iterator SpIter;
    SpIter isp=specIVec_.begin();
    const SpIter ispe=specIVec_.end();
    std::vector<double>::iterator ipt = ptSpec_.begin();
    for( ; isp!=ispe; ++ipt, ++isp ){
      *ipt = **isp;
      ++(*isp);
    }

    // calculate the internal energy from the total internal energy
    const double intEnerg = *ie0 - *ike;

    // calculate the temperature from composition, internal energy, and pressure:
    try{
      // pressure doesn't matter here...
      gas_->setState_TPY( *itemp, 101325, &ptSpec_[0] );
      gas_->setState_UV( intEnerg, 1.0/(*irho), 1e-8 );
      *itemp = gas_->temperature();
    }
    catch( Cantera::CanteraError ){
      std::cout << "Error in Temperature solve at grid point " << i << std::endl
                << " E0=" << *ie0 << std::endl
                << " KE=" << *ike << std::endl
                << " e=" << intEnerg << std::endl
                << " Tguess=" << *itemp << std::endl
                << " Yi=";
      double ysum=0;
      for( int i=0; i<nspec_; ++i ){
	std::cout << ptSpec_[i] << " ";
	ysum += ptSpec_[i];
      }
      std::cout << std::endl << "sum Yi = " << ysum << std::endl;

      SpatialOps::write_matlab(temp,"T_fail");
      SpatialOps::write_matlab(ke_->field_ref(),"ke_fail");
      SpatialOps::write_matlab(e0_->field_ref(),"e0_fail");
      int i=0;
      for( size_t i=0; i<species_.size(); ++i ){
        const std::string spname2 = "Y_" + CanteraObjects::species_name(i) + "_fail";
        SpatialOps::write_matlab( species_[i]->field_ref(), spname2 );
      }

      Cantera::showErrors();

      throw std::runtime_error("Problems in Temperature expression.");
    }
  } // grid loop
}

//--------------------------------------------------------------------

template< typename FieldT >
TemperatureFromE0<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& e0Tag,
         const Expr::Tag& keTag,
         const Expr::Tag& rhoTag,
         const Expr::TagList& yiTags )
: ExpressionBuilder(result),
  e0Tag_  ( e0Tag  ),
  keTag_  ( keTag  ),
  rhoTag_ ( rhoTag ),
  yiTags_  ( yiTags  )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
TemperatureFromE0<FieldT>::Builder::build() const
{
  return new TemperatureFromE0<FieldT>( e0Tag_, keTag_, rhoTag_, yiTags_ );
}

//--------------------------------------------------------------------

#endif
