#ifndef SpeciesPrimVar_h
#define SpeciesPrimVar_h

#include <expression/ExprLib.h>

template<typename FieldT>
class SpeciesPrimVar
  : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, rhoPhi_, rho_ )

  SpeciesPrimVar( const Expr::Tag& rhoPhiTag,
                  const Expr::Tag& rhoTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag rhoPhiTag_, rhoTag_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& rhoPhiTag,
	     const Expr::Tag& rhoTag );
    Expr::ExpressionBase* build() const;
  };

  void evaluate();
};





// ###################################################################
//
//                           Implementation
//
// ###################################################################





template< typename FieldT >
SpeciesPrimVar<FieldT>::
SpeciesPrimVar( const Expr::Tag& rhoPhiTag,
                const Expr::Tag& rhoTag )
  : Expr::Expression<FieldT>()
{
  this->set_gpu_runnable(true);

  rhoPhi_ = this->template create_field_request<FieldT>( rhoPhiTag );
  rho_    = this->template create_field_request<FieldT>( rhoTag    );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
SpeciesPrimVar<FieldT>::
evaluate()
{
  using namespace SpatialOps;
  FieldT& phi = this->value();
  const FieldT& rhoPhi = rhoPhi_->field_ref();
  const FieldT& rho    = rho_   ->field_ref();
  phi <<= min( 1.0, max( 0.0, rhoPhi / rho ) );
}

//--------------------------------------------------------------------

template< typename FieldT >
SpeciesPrimVar<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& rhoPhiTag,
         const Expr::Tag& rhoTag )
  : ExpressionBuilder(result),
    rhoPhiTag_( rhoPhiTag ), rhoTag_( rhoTag )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
SpeciesPrimVar<FieldT>::Builder::build() const
{
  return new SpeciesPrimVar<FieldT>( rhoPhiTag_, rhoTag_ );
}

//--------------------------------------------------------------------


#endif // SpeciesPrimVar_h
