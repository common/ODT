#ifndef SpeciesDiffusion_h
#define SpeciesDiffusion_h

#include <expression/ExprLib.h>
#include <spatialops/structured/stencil/FVStaggeredOperatorTypes.h>

#include <pokitt/CanteraObjects.h>
#include <cantera/IdealGasMix.h>
#include <cantera/transport.h>

/**
 *  \class  SpeciesDiffFlux
 *  \author Nathan Yonkee
 *  \date December, 2014
 *
 *  \brief Calculates the mass diffusive flux for each species relative to the mass averaged velocity [kg/m^2].
 *
 *  This class calculates the mass flux for each species relative to the mass averaged velocity.
 *  Mixture averaged diffusion coefficients are calculated such that
 *
 *  \f[
 *  j_i = -n D_i \nabla x_i
 *  \f]
 *
 *  where \f$ j_i \f$ is the mass flux, \f$ n \f$ is the molar density, \f$ D_i \f$ is the diffusion coefficient
 *  and \f$ x_i \f$ is the gradient of the mol fraction.
 *
 *  Using mass terms on the RHS, this is equivalent to
 *
 *  \f[
 *  j_i = -\frac{\rho}{MMW} D_i \nabla (y_i MMW)
 *  \f]
 *
 *  where \f$ MMW \f$ is the mixture molecular weight.
 *
 *  \tparam ScalarT the type of the scalar field (diffusivity, mole fraction)
 *  \tparam FluxT the type of the flux field.
 */
template< typename FluxT >
class SpeciesDiffFlux : public Expr::Expression< FluxT >
{
  typedef typename SpatialOps::VolType<FluxT>::VolField  ScalarT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Gradient,   ScalarT,FluxT>::type GradT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Interpolant,ScalarT,FluxT>::type InterpT;

public:

  typedef typename Expr::Expression<FluxT>::ValVec SpecFluxT;

  typedef SpatialOps::SpatFldPtr<  FluxT> FluxPtrT;
  typedef SpatialOps::SpatFldPtr<ScalarT> ScalarPtrT;

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a SpeciesDiffFlux expression
     *  @param fluxes the tags for the mass diffusive flux
     *  @param massFracs mass fractions
     *  @param densTag mass density
     *  @param mmwTag mixture molecular weight
     *  @param diffCoeffTags diffusion coefficients
     */
    Builder( const Expr::TagList& fluxes,
             const Expr::TagList& massFracs,
             const Expr::Tag& densTag,
             const Expr::Tag& mmwTag,
             const Expr::TagList& diffCoeffTags);
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::Tag densTag_, mmwTag_;
    const Expr::TagList yiTags_, diffCoeffTags_;
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  protected:

  SpeciesDiffFlux( const Expr::TagList& massFracs,
                   const Expr::Tag& densTag,
                   const Expr::Tag& mmwTag,
                   const Expr::TagList& diffCoeffTags);

  ~SpeciesDiffFlux(){}

  const GradT*   gradOp_;
  const InterpT* interpOp_;

  DECLARE_FIELDS( ScalarT, density_, mmw_ )

  DECLARE_VECTOR_OF_FIELDS( ScalarT, species_    )
  DECLARE_VECTOR_OF_FIELDS( ScalarT, diffCoeffs_ )
};

//====================================================================

/**
 *  @class  SpeciesDiffFluxFromYs
 *  @author Amir Biglari
 *
 *  @brief Calculates the mixture-averaged form of the diffusive fluxes 
 *  for all species. This uses the mixture-averaged diffussion
 *  coefficients provided from uintah, Which is related to the mass 
 *  diffusive flux, \f$ j_i \f$ with respect to the mass-average velocity in terms
 *  of the mass fraction gradient. Therefore, the equation f0r the the 
 *  mixture-averaged form of the diffusive fluxes is defined as
 * \f[
 *     j_i = -\rho \it{D_i}^{mix} \nabla y_i,
 * \f]
 *
 */
template< typename FluxT >
class SpeciesDiffFluxFromYs
    : public Expr::Expression< FluxT >
{
  typedef typename SpatialOps::VolType<FluxT>::VolField ScalarT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Gradient,   ScalarT,FluxT>::type GradT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Interpolant,ScalarT,FluxT>::type InterpT;

  typedef typename Expr::Expression<FluxT>::ValVec SpecFluxT;
  typedef SpatialOps::SpatFldPtr<FluxT> FluxPtrT;
  
  SpeciesDiffFluxFromYs( const Expr::Tag& temperature,
                         const Expr::TagList& massFrac,
                         const Expr::Tag& densTag );
  
  ~SpeciesDiffFluxFromYs();
  
  Cantera::MixTransport* const trans_;
  Cantera::ThermoPhase& gas_;

  const int nspec_;
  const double pressure_;
  
  const Expr::TagList specTags_;
  
  const GradT*   gradOp_;
  const InterpT* interpOp_;
  
  DECLARE_FIELDS( ScalarT, temperature_, density_ )
  DECLARE_VECTOR_OF_FIELDS( ScalarT, species_ )
  
  std::vector<FluxPtrT> yInterp_;
  
  std::vector<double> ptSpec_, ptDiffCoeff_;//, ptRhoGrad_, ptFlux_
  
  std::vector< typename FluxT::const_iterator > specIVec_;
  std::vector< typename FluxT::iterator       > fluxIVec_;

public:
  
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& fluxes,
             const Expr::Tag& temperatureTag,
             const Expr::TagList& massFrac,
             const Expr::Tag& densTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList yiT_;
    const Expr::Tag tT_, densT_;
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
};


//====================================================================

/**
 *  @class SimpleSpeciesDiffusiveFlux
 *  @date   February, 2018
 *  @brief Calculates a species diffusive fluxes
 *
 *
 *  @param rho_      : gas phase density
 *  @param diffCoeff_: mass-averaged diffusivity used for all species diffusive fluxes
 *  @param species   : species mass fractions
 */

template< typename FluxT >
class SimpleSpeciesDiffFlux : public Expr::Expression< FluxT >
{
  typedef typename SpatialOps::VolType<FluxT>::VolField  ScalarT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Gradient,   ScalarT,FluxT>::type GradT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Interpolant,ScalarT,FluxT>::type InterpT;

  typedef typename Expr::Expression<FluxT>::ValVec SpecFluxT;

  SimpleSpeciesDiffFlux( const Expr::Tag& diffCoeffTag,
                         const Expr::TagList& speciesTags,
                         const Expr::Tag& rhoTag );

  ~SimpleSpeciesDiffFlux(){};

  const GradT* gradOp_;
  const InterpT* interpOp_;
  DECLARE_VECTOR_OF_FIELDS( ScalarT, species_ )
  DECLARE_FIELDS( ScalarT, rho_, diffCoeff_ )

public:

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& result,
             const int nspec,
             const Expr::Tag& diffCoeffTag,
             const Expr::TagList& massFracs,
             const Expr::Tag& densTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::TagList yiTags_;
    const Expr::Tag rhoTag_, diffCoeffTag_;
  };


  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
};



// ###################################################################
//
//                         Implementation
//
// ###################################################################




//--------------------------------------------------------------------
template<typename FluxT>
SpeciesDiffFlux<FluxT>::
SpeciesDiffFlux( const Expr::TagList& yiTags,
                 const Expr::Tag& densTag,
                 const Expr::Tag& mmwTag,
                 const Expr::TagList& diffCoeffTags )
  : Expr::Expression<FluxT>()
{
  density_ = this->template create_field_request<ScalarT>( densTag );
  mmw_     = this->template create_field_request<ScalarT>( mmwTag  );

  this->template create_field_vector_request<ScalarT>( yiTags,        species_    );
  this->template create_field_vector_request<ScalarT>( diffCoeffTags, diffCoeffs_ );
}
//--------------------------------------------------------------------
template<typename FluxT>
void
SpeciesDiffFlux<FluxT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}
//--------------------------------------------------------------------
template<typename FluxT>
void
SpeciesDiffFlux<FluxT>::evaluate()
{
  using namespace SpatialOps;
  SpecFluxT& fluxes = this->get_value_vec();

  const ScalarT& density = density_->field_ref();
  const ScalarT& mmw     = mmw_    ->field_ref();


  ScalarPtrT molConc = SpatialFieldStore::get<ScalarT>( density );
  *molConc <<= density / mmw;

  FluxPtrT fluxSum = SpatialFieldStore::get<FluxT>( *(fluxes[0]) );
  *fluxSum <<= 0.0;

  for( size_t i=0; i<species_.size(); ++i ){
    const ScalarT& yi       = species_   [i]->field_ref();
    const ScalarT& diffCoef = diffCoeffs_[i]->field_ref();
    FluxT& flux = *fluxes[i];

    // convert mass fraction gradients to mole fraction gradients
    // $ M_i $ cancel out when converting from molar flux to mass flux
    flux <<= - (*interpOp_) ( *molConc * diffCoef ) * (*gradOp_) ( yi * mmw );
    *fluxSum <<= *fluxSum + flux;
  }

  // enforce the sum of fluxes to equal 0
  for( size_t i=0; i<species_.size(); ++i ){
    const ScalarT& yi = species_[i]->field_ref();
    FluxT& flux = *fluxes[i];
    flux <<= flux - *fluxSum * (*interpOp_) ( yi );
  }
}


//====================================================================


//--------------------------------------------------------------------
template<typename FluxT>
SpeciesDiffFlux<FluxT>::Builder::
Builder( const Expr::TagList& result,
         const Expr::TagList& massFracs,
         const Expr::Tag& densTag,
         const Expr::Tag& mmwTag,
         const Expr::TagList& diffCoeffTags)
: ExpressionBuilder(result),
  densTag_ ( densTag ),
  yiTags_( massFracs    ),
  mmwTag_(mmwTag),
  diffCoeffTags_(diffCoeffTags)
{}
//--------------------------------------------------------------------
template<typename FluxT>
Expr::ExpressionBase*
SpeciesDiffFlux<FluxT>::Builder::build() const
{
  return new SpeciesDiffFlux<FluxT>( yiTags_, densTag_, mmwTag_, diffCoeffTags_ );
}

//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------
template<typename FluxT>
SpeciesDiffFluxFromYs<FluxT>::
SpeciesDiffFluxFromYs( const Expr::Tag& temperature,
                       const Expr::TagList& yiTags,
                       const Expr::Tag& densTag  )
  : Expr::Expression<FluxT>(),
    trans_( dynamic_cast<Cantera::MixTransport*>(CanteraObjects::get_transport()) ),
    gas_     ( trans_->thermo() ),
    nspec_   ( gas_.nSpecies()  ),
    pressure_( gas_.pressure()  )
{
  density_     = this->template create_field_request<ScalarT>( densTag     );
  temperature_ = this->template create_field_request<ScalarT>( temperature );
  this->template create_field_vector_request( yiTags, species_ );

  ptSpec_.resize     ( nspec_ );
  ptDiffCoeff_.resize( nspec_ );
}
//--------------------------------------------------------------------
template<typename FluxT>
SpeciesDiffFluxFromYs<FluxT>::~SpeciesDiffFluxFromYs()
{
  CanteraObjects::restore_transport( trans_ );
}
//--------------------------------------------------------------------
template<typename FluxT>
void
SpeciesDiffFluxFromYs<FluxT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}
//--------------------------------------------------------------------
template<typename FluxT>
void
SpeciesDiffFluxFromYs<FluxT>::evaluate()
{
  using namespace SpatialOps;
  SpecFluxT& fluxes = this->get_value_vec();
  
  FluxPtrT tempInterp = SpatialFieldStore::get<FluxT>( *(fluxes[0]) );
  
  yInterp_.clear();
  for( size_t i=0; i<nspec_; ++i ){
    yInterp_.push_back( SpatialFieldStore::get<FluxT>( *(fluxes[0]) ) );
  }
  
  // calculate the gradient of each species mass fraction times negative of the density.
  for( size_t i=0; i<species_.size(); ++i ){
    const ScalarT& yi = species_[i]->field_ref();
    *fluxes[i] <<= - (*gradOp_)( yi ) * (*interpOp_)( density_->field_ref() );
  }
  
  // interpolate species mass fractions to faces
  for( size_t i=0; i<species_.size(); ++i ){
    interpOp_->apply_to_field( species_[i]->field_ref(), *yInterp_[i] );
  }
  // interpolate temperature to faces
  interpOp_->apply_to_field( temperature_->field_ref(), *tempInterp );
  
  // pack a vector with species iterators.
  specIVec_.clear();
  for( size_t i=0; i<yInterp_.size(); ++i ){
    specIVec_.push_back( yInterp_[i]->interior_begin() );
  }
  
  // pack a vector with species flux iterators.
  fluxIVec_.clear();
  for( size_t i=0; i<fluxes.size(); ++i ){
    fluxIVec_.push_back( fluxes[i]->interior_begin() );
    *fluxes[i]->begin() = 0.0;
    *(fluxes[i]->end()-1) = 0.0;
  }
  
  typename       FluxT::const_iterator itemp  = tempInterp->interior_begin();
  const typename FluxT::const_iterator itempe = tempInterp->interior_end();

  double fluxSum;
  // loop over points to calculate fluxes.
  for( ; itemp!=itempe; ++itemp ){
    fluxSum = 0.0;
    // extract compositions and pack them into the temporary buffer
    std::vector<double>::iterator ipt = ptSpec_.begin();
    for( typename std::vector<typename FluxT::const_iterator>::const_iterator ispec = specIVec_.begin();
        ispec!=specIVec_.end();
        ++ipt, ++ispec )
    {
      *ipt = **ispec;
    }
    
    // extract composition gradients times negative of the density and pack them into the temporary buffer
//    ipt = ptRhoGrad_.begin();
//    for( typename std::vector<typename FluxT::iterator>::const_iterator iflux = fluxIVec_.begin();
//        iflux != fluxIVec_.end();
//        ++iflux, ++ipt )
//    {
//      *ipt = **iflux;
//    }
    
    // calculate the mixture-averaged diffussion coefficients provided from uintah, Which is
    // related to the mass diffusive flux, \f$ j_i \f$ with respect to the mass-average velocity
    // in terms of the mass fraction gradient.
//    const doublereal* y;
    try{
      // note that we should really use the spatially varying pressure here...
      gas_.setState_TPY( *itemp, pressure_, &ptSpec_[0] );
      
      trans_->getMixDiffCoeffsMass(&ptDiffCoeff_[0]);
//      y  = trans_->thermo().massFractions();
    }
    catch( Cantera::CanteraError ){
      std::cout << "Error in SpeciesDiffFluxFromYs" << std::endl
      << " T=" << *itemp << ", p=" << pressure_ << std::endl
      << " Yi=";
      double ysum=0;
      for( int i=0; i<nspec_; ++i ){
        std::cout << ptSpec_[i] << " ";
        ysum += ptSpec_[i];
      }
      std::cout << std::endl << "sum Yi = " << ysum << std::endl;
      throw std::runtime_error("Problems in SpeciesDiffFluxFromYs expression.");
    }
    
    // calculate the fluxes
//    std::vector<double>::iterator idiff = ptDiffCoeff_.begin(), iflux = ptFlux_.begin();
//    for( std::vector<double>::iterator irhograd = ptRhoGrad_.begin();
//        irhograd!=ptRhoGrad_.end();
//        ++irhograd, ++iflux, ++idiff )
//    {
//      *iflux *= *idiff * *irhograd;            // multiply mass fraction grad by mix MW
//    }
    
    // calculate the fluxes
    std::vector<double>::iterator idiff = ptDiffCoeff_.begin();
    for( typename std::vector<typename FluxT::iterator>::iterator iflux = fluxIVec_.begin();
        iflux!=fluxIVec_.end();
        ++iflux, ++idiff )
    {
      **iflux *= *idiff;
      fluxSum += **iflux;
    }
    
    // correct the fluxes and increment flux iterators and species iterators to the next point
    int i =0;
    typename std::vector<typename FluxT::iterator>::iterator iflux = fluxIVec_.begin();
    for( typename std::vector<typename FluxT::const_iterator>::iterator specVecIter=specIVec_.begin();
        specVecIter!=specIVec_.end();
        ++specVecIter, ++iflux, ++i )
    {
      // AB: We may need to sustitute the soecVarIter with y's extracted from cantera
      // const doublereal* y  = m_thermo->massFractions();
      **iflux -= **specVecIter * fluxSum ;
//      **iflux -= y[i] * fluxSum ;
      ++(*iflux);  // increment flux iterator to the next mesh point
      ++(*specVecIter); // increment species iterator to the next mesh point
    }
    
  } // loop over grid points.
  
}

//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------
template<typename FluxT>
SpeciesDiffFluxFromYs<FluxT>::Builder::
Builder( const Expr::TagList& result,
         const Expr::Tag& temperature,
         const Expr::TagList& massFrac,
         const Expr::Tag& densTag )
  : ExpressionBuilder(result),
    tT_ ( temperature ),
    densT_( densTag ),
    yiT_( massFrac )
{
}
//--------------------------------------------------------------------
template<typename FluxT>
Expr::ExpressionBase*
SpeciesDiffFluxFromYs<FluxT>::Builder::build() const
{
  return new SpeciesDiffFluxFromYs<FluxT>( tT_, yiT_, densT_ );
}
//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------
template< typename FluxT >
SimpleSpeciesDiffFlux<FluxT>::
SimpleSpeciesDiffFlux( const Expr::Tag& diffCoeffTag,
                       const Expr::TagList& yiTags,
                       const Expr::Tag& rhoTag )
  : Expr::Expression<FluxT>()
{
  rho_       = this->template create_field_request<ScalarT>( rhoTag       );
  diffCoeff_ = this->template create_field_request<ScalarT>( diffCoeffTag );
  this->template create_field_vector_request<ScalarT>( yiTags, species_ );
}
//--------------------------------------------------------------------
template< typename FluxT >
void
SimpleSpeciesDiffFlux<FluxT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}
//--------------------------------------------------------------------
template< typename FluxT >
void
SimpleSpeciesDiffFlux<FluxT>::evaluate()
{
  using namespace SpatialOps;
  SpecFluxT& fluxes = this->get_value_vec();

  SpatialOps::SpatFldPtr<FluxT> rhoInterp = SpatialOps::SpatialFieldStore::get<FluxT>( *fluxes[0] );
  *rhoInterp <<= (*interpOp_)( rho_->field_ref() );

  SpatialOps::SpatFldPtr<FluxT> diffCoeffInterp = SpatialOps::SpatialFieldStore::get<FluxT>( *fluxes[0] );
  *diffCoeffInterp <<= (*interpOp_)( diffCoeff_->field_ref() );

  for( size_t i=0; i<fluxes.size(); ++i ){
    const ScalarT& yi = species_[i]->field_ref();
    FluxT& flux = *fluxes[i];
    flux <<= -(*diffCoeffInterp) * (*rhoInterp) * (*gradOp_)( yi );
    *(flux.begin()) = 0.0;
    *(flux.end()-1) = 0.0;
  }
}
//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------
template< typename FluxT >
SimpleSpeciesDiffFlux<FluxT>::Builder::
Builder( const Expr::TagList& result,
         const int nspecies,
         const Expr::Tag& diffCoeffTag,
         const Expr::TagList& yTags,
         const Expr::Tag& rhoTag )
: ExpressionBuilder(result),
  diffCoeffTag_( diffCoeffTag ),
  yiTags_      ( yTags        ),
  rhoTag_      ( rhoTag       )
{
  assert( size_t(nspecies) == yTags.size() );
}
//--------------------------------------------------------------------
template< typename FluxT >
Expr::ExpressionBase*
SimpleSpeciesDiffFlux<FluxT>::Builder::build() const
{
  return new SimpleSpeciesDiffFlux<FluxT>( diffCoeffTag_, yiTags_, rhoTag_ );
}
//--------------------------------------------------------------------

#endif
