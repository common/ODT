#ifndef DensityEquation_h
#define DensityEquation_h

#include <expression/ExprLib.h>

#include <OperatorsAndFields.h>
#include <gas/ScalarRHS.h>

class DensityEquation : public Expr::TransportEquation
{
public:

  DensityEquation( Expr::ExpressionFactory& exprFactory,
                   const bool doAdvection);

  ~DensityEquation();

  void setup_boundary_conditions( Expr::ExpressionFactory& );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

};


#endif // DensityEquation_h
