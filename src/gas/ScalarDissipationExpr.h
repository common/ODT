#ifndef ScalarDissipationExpr_h
#define ScalarDissipationExpr_h

#include <expression/ExprLib.h>

#include <spatialops/structured/SpatialFieldStore.h>  // scratch fields
#include <spatialops/SpatialOpsTools.h>   // IsSameType
#include <boost/static_assert.hpp>

/**
 *  @class ScalarDissipationExpr
 *  @brief Calculates the scalar dissipation rate in 1D
 *
 *  Given the viscosity, mixture fraction and Schmidt number, this
 *  calculates
 *  \f[ \chi = 2\frac{\lambda}{\rho c_p} \left( \frac{\partial f}{\partial x} \right)^2 \f]
 *  which essentially assumes that we have a unity lewis number
 *  (mixture fraction diffusivity equals heat diffusivity)
 *
 *  \par Template Parameters
 *  <ul>
 *  <li> \b FieldT - The field type for the scalar dissipation rate.
 *  </ul>
 */
template< typename ScalarT >
class ScalarDissipationExpr
  : public Expr::Expression< ScalarT >
{
  typedef typename SpatialOps::BasicOpTypes<ScalarT>::InterpF2CX InterpT;
  typedef typename SpatialOps::BasicOpTypes<ScalarT>::GradX      GradT;
  typedef typename GradT::DestFieldType FluxT;

  const GradT*   gradOp_;
  const InterpT* interpOp_;

  DECLARE_FIELDS( ScalarT, mixfr_, thermcond_, density_, cp_ )

  ScalarDissipationExpr( const Expr::Tag& mixfrTag,
                         const Expr::Tag& thermcondTag,
                         const Expr::Tag& densityTag,
                         const Expr::Tag& cpTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag mixfrTag_, thermcondTag_,densityTag_,cpTag_;
  public:
    Builder( const Expr::Tag& chiTag,
             const Expr::Tag& mixfrTag,
             const Expr::Tag& thermcondTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& cpTag );
    Expr::ExpressionBase* build() const;
  };

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};





// ###################################################################
//
//                           Implementation
//
// ###################################################################





template< typename ScalarT >
ScalarDissipationExpr<ScalarT>::
ScalarDissipationExpr( const Expr::Tag& mixfrTag,
                       const Expr::Tag& thermcondTag,
                       const Expr::Tag& densityTag,
                       const Expr::Tag& cpTag )
  : Expr::Expression<ScalarT>()
{
  mixfr_     = this->template create_field_request<ScalarT>( mixfrTag     );
  thermcond_ = this->template create_field_request<ScalarT>( thermcondTag );
  density_   = this->template create_field_request<ScalarT>( densityTag   );
  cp_        = this->template create_field_request<ScalarT>( cpTag        );
}

//--------------------------------------------------------------------

template< typename ScalarT >
void
ScalarDissipationExpr<ScalarT>::
evaluate()
{
  using namespace SpatialOps;
  ScalarT& chi = this->value();
  const ScalarT& f      = mixfr_    ->field_ref();
  const ScalarT& lambda = thermcond_->field_ref();
  const ScalarT& rho    = density_  ->field_ref();
  const ScalarT& cp     = cp_       ->field_ref();
  chi <<= 2.0*lambda / ( rho*cp ) * ( (*interpOp_)((*gradOp_)( f ) * (*gradOp_)( f )) );
}

//--------------------------------------------------------------------

template< typename ScalarT >
void
ScalarDissipationExpr<ScalarT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename ScalarT >
ScalarDissipationExpr<ScalarT>::Builder::
Builder( const Expr::Tag& chiTag,
         const Expr::Tag& mixfrTag,
         const Expr::Tag& thermcondTag,
         const Expr::Tag& densityTag,
         const Expr::Tag& cpTag)
  : ExpressionBuilder(chiTag),
    mixfrTag_     ( mixfrTag     ),
    thermcondTag_ ( thermcondTag ),
    densityTag_   ( densityTag   ),
    cpTag_        ( cpTag        )
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionBase*
ScalarDissipationExpr<ScalarT>::Builder::build() const
{
  return new ScalarDissipationExpr<ScalarT>( mixfrTag_, thermcondTag_, densityTag_, cpTag_ );
}

//--------------------------------------------------------------------


#endif // ScalarDissipationExpr_h
