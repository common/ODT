/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef RadiativeHeatFlux_Expr_h
#define RadiativeHeatFlux_Expr_h

#include <expression/Expression.h>

/**
 *  \class RadiativeHeatFlux
 */
template< typename FieldT >
class RadiativeHeatFlux
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELD( FieldT, gasTemp_ )
  DECLARE_FIELD( FieldT, pressure_ )
  DECLARE_FIELD( FieldT, xCoord_ )
  DECLARE_FIELD( FieldT, absCoeff_ )
  const double& wallTemp_;

  /* declare any operators associated with this expression here */
  
  RadiativeHeatFlux( const Expr::Tag& gasTempTag,
                     const Expr::Tag& pressureTag,
                     const Expr::Tag& xCoordTag,
                     const Expr::Tag& absCoeffTag,
                     const double&    wallTemp );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag gasTempTag_, pressureTag_, xCoordTag_, absCoeffTag_;
    const double wallTemp_;
  public:
    /**
     *  @brief Build a RadiativeHeatFlux expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& gasTempTag,
             const Expr::Tag& pressureTag,
             const Expr::Tag& xCoordTag,
             const Expr::Tag& absCoeffTag,
             const double&    wallTemp,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( resultTag, nghost ),
        gasTempTag_( gasTempTag ),
        pressureTag_( pressureTag ),
        xCoordTag_( xCoordTag ),
        absCoeffTag_( absCoeffTag ),
        wallTemp_   ( wallTemp )
    {}

    Expr::ExpressionBase* build() const{
      return new RadiativeHeatFlux<FieldT>( gasTempTag_,pressureTag_,xCoordTag_,absCoeffTag_,wallTemp_ );
    }
  };

  ~RadiativeHeatFlux(){}
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FieldT >
RadiativeHeatFlux<FieldT>::

RadiativeHeatFlux( const Expr::Tag& gasTempTag,
                   const Expr::Tag& pressureTag,
                   const Expr::Tag& xCoordTag,
                   const Expr::Tag& absCoeffTag,
                   const double& wallTemp )
  : Expr::Expression<FieldT>(),
    wallTemp_( wallTemp )
{
  gasTemp_ = this->template create_field_request<FieldT>( gasTempTag );
  pressure_ = this->template create_field_request<FieldT>( pressureTag );
  xCoord_ = this->template create_field_request<FieldT>( xCoordTag );
  absCoeff_ = this->template create_field_request<FieldT>( absCoeffTag );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
RadiativeHeatFlux<FieldT>::
evaluate()
{
  FieldT& result = this->value();

  const FieldT& gasTemp = gasTemp_->field_ref();
  const FieldT& pressure = pressure_->field_ref();
  const FieldT& xCoord = xCoord_->field_ref();
  const FieldT& absCoeff = absCoeff_->field_ref();

  result <<= -5.670373e-8*absCoeff*( pow(wallTemp_, 4) - pow(gasTemp, 4) );
}

//--------------------------------------------------------------------


#endif // RadiativeHeatFlux_Expr_h