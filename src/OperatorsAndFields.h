#ifndef ODT_OperatorsAndFields_h
#define ODT_OperatorsAndFields_h

/**
 * \file OperatorsAndFields.h
 * \brief Defines the types of fields and operators for use in ODT.
 */

#include <spatialops/structured/FVStaggered.h>
#include <spatialops/Nebo.h>

typedef SpatialOps::SVolField   VolField;
typedef SpatialOps::SSurfXField FaceField;

typedef SpatialOps::BasicOpTypes<VolField>  OpTypes;

typedef OpTypes::InterpC2FX   InterpOp;
typedef OpTypes::GradX        GradOp;
typedef OpTypes::DivX         DivOp;

typedef SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, FaceField, VolField >::type  InterpFaceCell;

#endif // ODT_OperatorsAndFields_h
