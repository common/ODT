#ifndef ParticletoCellMassExpr_h
#define ParticletoCellMassExpr_h

#include <expression/Expression.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/particles/ParticleFieldTypes.h>

#include "ParticleFieldsAndOperators.h"

namespace Particle{
//====================================================================


/**
 *  @class ParticletoCellMassExpr
 *  @author Naveen Punati
 *  @date   February, 2011
 *
 *  @brief calculates particle field influence on gas phase momentum.
 */
template< typename ParticletoCellOp >
class ParticletoCellMassExpr
  : public Expr::Expression<typename ParticletoCellOp::DestFieldType>
{
  typedef typename ParticletoCellOp::SrcFieldType  ParticleFieldT;
  typedef typename ParticletoCellOp::DestFieldType GasFieldT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pcT_, psT_,gcT_;
    const Expr::TagList  psrcTlist_;
    const double ca_;
  public:

    Builder( const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::TagList&,
             const Expr::Tag&,
             const double );

    Expr::ExpressionBase* build() const;
  };

  ~ParticletoCellMassExpr(){}

  void evaluate();
 void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  ParticletoCellMassExpr( const Expr::Tag&,
                          const Expr::Tag&,
                          const Expr::TagList&,
                          const Expr::Tag&,
                          const double );

  DECLARE_FIELD ( GasFieldT, gcoord_ )
  DECLARE_FIELDS( ParticleFieldT, parloc_, psize_)
  DECLARE_VECTOR_OF_FIELDS( ParticleFieldT, srcfields_ )

  ParticletoCellOp* p2c_;

  const double cellarea_;
};

// ###################################################################
//
//                           Implementation
//
// ###################################################################


//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellMassExpr<ParticletoCellOp>::
ParticletoCellMassExpr( const Expr::Tag& pcoordTag,
                        const Expr::Tag& psizeTag,
                        const Expr::TagList& psrcTagList,
                        const Expr::Tag& gcoordTag,
                        const double cellarea )
  : Expr::Expression<GasFieldT>(),
    cellarea_(cellarea)
{
  gcoord_ = this->template create_field_request<GasFieldT     >( gcoordTag );
  parloc_ = this->template create_field_request<ParticleFieldT>( pcoordTag );
  psize_  = this->template create_field_request<ParticleFieldT>( psizeTag  );

  this->template create_field_vector_request<ParticleFieldT>( psrcTagList, srcfields_ );
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellMassExpr<ParticletoCellOp>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  p2c_ = opDB.retrieve_operator<ParticletoCellOp>();
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellMassExpr<ParticletoCellOp>::
evaluate()
{
  GasFieldT& result = this->value();

  const GasFieldT     & gcoord = gcoord_->field_ref();
  const ParticleFieldT& parloc = parloc_->field_ref();
  const ParticleFieldT& psize  = psize_ ->field_ref();

  using namespace SpatialOps;

  //create scratch fields to hold the temporary values
  SpatFldPtr<GasFieldT     > gasmassdiff = SpatialFieldStore::get<GasFieldT     >( gcoord );
  SpatFldPtr<ParticleFieldT> parsrctmp   = SpatialFieldStore::get<ParticleFieldT>( parloc );
  *parsrctmp <<= 0.0 ;

  for( size_t i=0; i<srcfields_.size(); ++i ){
    *parsrctmp <<= *parsrctmp + srcfields_[i]->field_ref();
  }

/*
  ParticleField::const_iterator piter1 = parloc_->begin();
  ParticleField::const_iterator piter2 = psize_->begin();
  ParticleField::iterator piter3 = parsrctmp->begin();
  for(;piter1 != parloc_->end();++piter1,++piter2,++piter3){
    std::cout<<"from particle to cell mass src : "<< *piter1 <<"  "<< *piter2<< "  "<< *piter3 <<std::endl;
  }*/

  p2c_->set_coordinate_information( &parloc, NULL, NULL, &psize );
  p2c_->apply_to_field( *parsrctmp, *gasmassdiff );

  const double dx = gcoord[1] - gcoord[0];
  const double volume = dx * cellarea_;

  result <<= *gasmassdiff / volume;

/*
  typename GasFieldT::iterator piter = result.begin();
  for(;piter != result.end();++piter){
    std::cout<<"from particle to cell mass src : "<< *piter <<std::endl;
  }
  */
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellMassExpr<ParticletoCellOp>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& pcoordTag,
         const Expr::Tag& psizeTag,
         const Expr::TagList& psrcTagList,
         const Expr::Tag& gcoordTag,
         const double cellarea )
  : ExpressionBuilder(result),
    pcT_( pcoordTag ),
    psT_(psizeTag),
    psrcTlist_(psrcTagList),
    gcT_(gcoordTag),
    ca_(cellarea)
{}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
Expr::ExpressionBase*
ParticletoCellMassExpr<ParticletoCellOp>::Builder::build() const
{
  return new ParticletoCellMassExpr<ParticletoCellOp> (pcT_, psT_, psrcTlist_,gcT_,ca_);
}

} // Particle

#endif // ParticletoCellMassExpr_h
