#ifndef ParticlePositionEquation_h
#define ParticlePositionEquation_h

#include <expression/TransportEquation.h>

#include "ParticleFieldsAndOperators.h"



namespace Particle{


  /**
   *  @class ParticlePositionEquation
   *  @brief Describes the position of a particle as a function of time
   *
   *  @todo Initial particle position expression
   */
 
  class ParticlePositionEquation
    : public Expr::TransportEquation
  {
  public:

    enum Direction{ XDIR, YDIR, ZDIR };

    /**
     *  Construct a transport equation for the position of a particle
     *
     *  @param patch the patch to build this transport equation on.
     *
     *  @param exprFactory the factory that we will use to register
     *         expressions associated with this transport equation.
     *
     *  @param icReg the registry holding expressions that evaluate
     *         initial conditions.
     *
     *  @param dir The component of the position vector that this equation solves
     */
    ParticlePositionEquation( Expr::ExpressionFactory&,
                              const Direction dir,
                              const double startloc,
                              const double endloc,
                              const int npar,
                              const int rndseed);

    ~ParticlePositionEquation();

    /**
     *  Build the expression that will set the initial conditions for
     *  the particle positions.
     */
    Expr::ExpressionID initial_condition( Expr::ExpressionFactory& );

  private:

    static std::string get_solnvar_name( const Direction dir );
    static Expr::Tag get_rhs_tag( const Direction dir );

    const double  startloc_, endloc_;
    const int     npar_, rndseed_;

    // no bcs for this - it is an ODE
    void setup_boundary_conditions( Expr::ExpressionFactory& ){}

  };

} // namespace Particle

#endif // ParticlePositionEquation_h
