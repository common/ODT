#ifndef ParticleMomentumEquation_h
#define ParticleMomentumEquation_h

#include <expression/TransportEquation.h>
#include "ParticleFieldsAndOperators.h"

#include "ParticleMomentumRHS.h"

#include <OperatorsAndFields.h>

namespace Particle{

  /**
   *  \class MomentumEquation
   *  \ingroup Particle
   *  \brief Sets up the ODE to describe evolution of the particle momentum.
   *
   *  \todo initial conditions for particle momentum (extract from mass and velocity ICs)
   */
 
  class MomentumEquation
    : public Expr::TransportEquation
  {
  public:

    enum Direction{ XDIR, YDIR, ZDIR };

    /**
     *  \brief Construct a MomentumEquation to solve for one component of momentum
     *  \param exprFactory Expressions will be registered on this factory.
     *  \param dir The MomentumEquation::Direction for this component of momentum.
     *  \param cvelTag The Expr::Tag for the carrier velocity.
     */
    MomentumEquation( Expr::ExpressionFactory& exprFactory,
                      const Direction dir,
                      const Expr::Tag cvelTag );

    ~MomentumEquation();

    /**
     *  \brief Build the expression that will set the initial
     *  conditions for the particle momentum.
     */
    Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory);
    
  private:

    typedef SpatialOps::Particle::CellToParticle<VolField> VelInterpOp;

    static std::string get_solnvar_name( const Direction dir );
    static Expr::Tag get_rhs_tag( const Direction dir );

    // no bcs for this - it is an ODE
    void setup_boundary_conditions( Expr::ExpressionFactory& ){};

    const Direction dir_;
  };

  
} // namespace Particle

#endif // ParticleMomentumEquation_h
