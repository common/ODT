#ifndef ParticleSize_Expr_h
#define ParticleSize_Expr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

#include <stdlib.h>     /* srand, rand */

namespace Particle{

  /**
   *  \class ParticleSize
   *  \ingroup Particle
   *  \brief Specify the particles size
   */
  template< typename ParticleField >
  class ParticleSize
    : public Expr::Expression<SpatialOps::Particle::ParticleField>
  {
    const std::vector<double> sizes_;

    ParticleSize( const std::vector<double>& sizes);
  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      /**
       *  \param pdragTag the result of this expression
       */
      Builder( const Expr::Tag& psizeTag,
               const std::vector<double> sizes);

      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const std::vector<double> sizes_;
    };

    ~ParticleSize(){}

    void bind_operators( const SpatialOps::OperatorDatabase& opDB ){}
    void evaluate();

  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################



  template<typename ParticleField>
  ParticleSize<ParticleField>::
  ParticleSize( const std::vector<double>& sizes)
  : Expr::Expression<ParticleField>(),
    sizes_( sizes )
  {}

  //------------------------------------------------------------------

  template<typename ParticleField>
  void
  ParticleSize<ParticleField>::
  evaluate()
  {
    using namespace SpatialOps;
    ParticleField& result = this->value();

    typename ParticleField::iterator iresult = result.begin();
    std::vector<double>::const_iterator isizes = sizes_.begin();
    for (; iresult != result.end(); ++iresult, ++isizes) {
      *iresult = *isizes;
    }
  }

  //------------------------------------------------------------------

  template<typename ParticleField>
  ParticleSize<ParticleField>::
  Builder::Builder( const Expr::Tag& psizeTag,
                    const std::vector<double> sizes )
    : ExpressionBuilder(psizeTag),
      sizes_( sizes )
  {}

  //------------------------------------------------------------------

  template<typename ParticleField>
  Expr::ExpressionBase*
  ParticleSize<ParticleField>::Builder::build() const
  {
    return new ParticleSize<ParticleField>( sizes_ );
  }

  //------------------------------------------------------------------

} // Particle

#endif // ParticleSize_Expr_h
