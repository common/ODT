#ifndef ParticlePositionRHS_h
#define ParticlePositionRHS_h

#include <expression/ExprLib.h>

#include "ParticleFieldsAndOperators.h"
#include "ParticleStringNames.h"

namespace Particle{


  //==================================================================

  /**
   *  \class ParticlePositionRHS
   *  \ingroup Particle
   *  \brief Calculates the change in particle position according to
   *         \f$\frac{d x}{d t}=u_p\f$, where \f$u_p\f$ is the
   *         particle velocity.
   *
   *  \author James C. Sutherland
   */
  class ParticlePositionRHS :
    public Expr::Expression<ParticleField>
  {
    DECLARE_FIELD( ParticleField, pvel_ )

    ParticlePositionRHS( const Expr::Tag& particleVelocity );

  public:
    /**
     *  \class Builder
     *  \ingroup Particle
     *  \brief constructs ParticlePositionRHS objects
     */
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      /**
       *  \param positionRHSTag the value that this expression evaluates
       *  \param particleVelocity the advecting velocity for the particle
       */
      Builder( const Expr::Tag& positionRHSTag,
               const Expr::Tag& particleVelocity );
      ~Builder(){}
      Expr::ExpressionBase* build() const{return new ParticlePositionRHS( pvel_ );}

    private:
      const Expr::Tag pvel_;
    };

    ~ParticlePositionRHS(){}

    void evaluate();

  };

  //==================================================================

  ParticlePositionRHS::
  ParticlePositionRHS( const Expr::Tag& particleVelocity )
    : Expr::Expression<ParticleField>()
  {
    this->set_gpu_runnable(true);
    pvel_ = this->create_field_request<ParticleField>( particleVelocity );
  }

  //------------------------------------------------------------------

  void
  ParticlePositionRHS::evaluate()
  {
    using SpatialOps::operator<<=;
    ParticleField& rhs = this->value();
    rhs <<= pvel_->field_ref();
  }

  //------------------------------------------------------------------

  ParticlePositionRHS::
  Builder::Builder( const Expr::Tag& positionRHSTag,
                    const Expr::Tag& particleVelocity )
    : ExpressionBuilder( positionRHSTag ),
      pvel_( particleVelocity )
  {}

  //==================================================================

} // namespace Particle

#endif // ParticlePositionEquation_h
