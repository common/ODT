#ifndef CelltoParticleInterpolantExpr_h
#define CelltoParticleInterpolantExpr_h

#include <expression/ExprLib.h>
#include <spatialops/OperatorDatabase.h>

namespace Particle{
//====================================================================


/**
 *  @class CelltoParticleInterpolantExpr
 *  @author Naveen Punati
 *  @date   January, 2011
 *
 *  @brief Interpolates gas field information to the particle location.
 */
template< typename InterpT >
class CelltoParticleInterpolantExpr
  : public Expr::Expression<typename InterpT::DestFieldType>
{

  typedef typename InterpT::DestFieldType ParticleFieldT;
  typedef typename InterpT::SrcFieldType  GasFieldT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag gT_, pT_,psizeT_;
  public:

    /**
     *  @param parTag the Tag for the field this expression calculates
     *  @param gasTag The Expr::Tag that describes the gas phase information needs
     *                to be interpolated to particle locations
     *  @param parLocTag The Expr::Tag that describes the particle locations
     */
    Builder( const Expr::Tag& partTag,
             const Expr::Tag& gasTag,
             const Expr::Tag& parLocTag,
             const Expr::Tag& parsizeTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  CelltoParticleInterpolantExpr( const Expr::Tag&,
                                 const Expr::Tag&,
                                 const Expr::Tag& );

  DECLARE_FIELD( GasFieldT, gasField_ )
  DECLARE_FIELDS( ParticleFieldT, parloc_, parsize_ )
  InterpT* interpOp_;
};

// ###################################################################
//
//                           Implementation
//
// ###################################################################


//--------------------------------------------------------------------

template< typename InterpT >
CelltoParticleInterpolantExpr<InterpT>::
CelltoParticleInterpolantExpr( const Expr::Tag& gasTag,
                               const Expr::Tag& parTag,
                               const Expr::Tag& parsizeTag )
  : Expr::Expression<ParticleFieldT>()
{
  gasField_ = this->template create_field_request<GasFieldT     >( gasTag     );
  parloc_   = this->template create_field_request<ParticleFieldT>( parTag     );
  parsize_  = this->template create_field_request<ParticleFieldT>( parsizeTag );
}

//--------------------------------------------------------------------

template< typename InterpT >
void
CelltoParticleInterpolantExpr<InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename InterpT >
void
CelltoParticleInterpolantExpr<InterpT>::
evaluate()
{
  ParticleFieldT& result = this->value();

  const GasFieldT     & gasField = gasField_->field_ref();
  const ParticleFieldT& parloc   = parloc_  ->field_ref();
  const ParticleFieldT& parsize  = parsize_ ->field_ref();

  interpOp_->set_coordinate_information( &parloc, NULL, NULL, &parsize );
  interpOp_->apply_to_field( gasField, result );
}

//--------------------------------------------------------------------

template< typename InterpT >
CelltoParticleInterpolantExpr<InterpT>::Builder::
Builder( const Expr::Tag& parTag, const Expr::Tag& gasTag,
         const Expr::Tag& parLocTag, const Expr::Tag& parsizeTag)
  : ExpressionBuilder(parTag),
    gT_    (gasTag    ),
    pT_    (parLocTag ),
    psizeT_(parsizeTag)
{}

//--------------------------------------------------------------------

template< typename InterpT >
Expr::ExpressionBase*
CelltoParticleInterpolantExpr<InterpT>::Builder::build() const
{
  return new CelltoParticleInterpolantExpr<InterpT>(gT_,pT_,psizeT_);
}

} //namespace Particle

#endif // AdvectVelocity_h
