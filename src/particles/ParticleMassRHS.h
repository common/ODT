#ifndef ParticleMassRHS_Expr_h
#define ParticleMassRHS_Expr_h

#include <expression/Expression.h>

#include "ParticleFieldsAndOperators.h"

namespace Particle{

/**
 *  \class ParticleMassRHS
 *  \ingroup Particle
 *  \brief Calculates the time-rate-of change of the particle mass.
 *
 *  Currently the value is hard coded to 0.  When models are included that
 *  add source terms to this equation, they will be added as a dependency here
 *  to take care of mass loss.
 */
class ParticleMassRHS
 : public Expr::Expression<ParticleField>
{
  ParticleMassRHS(){}

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& pMassTag ) : ExpressionBuilder(pMassTag) {}
    Expr::ExpressionBase* build() const{ return new ParticleMassRHS(); }
  };

  ~ParticleMassRHS(){}
  void evaluate()
  {
    using namespace SpatialOps;
    ParticleField& result = this->value();
    result <<= 0.0;
  }
};

//--------------------------------------------------------------------

} // namespace Particle

#endif // ParticleMassRHS_Expr_h
