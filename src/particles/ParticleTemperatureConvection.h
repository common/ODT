#ifndef ParticleTemperatureConvection_Expr_h
#define ParticleTemperatureConvection_Expr_h

#include <expression/Expression.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{

  /**
   *  \class ParticleTemperatureConvection
   *  \ingroup Particle
   *  \brief Evaluates the convection term in the particle temperature equation.
   *
   *  \author Naveen Punati
   *
   *
   */

  template< typename FieldToParticleOp >
  class ParticleTemperatureConvection
  : public Expr::Expression<typename FieldToParticleOp::DestFieldType>
  {
    typedef typename FieldToParticleOp::DestFieldType  ParticleFieldT;
    typedef typename FieldToParticleOp::SrcFieldType   GasFieldT;

    DECLARE_FIELDS( ParticleFieldT, pxpos_, pmass_, pRe_, ptemp_, psize_, pcp_ )
    DECLARE_FIELDS( GasFieldT, gcp_, gmu_, grho_, glambda_, gtemp_ )

    FieldToParticleOp* op_;

    ParticleTemperatureConvection(const Expr::Tag& pxposTag,
                                  const Expr::Tag& pmassTag,
                                  const Expr::Tag& pReTag,
                                  const Expr::Tag& ptempTag,
                                  const Expr::Tag& psizeTag,
                                  const Expr::Tag& pcoalcpTag,
                                  const Expr::Tag& gcpTag,
                                  const Expr::Tag& gmuTag,
                                  const Expr::Tag& grhoTag,
                                  const Expr::Tag& glambdaTag,
                                  const Expr::Tag& gtempTag );

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      Builder( const Expr::Tag& pTempConvTag,
               const Expr::Tag& pxposTag,
               const Expr::Tag& pmassTag,
               const Expr::Tag& pReTag,
               const Expr::Tag& ptempTag,
               const Expr::Tag& psizeTag,
               const Expr::Tag& pcoalcpTag,
               const Expr::Tag& gcpTag,
               const Expr::Tag& gmuTag,
               const Expr::Tag& grhoTag,
               const Expr::Tag& glambdaTag,
               const Expr::Tag& gtempTag );
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag pxposTag_,pmassTag_,pReTag_,ptempTag_,psizeTag_,pcoalcpTag_,gcpTag_,gmuTag_,grhoTag_,glambdaTag_,gtempTag_;
    };

    ParticleTemperatureConvection(){}
    void bind_operators( const SpatialOps::OperatorDatabase& opDB );
    void evaluate();

  };

  //---------------------------------------------------------------------
  //
  //                   Implementation
  //
  //---------------------------------------------------------------------
  template<typename FieldToParticleOp>
  ParticleTemperatureConvection<FieldToParticleOp>::
  ParticleTemperatureConvection(const Expr::Tag& pxposTag,
                                const Expr::Tag& pmassTag,
                                const Expr::Tag& pReTag,
                                const Expr::Tag& ptempTag,
                                const Expr::Tag& psizeTag,
                                const Expr::Tag& pcoalcpTag,
                                const Expr::Tag& gcpTag,
                                const Expr::Tag& gmuTag,
                                const Expr::Tag& grhoTag,
                                const Expr::Tag& glambdaTag,
                                const Expr::Tag& gtempTag )
      : Expr::Expression<ParticleFieldT>()
  {
    pxpos_   = this->template create_field_request<ParticleFieldT>( pxposTag   );
    pmass_   = this->template create_field_request<ParticleFieldT>( pmassTag   );
    pRe_     = this->template create_field_request<ParticleFieldT>( pReTag     );
    ptemp_   = this->template create_field_request<ParticleFieldT>( ptempTag   );
    psize_   = this->template create_field_request<ParticleFieldT>( psizeTag   );
    pcp_     = this->template create_field_request<ParticleFieldT>( pcoalcpTag );

    gcp_     = this->template create_field_request<GasFieldT     >( gcpTag     );
    gmu_     = this->template create_field_request<GasFieldT     >( gmuTag     );
    grho_    = this->template create_field_request<GasFieldT     >( grhoTag    );
    glambda_ = this->template create_field_request<GasFieldT     >( glambdaTag );
    gtemp_   = this->template create_field_request<GasFieldT     >( gtempTag   );
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  ParticleTemperatureConvection<FieldToParticleOp>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    op_ = opDB.retrieve_operator<FieldToParticleOp>();
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  ParticleTemperatureConvection<FieldToParticleOp>::
  evaluate()
  {
    using namespace SpatialOps;

    ParticleFieldT& convterm = this->value();

    const ParticleFieldT& pxpos = pxpos_->field_ref();
    const ParticleFieldT& pmass = pmass_->field_ref();
    const ParticleFieldT& pRe   = pRe_  ->field_ref();
    const ParticleFieldT& ptemp = ptemp_->field_ref();
    const ParticleFieldT& psize = psize_->field_ref();
    const ParticleFieldT& pcp   = pcp_  ->field_ref();

    const GasFieldT& gcp     = gcp_    ->field_ref();
    const GasFieldT& gmu     = gmu_    ->field_ref();
    const GasFieldT& grho    = grho_   ->field_ref();
    const GasFieldT& glambda = glambda_->field_ref();
    const GasFieldT& gtemp   = gtemp_  ->field_ref();

    //calculating gas Prandtl number
    SpatialOps::SpatFldPtr<GasFieldT> gPr = SpatialOps::SpatialFieldStore::get<GasFieldT>( gcp );
    *gPr <<= gcp * gmu / glambda;

    //interpolate the gas Prandtl number to particle locations.....
    SpatialOps::SpatFldPtr<ParticleFieldT> pPr = SpatialOps::SpatialFieldStore::get<ParticleFieldT>( convterm );
    op_->set_coordinate_information( &pxpos, NULL, NULL, &psize );
    op_->apply_to_field( *gPr, *pPr );

    //calculating gas Nusselt number from Re_p and interpolated Prandtl number
    SpatialOps::SpatFldPtr<ParticleFieldT> Nu = SpatialOps::SpatialFieldStore::get<ParticleFieldT>( convterm );
    *Nu <<= 2.0 + 0.6 * pow(pRe, 0.5) * pow(*pPr, (1.0/3.0));

    //interpolate gas phase thermal conductivity to particle locations
    SpatialOps::SpatFldPtr<ParticleFieldT> plambda = SpatialOps::SpatialFieldStore::get<ParticleFieldT>( convterm );
    op_->apply_to_field( glambda, *plambda );

    //calculate the convective heat transfer coefficient
    SpatialOps::SpatFldPtr<ParticleFieldT> coef_h = SpatialOps::SpatialFieldStore::get<ParticleFieldT>( convterm );
    *coef_h <<= *Nu * *plambda / psize ;

    //interpolate gas phase temperature to particle locations
    SpatialOps::SpatFldPtr<ParticleFieldT> int_temp = SpatialOps::SpatialFieldStore::get<ParticleFieldT>( convterm );
    op_->apply_to_field( gtemp, *int_temp );

    //time to construct the convective heat transfer term for particle temperature equation
    // ( A_p / (m_p*c_p)) * h * (T_g-T_p)
    convterm <<= ((3.1416 * pow(psize,2)) / (pmass * pcp) ) * ( *coef_h * ( *int_temp - ptemp)) ;

  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  ParticleTemperatureConvection<FieldToParticleOp>::
  Builder::Builder( const Expr::Tag& pTempConvTag,
                    const Expr::Tag& pxposTag,
                    const Expr::Tag& pmassTag,
                    const Expr::Tag& pReTag,
                    const Expr::Tag& ptempTag,
                    const Expr::Tag& psizeTag,
                    const Expr::Tag& pcoalcpTag,
                    const Expr::Tag& gcpTag,
                    const Expr::Tag& gmuTag,
                    const Expr::Tag& grhoTag,
                    const Expr::Tag& glambdaTag,
                    const Expr::Tag& gtempTag )
  : ExpressionBuilder( pTempConvTag ),
    pxposTag_  ( pxposTag  ),
    pmassTag_  ( pmassTag  ),
    pReTag_    ( pReTag    ),
    ptempTag_  ( ptempTag  ),
    psizeTag_  ( psizeTag  ),
    pcoalcpTag_( pcoalcpTag),
    gcpTag_    ( gcpTag    ),
    gmuTag_    ( gmuTag    ),
    grhoTag_   ( grhoTag   ),
    glambdaTag_( glambdaTag),
    gtempTag_  ( gtempTag  )
  {}

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  Expr::ExpressionBase*
  ParticleTemperatureConvection<FieldToParticleOp>::Builder::build() const
  {
    return new ParticleTemperatureConvection<FieldToParticleOp>( pxposTag_,pmassTag_,pReTag_,ptempTag_,psizeTag_,pcoalcpTag_,gcpTag_,gmuTag_,grhoTag_,glambdaTag_,gtempTag_ );
  }

} // Particle

#endif // ParticleTemperatureConvection_Expr_h
