#ifndef ParticleCp_Expr_h
#define ParticleCp_Expr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{

  /**
   *  \class ParticleCp
   *  \ingroup Particle
   *  \brief Evaluates the heat capacity of the coal.
   *
   *  \author Naveen Punati
   *
   *
   */

  //###################################################################
  //
  //
  //####################################################################

  template< typename FieldT >
  class ParticleCp
  : public Expr::Expression<FieldT>
  {
    DECLARE_FIELD( FieldT, ptemp_ )

    ParticleCp( const Expr::Tag& ptempTag );

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      Builder( const Expr::Tag& pCpTag,
               const Expr::Tag& ptempTag );
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag ptempTag_;
    };

    ParticleCp(){}
    void evaluate();
  };





  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################

  template<typename FieldT>
  ParticleCp<FieldT>::
  ParticleCp( const Expr::Tag& ptempTag )
  : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable(true);

    ptemp_ = this->template create_field_request<FieldT>( ptempTag );
  }

  //------------------------------------------------------------------

  template<typename FieldT>
  void
  ParticleCp<FieldT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    const FieldT& ptemp = ptemp_->field_ref();

    const double unitConvFac = 1000; // convert from kJ/(kg K) to J/(kg K)
    result <<= unitConvFac * ( -0.218 + 3.807e-3 * ptemp - 1.758e-6 * pow( ptemp,2) );
  }

  //------------------------------------------------------------------

  template<typename FieldT>
  ParticleCp<FieldT>::
  Builder::Builder( const Expr::Tag& pCpTag,
                    const Expr::Tag& ptempTag )
  : ExpressionBuilder(pCpTag),
    ptempTag_( ptempTag)
  {}

  //------------------------------------------------------------------

  template<typename FieldT>
  Expr::ExpressionBase*
  ParticleCp<FieldT>::Builder::build() const
  {
    return new ParticleCp<FieldT>( ptempTag_ );
  }

} // Particle

#endif // ParticleCp_Expr_h
