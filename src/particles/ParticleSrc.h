#ifndef ParticleSrc_h
#define ParticleSrc_h

#include "ParticleFieldsAndOperators.h"
#include <OperatorsAndFields.h>
#include <iostream>
#include <list>

#include <eddies/LagrangePoly.h>

namespace Particle{

/**
 *  \class ParticleSrc
 *  \ingroup Particle
 *  \author Naveen Punati
 *  \brief A source term to add to equations to represent an eddy
 *         occuring over a finite time interval.
 */
class ParticleSrc
{
public:

  ParticleSrc( const double* time,
               const SpatialOps::OperatorDatabase& opDB,
               const ParticleField& coord,
               const ParticleField& pxvel,
               const ParticleField& pdrag,
               const ParticleField& ptau,
               const VolField& gascoord );

  ~ParticleSrc();

  /**
   *  \brief Add an eddy to contribute to the source term.  Multiple
   *         eddies may be contributing the source term at any given
   *         instant in time.  After an eddy expires it is no longer
   *         considered.
   *
   *  \param tstart the eddy start time
   *  \param tduration the time duration of the eddy (end time = tstart+tduration)
   *  \param sindex the starting index for the eddy
   *  \param eindex the ending index for the eddy
   */
  void add_eddy_event( const double tstart,
                       const double tduration,
                       const int sindex,
                       const int eindex );

  /**
   *  \brief Apply the eddy source term to the given field.
   */
  void operator()( ParticleField& field );

  /**
   *  ???
   */
  double eddy_newloc(const int sindex, const int eindex, const double );

  static inline bool is_gpu_runnable(){ return false; }

private:

  struct EddyInfo
  {
    const double tstart, tstop;
    const int sindex,eindex;
    std::vector<double> pardft0;

    EddyInfo( const double startTime,
              const double tduration,
              const int si,
              const int ei,
              std::vector<double> pardf )
      : tstart( startTime ),
        tstop( startTime+tduration ),
        sindex(si),
        eindex(ei),
        pardft0(pardf)
    {}
  };

  const double* const time_;  ///< The simulation time - required for eddy source term.

  typedef std::list< EddyInfo > EddyList;
  EddyList eddies_;
  std::vector<double> pardf_;

  const ParticleField& pcoord_;
  const ParticleField& pXvel_;
  const ParticleField& pDrag_;
  const ParticleField& pTau_;
  const VolField& gascoord_;

  const CellFieldToParticleOp* const C2P_;
};



} //name space particle

#endif // ParticleSrc_h
