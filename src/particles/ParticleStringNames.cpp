#include "ParticleStringNames.h"

namespace Particle{

  const ParticleStringNames&
  ParticleStringNames::self()
  {
    static ParticleStringNames s;
    return s;
  }

  ParticleStringNames::ParticleStringNames()
    : xvel("p_xvel"), yvel("p_yvel"), zvel("p_zvel"),
      xpos("p_x"), ypos("p_y"), zpos("p_z"),
      xmom("p_xmom"), ymom("p_ymom"), zmom("p_zmom"),
      mass("p_mass"),
      size("p_size"),
      dragc("p_drag_coef"),
      tau("p_tau"),
      pdensity("p_density"),
      parRe("p_Re"),
      Xmomdragterm("p_Xmomdragterm"),
      Ymomdragterm("p_Ymomdragterm"),
      Zmomdragterm("p_Zmomdragterm"),
      ptemperature("p_temperature"),
      ptempconv("ptempconv"),
      pheatcapacity("pheatcapacity")
  {}

  ParticleStringNames::~ParticleStringNames(){}

} // namespace Particle
