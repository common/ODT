#ifndef ParticleResponseTime_Expr_h
#define ParticleResponseTime_Expr_h

#include <expression/Expression.h>

#include "ParticleFieldsAndOperators.h"

namespace Particle{

/**
 *  \class ParticleResponseTime
 *  \ingroup Particle
 *  \brief Calculates the particle ParticleResponseTime.
 */
template< typename FieldToParticleOp >
class ParticleResponseTime
 : public Expr::Expression<ParticleField>
{
  typedef typename FieldToParticleOp::SrcFieldType      FieldT;

  DECLARE_FIELDS( ParticleField, pdensity_, psize_, ppos_ )
  DECLARE_FIELD ( FieldT, gasVisc_ )

  FieldToParticleOp* op_;

  ParticleResponseTime( const Expr::Tag& particleDensity,
                        const Expr::Tag& particleSize,
                        const Expr::Tag& particlePos,
                        const Expr::Tag& gasViscosity );

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& prtTag,
             const Expr::Tag& particleDensity,
             const Expr::Tag& particleSize,
             const Expr::Tag& particlePos,
             const Expr::Tag& gasViscosity );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::Tag pdt_, pst_, ppt_, gvist_;
  };

  ~ParticleResponseTime(){}

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};

 // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################

template< typename FieldToParticleOp >
ParticleResponseTime<FieldToParticleOp>::
ParticleResponseTime( const Expr::Tag& particleDensity,
                      const Expr::Tag& particleSize,
                      const Expr::Tag& particlePos,
                      const Expr::Tag& gasViscosity )
  : Expr::Expression<ParticleField>()
{
  pdensity_ = this->template create_field_request<ParticleField>( particleDensity );
  psize_    = this->template create_field_request<ParticleField>( particleSize    );
  ppos_     = this->template create_field_request<ParticleField>( particlePos     );
  gasVisc_  = this->template create_field_request<FieldT       >( gasViscosity    );
}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
void
ParticleResponseTime<FieldToParticleOp>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  op_ = opDB.retrieve_operator<FieldToParticleOp>();
}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
void
ParticleResponseTime<FieldToParticleOp>::evaluate()
{
  ParticleField& result = this->value();

  const ParticleField& pdensity = pdensity_->field_ref();
  const ParticleField& psize    = psize_   ->field_ref();
  const ParticleField& ppos     = ppos_    ->field_ref();
  const FieldT       & gasVisc  = gasVisc_ ->field_ref();

  SpatialOps::SpatFldPtr<ParticleField> inttmp = SpatialOps::SpatialFieldStore::get<ParticleField>( result );
  op_->set_coordinate_information( &ppos, NULL, NULL, &psize );
  op_->apply_to_field( gasVisc, *inttmp );

  using namespace SpatialOps;
  result <<= pdensity * psize * psize / ( 18.0 * *inttmp );
}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
ParticleResponseTime<FieldToParticleOp>::
Builder::Builder( const Expr::Tag& prtTag,
                  const Expr::Tag& particleDensity,
                  const Expr::Tag& particleSize,
                  const Expr::Tag& particlePos,
                  const Expr::Tag& gasviscosity)
  : ExpressionBuilder( prtTag ),
    pdt_  ( particleDensity ),
    pst_  ( particleSize    ),
    ppt_  ( particlePos     ),
    gvist_( gasviscosity    )
{}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
Expr::ExpressionBase*
ParticleResponseTime<FieldToParticleOp>::Builder::build() const
{
  return new ParticleResponseTime( pdt_, pst_, ppt_, gvist_ );
}


} // namespace Particle

#endif // ParticleResponseTime_Expr_h
