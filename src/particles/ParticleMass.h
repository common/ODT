#ifndef ParticleMass_Expr_h
#define ParticleMass_Expr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

#include <stdlib.h>     /* srand, rand */

namespace Particle{

  /**
   *  \class ParticleMass
   *  \ingroup Particle
   *  \brief Calculates the initial particle mass
   */
  template< typename ParticleField >
  class ParticleMass
    : public Expr::Expression<SpatialOps::Particle::ParticleField>
  {
    DECLARE_FIELDS( ParticleField, pdens_, psize_ )

    ParticleMass( const Expr::Tag& pdensityt,
                  const Expr::Tag& psizet );
  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      /**
       *  \param pdragTag the result of this expression
       */
      Builder( const Expr::Tag& pmasst,
               const Expr::Tag& pdensityt,
               const Expr::Tag& psizet );
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag pdensityt_, psizet_;
    };

    ~ParticleMass(){}

    void evaluate();

  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################



  template<typename ParticleField>
  ParticleMass<ParticleField>::
  ParticleMass(const Expr::Tag& pdensityt,
               const Expr::Tag& psizet)
  : Expr::Expression<ParticleField>()
  {
    this->set_gpu_runnable(true);
    pdens_ = this->template create_field_request<ParticleField>( pdensityt );
    psize_ = this->template create_field_request<ParticleField>( psizet    );
  }

  //------------------------------------------------------------------

  template<typename ParticleField>
  void
  ParticleMass<ParticleField>::
  evaluate()
  {
    using namespace SpatialOps;
    ParticleField& result = this->value();
    const ParticleField& pdens = pdens_->field_ref();
    const ParticleField& psize = psize_->field_ref();
    result <<=  pdens * (3.141592653589793/6.0) * pow(psize,3.0);;
  }

  //------------------------------------------------------------------

  template<typename ParticleField>
  ParticleMass<ParticleField>::
  Builder::Builder( const Expr::Tag& pmasst,
                    const Expr::Tag& pdensityt,
                    const Expr::Tag& psizet)
    : ExpressionBuilder(pmasst),
      pdensityt_( pdensityt ),
      psizet_   ( psizet )
  {}

  //------------------------------------------------------------------

  template<typename ParticleField>
  Expr::ExpressionBase*
  ParticleMass<ParticleField>::Builder::build() const
  {
    return new ParticleMass<ParticleField>( pdensityt_, psizet_ );
  }

  //------------------------------------------------------------------

} // Particle

#endif // ParticleMass_Expr_h
