#ifndef ParticleStringNames_h
#define ParticleStringNames_h

#include <string>

namespace Particle{

  /**
   *  \class ParticleStringNames
   *  \ingroup Particle
   *  \brief Defines names for various particle quantities
   */
  class ParticleStringNames{

  public:

    static const ParticleStringNames& self();
    
    const std::string xvel, yvel, zvel;
    const std::string xpos, ypos, zpos;
    const std::string xmom, ymom, zmom;
    const std::string mass;
    const std::string size;
    const std::string dragc;
    const std::string tau;
    const std::string pdensity;
    const std::string parRe;
    const std::string Xmomdragterm,Ymomdragterm,Zmomdragterm;
    const std::string ptemperature,ptempconv,pheatcapacity;

  private:

    ParticleStringNames();  // implemented as a sigleton
    ~ParticleStringNames();
    ParticleStringNames(const ParticleStringNames&);            // no copying
    ParticleStringNames& operator=(const ParticleStringNames&); // no assignment
  };

} // namespace Particle

#endif // ParticleStringNames_h
