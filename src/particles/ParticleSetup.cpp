#include <stdexcept>
#include <sstream>

#include <boost/foreach.hpp>

#include "ParticleSetup.h"
#include "gas/RxnDiffusionSetup.h"

#include "ParticlePositionEquation.h"
#include "ParticleFieldsAndOperators.h"
#include "ParticleMomentumEquation.h"
#include "ParticleEnergyEquation.h"
#include "ParticleMassEquation.h"
#include "ParticleDragCoefficient.h"
#include "ParticleResponseTime.h"
#include "ParticleRe.h"
#include "ParticleDrag.h"
#include "ParticleDensity.h"
#include "CelltoParticleInterpolantExpr.h"

#include "ParticleStringNames.h"

// Particle initilization
#include "ParticleSize.h"
#include "ParticleMass.h"

#include "ParticletoCellConvEnergyExpr.h"
#include "ParticletoCellMomExpr.h"
#include "ParticletoCellMassExpr.h"
#include "ParticletoCellEnergyExpr.h"

#include "ParticleVelocityIC.h"

#include <StringNames.h>
#include <coal/StringNames.h>

//--- Cantera includes ---//
#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/IdealGasMix.h>

#include <spatialops/structured/Grid.h>

using Expr::Tag;
using Expr::TagList;
using Expr::STATE_N;
using Expr::STATE_NONE;

namespace Particle{

ParticleSetup::ParticleSetup( Expr::TimeStepper* integrator,
                              PatchT& patch,
                              Expr::ExpressionFactory& expfactory,
                              const SpatialOps::Grid& grid,
                              const YAML::Node& pg )
  : patch_( patch ),
    coalimplement_(true),
    exprFactory_( expfactory ),
    grid_( grid )
{
  const ParticleStringNames& snam = ParticleStringNames::self();
  const StringNames& gasName = StringNames::self();

  typedef SpatialOps::Particle::CellToParticle<VolField> InterpOp;

  // gas phase velocities - input for particle drag term expressions
  const Tag XvelTag( gasName.xvel, STATE_N );
  const Tag YvelTag( gasName.yvel, STATE_N );

  const YAML::Node particlespg = pg["Particles"];
  const int  npar  = particlespg["NumParticles"    ].as<int>();

  std::cout<<" # of particles : "<< npar <<std::endl;

  // ODE for particle x-position (within the ODT line)
  const YAML::Node particlpos = particlespg["Position"];
  const double     startloc   = particlpos["StartLocation"     ].as<double>();
  const double     endloc     = particlpos["EndLocation"       ].as<double>();
  int rndseed = 0; // no random arrangement
  if (particlpos["RandomPlacement"]) {
    rndseed = particlpos["RandomPlacement"].as<int>();
  }
  typedef Particle::ParticlePositionEquation PosEqn;
  equations_.push_back( new PosEqn( exprFactory_, PosEqn::XDIR, startloc, endloc, npar, rndseed));

  // ODE for particle velocity in x and y directions
  typedef Particle::MomentumEquation MomEqnT;
  equations_.push_back( new MomEqnT( exprFactory_, MomEqnT::XDIR, XvelTag));
  equations_.push_back( new MomEqnT( exprFactory_, MomEqnT::YDIR, YvelTag ));

  // ODE for particle total mass
  typedef Particle::ParticleMassEquation massEqn;
  equations_.push_back( new massEqn( exprFactory_));

  // ODE for particle size
  const YAML::Node psizenode = particlespg["Size"];
  particle_initial_fuctions(psizenode, psizes_, npar);
  exprFactory_.register_expression( new ParticleSize<ParticleField>::Builder( Tag(snam.size,STATE_N), psizes_) );
  exprFactory_.register_expression( new ParticleSize<ParticleField>::Builder( Tag("Initial_"+snam.size,STATE_N), psizes_) );

  // register the initial particle mass as a constant expression ( Density and size for all the particle is the same )
  pdensity_  = particlespg["Density"].as<double>();
  exprFactory_.register_expression( new ParticleMass<ParticleField>::Builder(Tag("Initial_"+snam.mass, STATE_N),
                                                                             Tag("Initial_"+snam.pdensity, STATE_N),
                                                                             Tag("Initial_"+snam.size, STATE_N)));


  // Register expression for initial particle density (needed for some of the char models)
  exprFactory_.register_expression( new Expr::ConstantExpr<ParticleField>::Builder( Tag("Initial_"+snam.pdensity, STATE_N),
                                                                                    pdensity_) );


  typedef Particle::ParticleDensity<ParticleField>::Builder densitybuilder;
  exprFactory_.register_expression( new densitybuilder( Tag(snam.pdensity,STATE_N),
                                                        Tag(snam.mass,STATE_N),
                                                        Tag(snam.size,STATE_N)) );

  exprFactory_.register_expression( new ParticleDrag<InterpOp>::Builder( Tag(snam.Xmomdragterm,STATE_N),
                                                                         XvelTag,
                                                                         Tag(snam.dragc,       STATE_N),
                                                                         Tag(snam.tau,         STATE_N),
                                                                         Tag(snam.xpos,        STATE_N),
                                                                         Tag(snam.xvel,        STATE_N),
                                                                         Tag(snam.size,        STATE_N) ) );
  exprFactory_.register_expression( new ParticleDrag<InterpOp>::Builder( Tag(snam.Ymomdragterm,STATE_N),
                                                                         YvelTag,
                                                                         Tag(snam.dragc,STATE_N),
                                                                         Tag(snam.tau,  STATE_N),
                                                                         Tag(snam.xpos, STATE_N),
                                                                         Tag(snam.yvel, STATE_N),
                                                                         Tag(snam.size, STATE_N) ));

  typedef Particle::ReynoldsNumber<InterpOp>::Builder ParReynoldsNum;
  exprFactory_.register_expression( new ParReynoldsNum( Tag(snam.parRe,       STATE_N),
                                                        Tag(gasName.density,  STATE_N),
                                                        Tag(snam.size,        STATE_N),
                                                        Tag(snam.yvel,        STATE_N),
                                                        Tag(snam.xpos,        STATE_N),
                                                        Tag(gasName.viscosity,STATE_N),
                                                        Tag(gasName.yvel,     STATE_N) ));

  typedef Particle::DragCoefficient::Builder DragCoeff;
  exprFactory_.register_expression( new DragCoeff( Tag( snam.dragc, STATE_N ), Tag(snam.parRe,STATE_N) ));

  typedef Particle::ParticleResponseTime<InterpOp>::Builder ParResTime;
  exprFactory_.register_expression( new ParResTime( Tag( snam.tau,         STATE_N ),
                                                    Tag(snam.pdensity,     STATE_N),
                                                    Tag(snam.size,         STATE_N) ,
                                                    Tag(snam.xpos,         STATE_N),
                                                    Tag(gasName.viscosity, STATE_N) ));

  // add particle energy equation to the integrator

  const double tempval  = particlespg["Temperature"    ].as<double>();
  const double walltemp = particlespg["WallTemperature"].as<double>(500.0);
  
  typedef Particle::EnergyEquation EnergyEqnT;
  equations_.push_back( new EnergyEqnT( exprFactory_,walltemp,tempval,coalimplement_));

  // plug the equations into the time integrator
  const int nghost = 0;
  BOOST_FOREACH( Expr::TransportEquation* eqn, equations_ ){
    integrator->add_equation<ParticleField>( eqn->solution_variable_name(),
                                             eqn->get_rhs_tag(),
                                             nghost );
  }

  //add particle influence on the gas phase momentum
  add_P2C_srcterm( pg);

  //set up the coal subprocesses
  if (coalimplement_)  setup_coal_subprocesses(integrator,pg);

  //set up operators for particles...interpolates from cell to particles
  setup_operators();
}

//---------------destructor---------------------------
ParticleSetup::~ParticleSetup(){
  // wipe out the transport equations
  BOOST_FOREACH( Expr::TransportEquation* eqn, equations_ ){
    delete eqn;
  }
  BOOST_FOREACH( TMSrc::value_type& vt, TMsrcterms_ ){
    delete vt.second;
  }
  BOOST_FOREACH( ParticleSrc* ps, TMSrclist_ ){
    delete ps;
  }
  if( coalimplement_ ) delete coalInterface_;
}

//-------------------set particle operators
void
ParticleSetup::setup_operators()
{
  SpatialOps::OperatorDatabase& opDB = patch_.operator_database();
  const Tag coordTag( StringNames::self().xcoord, STATE_N );
  const VolField& coord = patch_.field_manager_list().field_manager<VolField>().field_ref( coordTag );
  const size_t ng = coord.get_ghost_data().get_minus(0);
  const double dx = grid_.spacing<SpatialOps::XDIR>();

  // to interpolate from cell to particles (Gas phase to particles)
  opDB.register_new_operator<CellFieldToParticleOp>( new CellFieldToParticleOp( dx, coord[ng] ) );

  // to interpolate from particles to cell (particles to gas phase)
  opDB.register_new_operator<ParticleFieldToCellOp>( new ParticleFieldToCellOp( dx, coord[ng] ) );
}

//------------------------------------------------------------------------------------
void
ParticleSetup::add_P2C_srcterm(const YAML::Node& pg)
{
  const YAML::Node particlespg = pg["Particles"];
  const double cellarea = particlespg["CellArea"].as<double>();

  const ParticleStringNames& snam = ParticleStringNames::self();
  const Tag pxposTag    (snam.xpos,        STATE_N);
  const Tag pmassTag    (snam.mass,        STATE_N);
  const Tag psizeTag    (snam.size,        STATE_N);
  const Tag pxmomdragTag(snam.Xmomdragterm,STATE_N);
  const Tag pymomdragTag(snam.Ymomdragterm,STATE_N);

  const StringNames& gasName = StringNames::self();
  const Tag gascoordTag(gasName.xcoord,STATE_N);

  //register expressions to compute the influence of particles on gas phase X & Y momentum
  // based on particle drag terms build expressions to calculate the particle influence on gas phase momentum
  typedef Particle::ParticletoCellMomExpr<ParticleFieldToCellOp>::Builder P2Cbuilder;
  const Tag P2C_X_mom_Tag("P2CMomSrc_X",STATE_N);
  const Tag P2C_Y_mom_Tag("P2CMomSrc_Y",STATE_N);

  exprFactory_.register_expression( new P2Cbuilder(P2C_X_mom_Tag,pxposTag,pxmomdragTag,psizeTag,pmassTag,gascoordTag,cellarea) );
  exprFactory_.register_expression( new P2Cbuilder(P2C_Y_mom_Tag,pxposTag,pymomdragTag,psizeTag,pmassTag,gascoordTag,cellarea) );

  //get the ids for the just built expressions and also the gas phase momentum RHS
  const Tag gasXmomTag( gasName.xmom+"RHS", STATE_N );
  const Tag gasYmomTag( gasName.ymom+"RHS", STATE_N );

  //attach the dependency to the gas phase momentum RHS

  exprFactory_.attach_dependency_to_expression( P2C_X_mom_Tag,
                                                gasXmomTag,
                                                Expr::ADD_SOURCE_EXPRESSION );

  exprFactory_.attach_dependency_to_expression( P2C_Y_mom_Tag,
                                                gasYmomTag,
                                                Expr::ADD_SOURCE_EXPRESSION );

  //energy coupling
  const Tag convtermTag(snam.ptempconv,STATE_N);
  const Tag P2C_energy_conv_src_Tag("P2CconvenergySrc",STATE_N);
  const coal::StringNames& coalName = coal::StringNames::self();
  const std::string pcpName = coalimplement_ ? coalName.coal_cp : snam.pheatcapacity;
  const Tag pcpTag ( pcpName,  STATE_N );

  typedef Particle::ParticletoCellConvEnergyExpr<ParticleFieldToCellOp>::Builder P2Cconvenergybuilder;
  exprFactory_.register_expression( new P2Cconvenergybuilder( P2C_energy_conv_src_Tag, pxposTag,
                                                              psizeTag,                pmassTag,
                                                              pcpTag,                  convtermTag,
                                                              gascoordTag,             cellarea ) );

  exprFactory_.attach_dependency_to_expression( P2C_energy_conv_src_Tag,
                                                Tag( gasName.rhoE0+"RHS", STATE_N ),
                                                Expr::ADD_SOURCE_EXPRESSION );
}

//------------adds eddy effects on the particles----------
void
ParticleSetup::create_eddy_srcterm( const double* time, const YAML::Node& pg )
{

  typedef Expr::Expression<ParticleField>  ParticleRHSExpr;
  typedef Expr::Expression<VolField>  GasRHSExpr;
  const ParticleStringNames& snam = ParticleStringNames::self();

  const Expr::FieldManagerList& fml = patch_.field_manager_list();
  const Expr::FieldMgrSelector<ParticleField>::type& fieldMan    = fml.field_manager<ParticleField>();
  const Expr::FieldMgrSelector<VolField     >::type& gasfieldMan = fml.field_manager<VolField     >();

  const Tag pxposTag(snam.xpos, STATE_N);
  const Tag pxvelTag(snam.xvel, STATE_N);
  const Tag pdragTag(snam.dragc,STATE_N);
  const Tag  ptauTag(snam.tau,  STATE_N);

  const ParticleField& Xcoord = fieldMan.field_ref(pxposTag);
  const ParticleField& Xvel   = fieldMan.field_ref(pxvelTag);
  const ParticleField& Pdrag  = fieldMan.field_ref(pdragTag);
  const ParticleField& Ptau   = fieldMan.field_ref(ptauTag );

  const StringNames& gasName = StringNames::self();
  const Tag gascoordTag(gasName.xcoord,STATE_N);
  const VolField& gascoord = gasfieldMan.field_ref(gascoordTag);

  // Associate source term object to particle X-momentum RHS
  // Source term to add eddy effects....only added to particle X-momentum
  Expr::ExpressionID XmomRHSID( exprFactory_.get_id( Tag(snam.xmom+"_RHS", STATE_N) ) );
  ParticleRHSExpr& XmomRHSExpr = dynamic_cast<ParticleRHSExpr&>( exprFactory_.retrieve_expression( XmomRHSID, patch_.id() ) );
  ParticleSrc* Xmomsrc = new ParticleSrc(time,patch_.operator_database(),Xcoord,Xvel,Pdrag,Ptau,gascoord);
  XmomRHSExpr.process_after_evaluate( boost::ref(*Xmomsrc), Xmomsrc->is_gpu_runnable() );
  TMsrcterms_.insert( std::pair<std::string, ParticleSrc*>("p_Xmom", Xmomsrc ));
}


//-------------sets eddy events------------------
void
ParticleSetup::set_eddy_events(const std::string& variable, const double time, const double eddylife, const int sindex, const int eindex)
{
  TMsrcterms_.find(variable)->second->add_eddy_event(time,eddylife,sindex,eindex);
}

//-----------------set initial conditions-----------------------
void
ParticleSetup::set_initial_conditions( const YAML::Node& pg, Expr::ExpressionFactory& icExprFactory )
{
  const ParticleStringNames& snam = ParticleStringNames::self();

  Expr::FieldManagerList& fml = patch_.field_manager_list();

  typedef SpatialOps::Particle::CellToParticle<VolField> InterpC2P;
  const StringNames& gasName = StringNames::self();
  icExprFactory.register_expression( new ParticleVelocityIC<InterpC2P>::Builder(
      Tag(snam.yvel,    STATE_N),  Tag(snam.xpos, STATE_N),
      Tag(gasName.yvel, STATE_N),  Tag(snam.size, STATE_N)  ) );

  icExprFactory.register_expression( new Expr::ConstantExpr<ParticleField>::Builder( Tag(snam.pdensity, STATE_N), pdensity_));
  icExprFactory.register_expression( new ParticleSize<ParticleField>::Builder( Tag(snam.size,     STATE_N), psizes_   ));
  icExprFactory.register_expression( new Expr::ConstantExpr<ParticleField>::Builder( Tag(snam.xvel,     STATE_N), 0.0      ));

  std::set<Expr::ExpressionID> icID;
  for( EqnList::iterator ieq=equations_.begin(); ieq!=equations_.end(); ++ieq){
    icID.insert( (*ieq)->initial_condition( icExprFactory ) );
  }
  assert( icID.size() >= equations_.size() );

  try{
    Expr::ExpressionTree icTree(icExprFactory, patch_.id() );
    icTree.insert_tree( icID );
    icTree.register_fields( fml );
    icTree.lock_fields(fml);
    fml.allocate_fields( patch_.field_info() );
    icTree.bind_fields( fml );
    icTree.bind_operators( patch_.operator_database() );
    icTree.execute_tree();
  }
  catch( std::runtime_error& e ){
    std::ostringstream err;
    err << "Error while setting particle intitial conditions." << std::endl
        << e.what() << std::endl;
    throw std::runtime_error( err.str() );
  }
  //initialize all the coal variables.....
  if( coalimplement_ )  coalInterface_->set_initial_conditions( fml );
}

//--------------------------------------------------------------------------------

void ParticleSetup::setup_coal_subprocesses( Expr::TimeStepper* integrator, const YAML::Node& pg )
{
  const ParticleStringNames& snam = ParticleStringNames::self();
  const StringNames& gasName = StringNames::self();

  typedef Particle::CelltoParticleInterpolantExpr<CellFieldToParticleOp>::Builder C2Pexprbuilder;

  //define all the tags needed for creating the coalinterface object.....
  const Tag prtTempTag    ( snam.ptemperature,        STATE_N );
  const Tag prtDensTag    ( snam.pdensity,            STATE_N );
  const Tag prtLocTag     ( snam.xpos,                STATE_N );
  const Tag prtMassTag    ( snam.mass,                STATE_N );
  const Tag prtSizeTag    ( snam.size,                STATE_N );
  const Tag intialPrtMass ( "Initial_"+snam.mass,     STATE_N );
  const Tag initialPrtDens( "Initial_"+snam.pdensity, STATE_N );
  const Tag initialPrtSize( "Initial_"+snam.size,     STATE_N );

  // Set tag species names based on gas chemistry model
  const GasChemistry gaschem = gaschem_model( pg["GasChemistry"].as<std::string>() );
  std::string specPrefix = "";
  switch (gaschem) {
    case COLD_FLOW  : case DETAILED_KINETICS:
      break;
    case FLAME_SHEET: case EQUILIBRIUM      :
      specPrefix = gasName.flsspecies + "_";
      break;

    case INVALID_GASMODEL:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid selection for gas-phase chemistry model" << std::endl << std::endl;
      throw std::runtime_error( msg.str() );
      break;
  }

  // set up a map so the coal code knows what species tags to use.
  coal::SpeciesTagMap spTagMap;
  for( int i=0; i<CanteraObjects::number_species(); ++i ){
    const std::string& spNam = CanteraObjects::species_name( i );
    const coal::GasSpeciesName spec = coal::gas_name_to_enum( spNam );
    const Tag specTag( specPrefix + spNam,STATE_N);
    const Tag intspecTag( "int_" + specPrefix + spNam, STATE_N );

    //interpolate species mass fraction from cell locations to particle locations
    exprFactory_.register_expression(new C2Pexprbuilder(intspecTag,specTag,prtLocTag,prtSizeTag));

    spTagMap[ spec ] = intspecTag;
  }

  // mixture molecular weight expression
  const Tag gasMWTag (gasName.mixtureMW,STATE_N);
  const Tag intgasMWTag ("int_"+gasName.mixtureMW,STATE_N);

  //interpolate mixture molecular weight from cell locations to particle locations
  exprFactory_.register_expression( new C2Pexprbuilder(intgasMWTag,gasMWTag,prtLocTag,prtSizeTag) );

  //interpolate the gas temperature to particle locations...
  const Tag gastempTag (gasName.temperature,STATE_N);
  const Tag intGasTempTag ("int_"+gasName.temperature,STATE_N);
  exprFactory_.register_expression( new C2Pexprbuilder(intGasTempTag,gastempTag,prtLocTag,prtSizeTag) );

  //interpolate gas pressure to particle locations...
  const Tag gaspressureTag (gasName.pressure,STATE_N);
  const Tag intGasPressureTag ("int_"+gasName.pressure,STATE_N);
  exprFactory_.register_expression( new C2Pexprbuilder(intGasPressureTag,gaspressureTag,prtLocTag,prtSizeTag) );

  const Tag ReTag(snam.parRe,STATE_N);
  //right now schmidt number of the gas phase passed as constant expression....not correct
  const Tag ScTag("parSc",STATE_N);
  exprFactory_.register_expression( new Expr::ConstantExpr<ParticleField>::Builder( ScTag,1.0 ) );
	
  const YAML::Node particlespg = pg["Particles"];

  const coal::CoalType coalType = coal::coal_type( particlespg["CoalType"].as<std::string>() );
  const DEV::DevModel devModel = DEV::devol_model( particlespg["DevModel"].as<std::string>() );
  const CHAR::CharModel charModel = CHAR::char_model( particlespg["CharModel"].as<std::string>() );

  std::cout << "\n  *******************************************************************\n"
      << "      Coal Type        : " << coal::coal_type_name(coalType) << "\n"
      << "      Devolatilization : " << DEV::dev_model_name(devModel) << "\n"
      << "      Char Oxidation   : " << CHAR::char_model_name(charModel)  << "\n"
      << "  *******************************************************************\n\n";
  coalInterface_ = new coal::CoalInterface<ParticleField>(
      coalType,         devModel,
      charModel,        prtSizeTag,
      prtTempTag,       intGasTempTag,
      intgasMWTag,      prtDensTag,
      intGasPressureTag,prtMassTag,
      ReTag,            ScTag,
      spTagMap,         intialPrtMass,
      initialPrtDens,   initialPrtSize
      );

  coalInterface_->register_expressions( exprFactory_ );
  coalInterface_->hook_up_time_integrator( *integrator, exprFactory_ );
  //set up coal source terms
  setup_coal_src_terms(pg);
}

//--------------------------------------------------------------------

void ParticleSetup::setup_coal_src_terms( const YAML::Node& pg )
{
  const double cellarea = pg["Particles"]["CellArea"].as<double>();

  typedef Expr::Expression<VolField>  GasRHSExpr;
  const ParticleStringNames& snam = ParticleStringNames::self();

  const Tag pxposTag( snam.xpos, STATE_N );
  const Tag psizeTag( snam.size, STATE_N );

  const StringNames& gasName = StringNames::self();
  const Tag gascoordTag( gasName.xcoord, STATE_N );

  typedef Particle::ParticletoCellMassExpr<ParticleFieldToCellOp>::Builder P2Cmassbuilder;

  const int nsp = CanteraObjects::number_species();
  //subtract particle production rates of coal subprocesses from individual gas phase species
  for( int i=0; i<nsp; ++i ){
    const std::string spNam = CanteraObjects::species_name( i );
    const coal::GasSpeciesName spec = coal::gas_name_to_enum( spNam );
    TagList tagList = coalInterface_->gas_species_source_term(spec,false);
    if( !tagList.empty()){
     const Tag P2C_species_src_Tag("P2CSpeciesSrc_"+spNam,STATE_N);
     const Expr::ExpressionID P2C_species_src_id =
       exprFactory_.register_expression( new P2Cmassbuilder( P2C_species_src_Tag, pxposTag, psizeTag, tagList,
                                                             gascoordTag, cellarea ) );

     exprFactory_.attach_dependency_to_expression( P2C_species_src_Tag,
                                                   Tag( "rhoY_"+spNam+"_RHS", STATE_N ) ,
                                                   Expr::SUBTRACT_SOURCE_EXPRESSION );
    }
  }

  //jtm- Currently, only one tar species is considered, hence the use of tag_list.

   const TagList tarTagList = tag_list( coalInterface_->tar_source_term() );
   const Tag P2C_tar_src_Tag("P2CTarSrc",STATE_N);
   std::cout<<std::endl<<"ParticleSetup: interpolating tar from particle to gas...     \n\n";
     exprFactory_.register_expression( new P2Cmassbuilder( P2C_tar_src_Tag, pxposTag, psizeTag, tarTagList,
                                                           gascoordTag, cellarea ) );
 // tar produced in coal phase is subtracted to gas phase
   exprFactory_.attach_dependency_to_expression( P2C_tar_src_Tag,
                                                 Tag( gasName.tarSourceTerm, STATE_N ) ,
                                                 Expr::SUBTRACT_SOURCE_EXPRESSION );

  //production rates from the particle are consumption rates for the gas phase....
  //subtracting them gas phase density....in CPD we have CHx species.....current implementation doesn't conserve mass
  const TagList speciesTaglist = coalInterface_->gas_species_source_terms();
  if(!speciesTaglist.empty()){
    const Tag P2C_density_src_Tag("P2CmassSrc",STATE_N);
    const Expr::ExpressionID P2C_density_src_id =
      exprFactory_.register_expression( new P2Cmassbuilder( P2C_density_src_Tag,
                                                            pxposTag, psizeTag, speciesTaglist,
                                                            gascoordTag, cellarea ) );

    exprFactory_.attach_dependency_to_expression( P2C_density_src_Tag,
                                                  Tag( gasName.density+"RHS", STATE_N ),
                                                  Expr::SUBTRACT_SOURCE_EXPRESSION );
  }

  //add production rates from the coal interface to particle mass
  const Expr::TagList pmasssrcTaglist = coalInterface_->particle_mass_rhs_taglist();
  if(!pmasssrcTaglist.empty()){
    for( Expr::TagList::const_iterator tagLiter=pmasssrcTaglist.begin(); tagLiter!=pmasssrcTaglist.end(); ++tagLiter ){
      exprFactory_.attach_dependency_to_expression( *tagLiter,
                                                    Tag(snam.mass+"_RHS",STATE_N),
                                                    Expr::ADD_SOURCE_EXPRESSION );
    }
  }

  //add the energy production rate from the coal models to particle temperature RHS..units K/s
  const Tag ptempprodTag = coalInterface_->particle_temperature_rhs_tag();
  if(ptempprodTag != Tag()){
    exprFactory_.attach_dependency_to_expression( ptempprodTag,
                                                  Tag(snam.ptemperature+"_RHS",STATE_N),
                                                  Expr::ADD_SOURCE_EXPRESSION );
  }

  //add the energy production rate from the coal models to gas phase energy
  //on the gas phase side.....we are solving total internal energy...
  //the production rate coming of coal is J/s ....should be scaled accordingly before adding to gas phase
  const Tag energytogasTag = coalInterface_->heat_released_to_gas_tag();
  if(energytogasTag!= Tag()){
    //register an expression to compute the source term for the gas phase
    typedef Particle::ParticletoCellEnergyExpr<ParticleFieldToCellOp>::Builder P2Cenergybuilder;
    const Tag P2C_energy_src_Tag("P2CenergySrc",STATE_N);
    exprFactory_.register_expression( new P2Cenergybuilder( P2C_energy_src_Tag,
                                                            pxposTag, psizeTag, energytogasTag,
                                                            gascoordTag, cellarea ) );

    exprFactory_.attach_dependency_to_expression( P2C_energy_src_Tag,
                                                  Tag( gasName.rhoE0+"RHS", STATE_N ) ,
                                                  Expr::ADD_SOURCE_EXPRESSION );
  }

}
  
//--------------------------------------------------------------------
//--------------------------------------------------------------------
/**
 *  \brief  Generate a vector of double (rhs) with the size of number of particles.
 *          For example, in the case particle size, this function return a vector of 
 *          particle sizes.
 *
 *  \par  params  : A yaml class including the required node
 *  \par  rhs     : a vector of double with size of npar
 *  \par  npar    : number of particle, specified in the input file
 */
void
particle_initial_fuctions( const YAML::Node& params,
                           std::vector<double>& rhs,
                           const int& npar)
{
  rhs.clear();
  
  /*
   *   'Constant' : All the particles have the same size, and the following produce a vector of
   *    the size specified.
   */
  if( params["Constant"] ){
    for (int i=0; i<npar; ++i) {
      rhs.push_back( params["Constant"].as<double>() );
    }
    return;
  }
  /*
   *  The number of particles with the desired size can be specified. 
   *  This node has two keyword as following:
   *
   *  Numbers : contains the number of particles with the desired sizes.
   *  Sizes   : contains the sizes of each group of particles.
   *
   *  For example: 
   *
   *  Numbers : [1, 2, 3]
   *  Sizes   : [x, y, z]
   *
   *  will produce the following vector for particles size:
   *
   *  [x, y, y, z, z, z]
   */

  if( params["Numbers"]){
    const std::vector<int> distribute = params["Numbers"].as<std::vector<int> >();
    const std::vector<double> sizes = params["Sizes"].as<std::vector<double> >();
    if(distribute.size() != sizes.size()){
      throw std::runtime_error("Particles Initilization: The size of 'Numbers' and 'Values' should be equal!");
    }
    
    int nparcheck = 0;
    for(int i=0; i < distribute.size(); ++i){
      nparcheck += distribute[i];
      for(int j=0; j< distribute[i]; ++j ){
        rhs.push_back( sizes[i]);
      }
    }

    if (nparcheck != npar) {
      throw std::runtime_error("Particles Initilization: The number of particles specified in the 'Numbers' is not match with 'npar'!");
    }
    return;
  }
  throw std::runtime_error("Particles Initilization: No or wrong keyword is specified!");
  
}
  
//--------------------------------------------------------------------
//--------------------------------------------------------------------
} // namespace Particle
