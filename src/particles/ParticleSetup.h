#ifndef ParticleSetup_h
#define ParticleSetup_h

#include <expression/ExprLib.h>

#include <OperatorsAndFields.h> 
#include "ParticleFieldsAndOperators.h"

//---------adds source term to the particle transport equations---------
#include "ParticleSrc.h"
#include <parser/ParseTools.h>

//--------CPD interface---------
#include <coal/CoalInterface.h>
/**
 *  \file ParticleSetup.h
 *  \defgroup Particle Particle Transport
 */

namespace SpatialOps{ class Grid; }

namespace Particle{


/**
 *  \class ParticleSetup
 *  \ingroup Particle
 *  \todo add documentation
 */
class ParticleSetup 
{

  typedef Expr::ExprPatch  PatchT;
  typedef SpatialOps::SVolField VolFieldT;
  
public:

  ParticleSetup( Expr::TimeStepper*,
                 PatchT&,
                 Expr::ExpressionFactory&,
                 const SpatialOps::Grid&,
                 const YAML::Node& );

  ~ParticleSetup();  

  /**
  *  \brief set initial conditions for all the particle variables
  *
  *  \param ParsedGroup
  */
  void set_initial_conditions( const YAML::Node& pg, Expr::ExpressionFactory& icExprFactory);

   /**
  *  \brief Creates source term object for the eddy event and adds it to particle X-momentum equation
  */
  void create_eddy_srcterm(const double* time_,const YAML::Node& pg);
  
  /**
  * \brief Provides support for the eddy events implementation for the particles
  */
  void set_eddy_events(const std::string& variable, const double time, const double eddylife, const int, const int);



private:  

  /**
  *  \brief Creates source term for accounting particle phase influence on both X and Y gas phase momentum....
  *   creates source term for the influence of particle temperature on gas phase and adds it to gas phase energy 
  */
  void add_P2C_srcterm(const YAML::Node& pg);

  /**
  * \brief couples coal subprocesses with the particle code
  */
  void setup_coal_subprocesses(Expr::TimeStepper*,const YAML::Node& pg);

  /**
  * \brief Adds contribution from the dispersed phase to the gas phase (mass coupling)
  */
  void setup_coal_src_terms( const YAML::Node& pg );

  std::vector<double> retunsparsize(const int npar);

  /**
  *  \brief Creates source term for accounting particle phase influence gas phase mass....
  *  Dependencies are attached to both species and density of the gas phase
  *  Dependencies are attached to particle mass also
  */
  void add_mass_src_term(const double* time_,const YAML::Node& pg);

  PatchT& patch_;
  Expr::ExpressionFactory& exprFactory_;
  const SpatialOps::Grid& grid_;

  /**
   * Build any particle-cell interpolants required here.
   */
  void setup_operators();

  typedef std::map< std::string, ParticleSrc* > TMSrc;
  TMSrc TMsrcterms_;
	
  typedef std::vector<ParticleSrc*> TMSrclist;
  TMSrclist TMSrclist_;

  typedef std::list<Expr::TransportEquation*> EqnList;
  EqnList equations_;

  //for CPD model
  coal::CoalInterface<ParticleField>* coalInterface_;
	
  //if true that means coal models are implemented and get the coal heat capacity defined in the coal models..otherwise compute the heat capacity
  const bool coalimplement_;
  double pdensity_;
  std::vector<double> psizes_;
};

void particle_initial_fuctions( const YAML::Node& params, std::vector<double>& rhs, const int& npar);
  
void mix_random_particle(std::vector<double>& rhs, const int randomseed);
} //name space Particle
#endif
