#ifndef ParticletoCellMomExpr_h
#define ParticletoCellMomExpr_h

#include <expression/Expression.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{
//====================================================================


/**
 *  @class ParticletoCellMomExpr
 *  @author Naveen Punati
 *  @date   February, 2011
 *
 *  @brief calculates particle field influence on gas phase momentum.
 */
template< typename ParticletoCellOp >
class ParticletoCellMomExpr
  : public Expr::Expression<typename ParticletoCellOp::DestFieldType>
{

  typedef typename ParticletoCellOp::SrcFieldType  ParticleFieldT;
  typedef typename ParticletoCellOp::DestFieldType  GasFieldT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pcT_, pdT_, psT_, pmT_,gcT_;
    const double ca_;
  public:

    Builder( const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const double);
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };

  ~ParticletoCellMomExpr(){}

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  ParticletoCellMomExpr( const Expr::Tag&,
                         const Expr::Tag&,
                         const Expr::Tag&,
                         const Expr::Tag&,
                         const Expr::Tag&,
                         const double);

  DECLARE_FIELD ( GasFieldT, gcoord_ )
  DECLARE_FIELDS( ParticleFieldT, parloc_, pdrag_, psize_, pmass_ )
  ParticletoCellOp* p2c_;
  const double cellarea_;

};

// ###################################################################
//
//                           Implementation
//
// ###################################################################


//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellMomExpr<ParticletoCellOp>::
ParticletoCellMomExpr( const Expr::Tag& pcoordTag,
                       const Expr::Tag& pdragTag,
                       const Expr::Tag& psizeTag,
                       const Expr::Tag& pmassTag,
                       const Expr::Tag& gcoordTag,
                       const double cellarea)
  : Expr::Expression<GasFieldT>(),
    cellarea_(cellarea)
{
  gcoord_ = this->template create_field_request<GasFieldT     >( gcoordTag );
  parloc_ = this->template create_field_request<ParticleFieldT>( pcoordTag );
  pdrag_  = this->template create_field_request<ParticleFieldT>( pdragTag  );
  psize_  = this->template create_field_request<ParticleFieldT>( psizeTag  );
  pmass_  = this->template create_field_request<ParticleFieldT>( pmassTag  );
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellMomExpr<ParticletoCellOp>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  p2c_ = opDB.retrieve_operator<ParticletoCellOp>();
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellMomExpr<ParticletoCellOp>::
evaluate()
{
  GasFieldT& result = this->value();

  const GasFieldT     & gcoord = gcoord_->field_ref();
  const ParticleFieldT& parloc = parloc_->field_ref();
  const ParticleFieldT& pdrag  = pdrag_ ->field_ref();
  const ParticleFieldT& psize  = psize_ ->field_ref();
  const ParticleFieldT& pmass  = pmass_ ->field_ref();

  using namespace SpatialOps;

  // get a scratch field of VolField type to interpolate
  SpatFldPtr<GasFieldT     > gasmomdiff = SpatialFieldStore::get<GasFieldT     >( gcoord );
  SpatFldPtr<ParticleFieldT> dragtmp    = SpatialFieldStore::get<ParticleFieldT>( parloc );

  *dragtmp <<= -pdrag * pmass;

  p2c_->set_coordinate_information( &parloc, NULL, NULL, &psize );
  p2c_->apply_to_field( *dragtmp, *gasmomdiff );

  const double dx = gcoord[1] - gcoord[0];
  const double volume = dx * cellarea_;

  result <<= *gasmomdiff / volume;
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellMomExpr<ParticletoCellOp>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& pcoordTag,
         const Expr::Tag& pdragTag,
         const Expr::Tag& psizeTag,
         const Expr::Tag& pmassTag,
         const Expr::Tag& gcoordTag,
         const double cellarea )
  : ExpressionBuilder(result),
    pcT_( pcoordTag ),
    pdT_(pdragTag),
    psT_(psizeTag),
    pmT_(pmassTag),
    gcT_(gcoordTag),
    ca_(cellarea)
{}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
Expr::ExpressionBase*
ParticletoCellMomExpr<ParticletoCellOp>::Builder::build() const
{
  return new ParticletoCellMomExpr<ParticletoCellOp> (pcT_, pdT_, psT_, pmT_,gcT_,ca_);
}

} // Particle

#endif // ParticletoCellMomExpr_h
