#ifndef ParticleMomentumRHS_Expr_h
#define ParticleMomentumRHS_Expr_h

#include <expression/Expression.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{

  /**
   *  \class MomentumRHS
   *  \ingroup Particle
   *  \brief Evaluates the RHS of the particle momentum equations.
   *
   *  \author James C. Sutherland
   *
   *  \par Template Parameters
   *  <ul>
   *   <li> \b FieldToParticleOp The operator that interpolates mesh variables to particle positions.
   *  </ul>
   *
   *  The particle momentum equation can be written as
   *   \f[
   *      m_p \frac{\mathrm{d} \mathbf{u}_{p}}{\mathrm{d} t} =
   *      m_{p}\mathbf{f}_{p}
   *      -\int_{\mathsf{S}} \left( \tau_{p}\cdot\mathbf{a}+p\mathbf{a} \right) \mathrm{d}\mathsf{S}
   *   \f]
   *
   *  Invoking a drag model for the stress term and assuming that the
   *  particle is sub-grid so that there is no resolved pressure
   *  distribution around the surface we can write we have
   *
   *  \f[
   *    \frac{\mathrm{d} m_{p}\mathbf{u}_{p}}{\mathrm{d} t} =
   *    c_{d}\frac{A_{p}}{2}\rho_{p}\left(\mathbf{v}-\mathbf{u}_{p}\right)^{2}
   *    +m_{p}\mathbf{f}_{p}
   *    -\mathsf{V}_p \nabla p,
   *  \f]
   *
   *  which is the working equation.
   *
   */

  //###################################################################
  //
  //
  //####################################################################

  template< typename FieldToParticleOp >
  class MomentumRHS
    : public Expr::Expression<SpatialOps::Particle::ParticleField>
  {
    typedef typename SpatialOps::Particle::ParticleField  ParticleField;
    typedef typename FieldToParticleOp::SrcFieldType      FieldT;

    const bool haveBodyForce_;

    DECLARE_FIELDS( ParticleField, particleDensity_, particlePos_, dragterm_ )
    DECLARE_FIELD ( FieldT, gasDensity_ )

    const FieldToParticleOp* op_;

    const double bodyForce_;

    MomentumRHS( const Expr::Tag& gasDensityTag,
                 const Expr::Tag& particleDensityTag,
                 const Expr::Tag& posTag,
                 const Expr::Tag& dragtermTag,
                 const double bodyForce );

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      Builder( const Expr::Tag& pMomRHSTag,
               const Expr::Tag& gasDensityTag,
               const Expr::Tag& particleDensityTag,
               const Expr::Tag& posTag,
               const Expr::Tag& pdragTag,
               const double bodyForce=0.0 );
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag gdt_, pdt_, ppt_,dragt_;
      const double bf_;
    };

    ~MomentumRHS(){}

    void bind_operators( const SpatialOps::OperatorDatabase& opDB );
    void evaluate();

  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################



  template<typename FieldToParticleOp>
  MomentumRHS<FieldToParticleOp>::
  MomentumRHS( const Expr::Tag& gasDensityTag,
               const Expr::Tag& particleDensityTag,
               const Expr::Tag& posTag,
               const Expr::Tag& pdragTag,
               const double bodyForce )
    : Expr::Expression<ParticleField>(),
      haveBodyForce_( bodyForce==0.0 ),
      bodyForce_( bodyForce )
  {
    this->set_gpu_runnable(true);

//  particleDensity_ = this->template create_field_request<ParticleField>( particleDensityTag );
//  particlePos_     = this->template create_field_request<ParticleField>( posTag             );
    dragterm_        = this->template create_field_request<ParticleField>( pdragTag           );
//  gasDensity_      = this->template create_field_request<FieldT       >( gasDensityTag      );
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  MomentumRHS<FieldToParticleOp>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    op_ = opDB.retrieve_operator<FieldToParticleOp>();
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  MomentumRHS<FieldToParticleOp>::
  evaluate()
  {
    using namespace SpatialOps;

    ParticleField& result = this->value();
    result <<= dragterm_->field_ref();
/*
    // assemble body force term:
    if( haveBodyForce_ ){
      SpatialOps::SpatFldPtr<ParticleField> tmp = SpatialOps::SpatialFieldStore<ParticleField>::self().get( result );
      op_->apply_to_field( *particlePos_,*gasDensity_, *tmp );
      *tmp *= -1.0;
      *tmp += *particleDensity_;
      const double grav = -9.8; // units are m/s^2
      *tmp *= grav;
      *tmp/= *particleDensity_;
      result += *tmp;
    }*/
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  MomentumRHS<FieldToParticleOp>::
  Builder::Builder( const Expr::Tag& momRHSTag,
                    const Expr::Tag& gasDensityTag,
                    const Expr::Tag& particleDensityTag,
                    const Expr::Tag& posTag,
                    const Expr::Tag& pdragTag,
                    const double bodyForce )
    : ExpressionBuilder( momRHSTag ),
      gdt_  ( gasDensityTag      ),
      pdt_  ( particleDensityTag ),
      ppt_  ( posTag             ),
      dragt_(pdragTag            ),
      bf_ ( bodyForce )
  {}

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  Expr::ExpressionBase*
  MomentumRHS<FieldToParticleOp>::Builder::build() const
  {
    return new MomentumRHS<FieldToParticleOp>( gdt_,pdt_, ppt_,dragt_,bf_ );
  }

  //------------------------------------------------------------------


} // Particle

#endif // ParticleMomentumRHS_Expr_h
