#include "ParticleSrc.h"

namespace Particle{

  
  //========================Implementation============================================

ParticleSrc::ParticleSrc( const double* time,
                          const SpatialOps::OperatorDatabase& opDB,
                          const ParticleField& coord, 
                          const ParticleField& pxvel, 
                          const ParticleField& pdrag,
                          const ParticleField& ptau,
                          const VolField& gascoord )  
  : time_  ( time  ),
    pcoord_( coord ),
    pXvel_ ( pxvel ),
    pDrag_ ( pdrag ),
    pTau_  ( ptau  ),
    gascoord_( gascoord ),
    C2P_( opDB.retrieve_operator<CellFieldToParticleOp>() )
{}

//--------------------------------------------------------------------

ParticleSrc::~ParticleSrc()
{}

//--------------------------------------------------------------------


void ParticleSrc::
add_eddy_event( const double tstart,
                const double tduration,
                const int sindex,
                const int eindex )
{  
  pardf_.clear();  
  // constructs the particle df at the time of eddy occurance
  ParticleField::const_iterator coorditer = pcoord_.begin();
  for( ; coorditer != pcoord_.end(); ++coorditer){
    double newloc = eddy_newloc( sindex, eindex, *coorditer );
    //std::cout<<" particle new loc : "<<newloc<<std::endl;
    if(newloc != 0.0)
      pardf_.push_back(newloc - *coorditer);
    else
      pardf_.push_back( 0.0);
  }
  eddies_.push_back( EddyInfo( tstart, 1.0*tduration, sindex,eindex, pardf_ ) );
}

//--------------------------------------------------------------------

void ParticleSrc::
operator()( ParticleField& field )
{  
  EddyList::iterator ieddy = eddies_.begin();
  EddyList::iterator ieddytemp = ieddy;

  std::vector<double> dessum;
  ParticleField::const_iterator idfiter = field.begin();
  for( ;idfiter != field.end(); ++idfiter ){
    dessum.push_back( 0.0 );
  }
  for( ; ieddy!=eddies_.end(); ++ieddy ){   

    const double starttime = ieddy->tstart;
    const double endtime = ieddy->tstop;
    
    if( *time_ >= starttime && *time_ <= endtime ){

      const double eddylife = endtime - starttime;
      const int sindex = ieddy->sindex;
      const int eindex = ieddy->eindex;
      std::vector<double> pardft0 = ieddy->pardft0;

      //applying the conditions for particle-eddy interaction
      //1. Particle leaves the eddy before eddy time elapses
      //2. Particle enters eddy between starttime and endtime
      //3. Particle leaves the eddy before its time elapses and enters it again


      // its time to apply the above specified conditions
      std::vector<double>::iterator dft0iter = pardft0.begin();
      ParticleField::const_iterator coorditer = pcoord_.begin();
      for(; dft0iter != pardft0.end(); ++dft0iter, ++coorditer){
        //std::cout<<" from eddy : "<< *dft0iter <<" new coord vel : "<< *newdfiter <<std::endl;
        const double newloc = eddy_newloc(sindex, eindex, *coorditer);
        if( *dft0iter != 0.0 && newloc == 0.0)
          *dft0iter = 0.0;
        else if( *dft0iter == 0.0 && newloc != 0.0){         
          *dft0iter =  (newloc - *coorditer) * ((endtime - *time_)/( endtime - starttime ));
          //*dft0iter =  *dft0iter * ((endtime - *time_)/( endtime - starttime ));
        }
        else{}
      }

      // sum the effect of all eddies in to dessum vector
      std::vector<double>::iterator dessumiter = dessum.begin();
      std::vector<double>::iterator desdfiter = pardft0.begin();
      for( ; desdfiter != pardft0.end(); ++dessumiter, ++desdfiter){
        *dessumiter += (*desdfiter)/eddylife;        
      }
    }
  
    // kill this eddy if we are past its lifetime
    else if( *time_ >= endtime ){
      ieddytemp = ++ieddy;
      --ieddy;
      eddies_.erase( ieddy );
      ieddy = ieddytemp;
    }

  }  // eddies loop closing

  ParticleField::iterator isrc = field.begin();      
  std::vector<double>::const_iterator idf = dessum.begin();
  ParticleField::const_iterator dragiter = pDrag_.begin();
  ParticleField::const_iterator veliter = pXvel_.begin();
  ParticleField::const_iterator tauiter = pTau_.begin();

  for( ; isrc != field.end(); ++isrc, ++idf,  ++dragiter, ++veliter, ++tauiter ){
    
    if( *idf != 0){
      const double dummy = ( *dragiter * ( *idf  - *veliter)  ) / *tauiter ;
      *isrc += dummy; 
    }  //if closing
  }
  
}

//-----------------------calculates new location for the particle--------------------  

double ParticleSrc::eddy_newloc(const int sindex, const int eindex, const double parcoord ){  

  const double y0 = gascoord_[sindex];
  const double eddyl = gascoord_[eindex] - y0 ;
  
  double newlocation=0.0;
  if( parcoord < y0 && parcoord > (y0+eddyl) )
    newlocation = 0.0;
  else {
    if(parcoord >= y0 && parcoord < (y0 +eddyl/3)){
      newlocation = 3*parcoord - 2*y0;
    }
    else if(parcoord >= (y0 +eddyl/3) && parcoord < (y0 + (2*eddyl)/3)){
      newlocation = 4*y0 + 2*eddyl - 3*parcoord;
    }
    else if(parcoord >= (y0 + (2*eddyl)/3) && parcoord <= (y0+eddyl)){
      newlocation = 3*parcoord - 2*y0 - 2*eddyl;
    }
  }

  return newlocation;  
}

} // namespace Particle

