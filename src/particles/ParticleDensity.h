#ifndef ParticleDensity_Expr_h
#define ParticleDensity_Expr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{

  /**
   *  \class ParticleDensity
   *  \ingroup Particle
   *  \brief Evaluates the particle density.
   *
   *  \author Naveen Punati
   *  \param  pmassTag - Particle Mass
   *  \param  psizeTag - Particle Size
   *
   */

  //###################################################################
  //
  //
  //####################################################################

  template< typename FieldT >
  class ParticleDensity
    : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, pmass_, psize_ )

    ParticleDensity( const Expr::Tag& pmassTag,
                     const Expr::Tag& psizeTag );

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      Builder( const Expr::Tag& pDensityTag,
               const Expr::Tag& pmassTag,
               const Expr::Tag& psizeTag );
      ~Builder(){}
      Expr::ExpressionBase* build() const{ return new ParticleDensity(pmassTag_,psizeTag_); }
    private:
      const Expr::Tag pmassTag_,psizeTag_;
    };

    ~ParticleDensity(){}

    void evaluate();
  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################



  template<typename FieldT>
  ParticleDensity<FieldT>::
  ParticleDensity(const Expr::Tag& pmassTag,
                  const Expr::Tag& psizeTag )
    : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable(true);

    pmass_ = this->template create_field_request<FieldT>( pmassTag );
    psize_ = this->template create_field_request<FieldT>( psizeTag );
  }

  //------------------------------------------------------------------

  template<typename FieldT>
  void
  ParticleDensity<FieldT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    const FieldT& pmass = pmass_->field_ref();
    const FieldT& psize = psize_->field_ref();
    result <<= pmass / ( (22.0/42.0) * pow( psize, 3.0 )) ;
  }

  //------------------------------------------------------------------

  template<typename FieldT>
  ParticleDensity<FieldT>::
  Builder::Builder( const Expr::Tag& pDensTag,
                    const Expr::Tag& pmassTag,
                    const Expr::Tag& psizeTag )
    : ExpressionBuilder(pDensTag),
      pmassTag_( pmassTag ),
      psizeTag_( psizeTag )
  {}

  //------------------------------------------------------------------

} // Particle

#endif // ParticleDensity_Expr_h
