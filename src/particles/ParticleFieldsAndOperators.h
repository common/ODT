#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/particles/ParticleOperators.h>

#include <OperatorsAndFields.h>

namespace Particle{

  typedef SpatialOps::Particle::ParticleField  ParticleField;     ///< Basic particle field type
  typedef SpatialOps::Particle::CellToParticle<VolField> CellFieldToParticleOp; ///< Interpolate mesh fields to lagrangian particle
  typedef SpatialOps::Particle::ParticleToCell<VolField> ParticleFieldToCellOp; ///< Interpolate particle fields to Eulerian mesh

} // namespace Particle
