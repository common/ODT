#ifndef ParticleDragCoefficient_Expr_h
#define ParticleDragCoefficient_Expr_h

#include <expression/Expression.h>

#include "ParticleFieldsAndOperators.h"
#include <cmath>

namespace Particle{

/**
 *  \class DragCoefficient
 *  \ingroup Particle
 *  \brief Calculates the particle drag coefficient (currently the value is hard coded).
 */
class DragCoefficient
 : public Expr::Expression<ParticleField>
{
  DragCoefficient( const Expr::Tag& parRe );

  DECLARE_FIELD( ParticleField, pRe_ )

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& dragTag,
             const Expr::Tag& reTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new DragCoefficient(pRet_); }
  private:
    const Expr::Tag pRet_;
  };

  ~DragCoefficient(){}
  void evaluate();
};

//###########################################################
//
//                  Implementation
//
//##########################################################

DragCoefficient::DragCoefficient( const Expr::Tag& parRe )
  : Expr::Expression<ParticleField>()
{
  this->set_gpu_runnable(true);
  pRe_ = this->create_field_request<ParticleField>( parRe );
}

//--------------------------------------------------------------------

void
DragCoefficient::evaluate()
{
  using namespace SpatialOps;
  ParticleField& result = this->value();
  const ParticleField& pRe = pRe_->field_ref();

  result <<= cond( pRe <= 1.0,  1.0 )
                 ( pRe <= 1000, 1.0 + 0.15 * pow( pRe, 0.687 ) )
                 ( 0.0183 * pRe );
}

//--------------------------------------------------------------------

DragCoefficient::Builder::Builder( const Expr::Tag& dragTag,
                                   const Expr::Tag& parRe )
: ExpressionBuilder(dragTag),
  pRet_(parRe)
{}

//--------------------------------------------------------------------

} // namespace Particle

#endif // DragCoefficient_Expr_h
