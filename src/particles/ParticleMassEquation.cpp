#include "ParticleMassEquation.h"
#include "ParticleStringNames.h"

#include "ParticleMassRHS.h"

namespace Particle{

  //==================================================================

  class ParticleMassIC : public Expr::Expression<ParticleField> {
  public:
    struct Builder : public Expr::ExpressionBuilder
    {
      Builder( const Expr::Tag& pmasst,
               const Expr::Tag& pdensityt,
               const Expr::Tag& sizet )
      : ExpressionBuilder(pmasst),
        pdensityt_(pdensityt), sizet_(sizet)
      {}
      Expr::ExpressionBase* build() const{ return new ParticleMassIC(pdensityt_, sizet_); }
    private:
      const Expr::Tag pdensityt_, sizet_;
    };
    void evaluate()
    {
      using namespace SpatialOps;
      ParticleField& mass = this->value();
      const ParticleField& pdensity = pdensity_->field_ref();
      const ParticleField& size     = size_    ->field_ref();
      mass <<= (3.141592653589793) * size * size * size * pdensity / 6.0;
    }
    private:
    ParticleMassIC(const Expr::Tag pdensityt,
                   const Expr::Tag sizet)
    : Expr::Expression<ParticleField>()
    {
      this->set_gpu_runnable(true);
      pdensity_ = this->create_field_request<ParticleField>( pdensityt );
      size_     = this->create_field_request<ParticleField>( sizet     );
    }
    DECLARE_FIELDS( ParticleField, pdensity_, size_ )
  };

  //==================================================================
  //------------------------------------------------------------------


  ParticleMassEquation::
  ParticleMassEquation( Expr::ExpressionFactory& exprFactory)
  : Expr::TransportEquation( get_solnvar_name(), get_rhs_tag() )
  {
    exprFactory.register_expression( new Particle::ParticleMassRHS::Builder( get_rhs_tag() ) );
  }

  //------------------------------------------------------------------

  ParticleMassEquation::~ParticleMassEquation()
  {}

  //------------------------------------------------------------------

  Expr::ExpressionID
  ParticleMassEquation::
  initial_condition( Expr::ExpressionFactory& exprFactory)
  {
    const ParticleStringNames& sName = ParticleStringNames::self();
    Expr::Tag pmasst    ( sName.mass,     Expr::STATE_N);
    Expr::Tag pdensityt ( sName.pdensity, Expr::STATE_N);
    Expr::Tag sizet     ( sName.size,     Expr::STATE_N);
    return exprFactory.register_expression( new ParticleMassIC::Builder( pmasst, pdensityt, sizet));
  }

  //------------------------------------------------------------------

  std::string
  ParticleMassEquation::
  get_solnvar_name()
  {
    return Particle::ParticleStringNames::self().mass;
  }

  //------------------------------------------------------------------

  Expr::Tag
  ParticleMassEquation::
  get_rhs_tag( )
  {
    const Particle::ParticleStringNames& snam = Particle::ParticleStringNames::self();
    return Expr::Tag( snam.mass+"_RHS", Expr::STATE_N );
  }

  //------------------------------------------------------------------

} // namespace Particle
