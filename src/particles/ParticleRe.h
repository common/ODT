#ifndef ParticleRe_Expr_h
#define ParticleRe_Expr_h

#include <expression/Expression.h>

#include "ParticleFieldsAndOperators.h"
#include<cmath>

namespace Particle{

/**
 *  \class ReynoldsNumber
 *  \ingroup Particle
 *  \brief Calculates the particle ReynoldsNumber.
 */
template< typename FieldToParticleOp >
class ReynoldsNumber
 : public Expr::Expression<ParticleField>
{
  typedef typename FieldToParticleOp::SrcFieldType      FieldT;

  DECLARE_FIELDS( ParticleField, psize_, ppos_, pvel_ )
  DECLARE_FIELDS( FieldT, gasVisc_, gasVel_, pdensity_ )

  FieldToParticleOp* op_;

  ReynoldsNumber( const Expr::Tag& particleDensity,
                  const Expr::Tag& particleSize,
                  const Expr::Tag& particleVel,
                  const Expr::Tag& particlePos,
                  const Expr::Tag& gasViscosity,
                  const Expr::Tag& gasVelocity );

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  \brief Create a ReynoldsNumber::Builder
     *  \param particleDensity the particle mass density
     *  \param particleSize the size (diameter) of the particle
     *  \param particleVel the particle velocity
     *  \param particlePos the particle position
     *  \param gasViscosity the local gas phase viscosity
     *  \param gasVelocity  the local gas phase velocity
     */
    Builder( const Expr::Tag& reTag,
             const Expr::Tag& particleDensity,
             const Expr::Tag& particleSize,
             const Expr::Tag& particleVel,
             const Expr::Tag& particlePos,
             const Expr::Tag& gasViscosity,
             const Expr::Tag& gasVelocity );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const Expr::Tag pdt_, pst_, pvt_, ppt_, gvist_, gvelt_;
  };

  ~ReynoldsNumber(){}

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

};

// ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################

template< typename FieldToParticleOp >
ReynoldsNumber<FieldToParticleOp>::
ReynoldsNumber( const Expr::Tag& particleDensity,
                const Expr::Tag& particleSize,
                const Expr::Tag& particleVel,
                const Expr::Tag& particlePos,
                const Expr::Tag& gasViscosity,
                const Expr::Tag& gasVelocity )
  : Expr::Expression<ParticleField>()
{
  psize_    = this->template create_field_request<ParticleField>( particleSize );
  pvel_     = this->template create_field_request<ParticleField>( particleVel  );
  ppos_     = this->template create_field_request<ParticleField>( particlePos  );

  pdensity_ = this->template create_field_request<FieldT>( particleDensity );
  gasVisc_  = this->template create_field_request<FieldT>( gasViscosity    );
  gasVel_   = this->template create_field_request<FieldT>( gasVelocity     );
}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
void
ReynoldsNumber<FieldToParticleOp>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  op_ = opDB.retrieve_operator<FieldToParticleOp>();
}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
void
ReynoldsNumber<FieldToParticleOp>::evaluate()
{
  using namespace SpatialOps;
  ParticleField& result = this->value();

  const ParticleField& psize = psize_->field_ref();
  const ParticleField& pvel  = pvel_ ->field_ref();
  const ParticleField& ppos  = ppos_ ->field_ref();

  const FieldT& pdensity = pdensity_->field_ref();
  const FieldT& gasVisc  = gasVisc_ ->field_ref();
  const FieldT& gasVel   = gasVel_  ->field_ref();

  SpatFldPtr<ParticleField> inttmpvel = SpatialFieldStore::get<ParticleField>( result );
  SpatFldPtr<ParticleField> inttmpvis = SpatialFieldStore::get<ParticleField>( result );
  SpatFldPtr<ParticleField> intgasden = SpatialFieldStore::get<ParticleField>( result );

  op_->set_coordinate_information( &ppos, NULL, NULL, &psize );
  op_->apply_to_field( gasVel,   *inttmpvel );
  op_->apply_to_field( gasVisc,  *inttmpvis );
  op_->apply_to_field( pdensity, *intgasden );

  result <<= abs( *inttmpvel - pvel ) * *intgasden * psize / *inttmpvis;
}

//--------------------------------------------------------------------

template< typename FieldToParticleOp >
ReynoldsNumber<FieldToParticleOp>::Builder::
Builder( const Expr::Tag& reTag,
         const Expr::Tag& particleDensity,
         const Expr::Tag& particleSize,
         const Expr::Tag& particleVel,
         const Expr::Tag& particlePos,
         const Expr::Tag& gasViscosity,
         const Expr::Tag& gasVelocity )
  : ExpressionBuilder( reTag ),
    pdt_  ( particleDensity ),
    pst_  ( particleSize    ),
    pvt_  ( particleVel     ),
    ppt_  ( particlePos     ),
    gvist_( gasViscosity    ),
    gvelt_( gasVelocity     )
{}

//--------------------------------------------------------------------
template< typename FieldToParticleOp >
Expr::ExpressionBase*
ReynoldsNumber<FieldToParticleOp>::Builder::build() const
{
  return new ReynoldsNumber( pdt_, pst_, pvt_,ppt_, gvist_,gvelt_ );
}


} // namespace Particle
#endif // ParticleRe_Expr_h
