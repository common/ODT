#ifndef ParticletoCellEnergyExpr_h
#define ParticletoCellEnergyExpr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

#include "ParticleFieldsAndOperators.h"

namespace Particle{
//====================================================================


/**
 *  @class ParticletoCellEnergyExpr
 *  @author Naveen Punati
 *  @date   February, 2011
 *
 *  @brief calculates particle field influence on gas phase energy.
 */
template< typename ParticletoCellOp >
class ParticletoCellEnergyExpr
  : public Expr::Expression<typename ParticletoCellOp::DestFieldType>
{
  typedef typename ParticletoCellOp::SrcFieldType  ParticleFieldT;
  typedef typename ParticletoCellOp::DestFieldType GasFieldT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pcT_, psT_,gcT_,penergyT_;
    const double ca_;
  public:

    Builder( const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const double );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };

  ~ParticletoCellEnergyExpr(){}

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  ParticletoCellEnergyExpr( const Expr::Tag&,
                            const Expr::Tag&,
                            const Expr::Tag&,
                            const Expr::Tag&,
                            const double );

  DECLARE_FIELD ( GasFieldT, gcoord_ )
  DECLARE_FIELDS( ParticleFieldT, parloc_, psize_, penergy_ )
  ParticletoCellOp* p2c_;
  const double cellarea_;
};

// ###################################################################
//
//                           Implementation
//
// ###################################################################


//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellEnergyExpr<ParticletoCellOp>::
ParticletoCellEnergyExpr( const Expr::Tag& pcoordTag,
                          const Expr::Tag& psizeTag,
                          const Expr::Tag& penergyTag,
                          const Expr::Tag& gcoordTag,
                          const double cellarea )
  : Expr::Expression<GasFieldT>(),
    cellarea_( cellarea )
{
  gcoord_  = this->template create_field_request<GasFieldT     >( gcoordTag  );
  parloc_  = this->template create_field_request<ParticleFieldT>( pcoordTag  );
  psize_   = this->template create_field_request<ParticleFieldT>( psizeTag   );
  penergy_ = this->template create_field_request<ParticleFieldT>( penergyTag );
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellEnergyExpr<ParticletoCellOp>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  p2c_ = opDB.retrieve_operator<ParticletoCellOp>();
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellEnergyExpr<ParticletoCellOp>::
evaluate()
{
  using namespace SpatialOps;
  GasFieldT& result = this->value();

  const GasFieldT     & gcoord  = gcoord_ ->field_ref();
  const ParticleFieldT& parloc  = parloc_ ->field_ref();
  const ParticleFieldT& psize   = psize_  ->field_ref();
  const ParticleFieldT& penergy = penergy_->field_ref();

  result <<= 0.0;

  //interpolate the energy production from particle locations to mesh
  p2c_->set_coordinate_information( &parloc, NULL, NULL, &psize );
  p2c_->apply_to_field( penergy, result );

  const double dx = gcoord[1] - gcoord[0];
  const double volume = dx * cellarea_;

  result <<= result/volume ;
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellEnergyExpr<ParticletoCellOp>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& pcoordTag,
         const Expr::Tag& psizeTag,
         const Expr::Tag& penergyTag,
         const Expr::Tag& gcoordTag,
         const double cellarea )
  : ExpressionBuilder(result),
    pcT_( pcoordTag ),
    psT_(psizeTag),
    penergyT_(penergyTag),
    gcT_(gcoordTag),
    ca_(cellarea)
{}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
Expr::ExpressionBase*
ParticletoCellEnergyExpr<ParticletoCellOp>::Builder::
build() const
{
  return new ParticletoCellEnergyExpr<ParticletoCellOp> (pcT_, psT_,penergyT_,gcT_,ca_);
}

} // Particle

#endif // ParticletoCellEnergyExpr_h
