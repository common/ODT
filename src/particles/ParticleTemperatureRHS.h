#ifndef ParticleTemperatureRHS_Expr_h
#define ParticleTemperatureRHS_Expr_h

#include <expression/Expression.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{

  /**
   *  \class ParticleTemperatureRHS
   *  \ingroup Particle
   *  \brief Evaluates the RHS of the particle temperature equation.
   *
   *  \author Naveen Punati
   *
   *
   */

  template< typename FieldT >
  class ParticleTemperatureRHS
  : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, pxpos_, pmass_, ptemp_, psize_, pcp_, pwalltemp_, convterm_ )

    ParticleTemperatureRHS(const Expr::Tag& pxposTag,
                           const Expr::Tag& pmassTag,
                           const Expr::Tag& ptempTag,
                           const Expr::Tag& psizeTag,
                           const Expr::Tag& pcoalcpTag,
                           const Expr::Tag& walltempTag,
                           const Expr::Tag& convtermTag );

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      Builder( const Expr::Tag& pTRHSTag,
               const Expr::Tag& pxposTag,
               const Expr::Tag& pmassTag,
               const Expr::Tag& ptempTag,
               const Expr::Tag& psizeTag,
               const Expr::Tag& pcoalcpTag,
               const Expr::Tag& walltempTag,
               const Expr::Tag& convtermTag );
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag pxposTag_,pmassTag_,ptempTag_,psizeTag_,pcoalcpTag_,walltempTag_,convtermTag_;
    };

    ParticleTemperatureRHS(){}
    void evaluate();
  };





  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################

  template<typename FieldT>
  ParticleTemperatureRHS<FieldT>::
  ParticleTemperatureRHS( const Expr::Tag& pxposTag,
                          const Expr::Tag& pmassTag,
                          const Expr::Tag& ptempTag,
                          const Expr::Tag& psizeTag,
                          const Expr::Tag& pcoalcpTag,
                          const Expr::Tag& walltempTag,
                          const Expr::Tag& convtermTag )
  : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable(true);

    pxpos_     = this->template create_field_request<FieldT>( pxposTag    );
    pmass_     = this->template create_field_request<FieldT>( pmassTag    );
    ptemp_     = this->template create_field_request<FieldT>( ptempTag    );
    psize_     = this->template create_field_request<FieldT>( psizeTag    );
    pcp_       = this->template create_field_request<FieldT>( pcoalcpTag  );
    pwalltemp_ = this->template create_field_request<FieldT>( walltempTag );
    convterm_  = this->template create_field_request<FieldT>( convtermTag );
  }

  //------------------------------------------------------------------

  template<typename FieldT>
  void
  ParticleTemperatureRHS<FieldT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& tempRHS = this->value();

    const FieldT& pxpos     = pxpos_    ->field_ref();
    const FieldT& pmass     = pmass_    ->field_ref();
    const FieldT& ptemp     = ptemp_    ->field_ref();
    const FieldT& psize     = psize_    ->field_ref();
    const FieldT& pcp       = pcp_      ->field_ref();
    const FieldT& pwalltemp = pwalltemp_->field_ref();
    const FieldT& convterm  = convterm_ ->field_ref();

    //define emissivity and stefan-boltzman constant(sigma) for the radiation model
    const double emissivity = 0.8; //unit less
    const double sigma = 5.6704e-8; // W/(m^2*K^4)

    // add the contribution from convective and radiation models to RHS
    tempRHS <<= convterm
       // radiation term: ( A_p / (m_p*c_p)) * epsilon*sigma * (T_w^4-T_p^4)
       + ( (3.1416 * pow( psize,2.0)) / (pmass * pcp) )
       * sigma * emissivity * ( pow( pwalltemp,4.0) - pow( ptemp,4.0) );
  }

  //------------------------------------------------------------------

  template<typename FieldT>
  ParticleTemperatureRHS<FieldT>::
  Builder::Builder( const Expr::Tag& pTRHSTag,
                    const Expr::Tag& pxposTag,
                    const Expr::Tag& pmassTag,
                    const Expr::Tag& ptempTag,
                    const Expr::Tag& psizeTag,
                    const Expr::Tag& pcoalcpTag,
                    const Expr::Tag& walltempTag,
                    const Expr::Tag& convtermTag)
  : ExpressionBuilder(pTRHSTag),
    pxposTag_( pxposTag ),
    pmassTag_( pmassTag ),
    ptempTag_( ptempTag),
    psizeTag_(psizeTag),
    pcoalcpTag_(pcoalcpTag),
    walltempTag_(walltempTag),
    convtermTag_(convtermTag)
  {}

  //------------------------------------------------------------------

  template<typename FieldT>
  Expr::ExpressionBase*
  ParticleTemperatureRHS<FieldT>::Builder::build() const
  {
    return new ParticleTemperatureRHS<FieldT>( pxposTag_,pmassTag_,ptempTag_,psizeTag_,pcoalcpTag_,walltempTag_,convtermTag_ );
  }



} // Particle

#endif // ParticleTemperatureRHS_Expr_h
