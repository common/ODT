#ifndef ParticleVelocityIC_h
#define ParticleVelocityIC_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

#include "ParticleFieldsAndOperators.h"
#include<cmath>


namespace Particle{

  /**
   *  \class   ParticleVelocityIC
   *  \ingroup Particle
   *  \brief   Initialize the particle velocity base on initial gas velocity
   *
   *  \author Babak Goshayeshi (www.bgoshayeshi.com)
   */

  template< typename FieldToParticleOp >
  class ParticleVelocityIC
      : public Expr::Expression<ParticleField>
  {
    typedef typename FieldToParticleOp::SrcFieldType      FieldT;

    DECLARE_FIELDS( ParticleField, pxpos_, psize_ )
    DECLARE_FIELD( FieldT, gasvel_ )

    FieldToParticleOp* op_;

    ParticleVelocityIC(const Expr::Tag& pxpost,
                       const Expr::Tag& gasvelt,
                       const Expr::Tag& psizet);

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      Builder( const Expr::Tag& pyvelt,
               const Expr::Tag& pxpost,
               const Expr::Tag& gasvelt,
               const Expr::Tag& psizet);
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag pxpost_, gasvelt_, psizet_;
    };

    ~ParticleVelocityIC(){}

    void bind_operators( const SpatialOps::OperatorDatabase& opDB );
    void evaluate();
  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################



  template<typename FieldToParticleOp>
  ParticleVelocityIC<FieldToParticleOp>::
  ParticleVelocityIC( const Expr::Tag& pxpost,
                      const Expr::Tag& gasvelt,
                      const Expr::Tag& psizet )
  : Expr::Expression<ParticleField>()
  {
    pxpos_ = this->template create_field_request<ParticleField>( pxpost );
    psize_ = this->template create_field_request<ParticleField>( psizet );

    gasvel_ = this->template create_field_request<FieldT>( gasvelt );
  }

  //--------------------------------------------------------------------

  template< typename FieldToParticleOp >
  void
  ParticleVelocityIC<FieldToParticleOp>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    op_ = opDB.retrieve_operator<FieldToParticleOp>();
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  ParticleVelocityIC<FieldToParticleOp>::
  evaluate()
  {
    ParticleField& result = this->value();
    const ParticleField& pxpos = pxpos_ ->field_ref();
    const ParticleField& psize = psize_ ->field_ref();
    const FieldT       & gasvel= gasvel_->field_ref();
    op_->set_coordinate_information( &pxpos, NULL, NULL, &psize );
    op_->apply_to_field( gasvel, result );
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  ParticleVelocityIC<FieldToParticleOp>::
  Builder::Builder(const Expr::Tag& pyvelt,
                   const Expr::Tag& pxpost,
                   const Expr::Tag& gasvelt,
                   const Expr::Tag& psizet )
  : ExpressionBuilder(pyvelt),
    pxpost_  ( pxpost  ),
    gasvelt_ ( gasvelt ),
    psizet_  ( psizet  )
  {}

  //--------------------------------------------------------------------

  template< typename FieldToParticleOp >
  Expr::ExpressionBase*
  ParticleVelocityIC<FieldToParticleOp>::Builder::build() const
  {
    return new ParticleVelocityIC( pxpost_,gasvelt_, psizet_);
  }


} // Particle

#endif // ParticleVelocityIC_h
