/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ParticleTempGasRadTerm_Expr_h
#define ParticleTempGasRadTerm_Expr_h

#include <expression/Expression.h>

/**
 *  \class ParticleTempGasRadTerm
 */
template< typename ParticleT,
          typename FieldT >
class ParticleTempGasRadTerm
 : public Expr::Expression<ParticleT>
{
  DECLARE_FIELDS( FieldT,    emissivity_, gasTemp_ )
  DECLARE_FIELDS( ParticleT, cp_, prtDiam_, prtMass_, prtTemp_ )
  const double sigma;
  const double pi;

  /* declare any operators associated with this expression here */
  
  ParticleTempGasRadTerm( const Expr::Tag& cpTag,
                          const Expr::Tag& emissivityTag,
                          const Expr::Tag& gasTempTag,
                          const Expr::Tag& prtDiamTag,
                          const Expr::Tag& prtMassTag,
                          const Expr::Tag& prtTempTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a ParticleTempGasRadTerm expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& emissivityTag,
             const Expr::Tag& gasTempTag,
             const Expr::Tag& prtDiamTag,
             const Expr::Tag& prtMassTag,
             const Expr::Tag& prtTempTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS );

    Expr::ExpressionBase* build() const;

  private:
    const Expr::Tag cpTag_, emissivityTag_, gasTempTag_, prtDiamTag_, prtMassTag_, prtTempTag_;
  };

  ~ParticleTempGasRadTerm();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename ParticleT, typename FieldT >
ParticleTempGasRadTerm<ParticleT, FieldT>::

ParticleTempGasRadTerm( const Expr::Tag& cpTag,
                        const Expr::Tag& emissivityTag,
                        const Expr::Tag& gasTempTag,
                        const Expr::Tag& prtDiamTag,
                        const Expr::Tag& prtMassTag,
                        const Expr::Tag& prtTempTag )
  : Expr::Expression<ParticleT>(),
    sigma(5.670373e-8   ), // W/(m^2*K^4)
    pi   (3.14159265359 )
{
  emissivity_ = this->template create_field_request<FieldT   >( emissivityTag );
  gasTemp_    = this->template create_field_request<FieldT   >( gasTempTag    );
  cp_         = this->template create_field_request<ParticleT>( cpTag         );
  prtDiam_    = this->template create_field_request<ParticleT>( prtDiamTag    );
  prtMass_    = this->template create_field_request<ParticleT>( prtMassTag    );
  prtTemp_    = this->template create_field_request<ParticleT>( prtTempTag    );
}

//--------------------------------------------------------------------

template< typename ParticleT, typename FieldT >
ParticleTempGasRadTerm<ParticleT, FieldT>::
~ParticleTempGasRadTerm()
{}

//--------------------------------------------------------------------

template< typename ParticleT, typename FieldT >
void
ParticleTempGasRadTerm<ParticleT, FieldT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  // bind operators as follows:
  // op_ = opDB.retrieve_operator<OpT>();
}

//--------------------------------------------------------------------

template< typename ParticleT, typename FieldT >
void
ParticleTempGasRadTerm<ParticleT, FieldT>::
evaluate()
{
  ParticleT& result = this->value();

  const FieldT&    gasTemp    = gasTemp_   ->field_ref();
  const FieldT&    emissivity = emissivity_->field_ref();
  const ParticleT& cp         = cp_        ->field_ref();
  const ParticleT& prtDiam    = prtDiam_   ->field_ref();
  const ParticleT& prtMass    = prtMass_   ->field_ref();
  const ParticleT& prtTemp    = prtTemp_   ->field_ref();

  const int numCells   = gasTemp.window_without_ghost().local_npts();

  /*
   * The evaluation below uses average values for gas temperature
   * and emissivity.
   */

  result <<= pi * pow(prtDiam,4) / (prtMass * cp)
		   * sigma*nebo_sum_interior(emissivity)/numCells
		   * ( pow(nebo_sum_interior(gasTemp)/numCells, 4) - pow(prtTemp, 4) ) ;
}

//--------------------------------------------------------------------

template< typename ParticleT, typename FieldT >
ParticleTempGasRadTerm<ParticleT, FieldT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& cpTag,
                  const Expr::Tag& emissivityTag,
                  const Expr::Tag& gasTempTag,
                  const Expr::Tag& prtDiamTag,
                  const Expr::Tag& prtMassTag,
                  const Expr::Tag& prtTempTag,
                  const int nghost )
  : ExpressionBuilder( resultTag, nghost ),
    cpTag_        ( cpTag         ),
    emissivityTag_( emissivityTag ),
    gasTempTag_   ( gasTempTag    ),
    prtDiamTag_   ( prtDiamTag    ),
    prtMassTag_   ( prtMassTag    ),
    prtTempTag_   ( prtTempTag    )
{}

//--------------------------------------------------------------------

template< typename ParticleT, typename FieldT >
Expr::ExpressionBase*
ParticleTempGasRadTerm<ParticleT, FieldT>::
Builder::build() const
{
  return new ParticleTempGasRadTerm<ParticleT, FieldT>( cpTag_,emissivityTag_,gasTempTag_,prtDiamTag_,prtMassTag_,prtTempTag_ );
}


#endif // ParticleTempGasRadTerm_Expr_h
