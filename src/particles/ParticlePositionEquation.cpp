#include "ParticlePositionEquation.h"
#include "ParticleStringNames.h"

#include "ParticlePositionRHS.h"

namespace Particle{

  /**
   *  \class   ParticlePosIC
   *  \ingroup Particle
   *  \brief   Initializing particle postion in x direction 
   *  \author  Babak Goshayeshi (www.bgoshayeshi.com)
   */
  class ParticlePosIC: public Expr::Expression<ParticleField> {
  public:
    struct Builder : public Expr::ExpressionBuilder
    {
      /**
       * @brief Construct a ParticlePosIC expression
       * @param particles position tag
       * @param Location where particles placement start
       * @param Location where particles placement end
       * @param number of particle
       * @param Randomseed for randmon particles placement
       */
      Builder( const Expr::Tag& ppost,
               const double startloc,
               const double endloc,
               const int npar,
               const int rndseed )
      : ExpressionBuilder(ppost),
        startloc_( startloc ),
        endloc_  ( endloc   ),
        npar_    ( npar     ),
        rndseed_ ( rndseed  )
      {}

      Expr::ExpressionBase* build() const{ return new ParticlePosIC(startloc_, endloc_, npar_, rndseed_); }

    private:
      const double  startloc_, endloc_;
      const int     npar_, rndseed_;
    };

    void evaluate()
    {
      ParticleField& pxpos = this->value();
      ParticleField::iterator ipxpos = pxpos.begin();
      for (int i=0; ipxpos != pxpos.end(); ++ipxpos, ++i) {
        *ipxpos = startloc_ + i*(endloc_-startloc_)/npar_;
      };

      if( rndseed_>0 ){
        mix_random_particle( pxpos );
      }
    }

  private:

    ParticlePosIC( const double startloc,
                   const double endloc,
                   const int npar,
                   const int rndseed )
    : Expr::Expression<ParticleField>(),
      startloc_ ( startloc ),
      endloc_   ( endloc   ),
      npar_     ( npar     ),
      rndseed_  ( rndseed  )
    {}

    // arrange the particles position randomly
    void
    mix_random_particle( ParticleField& pxpos )
    {
      srand( rndseed_ );

      std::vector<double> temp;
      temp.clear();
      
      ParticleField::iterator ipxpos = pxpos.begin();
      for( ; ipxpos != pxpos.end(); ++ipxpos ){
        temp.push_back( *ipxpos );
      }
      
      ipxpos = pxpos.begin();
      for( int i=0; ipxpos != pxpos.end()-1; ++ipxpos, ++i ){
        const int rnd = rand() % (npar_-i);

        *ipxpos = temp[rnd];
        temp.erase(temp.begin() + rnd);
      }
      *ipxpos = temp[0];
    }

    const double  startloc_, endloc_;
    const int     npar_, rndseed_;
  };

  // #################################################################
  //
  //                          Implementation
  //
  // #################################################################




  ParticlePositionEquation::
  ParticlePositionEquation( Expr::ExpressionFactory& exprFactory,
                            const Direction dir,
                            const double startloc,
                            const double endloc,
                            const int npar,
                            const int rndseed )
  : Expr::TransportEquation( get_solnvar_name( dir ), get_rhs_tag( dir ) ),
    startloc_( startloc ),
    endloc_  ( endloc   ),
    npar_    ( npar     ),
    rndseed_ ( rndseed  )
  {
    const ParticleStringNames& sname = ParticleStringNames::self();
    Expr::ExpressionBuilder* builder;
    switch( dir ){
      case XDIR : builder = new ParticlePositionRHS::Builder( get_rhs_tag(dir), Expr::Tag( sname.xvel, Expr::STATE_N ) ); break;
      case YDIR : builder = new ParticlePositionRHS::Builder( get_rhs_tag(dir), Expr::Tag( sname.yvel, Expr::STATE_N ) ); break;
      case ZDIR : builder = new ParticlePositionRHS::Builder( get_rhs_tag(dir), Expr::Tag( sname.zvel, Expr::STATE_N ) ); break;
    }
    exprFactory.register_expression( builder, false );
  }

  //------------------------------------------------------------------

  ParticlePositionEquation::~ParticlePositionEquation()
  {}

  //------------------------------------------------------------------

  Expr::ExpressionID
  ParticlePositionEquation::
  initial_condition( Expr::ExpressionFactory& exprFactory )
  {
    const ParticleStringNames& sname = ParticleStringNames::self();
    const Expr::Tag pxpost( sname.xpos, Expr::STATE_N );
    return exprFactory.register_expression( new ParticlePosIC::Builder(pxpost,
                                                                       startloc_, endloc_, npar_, rndseed_));
  }

  //------------------------------------------------------------------

  std::string
  ParticlePositionEquation::
  get_solnvar_name( const Direction dir )
  {
    const ParticleStringNames& sname = ParticleStringNames::self();
    std::string name;
    switch( dir ){
      case XDIR : name = sname.xpos; break;
      case YDIR : name = sname.ypos; break;
      case ZDIR : name = sname.zpos; break;
    }
    return name;
  }

  Expr::Tag
  ParticlePositionEquation::
  get_rhs_tag( const Direction dir )
  {
    const ParticleStringNames& sname = ParticleStringNames::self();
    Expr::Tag tag;
    switch( dir ){
      case XDIR : tag = Expr::Tag( sname.xpos+"_RHS", Expr::STATE_N ); break;
      case YDIR : tag = Expr::Tag( sname.ypos+"_RHS", Expr::STATE_N ); break;
      case ZDIR : tag = Expr::Tag( sname.zpos+"_RHS", Expr::STATE_N ); break;
    }
    return tag;
  }

  //==================================================================

} // namespace Particle
