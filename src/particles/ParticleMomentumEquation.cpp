#include "ParticleMomentumEquation.h"
#include "ParticleStringNames.h"
#include <StringNames.h>

namespace Particle{



  class PMomICExpr : public Expr::Expression<ParticleField>
  {
  public:
    struct Builder : public Expr::ExpressionBuilder
    {
      Builder( const Expr::Tag pmomt,
               const Expr::Tag prhot,
               const Expr::Tag pvelt )
      : ExpressionBuilder(pmomt),
        prhot_( prhot ),
        pvelt_( pvelt )
      {}
      Expr::ExpressionBase* build() const{ return new PMomICExpr(prhot_, pvelt_); }
    private:
      const Expr::Tag prhot_, pvelt_;
    };

    void evaluate()
    {
      using namespace SpatialOps;
      ParticleField& mom = this->value();
      const ParticleField& rho = rho_->field_ref();
      const ParticleField& vel = vel_->field_ref();
      mom <<= rho * vel;
    }

    ~PMomICExpr(){}

    private:
    PMomICExpr( const Expr::Tag prhot,
                const Expr::Tag pvelt)
    : Expr::Expression<ParticleField>()
    {
      this->set_gpu_runnable(true);
      rho_ = this->create_field_request<ParticleField>( prhot );
      vel_ = this->create_field_request<ParticleField>( pvelt );
    }

    DECLARE_FIELDS( ParticleField, rho_, vel_ )

  };

  //==================================================================


  MomentumEquation::
  MomentumEquation( Expr::ExpressionFactory& exprFactory,
                    const Direction dir,
                    const Expr::Tag cvelTag )
  : Expr::TransportEquation( get_solnvar_name( dir ),
                             get_rhs_tag(dir) ),
    dir_( dir )
  {
    const ParticleStringNames& snam = ParticleStringNames::self();
    const StringNames& gasName = StringNames::self();

    const Expr::Tag gasdensityTag( gasName.density, Expr::STATE_N );
    const Expr::Tag pdensityTag  ( snam.pdensity,   Expr::STATE_N );
    const Expr::Tag dragTag      ( snam.dragc,      Expr::STATE_N );
    const Expr::Tag tauTag       ( snam.tau,        Expr::STATE_N );
    const Expr::Tag posTag       ( snam.xpos,       Expr::STATE_N );

    std::string dragTermName;
    switch( dir ){
      case XDIR : dragTermName = snam.Xmomdragterm; break;
      case YDIR : dragTermName = snam.Ymomdragterm; break;
      case ZDIR : dragTermName = snam.Zmomdragterm; break;
    }
    const Expr::Tag pdragtermTag( dragTermName, Expr::STATE_N );

    exprFactory.register_expression( new MomentumRHS<VelInterpOp>::Builder( get_rhs_tag(dir), gasdensityTag,pdensityTag,posTag, pdragtermTag ) );
  }

  //------------------------------------------------------------------

  MomentumEquation::
  ~MomentumEquation()
  {}

  //------------------------------------------------------------------


  Expr::ExpressionID
  MomentumEquation::
  initial_condition( Expr::ExpressionFactory& exprFactory)
  {
    const ParticleStringNames& snam = ParticleStringNames::self();
    Expr::Tag pmomt;
    switch( dir_ ){
      case XDIR : pmomt = Expr::Tag( snam.xmom, Expr::STATE_N ); break;
      case YDIR : pmomt = Expr::Tag( snam.ymom, Expr::STATE_N ); break;
      case ZDIR : pmomt = Expr::Tag( snam.zmom, Expr::STATE_N ); break;
    }
    const Expr::Tag pvelt(get_solnvar_name(dir_), Expr::STATE_N );
    const Expr::Tag prhot(snam.pdensity, Expr::STATE_N );
    return exprFactory.register_expression( new PMomICExpr::Builder( pmomt, prhot, pvelt));
    // need to form the IC expression
  }

  //------------------------------------------------------------------


  std::string
  MomentumEquation::
  get_solnvar_name( const Direction dir )
  {
    const ParticleStringNames& snam = ParticleStringNames::self();
    std::string name = "";
    switch( dir ){
      case XDIR : name=snam.xvel; break;
      case YDIR : name=snam.yvel; break;
      case ZDIR : name=snam.zvel; break;
    }
    return name;
  }

  //------------------------------------------------------------------


  Expr::Tag
  MomentumEquation::
  get_rhs_tag( const Direction dir )
  {
    const ParticleStringNames& snam = ParticleStringNames::self();
    Expr::Tag tag;
    switch( dir ){
      case XDIR : tag = Expr::Tag( snam.xmom+"_RHS", Expr::STATE_N ); break;
      case YDIR : tag = Expr::Tag( snam.ymom+"_RHS", Expr::STATE_N ); break;
      case ZDIR : tag = Expr::Tag( snam.zmom+"_RHS", Expr::STATE_N ); break;
    }
    return tag;
  }

  //------------------------------------------------------------------

} // namespace Particle
