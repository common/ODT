#ifndef ParticleMassEquation_h
#define ParticleMassEquation_h

#include <expression/TransportEquation.h>
#include "ParticleFieldsAndOperators.h"

namespace Particle{

  /**
   *  \class ParticleMassEquation
   *  \ingroup Particle
   *  \brief Sets up the ODE to describe evolution of the particle mass.
   */  
  class ParticleMassEquation
    : public Expr::TransportEquation
  {
  public:

    ParticleMassEquation(Expr::ExpressionFactory& exprFactory);
    ~ParticleMassEquation();


    /**
     *  \brief Build the expression that will set the initial
     *  conditions for the particle momentum.
     */
    Expr::ExpressionID initial_condition( Expr::ExpressionFactory& );
    
  private:    

    static std::string get_solnvar_name();
    static Expr::Tag get_rhs_tag();

    // no bcs for this - it is an ODE
    void setup_boundary_conditions( Expr::ExpressionFactory& ){}
    
  };

} // namespace Particle

#endif // ParticleMomentumEquation_h
