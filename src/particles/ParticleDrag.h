#ifndef ParticleDrag_Expr_h
#define ParticleDrag_Expr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particle{

  /**
   *  \class ParticleDrag
   *  \ingroup Particle
   *  \brief Calculates the particle drag force
   */
  template< typename FieldToParticleOp >
  class ParticleDrag
    : public Expr::Expression<SpatialOps::Particle::ParticleField>
  {
    typedef typename SpatialOps::Particle::ParticleField  ParticleField;
    typedef typename FieldToParticleOp::SrcFieldType      FieldT;


    DECLARE_FIELDS( ParticleField, dragCoef_, particlePos_, tau_, pvel_, psize_ )
    DECLARE_FIELD ( FieldT, carrierVel_ )

    FieldToParticleOp* op_;

    ParticleDrag(const Expr::Tag& carrierVelTag,
                 const Expr::Tag& dragCoefTag,
                 const Expr::Tag& tauTag,
                 const Expr::Tag& posTag,
                 const Expr::Tag& pvelTag,
                 const Expr::Tag& psizeTag );

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    public:
      /**
       *  \param pdragTag the result of this expression
       *  \param carrierVelTag The gas-phase velocity
       *  \param dragCoefTag the particle drag coefficient
       *  \param tauTag ?
       *  \param posTag the particle coordinate field
       *  \param pvelTag the particle velocity field
       */
      Builder( const Expr::Tag& pdragTag,
               const Expr::Tag& carrierVelTag,
               const Expr::Tag& dragCoefTag,
               const Expr::Tag& tauTag,
               const Expr::Tag& posTag,
               const Expr::Tag& pvelTag,
               const Expr::Tag& psizeTag );
      ~Builder(){}
      Expr::ExpressionBase* build() const;
    private:
      const Expr::Tag  cvt_, dct_,ptau_, ppt_,pvt_,pst_;
    };

    ~ParticleDrag(){}

    void bind_operators( const SpatialOps::OperatorDatabase& opDB );
    void evaluate();

  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################



  template<typename FieldToParticleOp>
  ParticleDrag<FieldToParticleOp>::
  ParticleDrag(const Expr::Tag& carrierVelTag,
               const Expr::Tag& dragCoefTag,
               const Expr::Tag& tauTag,
               const Expr::Tag& posTag,
               const Expr::Tag& pvelTag,
               const Expr::Tag& psizeTag )
    : Expr::Expression<ParticleField>()
  {
    this->set_gpu_runnable(false);  // need new particle operators...

    dragCoef_    = this->template create_field_request<ParticleField>( dragCoefTag   );
    particlePos_ = this->template create_field_request<ParticleField>( posTag        );
    tau_         = this->template create_field_request<ParticleField>( tauTag        );
    pvel_        = this->template create_field_request<ParticleField>( pvelTag       );
    psize_       = this->template create_field_request<ParticleField>( psizeTag      );
    carrierVel_  = this->template create_field_request<FieldT       >( carrierVelTag );
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  ParticleDrag<FieldToParticleOp>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    op_ = opDB.retrieve_operator<FieldToParticleOp>();
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  void
  ParticleDrag<FieldToParticleOp>::
  evaluate()
  {
    ParticleField& result = this->value();

    const ParticleField& dragCoef    = dragCoef_   ->field_ref();
    const ParticleField& particlePos = particlePos_->field_ref();
    const ParticleField& tau         = tau_        ->field_ref();
    const ParticleField& pvel        = pvel_       ->field_ref();
    const ParticleField& psize       = psize_      ->field_ref();
    const FieldT       & carrierVel  = carrierVel_ ->field_ref();

    SpatialOps::SpatFldPtr<ParticleField> inttmp = SpatialOps::SpatialFieldStore::get<ParticleField>( result );

    // assemble drag term: cd * A/2*rho*(v-up) = cd * 3/(4r) * m * (v-up)  (assumes a spherical particle)
    op_->set_coordinate_information( &particlePos, NULL, NULL, &psize );
    op_->apply_to_field( carrierVel, *inttmp );

    using namespace SpatialOps;
    result <<= ( *inttmp - pvel ) * dragCoef / tau;
  }

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  ParticleDrag<FieldToParticleOp>::
  Builder::Builder( const Expr::Tag& pdragTag,
                    const Expr::Tag& carrierVelTag,
                    const Expr::Tag& dragCoefTag,
                    const Expr::Tag& tauTag,
                    const Expr::Tag& posTag,
                    const Expr::Tag& pVelTag,
                    const Expr::Tag& psizeTag)
    : ExpressionBuilder(pdragTag),
      cvt_( carrierVelTag ),
      dct_( dragCoefTag ),
      ptau_( tauTag ),
      ppt_( posTag ),
      pvt_( pVelTag ),
      pst_(psizeTag)
  {}

  //------------------------------------------------------------------

  template<typename FieldToParticleOp>
  Expr::ExpressionBase*
  ParticleDrag<FieldToParticleOp>::Builder::build() const
  {
    return new ParticleDrag<FieldToParticleOp>( cvt_, dct_,ptau_, ppt_,pvt_,pst_ );
  }

  //------------------------------------------------------------------

} // Particle

#endif // ParticleDrag_Expr_h
