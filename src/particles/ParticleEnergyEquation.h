#ifndef ParticleEnergyEquation_h
#define ParticleEnergyEquation_h

#include <expression/TransportEquation.h>
#include "ParticleFieldsAndOperators.h"

#include "ParticleTemperatureRHS.h"
#include "ParticleTemperatureConvection.h"
#include "ParticleTempGasRadTerm.h"
#include "ParticleCp.h"

#include <OperatorsAndFields.h>

#include "ParticleStringNames.h"
#include <StringNames.h>
#include <coal/StringNames.h>

namespace Particle{

  /**
   *	\class EnergyEquation
   *	\ingroup Particle
   *	\brief Sets up the initial condition for the ODE to describe evolution
   *          of the particle Energy.
   *	\author Babak Goshayeshi  ( www.bgoshayeshi.com)
   *
   */
  /*
	class ParticleEnergyIC : public Expr::Expression<ParticleField> {
	public:
		struct Builder : public Expr::ExpressionBuilder
			{
		Builder(const Expr::Tag& ptempt,
						const double ptemp)
		: ExpressionBuilder(ptempt),
		ptemp_ ( ptemp)
		{}
		Expr::ExpressionBase* build() const{ return new ParticleEnergyIC(ptemp_); }
			private:
		const double ptemp_;
			};
		void advertise_dependents( Expr::ExprDeps& exprDeps ){}
		void bind_fields( const Expr::FieldManagerList& fml ) {}

		void evaluate()
		{
	ParticleField& ptempvalue = this->value();
	using namespace SpatialOps;
	ptempvalue <<= ptemp_;
		}
	private:
		ParticleEnergyIC(const double ptemp)
		: Expr::Expression<ParticleField>(),
		ptemp_ ( ptemp)
		{}
		const double ptemp_;
	};
   */

  /**
   *  \class EnergyEquation
   *  \ingroup Particle
   *  \brief Sets up the ODE to describe evolution of the particle Energy.
   *
   *  \todo initial conditions for particle Energy
   */
  class EnergyEquation
    : public Expr::TransportEquation
  {
  public:

    /**
     *  \brief Construct a EnergyEquation to solve for thr particle temperature
     *  \param exprFactory Expressions will be registered on this factory.
     */
    EnergyEquation( Expr::ExpressionFactory& exprFactory, const double walltemp, const double inttemp, const bool coalimplement);

    ~EnergyEquation();

    /**
     *  \brief Build the expression that will set the initial
     *  conditions for the particle Energy.
     */
    Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory);

  private:

    const double inttemp_;
    typedef SpatialOps::Particle::CellToParticle<VolField> VelInterpOp;

    static std::string get_solnvar_name();
    static Expr::Tag get_rhs_tag();

    // no bcs for this - it is an ODE
    void setup_boundary_conditions( Expr::ExpressionFactory& ){}

  };

//###############################################################
//
//                       Implementation
//
//###############################################################

  EnergyEquation::
  EnergyEquation( Expr::ExpressionFactory& exprFactory,
                  const double walltemp,
                  const double inttemp,
                  const bool coalimplement )
    : Expr::TransportEquation( get_solnvar_name(), get_rhs_tag() ),
      inttemp_( inttemp )
  {
    const ParticleStringNames& snam = ParticleStringNames::self();
    const StringNames& gasName = StringNames::self();
    const coal::StringNames& coalName = coal::StringNames::self();

    const Expr::Tag
      gcpTag       ( gasName.heat_capacity,        Expr::STATE_N ),
      gmuTag       ( gasName.viscosity,            Expr::STATE_N ),
      grhoTag      ( gasName.density,              Expr::STATE_N ),
      glambdaTag   ( gasName.thermal_conductivity, Expr::STATE_N ),
      gtempTag     ( gasName.temperature,          Expr::STATE_N ),
      pxposTag     ( snam.xpos,                    Expr::STATE_N ),
      pmassTag     ( snam.mass,                    Expr::STATE_N ),
      pReTag       ( snam.parRe,                   Expr::STATE_N ),
      ptempTag     ( snam.ptemperature,            Expr::STATE_N ),
      psizeTag     ( snam.size,                    Expr::STATE_N ),
      pconvtermTag ( snam.ptempconv,               Expr::STATE_N ),
      walltempTag  ( "wall_temp",                  Expr::STATE_N ),
      gasRadTermTag( "prtTempGasRadTerm",          Expr::STATE_N ),
      tempRHSModTag( "tempRHSMod",                 Expr::STATE_N );

    Expr::Tag pcpTag;
    if( coalimplement ){
      pcpTag = Expr::Tag( coalName.coal_cp, Expr::STATE_N );
    }
    else{
      pcpTag = Expr::Tag( snam.pheatcapacity, Expr::STATE_N );
      exprFactory.register_expression( new ParticleCp<ParticleField>::Builder(pcpTag,ptempTag));
    }

    exprFactory.register_expression(
        new ParticleTemperatureConvection<VelInterpOp>::Builder( pconvtermTag,
                                                                 pxposTag, pmassTag, pReTag, ptempTag,
                                                                 psizeTag, pcpTag, gcpTag,
                                                                 gmuTag, grhoTag, glambdaTag, gtempTag ) );

    exprFactory.register_expression( new Expr::ConstantExpr<ParticleField>::Builder( walltempTag, walltemp ) );

    exprFactory.register_expression( new ParticleTemperatureRHS<ParticleField>::
                                     Builder( get_rhs_tag(), pxposTag, pmassTag,
                                              ptempTag, psizeTag, pcpTag, walltempTag,
                                              pconvtermTag ) );

  }

  //------------------------------------------------------------------

  EnergyEquation::
  ~EnergyEquation()
  {}

  //------------------------------------------------------------------

  Expr::ExpressionID
  EnergyEquation::
  initial_condition( Expr::ExpressionFactory& exprFactory)
  {
    using namespace Expr;
    const ParticleStringNames& snam = ParticleStringNames::self();
		return exprFactory.register_expression( new ConstantExpr<ParticleField>::Builder( Tag(snam.ptemperature,STATE_N), inttemp_));
    
    // need to form the IC expression
  }

  //------------------------------------------------------------------

  std::string
  EnergyEquation::
  get_solnvar_name()
  {
    return Particle::ParticleStringNames::self().ptemperature;
  }

  //------------------------------------------------------------------

  Expr::Tag
  EnergyEquation::
  get_rhs_tag()
  {
    const Particle::ParticleStringNames& snam = Particle::ParticleStringNames::self();
    return Expr::Tag( snam.ptemperature+"_RHS", Expr::STATE_N );
  }

  //------------------------------------------------------------------

} // namespace Particle

#endif // ParticleEnergyEquation_h