#ifndef ParticletoCellConvEnergyExpr_h
#define ParticletoCellConvEnergyExpr_h

#include <expression/Expression.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/particles/ParticleFieldTypes.h>

#include "ParticleFieldsAndOperators.h"

namespace Particle{
//====================================================================


/**
 *  @class ParticletoCellConvEnergyExpr
 *  @author Naveen Punati
 *  @date   February, 2011
 *
 *  @brief calculates particle field influence on gas phase energy.
 */
template< typename ParticletoCellOp >
class ParticletoCellConvEnergyExpr
  : public Expr::Expression<typename ParticletoCellOp::DestFieldType>
{
  typedef typename ParticletoCellOp::SrcFieldType  ParticleFieldT;
  typedef typename ParticletoCellOp::DestFieldType GasFieldT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pcT_, psT_,gcT_,pconvtermT_,pmassT_,pcpT_;
    const double ca_;
  public:

    Builder( const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const Expr::Tag&,
             const double );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };

  ~ParticletoCellConvEnergyExpr(){}

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  ParticletoCellConvEnergyExpr( const Expr::Tag&,
                                const Expr::Tag&,
                                const Expr::Tag&,
                                const Expr::Tag&,
                                const Expr::Tag&,
                                const Expr::Tag&,
                                const double );

  DECLARE_FIELD( GasFieldT, gcoord_ )
  DECLARE_FIELDS( ParticleFieldT, parloc_, psize_, pconvterm_, pmass_, pcp_ )
  ParticletoCellOp* p2c_;
  const double cellarea_;
};

// ###################################################################
//
//                           Implementation
//
// ###################################################################


//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellConvEnergyExpr<ParticletoCellOp>::
ParticletoCellConvEnergyExpr( const Expr::Tag& pcoordTag,
                              const Expr::Tag& psizeTag,
                              const Expr::Tag& pmassTag,
                              const Expr::Tag& pcpTag,
                              const Expr::Tag& pconvtermTag,
                              const Expr::Tag& gcoordTag,
                              const double cellarea )
  : Expr::Expression<GasFieldT>(),
    cellarea_( cellarea )
{
  gcoord_    = this->template create_field_request<GasFieldT     >( gcoordTag    );
  parloc_    = this->template create_field_request<ParticleFieldT>( pcoordTag    );
  psize_     = this->template create_field_request<ParticleFieldT>( psizeTag     );
  pmass_     = this->template create_field_request<ParticleFieldT>( pmassTag     );
  pcp_       = this->template create_field_request<ParticleFieldT>( pcpTag       );
  pconvterm_ = this->template create_field_request<ParticleFieldT>( pconvtermTag );
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellConvEnergyExpr<ParticletoCellOp>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  p2c_ = opDB.retrieve_operator<ParticletoCellOp>();
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
void
ParticletoCellConvEnergyExpr<ParticletoCellOp>::
evaluate()
{
  using namespace SpatialOps;

  GasFieldT& result = this->value();

  const GasFieldT     & gcoord    = gcoord_   ->field_ref();
  const ParticleFieldT& parloc    = parloc_   ->field_ref();
  const ParticleFieldT& psize     = psize_    ->field_ref();
  const ParticleFieldT& pmass     = pmass_    ->field_ref();
  const ParticleFieldT& pcp       = pcp_      ->field_ref();
  const ParticleFieldT& pconvterm = pconvterm_->field_ref();

  //interpolate the energy production from particle locations to mesh
  SpatFldPtr<ParticleFieldT> energytmp = SpatialFieldStore::get<ParticleFieldT>( parloc );
  *energytmp <<= pmass * pcp * pconvterm;
  p2c_->set_coordinate_information( &parloc, NULL, NULL, &psize );
  p2c_->apply_to_field( *energytmp, result );

  const double dx = gcoord[1] - gcoord[0];
  const double volume = dx * cellarea_;

  result <<= -result/volume ;
}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
ParticletoCellConvEnergyExpr<ParticletoCellOp>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& pcoordTag,
         const Expr::Tag& psizeTag,
         const Expr::Tag& pmassTag,
         const Expr::Tag& pcpTag,
         const Expr::Tag& pconvtermTag,
         const Expr::Tag& gcoordTag,
         const double cellarea )
  : ExpressionBuilder(result),
    pcT_       ( pcoordTag   ),
    psT_       ( psizeTag    ),
    pmassT_    ( pmassTag    ),
    pcpT_      ( pcpTag      ),
    pconvtermT_( pconvtermTag),
    gcT_       ( gcoordTag   ),
    ca_(cellarea)
{}

//--------------------------------------------------------------------

template< typename ParticletoCellOp >
Expr::ExpressionBase*
ParticletoCellConvEnergyExpr<ParticletoCellOp>::Builder::build() const
{
  return new ParticletoCellConvEnergyExpr<ParticletoCellOp> (pcT_, psT_, pmassT_,pcpT_,pconvtermT_,gcT_,ca_);
}

} // Particle

#endif // ParticletoCellConvEnergyExpr_h
