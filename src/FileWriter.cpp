#include "FileWriter.h"
#include "OperatorsAndFields.h"

#include <sstream>
#include <stdexcept>

//-------------------------------------------------------------------

FileWriter::FileWriter( Expr::FieldManagerList& fml,
                        const std::string dbName,
                        const bool overWriteExisting )
  : db_( fml, dbName, overWriteExisting ),
    fml_( fml )
{}

//-------------------------------------------------------------------

FileWriter::~FileWriter()
{}

//-------------------------------------------------------------------

void FileWriter::write_file( const double timeStamp ) const
{
  std::cout << "writing files at t=" << timeStamp << std::endl;
  std::stringstream fnam;  fnam << timeStamp;
  db_.write_database( fnam.str() );
}

//-------------------------------------------------------------------

void
FileWriter::set_fields_from_file( const std::string& restartTime,
                                  const std::map<std::string,std::string>& varMap )
{
  for( std::map<std::string,std::string>::const_iterator inm=varMap.begin(); inm!=varMap.end(); ++inm ){
    db_.extract_field_from_database( restartTime, Expr::Tag( inm->first, Expr::STATE_N ), inm->second  );
  }
}

//-------------------------------------------------------------------

void
FileWriter::request_field_output( const Expr::Tag & fieldTag,
                                  const std::string & fieldAlias )
{
  if( fml_.field_manager<VolField>().has_field(fieldTag) ){
    request_field_output<VolField>(fieldTag,fieldAlias);
  }
  else if( fml_.field_manager<FaceField>().has_field(fieldTag) ){
    request_field_output<FaceField>(fieldTag,fieldAlias);
  }
  else if( fml_.field_manager<SpatialOps::Particle::ParticleField>().has_field(fieldTag) ){
    request_field_output<SpatialOps::Particle::ParticleField>(fieldTag,fieldAlias);
  }
  else{
    std::ostringstream msg;
    msg << "The requested field: " << fieldTag << " was not found" << std::endl;
    throw std::runtime_error( msg.str() );
  }
}

//-------------------------------------------------------------------
