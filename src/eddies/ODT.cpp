#include "ODT.h"

//--------------------------------- constructs ODT object-------------------------------------
ODT::ODT( const VolField& xmom,
          const VolField& ymom,
          const VolField& rho,
          const VolField& coord,
          const VolField& visc,
          const FaceField& flux,
          const std::vector<double>& coord_vec,
          const ODTParameters& params
          )
  : umom_(xmom),
    vmom_(ymom),
    wmom_(ymom),
    rho_(rho),
    coord_(coord),
    kinvisc_(visc),
    stress_(flux),
    lagrangeCoefs_(coord_vec),
    params_(params)
{

  eddy_select();
  threevelcomp_ = false;  // if 2 velocity component system

  if(implementEddy_){
    triplet_map( rho_, temprho_ );
    triplet_map( umom_,tempumom_);
    triplet_map( vmom_,tempvmom_);
  }

  if(implementEddy_){

    assert( sindex_ < coord_.window_with_ghost().glob_npts() );
    assert( eindex_ < coord_.window_with_ghost().glob_npts() );
    assert( sindex_ >= 0 );
    assert( eindex_ >= 0 );
    assert( eindex_ > sindex_ );
    assert( newloc_.size() == size_t(eindex_ - sindex_+1) );
    Kvec_.clear();
    VolField::const_iterator coorditer = coord_.begin() + sindex_;
    VolField::const_iterator coorditerend = coord_.begin() + eindex_;
    std::vector<double>::const_iterator newcoorditer = newloc_.begin();
    for(;coorditer != coorditerend; ++coorditer,++newcoorditer)
      Kvec_.push_back(*coorditer - *newcoorditer);

    Kvec_.push_back(0.0);
  }


  if(implementEddy_)
    calculate_kernel();

}

//--------------------------------------------------------------------------------------------

//---------------- constructs ODT object - 3 velocity components----------------
ODT::ODT( const VolField& xmom,
          const VolField& ymom,
          const VolField& zmom,
          const VolField& rho,
          const VolField& coord,
          const VolField& visc,
          const FaceField& flux,
          const std::vector<double>& coord_vec,
          const ODTParameters& params
          )
  : umom_(xmom),
    vmom_(ymom),
    wmom_(zmom),
    rho_(rho),
    coord_(coord),
    kinvisc_(visc),
    stress_(flux),
    lagrangeCoefs_(coord_vec),
    params_(params)
{

  threevelcomp_ = true;  // if 3 velocity component system
  eddy_select();

  if(implementEddy_){
    triplet_map( rho_, temprho_ );
    triplet_map( umom_,tempumom_);
    triplet_map( vmom_,tempvmom_);
    triplet_map( wmom_,tempwmom_);
  }

  if(implementEddy_){

    assert( sindex_ < coord_.window_with_ghost().glob_npts() );
    assert( eindex_ < coord_.window_with_ghost().glob_npts() );
    assert( sindex_ >= 0 );
    assert( eindex_ >= 0 );
    assert( eindex_ > sindex_ );
    assert( newloc_.size() == size_t(eindex_ - sindex_+1) );

    VolField::const_iterator coorditer = coord_.begin() + sindex_;
    VolField::const_iterator coorditerend = coord_.begin() + eindex_;
    std::vector<double>::const_iterator newcoorditer = newloc_.begin();
    for(;coorditer != coorditerend; ++coorditer,++newcoorditer)
      Kvec_.push_back(*coorditer - *newcoorditer);

    Kvec_.push_back(0.0);
  }


  if(implementEddy_)
    calculate_kernel();

}

//--------------------------------------------------------------------------------------------



//--------------------------------------transforms field-----------------------------------

//--------triplet mapping and kernel transformation will be done here--------
void ODT::transform( VolField& field,
                     const KernelSelector op )
{
  if( !implementEddy_ ) {
    return;
  }

  if(&field[0] == &rho_[0]){
    for(size_t i=1;i<temprho_.size()-1;i++)
      field[sindex_+i] = temprho_[i];
  }
  else if(&field[0] == &umom_[0]){
    for(size_t i=1;i<tempumom_.size()-1;i++)
      field[sindex_+i] = tempumom_[i]+c1_*Kvec_[i];
  }
  else if(&field[0] == &vmom_[0]){
    for(size_t i=1;i<tempvmom_.size()-1;i++)
      field[sindex_+i] = tempvmom_[i]+c2_*Kvec_[i];
  }
  else if(threevelcomp_ && &field[0] == &wmom_[0]){
    for(size_t i=1;i<tempvmom_.size()-1;i++)
      field[sindex_+i] = tempwmom_[i]+c3_*Kvec_[i];
  }
  else{
    triplet_map(field);
  }
}

//------------------------------------------------------------------------------------------



//---------------------------------- randomly selects eddy length, location------------------------
void ODT::eddy_select(){

  implementEddy_ = true;

  //------this uses shear stress to identify eddy starting location-----------
  FaceField::const_iterator striterst = stress_.begin();
  FaceField::const_iterator striteren = stress_.end();
  std::vector<int> stressindices;
  int count = 0;
  for(int i=0;striterst!=striteren;++i,++striterst){
    if(fabs(*striterst) > params_.stresstol){
      stressindices.push_back(i);
      count++;
    }
  }
  if(count<=0){
    implementEddy_ = false;
    return;
  }
  const double r1 = double(rand())/double(RAND_MAX);
  const int ran_loc = int(r1*double(stressindices.size()-1));
  y0_ = coord_[stressindices[ran_loc]];

  //scaling analysis is used to determine the eddy length
  const double r2 = double(rand())/double(RAND_MAX);
  const double kol_LS = params_.integLS / (pow(params_.Re,0.75));
  maxeddy_ = params_.integLS;
  mineddy_ = 6*kol_LS;

  Lp_ = exp( ( log(params_.integLS ) + log(kol_LS) )/2 );
  factorc_ = 2*Lp_/(exp(-2*Lp_/maxeddy_) - exp(-2*Lp_/mineddy_));
  eddyl_ = -2*Lp_/log((2*Lp_*r2/factorc_) + exp(-2*Lp_/mineddy_));

  // eddy starting location will be the centre of the eddy
  y0_ = y0_ - eddyl_/2.0;

  VolField::const_iterator coord_iterbegin = coord_.begin();
  VolField::const_iterator coord_iterend = coord_.end();

  if( (y0_+eddyl_) > (*(coord_iterend-2))){
    implementEddy_ = false;
    return;
  }

  for(int i =0;coord_iterbegin!=coord_iterend;++coord_iterbegin,i++){
    if(y0_<= (*coord_iterbegin)){
      y0_ = (*coord_iterbegin);
      sindex_ = i;
      break;
    }
  }

  coord_iterbegin = coord_.begin() + sindex_;
  for(int i=sindex_; coord_iterbegin!=coord_iterend;++coord_iterbegin,++i){
    if((y0_+eddyl_)<= *coord_iterbegin){
      eddyl_ = (*coord_iterbegin)-y0_;
      eindex_ = i;
      tmpoints_ = i+1-sindex_;
      break;
    }
  }

  if(tmpoints_ % 3 ==1){
    const double adjus = coord_[eindex_+1]-coord_[eindex_];
    eindex_ = eindex_ + 1;
    tmpoints_ += 1;
    eddyl_ = eddyl_ + adjus;
  }
  if(tmpoints_<6 ){
    implementEddy_ = false;
    return;
  }
}
//-------------------------------------------------------------------------------------------



//------------------------triplet mapping temporary storage--------------------------

void ODT::triplet_map( const VolField& field, std::vector<double>& tempfield )
{
  triplet_map_kernel(field);
  for(size_t i=0;i<phif_.size();i++)
    tempfield.push_back(phif_[i]+ gvec_[i]*gamma_);
}

//---------------------------------------------------------------------------------------


//------------------------triplet mapping --------------------------

void ODT::triplet_map(VolField& field){
  triplet_map_kernel(field);
  for(size_t i=1;i<(phif_.size()-1);i++)
    field[i+sindex_] = phif_[i]+ gvec_[i]*gamma_;
}

//---------------------------------------------------------------------------------------

//----------------------triplet mapping for large eddy suppression------------------------------
void ODT::triplet_map( const VolField& field, std::vector<double>& tempfield, const double eddy_l, const double eddy_loc, const int si, const int ei ){
  triplet_map_les(field,eddy_l,eddy_loc,si,ei);
  for(size_t i=0;i<phif_.size();i++)
    tempfield.push_back(phif_[i]+ gvec_[i]*gamma_);
}
//----------------------------------------------------------------------------------------



//-------------------------------calculates kernel----------------------------
void ODT::calculate_kernel( ){

  const double integA  = custom_integral(Kvec_    );

  const double integB1 = custom_integral(tempumom_);
  const double integB2 = custom_integral(tempvmom_);
  const double Q1 = pow(integB1,2)/(2*integA);
  const double Q2 = pow(integB2,2)/(2*integA);

  // changed to consider three velocity component system
  double integB3 = 0.0, Q3 = 0.0;
  if(threevelcomp_){
    integB3 = custom_integral(tempwmom_);
    Q3 = pow(integB3,2)/(2*integA);
  }

  double visc_sum = 0.0, rho_sum = 0.0;
  const double* rhostart = &rho_[sindex_];
  const double* rhonext = &rho_[sindex_+1];
  const double* rhostop = &rho_[eindex_];
  const double* visstart = &kinvisc_[sindex_];
  const double* visnext = &kinvisc_[sindex_+1];
  const double* coordstart = &coord_[sindex_];
  const double* coordnext = &coord_[sindex_+1];

  for( ; rhostart!=rhostop; ++rhostart,++rhonext,++coordstart,++coordnext,++visstart,++visnext){
    double dummy3 = *coordnext - *coordstart;
    visc_sum += 0.5*dummy3*( *visstart +  *visnext );
    rho_sum  += 0.5*dummy3*(*rhostart + *rhonext);
  }

  const double visc_avg = visc_sum/eddyl_;
  const double rho_avg = rho_sum/eddyl_;

  double sqrtterm = 0.0;
  if(threevelcomp_)
    sqrtterm = (Q3 + Q2 + Q1)/(rho_avg*eddyl_) - params_.z *pow((visc_avg/eddyl_),2);
  else
    sqrtterm = (Q2 + Q1)/(rho_avg*eddyl_) - params_.z *pow((visc_avg/eddyl_),2);


  if(sqrtterm<0){
    implementEddy_ = false;
    return;
  }

  const double eddy_rate = (params_.c/(pow(eddyl_,3)))*sqrt(sqrtterm);
  const double f_l = (factorc_/(eddyl_*eddyl_))*exp(-2*Lp_/eddyl_);
  const double g_l = 1.0/(maxeddy_-mineddy_);
  const double probability = eddy_rate*params_.timestep/(f_l*g_l);
  if(probability>1.0){
    implementEddy_ = false;
    return;
  }
  const double r3 = double(rand())/double(RAND_MAX);
  if(probability<r3){
    implementEddy_ = false;
    return;
  }
/*
  //for suppressing large eddies
  if(tmpoints_>=18){
    double eddy_l = eddyl_/3.0;
    double eddy_loc = y0_;
    int si = sindex_;
    int ei = si;
    VolField::const_iterator coord_iterbegin = &coord_[si];
    VolField::const_iterator coord_iterend   = coord_.end();
    for(int i=0;i<3;i++){
      si = ei;
      eddy_loc = coord_[si];

      coord_iterbegin = &coord_[si];
      for(int j =si;coord_iterbegin!=coord_iterend;++coord_iterbegin,j++){
        if((y0_+(i+1)*(eddyl_/3))<= (*coord_iterbegin)){
          ei = j;
          eddy_l = coord_[ei] - coord_[si];
          break;
        }
      }
      large_eddy_suppression(eddy_l, eddy_loc, si, ei);

      if(!implementEddy_)
        return;
    }
  }
  */

  double sqrtterm1 = 0.0, sqrtterm2 = 0.0, sqrtterm3 = 0.0;
  if(threevelcomp_){
    sqrtterm1 = sqrt( pow((integB1/integA),2) * (1-params_.alpha)
                      + pow((integB2/integA),2) * params_.alpha/2
                      + pow((integB3/integA),2) * params_.alpha/2 );
    sqrtterm2 = sqrt( pow((integB2/integA),2) * (1-params_.alpha)
                      + pow((integB1/integA),2) * params_.alpha/2
                      + pow((integB3/integA),2) * params_.alpha/2 );
    sqrtterm3 = sqrt( pow((integB3/integA),2) * (1-params_.alpha)
                      + pow((integB1/integA),2) * params_.alpha/2
                      + pow((integB2/integA),2) * params_.alpha/2 );
  }
  else{
    sqrtterm1 = sqrt( pow((integB1/integA),2) * (1-params_.alpha)
                      + pow((integB2/integA),2) * params_.alpha );
    sqrtterm2 = sqrt( pow((integB2/integA),2) * (1-params_.alpha)
                      + pow((integB1/integA),2) * params_.alpha );
  }


  if(std::isnan(sqrtterm1) || std::isnan(sqrtterm2) || std::isnan(sqrtterm3)){
    implementEddy_ = false;
    return;
  }


  double signsym = (integB1<0.0) ? -1.0 : 1.0;
  c1_ = -(integB1/integA) + signsym* sqrtterm1;
  signsym = (integB2>0)  ? 1.0 : -1.0;
  c2_ = -(integB2/integA) + signsym* sqrtterm2;
  if(threevelcomp_){
    signsym = (integB3>0)  ? 1.0 : -1.0;
    c3_ = -(integB3/integA) + signsym* sqrtterm3;
  }

  eddylifetime_ = eddyl_/sqrt(sqrtterm);
}
//-----------------------------------------------------------------------------------



//---------------------------------suppresses large eddies-----------------------------
void ODT::large_eddy_suppression(double eddy_l, double eddy_loc, int si, int ei){


  std::vector<double> lestemprho,lestempumom,lestempvmom,lestempwmom,lesKvec;


  triplet_map(rho_,lestemprho,eddy_l,eddy_loc,si,ei );
  triplet_map(umom_,lestempumom,eddy_l,eddy_loc,si,ei );
  triplet_map(vmom_,lestempvmom,eddy_l,eddy_loc,si,ei);

  //three component velocity system
  if(threevelcomp_)
    triplet_map(wmom_,lestempwmom,eddy_l,eddy_loc,si,ei);

  VolField::const_iterator coorditer = coord_.begin() + si;
  VolField::const_iterator coorditerend = coord_.end() + (ei+1);
  std::vector<double>::iterator newcoorditer = newloc_.begin();
  for(;coorditer != coorditerend; ++coorditer,++newcoorditer)
    lesKvec.push_back(*coorditer - *newcoorditer);

  const double integA  = custom_integral(lesKvec,    lestemprho,lesKvec,si);
  const double integB1 = custom_integral(lestempumom,lestemprho,lesKvec,si);
  const double integB2 = custom_integral(lestempvmom,lestemprho,lesKvec,si);

  const double Q1 = pow(integB1,2)/(2*integA);
  const double Q2 = pow(integB2,2)/(2*integA);

  //three component velocity system
  double integB3 = 0.0, Q3 = 0.0;
  if(threevelcomp_){
    integB3 = custom_integral(lestempwmom,lestemprho,lesKvec,si);
    Q3 = pow(integB3,2)/(2*integA);
  }

  double visc_sum = 0.0, rho_sum = 0.0;
  const double* rhostart   = &rho_[si];
  const double* rhonext    = &rho_[si+1];
  const double* rhostop    = &rho_[ei];
  const double* visstart   = &kinvisc_[si];
  const double* visnext    = &kinvisc_[si+1];
  const double* coordstart = &coord_[si];
  const double* coordnext  = &coord_[si+1];

  for( ; rhostart!=rhostop; ++rhostart,++rhonext,++coordstart,++coordnext,++visstart,++visnext){
    double dummy3 = *coordnext - *coordstart;
    visc_sum += 0.5*dummy3*( *visstart +  *visnext );
    rho_sum  += 0.5*dummy3*(*rhostart + *rhonext);
  }

  const double visc_avg = visc_sum/eddy_l;
  const double rho_avg = rho_sum/eddy_l;

  double sqrtterm = 0.0;
  if(threevelcomp_)
    sqrtterm = (eddy_l/pow(visc_avg,2)) * ((Q3 + Q2 + Q1)/rho_avg) - params_.z ;
  else
    sqrtterm = (eddy_l/pow(visc_avg,2)) * ((Q2 + Q1)/rho_avg) - params_.z ;

  if(sqrtterm<0){
    implementEddy_ = false;
    return;
  }
}

//-----------------------------------------------------------------------------------------





//---------------------------integration for terms in energy conservation for les-----------------
double ODT::custom_integral(const std::vector<double>& field, const std::vector<double>& lesrho, const std::vector<double>& Kles, const int lessi) {

  double integval = 0.0;
  const double* rhostart = &lesrho[0];
  const double* rhonext  = &lesrho[1];
  const double* rhoend   = &lesrho[lesrho.size()-1];

  const double* Kstart   = &Kles[0];
  const double* Knext = &Kles[1];

  const double* fieldstart   = &field[0];
  const double* fieldnext = &field[1];

  const double* coordstart = &coord_[lessi];
  const double* coordnext  = &coord_[lessi + 1];

  for(;rhostart!=rhoend;++rhostart,++rhonext,++Kstart,++Knext,++fieldstart,++fieldnext,++coordstart,++coordnext){
    integval += (*coordnext - *coordstart)*((*Kstart * *fieldstart/ *rhostart) + (*Knext * *fieldnext/ *rhonext));
  }

  return (0.5*integval);
}

//-----------------------------------------------------------------------------------------


//----------------------------supports continuous tripplet mapping procedure----------------------
double ODT::interpolate( const double newlocation, const VolField& field ){
  std::vector<int> indices;
  std::vector<double> coefs;
  lagrangeCoefs_.get_interp_coefs_indices( newlocation, 2, coefs, indices );
  double sum=0.0;
  for(size_t j=0;j<coefs.size();j++){
    sum += coefs[j]*field[indices[j]];
  }
  return sum;
}

//-----------------------------------------------------------------------------------------------


//------------------------------integration - Trapezoidal rule--------------------------
double ODT::integrate( const double* xstart, const double* start, const double* const stop ){
  double sum = 0.0;
  const double* fa = start;
  const double* fb = ++start;
  const double* a  = xstart;
  const double* b  = ++xstart;
  for( ; fa!=stop; ++fa, ++fb, ++a, ++b ){
    sum += 0.5*(*b-*a)*( *fa+*fb );
  }
  return sum;
}
//-------------------------------------------------------------------------------------


//------------------integration for terms in energy conservation-------------
double ODT::custom_integral( const std::vector<double>& field ) {

  double integval = 0.0;
  const double* rhostart = &temprho_[0];
  const double* rhonext  = &temprho_[1];
  const double* rhoend   = &temprho_[temprho_.size()-1];

  const double* Kstart   = &Kvec_[0];
  const double* Knext = &Kvec_[1];

  const double* fieldstart   = &field[0];
  const double* fieldnext = &field[1];

  const double* coordstart = &coord_[sindex_];
  const double* coordnext  = &coord_[sindex_ + 1];

  for(;rhostart!=rhoend;++rhostart,++rhonext,++Kstart,++Knext,++fieldstart,++fieldnext,++coordstart,++coordnext){
    integval += (*coordnext - *coordstart)*((*Kstart * *fieldstart/ *rhostart) + (*Knext * *fieldnext/ *rhonext));
  }

  return (0.5*integval);
}
//------------------------------------------------------------------------------


//----------------------------------prints kernel values-----------------------------------

void ODT::printKernel() const{
  if(implementEddy_){
    std::cout << "c1  :  " << c1_
              << "    c2  :  " << c2_ << std::endl;
  }
}

//----------------------------------------------------------------------------------------


//-------------------------------calculates kernel for triplet map----------------------------

void ODT::triplet_map_kernel(const VolField& field){
  //should be changed to void...to check triplrt map conservation returns double
  phif_.clear();
  newloc_.clear();
  phif_.push_back(field[sindex_]);
  newloc_.push_back(coord_[sindex_]);
  for(int i=sindex_+1;i<eindex_;i++){ //,j++){
    double newlocation=0.0;
    if(coord_[i]>=y0_ && coord_[i]<(y0_ +eddyl_/3)){
      newlocation = 3*coord_[i] - 2*y0_;
      newloc_.push_back(newlocation);
      phif_.push_back(interpolate(newlocation,field));
    }
    else if(coord_[i]>=(y0_ +eddyl_/3) && coord_[i]<(y0_ + (2*eddyl_)/3)){
      newlocation = 4*y0_ + 2*eddyl_ - 3*coord_[i];
      newloc_.push_back(newlocation);
      phif_.push_back(interpolate(newlocation,field));
    }
    else if(coord_[i]>=(y0_ + (2*eddyl_)/3) && coord_[i]<=(y0_+eddyl_)){
      newlocation = 3*coord_[i] - 2*y0_ - 2*eddyl_;
      newloc_.push_back(newlocation);
      phif_.push_back(interpolate(newlocation,field));
    }
  }
  phif_.push_back(field[eindex_]);
  newloc_.push_back(coord_[eindex_]);

  //---------evaluates G vector for triplet map kernel--------
  gvec_.clear();
  for(int i=sindex_;i<eindex_;i++){
    if(coord_[i]>=y0_ && coord_[i]<(y0_ +eddyl_/3)){
      gvec_.push_back((3/eddyl_)*(coord_[i]-y0_));
    }
    else if(coord_[i]>=(y0_ +eddyl_/3) && coord_[i]<(y0_ + (2*eddyl_)/3)){
      gvec_.push_back((-3/eddyl_)*(coord_[i]-y0_-eddyl_/3)+1);
    }

    else if(coord_[i]>=(y0_ + (2*eddyl_)/3) && coord_[i]<=(y0_+eddyl_)){
      gvec_.push_back((3/eddyl_)*(coord_[i]-y0_-2*eddyl_/3));
    }
  }
  gvec_.push_back(0.0);

  //------To evaluate the triplet map kernel coefficient----------
  double phi_integ = integrate(&coord_[sindex_], &field[sindex_],&field[eindex_]);
  double phif_integ = integrate(&coord_[sindex_],&phif_[0],&phif_[phif_.size()-1]);
  double G_integ = integrate(&coord_[sindex_],&gvec_[0],&gvec_[gvec_.size()-1]);

  gamma_ = (phi_integ - phif_integ)/G_integ;
  //return phi_integ;
}


//----------------------------------------------------------------------------------------



//-----------------------------triplet mapping for large eddy suppression--------------------------
void  ODT::triplet_map_les( const VolField& field, const double eddy_l, const double eddy_loc, const int si, const int ei ){
  //should be changed to void...to check triplrt map conservation returns double
  phif_.clear();
  newloc_.clear();
  phif_.push_back(field[si]);
  newloc_.push_back(coord_[si]);
  for(int i=si+1;i<ei ;i++){ //,j++){
    double newlocation=0.0;
    if(coord_[i]>=eddy_loc && coord_[i]<(eddy_loc +eddy_l/3)){
      newlocation = 3*coord_[i] - 2*eddy_loc;
      newloc_.push_back(newlocation);
      phif_.push_back(interpolate(newlocation,field));
    }
    else if(coord_[i]>=(eddy_loc +eddy_l/3) && coord_[i]<(eddy_loc + (2*eddy_l)/3)){
      newlocation = 4*eddy_loc + 2*eddyl_ - 3*coord_[i];
      newloc_.push_back(newlocation);
      phif_.push_back(interpolate(newlocation,field));
    }
    else if(coord_[i]>=(eddy_loc + (2*eddy_l)/3) && coord_[i]<=(eddy_loc+eddy_l)){
      newlocation = 3*coord_[i] - 2*eddy_loc - 2*eddy_l;
      newloc_.push_back(newlocation);
      phif_.push_back(interpolate(newlocation,field));
    }
  }
  phif_.push_back(field[ei ]);
  newloc_.push_back(coord_[ei ]);

  //---------evaluates G vector for triplet map kernel--------
  gvec_.clear();
  for(int i=si;i<ei ;i++){
    if(coord_[i]>=eddy_loc && coord_[i]<(eddy_loc +eddy_l/3)){
      gvec_.push_back((3/eddy_l)*(coord_[i]-eddy_loc));
    }
    else if(coord_[i]>=(eddy_loc +eddy_l/3) && coord_[i]<(eddy_loc + (2*eddy_l)/3)){
      gvec_.push_back((-3/eddy_l)*(coord_[i]-eddy_loc-eddy_l/3)+1);
    }

    else if(coord_[i]>=(eddy_loc + (2*eddy_l)/3) && coord_[i]<=(eddy_loc+eddy_l)){
      gvec_.push_back((3/eddy_l)*(coord_[i]-eddy_loc-2*eddy_l/3));
    }
  }
  gvec_.push_back(0.0);

  //------To evaluate the triplet map kernel coefficient----------
  double phi_integ = integrate(&coord_[si], &field[si],&field[ei ]);
  double phif_integ = integrate(&coord_[si],&phif_[0],&phif_[phif_.size()-1]);
  double G_integ = integrate(&coord_[si],&gvec_[0],&gvec_[gvec_.size()-1]);

  gamma_ = (phi_integ - phif_integ)/G_integ;
  //return phi_integ;
}


//---------------------------------------------------------------------------------------------

