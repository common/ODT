#ifndef ODTSrc_h
#define ODTSrc_h

#include <expression/ExprLib.h>


struct EddyInfo
{
  const double scalefac;
  const double tstart, tstop;
  const std::vector< double > df;
  const int istart, npts;

  EddyInfo( const double startTime,
            const double tduration,
            const std::vector<double>& fieldChange,
            const int fieldStartIndex )
    : scalefac( 1.0/(tduration) ),
      tstart( startTime ),
      tstop( startTime+tduration ),
      df( fieldChange ),
      istart( fieldStartIndex ),
      npts( fieldChange.size() )
  {}
};


/**
 *  @class ODTSrc
 *  @author James C. Sutherland
 *  @brief A source term to add to equations to represent an eddy
 *         occurring over a finite time interval.
 *
 *  Note that we are not using this since it poses significant challenges for reacting flow...
 */
template< typename FieldT >
class ODTSrc
  : public Expr::Expression<FieldT>
{
public:

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder();
    Expr::ExpressionBase* build( const Expr::ExpressionID& id,
				 const Expr::ExpressionRegistry& reg ) const;
  };

  ~ODTSrc();

  /**
   *  @brief Add an eddy to contribute to the source term.  Multiple
   *         eddies may be contributing the source term at any given
   *         instant in time.  After an eddy expires it is no longer
   *         considered.
   */
  void add_eddy_event( const double tstart,
                       const double tduration,
                       const std::vector<double>& df,
                       const int istart );

  void evaluate();


protected:
  ODTSrc( const Expr::ExpressionID& id,
          const Expr::ExpressionRegistry& reg );

  // The simulation time - required for eddy source term.
  DECLARE_FIELD( SpatialOps::SingleValueField, time_ )

  typedef std::list< EddyInfo > EddyList;
  EddyList eddies_;
};

//====================================================================




//====================================================================
//
//                          Implementation
//
//====================================================================




//====================================================================

template< typename FieldT >
ODTSrc<FieldT>::
ODTSrc( const Expr::ExpressionID& id,
        const Expr::ExpressionRegistry& reg
  : Expr::Expression<FieldT>( id, reg )
{
  time_ = this->template create_field_request<FieldT>( Expr::Tag( "time", Expr::STATE_NONE ) );
}

//--------------------------------------------------------------------

template< typename FieldT >
ODTSrc<FieldT>::
~ODTSrc()
{}

//--------------------------------------------------------------------

template< typename FieldT >
void
ODTSrc<FieldT>::
add_eddy_event( const double tstart,
                const double tduration,
                const std::vector<double>& df,
                const int istart )
{
  eddies_.insert( EddyInfo( tstart, tstart, tduration, df, istart ) );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
ODTSrc<FieldT>::
evaluate()
{
  FieldT& src = this->value();
  src = 0.0;
  const SpatialOps::SingleValueField& time = time_->field_ref();

  // loop over eddies to add as source terms.
  for( EddyList::iterator ieddy=eddies_.begin(); ieddy!=eddies_.end(); ++ieddy ){

    // if this eddy is active, add its contribution to the source term
    if( time[0] >= ieddy->tstart ){
      const int ioffset = ieddy->istart;
      typename FieldT::iterator isrc = src.begin() + ioffset;
      const typename FieldT::iterator iend = isrc + ieddy->npts;
      std::vector<double>::iterator idf = ieddy->df.begin();
      for( ; isrc!=iend; ++isrc, ++idf ){
        *isrc += *idf;
      }
    }

    // kill this eddy if we are past its lifetime
    if( time[0] >= ieddy->tstop ){
      eddies_.remove( *ieddy );
    }
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
ODTSrc<FieldT>::Builder::
Builder()
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
ODTSrc<FieldT>::Builder::
build( const Expr::ExpressionID& id,
       const Expr::ExpressionRegistry& reg ) const
{
  return new ODTSrc<FieldT>( id, reg );
}

//--------------------------------------------------------------------

#endif // ODTSrc_h
