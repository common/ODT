#ifndef ODT_h
#define ODT_h

#include "LagrangePoly.h"

#include <OperatorsAndFields.h>

#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()


/**
 *  @struct ODTParameters
 *  \todo Implement a reader method to load these from the parser?
 */
struct ODTParameters
{
  double alpha;        ///< The velocity redistribution parameter [0,1]
  double z;            ///< The viscous cutoff parameter
  double beta;         ///< Parameter for Large eddy suppression
  double c;            ///< The eddy rate parameter
  double stresstol;    ///< The threshold for the shear stress in considering eddy locations
  double domainLength; ///< Length of the ODT domain
  double Re;           ///< Reynolds number of the flow
  double integLS;      ///< Integral length scale
  double timestep;     ///< The time step for the time integrator (assumed constant)
};


class ODT
{
public:

  enum KernelSelector
    {
      NONE,
      KINETIC_ENERGY_KERNEL
    };

  ODT( const VolField& xmom,
       const VolField& ymom,
       const VolField& rho,
       const VolField& coord,
       const VolField& visc,
       const FaceField& flux,
       const std::vector<double>& coord_vec,
       const ODTParameters& params
       );

  ODT( const VolField& xmom,
       const VolField& ymom,
       const VolField& zmom,
       const VolField& rho,
       const VolField& coord,
       const VolField& visc,
       const FaceField& flux,
       const std::vector<double>& coord_vec,
       const ODTParameters& params
       );

  void transform( VolField&, const KernelSelector = NONE );
  bool has_eddy() const { return implementEddy_; }
  void printKernel() const;
  int return_sindex() const{ return sindex_;}
  int return_eindex() const{ return eindex_;}
  double return_eddylifetime() const{ return eddylifetime_;}


protected:
  void eddy_select();
  double interpolate(const double, const VolField&);
  void triplet_map_kernel(const VolField&);
  void triplet_map_les(const VolField& field, const double eddy_l, const double eddy_loc, const int si, const int ei);
  double integrate(const double* , const double* , const double* const );
  void kernel();
  void large_eddy_suppression(double, double, int, int);
  void triplet_map(VolField&);
  void triplet_map( const VolField&, std::vector<double>& tempfield );
  void triplet_map( const VolField& field, std::vector<double>& tempfield, const double eddy_l, const double eddy_loc, const int si, const int ei );
  void calculate_kernel();
  double custom_integral(const std::vector<double>& field);
  double custom_integral(const std::vector<double>& field, const std::vector<double>& lesrho, const std::vector<double>& Kles, const int lessi_);

private:
  const VolField &umom_, &vmom_,&wmom_, &rho_, &coord_, &kinvisc_;
  const FaceField& stress_;
  const LagrangeCoefficients lagrangeCoefs_;
  const ODTParameters params_;
  double y0_,eddyl_;         // Eddy location and length
  int tmpoints_;             // number of points covered by eddy
  int sindex_,eindex_;       // starting and ending locations of eddy on the mesh
  double c1_,c2_,c3_,gamma_;     // kernel coefficients
  std::vector<double> gvec_, phif_, newloc_;
  bool implementEddy_,threevelcomp_;
  double Lp_,maxeddy_,mineddy_;
  double factorc_,time_;
  std::vector<double> temprho_,tempumom_,tempvmom_,tempwmom_,Kvec_;
  double eddylifetime_;


};
#endif
